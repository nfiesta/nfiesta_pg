--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
SELECT nfiesta.fn_delete_param_area(gid)
FROM nfiesta.f_a_param_area
WHERE param_area_code = 'NFR27';

SELECT nfiesta.fn_create_param_area(3, estimation_cells, 'NFRDs')
FROM
	(SELECT array_agg(id) AS estimation_cells
	FROM nfiesta.c_estimation_cell
	WHERE label in ('25kmE4625N2925', '25kmE4625N2950', '25kmE4650N2900', '25kmE4650N2925', '25kmE4650N2950')) AS t1
;

SELECT nfiesta.fn_create_param_area(3, estimation_cells, 'NFRDs')
FROM
	(SELECT array_append(array_agg(id), 131) AS estimation_cells
	FROM nfiesta.c_estimation_cell
	WHERE label in ('25kmE4650N2925', '25kmE4650N2950')) AS t1
;


