-- Copyright 2017, 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.

/*
tests of API functions for the GUI congiguration of ratios involving GREG-map total estimates
*/

-------------------------------------------------------------------------------------------------------------------------------------------------------
-- tests for fn_api_get_gregmap_configs4ratio(_estimation_period INT, _estimation_cells INT[], _variables_nom INT[], _variables_denom INT[])
-------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------- testing invalid inputs ------------------------------------------------------------------------------
-- test NULL for _estimation_period argument
SELECT * FROM nfiesta.fn_api_get_gregmap_configs4ratio(NULL, ARRAY[100], ARRAY[1,2,3], ARRAY[3,3,3]);

-- test NULL for _estimation_cell argument
SELECT * FROM nfiesta.fn_api_get_gregmap_configs4ratio(1, NULL, ARRAY[1,2,3], ARRAY[3,3,3]);

-- test NULL for _variables_nom argument
SELECT * FROM nfiesta.fn_api_get_gregmap_configs4ratio(1, ARRAY[100], NULL, ARRAY[3,3,3]);

-- test NULL for _variables_denom argument
SELECT * FROM nfiesta.fn_api_get_gregmap_configs4ratio(1, ARRAY[100], ARRAY[1,2,3], NULL);

-- test for NULL as an element of _estimation_cell argument
SELECT * FROM nfiesta.fn_api_get_gregmap_configs4ratio(1, ARRAY[100, NULL], ARRAY[1,2,3], ARRAY[3,3,3]);
SELECT * FROM nfiesta.fn_api_get_gregmap_configs4ratio(1, ARRAY[NULL]::int[], ARRAY[1,2,3], ARRAY[3,3,3]);

-- test for NULL as an element of _variables_nom argument
SELECT * FROM nfiesta.fn_api_get_gregmap_configs4ratio(1, ARRAY[100], ARRAY[1,NULL,3], ARRAY[3,3,3]);
SELECT * FROM nfiesta.fn_api_get_gregmap_configs4ratio(1, ARRAY[100], ARRAY[NULL]::int[], ARRAY[3,3,3]);

-- test for NULL as an element of _variables_denom argument
SELECT * FROM nfiesta.fn_api_get_gregmap_configs4ratio(1, ARRAY[100], ARRAY[1,2,3], ARRAY[3,3,NULL]);
SELECT * FROM nfiesta.fn_api_get_gregmap_configs4ratio(1, ARRAY[100], ARRAY[1,2,3]::int[], ARRAY[NULL]::int[]);

-- test if estimation cells belong to one estimation cell collection (adding 1 to the valid array _estimation_cells, 1 
-- belongs to another collection but the same stratum
SELECT * FROM nfiesta.fn_api_get_gregmap_configs4ratio(1, ARRAY[1,44,45,46,47,48,49,50,51,52,53,56,57,58,62], ARRAY[1,2,3], ARRAY[3,3,3]);

-- test if _variables_nom and _variables_denom have equal lengths
SELECT FROM nfiesta.fn_api_get_gregmap_configs4ratio(1, ARRAY[100], ARRAY[1,2,3], ARRAY[3,3]);

------------------------------------------------ testing valid inputs ---------------------------------------------------------------------------------
-- two complete configs, one force true, the other false, INSPIRE 50x50 km param area type
SELECT * FROM nfiesta.fn_api_get_gregmap_configs4ratio(1, ARRAY[55,57,69], ARRAY[1,2,3], ARRAY[3,3,3]);

-- two complete configs, one force true, the other false, two param area types: INSPIRE 50x50 km and strata set 
SELECT * FROM nfiesta.fn_api_get_gregmap_configs4ratio(1, ARRAY[100], ARRAY[1,2,3], ARRAY[3,3,3]);

-------------------------------------------------------------------------------------------------------------------------------------------------------
-- tests for fn_api_get_single2gregmap_configs4ratio(_estimation_period INT, _estimation_cells INT[], _variables_nom INT[], _variables_denom INT[])
-------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------- testing invalid inputs ------------------------------------------------------------------------------
-- test NULL for _estimation_period argument
SELECT * FROM nfiesta.fn_api_get_single2gregmap_configs4ratio(NULL, ARRAY[100], ARRAY[1,2,3], ARRAY[3,3,3]);

-- test NULL for _estimation_cell argument
SELECT * FROM nfiesta.fn_api_get_single2gregmap_configs4ratio(1, NULL, ARRAY[1,2,3], ARRAY[3,3,3]);

-- test NULL for _variables_nom argument
SELECT * FROM nfiesta.fn_api_get_single2gregmap_configs4ratio(1, ARRAY[100], NULL, ARRAY[3,3,3]);

-- test NULL for _variables_denom argument
SELECT * FROM nfiesta.fn_api_get_single2gregmap_configs4ratio(1, ARRAY[100], ARRAY[1,2,3], NULL);

-- test for NULL as an element of _estimation_cell argument
SELECT * FROM nfiesta.fn_api_get_single2gregmap_configs4ratio(1, ARRAY[100, NULL], ARRAY[1,2,3], ARRAY[3,3,3]);
SELECT * FROM nfiesta.fn_api_get_single2gregmap_configs4ratio(1, ARRAY[NULL]::int[], ARRAY[1,2,3], ARRAY[3,3,3]);

-- test for NULL as an element of _variables_nom argument
SELECT * FROM nfiesta.fn_api_get_single2gregmap_configs4ratio(1, ARRAY[100], ARRAY[1,NULL,3], ARRAY[3,3,3]);
SELECT * FROM nfiesta.fn_api_get_single2gregmap_configs4ratio(1, ARRAY[100], ARRAY[NULL]::int[], ARRAY[3,3,3]);

-- test for NULL as an element of _variables_denom argument
SELECT * FROM nfiesta.fn_api_get_single2gregmap_configs4ratio(1, ARRAY[100], ARRAY[1,2,3], ARRAY[3,3,NULL]);
SELECT * FROM nfiesta.fn_api_get_single2gregmap_configs4ratio(1, ARRAY[100], ARRAY[1,2,3]::int[], ARRAY[NULL]::int[]);

-- test if estimation cells belong to one estimation cell collection (adding 1 to the valid array _estimation_cells, 1 
-- belongs to another collection but the same stratum
SELECT * FROM nfiesta.fn_api_get_single2gregmap_configs4ratio(1, ARRAY[1,44,45,46,47,48,49,50,51,52,53,56,57,58,62], ARRAY[1,2,3], ARRAY[3,3,3]);

-- test if _variables_nom and _variables_denom have equal lengths
SELECT FROM nfiesta.fn_api_get_single2gregmap_configs4ratio(1, ARRAY[100], ARRAY[1,2,3], ARRAY[3,3]);

------------------------------------------------ testing valid inputs ---------------------------------------------------------------------------------
-- one uncomplete config, INSPIRE 50x50 km param area type, common panel refyearset group (19) exists for cells 55 and 69, but not including cell 57
SELECT * FROM nfiesta.fn_api_get_single2gregmap_configs4ratio(1, ARRAY[55,57,69], ARRAY[1,2,3], ARRAY[3,3,3]);

-- one complete config is obtained if cell 57 is dropped from the above input
SELECT * FROM nfiesta.fn_api_get_single2gregmap_configs4ratio(1, ARRAY[55,69], ARRAY[1,2,3], ARRAY[3,3,3]);

-- two complete configs, two param area types: INSPIRE 50x50 km and the whole strata set 
SELECT * FROM nfiesta.fn_api_get_single2gregmap_configs4ratio(1, ARRAY[100], ARRAY[1,2,3], ARRAY[3,3,3]);

-------------------------------------------------------------------------------------------------------------------------------------------------------
-- tests forfn_api_get_gregmap2single_configs4ratio(_estimation_period INT, _estimation_cells INT[], _variables_nom INT[], _variables_denom INT[])
-------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------- testing invalid inputs ------------------------------------------------------------------------------
-- test NULL for _estimation_period argument
SELECT * FROM nfiesta.fn_api_get_gregmap2single_configs4ratio(NULL, ARRAY[100], ARRAY[1,2,3], ARRAY[3,3,3]);

-- test NULL for _estimation_cell argument
SELECT * FROM nfiesta.fn_api_get_gregmap2single_configs4ratio(1, NULL, ARRAY[1,2,3], ARRAY[3,3,3]);

-- test NULL for _variables_nom argument
SELECT * FROM nfiesta.fn_api_get_gregmap2single_configs4ratio(1, ARRAY[100], NULL, ARRAY[3,3,3]);

-- test NULL for _variables_denom argument
SELECT * FROM nfiesta.fn_api_get_gregmap2single_configs4ratio(1, ARRAY[100], ARRAY[1,2,3], NULL);

-- test for NULL as an element of _estimation_cell argument
SELECT * FROM nfiesta.fn_api_get_gregmap2single_configs4ratio(1, ARRAY[100, NULL], ARRAY[1,2,3], ARRAY[3,3,3]);
SELECT * FROM nfiesta.fn_api_get_gregmap2single_configs4ratio(1, ARRAY[NULL]::int[], ARRAY[1,2,3], ARRAY[3,3,3]);

-- test for NULL as an element of _variables_nom argument
SELECT * FROM nfiesta.fn_api_get_gregmap2single_configs4ratio(1, ARRAY[100], ARRAY[1,NULL,3], ARRAY[3,3,3]);
SELECT * FROM nfiesta.fn_api_get_gregmap2single_configs4ratio(1, ARRAY[100], ARRAY[NULL]::int[], ARRAY[3,3,3]);

-- test for NULL as an element of _variables_denom argument
SELECT * FROM nfiesta.fn_api_get_gregmap2single_configs4ratio(1, ARRAY[100], ARRAY[1,2,3], ARRAY[3,3,NULL]);
SELECT * FROM nfiesta.fn_api_get_gregmap2single_configs4ratio(1, ARRAY[100], ARRAY[1,2,3]::int[], ARRAY[NULL]::int[]);

-- test if _variables_nom and _variables_denom have equal lengths
SELECT FROM nfiesta.fn_api_get_gregmap2single_configs4ratio(1, ARRAY[100], ARRAY[1,2,3], ARRAY[3,3]);

------------------------------------------------ testing valid inputs ---------------------------------------------------------------------------------
-- one uncomplete config, INSPIRE 50x50 km param area type, common panel refyearset group (19) exists for cells 55 and 69, but not including cell 57
-- the other uncomplete config for using strata set as param area
SELECT * FROM nfiesta.fn_api_get_gregmap2single_configs4ratio(1, ARRAY[55,57,69], ARRAY[1,2,3], ARRAY[3,3,3]);

-- one complete and one uncomplete config is obtained if cell 57 is dropped from the above input
SELECT * FROM nfiesta.fn_api_get_gregmap2single_configs4ratio(1, ARRAY[55,69], ARRAY[1,2,3], ARRAY[3,3,3]);

-- two complete configs, two param area types: INSPIRE 50x50 km and the whole strata set 
SELECT * FROM nfiesta.fn_api_get_gregmap2single_configs4ratio(1, ARRAY[100], ARRAY[1,2,3], ARRAY[3,3,3]);

-------------------------------------------------------------------------------------------------------------------------------------------------------
-- tests for function nfiesta.fn_api_save_ratio_config(_total_estimate_conf_nominator INT, _total_estimate_conf_denominator INT)
-------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------- testing invalid inputs ------------------------------------------------------------------------------
-- test NULL for _total_estimate_conf_nominator
SELECT * FROM nfiesta.fn_api_save_ratio_config(NULL,1);

-- test NULL for _total_estimate_conf_denominator
SELECT * FROM nfiesta.fn_api_save_ratio_config(1,NULL);

-- test for non existing _total_estimate_conf_nominator (among records of t_total_estimate_conf)
SELECT * FROM nfiesta.fn_api_save_ratio_config(-1,1);

-- test for non existing _total_estimate_conf_denominator (among records of t_total_estimate_conf)
SELECT * FROM nfiesta.fn_api_save_ratio_config(1,-1);

-- trying to configure a ratio of totals, with different sets of panels (panel_refyearset_groups 14 and 15)
SELECT * FROM nfiesta.fn_api_save_ratio_config(1,5);

------------------------------------------------ testing valid inputs ---------------------------------------------------------------------------------
-- save a configuration of a ratio of forest to non-forest within the same cell and using the same panel_refyearset_group and return new config id
SELECT * FROM nfiesta.fn_api_save_ratio_config(1,2);

-- skip already existing configuration and return NULL
SELECT * FROM nfiesta.fn_api_save_ratio_config(1,2);
