-- get all panel refyearset groups
SELECT * FROM nfiesta.fn_api_get_list_of_panel_refyearset_groups();

-- check db dependencies
SELECT * FROM nfiesta.fn_api_before_delete_panel_refyearset_group(NULL);
SELECT * FROM nfiesta.fn_api_before_delete_panel_refyearset_group(-20);
SELECT * FROM nfiesta.fn_api_before_delete_panel_refyearset_group(1);
SELECT * FROM nfiesta.fn_api_before_delete_panel_refyearset_group(26);

-- delete selected panel refyearset group
SELECT * FROM nfiesta.fn_api_delete_panel_refyearset_group(NULL);
SELECT * FROM nfiesta.fn_api_delete_panel_refyearset_group(-20);
SELECT * FROM nfiesta.fn_api_delete_panel_refyearset_group(26);

-- update selected panel refyearset group
SELECT * FROM nfiesta.fn_api_update_panel_refyearset_group(NULL,NULL,NULL,NULL,NULL);
SELECT * FROM nfiesta.fn_api_update_panel_refyearset_group(10,NULL,NULL,NULL,NULL);
SELECT * FROM nfiesta.fn_api_update_panel_refyearset_group(1,'no_of_panels: 1;{1}','no_of_panels: 1;{1}','new_label_en','new_description_en');
SELECT * FROM nfiesta.fn_api_update_panel_refyearset_group(-20,'new_label','new_description','new_label_en','new_description_en');
SELECT * FROM nfiesta.fn_api_update_panel_refyearset_group(25,'new_label_33','new_description_33','new_label_en_33','new_description_en_33');