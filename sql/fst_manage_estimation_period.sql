---------------------------------
-- get all estimation periods
---------------------------------
SELECT * FROM nfiesta.fn_api_get_list_of_estimation_periods();

---------------------------------
-- check DB dependencies
---------------------------------
-- testing false inputs
-----------------------
-- passing NULL for _id
SELECT * FROM nfiesta.fn_api_before_delete_estimation_period(NULL);
-- non-existing group, no records
SELECT * FROM nfiesta.fn_api_before_delete_estimation_period(-20);
-- testing valid inputs
-----------------------
-- estimation period 1
SELECT * FROM nfiesta.fn_api_before_delete_estimation_period(1);

------------------------------------------
-- delete selected estimation period
------------------------------------------
-- testing false inputs
-----------------------
-- passing NULL for _id
SELECT * FROM nfiesta.fn_api_delete_estimation_period(NULL);
-- non-existing group, no records
SELECT * FROM nfiesta.fn_api_delete_estimation_period(-20);
-- testing valid inputs
-----------------------
SELECT * FROM nfiesta.fn_api_delete_estimation_period(1);

------------------------------------------
-- update selected estimation period
------------------------------------------
-- testing false inputs
-----------------------
-- passing NULL for arguments
SELECT * FROM nfiesta.fn_api_update_estimation_period(NULL,NULL,NULL,NULL,NULL,NULL);
SELECT * FROM nfiesta.fn_api_update_estimation_period(10,NULL,NULL,NULL,NULL,NULL);
-- non-existing group, no records
SELECT * FROM nfiesta.fn_api_update_estimation_period(-20,'new_label','new_description','new_label_en','new_description_en',false);
-- testing valid inputs
-----------------------
-- existing _label and _description of the same id as input _id
SELECT * FROM nfiesta.fn_api_update_estimation_period(1,'NFI2','NFI2','new_label_en','new_description_en',true);
-- group 1
SELECT * FROM nfiesta.fn_api_update_estimation_period(1,'new_label_1','new_description_1','new_label_en_1','new_description_en_1',true);

----------------------------------------------
-- check existing labels and descriptions
----------------------------------------------
-- testing false inputs
-----------------------
-- passing NULL for _label
SELECT * FROM nfiesta.fn_api_do_labels_and_descriptions_of_estimation_period_exist(
	NULL, 'new_label_en_1', 'new_description_1', 'new_description_en_1');
-- passing NULL for _label_en
SELECT * FROM nfiesta.fn_api_do_labels_and_descriptions_of_estimation_period_exist(
	'new_label_1', NULL, 'new_description_1', 'new_description_en_1');
-- passing NULL for _description
SELECT * FROM nfiesta.fn_api_do_labels_and_descriptions_of_estimation_period_exist(
	'new_label_1', 'new_label_en_1', NULL, 'new_description_en_1');
-- passing NULL for _description
SELECT * FROM nfiesta.fn_api_do_labels_and_descriptions_of_estimation_period_exist(
	'new_label_1', 'new_label_en_1', 'new_description_1', NULL);
-- testing valid inputs
-----------------------
-- all found, all TRUE
SELECT * FROM nfiesta.fn_api_do_labels_and_descriptions_of_estimation_period_exist(
	'new_label_1', 'new_label_en_1', 'new_description_1', 'new_description_en_1');
-- first found (TRUE), the rest FALSE
SELECT * FROM nfiesta.fn_api_do_labels_and_descriptions_of_estimation_period_exist(
	'new_label_1', 'no_label_en', 'no_description', 'no_description_en');

------------------------------------------
-- save new estimation period
------------------------------------------
-- testing invalid inputs 
-------------------------
-- test NULL for _estimate_date_begin argument
SELECT * FROM nfiesta.fn_api_save_estimation_period(NULL, '2015-12-31', 'nejaky label', 'nejaky description', 'some label en', 'some description_en', NULL);
-- test NULL for _estimate_date_end argument
SELECT * FROM nfiesta.fn_api_save_estimation_period('2011-01-01', NULL, 'nejaky label', 'nejaky description', 'some label en', 'some description_en', NULL);
-- passing NULL for _label
SELECT * FROM nfiesta.fn_api_save_estimation_period('2011-01-01', '2015-12-31', NULL, 'nejaky description', 'some label en', 'some description_en', NULL);
-- passing NULL for _description
SELECT * FROM nfiesta.fn_api_save_estimation_period('2011-01-01', '2015-12-31', 'nejaky label', NULL, 'some label en', 'some description_en', NULL);
-- passing NULL for _label_en
SELECT * FROM nfiesta.fn_api_save_estimation_period('2011-01-01', '2015-12-31', 'nejaky label', 'nejaky description', NULL, 'some description_en', NULL);
-- passing NULL for _description_en
SELECT * FROM nfiesta.fn_api_save_estimation_period('2011-01-01', '2015-12-31', 'nejaky label', 'nejaky description', 'some label en', NULL, NULL);
-- test if saving a estimation period with begin date and end date already used 
SELECT * FROM nfiesta.fn_api_save_estimation_period('2011-01-01', '2015-12-31', 'nejaky label', 'nejaky description', 'some label en', 'some description_en', false);
-- testing valid inputs
-----------------------
SELECT * FROM nfiesta.fn_api_save_estimation_period('2016-01-01', '2020-12-31', 'NIL3', '3. cyklus NIL', 'NFI3', '3rd cycle of czech NFI', true);