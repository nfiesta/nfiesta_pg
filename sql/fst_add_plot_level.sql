-------------------------------------------------------------------------PREPARE TEST HIERARCHIES--------------------------------------------------------

alter table nfiesta.t_target_data disable trigger trg__target_data__pr_upd;
alter table nfiesta.t_target_data disable trigger trg__target_data__ch_upd;

alter table nfiesta.t_target_data disable trigger trg__target_data__del;
alter table nfiesta.t_target_data disable trigger trg__target_data__ins;
alter table nfiesta.t_target_data disable trigger trg__target_data__upd;
alter table nfiesta.t_auxiliary_data disable trigger trg__auxiliary_data__del;
alter table nfiesta.t_auxiliary_data disable trigger trg__auxiliary_data__ins;
alter table nfiesta.t_auxiliary_data disable trigger trg__auxiliary_data__upd;

alter table nfiesta.t_available_datasets disable trigger trg__t_available_datasets__refresh_t_additivity_set_plot__del;
alter table nfiesta.t_available_datasets disable trigger trg__t_available_datasets__refresh_t_additivity_set_plot__ins;
alter table nfiesta.t_available_datasets disable trigger trg__t_available_datasets__refresh_t_additivity_set_plot__upd;
alter table nfiesta.t_variable_hierarchy disable trigger trg__t_variable_hierarchy__refresh_t_additivity_set_plot__del;
alter table nfiesta.t_variable_hierarchy disable trigger trg__t_variable_hierarchy__refresh_t_additivity_set_plot__ins;
alter table nfiesta.t_variable_hierarchy disable trigger trg__t_variable_hierarchy__refresh_t_additivity_set_plot__upd;

-------------------------------------------------------------------------TARGET LOCAL DENSITY

with w_fnres as (
	select
        	nfiesta.fn_add_plot_target_attr(node, edges, panel, reference_year_set) as res
	from
        	(select distinct panel, reference_year_set, node, edges from nfiesta.v_variable_hierarchy_plot_deprecated) as foo
)
select plot, reference_year_set,
	variable, round(ldsity::numeric, 10) as ldsity,
	variables_def, variables_found, round(ldsity_sum::numeric, 10) as ldsity_sum,
	round(diff::numeric, 10) as diff
from (select (res).* from w_fnres) as foo
where diff > 0.0001 or diff is null
order by plot, reference_year_set, variable;

-------------------------------------------------PLOT 8994
with w_data as (
	select t_target_data.id, t_target_data.plot, t_available_datasets.reference_year_set, t_available_datasets.variable
	from nfiesta.t_target_data
	inner join nfiesta.t_available_datasets on (t_target_data.available_datasets = t_available_datasets.id)
	where plot = 8994 and reference_year_set = 3 and variable = 1
)
update nfiesta.t_target_data SET value = 1.13
from w_data where t_target_data.id = w_data.id;

with w_fnres as (
	select
        	nfiesta.fn_add_plot_target_attr(node, edges, panel, reference_year_set) as res
	from
        	(select distinct panel, reference_year_set, node, edges from nfiesta.v_variable_hierarchy_plot_deprecated) as foo
)
select plot, reference_year_set,
	variable, round(ldsity::numeric, 10) as ldsity,
	variables_def, variables_found, round(ldsity_sum::numeric, 10) as ldsity_sum,
	round(diff::numeric, 10) as diff
from (select (res).* from w_fnres) as foo
where diff > 0.0001 or diff is null
order by plot, reference_year_set, variable;

insert into nfiesta.c_area_domain values (2, 'land owner', 'Land owner.', null::varchar, null::text, true);
insert into nfiesta.c_area_domain values (3, 'land registry & land owner', 'Combination of: Land use indicated by land registry. & Land owner.', null::varchar, null::text, true);

insert into nfiesta.c_area_domain_category values (3, 3, 'forest & LČR', 'Combination of: Forest area indicated by land registry. (CZ. PUPFL). & Owned by LČR.');
insert into nfiesta.c_area_domain_category values (4, 3, 'forest & non-LČR', 'Combination of: Forest area indicated by land registry. (CZ. PUPFL). & Owned by non-LČR.');
insert into nfiesta.t_variable values (7,1,NULL,3,NULL),	(8,1,NULL,4,NULL);
insert into nfiesta.t_variable_hierarchy values (3,7,1),(4,8,1);

insert into nfiesta.c_area_domain_category values (5, 3, 'non-forest & LČR', 'Combination of: Non-forest area indicated by land registry (CZ. non-PUPFL). & Owned by LČR.');
insert into nfiesta.c_area_domain_category values (6, 3, 'non-forest & non-LČR', 'Combination of: Non-forest area indicated by land registry (CZ. non-PUPFL). & Owned by non-LČR.');
insert into nfiesta.t_variable values (9,1,NULL,5,NULL),	(10,1,NULL,6,NULL);
insert into nfiesta.t_variable_hierarchy values (5,9,2),(6,10,2);

insert into nfiesta.c_area_domain_category values (7, 2, 'LČR', 'Owned by LČR.');
insert into nfiesta.c_area_domain_category values (8, 2, 'non-LČR', 'Owned by non-LČR.');
insert into nfiesta.t_variable values (11,1,NULL,7,NULL),	(12,1,NULL,8,NULL);
insert into nfiesta.t_variable_hierarchy values (7,11,3),(8,12,3);

insert into nfiesta.t_available_datasets (panel, reference_year_set, variable, ldsity_threshold, last_change) values (3, 3, 7, 0.0000000001, now()), (3, 3, 8, 0.0000000001, now());
insert into nfiesta.t_available_datasets (panel, reference_year_set, variable, ldsity_threshold, last_change) values (3, 3, 9, 0.0000000001, now()), (3, 3, 10, 0.0000000001, now());
insert into nfiesta.t_available_datasets (panel, reference_year_set, variable, ldsity_threshold, last_change) values (3, 3, 11, 0.0000000001, now()), (3, 3, 12, 0.0000000001, now());

with w_new_noads as (
        select
                gid, t_panel.id as panel, cm_refyearset2panel_mapping.reference_year_set, t_variable.variable, t_variable.variable_sup
        from sdesign.f_p_plot
        inner join sdesign.t_cluster ON t_cluster.id = f_p_plot.cluster
        inner join sdesign.cm_cluster2panel_mapping ON cm_cluster2panel_mapping.cluster = t_cluster.id
        inner join sdesign.t_panel ON t_panel.id = cm_cluster2panel_mapping.panel
        inner join sdesign.cm_refyearset2panel_mapping ON cm_refyearset2panel_mapping.panel = t_panel.id
        , (values (7, 1), (8, 1), (9, 2), (10, 2), (11, 3), (12, 3)) AS t_variable (variable, variable_sup)
        where t_panel.id = 3 and cm_refyearset2panel_mapping.reference_year_set = 3
)
, w_new as (
        select w_new_noads.* , t_available_datasets.id as available_datasets, t_available_datasets_sup.id as available_datasets_sup
        from w_new_noads
        inner join nfiesta.t_available_datasets on (       w_new_noads.panel               = t_available_datasets.panel and
                                                                w_new_noads.reference_year_set  = t_available_datasets.reference_year_set and
                                                                w_new_noads.variable            = t_available_datasets.variable)
        inner join nfiesta.t_available_datasets as t_available_datasets_sup on (
                                                                w_new_noads.panel               = t_available_datasets_sup.panel and
                                                                w_new_noads.reference_year_set  = t_available_datasets_sup.reference_year_set and
                                                                w_new_noads.variable_sup        = t_available_datasets_sup.variable)
)
insert into nfiesta.t_target_data (plot, available_datasets, value)
select
		w_new.gid, w_new.available_datasets,
		coalesce(t_target_data.value, 0) / 2 as value
from w_new
left join nfiesta.t_target_data on (t_target_data.plot = w_new.gid and t_target_data.available_datasets = w_new.available_datasets_sup and t_target_data.is_latest = false)
;

--delete from nfiesta.t_target_data where value = 0;


-------------------------------------------------------------------------AUXILIARY LOCAL DENSITY

with w_fnres as (
	select
        	nfiesta.fn_add_plot_aux_attr(node, edges, panel) as res
	from
        	(select distinct panel, node, edges from nfiesta.v_variable_hierarchy_plot_aux_deprecated) as foo
)
select plot,
	variable, round(ldsity::numeric, 10) as ldsity,
	variables_def, variables_found, round(ldsity_sum::numeric, 10) as ldsity_sum,
	round(diff::numeric, 10) as diff
from (select (res).* from w_fnres) as foo
where diff > 0.0001 or diff is null
order by plot, variable;

insert into nfiesta.c_auxiliary_variable values (2, 'f-a-z', 'forest altitude zone');
insert into nfiesta.c_auxiliary_variable_category values (3, 2, 'forest-low', 'forest in lowlands');
insert into nfiesta.c_auxiliary_variable_category values (4, 2, 'forest-hi', 'forest in mountains');
insert into nfiesta.c_auxiliary_variable_category values (5, 2, 'forest-all', 'without distinction');
insert into nfiesta.t_variable values (13,NULL,NULL,NULL,3), (14,NULL,NULL,NULL,4), (15,NULL,NULL,NULL,5);
insert into nfiesta.t_variable_hierarchy values (9, 13, 5),(10, 14, 5); -- compare against category from different variable
insert into nfiesta.t_variable_hierarchy values (11, 13, 15),(12, 14, 15); -- compare against explicit category "without distinction"
insert into nfiesta.t_available_datasets (panel, reference_year_set, variable, ldsity_threshold, last_change) values (3, NULL, 13, 0.0000000001, now()), (3, NULL, 14, 0.0000000001, now()), (3, NULL, 15, 0.0000000001, now());

with w_new_noads as (
	select
		gid, t_panel.id as panel, t_variable.variable
	from sdesign.f_p_plot
	inner join sdesign.t_cluster ON t_cluster.id = f_p_plot.cluster
	inner join sdesign.cm_cluster2panel_mapping ON cm_cluster2panel_mapping.cluster = t_cluster.id
	inner join sdesign.t_panel ON t_panel.id = cm_cluster2panel_mapping.panel
	, (values (13), (14), (15)) AS t_variable (variable)
	where t_panel.id = 3
)
, w_new as (
        select 
		w_new_noads.gid, w_new_noads.panel, w_new_noads.variable, 
		t_available_datasets.id as available_datasets
        from w_new_noads
        inner join nfiesta.t_available_datasets on (       w_new_noads.panel               = t_available_datasets.panel and
                                                                w_new_noads.variable            = t_available_datasets.variable)
)
insert into nfiesta.t_auxiliary_data (plot, available_datasets, value)
select
	w_new.gid, w_new.available_datasets, 
	case when w_new.variable = 15 then coalesce(t_auxiliary_data.value, 0)
	else coalesce(t_auxiliary_data.value, 0) / 2 end as value
from w_new
left join nfiesta.t_auxiliary_data on (t_auxiliary_data.plot = w_new.gid and t_auxiliary_data.available_datasets = (select id from nfiesta.t_available_datasets where panel = w_new.panel and variable = 5))
;

delete from nfiesta.t_auxiliary_data where value = 0;

-------------------------------------------------PLOT 8994

with w_data as (
	select t_auxiliary_data.id, t_auxiliary_data.plot, t_available_datasets.variable
	from nfiesta.t_auxiliary_data
	inner join nfiesta.t_available_datasets on (t_auxiliary_data.available_datasets = t_available_datasets.id)
	where plot = 8994 and variable = 13
)
update nfiesta.t_auxiliary_data SET value = 0.7
from w_data where t_auxiliary_data.id = w_data.id;

with w_data as (
	select t_auxiliary_data.id, t_auxiliary_data.plot, t_available_datasets.variable
	from nfiesta.t_auxiliary_data
	inner join nfiesta.t_available_datasets on (t_auxiliary_data.available_datasets = t_available_datasets.id)
	where plot = 8994 and variable = 14
)
update nfiesta.t_auxiliary_data SET value = 0.31
from w_data where t_auxiliary_data.id = w_data.id;

with w_data as (
	select t_auxiliary_data.id, t_auxiliary_data.plot, t_available_datasets.variable
	from nfiesta.t_auxiliary_data
	inner join nfiesta.t_available_datasets on (t_auxiliary_data.available_datasets = t_available_datasets.id)
	where plot = 8994 and variable = 15
)
update nfiesta.t_auxiliary_data SET value = 1.1
from w_data where t_auxiliary_data.id = w_data.id;

with w_fnres as (
	select
        	nfiesta.fn_add_plot_aux_attr(node, edges, panel) as res
	from
        	(select distinct panel, node, edges from nfiesta.v_variable_hierarchy_plot_aux_deprecated) as foo
)
select plot,
	variable, round(ldsity::numeric, 10) as ldsity,
	variables_def, variables_found, round(ldsity_sum::numeric, 10) as ldsity_sum,
	round(diff::numeric, 10) as diff
from (select (res).* from w_fnres) as foo
where diff > 0.0001 or diff is null
order by plot, variable;

alter table nfiesta.t_target_data enable trigger trg__target_data__pr_upd;
alter table nfiesta.t_target_data enable trigger trg__target_data__ch_upd;

alter table nfiesta.t_target_data enable trigger trg__target_data__del;
alter table nfiesta.t_target_data enable trigger trg__target_data__ins;
alter table nfiesta.t_target_data enable trigger trg__target_data__upd;
alter table nfiesta.t_auxiliary_data enable trigger trg__auxiliary_data__del;
alter table nfiesta.t_auxiliary_data enable trigger trg__auxiliary_data__ins;
alter table nfiesta.t_auxiliary_data enable trigger trg__auxiliary_data__upd;

alter table nfiesta.t_available_datasets enable trigger trg__t_available_datasets__refresh_t_additivity_set_plot__del;
alter table nfiesta.t_available_datasets enable trigger trg__t_available_datasets__refresh_t_additivity_set_plot__ins;
alter table nfiesta.t_available_datasets enable trigger trg__t_available_datasets__refresh_t_additivity_set_plot__upd;
alter table nfiesta.t_variable_hierarchy enable trigger trg__t_variable_hierarchy__refresh_t_additivity_set_plot__del;
alter table nfiesta.t_variable_hierarchy enable trigger trg__t_variable_hierarchy__refresh_t_additivity_set_plot__ins;
alter table nfiesta.t_variable_hierarchy enable trigger trg__t_variable_hierarchy__refresh_t_additivity_set_plot__upd;

-------------------------------------------------------------------------TEST DATA MANIPULATION--------------------------------------------------------
-- not allowed only one record from available dataset (for plot)
delete from nfiesta.t_target_data where plot = 486 and is_latest = false;

-- additivity fail when delete
delete from nfiesta.t_target_data where plot = 5 and available_datasets = 3;

update nfiesta.t_target_data SET value = 13 where (plot = 5 and available_datasets = 4) or (plot = 5 and available_datasets = 1);

alter table nfiesta.t_target_data disable trigger trg__target_data__pr_upd;
update nfiesta.t_target_data SET value = 13 where plot = 6 and available_datasets in (1, 3);
alter table nfiesta.t_target_data enable trigger trg__target_data__pr_upd;

update nfiesta.t_target_data set is_latest=false 
where plot = 8155 
and available_datasets in (select id from nfiesta.t_available_datasets where panel = 3 and reference_year_set = 3 and variable = 8);

----------------

alter table nfiesta.t_target_data disable trigger trg__target_data__del;
delete from nfiesta.t_target_data
where plot = 8155 
and available_datasets in (select id from nfiesta.t_available_datasets where panel = 3 and reference_year_set = 3 and variable in (7, 8));
alter table nfiesta.t_target_data enable trigger trg__target_data__del;

insert into nfiesta.t_target_data (plot, value, available_datasets, value_inserted, is_latest) 
values (8155, 0.5, 36, now(), true), 
(8155, 0.51, 35, now(), true);

insert into nfiesta.t_target_data (plot, value, available_datasets, value_inserted, is_latest) 
values (8155, 0.5, 36, now(), true), 
(8155, 0.5, 35, now(), true);

---

alter table nfiesta.t_auxiliary_data disable trigger trg__auxiliary_data__del;
delete from nfiesta.t_auxiliary_data
where plot = 8995 
and available_datasets in (select id from nfiesta.t_available_datasets where panel = 3 and reference_year_set is null and variable in (13, 14));
alter table nfiesta.t_auxiliary_data enable trigger trg__auxiliary_data__del;

insert into nfiesta.t_auxiliary_data (plot, value, available_datasets, value_inserted, is_latest) 
values (8995, 0.7, 41, now(), true), 
(8995, 0.31, 42, now(), true);

alter table nfiesta.t_auxiliary_data disable trigger trg__auxiliary_data__ins;
insert into nfiesta.t_auxiliary_data (plot, value, available_datasets, value_inserted, is_latest) 
values (8995, 0.5, 41, now(), true), 
(8995, 0.5, 42, now(), true);
alter table nfiesta.t_auxiliary_data enable trigger trg__auxiliary_data__ins;

----------------

with w_plot_data as (
        select t_target_data.id, t_target_data.plot, t_target_data.value, t_target_data.available_datasets, t_target_data.value_inserted, t_target_data.is_latest
        from nfiesta.t_target_data
	join nfiesta.t_available_datasets on (t_target_data.available_datasets = t_available_datasets.id)
        where t_target_data.plot = 8155 and t_available_datasets.reference_year_set = 3 and t_available_datasets.variable in (7,8)
        order by t_available_datasets.variable
)
, w_plot_upd as (
        update nfiesta.t_target_data set is_latest=false 
	from w_plot_data 
	where t_target_data.id = w_plot_data.id 
	returning t_target_data.plot, t_target_data.available_datasets
)
insert into nfiesta.t_target_data (plot, value, available_datasets, value_inserted, is_latest)
select plot, 0.5, available_datasets, now(), true from w_plot_upd;


----------------additivity broken by introduction of all-zero local density (no insert in t_target_data) in t_available_dataset

--select * from nfiesta.c_area_domain_category;
insert into nfiesta.c_area_domain_category (id, area_domain, label, description) 
values (9, 3,'non-forest & non-LČR & TEST A','non-forest & non-LČR & TEST A'),
(10, 3,'non-forest & non-LČR & TEST B','non-forest & non-LČR & TEST B');

--select * from nfiesta.t_variable;
insert into nfiesta.t_variable (id, target_variable, area_domain_category) 
values 
(16, 1,9),
(17, 1,10);


--select * from nfiesta.t_variable_hierarchy;
insert into nfiesta.t_variable_hierarchy (id,variable,variable_superior) 
values
(13, 16, 10),
(14, 17, 10);

--select * from nfiesta.t_available_datasets order by variable;
insert into nfiesta.t_available_datasets (panel, reference_year_set, variable, ldsity_threshold, last_change) 
values (3,3,16, 0.0000000001, now());

-------------------------------------------------PLOT 9120
alter table nfiesta.t_target_data disable trigger trg__target_data__pr_upd;
alter table nfiesta.t_target_data disable trigger trg__target_data__ch_upd;
alter table nfiesta.t_target_data disable trigger trg__target_data__upd;

with w_data as (
	select t_target_data.id, t_target_data.plot, t_available_datasets.reference_year_set, t_available_datasets.variable
	from nfiesta.t_target_data
	inner join nfiesta.t_available_datasets on (t_target_data.available_datasets = t_available_datasets.id)
	where plot = 9120 and reference_year_set = 3 and variable = 7
)
update nfiesta.t_target_data SET value = 0.8
from w_data where t_target_data.id = w_data.id;

with w_data as (
	select t_target_data.id, t_target_data.plot, t_available_datasets.reference_year_set, t_available_datasets.variable
	from nfiesta.t_target_data
	inner join nfiesta.t_available_datasets on (t_target_data.available_datasets = t_available_datasets.id)
	where plot = 9120 and reference_year_set = 3 and variable = 8
)
update nfiesta.t_target_data SET value = 0.19
from w_data where t_target_data.id = w_data.id;

alter table nfiesta.t_target_data enable trigger trg__target_data__pr_upd;
alter table nfiesta.t_target_data enable trigger trg__target_data__ch_upd;
alter table nfiesta.t_target_data enable trigger trg__target_data__upd;

with w_fnres as (
	select
        	nfiesta.fn_add_plot_target_attr(node, edges, panel, reference_year_set) as res
	from
        	(select distinct panel, reference_year_set, node, edges from nfiesta.v_variable_hierarchy_plot_deprecated) as foo
)
select plot, reference_year_set,
	variable, round(ldsity::numeric, 10) as ldsity,
	variables_def, variables_found, round(ldsity_sum::numeric, 10) as ldsity_sum,
	round(diff::numeric, 10) as diff
from (select (res).* from w_fnres) as foo
where diff > 0.0001 or diff is null
order by plot, reference_year_set, variable;
