-- Copyright 2017, 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.

/*
tests of API functions for the GUI congiguration of regression estimates
*/

-------------------------------------------------------------------------------------------------------------------------------------------------------
-- tests for function nfiesta.fn_api_get_1pgroups4gregmap(INT, INT[], INT[])
-------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------- testing invalid inputs ------------------------------------------------------------------------------
-- test NULL for _estimation_period argument
SELECT * FROM nfiesta.fn_api_get_1pgroups4gregmap(NULL, ARRAY[44,45,46,47,48,49,50,51,52,53,56,57,58,62], ARRAY[1,2,3]);

-- test NULL for _estimation_cell argument
SELECT * FROM nfiesta.fn_api_get_1pgroups4gregmap(1, NULL, ARRAY[1,2,3]);

-- test NULL for _variables argument
SELECT * FROM nfiesta.fn_api_get_1pgroups4gregmap(1, ARRAY[44,45,46,47,48,49,50,51,52,53,56,57,58,62], NULL);

-- test for NULL as an element of _estimation_cell argument
SELECT * FROM nfiesta.fn_api_get_1pgroups4gregmap(1, ARRAY[44,45,46,47,NULL,49,50,51,52,53,56,57,58,62], ARRAY[1,2,3]);
SELECT * FROM nfiesta.fn_api_get_1pgroups4gregmap(1, ARRAY[NULL], ARRAY[1,2,3]);
SELECT * FROM nfiesta.fn_api_get_1pgroups4gregmap(1, ARRAY[NULL]::int[], ARRAY[1,2,3]);

-- test for NULL as an element of _variables argument
SELECT * FROM nfiesta.fn_api_get_1pgroups4gregmap(1, ARRAY[44,45,46,47,48,49,50,51,52,53,56,57,58,62], ARRAY[NULL,2,NULL]);
SELECT * FROM nfiesta.fn_api_get_1pgroups4gregmap(1, ARRAY[44,45,46,47,48,49,50,51,52,53,56,57,58,62], ARRAY[NULL]);
SELECT * FROM nfiesta.fn_api_get_1pgroups4gregmap(1, ARRAY[44,45,46,47,48,49,50,51,52,53,56,57,58,62], ARRAY[NULL]::int[]);

-- test if estimation cells belong to one estimation cell collection (adding 1 to the valid array _estimation_cells, 1 
-- belongs to another collection but the same stratum
SELECT * FROM nfiesta.fn_api_get_1pgroups4gregmap(1, ARRAY[1,44,45,46,47,48,49,50,51,52,53,56,57,58,62], ARRAY[1,2,3]);

------------------------------------------------ testing valid inputs ---------------------------------------------------------------------------------
-- cells of one stratum three variables, complete group
SELECT * FROM nfiesta.fn_api_get_1pgroups4gregmap(1, ARRAY[44,45,46,47,48,49,50,51,52,53,56,57,58,62], ARRAY[1,2,3]);

-- cells of one stratum, one variable without configuration added to three with configuration making the group incomplete
SELECT * FROM nfiesta.fn_api_get_1pgroups4gregmap(1, ARRAY[44,45,46,47,48,49,50,51,52,53,56,57,58,62], ARRAY[1,2,3,-1]);

-- cells of one stratum, one variable without configuration, no group given (NULL)
SELECT * FROM nfiesta.fn_api_get_1pgroups4gregmap(1, ARRAY[44,45,46,47,48,49,50,51,52,53,56,57,58,62], ARRAY[-1]);

-------------------------------------------------------------------------------------------------------------------------------------------------------
-- tests for function nfiesta.fn_api_get_group4panel_refyearset_combinations(_panels integer[], _refyearsets integer[])
-------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------- testing invalid inputs ------------------------------------------------------------------------------
-- passing NULL for _panels
SELECT * FROM nfiesta.fn_api_get_group4panel_refyearset_combinations(NULL, ARRAY[2,3,3]);

-- passing NULL for _refyearsets
SELECT * FROM nfiesta.fn_api_get_group4panel_refyearset_combinations(ARRAY[1,2,5], NULL);

-- passing _panels and _refyearsets of unequal length
SELECT * FROM nfiesta.fn_api_get_group4panel_refyearset_combinations(ARRAY[1,2,5], ARRAY[2]);

-- passing NULL within _panels
SELECT * FROM nfiesta.fn_api_get_group4panel_refyearset_combinations(ARRAY[1,NULL,5], ARRAY[2,3,3]);

-- passing NULL within _refyearsets
SELECT * FROM nfiesta.fn_api_get_group4panel_refyearset_combinations(ARRAY[1,2,5], ARRAY[2,NULL,3]);

------------------------------------------------ testing valid inputs ---------------------------------------------------------------------------------
-- existing group id = 23
SELECT * FROM nfiesta.fn_api_get_group4panel_refyearset_combinations(ARRAY[1,5,7], ARRAY[2,3,3]);

-- testing the dependence on the order of arrays, should give the same group id = 23
SELECT * FROM nfiesta.fn_api_get_group4panel_refyearset_combinations(ARRAY[7,5,1], ARRAY[3,3,2]);

-- should give no group because the ordered pairs of panel and refyearset do not match group id = 23
SELECT * FROM nfiesta.fn_api_get_group4panel_refyearset_combinations(ARRAY[1,5,7], ARRAY[3,2,3]);

-- existing group No 10
SELECT * FROM nfiesta.fn_api_get_group4panel_refyearset_combinations(ARRAY[1,2,5], ARRAY[NULL,NULL,NULL]::int[]);

-- non existing group, no records retrieved
SELECT * FROM nfiesta.fn_api_get_group4panel_refyearset_combinations(ARRAY[1,2,5], ARRAY[2,3,3]);

-------------------------------------------------------------------------------------------------------------------------------------------------------
-- tests for function nfiesta.fn_api_get_panel_refyearset_combinations4groups(_panel_refyearset_groups INT[])
-------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------- testing invalid inputs ------------------------------------------------------------------------------
-- passing NULL for _panel_refyearset_groups
SELECT * FROM nfiesta.fn_api_get_panel_refyearset_combinations4groups(NULL);

-- passing an _panel_refyearset_groups containing NULL
SELECT * FROM nfiesta.fn_api_get_panel_refyearset_combinations4groups(ARRAY[23, 10, NULL]);

------------------------------------------------ testing valid inputs ---------------------------------------------------------------------------------
-- group 23
SELECT * FROM nfiesta.fn_api_get_panel_refyearset_combinations4groups(ARRAY[23]);

-- group 10 (no reference year sets)
SELECT * FROM nfiesta.fn_api_get_panel_refyearset_combinations4groups(ARRAY[10]);

-- group 20
SELECT * FROM nfiesta.fn_api_get_panel_refyearset_combinations4groups(ARRAY[20]);

-- groups 20 and 23, note combination 1 (panel) and 2 (refyearset) included only once in the function output
SELECT * FROM nfiesta.fn_api_get_panel_refyearset_combinations4groups(ARRAY[20,23]);

-- non-existing group, no records
SELECT * FROM nfiesta.fn_api_get_panel_refyearset_combinations4groups(ARRAY[-20]);

 -- combination of non-existing and existing group, records of the existing returned
SELECT * FROM nfiesta.fn_api_get_panel_refyearset_combinations4groups(ARRAY[-20, 23]);

-------------------------------------------------------------------------------------------------------------------------------------------------------
-- tests for function nfiesta.fn_api_do_labels_and_descriptions_of_panel_refyearset_group_exist(
--	IN _label VARCHAR(200), IN _label_en VARCHAR(200), IN _description TEXT, IN _description_en TEXT,
--	OUT _label_unique BOOLEAN, OUT _label_en_unique BOOLEAN, OUT _description_unique BOOLEAN, OUT _description_en_unique BOOLEAN)
-------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------- testing invalid inputs ------------------------------------------------------------------------------
-- passing NULL for _label
SELECT * FROM nfiesta.fn_api_do_labels_and_descriptions_of_panel_refyearset_group_exist(
	NULL, 'no_of_panels: 2;01-01-2011-12-31-2014', 'no_ofa_panels: 2;01-01-2011-12-31-2014', 'no_of_panels: 2;01-01-2011-12-31-2014');

-- passing NULL for _label_en
SELECT * FROM nfiesta.fn_api_do_labels_and_descriptions_of_panel_refyearset_group_exist(
	'no_of_panels: 2;01-01-2011-12-31-2014', NULL, 'no_of_panels: 2;01-01-2011-12-31-2014', 'no_of_panels: 2;01-01-2011-12-31-2014');

-- passing NULL for _description
SELECT * FROM nfiesta.fn_api_do_labels_and_descriptions_of_panel_refyearset_group_exist(
	'no_of_panels: 2;01-01-2011-12-31-2014', 'no_of_panels: 2;01-01-2011-12-31-2014', NULL, 'no_of_panels: 2;01-01-2011-12-31-2014');

-- passing NULL for _description
SELECT * FROM nfiesta.fn_api_do_labels_and_descriptions_of_panel_refyearset_group_exist(
	'no_of_panels: 2;01-01-2011-12-31-2014', 'no_of_panels: 2;01-01-2011-12-31-2014', 'no_of_panels: 2;01-01-2011-12-31-2014', NULL);

------------------------------------------------ testing valid inputs ---------------------------------------------------------------------------------
-- all found, all TRUE
SELECT * FROM nfiesta.fn_api_do_labels_and_descriptions_of_panel_refyearset_group_exist(
	'no_of_panels: 2;01-01-2011-12-31-2014', 'no_of_panels: 2;01-01-2011-12-31-2014', 'no_of_panels: 2;01-01-2011-12-31-2014', 'no_of_panels: 2;01-01-2011-12-31-2014');
	
-- first found (TRUE), the rest FALSE
SELECT * FROM nfiesta.fn_api_do_labels_and_descriptions_of_panel_refyearset_group_exist(
	'no_of_panels: 2;01-01-2011-12-31-2014', 'no_label_en', 'no_description', 'no_description_en');

-------------------------------------------------------------------------------------------------------------------------------------------------------
-- tests for function nfiesta.fn_api_get_wmodels_sigma_paramtypes(_panels INT[], _estimation_cells INT[])
-------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------- testing invalid inputs ------------------------------------------------------------------------------
-- test NULL for _panels argument
SELECT * FROM nfiesta.fn_api_get_wmodels_sigma_paramtypes(NULL, ARRAY[51,55,56,60,61,67,68,69,70,75,76,77,83]);

-- test NULL for _estimation_cell argument
SELECT * FROM nfiesta.fn_api_get_wmodels_sigma_paramtypes(ARRAY[1,2], NULL);

-- test for NULL as an element of _panels
SELECT * FROM nfiesta.fn_api_get_wmodels_sigma_paramtypes(ARRAY[1,NULL], ARRAY[51,55,56,60,61,67,68,69,70,75,76,77,83]);
SELECT * FROM nfiesta.fn_api_get_wmodels_sigma_paramtypes(ARRAY[NULL], ARRAY[51,55,56,60,61,67,68,69,70,75,76,77,83]);
SELECT * FROM nfiesta.fn_api_get_wmodels_sigma_paramtypes(ARRAY[NULL]::int[], ARRAY[51,55,56,60,61,67,68,69,70,75,76,77,83]);

-- test for NULL as an element of _estimation_cells
SELECT * FROM nfiesta.fn_api_get_wmodels_sigma_paramtypes(ARRAY[1,2], ARRAY[NULL,55,56,60,61,67,68,69,70,75,76,77,83]);
SELECT * FROM nfiesta.fn_api_get_wmodels_sigma_paramtypes(ARRAY[1,2], ARRAY[NULL]);
SELECT * FROM nfiesta.fn_api_get_wmodels_sigma_paramtypes(ARRAY[1,2], ARRAY[NULL]::int[]);

-- test if estimation cells belong to one estimation cell collection (adding 1 to the valid array _estimation_cells
SELECT * FROM nfiesta.fn_api_get_wmodels_sigma_paramtypes(ARRAY[1,2], ARRAY[1,55,56,60,61,67,68,69,70,75,76,77,83]);

------------------------------------------------ testing valid inputs ---------------------------------------------------------------------------------
-- update label, label_en and description en in the test data for table nfiesta.t_model
UPDATE nfiesta.t_model
SET description_en = description;

UPDATE nfiesta.t_model
SET label = left(description, 200)::VARCHAR(200);

UPDATE nfiesta.t_model
SET label_en = label;

-- update label_en and description en in the test data for table nfiesta.c_param_area_type
UPDATE nfiesta.c_param_area_type 
SET label_en = label;

UPDATE nfiesta.c_param_area_type 
SET description_en = description;

-- update label, label_en, description and description en in the test data for table nfiesta.f_a_param_area
UPDATE nfiesta.f_a_param_area 
SET label = param_area_code;

UPDATE nfiesta.f_a_param_area 
SET label_en = param_area_code;

UPDATE nfiesta.f_a_param_area 
SET description = param_area_code;

UPDATE nfiesta.f_a_param_area 
SET description_en = param_area_code;

-- three alternative model, sigma and param type combinations found, only one allows for configuration of regression estimates for all cell 
-- provided as input to the function
SELECT * FROM nfiesta.fn_api_get_wmodels_sigma_paramtypes(ARRAY[1,2], ARRAY[55,56,60,61,67,68,69,70,75,76,77,83]);

-------------------------------------------------------------------------------------------------------------------------------------------------------
-- tests for function nfiesta.fn_api_save_panel_refyearset_group(_panels INT[], _refyearsets INT[], _label VARCHAR(200), _description TEXT, 
--	_label_en VARCHAR(200), _description_en TEXT)
-------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------- testing invalid inputs ------------------------------------------------------------------------------
-- test NULL for _panels argument
SELECT * FROM nfiesta.fn_api_save_panel_refyearset_group(NULL, ARRAY[2,3,3], 'nejaky label', 'nejaky description', 'some label en', 'some description_en');

-- test NULL for _refyearsets argument
SELECT * FROM nfiesta.fn_api_save_panel_refyearset_group(ARRAY[1,5,7], NULL, 'nejaky label', 'nejaky description', 'some label en', 'some description_en');

-- passing NULL for _label
SELECT * FROM nfiesta.fn_api_save_panel_refyearset_group(ARRAY[1,5,7], ARRAY[2,3,3], NULL, 'nejaky description', 'some label en', 'some description_en');

-- passing NULL for _description
SELECT * FROM nfiesta.fn_api_save_panel_refyearset_group(ARRAY[1,5,7], ARRAY[2,3,3], 'nejaky label', NULL, 'some label en', 'some description_en');

-- passing NULL for _label_en
SELECT * FROM nfiesta.fn_api_save_panel_refyearset_group(ARRAY[1,5,7], ARRAY[2,3,3], 'nejaky label', 'nejaky description', NULL, 'some description_en');

-- passing NULL for _description
SELECT * FROM nfiesta.fn_api_save_panel_refyearset_group(ARRAY[1,5,7], ARRAY[2,3,3], 'nejaky label', 'nejaky description', 'some label en', NULL);

-- test for NULL as an element of _panels
SELECT * FROM nfiesta.fn_api_save_panel_refyearset_group(ARRAY[1,NULL,7], ARRAY[2,3,3], 'nejaky label', 'nejaky description', 'some label en', 'some description_en');
SELECT * FROM nfiesta.fn_api_save_panel_refyearset_group(ARRAY[NULL], ARRAY[2,3,3], 'nejaky label', 'nejaky description', 'some label en', 'some description_en');
SELECT * FROM nfiesta.fn_api_save_panel_refyearset_group(ARRAY[NULL]::int[], ARRAY[2,3,3], 'nejaky label', 'nejaky description', 'some label en', 'some description_en');

-- test for NULL as an element of _refyearsets
SELECT * FROM nfiesta.fn_api_save_panel_refyearset_group(ARRAY[1,5,7], ARRAY[1,NULL,7], 'nejaky label', 'nejaky description', 'some label en', 'some description_en');
SELECT * FROM nfiesta.fn_api_save_panel_refyearset_group(ARRAY[1,5,7], ARRAY[NULL], 'nejaky label', 'nejaky description', 'some label en', 'some description_en');
SELECT * FROM nfiesta.fn_api_save_panel_refyearset_group(ARRAY[1,5,7], ARRAY[NULL]::int[], 'nejaky label', 'nejaky description', 'some label en', 'some description_en');

-- test if saving a group with non-existing panel fails
SELECT * FROM nfiesta.fn_api_save_panel_refyearset_group(ARRAY[-1,5,7], ARRAY[2,3,3], 'nejaky label', 'nejaky description', 'some label en', 'some description_en');

-- test if saving a group with non-existing reference yearset fails
SELECT * FROM nfiesta.fn_api_save_panel_refyearset_group(ARRAY[1,5,7], ARRAY[-1,3,3], 'nejaky label', 'nejaky description', 'some label en', 'some description_en');

-- test if saving a group with a list of panels and refyearsets equal to an existing group fails
SELECT * FROM nfiesta.fn_api_save_panel_refyearset_group(ARRAY[1,5,7], ARRAY[2,3,3], 'nejaky label', 'nejaky description', 'some label en', 'some description_en');

------------------------------------------------ testing valid inputs ---------------------------------------------------------------------------------
SELECT * FROM nfiesta.fn_api_save_panel_refyearset_group(ARRAY[1,7], ARRAY[2,3], 'nejaky label', 'nejaky description', 'some label en', 'some description_en');

-------------------------------------------------------------------------------------------------------------------------------------------------------
-- tests for function nfiesta.fn_api_save_gregmap_total_config(_estimation_period INT, _estimation_cell INT, _variable INT,  
--	_panel_refyearset_group INT, _working_model INT, _sigma BOOLEAN, _param_area_type INT, _force_synthetic BOOLEAN)
-------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------- testing invalid inputs ------------------------------------------------------------------------------
-- test NULL for _estimation_period argument
SELECT * FROM nfiesta.fn_api_save_gregmap_total_config(NULL,55,1,19,1,FALSE,3,FALSE);

-- test NULL for _estimation_cell argument
SELECT * FROM nfiesta.fn_api_save_gregmap_total_config(1,NULL,1,19,1,FALSE,3,FALSE);

-- test NULL for _variable argument
SELECT * FROM nfiesta.fn_api_save_gregmap_total_config(1,55,NULL,19,1,FALSE,3,FALSE);

-- test NULL for _panel_refyearset_group argument
SELECT * FROM nfiesta.fn_api_save_gregmap_total_config(1,55,1,NULL,1,FALSE,3,FALSE);

-- test NULL for _working_model argument
SELECT * FROM nfiesta.fn_api_save_gregmap_total_config(1,55,1,19,NULL,FALSE,3,FALSE);

-- test NULL for _sigma argument
SELECT * FROM nfiesta.fn_api_save_gregmap_total_config(1,55,1,19,1,NULL,3,FALSE);

-- test NULL for _param_area_type argument
SELECT * FROM nfiesta.fn_api_save_gregmap_total_config(1,55,1,19,1,FALSE,NULL,FALSE);

-- test NULL for _force_synthetic argument
SELECT * FROM nfiesta.fn_api_save_gregmap_total_config(1,55,1,19,1,FALSE,3,NULL);

-- test for an invalid group of panel and reference-year set combinations - no panels  found
SELECT * FROM nfiesta.fn_api_save_gregmap_total_config(1,55,1,-1,1,FALSE,3,FALSE);

-- test for a group of panel and reference-year set combinations that has no equivaent in t_aux_conf (with no reference year sets attached)
SELECT * FROM nfiesta.fn_api_save_gregmap_total_config(1,55,1,33,1,FALSE,3,FALSE);

-- test for a parametrisation area not matching the estimation cell
SELECT * FROM nfiesta.fn_api_save_gregmap_total_config(1,55,1,19,1,FALSE,2,FALSE);

-- test for missing aux configuration - no one has sigma TRUE
SELECT * FROM nfiesta.fn_api_save_gregmap_total_config(1,55,1,19,1,TRUE,3,FALSE);

------------------------------------------------ testing valid inputs ---------------------------------------------------------------------------------
-- save a configuration with force_synthetic = FALSE  
SELECT * FROM nfiesta.fn_api_save_gregmap_total_config(1,55,1,19,1,FALSE,3,FALSE);

-- save a configuration with force_synthetic = TRUE  
SELECT * FROM nfiesta.fn_api_save_gregmap_total_config(1,55,1,19,1,FALSE,3,TRUE);
