with w_fnres as (
	select nfiesta.fn_add_aux_total_attr((select array_agg(id order by id) from nfiesta.t_variable)) as res
)
select estimation_cell,
	variable, round(aux_total::numeric, 10) as aux_total,
	round(aux_total_sum::numeric, 10) as aux_total_sum,
	variables_def, variables_found,
	round(diff::numeric, 10) as diff
from (select (res).* from w_fnres) as foo
where diff > 0.0001 or diff is null
order by variable, estimation_cell;

insert into nfiesta.t_aux_total (estimation_cell, aux_total, variable)
values
(1, 100000.2, 13),
(1, 150000.1, 14),
(1, 150000.5, 15);

select estimation_cell,
	variable, round(aux_total::numeric, 10) as aux_total,
	round(aux_total_sum::numeric, 10) as aux_total_sum,
	variables_def, variables_found,
	round(diff::numeric, 10) as diff
from (select (nfiesta.fn_add_aux_total_attr(ARRAY[1,2,3,4,5,6,13,14,15])).*) as fn
where diff > 0.0001 or diff is null
order by variable, estimation_cell;

------------------------------------------

with w_fnres as (
	select nfiesta.fn_add_aux_total_geo((select array_agg(id order by id) from nfiesta.c_estimation_cell)) as res
)
select variable,
	estimation_cell, round(aux_total::numeric, 10) as aux_total,
	round(aux_total_sum::numeric, 10) as aux_total_sum,
	estimation_cells_def, estimation_cells_found,
	round(diff::numeric, 10) as diff
from (select (res).* from w_fnres) as foo
where diff > 0.0001 or diff is null
order by estimation_cell, variable;
