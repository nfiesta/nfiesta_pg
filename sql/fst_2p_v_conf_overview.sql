--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
SELECT * from nfiesta.v_conf_overview
where estimate_type_str in ('2p_total', '1p2p_ratio', '2p1p_ratio', '2p2p_ratio')
order by estimate_type_str, estimation_cell_label, target_variable_label, area_domain_category_label, sub_population_category_label, param_area_code, model_description, force_synthetic;
