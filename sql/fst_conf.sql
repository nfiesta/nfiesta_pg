--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--
-- PostgreSQL database dump
--
\set srcdir `echo $SRC_DIR`

-- Dumped from database version 12.1 (Debian 12.1-1.pgdg100+1)
-- Dumped by pg_dump version 12.1 (Debian 12.1-1.pgdg100+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
--SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Data for Name: f_a_cell; Type: TABLE DATA; Schema: nfiesta; Owner: vagrant
--

\set afile :srcdir '/sql/csv/estimation_cell.csv'
CREATE FOREIGN TABLE csv.estimation_cell (
	estimation_cell		integer				not null,
	stratum_geometry	text				not null
) SERVER csv_files
OPTIONS ( header 'true', format 'csv', delimiter ';', filename :'afile' );

insert into nfiesta.f_a_cell(estimation_cell, geom)
select
	estimation_cell,
	ST_GeomFromEWKT(stratum_geometry) as geom
from csv.estimation_cell;

--
-- Data for Name: t_estimation_cell_hierarchy; Type: TABLE DATA; Schema: nfiesta; Owner: vagrant
--

/*
with w_nfrd as (
	select id, label, st_setsrid(geom, 5221) as geom from nfiesta.c_estimation_cell
	inner join nfiesta.f_a_cell on (f_a_cell.estimation_cell = c_estimation_cell.id)
	where estimation_cell_collection = 1 and label like 'NFRD%'
)
, 
w_nfr as (
	select id, label, st_setsrid(geom, 5221) as geom from nfiesta.c_estimation_cell
	inner join nfiesta.f_a_cell on (f_a_cell.estimation_cell = c_estimation_cell.id)
	where estimation_cell_collection = 1 and id not in (select id from w_nfrd)
)
select format('(%s, %s),', w_nfr.id, w_nfrd.id)
from w_nfrd
left join w_nfr on st_within(w_nfr.geom, w_nfrd.geom)
order by w_nfrd.id, w_nfr.id;
*/

/*
with w_50km as (
	select id, label, st_setsrid(geom, 5221) as geom from nfiesta.c_estimation_cell 
	inner join nfiesta.f_a_cell on (f_a_cell.estimation_cell = c_estimation_cell.id)
	where estimation_cell_collection = 3
)
,
w_25km as (
	select id, label, st_setsrid(geom, 5221) as geom from nfiesta.c_estimation_cell 
	inner join nfiesta.f_a_cell on (f_a_cell.estimation_cell = c_estimation_cell.id)
	where estimation_cell_collection = 4
)
select format('(%s, %s),', w_25km.id, w_50km.id)
from w_50km
left join w_25km on st_within(w_25km.geom, st_buffer(w_50km.geom, 10))
order by w_50km.id, w_25km.id;
*/

/*
select
	cell, cell_superior
from nfiesta.t_estimation_cell_hierarchy
order by cell, cell_superior;
*/

\set afile :srcdir '/sql/csv/estimation_cell_hierarchy.csv'
CREATE FOREIGN TABLE csv.estimation_cell_hierarchy (
	cell		integer		not null,
	cell_superior	integer		not null
) SERVER csv_files
OPTIONS ( header 'true', format 'csv', delimiter ';', filename :'afile' );

INSERT INTO nfiesta.t_estimation_cell_hierarchy (cell, cell_superior)
select
	cell, cell_superior
from csv.estimation_cell_hierarchy;

-- table with intersection of estimation_cells and stratas
INSERT INTO nfiesta.t_stratum_in_estimation_cell (stratum, estimation_cell, geom, area_m2)
SELECT
	stratum, estimation_cell, ST_Multi(geom), ST_Area(geom)
FROM
	(SELECT
		t1.id AS stratum, t2.estimation_cell, ST_Intersection(t1.geom, t2.geom) AS geom
	FROM
		sdesign.t_stratum AS t1
	INNER JOIN
		(SELECT estimation_cell, ST_Multi(ST_Union(geom)) AS geom 
		FROM nfiesta.f_a_cell
		GROUP BY estimation_cell) AS t2
	ON
		(t1.geom && t2.geom AND
		ST_Intersects(t1.geom, t2.geom)) AND
		NOT ST_Touches(t1.geom, t2.geom)
	) AS t1
;

--
-- Data for Name: c_param_area_type; Type: TABLE DATA; Schema: nfiesta; Owner: vagrant
--

COPY nfiesta.c_param_area_type  FROM stdin;
1	INSPIRE 50x50 km	INSPIRE grid 50x50 km.	\N	\N
2	stratum	Area covering all the stratum.	\N	\N
3	strata set	Area covering all the strata set.	\N	\N
\.

--
-- Data for Name: f_a_param_area; Type: TABLE DATA; Schema: nfiesta; Owner: vagrant
--

/*
select
	format(
		'(%s, ''%s'', ''%s''::text[]),',
		f_a_param_area.param_area_type,
		f_a_param_area.param_area_code,
		array_agg(c_estimation_cell.label order by c_estimation_cell.label)
	)
from nfiesta.cm_cell2param_area_mapping
inner join nfiesta.c_estimation_cell ON c_estimation_cell.id = cm_cell2param_area_mapping.estimation_cell
inner join nfiesta.f_a_param_area ON f_a_param_area.gid = cm_cell2param_area_mapping.param_area
group by f_a_param_area.param_area_code, f_a_param_area.param_area_type
order by f_a_param_area.param_area_code
;
*/

with w_data (f_a_param_area__param_area_type, f_a_param_area__param_area_code, c_estimation_cell__labels) as (
	values
		(1, '50kmE460N290', '{50kmE460N290}'::text[]),
		(1, '50kmE460N295', '{50kmE460N295}'::text[]),
		(1, '50kmE465N285', '{50kmE465N285}'::text[]),
		(1, '50kmE465N290', '{25kmE4650N2900,50kmE465N290}'::text[]),
		(1, '50kmE465N295', '{25kmE4650N2950,50kmE465N295}'::text[]),
		(1, '50kmE470N285', '{50kmE470N285}'::text[]),
		(1, '50kmE470N290', '{25kmE4700N2900,50kmE470N290}'::text[]),
		(1, '50kmE470N295', '{25kmE4700N2950,50kmE470N295}'::text[]),
		(1, '50kmE470N300', '{50kmE470N300}'::text[]),
		(1, '50kmE475N285', '{25kmE4750N2850,50kmE475N285}'::text[]),
		(1, '50kmE475N290', '{25kmE4750N2900,50kmE475N290}'::text[]),
		(1, '50kmE475N295', '{25kmE4750N2950,50kmE475N295}'::text[]),
		(1, '50kmE475N300', '{25kmE4750N3000,50kmE475N300}'::text[]),
		(1, '50kmE480N285', '{25kmE4800N2850,50kmE480N285}'::text[]),
		(1, '50kmE480N290', '{25kmE4800N2900,50kmE480N290}'::text[]),
		(1, '50kmE480N295', '{25kmE4800N2950,50kmE480N295}'::text[]),
		(1, '50kmE480N300', '{25kmE4800N3000,50kmE480N300,NFR27}'::text[]),
		(1, '50kmE480N305', '{25kmE4800N3050,50kmE480N305}'::text[]),
		(1, '50kmE485N285', '{50kmE485N285}'::text[]),
		(1, '50kmE485N290', '{25kmE4850N2900,50kmE485N290}'::text[]),
		(1, '50kmE485N295', '{25kmE4850N2950,50kmE485N295}'::text[]),
		(1, '50kmE485N300', '{25kmE4850N3000,50kmE485N300}'::text[]),
		(1, '50kmE485N305', '{25kmE4850N3050,50kmE485N305}'::text[]),
		(1, '50kmE490N290', '{25kmE4900N2900,50kmE490N290}'::text[]),
		(1, '50kmE490N295', '{25kmE4900N2950,50kmE490N295}'::text[]),
		(1, '50kmE490N300', '{25kmE4900N3000,50kmE490N300}'::text[]),
		(1, '50kmE495N295', '{25kmE4950N2950,50kmE495N295}'::text[]),
		(3, 'CM border', '{25kmE4675N2925,25kmE4700N2900,25kmE4700N2925,25kmE4725N2900,25kmE4725N2925,25kmE4750N2900,25kmE4750N2925,25kmE4750N2950,25kmE4750N2975,25kmE4775N2925,25kmE4775N2950,25kmE4775N2975,25kmE4800N2950,50kmE470N290,50kmE475N295,NFR16,NFR30,NFR31,NFR33,NFRD12,NFRD7}'::text[]),
		(3, 'MS', '{25kmE4825N2900,25kmE4825N3000,25kmE4850N2900,25kmE4850N2925,25kmE4850N2950,25kmE4850N2975,25kmE4875N2925,25kmE4875N2950,25kmE4875N2975,25kmE4900N2950,25kmE4900N2975,50kmE485N295,NFR27,NFR28,NFR29,NFR32,NFR34,NFR35,NFR36,NFR37,NFR38,NFR39,NFR40,NFR41,NFRD11,NFRD13,NFRD14}'::text[]),
		(2, 'NFR16', '{25kmE4675N2925,25kmE4700N2925,NFR16,NFRD7}'::text[]),
		(2, 'NFR27', '{NFR27}'::text[]),
		(2, 'NFR28', '{NFR28}'::text[]),
		(2, 'NFR29', '{25kmE4850N2975,NFR29}'::text[]),
		(2, 'NFR30', '{NFR30}'::text[]),
		(2, 'NFR31', '{25kmE4775N2975,NFR31}'::text[]),
		(2, 'NFR32', '{NFR32}'::text[]),
		(2, 'NFR33', '{NFR33}'::text[]),
		(2, 'NFR34', '{NFR34}'::text[]),
		(2, 'NFR35', '{NFR35}'::text[]),
		(2, 'NFR36', '{NFR36}'::text[]),
		(2, 'NFR37', '{NFR37}'::text[]),
		(2, 'NFR38', '{NFR38}'::text[]),
		(2, 'NFR39', '{NFR39}'::text[]),
		(2, 'NFR40', '{NFR40}'::text[]),
		(2, 'NFR41', '{NFR41}'::text[]),
		(2, 'NFRD11', '{25kmE4825N3000,25kmE4850N2975,NFR27,NFR28,NFR29,NFR32,NFRD11}'::text[]),
		(2, 'NFRD12', '{25kmE4775N2975,25kmE4800N2950,NFR30,NFR31,NFR33,NFRD12}'::text[]),
		(2, 'NFRD13', '{NFR34,NFR35,NFRD13}'::text[]),
		(2, 'NFRD14', '{25kmE4875N2925,25kmE4900N2950,NFR36,NFR37,NFR38,NFR39,NFR40,NFR41,NFRD14}'::text[]),
		(2, 'NFRD7', '{25kmE4675N2925,25kmE4700N2925,NFR16,NFRD7}'::text[])
)
SELECT nfiesta.fn_create_param_area(
	f_a_param_area__param_area_type,
	(SELECT array_agg(id) FROM nfiesta.c_estimation_cell WHERE label IN (SELECT UNNEST(c_estimation_cell__labels))),
	f_a_param_area__param_area_code
)
FROM w_data;

--
-- Data for Name: t_model; Type: TABLE DATA; Schema: nfiesta; Owner: vagrant
--

COPY nfiesta.t_model(id, description)  FROM stdin;
1	forest from OLIL
\.

--
--
-- Data for Name: t_aux_total; Type: TABLE DATA; Schema: nfiesta; Owner: vagrant
--

/*
select
	format('(''%s'', %s, %s),' , c_estimation_cell.label, t_aux_total.variable, t_aux_total.aux_total)
	from nfiesta.t_aux_total
	inner join nfiesta.c_estimation_cell ON c_estimation_cell.id = t_aux_total.estimation_cell
	order by c_estimation_cell.label, t_aux_total.variable;
*/
with w_data (estimation_cell__label, variable, aux_total) as (
	values
		('25kmE4625N2925', 5, 4077.6381604466715),
		('25kmE4625N2925', 6, 58414.07468374652),
		('25kmE4625N2950', 5, 9.255817289097822),
		('25kmE4625N2950', 6, 62480.06309698639),
		('25kmE4650N2900', 5, 1018.5289171476298),
		('25kmE4650N2900', 6, 61475.34675399436),
		('25kmE4650N2925', 5, 8935.540746272967),
		('25kmE4650N2925', 6, 53554.98194630571),
		('25kmE4650N2950', 5, 7845.853077036585),
		('25kmE4650N2950', 6, 54642.92788373595),
		('25kmE4675N2875', 5, 9536.249581777685),
		('25kmE4675N2875', 6, 52960.50656530557),
		('25kmE4675N2900', 5, 17445.90480676425),
		('25kmE4675N2900', 6, 45046.47893342914),
		('25kmE4675N2925', 5, 21741.28709717349),
		('25kmE4675N2925', 6, 40748.350950433334),
		('25kmE4675N2950', 5, 11186.049869240152),
		('25kmE4675N2950', 6, 51302.48454419314),
		('25kmE4675N2975', 5, 7705.6971487049195),
		('25kmE4675N2975', 6, 54783.39144466704),
		('25kmE4700N2875', 5, 16137.153104042127),
		('25kmE4700N2875', 6, 46357.83868426466),
		('25kmE4700N2900', 5, 22761.520324110435),
		('25kmE4700N2900', 6, 39729.66177143589),
		('25kmE4700N2925', 5, 23436.909488719135),
		('25kmE4700N2925', 6, 39052.11828323496),
		('25kmE4700N2950', 5, 18687.544124167063),
		('25kmE4700N2950', 6, 43801.00199871001),
		('25kmE4700N2975', 5, 4103.8484510744565),
		('25kmE4700N2975', 6, 58385.90657096046),
		('25kmE4725N2875', 5, 15926.043650562182),
		('25kmE4725N2875', 6, 46567.462499456626),
		('25kmE4725N2900', 5, 19127.573784871038),
		('25kmE4725N2900', 6, 43362.66793212143),
		('25kmE4725N2925', 5, 18309.90063565615),
		('25kmE4725N2925', 6, 44178.76030360489),
		('25kmE4725N2950', 5, 22904.429819539317),
		('25kmE4725N2950', 6, 39584.35328532847),
		('25kmE4725N2975', 5, 18257.80328423474),
		('25kmE4725N2975', 6, 44232.82495026666),
		('25kmE4725N3000', 5, 10.178966965837924),
		('25kmE4725N3000', 6, 62484.03810933867),
		('25kmE4750N2850', 5, 1317.0774900116744),
		('25kmE4750N2850', 6, 61179.62434143337), --total missing to test control mechanism
		('25kmE4750N2875', 5, 20051.377024569316),
		('25kmE4750N2875', 6, 42440.89535715145),
		('25kmE4750N2900', 5, 18654.971775225735),
		('25kmE4750N2900', 6, 43834.56219311187),
		('25kmE4750N2925', 5, 19126.084575599434),
		('25kmE4750N2925', 6, 43362.42240621046),
		('25kmE4750N2950', 5, 25581.940166804932),
		('25kmE4750N2950', 6, 36907.27253410572),
		('25kmE4750N2975', 5, 15666.034136242619),
		('25kmE4750N2975', 6, 46825.63917130574),
		('25kmE4750N3000', 5, 3098.928864395825),
		('25kmE4750N3000', 6, 59396.98303372368),
		('25kmE4775N2850', 5, 1546.2174632156443),
		('25kmE4775N2850', 6, 60948.99644680796),
		('25kmE4775N2875', 5, 7190.192609525512),
		('25kmE4775N2875', 6, 55301.07155701026),
		('25kmE4775N2900', 5, 24398.72525540188),
		('25kmE4775N2900', 6, 38090.30548448376),
		('25kmE4775N2925', 5, 30792.80212750932),
		('25kmE4775N2925', 6, 31695.73371845504),
		('25kmE4775N2950', 5, 25371.563350106568),
		('25kmE4775N2950', 6, 37118.23939775509),
		('25kmE4775N2975', 5, 20802.626712326815),
		('25kmE4775N2975', 6, 41690.22907455129),
		('25kmE4775N3000', 5, 7819.444152262431),
		('25kmE4775N3000', 6, 54678.276259360544),
		('25kmE4800N2850', 5, 3001.383871050524),
		('25kmE4800N2850', 6, 59492.564046032276),
		('25kmE4800N2875', 5, 9919.895330076215),
		('25kmE4800N2875', 6, 52570.560526324945),
		('25kmE4800N2900', 5, 6768.055236689406),
		('25kmE4800N2900', 6, 55720.64935974585),
		('25kmE4800N2925', 5, 40471.76025920259),
		('25kmE4800N2925', 6, 22016.957895160187),
		('25kmE4800N2950', 5, 29429.510180173944),
		('25kmE4800N2950', 6, 33061.01157551922),
		('25kmE4800N2975', 5, 27047.657441564454),
		('25kmE4800N2975', 6, 35446.48444045917),
		('25kmE4800N3000', 5, 24351.907113777528),
		('25kmE4800N3000', 6, 38147.69920521464),
		('25kmE4800N3025', 5, 20363.169306070868),
		('25kmE4800N3025', 6, 42143.774897249124),
		('25kmE4800N3050', 5, 5482.16152732425),
		('25kmE4800N3050', 6, 57034.024541089726),
		('25kmE4825N2850', 5, 5777.637072133195),
		('25kmE4825N2850', 6, 56715.24356468735),
		('25kmE4825N2875', 5, 11677.520508897851),
		('25kmE4825N2875', 6, 50812.302101620764),
		('25kmE4825N2900', 5, 19214.539497869628),
		('25kmE4825N2900', 6, 43273.989431508526),
		('25kmE4825N2925', 5, 13252.013576034484),
		('25kmE4825N2925', 6, 49237.01179713754),
		('25kmE4825N2950', 5, 6750.506093669608),
		('25kmE4825N2950', 6, 55740.83299790631),
		('25kmE4825N2975', 5, 17918.610510941915),
		('25kmE4825N2975', 6, 44576.88815814812),
		('25kmE4825N3000', 5, 39631.19112686976),
		('25kmE4825N3000', 6, 22870.343065949215),
		('25kmE4825N3025', 5, 42549.15462306371),
		('25kmE4825N3025', 6, 19960.322698407632),
		('25kmE4825N3050', 5, 1283.3713524357577),
		('25kmE4825N3050', 6, 61235.9900031779),
		('25kmE4850N2875', 5, 9995.8810954349),
		('25kmE4850N2875', 6, 52493.459440429186),
		('25kmE4850N2900', 5, 18781.686403386517),
		('25kmE4850N2900', 6, 43706.791708218196),
		('25kmE4850N2925', 5, 12372.962163858589),
		('25kmE4850N2925', 6, 50116.46781919926),
		('25kmE4850N2950', 5, 18136.38714439445),
		('25kmE4850N2950', 6, 44355.83802423657),
		('25kmE4850N2975', 5, 32459.210106552386),
		('25kmE4850N2975', 6, 30037.684192918565),
		('25kmE4850N3000', 5, 25292.543043313883),
		('25kmE4850N3000', 6, 37210.92666377624),
		('25kmE4850N3025', 5, 19327.233217403762),
		('25kmE4850N3025', 6, 43184.75229782685),
		('25kmE4850N3050', 5, 44.24275637677637),
		('25kmE4850N3050', 6, 62478.23497920127),
		('25kmE4875N2875', 5, 9473.04376231082),
		('25kmE4875N2875', 6, 53015.943066014835),
		('25kmE4875N2900', 5, 25794.59767209188),
		('25kmE4875N2900', 6, 36693.929981427515),
		('25kmE4875N2925', 5, 36721.1471691043),
		('25kmE4875N2925', 6, 25768.758482629622),
		('25kmE4875N2950', 5, 19008.758505144964),
		('25kmE4875N2950', 6, 43484.39313222283),
		('25kmE4875N2975', 5, 21443.576796528363),
		('25kmE4875N2975', 6, 41054.721415655426),
		('25kmE4875N3000', 5, 9735.901006194643),
		('25kmE4875N3000', 6, 52769.47886692159),
		('25kmE4875N3025', 5, 0),
		('25kmE4875N3025', 6, 62514.43312688947),
		('25kmE4900N2900', 5, 9075.155984001372),
		('25kmE4900N2900', 6, 53413.49837445729),
		('25kmE4900N2925', 5, 27024.93263600344),
		('25kmE4900N2925', 6, 35465.49477052202),
		('25kmE4900N2950', 5, 37293.53182671208),
		('25kmE4900N2950', 6, 25200.55975061102),
		('25kmE4900N2975', 5, 13649.038866772455),
		('25kmE4900N2975', 6, 48850.64248063552),
		('25kmE4900N3000', 5, 10973.891141796163),
		('25kmE4900N3000', 6, 51533.34213571171),
		('25kmE4925N2925', 5, 1104.09277166673),
		('25kmE4925N2925', 6, 61386.879032924895),
		('25kmE4925N2950', 5, 30345.906400736494),
		('25kmE4925N2950', 6, 32149.113286143882),
		('25kmE4925N2975', 5, 22828.56445438604),
		('25kmE4925N2975', 6, 39672.451903856076),
		('25kmE4925N3000', 5, 4099.144374871118),
		('25kmE4925N3000', 6, 58409.855944296505),
		('25kmE4950N2950', 5, 3378.6243383471274),
		('25kmE4950N2950', 6, 59117.288130038425),
		('25kmE4950N2975', 5, 5607.927243551286),
		('25kmE4950N2975', 6, 56894.35057451192),
		('50kmE460N290', 5, 4077.6901417155677),
		('50kmE460N290', 6, 245901.27611229016),
		('50kmE460N295', 5, 9.264795690868302),
		('50kmE460N295', 6, 249947.9138088904),
		('50kmE465N285', 5, 9537.174271808231),
		('50kmE465N285', 6, 240466.98961576386),
		('50kmE465N290', 5, 49142.15297896831),
		('50kmE465N290', 6, 200824.74577880872),
		('50kmE465N295', 5, 26739.84591940005),
		('50kmE465N295', 6, 223215.7005911814),
		('50kmE470N285', 5, 32062.655308405632),
		('50kmE470N285', 6, 217925.19694477157),
		('50kmE470N290', 5, 83635.58791206022),
		('50kmE470N290', 6, 166324.00279135193),
		('50kmE470N295', 5, 63956.1026049697),
		('50kmE470N295', 6, 186002.0878002862),
		('50kmE470N300', 5, 10.186410311968531),
		('50kmE470N300', 6, 249974.07104394725),
		('50kmE475N285', 5, 30102.381199499345),
		('50kmE475N285', 6, 219873.5491339981),
		('50kmE475N290', 5, 92973.41709633738),
		('50kmE475N290', 6, 156982.66810985073),
		('50kmE475N295', 5, 87421.85811126466),
		('50kmE475N295', 6, 162542.1637880156),
		('50kmE475N300', 5, 10919.465815146667),
		('50kmE475N300', 6, 239081.01928427117),
		('50kmE480N285', 5, 30376.558773028417),
		('50kmE480N285', 6, 219591.02575810824),
		('50kmE480N290', 5, 79704.90027700145),
		('50kmE480N290', 6, 170250.55385026505),
		('50kmE480N295', 5, 81146.34721697003),
		('50kmE480N295', 6, 168825.63086970628),
		('50kmE480N300', 5, 126899.9368343637),
		('50kmE480N300', 6, 123118.10154887408),
		('50kmE480N305', 5, 6766.876973096144),
		('50kmE480N305', 6, 243327.73168669987),
		('50kmE485N285', 5, 19467.510392283995),
		('50kmE485N285', 6, 230494.53769086907),
		('50kmE485N290', 5, 93670.83751822135),
		('50kmE485N290', 6, 156285.98027228235),
		('50kmE485N295', 5, 91046.62804456883),
		('50kmE485N295', 6, 158934.41719040117),
		('50kmE485N300', 5, 54354.391025069104),
		('50kmE485N300', 6, 195681.35267201802),
		('50kmE485N305', 5, 44.348067941234824),
		('50kmE485N305', 6, 250077.69692785398),
		('50kmE490N290', 5, 37198.309404464424),
		('50kmE490N290', 6, 212761.05626487714),
		('50kmE490N295', 5, 104118.91859412243),
		('50kmE490N295', 6, 145871.36542364003),
		('50kmE490N300', 5, 15073.533216508415),
		('50kmE490N300', 6, 234978.9735111151),
		('50kmE495N295', 5, 8984.043164561499),
		('50kmE495N295', 6, 241014.81203849273),
		('NFR16', 5, 290841.31958573376),
		('NFR16', 6, 491409.67989523703),
		('NFR27', 5, 57513.706827347625),
		('NFR27', 6, 11285.21108565751),
		('NFR28', 5, 95052.11499810671),
		('NFR28', 6, 73127.83975673899),
		('NFR29', 5, 115778.32660521219),
		('NFR29', 6, 155693.68674527633),
		('NFR30', 5, 89214.27736053856),
		('NFR30', 6, 68700.16304285811),
		('NFR31', 5, 90924.9331329999),
		('NFR31', 6, 192497.08269696802),
		('NFR32', 5, 8925.817320343464),
		('NFR32', 6, 58874.385426970424),
		('NFR33', 5, 124372.15738734085),
		('NFR33', 6, 236832.39374873432),
		('NFR34', 5, 14693.76362858637),
		('NFR34', 6, 159634.89275465423),
		('NFR35', 5, 46478.11274256501),
		('NFR35', 6, 264356.5273689169),
		('NFR36', 5, 42696.77784460379),
		('NFR36', 6, 82212.05668211885),
		('NFR37', 5, 8687.790167100748),
		('NFR37', 6, 35636.049048886794),
		('NFR38', 5, 67396.2731508777),
		('NFR38', 6, 98594.07901172162),
		('NFR39', 5, 39249.17121588118),
		('NFR39', 6, 138034.7187905061),
		('NFR40', 5, 67566.815560177),
		('NFR40', 6, 14904.16266335483),
		('NFR41', 5, 80049.5645403652),
		('NFR41', 6, 52367.29589896272),
		('NFRD11', 5, 277269.96575101005),
		('NFRD11', 6, 298981.1230146436),
		('NFRD12', 5, 304511.36788087874),
		('NFRD12', 6, 498029.63948856224),
		('NFRD13', 5, 61171.87637115135),
		('NFRD13', 6, 423991.42012357194),
		('NFRD14', 5, 305646.39247900556),
		('NFRD14', 6, 421748.3620955493),
		('NFRD7', 5, 290841.31958573416),
		('NFRD7', 6, 491409.6798952366)
)
insert into nfiesta.t_aux_total (estimation_cell, variable, aux_total)
select
	c_estimation_cell.id, w_data.variable, w_data.aux_total
from w_data
inner join nfiesta.c_estimation_cell ON c_estimation_cell.label = w_data.estimation_cell__label
;

--
-- Data for Name: t_model_variables; Type: TABLE DATA; Schema: nfiesta; Owner: vagrant
--

COPY nfiesta.t_model_variables  FROM stdin;
1	5	1
2	6	1
\.

--
-- Data for Name: t_panel2aux_conf; Type: TABLE DATA; Schema: nfiesta; Owner: vagrant
--

/*
select
	format ('(''%s'', ''%s'', %s, ''%s''::boolean),', t_panel.panel, f_a_param_area.param_area_code, t_aux_conf.model, t_aux_conf.sigma)
from nfiesta.t_panel2aux_conf
inner join nfiesta.t_aux_conf ON t_aux_conf.id = t_panel2aux_conf.aux_conf
inner join nfiesta.f_a_param_area ON (t_aux_conf.param_area = f_a_param_area.gid)
inner join sdesign.t_panel ON t_panel.id = t_panel2aux_conf.panel;
*/

-- Data for Name: t_aux_conf; Type: TABLE DATA; Schema: nfiesta; Owner: vagrant
--
--insert into nfiesta.t_aux_conf( param_area, model, description, sigma)
--select
--	gid,
--	1 as model,
--	concat(param_area_code, ' - ', 'forest from OLIL') as description,
--	false as sigma
--from nfiesta.f_a_param_area
--where gid != 2 order by gid;


-- add temporary column
alter table nfiesta.c_panel_refyearset_group add column panels integer[];
-- query
with w_data (t_panel__panel, f_a_param_area__param_area_code, t_aux_conf__model, t_aux_conf__sigma) as (
	values
		('NFRD14- 1plot, s2a', 'NFR36', 1, 'f'::boolean),
		('NFRD14- 1plot, s2a', 'NFR37', 1, 'f'::boolean),
		('NFRD14- 1plot, s2a', 'NFR38', 1, 'f'::boolean),
		('NFRD14- 1plot, s2a', 'NFR39', 1, 'f'::boolean),
		('NFRD14- 1plot, s2a', 'NFR40', 1, 'f'::boolean),
		('NFRD14- 1plot, s2a', 'NFR41', 1, 'f'::boolean),
		('NFRD14- 1plot, s2a', '50kmE480N290', 1, 'f'::boolean),
		('NFRD14- 1plot, s2a', '50kmE485N285', 1, 'f'::boolean),
		('NFRD14- 1plot, s2a', '50kmE485N290', 1, 'f'::boolean),
		('NFRD14- 1plot, s2a', '50kmE485N295', 1, 'f'::boolean),
		('NFRD14- 1plot, s2a', '50kmE490N290', 1, 'f'::boolean),
		('NFRD14- 1plot, s2a', '50kmE490N295', 1, 'f'::boolean),
		('NFRD14- 1plot, s2a', '50kmE490N300', 1, 'f'::boolean),
		('NFRD14- 1plot, s2a', '50kmE495N295', 1, 'f'::boolean),
		('NFRD14- 1plot, s2a', 'NFRD14', 1, 'f'::boolean),
		('NFRD14- 1plot, s2a', 'MS', 1, 'f'::boolean),
		('NFRD11- 1plot, s2a', 'NFR27', 1, 'f'::boolean),
		('NFRD11- 1plot, s2a', 'NFR28', 1, 'f'::boolean),
		('NFRD11- 1plot, s2a', 'NFR29', 1, 'f'::boolean),
		('NFRD11- 1plot, s2a', 'NFR32', 1, 'f'::boolean),
		('NFRD11- 1plot, s2a', '50kmE480N295', 1, 'f'::boolean),
		('NFRD11- 1plot, s2a', '50kmE480N300', 1, 'f'::boolean),
		('NFRD11- 1plot, s2a', '50kmE480N305', 1, 'f'::boolean),
		('NFRD11- 1plot, s2a', '50kmE485N295', 1, 'f'::boolean),
		('NFRD11- 1plot, s2a', '50kmE485N300', 1, 'f'::boolean),
		('NFRD11- 1plot, s2a', '50kmE485N305', 1, 'f'::boolean),
		('NFRD11- 1plot, s2a', '50kmE490N295', 1, 'f'::boolean),
		('NFRD11- 1plot, s2a', '50kmE490N300', 1, 'f'::boolean),
		('NFRD11- 1plot, s2a', 'NFRD11', 1, 'f'::boolean),
		('NFRD11- 1plot, s2a', 'MS', 1, 'f'::boolean),
		('NFRD13- 1plot, s2a', 'NFR34', 1, 'f'::boolean),
		('NFRD13- 1plot, s2a', 'NFR35', 1, 'f'::boolean),
		('NFRD13- 1plot, s2a', '50kmE475N285', 1, 'f'::boolean),
		('NFRD13- 1plot, s2a', '50kmE475N290', 1, 'f'::boolean),
		('NFRD13- 1plot, s2a', '50kmE480N285', 1, 'f'::boolean),
		('NFRD13- 1plot, s2a', '50kmE480N290', 1, 'f'::boolean),
		('NFRD13- 1plot, s2a', '50kmE480N295', 1, 'f'::boolean),
		('NFRD13- 1plot, s2a', '50kmE480N300', 1, 'f'::boolean),
		('NFRD13- 1plot, s2a', '50kmE485N285', 1, 'f'::boolean),
		('NFRD13- 1plot, s2a', '50kmE485N290', 1, 'f'::boolean),
		('NFRD13- 1plot, s2a', '50kmE485N295', 1, 'f'::boolean),
		('NFRD13- 1plot, s2a', 'NFRD13', 1, 'f'::boolean),
		('NFRD13- 1plot, s2a', 'MS', 1, 'f'::boolean),
		('NFRD7- 2plots', 'NFR16', 1, 'f'::boolean),
		('NFRD12- 2plots', 'NFR31', 1, 'f'::boolean),
		('NFRD7- 2plots', '50kmE460N290', 1, 'f'::boolean),
		('NFRD12- 2plots', '50kmE470N285', 1, 'f'::boolean),
--		('NFRD12- 2plots', 'NFR35', 1, 'f'::boolean),
--		('NFRD12- 2plots', 'NFRD13', 1, 'f'::boolean),
		('NFRD7- 2plots', '50kmE465N285', 1, 'f'::boolean),
		('NFRD12- 2plots', '50kmE475N300', 1, 'f'::boolean),
		('NFRD7- 2plots', 'CM border', 1, 'f'::boolean),
		('NFRD7- 2plots', 'NFRD7', 1, 'f'::boolean),
		('NFRD12- 2plots', '50kmE480N290', 1, 'f'::boolean),
		('NFRD12- 2plots', 'NFR33', 1, 'f'::boolean),
		('NFRD12- 2plots', 'CM border', 1, 'f'::boolean),
		('NFRD12- 2plots', '50kmE470N290', 1, 'f'::boolean),
--		('NFRD12- 2plots', 'NFR34', 1, 'f'::boolean),
		('NFRD7- 2plots', '50kmE470N285', 1, 'f'::boolean),
		('NFRD12- 2plots', '50kmE470N300', 1, 'f'::boolean),
		('NFRD7- 2plots', '50kmE475N290', 1, 'f'::boolean),
		('NFRD7- 2plots', '50kmE465N295', 1, 'f'::boolean),
		('NFRD12- 2plots', 'NFR30', 1, 'f'::boolean),
--		('NFRD12- 2plots', 'MS', 1, 'f'::boolean),
--		('NFRD12- 2plots', 'NFR16', 1, 'f'::boolean),
		('NFRD12- 2plots', '50kmE475N295', 1, 'f'::boolean),
--		('NFRD7- 2plots', 'NFRD12', 1, 'f'::boolean),
		('NFRD12- 2plots', '50kmE480N300', 1, 'f'::boolean),
		('NFRD7- 2plots', '50kmE465N290', 1, 'f'::boolean),
		('NFRD7- 2plots', '50kmE475N295', 1, 'f'::boolean),
		('NFRD12- 2plots', '50kmE470N295', 1, 'f'::boolean),
		('NFRD7- 2plots', '50kmE470N290', 1, 'f'::boolean),
--		('NFRD12- 2plots', 'NFRD7', 1, 'f'::boolean),
--		('NFRD7- 2plots', 'NFR31', 1, 'f'::boolean),
		('NFRD7- 2plots', '50kmE470N295', 1, 'f'::boolean),
		('NFRD12- 2plots', '50kmE475N290', 1, 'f'::boolean),
		('NFRD12- 2plots', '50kmE475N285', 1, 'f'::boolean),
		('NFRD12- 2plots', '50kmE480N295', 1, 'f'::boolean),
--		('NFRD7- 2plots', 'NFR33', 1, 'f'::boolean),
		('NFRD12- 2plots', 'NFRD12', 1, 'f'::boolean)
),
w_panels AS (
	SELECT
		array_agg(t1.id ORDER BY t1.id) AS panels,
		f_a_param_area__param_area_code AS param_area_code, t_aux_conf__model AS model, t_aux_conf__sigma AS sigma
	FROM sdesign.t_panel AS t1
	INNER JOIN w_data AS t2
	ON t1.panel = t2.t_panel__panel
	GROUP BY t2.f_a_param_area__param_area_code, t2.t_aux_conf__model, t2.t_aux_conf__sigma
),
w_ins_c AS (
		INSERT INTO nfiesta.c_panel_refyearset_group (label, description, panels)
		SELECT DISTINCT
			concat('no_of_panels: ', array_length(panels,1)::text,';',panels::text),
			concat('no_of_panels: ', array_length(panels,1)::text,';',panels::text),
			panels
		FROM w_panels
		RETURNING id, panels
),
w_ins_t AS (
	INSERT INTO nfiesta.t_panel_refyearset_group(panel_refyearset_group, panel, reference_year_set)
	SELECT
		t1.id, t2.panel, NULL
	FROM
		w_ins_c AS t1,
		unnest(t1.panels) AS t2(panel)
)
-- param_area_code, model, sigma
INSERT INTO nfiesta.t_aux_conf( param_area, model, description, sigma, panel_refyearset_group)
SELECT
	t2.gid,
	t1.model,
	concat(t2.param_area_code, ' - ', 'forest from OLIL') AS description,
	t1.sigma,
	t3.id
FROM w_panels AS t1
INNER JOIN nfiesta.f_a_param_area AS t2
ON t2.param_area_code = t1.param_area_code
INNER JOIN w_ins_c AS t3
ON t1.panels = t3.panels
ORDER BY gid;

ALTER TABLE nfiesta.c_panel_refyearset_group DROP COLUMN panels;

--
-- Name: c_param_area_type_id_seq; Type: SEQUENCE SET; Schema: nfiesta; Owner: vagrant
--

SELECT pg_catalog.setval('nfiesta.c_param_area_type_id_seq', (select max(id) from nfiesta.c_param_area_type), true);


--
-- Name: cm_cell2param_area_mapping_id_seq; Type: SEQUENCE SET; Schema: nfiesta; Owner: vagrant
--

SELECT pg_catalog.setval('nfiesta.cm_cell2param_area_mapping_id_seq', (select max(id) from nfiesta.cm_cell2param_area_mapping), true);


--
-- Name: cm_plot2param_area_mapping_id_seq; Type: SEQUENCE SET; Schema: nfiesta; Owner: vagrant
--

SELECT pg_catalog.setval('nfiesta.cm_plot2param_area_mapping_id_seq', (select max(id) from nfiesta.cm_plot2param_area_mapping), true);


--
-- Name: f_a_cell_gid_seq; Type: SEQUENCE SET; Schema: nfiesta; Owner: vagrant
--

SELECT pg_catalog.setval('nfiesta.f_a_cell_gid_seq', (select max(gid) from nfiesta.f_a_cell), true);


--
-- Name: f_a_param_area_gid_seq; Type: SEQUENCE SET; Schema: nfiesta; Owner: vagrant
--

SELECT pg_catalog.setval('nfiesta.f_a_param_area_gid_seq', (select max(gid) from nfiesta.f_a_param_area), true);


--
-- Name: t_aux_conf_id_seq; Type: SEQUENCE SET; Schema: nfiesta; Owner: vagrant
--

SELECT pg_catalog.setval('nfiesta.t_aux_conf_id_seq', (select max(id) from nfiesta.t_aux_conf), true);


--
-- Name: t_aux_total_id_seq; Type: SEQUENCE SET; Schema: nfiesta; Owner: vagrant
--

SELECT pg_catalog.setval('nfiesta.t_aux_total_id_seq', (select max(id) from nfiesta.t_aux_total), true);


--
-- Name: t_model_id_seq; Type: SEQUENCE SET; Schema: nfiesta; Owner: vagrant
--

SELECT pg_catalog.setval('nfiesta.t_model_id_seq', (select max(id) from nfiesta.t_model), true);


--
-- Name: t_model_variables_id_seq; Type: SEQUENCE SET; Schema: nfiesta; Owner: vagrant
--

SELECT pg_catalog.setval('nfiesta.t_model_variables_id_seq', (select max(id) from nfiesta.t_model_variables), true);

--
-- Name: t_panel_refyearset_group_id_seq; Type: SEQUENCE SET; Schema: nfiesta; Owner: vagrant
--

SELECT pg_catalog.setval('nfiesta.c_panel_refyearset_group_id_seq', (select max(id) from nfiesta.c_panel_refyearset_group), true);
SELECT pg_catalog.setval('nfiesta.t_panel_refyearset_group_id_seq', (select max(id) from nfiesta.t_panel_refyearset_group), true);

--
-- PostgreSQL database dump complete
--

