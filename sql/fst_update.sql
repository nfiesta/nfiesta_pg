--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

/*
here it is possible to test "data migration" -- "update with data" / "on production" scenario, when updatescript is handling also update / migration of data
when this file is empty, all updatescripts (up to version spacified in nfiesta.congrol file) are applied to empty DB (no testing data) during install test before testing data are loaded
so data migration (opdates etc.) runs on 0 records
*/

--alter extension nfiesta update to "X.Y.Z";
