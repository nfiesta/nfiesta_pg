insert into nfiesta.t_result
	(estimate_conf, point, var, calc_started, extension_version, calc_duration, sampling_units, min_ssize, act_ssize, is_latest)
select
	estimate_conf, point, var, calc_started, extension_version, calc_duration, sampling_units, min_ssize, act_ssize, is_latest
from nfiesta.t_result
order by estimate_conf, is_latest, calc_started
limit 5;

update nfiesta.t_result set is_latest = false
where t_result.id in (
	select t_result.id from nfiesta.t_result
	inner join nfiesta.t_estimate_conf ON t_estimate_conf.id = t_result.estimate_conf
	inner join nfiesta.t_total_estimate_conf ON t_total_estimate_conf.id = t_estimate_conf.total_estimate_conf
	where t_total_estimate_conf.estimation_cell = 27
);

insert into nfiesta.t_result
	(estimate_conf, point, var, calc_started, extension_version, calc_duration, sampling_units, min_ssize, act_ssize, is_latest)
select
	estimate_conf, point, var, calc_started, extension_version, calc_duration, sampling_units, min_ssize, act_ssize, true as is_latest
from nfiesta.t_result AS t1
inner join nfiesta.t_estimate_conf AS t2
on t1.estimate_conf = t2.id
inner join nfiesta.t_total_estimate_conf AS t3
on t2.total_estimate_conf = t3.id
inner join nfiesta.c_estimation_cell AS t4
on t3.estimation_cell = t4.id
where t3.estimation_cell = 27
order by t3.variable, t4.label, t3.aux_conf
;

update nfiesta.t_result set point = point + 16.16
where t_result.id in (
	select t_result.id from nfiesta.t_result
	inner join nfiesta.t_estimate_conf ON t_estimate_conf.id = t_result.estimate_conf
	inner join nfiesta.t_total_estimate_conf ON t_total_estimate_conf.id = t_estimate_conf.total_estimate_conf
	where t_total_estimate_conf.estimation_cell = 27
)
and not is_latest;

select
	estimate_conf, point, var, sampling_units, min_ssize, act_ssize, is_latest
from nfiesta.t_result AS t1
inner join nfiesta.t_estimate_conf AS t2
on t1.estimate_conf = t2.id
inner join nfiesta.t_total_estimate_conf AS t3
on t2.total_estimate_conf = t3.id
inner join nfiesta.c_estimation_cell AS t4
on t3.estimation_cell = t4.id
where t3.estimation_cell = 27
order by t3.variable, t4.label, t3.aux_conf, is_latest, calc_started;
