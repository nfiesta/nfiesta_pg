--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
with w_conf as (
		select * from nfiesta.v_conf_overview where estimate_type_str = '1p2p_ratio'
			and sigma = False
)
, w_conf_info as (
		select row_number() over() as i, estimate_conf, total_estimate_conf, total_estimate_conf__denom, (select count(*) as cnt from w_conf) from w_conf
)
, w_res as (
	select
		estimate_conf, clock_timestamp() as t_start,
		(select (point2p, var2p, est_info, min_ssize, act_ssize)::nfiesta.estimate_result from nfiesta.fn_1p2p_ratio_var(total_estimate_conf, total_estimate_conf__denom)) as res,
		clock_timestamp() as t_stop, version
	from w_conf_info
	, (select version from pg_available_extension_versions where name = 'nfiesta' and installed) as extension_version
	--where nfiesta.fn_raise_notice(concat('computing estimate, configuration: '::text, estimate_conf::text, ', ',i::text,' / ', cnt::text)) --PROGRESS MONITORING	
)
insert into nfiesta.t_result (estimate_conf, point, var, extension_version, calc_started, calc_duration, min_ssize, act_ssize, sampling_units)
select estimate_conf, (res).point, (res).var, version, t_start as calc_started, t_stop - t_start as calc_duration, (res).min_ssize, (res).act_ssize, (res).est_info
from w_res
;
with w_conf as (
		select * from nfiesta.v_conf_overview where estimate_type_str = '2p1p_ratio'
			and sigma = False
)
, w_conf_info as (
		select row_number() over() as i, estimate_conf, total_estimate_conf, total_estimate_conf__denom, (select count(*) as cnt from w_conf) from w_conf
)
, w_res as (
	select
		estimate_conf, clock_timestamp() as t_start,
		(select (point2p, var2p, est_info, min_ssize, act_ssize)::nfiesta.estimate_result from nfiesta.fn_2p1p_ratio_var(total_estimate_conf, total_estimate_conf__denom)) as res,
		clock_timestamp() as t_stop, version
	from w_conf_info
	, (select version from pg_available_extension_versions where name = 'nfiesta' and installed) as extension_version
	--where nfiesta.fn_raise_notice(concat('computing estimate, configuration: '::text, estimate_conf::text, ', ',i::text,' / ', cnt::text)) --PROGRESS MONITORING	
)
insert into nfiesta.t_result (estimate_conf, point, var, extension_version, calc_started, calc_duration, min_ssize, act_ssize, sampling_units)
select estimate_conf, (res).point, (res).var, version, t_start as calc_started, t_stop - t_start as calc_duration, (res).min_ssize, (res).act_ssize, (res).est_info
from w_res
;
with w_conf as (
		select * from nfiesta.v_conf_overview where estimate_type_str = '2p2p_ratio'
			and sigma = False
)
, w_conf_info as (
		select row_number() over() as i, estimate_conf, total_estimate_conf, total_estimate_conf__denom, (select count(*) as cnt from w_conf) from w_conf
)
, w_res as (
	select
		estimate_conf, clock_timestamp() as t_start,
		(select (point2p, var2p, est_info, min_ssize, act_ssize)::nfiesta.estimate_result from nfiesta.fn_2p2p_ratio_var(total_estimate_conf, total_estimate_conf__denom)) as res,
		clock_timestamp() as t_stop, version
	from w_conf_info
	, (select version from pg_available_extension_versions where name = 'nfiesta' and installed) as extension_version
	--where nfiesta.fn_raise_notice(concat('computing estimate, configuration: '::text, estimate_conf::text, ', ',i::text,' / ', cnt::text)) --PROGRESS MONITORING	
)
insert into nfiesta.t_result (estimate_conf, point, var, extension_version, calc_started, calc_duration, min_ssize, act_ssize, sampling_units)
select estimate_conf, (res).point, (res).var, version, t_start as calc_started, t_stop - t_start as calc_duration, (res).min_ssize, (res).act_ssize, (res).est_info
from w_res
;
select
	v_conf_overview.estimate_type_str,
	v_conf_overview.estimation_cell_label,
	v_conf_overview.target_variable_label,
	v_conf_overview.area_domain_category_label,
	v_conf_overview.sub_population_category_label,
	v_conf_overview.param_area_code,
	v_conf_overview.model_description,
	v_conf_overview.force_synthetic,
	denom.force_synthetic as force_synthetic__denom,
	round(point::numeric, 10) as point, round(var::numeric, 10) as var, round(min_ssize::numeric, 8) as min_ssize, act_ssize, sampling_units
from nfiesta.t_result
inner join nfiesta.v_conf_overview on (t_result.estimate_conf = v_conf_overview.estimate_conf)
inner join nfiesta.t_total_estimate_conf as denom on (v_conf_overview.total_estimate_conf__denom = denom.id )
where v_conf_overview.estimate_type_str in ('1p2p_ratio', '2p1p_ratio', '2p2p_ratio')
	and v_conf_overview.sigma = False
order by
	v_conf_overview.estimate_type_str,
	v_conf_overview.estimation_cell_label,
	v_conf_overview.target_variable_label,
	v_conf_overview.area_domain_category_label,
	v_conf_overview.sub_population_category_label,
	v_conf_overview.param_area_code,
	v_conf_overview.model_description,
	v_conf_overview.force_synthetic,
	denom.force_synthetic
;
