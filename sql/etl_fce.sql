--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
-- DATA DISCLAIMER
-- Any results produced on the basis of openly published Czech National Forest Inventory (CZNFI) sample data
-- do not reflect the true status or changes within any geographical area of the Czech Republic,
-- at, during or between any time occasion(s).
-- In particular, any such results must not be presented or interpreted as an alternative to any information published by CZNFI,
-- be it a past or future CZNFI publication.
--

-- check_TARGET_VARIABLE (input argument etl_id int[] is default NULL::int[])
select * from nfiesta.fn_etl_check_target_variable
(
json_build_object
	(
	'cs',json_build_object
			(
			'indikator',json_build_object('label','number of merch stems','description','Number of merchantable wood stems.'),
			'stav nebo zmena',json_build_object('label','state','description','State.'),
			'jednotka vystupu',json_build_object('label','pc.','description','Piece.'),
			'etl_join_id',4
			),
	'en',json_build_object
			(
			'indicator',json_build_object('label','number of merch stems','description','Number of merchantable wood stems.'),
			'stav nebo zmena',json_build_object('label','state','description','State.'),
			'jednotka vystupu',json_build_object('label','pc.','description','Piece.'),
			'etl_join_id',4
			)
	)
);


-- check_TARGET_VARIABLE (input argument etl_id int[] is not null)
select * from nfiesta.fn_etl_check_target_variable
(
json_build_object
	(
	'cs',json_build_object
			(
			'indikator',json_build_object('label','number of merch stems','description','Number of merchantable wood stems.'),
			'stav nebo zmena',json_build_object('label','state','description','State.'),
			'jednotka vystupu',json_build_object('label','pc.','description','Piece.'),
			'etl_join_id',4
			),
	'en',json_build_object
			(
			'indicator',json_build_object('label','number of merch stems','description','Number of merchantable wood stems.'),
			'stav nebo zmena',json_build_object('label','state','description','State.'),
			'jednotka vystupu',json_build_object('label','pc.','description','Piece.'),
			'etl_join_id',4
			)
	),
	array[5]
);


-- get_TARGET_VARIABLE (input argument etl_id int[] is default NULL::int[])
select id, jsonb_pretty(metadata::jsonb) as metadata from nfiesta.fn_etl_get_target_variable('cs');
select id, jsonb_pretty(metadata::jsonb) as metadata from nfiesta.fn_etl_get_target_variable('en');


-- get_TARGET_VARIABLE (input argument etl_id int[] is not null)
select id, jsonb_pretty(metadata::jsonb) as metadata from nfiesta.fn_etl_get_target_variable('cs', array[3]);
select id, jsonb_pretty(metadata::jsonb) as metadata from nfiesta.fn_etl_get_target_variable('en', array[3]);


-- import_TARGET_VARIABLE
select * from nfiesta.fn_etl_import_target_variable
(
json_build_object
	(
	'cs',json_build_object
			(
			'indikator',json_build_object('label','number of merch stems','description','Number of merchantable wood stems.'),
			'stav nebo zmena',json_build_object('label','state','description','State.'),
			'jednotka vystupu',json_build_object('label','pc.','description','Piece.'),
			'etl_join_id',50
			),
	'en',json_build_object
			(
			'indicator',json_build_object('label','number of merch stems','description','Number of merchantable wood stems.'),
			'stav nebo zmena',json_build_object('label','state','description','State.'),
			'jednotka vystupu',json_build_object('label','pc.','description','Piece.'),
			'etl_join_id',50
			)
	)
);


-- import_TARGET_VARIABLE_METADATA -- FI
select * from nfiesta.fn_etl_import_target_variable_metadata
(
5,
json_build_object
	(
	'fi',json_build_object
			(
			'indikator',json_build_object('label','number of merch stems','description','Number of merchantable wood stems.'),
			'stav nebo zmena',json_build_object('label','state','description','State.'),
			'jednotka vystupu',json_build_object('label','pc.','description','Piece.'),
			'etl_join_id',50
			),
	'en',json_build_object
			(
			'indicator',json_build_object('label','number of merch stems','description','Number of merchantable wood stems.'),
			'stav nebo zmena',json_build_object('label','state','description','State.'),
			'jednotka vystupu',json_build_object('label','pc.','description','Piece.'),
			'etl_join_id',50
			)
	)
);


-- import_TARGET_VARIABLE_METADATA -- CS
select * from nfiesta.fn_etl_import_target_variable_metadata
(
5,
json_build_object
	(
	'cs',json_build_object
			(
			'indikator',json_build_object('label','number of merch stems','description','Number of merchantable wood stems.'),
			'stav nebo zmena',json_build_object('label','state','description','State.'),
			'jednotka vystupu',json_build_object('label','pc.','description','Piece.'),
			'etl_join_id',50
			),
	'en',json_build_object
			(
			'indicator',json_build_object('label','number of merch stems','description','Number of merchantable wood stems.'),
			'stav nebo zmena',json_build_object('label','state','description','State.'),
			'jednotka vystupu',json_build_object('label','pc.','description','Piece.'),
			'etl_join_id',50
			)
	)
);


-- check_c_target_variable
select id, metadata from nfiesta.c_target_variable order by id;


-- check_TARGET_VARIABLE (input argument etl_id int[] is default NULL::int[])
select * from nfiesta.fn_etl_check_target_variable
(
json_build_object
	(
	'cs',json_build_object
			(
			'indikator',json_build_object('label','number of merch stems','description','Number of merchantable wood stems.'),
			'stav nebo zmena',json_build_object('label','state','description','State.'),
			'jednotka vystupu',json_build_object('label','pc.','description','Piece.'),
			'etl_join_id',50
			),
	'en',json_build_object
			(
			'indicator',json_build_object('label','number of merch stems','description','Number of merchantable wood stems.'),
			'stav nebo zmena',json_build_object('label','state','description','State.'),
			'jednotka vystupu',json_build_object('label','pc.','description','Piece.'),
			'etl_join_id',50
			)
	)
);


-- check_TARGET_VARIABLE (input argument etl_id int[] is not null)
select * from nfiesta.fn_etl_check_target_variable
(
json_build_object
	(
	'cs',json_build_object
			(
			'indikator',json_build_object('label','number of merch stems','description','Number of merchantable wood stems.'),
			'stav nebo zmena',json_build_object('label','state','description','State.'),
			'jednotka vystupu',json_build_object('label','pc.','description','Piece.'),
			'etl_join_id',50
			),
	'en',json_build_object
			(
			'indicator',json_build_object('label','number of merch stems','description','Number of merchantable wood stems.'),
			'stav nebo zmena',json_build_object('label','state','description','State.'),
			'jednotka vystupu',json_build_object('label','pc.','description','Piece.'),
			'etl_join_id',50
			)
	),
	array[5]
);


-- check_VARIABLES => etl process is stopped
with
w1 as	(
		select 'NFRD11- 1plot, s2a' as panel, '2011 - 2015' as reference_year_set union all
		select 'NFRD13- 1plot, s2a' as panel, '2011 - 2015' as reference_year_set union all
		select 'NFRD14- 1plot, s2a' as panel, '2011 - 2015' as reference_year_set
		)
,w2 as	(
		select
				0::integer as vad_id, 0::integer as vsp_id, 0::integer as vadc_id, 0::integer as vspc_id, true as check_variables
		)
,w3 as	(
		select w1.*, w2.* from w1,w2
		)
,w4 as	(
		select
				1 as id,
				json_agg(json_build_object
						(
						'panel',w3.panel,
						'reference_year_set', w3.reference_year_set,
						'area_domain_etl_id', w3.vad_id,
						'sub_population_etl_id', w3.vsp_id,
						'area_domain_category_etl_id', w3.vadc_id,
						'sub_population_category_etl_id', w3.vspc_id,
						'check_variables',w3.check_variables
						)
						) as variables
		from w3	
		)
,w5 as	(
		select
				1 as id,
				'CZ' as country,
				1 as target_variable
		)
,w6 as	(
		select w5.*, w4.variables from w5 inner join w4 on w5.id = w4.id
		)
,w7 as	(
		select json_build_object
			(								
			'country',w6.country,
			'target_variable',w6.target_variable,								
			'variables',w6.variables
			) as res from w6
		)
select jsonb_pretty((nfiesta.fn_etl_check_variables(w7.res))::jsonb) from w7;
	

-- check_VARIABLES => ETL process continues
with
w1 as	(
		select 'NFRD11- 1plot, s2a' as panel, '2011 - 2015' as reference_year_set union all
		select 'NFRD13- 1plot, s2a' as panel, '2011 - 2015' as reference_year_set union all
		select 'NFRD14- 1plot, s2a' as panel, '2011 - 2015' as reference_year_set
		)
,w2 as	(
		select
				0::integer as vad_id, 0::integer as vsp_id, 0::integer as vadc_id, 0::integer as vspc_id, true as check_variables
			union all
		select
				1::integer as vad_id, 0::integer as vsp_id, 1::integer as vadc_id, 0::integer as vspc_id, true as check_variables
			union all
		select
				1::integer as vad_id, 0::integer as vsp_id, 2::integer as vadc_id, 0::integer as vspc_id, true as check_variables
		)
,w3 as	(
		select w1.*, w2.* from w1,w2
		)
,w4 as	(
		select
				1 as id,
				json_agg(json_build_object
						(
						'panel',w3.panel,
						'reference_year_set', w3.reference_year_set,
						'area_domain_etl_id', w3.vad_id,
						'sub_population_etl_id', w3.vsp_id,
						'area_domain_category_etl_id', w3.vadc_id,
						'sub_population_category_etl_id', w3.vspc_id,
						'check_variables',w3.check_variables
						)
						) as variables
		from w3	
		)
,w5 as	(
		select
				1 as id,
				'CZ' as country,
				1 as target_variable
		)
,w6 as	(
		select w5.*, w4.variables from w5 inner join w4 on w5.id = w4.id
		)
,w7 as	(
		select json_build_object
			(								
			'country',w6.country,
			'target_variable',w6.target_variable,								
			'variables',w6.variables
			) as res from w6
		)
select jsonb_pretty((nfiesta.fn_etl_check_variables(w7.res))::jsonb) from w7;


-- get_AREA_DOMAINS
select id, etl_id, label, description, label_en, description_en, atomic from nfiesta.fn_etl_get_area_domains(1);
	

-- check_AREA_DOMAIN
select id, etl_id, label, description, label_en, description_en, atomic from nfiesta.fn_etl_check_area_domain(10,'land registry');
select id, etl_id, label, description, label_en, description_en, atomic from nfiesta.fn_etl_check_area_domain(10,'land_registry');

	
-- import_AREA_DOMAIN
select * from nfiesta.fn_etl_import_area_domain
	(
	11,
	'land tenure (4 categories)'::varchar,
	'The land tenure (according to the Land Use Registry) aggregated into 4 categories.'::text,
	'land tenure (4 categories)'::varchar,
	'The land tenure (according to the Land Use Registry) aggregated into 4 categories.'::text,
	true
	);


-- check_c_area_domain
select id, label, description, label_en, description_en, atomic from nfiesta.c_area_domain order by id;


-- check_AREA_DOMAIN_CATEGORY
select id, etl_id from nfiesta.fn_etl_check_area_domain_category(11,1,'non-forest');
select id, etl_id from nfiesta.fn_etl_check_area_domain_category(11,1,'non_forest');


-- get_AREA_DOMAIN_CATEGORIES
select id, etl_id, label, description, label_en, description_en from nfiesta.fn_etl_get_area_domain_categories(1,1);


-- import_AREA_DOMAIN_CATEGORY => I. category
select * from nfiesta.fn_etl_import_area_domain_category(21,2,'state'::varchar,'State.'::text,'state'::varchar,'State.'::text);


-- import_AREA_DOMAIN_CATEGORY => II. category
select * from nfiesta.fn_etl_import_area_domain_category(22,2,'municipal'::varchar,'Municipal.'::text,'municipal'::varchar,'Municipal.'::text);


-- import_AREA_DOMAIN_CATEGORY => III. category
select * from nfiesta.fn_etl_import_area_domain_category(23,2,'private and ecclesiastical'::varchar,'Private and ecclesiastical.'::text,'private and ecclesiastical'::varchar,'Private and ecclesiastical.'::text);


-- import_AREA_DOMAIN_CATEGORY => IV. category
select * from nfiesta.fn_etl_import_area_domain_category(24,2,'not identified'::varchar,'Not identified.'::text,'not identified'::varchar,'Not identified.'::text);


-- check_c_area_domain_category
select id, area_domain, label, description, label_en, description_en from nfiesta.c_area_domain_category order by id;


-- get_SUB_POPULATIONS
select id, etl_id, label, description, label_en, description_en, atomic from nfiesta.fn_etl_get_sub_populations(1);


-- check_SUB_POPULATION
select id, etl_id, label, description, label_en, description_en, atomic from nfiesta.fn_etl_check_sub_population(20,'coniferous or deciduous');
select id, etl_id, label, description, label_en, description_en, atomic from nfiesta.fn_etl_check_sub_population(20,'coniferous_or_deciduous');


-- import_SUB_POPULATION
select * from nfiesta.fn_etl_import_sub_population
	(
	11,
	'group of 14 woody species (according to Annex to Decree 84/1996 Coll.)'::varchar,
	'The group of 14 woody species (according to Annex to Decree 84/1996 Coll.).'::text,
	'group of 14 woody species (according to Annex to Decree 84/1996 Coll.)'::varchar,
	'The group of 14 woody species (according to Annex to Decree 84/1996 Coll.).'::text,
	true
	);


-- check_c_sub_population
select id, label, description, label_en, description_en, atomic from nfiesta.c_sub_population order by id;


-- get_SUB_POPULATION_CATEGORIES
select id, etl_id, label, description, label_en, description_en from nfiesta.fn_etl_get_sub_population_categories(1,1);


-- check_SUB_POPULATION_CATEGORY
select id, etl_id, label, description, label_en, description_en from nfiesta.fn_etl_check_sub_population_category(11,1,'deciduous');
select id, etl_id, label, description, label_en, description_en from nfiesta.fn_etl_check_sub_population_category(11,1,'Deciduous');


-- import_SUB_POPULATION_CATEGORY => I. category
select * from nfiesta.fn_etl_import_sub_population_category(1,2,'norway spruce'::varchar,'Norway spruce.'::text,'norway spruce'::varchar,'Norway spruce.'::text);


-- import_SUB_POPULATION_CATEGORY => II. category
select * from nfiesta.fn_etl_import_sub_population_category(2,2,'european silver fir'::varchar,'European silver fir.'::text,'european silver fir'::varchar,'European silver fir.'::text);


-- import_SUB_POPULATION_CATEGORY => III. category
select * from nfiesta.fn_etl_import_sub_population_category(3,2,'scots pine'::varchar,'Scots pine.'::text,'scots pine'::varchar,'Scots pine.'::text);


-- import_SUB_POPULATION_CATEGORY => IV. category
select * from nfiesta.fn_etl_import_sub_population_category(4,2,'european larch'::varchar,'European larch.'::text,'european larch'::varchar,'European larch.'::text);


-- import_SUB_POPULATION_CATEGORY => V. category
select * from nfiesta.fn_etl_import_sub_population_category(5,2,'other coniferous'::varchar,'Other coniferous.'::text,'other coniferous'::varchar,'Other coniferous.'::text);


-- import_SUB_POPULATION_CATEGORY => VI. category
select * from nfiesta.fn_etl_import_sub_population_category(6,2,'european beech'::varchar,'European beech.'::text,'european beech'::varchar,'European beech.'::text);


-- import_SUB_POPULATION_CATEGORY => VII. category
select * from nfiesta.fn_etl_import_sub_population_category(7,2,'oak species'::varchar,'Oak species.'::text,'oak species'::varchar,'Oak species.'::text);


-- import_SUB_POPULATION_CATEGORY => VIII. category
select * from nfiesta.fn_etl_import_sub_population_category(8,2,'uropean hornbeam'::varchar,'European hornbeam.'::text,'european hornbeam'::varchar,'European hornbeam.'::text);


-- import_SUB_POPULATION_CATEGORY => IX. category
select * from nfiesta.fn_etl_import_sub_population_category(9,2,'maple species'::varchar,'Maple species.'::text,'maple species'::varchar,'Maple species.'::text);


-- import_SUB_POPULATION_CATEGORY => X. category
select * from nfiesta.fn_etl_import_sub_population_category(10,2,'ash species'::varchar,'Ash species.'::text,'ash species'::varchar,'Ash species.'::text);


-- import_SUB_POPULATION_CATEGORY => XI. category
select * from nfiesta.fn_etl_import_sub_population_category(11,2,'alder species'::varchar,'Alder species.'::text,'alder species'::varchar,'Alder species.'::text);


-- import_SUB_POPULATION_CATEGORY => XII. category
select * from nfiesta.fn_etl_import_sub_population_category(12,2,'birch species'::varchar,'Birch species.'::text,'birch species'::varchar,'Birch species.'::text);


-- import_SUB_POPULATION_CATEGORY => XIII. category
select * from nfiesta.fn_etl_import_sub_population_category(13,2,'other hardwood broad'::varchar,'Other hardwood broad.'::text,'other hardwood broad'::varchar,'Other hardwood broad.'::text);


-- import_SUB_POPULATION_CATEGORY => XIV. category
select * from nfiesta.fn_etl_import_sub_population_category(14,2,'other softwood broad'::varchar,'Other softwood broad.'::text,'other softwood broad'::varchar,'Other softwood broad.'::text);


-- check_c_sub_population_category
select id, sub_population, label, description, label_en, description_en from nfiesta.c_sub_population_category order by id;


-- get_TOPICS
select * from nfiesta.fn_etl_get_topics();


-- save_TOPIC
select * from nfiesta.fn_etl_save_topic('forest resources (FE/C1)','Forest resources (FE/C1).','forest resources (FE/C1)','Forest resources (FE/C1).'); -- 3
select * from nfiesta.fn_etl_save_topic('forest resources (FE/C1) - test FOR update','Forest resources (FE/C1).','forest resources (FE/C1) - test FOR update','Forest resources (FE/C1).'); -- 4
select * from nfiesta.fn_etl_save_topic('forest resources (FE/C1) - test FOR DELETE','Forest resources (FE/C1).','forest resources (FE/C1) - test FOR DELETE','Forest resources (FE/C1).'); -- 5


-- get_TOPICS
select * from nfiesta.fn_etl_get_topics() order by id;


-- update_TOPIC
select * from nfiesta.fn_etl_update_topic(4,'forest resources (FE/C1) - test FOR UPDATE',null::text,'forest resources (FE/C1) - test FOR UPDATE',null::text);


-- get_TOPICS
select * from nfiesta.fn_etl_get_topics() order by id;


-- try_delete_TOPIC
select * from nfiesta.fn_etl_try_delete_topic(5);


-- delete_TOPIC
select * from nfiesta.fn_etl_delete_topic(5);


-- get_TOPICS
select * from nfiesta.fn_etl_get_topics() order by id;


-- get_TARGET_VARIABLE2TOPIC
select * from nfiesta.fn_etl_get_target_variable2topic(1);


-- save_TARGET_VARIABLE2TOPIC
select * from nfiesta.fn_etl_save_target_variable2topic(1,3);
select * from nfiesta.fn_etl_save_target_variable2topic(1,4);


-- get_TARGET_VARIABLE2TOPIC
select * from nfiesta.fn_etl_get_target_variable2topic(1);


-- delete_TARGET_VARIABLE2TOPIC
select * from nfiesta.fn_etl_delete_target_variable2topic(1,array[4]);


-- get_TARGET_VARIABLE2TOPIC
select * from nfiesta.fn_etl_get_target_variable2topic(1);


-- check_t_variable
select id, target_variable, sub_population_category, area_domain_category, auxiliary_variable_category from nfiesta.t_variable order by id;


-- import_VARIABLE
with
w1 as	(
		select 5 as target_variable, 0 as area_domain_category, 0 as sub_population_category	union all
		select 5 as target_variable, 0 as area_domain_category, 1 as sub_population_category	union all
		select 5 as target_variable, 0 as area_domain_category, 2 as sub_population_category
		),
w2 as	(
		select json_agg
				(json_build_object
							(
								'target_variable', w1.target_variable,
								'area_domain_category', w1.area_domain_category,
								'sub_population_category', w1.sub_population_category
							)
				) as res from w1
		)
select (nfiesta.fn_etl_import_variable(w2.res)) from w2;


-- check_t_variable
select id, target_variable, sub_population_category, area_domain_category, auxiliary_variable_category from nfiesta.t_variable order by id;


-- check_t_variable_hierarchy
select id, variable, variable_superior from nfiesta.t_variable_hierarchy order by id;


-- import_VARIABLE_HIERARCHY
with
w1 as	(
		select 
			5 as target_variable,
			'CZ' as country,
			'MS' as strata_set,
			'NFRD13' as stratum,
			'NFRD13- 1plot, s2a' as panel, 
			'2011 - 2015' as reference_year_set, 
			1 as sub_population_category, 
			0 as area_domain_category, 
			0 as sub_population_category_superior, 
			0 as area_domain_category_superior	
		union all
		select 
			5 as target_variable, 
			'CZ' as country,
			'MS' as strata_set,
			'NFRD13' as stratum,
			'NFRD13- 1plot, s2a' as panel, 
			'2011 - 2015' as reference_year_set, 
			2 as sub_population_category, 
			0 as area_domain_category, 
			0 as sub_population_category_superior, 
			0 as area_domain_category_superior
		),
w2 as	(
		select json_agg
				(json_build_object
							(
								'target_variable', w1.target_variable,
								'country', w1.country,
								'strata_set', w1.strata_set,
								'stratum', w1.stratum,
								'panel', w1.panel,
								'reference_year_set', w1.reference_year_set,
								'sub_population_category', w1.sub_population_category,
								'area_domain_category', w1.area_domain_category,
								'sub_population_category_superior', w1.sub_population_category_superior,
								'area_domain_category_superior', w1.area_domain_category_superior							
							)
				) as res from w1
		)
select (nfiesta.fn_etl_import_variable_hierarchy(w2.res)) from w2;


-- check_t_variable_hierarchy
select id, variable, variable_superior from nfiesta.t_variable_hierarchy order by id;


-- check_t_available_datasets
select id, panel, reference_year_set, variable from nfiesta.t_available_datasets order by panel, reference_year_set, variable;


-- check_t_target_data 
select plot, value, available_datasets, is_latest from nfiesta.t_target_data where plot = 8155 order by plot, available_datasets, is_latest;


-- import_LDSITY_VALUES
with
w1 as	(
		select
				5 as target_variable,
				'CZ' as country,
				'MS' as strata_set,
				'NFRD11' as stratum,
				'2011 - 2015' as reference_year_set,
				'NFRD11- 1plot, s2a' as panel,
				'100722' as cluster,
				'1p' as cluster_configuration,
				'NFI2' as inventory_campaign,
				'100722' as plot,
				0 as sub_population_category,
				0 as area_domain_category,
				2936.547670750836 as value,
				0.0000000001 as ldsity_threshold
		union all
		select
				5 as target_variable,
				'CZ' as country,
				'MS' as strata_set,
				'NFRD11' as stratum,
				'2011 - 2015' as reference_year_set,
				'NFRD11- 1plot, s2a' as panel,
				'100722' as cluster,
				'1p' as cluster_configuration,
				'NFI2' as inventory_campaign,
				'100722' as plot,
				1 as sub_population_category,
				0 as area_domain_category,
				2936.547670750836 as value,
				0.0000000001 as ldsity_threshold
		union all
		select
				5 as target_variable,
				'CZ' as country,
				'MS' as strata_set,
				'NFRD11' as stratum,
				'2011 - 2015' as reference_year_set,
				'NFRD11- 1plot, s2a' as panel,
				'100722' as cluster,
				'1p' as cluster_configuration,
				'NFI2' as inventory_campaign,
				'100722' as plot,
				2 as sub_population_category,
				0 as area_domain_category,
				0.0 as value,
				0.0000000001 as ldsity_threshold
		)
,w2 as	(
		select distinct w1.target_variable, w1.country, w1.strata_set, w1.stratum,
		w1.reference_year_set, w1.panel, w1.cluster_configuration, w1.inventory_campaign,
		w1.sub_population_category, w1.area_domain_category, w1.ldsity_threshold
		from w1
		)
,w3 as	(
		select
				1 as id4join,
				json_agg
				(json_build_object
							(
								'target_variable', w2.target_variable,
								'country', w2.country,
								'strata_set', w2.strata_set,
								'stratum', w2.stratum,
								'reference_year_set', w2.reference_year_set,
								'panel', w2.panel,
								'cluster_configuration', w2.cluster_configuration,
								'inventory_campaign', w2.inventory_campaign,
								'sub_population_category', w2.sub_population_category,
								'area_domain_category', w2.area_domain_category,
								'ldsity_threshold',w2.ldsity_threshold
							)
				) as res_available_datasets
		from w2
		)
,w4 as	(
		select
				1 as id4join,
				json_agg
				(json_build_object
							(
								'target_variable', w1.target_variable,
								'country', w1.country,
								'strata_set', w1.strata_set,
								'stratum', w1.stratum,
								'reference_year_set', w1.reference_year_set,
								'panel', w1.panel,
								'cluster', w1.cluster,
								'cluster_configuration', w1.cluster_configuration,
								'inventory_campaign', w1.inventory_campaign,
								'plot', w1.plot,
								'sub_population_category', w1.sub_population_category,
								'area_domain_category', w1.area_domain_category,
								'value', w1.value
							)
				) as res_ldsity_values
		from w1
		)
,w5 as	(
		select w3.res_available_datasets, w4.res_ldsity_values
		from w3 inner join w4 on w3.id4join = w4.id4join
		)
,w6 as	(
		select json_build_object
			(
			'available_datasets',w5.res_available_datasets,
			'ldsity_values',w5.res_ldsity_values
			) as res
		from w5
		)
select (nfiesta.fn_etl_import_ldsity_values(w6.res)) from w6;
	
	
-- check_t_available_datasets
select id, panel, reference_year_set, variable, ldsity_threshold from nfiesta.t_available_datasets order by panel, reference_year_set, variable;


-- check_t_target_data 
select plot, value, available_datasets, is_latest from nfiesta.t_target_data order by plot, available_datasets, is_latest;


-- check_TARGET_VARIABLE_METADATAS [EN] -- OK
with
w0 as
(
select json_agg(
json_build_object
			(
			'target_variable',2,
			'etl_id',5,
			'metadata',json_build_object
							(
							'cs',json_build_object
									(
									'indikator',json_build_object('label','number of merch stems','description','Number of merchantable wood stems.'),
									'stav nebo zmena',json_build_object('label','state','description','State.'),
									'jednotka vystupu',json_build_object('label','pc.','description','Piece.'),
									'etl_join_id',50					
									),
							'en',json_build_object
									(
									'indicator',json_build_object('label','number of merch stems','description','Number of merchantable wood stems.'),
									'stav nebo zmena',json_build_object('label','state','description','State.'),
									'jednotka vystupu',json_build_object('label','pc.','description','Piece.'),
									'etl_join_id',50
									)
							)
			)) as res
)
,w1 as	(select nfiesta.fn_etl_check_target_variable_metadatas(w0.res,'en',null::boolean) as res from w0)
select
		(w1.res).target_variable_source,
		(w1.res).target_variable_target,
		(w1.res).metadata_source,
		(w1.res).metadata_target,
		(w1.res).metadata_diff,
		(w1.res).metadata_diff_national_language,
		(w1.res).metadata_diff_english_language,
		(w1.res).missing_metadata_national_language
from
		w1;


-- check_TARGET_VARIABLE_METADATAS [CS] -- OK
with
w0 as
(
select json_agg(
json_build_object
			(
			'target_variable',2,
			'etl_id',5,
			'metadata',json_build_object
							(
							'cs',json_build_object
									(
									'indikator',json_build_object('label','number of merch stems','description','Number of merchantable wood stems.'),
									'stav nebo zmena',json_build_object('label','state','description','State.'),
									'jednotka vystupu',json_build_object('label','pc.','description','Piece.'),
									'etl_join_id',50					
									),
							'en',json_build_object
									(
									'indicator',json_build_object('label','number of merch stems','description','Number of merchantable wood stems.'),
									'stav nebo zmena',json_build_object('label','state','description','State.'),
									'jednotka vystupu',json_build_object('label','pc.','description','Piece.'),
									'etl_join_id',50
									)
							)
			)) as res
)
,w1 as	(select nfiesta.fn_etl_check_target_variable_metadatas(w0.res,'cs',null::boolean) as res from w0)
select
		(w1.res).target_variable_source,
		(w1.res).target_variable_target,
		(w1.res).metadata_source,
		(w1.res).metadata_target,
		(w1.res).metadata_diff,
		(w1.res).metadata_diff_national_language,
		(w1.res).metadata_diff_english_language,
		(w1.res).missing_metadata_national_language
from
		w1;


-- check_TARGET_VARIABLE_METADATAS [EN] -- KO
with
w0 as
(
select json_agg(
json_build_object
			(
			'target_variable',2,
			'etl_id',5,
			'metadata',json_build_object
							(
							'cs',json_build_object
									(
									'indikator',json_build_object('label','number of merch stems','description','Number of merchantable wood stems.'),
									'stav nebo zmena',json_build_object('label','state','description','State.'),
									'jednotka vystupu',json_build_object('label','pc.','description','Piece.'),
									'etl_join_id',50					
									),
							'en',json_build_object
									(
									'indicator',json_build_object('label','number of merch stems','description','Number of merchantable wood stems.'),
									'stav nebo zmena',json_build_object('label','change','description','Change.'),
									'jednotka vystupu',json_build_object('label','pc.','description','Piece.'),
									'etl_join_id',50
									)
							)
			)) as res
)
,w1 as	(select nfiesta.fn_etl_check_target_variable_metadatas(w0.res,'en',null::boolean) as res from w0)
select
		(w1.res).target_variable_source,
		(w1.res).target_variable_target,
		(w1.res).metadata_source,
		(w1.res).metadata_target,
		(w1.res).metadata_diff,
		(w1.res).metadata_diff_national_language,
		(w1.res).metadata_diff_english_language,
		(w1.res).missing_metadata_national_language
from
		w1;


-- check_TARGET_VARIABLE_METADATAS [CS] -- KO
with
w0 as
(
select json_agg(
json_build_object
			(
			'target_variable',2,
			'etl_id',5,
			'metadata',json_build_object
							(
							'cs',json_build_object
									(
									'indikator',json_build_object('label','number of merch stems','description','Number of merchantable wood stems.'),
									'stav nebo zmena',json_build_object('label','change','description','Change.'),
									'jednotka vystupu',json_build_object('label','pc.','description','Piece.'),
									'etl_join_id',50					
									),
							'en',json_build_object
									(
									'indicator',json_build_object('label','number of merch stems','description','Number of merchantable wood stems.'),
									'stav nebo zmena',json_build_object('label','state','description','State.'),
									'jednotka vystupu',json_build_object('label','pc.','description','Piece.'),
									'etl_join_id',50
									)
							)
			)) as res
)
,w1 as	(select nfiesta.fn_etl_check_target_variable_metadatas(w0.res,'cs',null::boolean) as res from w0)
select
		(w1.res).target_variable_source,
		(w1.res).target_variable_target,
		(w1.res).metadata_source,
		(w1.res).metadata_target,
		(w1.res).metadata_diff,
		(w1.res).metadata_diff_national_language,
		(w1.res).metadata_diff_english_language,
		(w1.res).missing_metadata_national_language
from
		w1;


-- update_TARGET_VARIABLE_METADATA [EN,FI] 
with
w0 as
(
select json_build_object
			(
			'cs',json_build_object
					(
					'indikator',json_build_object('label','number of merch stems','description','Number of merchantable wood stems.'),
					'stav nebo zmena',json_build_object('label','state','description','State.'),
					'jednotka vystupu',json_build_object('label','pc.','description','Piece.'),
					'etl_join_id',50					
					),
			'en',json_build_object
					(
					'indicator',json_build_object('label','number of merch stems','description','Number of merchantable wood stems.'),
					'stav nebo zmena',json_build_object('label','state','description','State.'),
					'jednotka vystupu',json_build_object('label','pc.','description','Piece.'),
					'etl_join_id',500
					),
			'fi',json_build_object
					(
					'indicator',json_build_object('label','number of merch stems','description','Number of merchantable wood stems.'),
					'stav nebo zmena',json_build_object('label','state','description','State.'),
					'jednotka vystupu',json_build_object('label','pc.','description','Piece.'),
					'etl_join_id',500
					)
			) as res
)
select nfiesta.fn_etl_update_target_variable_metadata
	(
		5,
		w0.res,
		true,
		false,
		false,
		false,
		true,
		'fi'::character varying
	)
from
	w0;



-- insert_TARGET_VARIABLE_METADATA [DE] 
with
w0 as
(
select json_build_object
			(
			'en',json_build_object
					(
					'indicator',json_build_object('label','number of merch stems','description','Number of merchantable wood stems.'),
					'stav nebo zmena',json_build_object('label','state','description','State.'),
					'jednotka vystupu',json_build_object('label','pc.','description','Piece.'),
					'etl_join_id',500
					),
			'de',json_build_object
					(
					'indicator',json_build_object('label','number of merch stems','description','Number of merchantable wood stems.'),
					'stav nebo zmena',json_build_object('label','state','description','State.'),
					'jednotka vystupu',json_build_object('label','pc.','description','Piece.'),
					'etl_join_id',500
					)
			) as res
)
select nfiesta.fn_etl_insert_target_variable_metadata
	(
		5,
		w0.res,
		true,
		'de'::character varying
	)
from
	w0;


-- ADDING NON-ATOMIC DATA INTO C_SUB_POPULATION AND C_SUB_POPULATION_CATEGORY
insert into nfiesta.c_sub_population(label,description,label_en,description_en,atomic) values
(
'coniferous or deciduous;group of 14 woody species (according to Annex to Decree 84/1996 Coll.)',
'Coniferous or deciduous.;The group of 14 woody species (according to Annex to Decree 84/1996 Coll.).',
'coniferous or deciduous;group of 14 woody species (according to Annex to Decree 84/1996 Coll.)',
'Coniferous or deciduous.;The group of 14 woody species (according to Annex to Decree 84/1996 Coll.).',
false
);

insert into nfiesta.c_sub_population_category(sub_population,label,description,label_en,description_en) values
(3,'coniferous;norway spruce','Coniferous.;Norway spruce.','coniferous;norway spruce','Coniferous.;Norway spruce.'),
(3,'coniferous;european silver fir','Coniferous.;European silver fir.','coniferous;european silver fir','Coniferous.;European silver fir.'),
(3,'coniferous;scots pine','Coniferous.;Scots pine.','coniferous;scots pine','Coniferous.;Scots pine.'),
(3,'coniferous;european larch','Coniferous.;European larch.','coniferous;european larch','Coniferous.;European larch.'),
(3,'coniferous;other coniferous','Coniferous.;Other coniferous.','coniferous;other coniferous','Coniferous.;Other coniferous.'),
(3,'deciduous;european beech','Deciduous.;European beech.','deciduous;european beech','Deciduous.;European beech.'),
(3,'deciduous;oak species','Deciduous.;Oak species.','deciduous;oak species','Deciduous.;Oak species.'),
(3,'deciduous;uropean hornbeam','Deciduous.;European hornbeam.','deciduous;european hornbeam','Deciduous.;European hornbeam.'),
(3,'deciduous;maple species','Deciduous.;Maple species.','deciduous;maple species','Deciduous.;Maple species.'),
(3,'deciduous;ash species','Deciduous.;Ash species.','deciduous;ash species','Deciduous.;Ash species.'),
(3,'deciduous;alder species','Deciduous.;Alder species.','deciduous;alder species','Deciduous.;Alder species.'),
(3,'deciduous;birch species','Deciduous.;Birch species.','deciduous;birch species','Deciduous.;Birch species.'),
(3,'deciduous;other hardwood broad','Deciduous.;Other hardwood broad.','deciduous;other hardwood broad','Deciduous.;Other hardwood broad.'),
(3,'deciduous;other softwood broad','Deciduous.;Other softwood broad.','deciduous;other softwood broad','Deciduous.;Other softwood broad.');


-- SUB POPULATION CATEGORY MAPPING
with
w1 as	(
		select
				id as type_id_non_atomic,
				label_en as type_label_en_non_atomic,
				string_to_array(label_en,';') as type_label_en_array_non_atomic,
				(select array_agg(t.res) from (select generate_series(1,array_length(string_to_array(label_en,';'),1)) res) as t) as type_poradi_non_atomic
		from
				nfiesta.c_sub_population where "atomic" = false
		)
,w2 as	(
		select
				w1.type_id_non_atomic,
				w1.type_label_en_non_atomic,
				unnest(w1.type_label_en_array_non_atomic) as type_label_en_i_non_atomic,
				unnest(w1.type_poradi_non_atomic) as type_poradi_non_atomic
		from
				w1
		)
,w3 as	(
		select
				w2.type_id_non_atomic,
				w2.type_label_en_i_non_atomic,
				w2.type_poradi_non_atomic,
				t1.id as type_id_atomic				
		from
				w2
				inner join (select * from nfiesta.c_sub_population where "atomic" = true) as t1
				on w2.type_label_en_i_non_atomic = t1.label_en
		)
,w4 as	(
		select
				w3.*,
				t1.id as category_id_atomic,
				t1.label_en as category_label_en_atomic
		from
				w3
				inner join	(
							select * from nfiesta.c_sub_population_category
							where sub_population in (select w3.type_id_atomic from w3)
							) as t1
				on w3.type_id_atomic = t1.sub_population
		)
---------
,w5 as	(
		select
				id as category_id_non_atomic,
				sub_population as category_sub_population_non_atomic,
				string_to_array(label_en,';') as category_label_en_array_non_atomic,
				(select array_agg(t.res) from (select generate_series(1,array_length(string_to_array(label_en,';'),1)) res) as t) as category_poradi_non_atomic
		from
				nfiesta.c_sub_population_category
		where
				sub_population in (select id from nfiesta.c_sub_population where "atomic" = false)
		)
,w6 as	(		
		select * from nfiesta.c_sub_population where "atomic" = false
		)
,w7 as (
		select
				w5.*,
				string_to_array(w6.label_en,';') as type_label_en_non_atomic
		from
				w5
				inner join w6 on w5.category_sub_population_non_atomic = w6.id
		)
,w8 as	(
		select
				w7.category_id_non_atomic,
				w7.category_sub_population_non_atomic,
				unnest(w7.category_label_en_array_non_atomic) as category_label_en_i_non_atomic,
				unnest(w7.category_poradi_non_atomic) as category_poradi_non_atomic,
				unnest(w7.type_label_en_non_atomic) as type_label_non_atomic
		from
				w7
		)
,w9 as	(
		select
				w8.category_id_non_atomic,
				w8.category_sub_population_non_atomic,
				w8.category_label_en_i_non_atomic,
				w8.category_poradi_non_atomic,
				w8.type_label_non_atomic,
				csp.id as type_id_atomic
		from
				w8
				inner join (select * from nfiesta.c_sub_population where "atomic" = true) as csp
				on w8.type_label_non_atomic = csp.label_en
		)
---------
,w10 as	(
		select w9.*, w4.* from w9 inner join w4
		on w9.category_sub_population_non_atomic = w4.type_id_non_atomic
		and w9.category_label_en_i_non_atomic = w4.category_label_en_atomic
		and w9.category_poradi_non_atomic = w4.type_poradi_non_atomic
		and w9.type_id_atomic = w4.type_id_atomic
		)
,w11 as (
		select w10.category_id_non_atomic as sub_population_category, w10.category_id_atomic as atomic_category from w10 except
		select sub_population_category, atomic_category from nfiesta.cm_sub_population_category
		)
,w12 as	(
		select json_agg(json_build_object(
				'sub_population_category_non_atomic',w11.sub_population_category,
				'sub_population_category_atomic',w11.atomic_category)) as res
		from
				w11
		)
select nfiesta.fn_etl_import_sub_population_category_mapping(w12.res) from w12;


-- CM_SUB_POPULATION_CATEGORY
select * from nfiesta.cm_sub_population_category order by id;


-- check_area_domains4update
select * from nfiesta.fn_etl_check_area_domains4update
(
	(select '
			[
			{"area_domain" : 1, "label" : "land registry", "description" : "Land use indicated by land registry.", "label_en" : "land registry", "description_en" : "Land use indicated by land registry."},
			{"area_domain" : 2, "label" : "land tenure (4 categoriess)", "description" : "The land tenure (according to the Land Use Registry) aggregated into 4 categories.", "label_en" : "land tenure (4 categories)", "description_en" : "The land tenure (according to the Land Use Registry) aggregated into 4 categories."}
			]
			'::json)
) order by area_domain;


-- update_area_domain_label and update_area_domain_description
select * from nfiesta.c_area_domain order by id;
select * from nfiesta.fn_etl_update_area_domain_label(1,'cs','land registry TEST');
select * from nfiesta.fn_etl_update_area_domain_label(1,'en','land registry TEST');
select * from nfiesta.fn_etl_update_area_domain_description(1,'cs','Land use indicated by land registry TEST.');
select * from nfiesta.fn_etl_update_area_domain_description(1,'en','Land use indicated by land registry TEST.');
select * from nfiesta.c_area_domain order by id;


-- check_area_domain_categories4update
select * from nfiesta.fn_etl_check_area_domain_categories4update
(
	(select '
			[
			{"area_domain" : 1, "area_domain_category" : 1, "label" : "forest", "description" : "Forest area indicated by land registry. (CZ. PUPFL)", "label_en" : "forest", "description_en" : "Forest area indicated by land registry. (CZ. PUPFL)"},
			{"area_domain" : 1, "area_domain_category" : 2, "label" : "non-forest", "description" : "Non-forest area indicated by land registry.", "label_en" : "non-forest", "description_en" : "Non-forest area indicated by land registry (CZ. non-PUPFL)."}
			]
			'::json)
) order by area_domain_category;


-- update_area_domain_category_label and update_area_domain_category_description
select * from nfiesta.c_area_domain_category order by id;
select * from nfiesta.fn_etl_update_area_domain_category_label(3,'cs','state TEST');
select * from nfiesta.fn_etl_update_area_domain_category_label(3,'en','state TEST');
select * from nfiesta.fn_etl_update_area_domain_category_description(3,'cs','State TEST.');
select * from nfiesta.fn_etl_update_area_domain_category_description(3,'en','State TEST.');
select * from nfiesta.c_area_domain_category order by id;


-- check_sub_populations4update
select * from nfiesta.fn_etl_check_sub_populations4update
(
	(select '
			[
			{"sub_population" : 1, "label" : "jehličnany/listnáče", "description" : "Skupina jehličnanů nebo listnáčů.", "label_en" : "conifers/broadleaves", "description_en" : "The group of conifers or broadleaves."}
			]
			'::json)
) order by sub_population;


-- update_sub_population_label and update_sub_population_description
select * from nfiesta.c_sub_population order by id;
select * from nfiesta.fn_etl_update_sub_population_label(1,'cs','coniferous or deciduous TEST');
select * from nfiesta.fn_etl_update_sub_population_label(1,'en','coniferous or deciduous TEST');
select * from nfiesta.fn_etl_update_sub_population_description(1,'cs','Coniferous or deciduous. TEST.');
select * from nfiesta.fn_etl_update_sub_population_description(1,'en','Coniferous or deciduous. TEST.');
select * from nfiesta.c_sub_population order by id;


-- check_sub_population_categories4update
select * from nfiesta.fn_etl_check_sub_population_categories4update
(
	(select '
			[
			{"sub_population" : 1, "sub_population_category" : 1, "label" : "coniferouss", "description" : "Coniferous.", "label_en" : "coniferous", "description_en" : "Coniferous."},
			{"sub_population" : 1, "sub_population_category" : 2, "label" : "deciduouss", "description" : "Deciduous.", "label_en" : "deciduous", "description_en" : "Deciduous."}
			]
			'::json)
) order by sub_population_category;


-- update_sub_population_category_label and update_sub_population_category_description
select * from nfiesta.c_sub_population_category order by id;
select * from nfiesta.fn_etl_update_sub_population_category_label(1,'cs','coniferous TEST');
select * from nfiesta.fn_etl_update_sub_population_category_label(1,'en','coniferous TEST');
select * from nfiesta.fn_etl_update_sub_population_category_description(1,'cs','Coniferous TEST.');
select * from nfiesta.fn_etl_update_sub_population_category_description(1,'en','Coniferous TEST.');
select * from nfiesta.c_sub_population_category order by id;


-- check_area_domain_categories4area_domains
select * from nfiesta.fn_etl_check_area_domain_categories4area_domains
(
	(select '
	[
	{"area_domain" : 1, "area_domain_category" : 1, "label" : "forest", "description" : "Forest area indicated by land registry. (CZ. PUPFL)", "label_en" : "forest", "description_en" : "Forest area indicated by land registry."}, 
	{"area_domain" : 1, "area_domain_category" : 2, "label" : "non-forest", "description" : "Non-forest area indicated by land registry (CZ. non-PUPFL).", "label_en" : "non-forest", "description_en" : "Non-forest area indicated by land registry (CZ. non-PUPFL)."},
	{"area_domain" : 2, "area_domain_category" : 3, "label" : "state TEST", "description" : "State TEST.", "label_en" : "state TEST", "description_en" : "State TEST."}, 
	{"area_domain" : 2, "area_domain_category" : 4, "label" : "municipal", "description" : "Municipal.", "label_en" : "municipal", "description_en" : "Municipal."}, 
	{"area_domain" : 2, "area_domain_category" : 5, "label" : "private and ecclesiastical", "description" : "Private and ecclesiastical.", "label_en" : "private and ecclesiastical", "description_en" : "Private and ecclesiastical."}, 
	{"area_domain" : 2, "area_domain_category" : 6, "label" : "not identified", "description" : "Not identified.", "label_en" : "not identified", "description_en" : "Not identified."}
	]'::json
	)
);


-- check_sub_population_categories4sub_populations
select * from nfiesta.fn_etl_check_sub_population_categories4sub_populations
(
	(
	select '
	[
	{"sub_population" : 1, "sub_population_category" : 1, "label" : "coniferous", "description" : "Coniferous.", "label_en" : "coniferous", "description_en" : "Coniferous."}, 
	{"sub_population" : 1, "sub_population_category" : 2, "label" : "deciduous", "description" : "Deciduous.", "label_en" : "deciduous", "description_en" : "Deciduous."}
	]'::json
	)
);


-- check_area_domains4etl
select * from nfiesta.fn_etl_check_area_domains4etl(1,array[300],'land registry TEST','cs',null::integer[]) order by id;
select * from nfiesta.fn_etl_check_area_domains4etl(1,array[300],'land registry TEST','cs',array[1]) order by id;
select * from nfiesta.fn_etl_check_area_domains4etl(1,array[300],'land registry TEST','cs',array[1,2]) order by id;


-- check_sub_populations4etl
select * from nfiesta.fn_etl_check_sub_populations4etl(1,array[300],'coniferous or deciduous TEST','cs',null::integer[]);
select * from nfiesta.fn_etl_check_sub_populations4etl(1,array[300],'coniferous or deciduous TEST','cs',array[1]);
select * from nfiesta.fn_etl_check_sub_populations4etl(1,array[300],'coniferous or deciduous TEST','cs',array[1,2]);


-- check_area_domains4etl_json
select * from nfiesta.fn_etl_check_area_domains4etl_json
(
	(select '[{"id" : 1, "area_domain" : [300], "label_en_type" : "land registry TEST", "category" : [{"area_domain_category" : [301], "label_en_category" : "forest"}, {"area_domain_category" : [302], "label_en_category" : "non-forest"}]}]'::json),
	array[2]
);
select * from nfiesta.fn_etl_check_area_domains4etl_json
(
	(select '[{"id" : 1, "area_domain" : [300], "label_en_type" : "land registry TEST", "category" : [{"area_domain_category" : [301], "label_en_category" : "forest"}, {"area_domain_category" : [302], "label_en_category" : "non-forest"}]}]'::json),
	array[1,2]
);


-- check_sub_populations4etl_json
select * from nfiesta.fn_etl_check_sub_populations4etl_json
(
	(select '
			[
			{"id" : 1, "sub_population" : [300], "label_en_type" : "group of 14 woody species (according to Annex to Decree 84/1996 Coll.)", "category" : [{"sub_population_category" : [301], "label_en_category" : "norway spruce"}, {"sub_population_category" : [302], "label_en_category" : "european silver fir"}, {"sub_population_category" : [303], "label_en_category" : "scots pine"}, {"sub_population_category" : [304], "label_en_category" : "european larch"}, {"sub_population_category" : [305], "label_en_category" : "other coniferous"}, {"sub_population_category" : [306], "label_en_category" : "european beech"}, {"sub_population_category" : [307], "label_en_category" : "oak species"}, {"sub_population_category" : [308], "label_en_category" : "european hornbeam"}, {"sub_population_category" : [309], "label_en_category" : "maple species"}, {"sub_population_category" : [310], "label_en_category" : "ash species"}, {"sub_population_category" : [311], "label_en_category" : "alder species"}, {"sub_population_category" : [312], "label_en_category" : "birch species"}, {"sub_population_category" : [313], "label_en_category" : "other hardwood broad"}, {"sub_population_category" : [314], "label_en_category" : "other softwood broad"}]},
			{"id" : 2, "sub_population" : [401], "label_en_type" : "coniferous or deciduous TEST", "category" : [{"sub_population_category" : [403], "label_en_category" : "coniferous TEST"}, {"sub_population_category" : [404], "label_en_category" : "deciduous"}]}
			]
			'::json),
	array[1,2,3]
);
select * from nfiesta.fn_etl_check_sub_populations4etl_json
(
	(select '
			[
			{"id" : 1, "sub_population" : [300], "label_en_type" : "group of 14 woody species (according to Annex to Decree 84/1996 Coll.)", "category" : [{"sub_population_category" : [301], "label_en_category" : "norway spruce"}, {"sub_population_category" : [302], "label_en_category" : "european silver fir"}, {"sub_population_category" : [303], "label_en_category" : "scots pine"}, {"sub_population_category" : [304], "label_en_category" : "european larch"}, {"sub_population_category" : [305], "label_en_category" : "other coniferous"}, {"sub_population_category" : [306], "label_en_category" : "european beech"}, {"sub_population_category" : [307], "label_en_category" : "oak species"}, {"sub_population_category" : [308], "label_en_category" : "european hornbeam"}, {"sub_population_category" : [309], "label_en_category" : "maple species"}, {"sub_population_category" : [310], "label_en_category" : "ash species"}, {"sub_population_category" : [311], "label_en_category" : "alder species"}, {"sub_population_category" : [312], "label_en_category" : "birch species"}, {"sub_population_category" : [313], "label_en_category" : "other hardwood broad"}, {"sub_population_category" : [314], "label_en_category" : "other softwood broad"}]},
			{"id" : 2, "sub_population" : [401], "label_en_type" : "coniferous or deciduous TEST", "category" : [{"sub_population_category" : [403], "label_en_category" : "coniferous TEST"}, {"sub_population_category" : [404], "label_en_category" : "deciduous"}]}
			]
			'::json),
	null::integer[]
);


-- check_area_domain_categories4etl
select * from nfiesta.fn_etl_check_area_domain_categories4etl
(
	1,
	(select '[{"id" : 1, "area_domain_category" : [301], "label_en" : "forestt"}, {"id" : 2, "area_domain_category" : [302], "label_en" : "non-forest"}]'::json)
) order by id;


-- check_sub_population_categories4etl
select * from nfiesta.fn_etl_check_sub_population_categories4etl
(
	3,
	(
	select '[
			{"id" : 1, "sub_population_category" : [403,301], "label_en" : "coniferous;norway spruce"},
			{"id" : 2, "sub_population_category" : [403,302], "label_en" : "conifers;European silver firr"},
			{"id" : 3, "sub_population_category" : [403,303], "label_en" : "conifers;Scots pine"},
			{"id" : 4, "sub_population_category" : [403,304], "label_en" : "coniferous;european larch"},
			{"id" : 5, "sub_population_category" : [403,305], "label_en" : "conifers;other coniferous"},
			{"id" : 6, "sub_population_category" : [404,306], "label_en" : "deciduous;european beech"},
			{"id" : 7, "sub_population_category" : [404,307], "label_en" : "broadleaves;oak species"},
			{"id" : 8, "sub_population_category" : [404,308], "label_en" : "broadleaves;European hornbeam"},
			{"id" : 9, "sub_population_category" : [404,309], "label_en" : "broadleaves;maple species"},
			{"id" : 10, "sub_population_category" : [404,310], "label_en" : "broadleaves;ash species"},
			{"id" : 11, "sub_population_category" : [404,311], "label_en" : "broadleaves;alder species"},
			{"id" : 12, "sub_population_category" : [404,312], "label_en" : "broadleaves;birch species"},
			{"id" : 13, "sub_population_category" : [404,313], "label_en" : "broadleaves;other hardwood broadd"},
			{"id" : 14, "sub_population_category" : [404,314], "label_en" : "deciduous;other softwood broad"}
			]'::json)
) order by id;


-- get_area_domain_categories4roller
select * from nfiesta.fn_etl_get_area_domain_categories4roller(1,1,null::integer[]);
select * from nfiesta.fn_etl_get_area_domain_categories4roller(1,1,array[1]);


-- get_sub_population_categories4roller
select * from nfiesta.fn_etl_get_sub_population_categories4roller(1,1,null::integer[]);
select * from nfiesta.fn_etl_get_sub_population_categories4roller(1,1,array[1]);


-- get_topics4target_variable
select * from nfiesta.fn_etl_get_topics4target_variable(1,'cs',null::boolean);
select * from nfiesta.fn_etl_get_topics4target_variable(1,'cs',true);
select * from nfiesta.fn_etl_get_topics4target_variable(1,'cs',false);