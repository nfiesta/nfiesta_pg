--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

/*
select
	format ('(''%s'', ''%s'', ''%s'', %s, ''%s'', %s, ''%s''::boolean, ''%s''::boolean),',
		v_conf_overview.estimation_cell_label,
		t_total_estimate_conf.estimate_date_begin,
		t_total_estimate_conf.estimate_date_end,
		t_variable.id,
		f_a_param_area.param_area_code,
		t_aux_conf.model,
		t_aux_conf.sigma,
		v_conf_overview.force_synthetic
	)
from nfiesta.v_conf_overview
inner join nfiesta.t_total_estimate_conf on (v_conf_overview.total_estimate_conf = t_total_estimate_conf.id)
inner join nfiesta.t_variable on (
	v_conf_overview.target_variable = t_variable.target_variable and
        case when (t_variable.sub_population_category is not null) then
		v_conf_overview.sub_population_category = t_variable.sub_population_category
        else 
		v_conf_overview.sub_population_category is null
	end and
        case when (t_variable.area_domain_category is not null) then
	        v_conf_overview.area_domain_category = t_variable.area_domain_category
        else
		v_conf_overview.area_domain_category is null
	end)
inner join nfiesta.t_aux_conf ON t_aux_conf.id = v_conf_overview.aux_conf
inner join nfiesta.f_a_param_area ON (t_aux_conf.param_area = f_a_param_area.gid)
where estimate_type_str = '2p_total'
order by v_conf_overview.estimation_cell, t_variable.id, v_conf_overview.aux_conf
;
*/
WITH w_data (
	v_conf_overview__estimation_cell_label,
	t_total_estimate_conf__estimate_date_begin,
	t_total_estimate_conf__estimate_date_end,
	t_variable__id,
	f_a_param_area__param_area_code,
	t_aux_conf__model,
	t_aux_conf__sigma,
	v_conf_overview__force_synthetic
	) as (
	values
		('NFR27', '2011-01-01', '2015-12-31', 1, 'NFR27', 1, 'f'::boolean, 'f'::boolean),
		('NFR27', '2011-01-01', '2015-12-31', 1, '50kmE480N300', 1, 'f'::boolean, 'f'::boolean),
		('NFR27', '2011-01-01', '2015-12-31', 1, 'MS', 1, 'f'::boolean, 'f'::boolean),
		('NFR27', '2011-01-01', '2015-12-31', 2, 'NFR27', 1, 'f'::boolean, 'f'::boolean),
		('NFR27', '2011-01-01', '2015-12-31', 2, '50kmE480N300', 1, 'f'::boolean, 'f'::boolean),
		('NFR27', '2011-01-01', '2015-12-31', 2, 'MS', 1, 'f'::boolean, 'f'::boolean),
		('NFR27', '2011-01-01', '2015-12-31', 3, 'NFR27', 1, 'f'::boolean, 'f'::boolean),
		('NFR27', '2011-01-01', '2015-12-31', 3, '50kmE480N300', 1, 'f'::boolean, 'f'::boolean),
		('NFR27', '2011-01-01', '2015-12-31', 3, 'MS', 1, 'f'::boolean, 'f'::boolean),
		('NFR28', '2011-01-01', '2015-12-31', 1, 'MS', 1, 'f'::boolean, 'f'::boolean),
		('NFR28', '2011-01-01', '2015-12-31', 2, 'MS', 1, 'f'::boolean, 'f'::boolean),
		('NFR28', '2011-01-01', '2015-12-31', 3, 'MS', 1, 'f'::boolean, 'f'::boolean),
		('NFR29', '2011-01-01', '2015-12-31', 1, 'MS', 1, 'f'::boolean, 'f'::boolean),
		('NFR29', '2011-01-01', '2015-12-31', 2, 'MS', 1, 'f'::boolean, 'f'::boolean),
		('NFR29', '2011-01-01', '2015-12-31', 3, 'MS', 1, 'f'::boolean, 'f'::boolean),
		('NFR32', '2011-01-01', '2015-12-31', 1, 'MS', 1, 'f'::boolean, 'f'::boolean),
		('NFR32', '2011-01-01', '2015-12-31', 2, 'MS', 1, 'f'::boolean, 'f'::boolean),
		('NFR32', '2011-01-01', '2015-12-31', 3, 'MS', 1, 'f'::boolean, 'f'::boolean),
		('NFR34', '2011-01-01', '2015-12-31', 1, 'MS', 1, 'f'::boolean, 'f'::boolean),
		('NFR34', '2011-01-01', '2015-12-31', 2, 'MS', 1, 'f'::boolean, 'f'::boolean),
		('NFR34', '2011-01-01', '2015-12-31', 3, 'MS', 1, 'f'::boolean, 'f'::boolean),
		('NFR35', '2011-01-01', '2015-12-31', 1, 'MS', 1, 'f'::boolean, 'f'::boolean),
		('NFR35', '2011-01-01', '2015-12-31', 2, 'MS', 1, 'f'::boolean, 'f'::boolean),
		('NFR35', '2011-01-01', '2015-12-31', 3, 'MS', 1, 'f'::boolean, 'f'::boolean),
		('NFR36', '2011-01-01', '2015-12-31', 1, 'MS', 1, 'f'::boolean, 'f'::boolean),
		('NFR36', '2011-01-01', '2015-12-31', 2, 'MS', 1, 'f'::boolean, 'f'::boolean),
		('NFR36', '2011-01-01', '2015-12-31', 3, 'MS', 1, 'f'::boolean, 'f'::boolean),
		('NFR37', '2011-01-01', '2015-12-31', 1, 'MS', 1, 'f'::boolean, 'f'::boolean),
		('NFR37', '2011-01-01', '2015-12-31', 2, 'MS', 1, 'f'::boolean, 'f'::boolean),
		('NFR37', '2011-01-01', '2015-12-31', 3, 'MS', 1, 'f'::boolean, 'f'::boolean),
		('NFR38', '2011-01-01', '2015-12-31', 1, 'MS', 1, 'f'::boolean, 'f'::boolean),
		('NFR38', '2011-01-01', '2015-12-31', 2, 'MS', 1, 'f'::boolean, 'f'::boolean),
		('NFR38', '2011-01-01', '2015-12-31', 3, 'MS', 1, 'f'::boolean, 'f'::boolean),
		('NFR39', '2011-01-01', '2015-12-31', 1, 'MS', 1, 'f'::boolean, 'f'::boolean),
		('NFR39', '2011-01-01', '2015-12-31', 2, 'MS', 1, 'f'::boolean, 'f'::boolean),
		('NFR39', '2011-01-01', '2015-12-31', 3, 'MS', 1, 'f'::boolean, 'f'::boolean),
		('NFR40', '2011-01-01', '2015-12-31', 1, 'NFR40', 1, 'f'::boolean, 'f'::boolean),
		('NFR40', '2011-01-01', '2015-12-31', 1, 'MS', 1, 'f'::boolean, 'f'::boolean),
		('NFR40', '2011-01-01', '2015-12-31', 2, 'NFR40', 1, 'f'::boolean, 'f'::boolean),
		('NFR40', '2011-01-01', '2015-12-31', 2, 'MS', 1, 'f'::boolean, 'f'::boolean),
		('NFR40', '2011-01-01', '2015-12-31', 3, 'NFR40', 1, 'f'::boolean, 'f'::boolean),
		('NFR40', '2011-01-01', '2015-12-31', 3, 'MS', 1, 'f'::boolean, 'f'::boolean),
		('NFR41', '2011-01-01', '2015-12-31', 1, 'MS', 1, 'f'::boolean, 'f'::boolean),
		('NFR41', '2011-01-01', '2015-12-31', 2, 'MS', 1, 'f'::boolean, 'f'::boolean),
		('NFR41', '2011-01-01', '2015-12-31', 3, 'MS', 1, 'f'::boolean, 'f'::boolean),
		('50kmE475N285', '2011-01-01', '2015-12-31', 1, '50kmE475N285', 1, 'f'::boolean, 'f'::boolean),
		('50kmE475N285', '2011-01-01', '2015-12-31', 2, '50kmE475N285', 1, 'f'::boolean, 'f'::boolean),
		('50kmE475N285', '2011-01-01', '2015-12-31', 3, '50kmE475N285', 1, 'f'::boolean, 'f'::boolean),
		('50kmE475N290', '2011-01-01', '2015-12-31', 1, '50kmE475N290', 1, 'f'::boolean, 'f'::boolean),
		('50kmE475N290', '2011-01-01', '2015-12-31', 2, '50kmE475N290', 1, 'f'::boolean, 'f'::boolean),
		('50kmE475N290', '2011-01-01', '2015-12-31', 3, '50kmE475N290', 1, 'f'::boolean, 'f'::boolean),
		('50kmE480N290', '2011-01-01', '2015-12-31', 1, '50kmE480N290', 1, 'f'::boolean, 'f'::boolean),
		('50kmE480N290', '2011-01-01', '2015-12-31', 2, '50kmE480N290', 1, 'f'::boolean, 'f'::boolean),
		('50kmE480N290', '2011-01-01', '2015-12-31', 3, '50kmE480N290', 1, 'f'::boolean, 'f'::boolean),
		('50kmE480N295', '2011-01-01', '2015-12-31', 1, '50kmE480N295', 1, 'f'::boolean, 'f'::boolean),
		('50kmE480N295', '2011-01-01', '2015-12-31', 2, '50kmE480N295', 1, 'f'::boolean, 'f'::boolean),
		('50kmE480N295', '2011-01-01', '2015-12-31', 3, '50kmE480N295', 1, 'f'::boolean, 'f'::boolean),
		('50kmE480N300', '2011-01-01', '2015-12-31', 1, '50kmE480N300', 1, 'f'::boolean, 'f'::boolean),
		('50kmE480N300', '2011-01-01', '2015-12-31', 2, '50kmE480N300', 1, 'f'::boolean, 'f'::boolean),
		('50kmE480N300', '2011-01-01', '2015-12-31', 3, '50kmE480N300', 1, 'f'::boolean, 'f'::boolean),
		('50kmE480N305', '2011-01-01', '2015-12-31', 1, '50kmE480N305', 1, 'f'::boolean, 'f'::boolean),
		('50kmE480N305', '2011-01-01', '2015-12-31', 2, '50kmE480N305', 1, 'f'::boolean, 'f'::boolean),
		('50kmE480N305', '2011-01-01', '2015-12-31', 3, '50kmE480N305', 1, 'f'::boolean, 'f'::boolean),
		('50kmE485N285', '2011-01-01', '2015-12-31', 1, '50kmE485N285', 1, 'f'::boolean, 'f'::boolean),
		('50kmE485N285', '2011-01-01', '2015-12-31', 2, '50kmE485N285', 1, 'f'::boolean, 'f'::boolean),
		('50kmE485N285', '2011-01-01', '2015-12-31', 3, '50kmE485N285', 1, 'f'::boolean, 'f'::boolean),
		('50kmE485N290', '2011-01-01', '2015-12-31', 1, '50kmE485N290', 1, 'f'::boolean, 'f'::boolean),
		('50kmE485N290', '2011-01-01', '2015-12-31', 2, '50kmE485N290', 1, 'f'::boolean, 'f'::boolean),
		('50kmE485N290', '2011-01-01', '2015-12-31', 3, '50kmE485N290', 1, 'f'::boolean, 'f'::boolean),
		('50kmE485N295', '2011-01-01', '2015-12-31', 1, '50kmE485N295', 1, 'f'::boolean, 'f'::boolean),
		('50kmE485N295', '2011-01-01', '2015-12-31', 1, 'MS', 1, 'f'::boolean, 'f'::boolean),
		('50kmE485N295', '2011-01-01', '2015-12-31', 2, '50kmE485N295', 1, 'f'::boolean, 'f'::boolean),
		('50kmE485N295', '2011-01-01', '2015-12-31', 2, 'MS', 1, 'f'::boolean, 'f'::boolean),
		('50kmE485N295', '2011-01-01', '2015-12-31', 3, '50kmE485N295', 1, 'f'::boolean, 'f'::boolean),
		('50kmE485N295', '2011-01-01', '2015-12-31', 3, 'MS', 1, 'f'::boolean, 'f'::boolean),
		('50kmE485N300', '2011-01-01', '2015-12-31', 1, '50kmE485N300', 1, 'f'::boolean, 'f'::boolean),
		('50kmE485N300', '2011-01-01', '2015-12-31', 2, '50kmE485N300', 1, 'f'::boolean, 'f'::boolean),
		('50kmE485N300', '2011-01-01', '2015-12-31', 3, '50kmE485N300', 1, 'f'::boolean, 'f'::boolean),
		('50kmE490N290', '2011-01-01', '2015-12-31', 1, '50kmE490N290', 1, 'f'::boolean, 'f'::boolean),
		('50kmE490N290', '2011-01-01', '2015-12-31', 2, '50kmE490N290', 1, 'f'::boolean, 'f'::boolean),
		('50kmE490N290', '2011-01-01', '2015-12-31', 3, '50kmE490N290', 1, 'f'::boolean, 'f'::boolean),
		('50kmE490N295', '2011-01-01', '2015-12-31', 1, '50kmE490N295', 1, 'f'::boolean, 'f'::boolean),
		('50kmE490N295', '2011-01-01', '2015-12-31', 2, '50kmE490N295', 1, 'f'::boolean, 'f'::boolean),
		('50kmE490N295', '2011-01-01', '2015-12-31', 3, '50kmE490N295', 1, 'f'::boolean, 'f'::boolean),
		('50kmE490N300', '2011-01-01', '2015-12-31', 1, '50kmE490N300', 1, 'f'::boolean, 'f'::boolean),
		('50kmE490N300', '2011-01-01', '2015-12-31', 2, '50kmE490N300', 1, 'f'::boolean, 'f'::boolean),
		('50kmE490N300', '2011-01-01', '2015-12-31', 3, '50kmE490N300', 1, 'f'::boolean, 'f'::boolean),
		('50kmE495N295', '2011-01-01', '2015-12-31', 1, '50kmE495N295', 1, 'f'::boolean, 'f'::boolean),
		('50kmE495N295', '2011-01-01', '2015-12-31', 2, '50kmE495N295', 1, 'f'::boolean, 'f'::boolean),
		('50kmE495N295', '2011-01-01', '2015-12-31', 3, '50kmE495N295', 1, 'f'::boolean, 'f'::boolean),
		('25kmE4650N2950', '2011-01-01', '2015-12-31', 1, '50kmE465N295', 1, 'f'::boolean, 't'::boolean),
		('25kmE4650N2950', '2011-01-01', '2015-12-31', 1, '50kmE465N295', 1, 'f'::boolean, 'f'::boolean),
		('25kmE4650N2950', '2011-01-01', '2015-12-31', 2, '50kmE465N295', 1, 'f'::boolean, 't'::boolean),
		('25kmE4650N2950', '2011-01-01', '2015-12-31', 2, '50kmE465N295', 1, 'f'::boolean, 'f'::boolean),
		('25kmE4650N2950', '2011-01-01', '2015-12-31', 3, '50kmE465N295', 1, 'f'::boolean, 't'::boolean),
		('25kmE4650N2950', '2011-01-01', '2015-12-31', 3, '50kmE465N295', 1, 'f'::boolean, 'f'::boolean),
		('25kmE4650N2900', '2011-01-01', '2015-12-31', 1, '50kmE465N290', 1, 'f'::boolean, 't'::boolean),
		('25kmE4650N2900', '2011-01-01', '2015-12-31', 1, '50kmE465N290', 1, 'f'::boolean, 'f'::boolean),
		('25kmE4650N2900', '2011-01-01', '2015-12-31', 2, '50kmE465N290', 1, 'f'::boolean, 't'::boolean),
		('25kmE4650N2900', '2011-01-01', '2015-12-31', 2, '50kmE465N290', 1, 'f'::boolean, 'f'::boolean),
		('25kmE4650N2900', '2011-01-01', '2015-12-31', 3, '50kmE465N290', 1, 'f'::boolean, 't'::boolean),
		('25kmE4650N2900', '2011-01-01', '2015-12-31', 3, '50kmE465N290', 1, 'f'::boolean, 'f'::boolean),
		('25kmE4700N2950', '2011-01-01', '2015-12-31', 1, '50kmE470N295', 1, 'f'::boolean, 't'::boolean),
		('25kmE4700N2950', '2011-01-01', '2015-12-31', 1, '50kmE470N295', 1, 'f'::boolean, 'f'::boolean),
		('25kmE4700N2950', '2011-01-01', '2015-12-31', 2, '50kmE470N295', 1, 'f'::boolean, 't'::boolean),
		('25kmE4700N2950', '2011-01-01', '2015-12-31', 2, '50kmE470N295', 1, 'f'::boolean, 'f'::boolean),
		('25kmE4700N2950', '2011-01-01', '2015-12-31', 3, '50kmE470N295', 1, 'f'::boolean, 't'::boolean),
		('25kmE4700N2950', '2011-01-01', '2015-12-31', 3, '50kmE470N295', 1, 'f'::boolean, 'f'::boolean),
		('25kmE4700N2900', '2011-01-01', '2015-12-31', 1, '50kmE470N290', 1, 'f'::boolean, 't'::boolean),
		('25kmE4700N2900', '2011-01-01', '2015-12-31', 1, '50kmE470N290', 1, 'f'::boolean, 'f'::boolean),
		('25kmE4700N2900', '2011-01-01', '2015-12-31', 2, '50kmE470N290', 1, 'f'::boolean, 't'::boolean),
		('25kmE4700N2900', '2011-01-01', '2015-12-31', 2, '50kmE470N290', 1, 'f'::boolean, 'f'::boolean),
		('25kmE4700N2900', '2011-01-01', '2015-12-31', 3, '50kmE470N290', 1, 'f'::boolean, 't'::boolean),
		('25kmE4700N2900', '2011-01-01', '2015-12-31', 3, '50kmE470N290', 1, 'f'::boolean, 'f'::boolean),
		('25kmE4750N3000', '2011-01-01', '2015-12-31', 1, '50kmE475N300', 1, 'f'::boolean, 't'::boolean),
		('25kmE4750N3000', '2011-01-01', '2015-12-31', 1, '50kmE475N300', 1, 'f'::boolean, 'f'::boolean),
		('25kmE4750N3000', '2011-01-01', '2015-12-31', 2, '50kmE475N300', 1, 'f'::boolean, 't'::boolean),
		('25kmE4750N3000', '2011-01-01', '2015-12-31', 2, '50kmE475N300', 1, 'f'::boolean, 'f'::boolean),
		('25kmE4750N3000', '2011-01-01', '2015-12-31', 3, '50kmE475N300', 1, 'f'::boolean, 't'::boolean),
		('25kmE4750N3000', '2011-01-01', '2015-12-31', 3, '50kmE475N300', 1, 'f'::boolean, 'f'::boolean),
		('25kmE4750N2950', '2011-01-01', '2015-12-31', 1, '50kmE475N295', 1, 'f'::boolean, 't'::boolean),
		('25kmE4750N2950', '2011-01-01', '2015-12-31', 1, '50kmE475N295', 1, 'f'::boolean, 'f'::boolean),
		('25kmE4750N2950', '2011-01-01', '2015-12-31', 2, '50kmE475N295', 1, 'f'::boolean, 't'::boolean),
		('25kmE4750N2950', '2011-01-01', '2015-12-31', 2, '50kmE475N295', 1, 'f'::boolean, 'f'::boolean),
		('25kmE4750N2950', '2011-01-01', '2015-12-31', 3, '50kmE475N295', 1, 'f'::boolean, 't'::boolean),
		('25kmE4750N2950', '2011-01-01', '2015-12-31', 3, '50kmE475N295', 1, 'f'::boolean, 'f'::boolean),
		('25kmE4750N2900', '2011-01-01', '2015-12-31', 1, '50kmE475N290', 1, 'f'::boolean, 't'::boolean),
		('25kmE4750N2900', '2011-01-01', '2015-12-31', 1, '50kmE475N290', 1, 'f'::boolean, 'f'::boolean),
		('25kmE4750N2900', '2011-01-01', '2015-12-31', 2, '50kmE475N290', 1, 'f'::boolean, 't'::boolean),
		('25kmE4750N2900', '2011-01-01', '2015-12-31', 2, '50kmE475N290', 1, 'f'::boolean, 'f'::boolean),
		('25kmE4750N2900', '2011-01-01', '2015-12-31', 3, '50kmE475N290', 1, 'f'::boolean, 't'::boolean),
		('25kmE4750N2900', '2011-01-01', '2015-12-31', 4, '50kmE475N290', 1, 'f'::boolean, 'f'::boolean),
		('25kmE4750N2850', '2011-01-01', '2015-12-31', 1, '50kmE475N285', 1, 'f'::boolean, 'f'::boolean),
		('25kmE4750N2850', '2011-01-01', '2015-12-31', 1, '50kmE475N285', 1, 'f'::boolean, 't'::boolean),
		('25kmE4750N2850', '2011-01-01', '2015-12-31', 2, '50kmE475N285', 1, 'f'::boolean, 'f'::boolean),
		('25kmE4750N2850', '2011-01-01', '2015-12-31', 2, '50kmE475N285', 1, 'f'::boolean, 't'::boolean),
		('25kmE4750N2850', '2011-01-01', '2015-12-31', 3, '50kmE475N285', 1, 'f'::boolean, 'f'::boolean),
		('25kmE4750N2850', '2011-01-01', '2015-12-31', 3, '50kmE475N285', 1, 'f'::boolean, 't'::boolean),
		/*('25kmE4800N2850', '2011-01-01', '2015-12-31', 1, '50kmE480N285', 1, 'f'::boolean, 't'::boolean),
		('25kmE4800N2850', '2011-01-01', '2015-12-31', 1, '50kmE480N285', 1, 'f'::boolean, 'f'::boolean),
		('25kmE4800N2850', '2011-01-01', '2015-12-31', 2, '50kmE480N285', 1, 'f'::boolean, 't'::boolean),
		('25kmE4800N2850', '2011-01-01', '2015-12-31', 2, '50kmE480N285', 1, 'f'::boolean, 'f'::boolean),
		('25kmE4800N2850', '2011-01-01', '2015-12-31', 3, '50kmE480N285', 1, 'f'::boolean, 't'::boolean),
		('25kmE4800N2850', '2011-01-01', '2015-12-31', 3, '50kmE480N285', 1, 'f'::boolean, 'f'::boolean),*/
		('25kmE4800N2900', '2011-01-01', '2015-12-31', 1, '50kmE480N290', 1, 'f'::boolean, 't'::boolean),
		('25kmE4800N2900', '2011-01-01', '2015-12-31', 1, '50kmE480N290', 1, 'f'::boolean, 'f'::boolean),
		('25kmE4800N2900', '2011-01-01', '2015-12-31', 2, '50kmE480N290', 1, 'f'::boolean, 't'::boolean),
		('25kmE4800N2900', '2011-01-01', '2015-12-31', 2, '50kmE480N290', 1, 'f'::boolean, 'f'::boolean),
		('25kmE4800N2900', '2011-01-01', '2015-12-31', 3, '50kmE480N290', 1, 'f'::boolean, 't'::boolean),
		('25kmE4800N2900', '2011-01-01', '2015-12-31', 3, '50kmE480N290', 1, 'f'::boolean, 'f'::boolean),
		('25kmE4800N2950', '2011-01-01', '2015-12-31', 1, '50kmE480N295', 1, 'f'::boolean, 't'::boolean),
		('25kmE4800N2950', '2011-01-01', '2015-12-31', 1, '50kmE480N295', 1, 'f'::boolean, 'f'::boolean),
		('25kmE4800N2950', '2011-01-01', '2015-12-31', 2, '50kmE480N295', 1, 'f'::boolean, 't'::boolean),
		('25kmE4800N2950', '2011-01-01', '2015-12-31', 3, '50kmE480N295', 1, 'f'::boolean, 'f'::boolean),
		('25kmE4800N2950', '2011-01-01', '2015-12-31', 3, '50kmE480N295', 1, 'f'::boolean, 't'::boolean),
		('25kmE4800N2950', '2011-01-01', '2015-12-31', 4, '50kmE480N295', 1, 'f'::boolean, 'f'::boolean),
		('25kmE4800N3000', '2011-01-01', '2015-12-31', 1, '50kmE480N300', 1, 'f'::boolean, 't'::boolean),
		('25kmE4800N3000', '2011-01-01', '2015-12-31', 1, '50kmE480N300', 1, 'f'::boolean, 'f'::boolean),
		('25kmE4800N3000', '2011-01-01', '2015-12-31', 2, '50kmE480N300', 1, 'f'::boolean, 't'::boolean),
		('25kmE4800N3000', '2011-01-01', '2015-12-31', 2, '50kmE480N300', 1, 'f'::boolean, 'f'::boolean),
		('25kmE4800N3000', '2011-01-01', '2015-12-31', 3, '50kmE480N300', 1, 'f'::boolean, 'f'::boolean),
		('25kmE4800N3000', '2011-01-01', '2015-12-31', 3, '50kmE480N300', 1, 'f'::boolean, 't'::boolean),
		('25kmE4800N3050', '2011-01-01', '2015-12-31', 1, '50kmE480N305', 1, 'f'::boolean, 'f'::boolean),
		('25kmE4800N3050', '2011-01-01', '2015-12-31', 1, '50kmE480N305', 1, 'f'::boolean, 't'::boolean),
		('25kmE4800N3050', '2011-01-01', '2015-12-31', 2, '50kmE480N305', 1, 'f'::boolean, 't'::boolean),
		('25kmE4800N3050', '2011-01-01', '2015-12-31', 2, '50kmE480N305', 1, 'f'::boolean, 'f'::boolean),
		('25kmE4800N3050', '2011-01-01', '2015-12-31', 3, '50kmE480N305', 1, 'f'::boolean, 'f'::boolean),
		('25kmE4800N3050', '2011-01-01', '2015-12-31', 3, '50kmE480N305', 1, 'f'::boolean, 't'::boolean),
		('25kmE4825N2900', '2011-01-01', '2015-12-31', 1, 'MS', 1, 'f'::boolean, 'f'::boolean),
		('25kmE4825N2900', '2011-01-01', '2015-12-31', 2, 'MS', 1, 'f'::boolean, 'f'::boolean),
		('25kmE4825N2900', '2011-01-01', '2015-12-31', 3, 'MS', 1, 'f'::boolean, 'f'::boolean),
		('25kmE4825N3000', '2011-01-01', '2015-12-31', 1, 'MS', 1, 'f'::boolean, 'f'::boolean),
		('25kmE4825N3000', '2011-01-01', '2015-12-31', 2, 'MS', 1, 'f'::boolean, 'f'::boolean),
		('25kmE4825N3000', '2011-01-01', '2015-12-31', 3, 'MS', 1, 'f'::boolean, 'f'::boolean),
		('25kmE4850N2900', '2011-01-01', '2015-12-31', 1, '50kmE485N290', 1, 'f'::boolean, 'f'::boolean),
		('25kmE4850N2900', '2011-01-01', '2015-12-31', 1, 'MS', 1, 'f'::boolean, 'f'::boolean),
		('25kmE4850N2900', '2011-01-01', '2015-12-31', 2, '50kmE485N290', 1, 'f'::boolean, 'f'::boolean),
		('25kmE4850N2900', '2011-01-01', '2015-12-31', 2, 'MS', 1, 'f'::boolean, 'f'::boolean),
		('25kmE4850N2900', '2011-01-01', '2015-12-31', 3, '50kmE485N290', 1, 'f'::boolean, 'f'::boolean),
		('25kmE4850N2900', '2011-01-01', '2015-12-31', 3, 'MS', 1, 'f'::boolean, 'f'::boolean),
		('25kmE4850N2925', '2011-01-01', '2015-12-31', 1, 'MS', 1, 'f'::boolean, 'f'::boolean),
		('25kmE4850N2925', '2011-01-01', '2015-12-31', 2, 'MS', 1, 'f'::boolean, 'f'::boolean),
		('25kmE4850N2925', '2011-01-01', '2015-12-31', 3, 'MS', 1, 'f'::boolean, 'f'::boolean),
		('25kmE4850N2950', '2011-01-01', '2015-12-31', 1, '50kmE485N295', 1, 'f'::boolean, 'f'::boolean),
		('25kmE4850N2950', '2011-01-01', '2015-12-31', 1, 'MS', 1, 'f'::boolean, 'f'::boolean),
		('25kmE4850N2950', '2011-01-01', '2015-12-31', 2, '50kmE485N295', 1, 'f'::boolean, 'f'::boolean),
		('25kmE4850N2950', '2011-01-01', '2015-12-31', 2, 'MS', 1, 'f'::boolean, 'f'::boolean),
		('25kmE4850N2950', '2011-01-01', '2015-12-31', 3, '50kmE485N295', 1, 'f'::boolean, 'f'::boolean),
		('25kmE4850N2950', '2011-01-01', '2015-12-31', 3, 'MS', 1, 'f'::boolean, 'f'::boolean),
		('25kmE4850N2975', '2011-01-01', '2015-12-31', 1, 'MS', 1, 'f'::boolean, 'f'::boolean),
		('25kmE4850N2975', '2011-01-01', '2015-12-31', 2, 'MS', 1, 'f'::boolean, 'f'::boolean),
		('25kmE4850N2975', '2011-01-01', '2015-12-31', 3, 'MS', 1, 'f'::boolean, 'f'::boolean),
		('25kmE4850N3000', '2011-01-01', '2015-12-31', 1, '50kmE485N300', 1, 'f'::boolean, 'f'::boolean),
		('25kmE4850N3000', '2011-01-01', '2015-12-31', 2, '50kmE485N300', 1, 'f'::boolean, 'f'::boolean),
		('25kmE4850N3000', '2011-01-01', '2015-12-31', 3, '50kmE485N300', 1, 'f'::boolean, 'f'::boolean),
		/*('25kmE4850N3050', '2011-01-01', '2015-12-31', 1, '50kmE485N350', 1, 'f'::boolean, 'f'::boolean),
		('25kmE4850N3050', '2011-01-01', '2015-12-31', 2, '50kmE485N350', 1, 'f'::boolean, 'f'::boolean),
		('25kmE4850N3050', '2011-01-01', '2015-12-31', 3, '50kmE485N350', 1, 'f'::boolean, 'f'::boolean),*/
		('25kmE4875N2925', '2011-01-01', '2015-12-31', 1, 'MS', 1, 'f'::boolean, 'f'::boolean),
		('25kmE4875N2925', '2011-01-01', '2015-12-31', 2, 'MS', 1, 'f'::boolean, 'f'::boolean),
		('25kmE4875N2925', '2011-01-01', '2015-12-31', 3, 'MS', 1, 'f'::boolean, 'f'::boolean),
		('25kmE4875N2950', '2011-01-01', '2015-12-31', 1, 'MS', 1, 'f'::boolean, 'f'::boolean),
		('25kmE4875N2950', '2011-01-01', '2015-12-31', 2, 'MS', 1, 'f'::boolean, 'f'::boolean),
		('25kmE4875N2950', '2011-01-01', '2015-12-31', 3, 'MS', 1, 'f'::boolean, 'f'::boolean),
		('25kmE4875N2975', '2011-01-01', '2015-12-31', 1, 'MS', 1, 'f'::boolean, 'f'::boolean),
		('25kmE4875N2975', '2011-01-01', '2015-12-31', 2, 'MS', 1, 'f'::boolean, 'f'::boolean),
		('25kmE4875N2975', '2011-01-01', '2015-12-31', 3, 'MS', 1, 'f'::boolean, 'f'::boolean),
		('25kmE4900N2900', '2011-01-01', '2015-12-31', 1, '50kmE490N290', 1, 'f'::boolean, 'f'::boolean),
		('25kmE4900N2900', '2011-01-01', '2015-12-31', 2, '50kmE490N290', 1, 'f'::boolean, 'f'::boolean),
		('25kmE4900N2900', '2011-01-01', '2015-12-31', 3, '50kmE490N290', 1, 'f'::boolean, 'f'::boolean),
		('25kmE4900N2950', '2011-01-01', '2015-12-31', 1, '50kmE490N295', 1, 'f'::boolean, 'f'::boolean),
		('25kmE4900N2950', '2011-01-01', '2015-12-31', 1, 'MS', 1, 'f'::boolean, 'f'::boolean),
		('25kmE4900N2950', '2011-01-01', '2015-12-31', 2, '50kmE490N295', 1, 'f'::boolean, 'f'::boolean),
		('25kmE4900N2950', '2011-01-01', '2015-12-31', 2, 'MS', 1, 'f'::boolean, 'f'::boolean),
		('25kmE4900N2950', '2011-01-01', '2015-12-31', 3, '50kmE490N295', 1, 'f'::boolean, 'f'::boolean),
		('25kmE4900N2950', '2011-01-01', '2015-12-31', 3, 'MS', 1, 'f'::boolean, 'f'::boolean),
		('25kmE4900N2975', '2011-01-01', '2015-12-31', 1, 'MS', 1, 'f'::boolean, 'f'::boolean),
		('25kmE4900N2975', '2011-01-01', '2015-12-31', 2, 'MS', 1, 'f'::boolean, 'f'::boolean),
		('25kmE4900N2975', '2011-01-01', '2015-12-31', 3, 'MS', 1, 'f'::boolean, 'f'::boolean),
		('25kmE4900N3000', '2011-01-01', '2015-12-31', 1, '50kmE490N300', 1, 'f'::boolean, 'f'::boolean),
		('25kmE4900N3000', '2011-01-01', '2015-12-31', 2, '50kmE490N300', 1, 'f'::boolean, 'f'::boolean),
		('25kmE4900N3000', '2011-01-01', '2015-12-31', 3, '50kmE490N300', 1, 'f'::boolean, 'f'::boolean),
		('25kmE4950N2950', '2011-01-01', '2015-12-31', 1, '50kmE495N295', 1, 'f'::boolean, 'f'::boolean),
		('25kmE4950N2950', '2011-01-01', '2015-12-31', 2, '50kmE495N295', 1, 'f'::boolean, 'f'::boolean),
		('25kmE4950N2950', '2011-01-01', '2015-12-31', 3, '50kmE495N295', 1, 'f'::boolean, 'f'::boolean)
)
select nfiesta.fn_2p_est_configuration(
	c_estimation_cell.id::integer,
	c_estimation_period.id,
	''::varchar,
	t_variable__id::integer,
	t_aux_conf.id::integer,
	v_conf_overview__force_synthetic::boolean)
from w_data
inner join nfiesta.f_a_param_area ON (f_a_param_area.param_area_code = w_data.f_a_param_area__param_area_code)
inner join nfiesta.t_aux_conf ON (
	t_aux_conf.param_area = f_a_param_area.gid and
	t_aux_conf.model = w_data.t_aux_conf__model and
	t_aux_conf.sigma = w_data.t_aux_conf__sigma)
inner join nfiesta.c_estimation_cell ON (c_estimation_cell.label = w_data.v_conf_overview__estimation_cell_label)
inner join nfiesta.c_estimation_period ON 
		w_data.t_total_estimate_conf__estimate_date_begin::date = c_estimation_period.estimate_date_begin AND
		w_data.t_total_estimate_conf__estimate_date_end::date = c_estimation_period.estimate_date_end
;

with w_data (
	v_conf_overview__estimation_cell_label,
	t_total_estimate_conf__estimate_date_begin,
	t_total_estimate_conf__estimate_date_end,
	t_variable__id,
	f_a_param_area__param_area_code,
	t_aux_conf__model,
	t_aux_conf__sigma,
	v_conf_overview__force_synthetic
	) as (
	values
		('NFR27', '2011-01-01', '2015-12-31', 1, 'NFRD11', 1, 'f'::boolean, 'f'::boolean)
)
select nfiesta.fn_2p_est_configuration(
	c_estimation_cell.id::integer,
	c_estimation_period.id,
	''::varchar,
	t_variable__id::integer,
	t_aux_conf.id::integer,
	v_conf_overview__force_synthetic::boolean)
from w_data
inner join nfiesta.f_a_param_area ON (f_a_param_area.param_area_code = w_data.f_a_param_area__param_area_code)
inner join nfiesta.t_aux_conf ON (
	t_aux_conf.param_area = f_a_param_area.gid and
	t_aux_conf.model = w_data.t_aux_conf__model and
	t_aux_conf.sigma = w_data.t_aux_conf__sigma)
inner join nfiesta.c_estimation_cell ON (c_estimation_cell.label = w_data.v_conf_overview__estimation_cell_label)
inner join nfiesta.c_estimation_period ON 
		w_data.t_total_estimate_conf__estimate_date_begin::date = c_estimation_period.estimate_date_begin AND
		w_data.t_total_estimate_conf__estimate_date_end::date = c_estimation_period.estimate_date_end

;

------------------------------------------2p/2p
with w_2pt as (
	select * from nfiesta.v_conf_overview where estimate_type_str = '2p_total' order by estimate_conf 
)
, w_2pr as (
	select 
		nom.total_estimate_conf as nom__tec_id,
		denom.total_estimate_conf as denom__tec_id
	from 
	w_2pt as nom
	inner join w_2pt as denom on (nom.estimation_cell = denom.estimation_cell 
		and nom.param_area = denom.param_area
		and nom.target_variable = 1 and nom.area_domain_category in (1, 2) and denom.target_variable = 1 and denom.area_domain_category is null
	)
)
insert into nfiesta.t_estimate_conf (estimate_type, total_estimate_conf, denominator) 
select 2, nom__tec_id, denom__tec_id
from w_2pr
order by nom__tec_id, denom__tec_id;
------------------------------------------2p/1p
with w_1pt as (
	select * from nfiesta.v_conf_overview where estimate_type_str = '1p_total' order by estimate_conf
)
, w_2pt as (
	select * from nfiesta.v_conf_overview where estimate_type_str = '2p_total' order by estimate_conf
)
, w_21pr as (
	select 
		nom.total_estimate_conf as nom__tec_id,
		denom.total_estimate_conf as denom__tec_id
	from 
	w_2pt as nom
	inner join w_1pt as denom on (nom.estimation_cell = denom.estimation_cell 
		and nom.target_variable = 1 and denom.target_variable = 2)
)
insert into nfiesta.t_estimate_conf (estimate_type, total_estimate_conf, denominator) 
select 2, nom__tec_id, denom__tec_id
from w_21pr
order by nom__tec_id, denom__tec_id;

------------------------------------------1p/2p
with w_1pt as (
	select * from nfiesta.v_conf_overview where estimate_type_str = '1p_total' order by estimate_conf
)
, w_2pt as (
	select * from nfiesta.v_conf_overview where estimate_type_str = '2p_total' order by estimate_conf
)
, w_12pr as (
	select 
		nom.total_estimate_conf as nom__tec_id,
		denom.total_estimate_conf as denom__tec_id
	from 
	w_1pt as nom
	inner join w_2pt as denom on (nom.estimation_cell = denom.estimation_cell 
		and nom.target_variable = 1 and nom.area_domain_category in (1, 2) and denom.target_variable = 1 and denom.area_domain_category is null)
)
insert into nfiesta.t_estimate_conf (estimate_type, total_estimate_conf, denominator) 
select 2, nom__tec_id, denom__tec_id
from w_12pr
order by nom__tec_id, denom__tec_id;
