with w_fnres as (
	select nfiesta.fn_add_res_total_attr((select array_agg(id order by id) from nfiesta.t_estimate_conf)) as res
)
select estimation_cell, aux_conf, force_synthetic,
	estimate_conf, variable, round(point_est::numeric, 10) as point_est,
	round(point_est_sum::numeric, 10) as point_est_sum, variables_def, variables_found, estimate_confs_found,
	round(diff::numeric, 10) as diff
from (select (res).* from w_fnres) as foo
where diff > 0.0001 or diff is null
order by variable, aux_conf, force_synthetic, estimation_cell, variables_def;

with w_fnres as (
	select nfiesta.fn_add_res_total_geo((select array_agg(id order by id) from nfiesta.t_estimate_conf)) as res
)
select variable, aux_conf, force_synthetic,
	estimate_conf, estimation_cell, round(point_est::numeric, 10) as point_est,
	round(point_est_sum::numeric, 10) as point_est_sum, estimation_cells_def, estimation_cells_found, estimate_confs_found,
	round(diff::numeric, 10) as diff
from (select (res).* from w_fnres) as foo
where diff > 0.0001 or diff is null
order by estimation_cell, aux_conf, force_synthetic, variable;

------------------------------------------

with w_fnres as (
	select nfiesta.fn_add_res_ratio_attr((select array_agg(id order by id) from nfiesta.t_estimate_conf)) as res
)
select estimation_cell, aux_conf, force_synthetic, denominator,
	estimate_conf, variable, round(point_est::numeric, 10) as point_est,
	round(point_est_sum::numeric, 10) as point_est_sum, variables_def, variables_found, estimate_confs_found,
	round(diff::numeric, 10) as diff
from (select (res).* from w_fnres) as foo
where diff > 0.0001 or diff is null
order by variable, aux_conf, force_synthetic, denominator, estimation_cell, variables_def;

------------------------------------------

select
	nfiesta.fn_est_configuration(cell, c_estimation_period.id, 'testing'::varchar, variable)
from 	(values (4))					as w_cell(cell),
	(values (7), (8), (9), (10), (11), (12)) 	as w_variable(variable),
	(values ('2011-01-01'::date, '2015-12-31'::date)) as w_period(estimate_date_begin, estimate_date_end)
	INNER JOIN nfiesta.c_estimation_period 
	ON 	w_period.estimate_date_begin = c_estimation_period.estimate_date_begin AND
		w_period.estimate_date_end = c_estimation_period.estimate_date_end
;

select
	nfiesta.fn_make_estimate(new.conf_id) from
		(select t_estimate_conf.id as conf_id
		from nfiesta.t_total_estimate_conf
		inner join nfiesta.t_estimate_conf on (t_total_estimate_conf.id = t_estimate_conf.total_estimate_conf)
		where variable >= 7) as new;

with w_fnres as (
	select nfiesta.fn_add_res_total_attr((select array_agg(id order by id) from nfiesta.t_estimate_conf), 0.0) as res
)
select estimation_cell, aux_conf, force_synthetic,
	estimate_conf, variable, round(point_est::numeric, 10) as point_est,
	round(point_est_sum::numeric, 10) as point_est_sum, variables_def, variables_found, estimate_confs_found,
	round(diff::numeric, 10) as diff
from (select (res).* from w_fnres) as foo
where variables_found && ARRAY[7, 8, 9, 10, 11, 12]
order by variable, aux_conf, force_synthetic, estimation_cell;
