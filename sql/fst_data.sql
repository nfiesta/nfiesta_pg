--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
-- DATA DISCLAIMER
-- Any results produced on the basis of openly published Czech National Forest Inventory (CZNFI) sample data
-- do not reflect the true status or changes within any geographical area of the Czech Republic,
-- at, during or between any time occasion(s).
-- In particular, any such results must not be presented or interpreted as an alternative to any information published by CZNFI,
-- be it a past or future CZNFI publication.
--

\set srcdir `echo $SRC_DIR`

--
-- Data for Name: c_topic; Type: TABLE DATA; Schema: nfiesta; Owner: vagrant
--

COPY nfiesta.c_topic  FROM stdin;
1	areal variable	Areal variable.	\N	\N
2	productive indicators	Indicators of productivity.	\N	\N
\.

--
-- Data for Name: c_target_variable; Type: TABLE DATA; Schema: nfiesta; Owner: vagrant
--

INSERT INTO nfiesta.c_target_variable(metadata)
select '{"cs":
	{"indicator": {"label": "les", "description": "Plocha lesa."},
	"state or change": {"label": "state variable", "description": "State variable."},
	"unit": {"label": "ha", "description": "Hektar."},
	"local densities": [
		{"object type label": "core",
		"object type description": "Contribution to local density which will be aggregated and is the input into the estimate of total.",
		"object label": "střed inventarizační plochy",
		"object description": "střed inventarizační plochy",
		"label": "plocha lesa", "description": "Plocha lesa.",
		"version": {"label": "1. verze", "description": "1. verze."},
		"definition variant": {"label": null, "description": null},
		"area domain": {"label": ["celá plocha geografické domény"], "description": ["Celá plocha geografické domény (celá zájmová oblast)."]},
		"population": {"label": null, "description": null}, "use_negative": false}]
	},
"en":
	{"indicator": {"label": "forest", "description": "Area of forests."},
	"state or change": {"label": "state variable", "description": "State variable."},
	"unit": {"label": "ha", "description": "Hectare."},
	"local densities": [
		{"object type label": "core",
		"object type description": "Contribution to local density which will be aggregated and is the input into the estimate of total.",
		"object label": "the center of the inventory plot",
		"object description": "the center of the inventory plot",
		"label": "area of forests", "description": "The area of forests.",
		"version": {"label": "version 1", "description": "Version 1."},
		"definition variant": {"label": null, "description": null},
		"area domain": {"label": ["entire area of geographical domain"], "description": ["The entire area of the geographical domain (the whole area of interest)."]},
		"population": {"label": null, "description": null}, "use_negative": false}]
	}
}'::json
union all
select '{"cs":
	{"indicator": {"label": "plocha", "description": "Rozloha celé geografické domény (např. rozloha kategorií pozemků)."},
	"state or change": {"label": "state variable", "description": "State variable."},
	"unit": {"label": "ha", "description": "Hektar."},
	"local densities": [
		{"object type label": "core",
		"object type description": "Contribution to local density which will be aggregated and is the input into the estimate of total.",
		"object label": "střed inventarizační plochy",
		"object description": "střed inventarizační plochy",
		"label": "rozloha území", "description": "Rozloha území.",
		"version": {"label": "1. verze", "description": "1. verze."},
		"definition variant": {"label": null, "description": null},
		"area domain": {"label": null, "description": null},
		"population": {"label": null, "description": null}, "use_negative": false}]
	},
"en":
	{"indicator": {"label": "area", "description": "Area of geographical domain (e.g. extent of land categories)."},
	"state or change": {"label": "state variable", "description": "State variable."},
	"unit": {"label": "ha", "description": "Hectare."},
	"local densities": [
		{"object type label": "core",
		"object type description": "Contribution to local density which will be aggregated and is the input into the estimate of total.",
		"object label": "the center of the inventory plot",
		"object description": "the center of the inventory plot",
		"label": "whole domain area", "description": "Whole domain area extent.",
		"version": {"label": "version 1", "description": "Version 1."},
		"definition variant": {"label": null, "description": null},
		"area domain": {"label": null, "description": null},
		"population": {"label": null, "description": null}, "use_negative": false}]
	}
}'::json
union all
select '{"cs":
	{"indicator": {"label": "nb", "description": "Hmotnost nadzemní biomasy dřevin."},
	"state or change": {"label": "state variable", "description": "State variable."},
	"unit": {"label": "t", "description": "Tuna."},
	"local densities": [
		{"object type label": "core",
		"object type description": "Contribution to local density which will be aggregated and is the input into the estimate of total.",
		"object label": "jedinci hroubí",
		"object description": "Kmeny jedinců hroubí.",
		"label": "nadzemní biomasa jedinců hroubí", "description": "Hmotnost nadzemní biomasy dřevin jedinců hroubí.",
		"version": {"label": "1. verze", "description": "1. verze."},
		"definition variant": {"label": null, "description": null},
		"area domain": {"label": ["kategorie Les","přístupný a schůdný"], "description": ["Kategorie pozemku FAO FRA - Les.", "Přístupný a schůdný střed."]},
		"population": {"label": ["živí jedinci hroubí"], "description": ["Živí jedinci hroubí včetně živých vývratů."]}, "use_negative": false}]
	},
"en":
	{"indicator": {"label": "agb", "description": "Above ground biomass."},
	"state or change": {"label": "state variable", "description": "State variable."},
	"unit": {"label": "t", "description": "Tons."},
	"local densities": [
		{"object type label": "core",
		"object type description": "Contribution to local density which will be aggregated and is the input into the estimate of total.",
		"object label": "merchantable wood stems",
		"object description": "Merchantable wood stems.",
		"label": "above ground biomass of merch. wood stems", "description": "Above ground biomass of merchantable wood stems.",
		"version": {"label": "version 1", "description": "Version 1."},
		"definition variant": {"label": null, "description": null},
		"area domain": {"label": ["forest category","accessible"], "description": ["FAO FRA forest category","Accessible plot center."]},
		"population": {"label": ["living stems"], "description": ["Living merchantable wood stems including living wind falls."]}, "use_negative": false}]
	}
}'::json
union all
select '{"cs":
	{"indicator": {"label": "zasoba", "description": "Zásoba hroubí."},
	"state or change": {"label": "state variable", "description": "State variable."},
	"unit": {"label": "m3 b.k.", "description": "Metry krychlové bez kůry."},
	"local densities": [
		{"object type label": "core",
		"object type description": "Contribution to local density which will be aggregated and is the input into the estimate of total.",
		"object label": "jedinci hroubí",
		"object description": "Kmeny jedinců hroubí.",
		"label": "objem ÚLT b.k.", "description": "Objem kmene hroubí podle tabulek ÚLT. Bez kůry.",
		"version": {"label": "1. verze", "description": "1. verze."},
		"definition variant": {"label": null, "description": null},
		"area domain": {"label": ["kategorie Les","přístupný a schůdný"], "description": ["Kategorie pozemku FAO FRA - Les.", "Přístupný a schůdný střed."]},
		"population": {"label": ["živí jedinci hroubí"], "description": ["Živí jedinci hroubí včetně živých vývratů."]}, "use_negative": false}]
	},
"en":
	{"indicator": {"label": "vol", "description": "Growing stock of merchantable wood stems."},
	"state or change": {"label": "state variable", "description": "State variable."},
	"unit": {"label": "m3 u.b.", "description": "Cubic meters under bark."},
	"local densities": [
		{"object type label": "core",
		"object type description": "Contribution to local density which will be aggregated and is the input into the estimate of total.",
		"object label": "merchantable wood stems",
		"object description": "Merchantable wood stems.",
		"label": "stem volume ULT u.b.", "description": "Stem volume ULT under bark.",
		"version": {"label": "version 1", "description": "Version 1."},
		"definition variant": {"label": null, "description": null},
		"area domain": {"label": ["forest category","accessible"], "description": ["FAO FRA forest category","Accessible plot center."]},
		"population": {"label": ["living stems"], "description": ["Living merchantable wood stems including living wind falls."]}, "use_negative": false}]
	}
}'::json
;

COPY nfiesta.cm_tvariable2topic  FROM stdin;
1	1	1
2	2	1
3	3	2
4	4	2
\.

--
-- Data for Name: c_sub_population; Type: TABLE DATA; Schema: nfiesta; Owner: vagrant
--

COPY nfiesta.c_sub_population  FROM stdin;
1	coniferous or deciduous	Coniferous or deciduous.	coniferous or deciduous	Coniferous or deciduous.	true
\.

--
-- Data for Name: c_sub_population_category; Type: TABLE DATA; Schema: nfiesta; Owner: vagrant
--

COPY nfiesta.c_sub_population_category  FROM stdin;
1	1	coniferous	Coniferous.	coniferous	Coniferous.
2	1	deciduous	Deciduous.	deciduous	Deciduous.
\.

--
-- Data for Name: c_area_domain; Type: TABLE DATA; Schema: nfiesta; Owner: vagrant
--

COPY nfiesta.c_area_domain  FROM stdin;
1	land registry	Land use indicated by land registry.	land registry	Land use indicated by land registry.	true
\.

--
-- Data for Name: c_area_domain_category; Type: TABLE DATA; Schema: nfiesta; Owner: vagrant
--

COPY nfiesta.c_area_domain_category  FROM stdin;
1	1	forest	Forest area indicated by land registry. (CZ. PUPFL)	forest	Forest area indicated by land registry.
2	1	non-forest	Non-forest area indicated by land registry (CZ. non-PUPFL).	non-forest	Non-forest area indicated by land registry (CZ. non-PUPFL).
\.

--
-- Data for Name: c_auxiliary_variable; Type: TABLE DATA; Schema: nfiesta; Owner: vagrant
--

COPY nfiesta.c_auxiliary_variable  FROM stdin;
1	OLIL 2013	Land use indicated by OLIL 2013.	OLIL 2013	Land use indicated by OLIL 2013.
\.

--
-- Data for Name: c_auxiliary_variable_category; Type: TABLE DATA; Schema: nfiesta; Owner: vagrant
--

COPY nfiesta.c_auxiliary_variable_category  FROM stdin;
1	1	forest	Forest area indicated by OLIL 2013.	forest	Forest area indicated by OLIL 2013.
2	1	non-forest	Non-forest area indicated by OLIL 2013.	non-forest	Non-forest area indicated by OLIL 2013.
\.

--
-- Data for Name: c_estimation_cell_collection; Type: TABLE DATA; Schema: nfiesta; Owner: vagrant
--

COPY nfiesta.c_estimation_cell_collection  FROM stdin;
1	NFR	Natural Forest Regions	NFR	Natural Forest Regions	True
2	NFRD	Natural Forest Region Districts	NFRD	Natural Forest Region Districts	True
3	INSPIRE 50x50km	INSPIRE grid 50x50km	INSPIRE 50x50km	INSPIRE grid 50x50km	True
4	INSPIRE 25x25km	INSPIRE grid 25x25km	INSPIRE 25x25km	INSPIRE grid 25x25km	True
\.


--
-- Data for Name: c_estimation_cell; Type: TABLE DATA; Schema: nfiesta; Owner: vagrant
--

COPY nfiesta.c_estimation_cell  FROM stdin;
1	1	NFR16	NFR16 - Českomoravská vrchovina	\N	\N
2	1	NFR27	NFR27 - Hrubý Jeseník	\N	\N
3	1	NFR28	NFR28 - Předhoří Hrubého Jeseníku	\N	\N
4	1	NFR29	NFR29 - Nízký Jeseník	\N	\N
5	1	NFR30	NFR30 - Drahanská vrchovina	\N	\N
6	1	NFR31	NFR31 - Českomoravské mezihoří	\N	\N
7	1	NFR32	NFR32 - Slezská nížina	\N	\N
8	1	NFR33	NFR33 - Předhoří Českomoravské vrchoviny	\N	\N
9	1	NFR34	NFR34 - Hornomoravský úval	\N	\N
10	1	NFR35	NFR35 - Jihomoravské úvaly	\N	\N
11	1	NFR36	NFR36 - Středomoravské Karpaty	\N	\N
12	1	NFR37	NFR37 - Kelečská pahorkatina	\N	\N
13	1	NFR38	NFR38 - Bílé Karpaty a Vizovické vrchy	\N	\N
14	1	NFR39	NFR39 - Podbeskydská pahorkatina	\N	\N
15	1	NFR40	NFR40 - Moravskoslezské Beskydy	\N	\N
16	1	NFR41	NFR41 - Hostýnské a Vsetínské vrchy a Javorníky	\N	\N
17	3	50kmE460N290	50kmE460N290	\N	\N
18	3	50kmE460N295	50kmE460N295	\N	\N
19	3	50kmE465N285	50kmE465N285	\N	\N
20	3	50kmE465N290	50kmE465N290	\N	\N
21	3	50kmE465N295	50kmE465N295	\N	\N
22	3	50kmE470N285	50kmE470N285	\N	\N
23	3	50kmE470N290	50kmE470N290	\N	\N
24	3	50kmE470N295	50kmE470N295	\N	\N
25	3	50kmE470N300	50kmE470N300	\N	\N
26	3	50kmE475N285	50kmE475N285	\N	\N
27	3	50kmE475N290	50kmE475N290	\N	\N
28	3	50kmE475N295	50kmE475N295	\N	\N
29	3	50kmE475N300	50kmE475N300	\N	\N
30	3	50kmE480N285	50kmE480N285	\N	\N
31	3	50kmE480N290	50kmE480N290	\N	\N
32	3	50kmE480N295	50kmE480N295	\N	\N
33	3	50kmE480N300	50kmE480N300	\N	\N
34	3	50kmE480N305	50kmE480N305	\N	\N
35	3	50kmE485N285	50kmE485N285	\N	\N
36	3	50kmE485N290	50kmE485N290	\N	\N
37	3	50kmE485N295	50kmE485N295	\N	\N
38	3	50kmE485N300	50kmE485N300	\N	\N
39	3	50kmE485N305	50kmE485N305	\N	\N
40	3	50kmE490N290	50kmE490N290	\N	\N
41	3	50kmE490N295	50kmE490N295	\N	\N
42	3	50kmE490N300	50kmE490N300	\N	\N
43	3	50kmE495N295	50kmE495N295	\N	\N
44	4	25kmE4625N2925	25kmE4625N2925	\N	\N
45	4	25kmE4625N2950	25kmE4625N2950	\N	\N
46	4	25kmE4650N2900	25kmE4650N2900	\N	\N
47	4	25kmE4650N2925	25kmE4650N2925	\N	\N
48	4	25kmE4650N2950	25kmE4650N2950	\N	\N
49	4	25kmE4675N2875	25kmE4675N2875	\N	\N
50	4	25kmE4675N2900	25kmE4675N2900	\N	\N
51	4	25kmE4675N2925	25kmE4675N2925	\N	\N
52	4	25kmE4675N2950	25kmE4675N2950	\N	\N
53	4	25kmE4675N2975	25kmE4675N2975	\N	\N
54	4	25kmE4700N2875	25kmE4700N2875	\N	\N
55	4	25kmE4700N2900	25kmE4700N2900	\N	\N
56	4	25kmE4700N2925	25kmE4700N2925	\N	\N
57	4	25kmE4700N2950	25kmE4700N2950	\N	\N
58	4	25kmE4700N2975	25kmE4700N2975	\N	\N
59	4	25kmE4725N2875	25kmE4725N2875	\N	\N
60	4	25kmE4725N2900	25kmE4725N2900	\N	\N
61	4	25kmE4725N2925	25kmE4725N2925	\N	\N
62	4	25kmE4725N2950	25kmE4725N2950	\N	\N
63	4	25kmE4725N2975	25kmE4725N2975	\N	\N
64	4	25kmE4725N3000	25kmE4725N3000	\N	\N
65	4	25kmE4750N2850	25kmE4750N2850	\N	\N
66	4	25kmE4750N2875	25kmE4750N2875	\N	\N
67	4	25kmE4750N2900	25kmE4750N2900	\N	\N
68	4	25kmE4750N2925	25kmE4750N2925	\N	\N
69	4	25kmE4750N2950	25kmE4750N2950	\N	\N
70	4	25kmE4750N2975	25kmE4750N2975	\N	\N
71	4	25kmE4750N3000	25kmE4750N3000	\N	\N
72	4	25kmE4775N2850	25kmE4775N2850	\N	\N
73	4	25kmE4775N2875	25kmE4775N2875	\N	\N
74	4	25kmE4775N2900	25kmE4775N2900	\N	\N
75	4	25kmE4775N2925	25kmE4775N2925	\N	\N
76	4	25kmE4775N2950	25kmE4775N2950	\N	\N
77	4	25kmE4775N2975	25kmE4775N2975	\N	\N
78	4	25kmE4775N3000	25kmE4775N3000	\N	\N
79	4	25kmE4800N2850	25kmE4800N2850	\N	\N
80	4	25kmE4800N2875	25kmE4800N2875	\N	\N
81	4	25kmE4800N2900	25kmE4800N2900	\N	\N
82	4	25kmE4800N2925	25kmE4800N2925	\N	\N
83	4	25kmE4800N2950	25kmE4800N2950	\N	\N
84	4	25kmE4800N2975	25kmE4800N2975	\N	\N
85	4	25kmE4800N3000	25kmE4800N3000	\N	\N
86	4	25kmE4800N3025	25kmE4800N3025	\N	\N
87	4	25kmE4800N3050	25kmE4800N3050	\N	\N
88	4	25kmE4825N2850	25kmE4825N2850	\N	\N
89	4	25kmE4825N2875	25kmE4825N2875	\N	\N
90	4	25kmE4825N2900	25kmE4825N2900	\N	\N
91	4	25kmE4825N2925	25kmE4825N2925	\N	\N
92	4	25kmE4825N2950	25kmE4825N2950	\N	\N
93	4	25kmE4825N2975	25kmE4825N2975	\N	\N
94	4	25kmE4825N3000	25kmE4825N3000	\N	\N
95	4	25kmE4825N3025	25kmE4825N3025	\N	\N
96	4	25kmE4825N3050	25kmE4825N3050	\N	\N
97	4	25kmE4850N2875	25kmE4850N2875	\N	\N
98	4	25kmE4850N2900	25kmE4850N2900	\N	\N
99	4	25kmE4850N2925	25kmE4850N2925	\N	\N
100	4	25kmE4850N2950	25kmE4850N2950	\N	\N
101	4	25kmE4850N2975	25kmE4850N2975	\N	\N
102	4	25kmE4850N3000	25kmE4850N3000	\N	\N
103	4	25kmE4850N3025	25kmE4850N3025	\N	\N
104	4	25kmE4850N3050	25kmE4850N3050	\N	\N
105	4	25kmE4875N2875	25kmE4875N2875	\N	\N
106	4	25kmE4875N2900	25kmE4875N2900	\N	\N
107	4	25kmE4875N2925	25kmE4875N2925	\N	\N
108	4	25kmE4875N2950	25kmE4875N2950	\N	\N
109	4	25kmE4875N2975	25kmE4875N2975	\N	\N
110	4	25kmE4875N3000	25kmE4875N3000	\N	\N
111	4	25kmE4875N3025	25kmE4875N3025	\N	\N
112	4	25kmE4900N2900	25kmE4900N2900	\N	\N
113	4	25kmE4900N2925	25kmE4900N2925	\N	\N
114	4	25kmE4900N2950	25kmE4900N2950	\N	\N
115	4	25kmE4900N2975	25kmE4900N2975	\N	\N
116	4	25kmE4900N3000	25kmE4900N3000	\N	\N
117	4	25kmE4925N2925	25kmE4925N2925	\N	\N
118	4	25kmE4925N2950	25kmE4925N2950	\N	\N
119	4	25kmE4925N2975	25kmE4925N2975	\N	\N
120	4	25kmE4925N3000	25kmE4925N3000	\N	\N
121	4	25kmE4950N2950	25kmE4950N2950	\N	\N
122	4	25kmE4950N2975	25kmE4950N2975	\N	\N
123	2	NFRD7	NFRD7 - Českomoravské vrchoviny	\N	\N
124	2	NFRD11	NFRD11 - Východosudetský	\N	\N
125	2	NFRD12	NFRD12 - Moravského podhůří	\N	\N
126	2	NFRD13	NFRD13 - Moravských úvalů	\N	\N
127	2	NFRD14	NFRD14 - Karpatský	\N	\N
\.

--
-- Data for Name: cm_plot2cell_mapping; Type: TABLE DATA; Schema: nfiesta; Owner: vagrant
--

\set afile :srcdir '/sql/csv/plot_cell_associations.csv'
CREATE FOREIGN TABLE csv.plot_cell_associations (
	country				character varying(20)		not null,
	strata_set			character varying(20)		not null,
	stratum				character varying(20)		not null,
	panel				character varying(20)		not null,
	cluster				character varying(20)		not null,
	plot				character varying(20)		not null,
	cell_collection			character varying(20)		not null,
	cell				character varying(20)		not null,
	comment				text
) SERVER csv_files
OPTIONS ( header 'true', format 'csv', delimiter ';', filename :'afile' );

--CREATE UNIQUE INDEX cm_plot2cell_mapping_plot_estimation_cell_idx ON nfiesta.cm_plot2cell_mapping (plot, estimation_cell);
--ALTER TABLE nfiesta.cm_plot2cell_mapping ADD CONSTRAINT cm_plot2cell_mapping__unique_estimation_cell UNIQUE USING INDEX cm_plot2cell_mapping_plot_estimation_cell_idx;

with w_data as (
	select
		f_p_plot.gid,
		c_estimation_cell.id
	from sdesign.c_country
	join sdesign.t_strata_set 			on t_strata_set.country = c_country.id
	join sdesign.t_stratum 				on t_stratum.strata_set = t_strata_set.id
	join sdesign.t_panel				on t_panel.stratum = t_stratum.id
	join sdesign.cm_cluster2panel_mapping 		on cm_cluster2panel_mapping.panel = t_panel.id
	join sdesign.t_cluster 				on t_cluster.id = cm_cluster2panel_mapping.cluster
	join sdesign.f_p_plot 				on f_p_plot.cluster = t_cluster.id
	join sdesign.t_cluster_configuration 		on t_cluster_configuration.id = t_panel.cluster_configuration
	join sdesign.cm_plot2cluster_config_mapping 	on (
		cm_plot2cluster_config_mapping.cluster_configuration = t_cluster_configuration.id and
		cm_plot2cluster_config_mapping.plot = f_p_plot.gid)
	----------------------------------------
	join csv.plot_cell_associations as t		on (
		c_country.label = t.country and
		t_strata_set.strata_set = t.strata_set and
		t_stratum.stratum = t.stratum and
		t_panel.panel = t.panel and
		t_cluster.cluster = t.cluster and
		f_p_plot.plot = t.plot)
	----------------------------------------
	join nfiesta.c_estimation_cell		on (c_estimation_cell.label = t.cell)
	join nfiesta.c_estimation_cell_collection 	on (
		c_estimation_cell.estimation_cell_collection = c_estimation_cell_collection.id and
		c_estimation_cell_collection.label = t.cell_collection)
)
insert into nfiesta.cm_plot2cell_mapping(plot, estimation_cell)
select
	gid, id
from w_data
order by gid, id
;

--
-- Data for Name: t_variable; Type: TABLE DATA; Schema: nfiesta; Owner: vagrant
--

COPY nfiesta.t_variable  FROM stdin;
1	1	\N	1	\N
2	1	\N	2	\N
3	1	\N	\N	\N
4	2	\N	\N	\N
5	\N	\N	\N	1
6	\N	\N	\N	2
\.

--
-- Data for Name: t_variable_hierarchy; Type: TABLE DATA; Schema: nfiesta; Owner: vagrant
--

\set afile :srcdir '/sql/csv/variable_hierarchy.csv'
CREATE FOREIGN TABLE csv.variable_hierarchy (
	target_variable			integer		not null,
	area_domain_category_sup	integer,
	sub_population_category_sup	integer,
	area_domain_category		integer,
	sub_population_category		integer
) SERVER csv_files
OPTIONS ( header 'true', format 'csv', delimiter ';', filename :'afile' );

with w_data as (
	select
		(select id from nfiesta.t_variable
			where t_variable.target_variable                = variable_hierarchy.target_variable and
			coalesce(t_variable.area_domain_category,0)     = coalesce(variable_hierarchy.area_domain_category,0) AND
			coalesce(t_variable.sub_population_category,0)  = coalesce(variable_hierarchy.sub_population_category,0)
		) as variable,
		(select id from nfiesta.t_variable
			where t_variable.target_variable                = variable_hierarchy.target_variable and
			coalesce(t_variable.area_domain_category,0)     = coalesce(variable_hierarchy.area_domain_category_sup,0) AND
			coalesce(t_variable.sub_population_category,0)  = coalesce(variable_hierarchy.sub_population_category_sup,0)
		) as variable_superior
	from csv.variable_hierarchy
)
INSERT INTO nfiesta.t_variable_hierarchy (variable, variable_superior)
select variable, variable_superior from w_data
where variable is not null and variable_superior is not null
;

--
-- Data for Name: t_available_datasets; Type: TABLE DATA; Schema: nfiesta; Owner: vagrant
--

\set afile :srcdir '/sql/csv/available_datasets.csv'
CREATE FOREIGN TABLE csv.available_datasets (
	country				character varying(20)		not null,
	strata_set			character varying(20)		not null,
	stratum				character varying(20)		not null,
	panel				character varying(20)		not null,
	reference_year_set		character varying(20),
	target_variable			integer,
	sub_population			integer,
	sub_population_category		integer,
	area_domain			integer,
	area_domain_category		integer,
	auxiliary_variable		integer,
	auxiliary_variable_category	integer,
	comment				text
) SERVER csv_files
OPTIONS ( header 'true', format 'csv', delimiter ';', filename :'afile' );

with w_data as (
	select
		t_panel.id as panel,
		t_reference_year_set.id as reference_year_set,
		t_variable.id as variable
	from sdesign.c_country
	join sdesign.t_strata_set 			on t_strata_set.country = c_country.id
	join sdesign.t_stratum 				on t_stratum.strata_set = t_strata_set.id
	join sdesign.t_panel				on t_panel.stratum = t_stratum.id
	join sdesign.cm_refyearset2panel_mapping	on cm_refyearset2panel_mapping.panel = t_panel.id
	join sdesign.t_reference_year_set		on cm_refyearset2panel_mapping.reference_year_set = t_reference_year_set.id
	----------------------------------------
	join csv.available_datasets as t		on (
		c_country.label = t.country and
		t_strata_set.strata_set = t.strata_set and
		t_stratum.stratum = t.stratum and
		t_panel.panel = t.panel and
		t_reference_year_set.reference_year_set = t.reference_year_set
	)
	----------------------------------------
	join nfiesta.t_variable 				on (
			t_variable.target_variable = t.target_variable and
			coalesce(t_variable.sub_population_category, 0) = coalesce(t.sub_population_category, 0) and
			coalesce(t_variable.area_domain_category, 0) = coalesce(t.area_domain_category, 0)
		)
)
, w_data_aux as (
	select
		t_panel.id as panel,
		t_variable.id as variable
	from sdesign.c_country
	join sdesign.t_strata_set 			on t_strata_set.country = c_country.id
	join sdesign.t_stratum 				on t_stratum.strata_set = t_strata_set.id
	join sdesign.t_panel				on t_panel.stratum = t_stratum.id
	----------------------------------------
	join csv.available_datasets as t		on (
		c_country.label = t.country and
		t_strata_set.strata_set = t.strata_set and
		t_stratum.stratum = t.stratum and
		t_panel.panel = t.panel
	)
	----------------------------------------
	join nfiesta.t_variable 				on (
			t_variable.auxiliary_variable_category = t.auxiliary_variable_category
		)
)
insert into nfiesta.t_available_datasets(panel, reference_year_set, variable, ldsity_threshold, last_change)
select
	panel, reference_year_set, variable, 0.0000000001 as ldsity_threshold, now() as last_change
from w_data
union all
select
	panel, null as reference_year_set, variable, 0.0000000001 as ldsity_threshold, now() as last_change
from w_data_aux
order by panel, reference_year_set, variable, ldsity_threshold, last_change
;


--
-- Data for Name: t_auxiliary_data; Type: TABLE DATA; Schema: nfiesta; Owner: vagrant
--

\set afile :srcdir '/sql/csv/plot_auxiliary_data_before.csv'
CREATE FOREIGN TABLE csv.plot_auxiliary_data (
	country				character varying(20)		not null,
	strata_set			character varying(20)		not null,
	stratum				character varying(20)		not null,
	panel				character varying(20)		not null,
	cluster				character varying(20)		not null,
	plot				character varying(20)		not null,
	tile				character varying(20),
	auxiliary_variable		character varying(20)		not null,
	auxiliary_variable_category	character varying(20)		not null,
	value				double precision		not null,
	comment				text
) SERVER csv_files
OPTIONS ( header 'true', format 'csv', delimiter ';', filename :'afile' );

with w_data as (
	select
		f_p_plot.gid	as plot,
		t_panel.id 	as panel,
		t_variable.id 	as variable,
		t.value
	from sdesign.c_country
	join sdesign.t_strata_set 			on t_strata_set.country = c_country.id
	join sdesign.t_stratum 				on t_stratum.strata_set = t_strata_set.id
	join sdesign.t_panel				on t_panel.stratum = t_stratum.id
	join sdesign.cm_cluster2panel_mapping 		on cm_cluster2panel_mapping.panel = t_panel.id
	join sdesign.t_cluster 				on t_cluster.id = cm_cluster2panel_mapping.cluster
	join sdesign.f_p_plot 				on f_p_plot.cluster = t_cluster.id
	join sdesign.t_cluster_configuration 		on t_cluster_configuration.id = t_panel.cluster_configuration
	join sdesign.cm_plot2cluster_config_mapping 	on (
		cm_plot2cluster_config_mapping.cluster_configuration = t_cluster_configuration.id and
		cm_plot2cluster_config_mapping.plot = f_p_plot.gid)
	----------------------------------------
	join csv.plot_auxiliary_data as t		on (
		c_country.label = t.country and
		t_strata_set.strata_set = t.strata_set and
		t_stratum.stratum = t.stratum and
		t_panel.panel = t.panel and
		t_cluster.cluster = t.cluster and
		f_p_plot.plot = t.plot)
	----------------------------------------
	join nfiesta.c_auxiliary_variable		on (c_auxiliary_variable.label = t.auxiliary_variable)
	join nfiesta.c_auxiliary_variable_category	on (
		c_auxiliary_variable_category.auxiliary_variable = c_auxiliary_variable.id and
		c_auxiliary_variable_category.label = t.auxiliary_variable_category)
	join nfiesta.t_variable on (t_variable.auxiliary_variable_category = c_auxiliary_variable_category.id)
)
insert into nfiesta.t_auxiliary_data(plot, available_datasets, value)
select
	plot,
	(	select
			id as available_datasets
		from nfiesta.t_available_datasets
		where t_available_datasets.panel = w_data.panel
		and t_available_datasets.variable = w_data.variable
	),
	value
from w_data
where value != 0;

--
-- Data for Name: t_target_data; Type: TABLE DATA; Schema: nfiesta; Owner: vagrant
--

\set afile :srcdir '/sql/csv/plot_target_data_before.csv'
CREATE FOREIGN TABLE csv.plot_target_data (
	country				character varying(20)		not null,
	inventory_campaign		character varying(20)		not null,
	reference_year_set		character varying(20)		not null,
	strata_set			character varying(20)		not null,
	stratum				character varying(20)		not null,
	panel				character varying(20)		not null,
	cluster				character varying(20)		not null,
	plot				character varying(20)		not null,
	target_variable			character varying(20)		not null,
	sub_population			character varying(20)		not null,
	sub_population_category		character varying(20)		not null,
	area_domain			character varying(20)		not null,
	area_domain_category		character varying(20)		not null,
	value				double precision		not null,
	comment				text
) SERVER csv_files
OPTIONS ( header 'true', format 'csv', delimiter ';', filename :'afile' );

with w_data as (
	select
		f_p_plot.gid		as plot,
		t_panel.id 		as panel,
		t_variable.id 		as variable,
		t_reference_year_set.id	as reference_year_set,
		t.value
	from sdesign.c_country
	join sdesign.t_strata_set 			on t_strata_set.country = c_country.id
	join sdesign.t_stratum 				on t_stratum.strata_set = t_strata_set.id
	join sdesign.t_panel				on t_panel.stratum = t_stratum.id
	join sdesign.cm_cluster2panel_mapping 		on cm_cluster2panel_mapping.panel = t_panel.id
	join sdesign.t_cluster 				on t_cluster.id = cm_cluster2panel_mapping.cluster
	join sdesign.f_p_plot 				on f_p_plot.cluster = t_cluster.id
	join sdesign.t_cluster_configuration 		on t_cluster_configuration.id = t_panel.cluster_configuration
	join sdesign.cm_plot2cluster_config_mapping 	on (
		cm_plot2cluster_config_mapping.cluster_configuration = t_cluster_configuration.id and
		cm_plot2cluster_config_mapping.plot = f_p_plot.gid)
	join sdesign.cm_refyearset2panel_mapping	on cm_refyearset2panel_mapping.panel = t_panel.id
	join sdesign.t_reference_year_set		on t_reference_year_set.id = cm_refyearset2panel_mapping.reference_year_set
	join sdesign.t_inventory_campaign		on t_inventory_campaign.id = t_reference_year_set.inventory_campaign
	----------------------------------------
	join csv.plot_target_data as t			on (
		c_country.label = t.country and
		t_strata_set.strata_set = t.strata_set and
		t_stratum.stratum = t.stratum and
		t_panel.panel = t.panel and
		t_cluster.cluster = t.cluster and
		f_p_plot.plot = t.plot and
		t_reference_year_set.reference_year_set = t.reference_year_set and
		t_inventory_campaign.inventory_campaign = t.inventory_campaign)
	----------------------------------------
	join nfiesta.c_target_variable		on (c_target_variable.metadata->'en'->'indicator'->>'label' = t.target_variable)
	left join nfiesta.c_sub_population			on (c_sub_population.label = t.sub_population)
	left join nfiesta.c_sub_population_category	on (
		c_sub_population_category.sub_population = c_sub_population.id and
		c_sub_population_category.label = t.sub_population_category)
	left join nfiesta.c_area_domain			on (c_area_domain.label = t.area_domain)
	left join nfiesta.c_area_domain_category		on (
		c_area_domain_category.area_domain = c_area_domain.id and
		c_area_domain_category.label = t.area_domain_category)
	join nfiesta.t_variable 				on (
		t_variable.target_variable = c_target_variable.id and
		coalesce(t_variable.sub_population_category, 0) = coalesce(c_sub_population_category.id, 0) and
		coalesce(t_variable.area_domain_category, 0) = coalesce(c_area_domain_category.id, 0))
)
insert into nfiesta.t_target_data(plot, available_datasets, value)
select
	plot,
	(	select
			id as available_datasets
		from nfiesta.t_available_datasets
		where t_available_datasets.panel = w_data.panel
		and t_available_datasets.variable = w_data.variable
		and t_available_datasets.reference_year_set = w_data.reference_year_set
	),
	value
from w_data
where value != 0;

--
-- Name: t_available_datasets_id_seq; Type: SEQUENCE SET; Schema: nfiesta; Owner: vagrant
--

SELECT pg_catalog.setval('nfiesta.t_available_datasets_id_seq', (select max(id) from nfiesta.t_available_datasets), true);

--
-- Name: c_area_domain_category_id_seq; Type: SEQUENCE SET; Schema: nfiesta; Owner: vagrant
--

SELECT pg_catalog.setval('nfiesta.c_area_domain_category_id_seq', (select max(id) from nfiesta.c_area_domain_category), true);


--
-- Name: c_area_domain_id_seq; Type: SEQUENCE SET; Schema: nfiesta; Owner: vagrant
--

SELECT pg_catalog.setval('nfiesta.c_area_domain_id_seq', (select max(id) from nfiesta.c_area_domain), true);


--
-- Name: c_auxiliary_variable_category_id_seq; Type: SEQUENCE SET; Schema: nfiesta; Owner: vagrant
--

SELECT pg_catalog.setval('nfiesta.c_auxiliary_variable_category_id_seq', (select max(id) from nfiesta.c_auxiliary_variable_category), true);


--
-- Name: c_auxiliary_variable_id_seq; Type: SEQUENCE SET; Schema: nfiesta; Owner: vagrant
--

SELECT pg_catalog.setval('nfiesta.c_auxiliary_variable_id_seq', (select max(id) from nfiesta.c_auxiliary_variable), true);


--
-- Name: c_estimation_cell_collection_id_seq; Type: SEQUENCE SET; Schema: nfiesta; Owner: vagrant
--

SELECT pg_catalog.setval('nfiesta.c_estimation_cell_collection_id_seq', (select max(id) from nfiesta.c_estimation_cell_collection), true);


--
-- Name: c_estimation_cell_id_seq; Type: SEQUENCE SET; Schema: nfiesta; Owner: vagrant
--

SELECT pg_catalog.setval('nfiesta.c_estimation_cell_id_seq', (select max(id) from nfiesta.c_estimation_cell), true);


--
-- Name: c_sub_population_category_id_seq; Type: SEQUENCE SET; Schema: nfiesta; Owner: vagrant
--

SELECT pg_catalog.setval('nfiesta.c_sub_population_category_id_seq', (select max(id) from nfiesta.c_sub_population_category), true);


--
-- Name: c_sub_population_id_seq; Type: SEQUENCE SET; Schema: nfiesta; Owner: vagrant
--

SELECT pg_catalog.setval('nfiesta.c_sub_population_id_seq', (select max(id) from nfiesta.c_sub_population), true);


--
-- Name: c_target_variable_id_seq; Type: SEQUENCE SET; Schema: nfiesta; Owner: vagrant
--

SELECT pg_catalog.setval('nfiesta.c_target_variable_id_seq', (select max(id) from nfiesta.c_target_variable), true);


--
-- Name: c_topic_id_seq; Type: SEQUENCE SET; Schema: nfiesta; Owner: vagrant
--

SELECT pg_catalog.setval('nfiesta.c_topic_id_seq', (select max(id) from nfiesta.c_topic), true);


--
-- Name: cm_tvariable2topic_id_seq; Type: SEQUENCE SET; Schema: nfiesta; Owner: vagrant
--

SELECT pg_catalog.setval('nfiesta.cm_tvariable2topic_id_seq', (select max(id) from nfiesta.cm_tvariable2topic), true);


--
-- Name: cm_plot2cell_mapping_id_seq; Type: SEQUENCE SET; Schema: nfiesta; Owner: vagrant
--

SELECT pg_catalog.setval('nfiesta.cm_plot2cell_mapping_id_seq', (select max(id) from nfiesta.cm_plot2cell_mapping), true);


--
-- Name: t_auxiliary_data_id_seq; Type: SEQUENCE SET; Schema: nfiesta; Owner: vagrant
--
SELECT pg_catalog.setval('nfiesta.t_auxiliary_data_id_seq', (select max(id) from nfiesta.t_auxiliary_data), true);


--
-- Name: t_target_data_id_seq; Type: SEQUENCE SET; Schema: nfiesta; Owner: vagrant
--

SELECT pg_catalog.setval('nfiesta.t_target_data_id_seq', (select max(id) from nfiesta.t_target_data), true);

--
-- Name: t_variable_id_seq; Type: SEQUENCE SET; Schema: nfiesta; Owner: vagrant
--

SELECT pg_catalog.setval('nfiesta.t_variable_id_seq', (select max(id) from nfiesta.t_variable), true);

--
-- PostgreSQL database dump complete
--
