\set srcdir `echo $SRC_DIR`

-- test on unique constraint - should fail
insert into nfiesta.t_target_data (plot, value, value_inserted, is_latest, available_datasets)
select plot, value, value_inserted, is_latest, available_datasets
from nfiesta.t_target_data
order by plot, available_datasets, is_latest
limit 5;

/*update nfiesta.t_target_data set is_latest = false
where plot in (select plot from nfiesta.cm_plot2cell_mapping where estimation_cell = 27);

insert into nfiesta.t_target_data (plot, value, reference_year_set, variable, is_latest)
select plot, value, reference_year_set, variable, true as is_latest
from nfiesta.t_target_data
where plot in (select plot from nfiesta.cm_plot2cell_mapping where estimation_cell = 27);

update nfiesta.t_target_data set value = value + 13.13
where plot in (select plot from nfiesta.cm_plot2cell_mapping where estimation_cell = 27)
and not is_latest;
*/

DROP INDEX IF EXISTS sdesign.idx__cm_cluster2panel_mapping__panel;
DROP INDEX IF EXISTS sdesign.idx__f_p_plot__cluster;

DROP FOREIGN TABLE csv.plot_target_data;

\set afile :srcdir '/sql/csv/plot_target_data.csv'
CREATE FOREIGN TABLE csv.plot_target_data (
	country				character varying(20)		not null,
	inventory_campaign		character varying(20)		not null,
	reference_year_set		character varying(20)		not null,
	strata_set			character varying(20)		not null,
	stratum				character varying(20)		not null,
	panel				character varying(20)		not null,
	cluster				character varying(20)		not null,
	plot				character varying(20)		not null,
	target_variable			character varying(20)		not null,
	sub_population			character varying(20)		not null,
	sub_population_category		character varying(20)		not null,
	area_domain			character varying(20)		not null,
	area_domain_category		character varying(20)		not null,
	value				double precision		not null,
	comment				text
) SERVER csv_files
OPTIONS ( header 'true', format 'csv', delimiter ';', filename :'afile' );

WITH w_existing AS (
	-- assuming all data already imported are either non-0, or 0s are included
	SELECT
		plot, value, available_datasets, is_latest, max(value) OVER(PARTITION BY plot, available_datasets) AS max_value
	FROM
		nfiesta.t_target_data
)
, w_data AS (
	-- newly imported csv
	select
		f_p_plot.gid		as plot,
		t_panel.id 		as panel,
		t_variable.id 		as variable,
		t_reference_year_set.id	as reference_year_set,
		t.value
	from sdesign.c_country
	join sdesign.t_strata_set 			on t_strata_set.country = c_country.id
	join sdesign.t_stratum 				on t_stratum.strata_set = t_strata_set.id
	join sdesign.t_panel				on t_panel.stratum = t_stratum.id
	join sdesign.cm_cluster2panel_mapping 		on cm_cluster2panel_mapping.panel = t_panel.id
	join sdesign.t_cluster 				on t_cluster.id = cm_cluster2panel_mapping.cluster
	join sdesign.f_p_plot 				on f_p_plot.cluster = t_cluster.id
	join sdesign.t_cluster_configuration 		on t_cluster_configuration.id = t_panel.cluster_configuration
	join sdesign.cm_plot2cluster_config_mapping 	on (
		cm_plot2cluster_config_mapping.cluster_configuration = t_cluster_configuration.id and
		cm_plot2cluster_config_mapping.plot = f_p_plot.gid)
	join sdesign.cm_refyearset2panel_mapping	on cm_refyearset2panel_mapping.panel = t_panel.id
	join sdesign.t_reference_year_set		on t_reference_year_set.id = cm_refyearset2panel_mapping.reference_year_set
	join sdesign.t_inventory_campaign		on t_inventory_campaign.id = t_reference_year_set.inventory_campaign
	----------------------------------------
	join csv.plot_target_data as t			on (
		c_country.label = t.country and
		t_strata_set.strata_set = t.strata_set and
		t_stratum.stratum = t.stratum and
		t_panel.panel = t.panel and
		t_cluster.cluster = t.cluster and
		f_p_plot.plot = t.plot and
		t_reference_year_set.reference_year_set = t.reference_year_set and
		t_inventory_campaign.inventory_campaign = t.inventory_campaign)
	----------------------------------------
	join nfiesta.c_target_variable		on (c_target_variable.metadata->'en'->'indicator'->>'label' = t.target_variable)
	left join nfiesta.c_sub_population			on (c_sub_population.label = t.sub_population)
	left join nfiesta.c_sub_population_category	on (
		c_sub_population_category.sub_population = c_sub_population.id and
		c_sub_population_category.label = t.sub_population_category)
	left join nfiesta.c_area_domain			on (c_area_domain.label = t.area_domain)
	left join nfiesta.c_area_domain_category		on (
		c_area_domain_category.area_domain = c_area_domain.id and
		c_area_domain_category.label = t.area_domain_category)
	join nfiesta.t_variable 				on (
		t_variable.target_variable = c_target_variable.id and
		coalesce(t_variable.sub_population_category, 0) = coalesce(c_sub_population_category.id, 0) and
		coalesce(t_variable.area_domain_category, 0) = coalesce(c_area_domain_category.id, 0))
)
, w_new AS (
	select
	plot,
	(
		select
			id as available_datasets
		from nfiesta.t_available_datasets
		where 		t_available_datasets.panel = w_data.panel
			and 	t_available_datasets.reference_year_set = w_data.reference_year_set
			and 	t_available_datasets.variable = w_data.variable
	),
	value
	from w_data
)
, w_compare AS(
	SELECT
		coalesce(t2.plot, t1.plot) AS plot,
		coalesce(t2.available_datasets, t1.available_datasets) AS available_datasets,
		coalesce(t2.value,0) AS new_value,
		t1.value AS old_value
	FROM
		w_existing AS t1
	-- using this type of join, these cases should appear:
	-- 1.exists in both tables, and values are the same - not import
	-- 2.exists in both tables, and values are different - import
	-- 3.in t1 does not exist (always 0) but in t2 yes and is 0 (stays 0) - not import
	-- 4.in t1 does not exist (always 0) but in t2 yes and is non-0 - import
	-- 5.in t1 exists as 0 and in t2 not (stays 0) - not import
	-- 6.in t1 exists as non-0 and in t2 not (newly 0) - import

	-- conclusion: always import already existing, new records only if they are non-0
	FULL OUTER JOIN
		w_new AS t2
	ON
		t1.plot = t2.plot AND
		t1.available_datasets = t2.available_datasets
),
w_which_one AS (
	SELECT
		plot, new_value AS value, available_datasets
	FROM
		w_compare
	WHERE
		(old_value IS NOT NULL AND old_value != new_value) OR 	-- if already exists, and the value changed (2.), (6.)
		old_value IS NULL AND new_value != 0			-- of not exists and new is not 0 (4.)

),
w_update AS (
	UPDATE nfiesta.t_target_data SET is_latest = false
	FROM
		w_which_one AS t1
	WHERE
		t_target_data.plot = t1.plot AND
		t_target_data.available_datasets = t1.available_datasets
	RETURNING t1.plot, t1.value, t1.available_datasets
)
INSERT INTO nfiesta.t_target_data(plot, value, available_datasets)
SELECT
	coalesce(t2.plot, t1.plot),
	coalesce(t2.value, t1.value),
	coalesce(t2.available_datasets, t1.available_datasets)
FROM
	w_which_one AS t1
-- this is a workaround for UPDATE to be fired
-- the CTE w_update itself would not be executed at all
-- maybe this should be replaced by a trigger
LEFT JOIN
	w_update AS t2
ON
	t1.plot = t2.plot AND
	t1.available_datasets = t2.available_datasets
;

select
	plot, value, available_datasets, is_latest
from nfiesta.t_target_data
where plot in (select plot from nfiesta.cm_plot2cell_mapping where estimation_cell = 27)
order by plot, available_datasets, is_latest;


-----------------------------------------------------------------------
-- test on unique constraint - should fail
insert into nfiesta.t_auxiliary_data (plot, value, available_datasets, value_inserted, is_latest)
select plot, value, available_datasets, value_inserted, is_latest
from nfiesta.t_auxiliary_data
order by plot, available_datasets, is_latest
limit 5;
/*
update nfiesta.t_auxiliary_data set is_latest = false
where plot in (select plot from nfiesta.cm_plot2cell_mapping where estimation_cell = 27);

insert into nfiesta.t_auxiliary_data (plot, value, variable, is_latest)
select plot, value, variable, true as is_latest
from nfiesta.t_auxiliary_data
where plot in (select plot from nfiesta.cm_plot2cell_mapping where estimation_cell = 27);

update nfiesta.t_auxiliary_data set value = value + 14.14
where plot in (select plot from nfiesta.cm_plot2cell_mapping where estimation_cell = 27)
and not is_latest;
*/
DROP FOREIGN TABLE csv.plot_auxiliary_data;

\set afile :srcdir '/sql/csv/plot_auxiliary_data.csv'
CREATE FOREIGN TABLE csv.plot_auxiliary_data (
	country				character varying(20)		not null,
	strata_set			character varying(20)		not null,
	stratum				character varying(20)		not null,
	panel				character varying(20)		not null,
	cluster				character varying(20)		not null,
	plot				character varying(20)		not null,
	tile				character varying(20),
	auxiliary_variable		character varying(20)		not null,
	auxiliary_variable_category	character varying(20)		not null,
	value				double precision		not null,
	comment				text
) SERVER csv_files
OPTIONS ( header 'true', format 'csv', delimiter ';', filename :'afile' );

WITH w_existing AS (
	-- assuming all data already imported are either non-0, or 0s are included
	SELECT
		plot, value, available_datasets, is_latest, max(value) OVER(PARTITION BY plot, available_datasets) AS max_value
	FROM
		nfiesta.t_auxiliary_data
),
w_data AS (
	-- newly imported csv
	select
		f_p_plot.gid	as plot,
		t_panel.id 	as panel,
		t_variable.id 	as variable,
		t.value
	from sdesign.c_country
	join sdesign.t_strata_set 			on t_strata_set.country = c_country.id
	join sdesign.t_stratum 				on t_stratum.strata_set = t_strata_set.id
	join sdesign.t_panel				on t_panel.stratum = t_stratum.id
	join sdesign.cm_cluster2panel_mapping 		on cm_cluster2panel_mapping.panel = t_panel.id
	join sdesign.t_cluster 				on t_cluster.id = cm_cluster2panel_mapping.cluster
	join sdesign.f_p_plot 				on f_p_plot.cluster = t_cluster.id
	join sdesign.t_cluster_configuration 		on t_cluster_configuration.id = t_panel.cluster_configuration
	join sdesign.cm_plot2cluster_config_mapping 	on (
		cm_plot2cluster_config_mapping.cluster_configuration = t_cluster_configuration.id and
		cm_plot2cluster_config_mapping.plot = f_p_plot.gid)
	----------------------------------------
	join csv.plot_auxiliary_data as t		on (
		c_country.label = t.country and
		t_strata_set.strata_set = t.strata_set and
		t_stratum.stratum = t.stratum and
		t_panel.panel = t.panel and
		t_cluster.cluster = t.cluster and
		f_p_plot.plot = t.plot)
	----------------------------------------
	join nfiesta.c_auxiliary_variable		on (c_auxiliary_variable.label = t.auxiliary_variable)
	join nfiesta.c_auxiliary_variable_category	on (
		c_auxiliary_variable_category.auxiliary_variable = c_auxiliary_variable.id and
		c_auxiliary_variable_category.label = t.auxiliary_variable_category)
	join nfiesta.t_variable on (t_variable.auxiliary_variable_category = c_auxiliary_variable_category.id)
)
, w_new as (
	select
	plot,
	(
		select
			id as available_datasets
		from nfiesta.t_available_datasets
		where 		t_available_datasets.panel = w_data.panel
			and 	t_available_datasets.variable = w_data.variable
	),
	value
	from w_data
)
, w_compare AS(
	SELECT
		coalesce(t2.plot, t1.plot) AS plot,
		coalesce(t2.available_datasets, t1.available_datasets) AS available_datasets,
		coalesce(t2.value,0) AS new_value,
		t1.value AS old_value
	FROM
		w_existing AS t1
	-- using this type of join, these cases should appear:
	-- 1.exists in both tables, and values are the same - not import
	-- 2.exists in both tables, and values are different - import
	-- 3.in t1 does not exist (always 0) but in t2 yes and is 0 (stays 0) - not import
	-- 4.in t1 does not exist (always 0) but in t2 yes and is non-0 - import
	-- 5.in t1 exists as 0 and in t2 not (stays 0) - not import
	-- 6.in t1 exists as non-0 and in t2 not (newly 0) - import

	-- conclusion: always import already existing, new records only if they are non-0
	FULL OUTER JOIN
		w_new AS t2
	ON
		t1.plot = t2.plot AND
		t1.available_datasets = t2.available_datasets
),
w_which_one AS (
	SELECT
		plot, new_value AS value, available_datasets
	FROM
		w_compare
	WHERE
		(old_value IS NOT NULL AND old_value != new_value) OR 	-- if already exists, and the value changed (2.), (6.)
		old_value IS NULL AND new_value != 0			-- of not exists and new is not 0 (4.)

),
w_update AS (
	UPDATE nfiesta.t_auxiliary_data SET is_latest = false
	FROM
		w_which_one AS t1
	WHERE
		t_auxiliary_data.plot = t1.plot AND
		t_auxiliary_data.available_datasets = t1.available_datasets
	RETURNING t1.plot, t1.available_datasets, t1.value
)
INSERT INTO nfiesta.t_auxiliary_data(plot, value, available_datasets)
SELECT
	coalesce(t2.plot, t1.plot),
	coalesce(t2.value, t1.value),
	coalesce(t2.available_datasets, t1.available_datasets)
FROM
	w_which_one AS t1
-- this is a workaround for UPDATE to be fired
-- the CTE w_update itself would not be executed at all
-- maybe this should be replaced by a trigger
LEFT JOIN
	w_update AS t2
ON
	t1.plot = t2.plot AND
	t1.available_datasets = t2.available_datasets
;

select
	plot, value, available_datasets, is_latest
from nfiesta.t_auxiliary_data
where plot in (select plot from nfiesta.cm_plot2cell_mapping where estimation_cell = 27)
order by plot, available_datasets, is_latest;

-----------------------------------------------------------------------

insert into nfiesta.t_aux_total (estimation_cell, aux_total, variable, aux_total_inserted, is_latest)
select estimation_cell, aux_total, variable, aux_total_inserted, is_latest
from nfiesta.t_aux_total
order by estimation_cell, variable, is_latest
limit 5;

update nfiesta.t_aux_total set is_latest = false
where estimation_cell = 27;

insert into nfiesta.t_aux_total (estimation_cell, aux_total, variable, aux_total_inserted, is_latest)
select estimation_cell, aux_total, variable, aux_total_inserted, true as is_latest
from nfiesta.t_aux_total
where estimation_cell = 27;

update nfiesta.t_aux_total set aux_total = aux_total + 15.15
where estimation_cell = 27
and not is_latest;

select
	estimation_cell, aux_total, variable, is_latest
from nfiesta.t_aux_total
where estimation_cell = 27
order by estimation_cell, variable, is_latest;

CREATE INDEX idx__cm_cluster2panel_mapping__panel ON sdesign.cm_cluster2panel_mapping USING btree (panel) INCLUDE (cluster, sampling_weight_ha);
CREATE INDEX idx__f_p_plot__cluster ON sdesign.f_p_plot USING btree (cluster) INCLUDE (gid);
