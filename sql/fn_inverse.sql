--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
-----------------------------------------
-- all 0 in one row
-----------------------------------------
WITH w AS (
	SELECT	array[
			array[2,0,3],
			array[3,2,4],
			array[0,0,0]
		] AS mat
)
, w_res AS (
	SELECT nfiesta.fn_inverse(mat) as mi FROM w
)
SELECT round(t.val::numeric, 10) as val, t.i 
	FROM unnest((SELECT mi FROM w_res)) WITH ORDINALITY AS t(val, i) 
	ORDER BY t.i;
-----------------------------------------
-- all 0 in one iumn
-----------------------------------------
WITH w AS (
	SELECT	array[
			array[0,2,3],
			array[0,2,4],
			array[0,6,5]
		] AS mat
)
, w_res AS (
	SELECT nfiesta.fn_inverse(mat) as mi FROM w
)
SELECT round(t.val::numeric, 10) as val, t.i 
	FROM unnest((SELECT mi FROM w_res)) WITH ORDINALITY AS t(val, i) 
	ORDER BY t.i;
-----------------------------------------
-- 1 row is a linear combination of 2 others
-----------------------------------------
WITH w AS (
	SELECT	array[
			array[1,2,3],
			array[2,2,1],
			array[3,4,4]
		] AS mat
)
, w_res AS (
	SELECT nfiesta.fn_inverse(mat) as mi FROM w
)
SELECT round(t.val::numeric, 10) as val, t.i 
	FROM unnest((SELECT mi FROM w_res)) WITH ORDINALITY AS t(val, i) 
	ORDER BY t.i;
-----------------------------------------
--  last column result of linear combination of first two columns (z = ax + by where a = 1, b = −1)
-----------------------------------------
WITH w AS (
	SELECT	array[
			array[2,2,0],
			array[4,2,2],
			array[1,6,-5]
		] AS mat
)
, w_res AS (
	SELECT nfiesta.fn_inverse(mat) as mi FROM w
)
SELECT round(t.val::numeric, 10) as val, t.i 
	FROM unnest((SELECT mi FROM w_res)) WITH ORDINALITY AS t(val, i) 
	ORDER BY t.i;
-----------------------------------------
-- OK
-----------------------------------------
WITH w AS (
	SELECT	array[
			array[2,2,1],
			array[4,-2,3],
			array[0,6,3]
		] AS mat
)
, w_res AS (
	SELECT nfiesta.fn_inverse(mat) as mi FROM w
)
SELECT round(t.val::numeric, 10) as val, t.i 
	FROM unnest((SELECT mi FROM w_res)) WITH ORDINALITY AS t(val, i) 
	ORDER BY t.i;
-----------------------------------------
-- OK (https://docs.scipy.org/doc/scipy/reference/tutorial/linalg.html#finding-the-inverse)
-----------------------------------------
WITH w AS (
	SELECT	array[
			array[1,3,5],
			array[2,5,1],
			array[2,3,8]
		] AS mat
)
, w_res AS (
	SELECT nfiesta.fn_inverse(mat) as mi FROM w
)
SELECT round(t.val::numeric, 10) as val, t.i 
	FROM unnest((SELECT mi FROM w_res)) WITH ORDINALITY AS t(val, i) 
	ORDER BY t.i;

-----------------------------------------
-- bigger matrix
-----------------------------------------
select setseed(1);
with w_r as (
        select row_number() over() as i,
        random() as r
        from generate_series(1,100)
)
,w_m  as (
        select
                a.i as r,
                b.i as c,
                a.r*b.r as val
        from w_r as a, w_r as b
)
, w_ra as (
        select r, array_agg(val order by c) as ra
        from w_m
        group by r
)
, w as (
	select array_agg(ra order by r) as mat 
	from w_ra
)
, w_res AS (
	SELECT nfiesta.fn_inverse(mat) as mi FROM w
)
SELECT round(sum(t.val)::numeric, 10) as val, count(t.i) 
	FROM unnest((SELECT mi FROM w_res)) WITH ORDINALITY AS t(val, i) 
	;
