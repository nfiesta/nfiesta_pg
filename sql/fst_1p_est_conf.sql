--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
WITH w_period AS (
	INSERT INTO nfiesta.c_estimation_period(estimate_date_begin, estimate_date_end, label, description)
	SELECT 
		'2011-01-01'::date as estimate_date_begin, '2015-12-31'::date as estimate_date_end,
		'NFI2', 'NFI2 cycle'
	RETURNING id
), w_data (estimation_cell, estimation_period, note, target_variable) as (
	select
		c_estimation_cell.id as estimation_cell,
		w_period.id AS estimation_period,
		''::varchar as note,
		c_target_variable.id as target_variable
	from nfiesta.c_estimation_cell, nfiesta.c_target_variable, w_period
	order by c_estimation_cell.id, c_target_variable.id
)
select nfiesta.fn_est_configuration(estimation_cell, estimation_period, note, target_variable)
from w_data;

with w_1pt as (
	select * from nfiesta.v_conf_overview where estimate_type_str = '1p_total' order by estimate_conf
)
, w_1pr as (
	select
		nom.total_estimate_conf as nom__tec_id,
		denom.total_estimate_conf as denom__tec_id
	from
	w_1pt as nom
	inner join w_1pt as denom on (nom.estimation_cell = denom.estimation_cell and nom.target_variable = 1 and denom.target_variable = 2)
)
insert into nfiesta.t_estimate_conf (estimate_type, total_estimate_conf, denominator) 
select 2, nom__tec_id, denom__tec_id
from w_1pr
order by nom__tec_id, denom__tec_id;

