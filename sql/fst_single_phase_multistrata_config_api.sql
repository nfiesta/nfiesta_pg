-- Copyright 2017, 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.

-------------------------------------------------------------------------------------------------------------------------------------------------------
-- tests of API nfiesta.fn_api_get_data_options4single_phase_config
-------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------- testing invalid inputs ------------------------------------------------------------------------------
--test NULL for _estimation_cells argument
SELECT * FROM nfiesta.fn_api_get_data_options4single_phase_config (NULL, ARRAY[1], '1-Jan-2016', '31-Dec-2017');

-- test for NULL within _estimation_cells
SELECT * FROM nfiesta.fn_api_get_data_options4single_phase_config (ARRAY[144, NULL], ARRAY[1], '1-Jan-2016', '31-Dec-2017');

-- test NULL for _variables argument
SELECT * FROM nfiesta.fn_api_get_data_options4single_phase_config (ARRAY[144], NULL, '1-Jan-2016', '31-Dec-2017');

-- test for NULL within _variables
SELECT * FROM nfiesta.fn_api_get_data_options4single_phase_config (ARRAY[144], ARRAY[1, NULL], '1-Jan-2016', '31-Dec-2017');

-- test NULL for _estimate_date_begin
SELECT * FROM nfiesta.fn_api_get_data_options4single_phase_config (ARRAY[144], ARRAY[1], NULL, '31-Dec-2017');

-- test NULL for _estimate_date_end
SELECT * FROM nfiesta.fn_api_get_data_options4single_phase_config (ARRAY[144], ARRAY[1], '1-Jan-2016', NULL);

-- test of _estimate_date_begin after _estimate_date_end
SELECT * FROM nfiesta.fn_api_get_data_options4single_phase_config (ARRAY[144], ARRAY[1], '31-Dec-2017', '1-Jan-2016');

-- test of _estimate_date_begin equal _estimate_date_end
SELECT * FROM nfiesta.fn_api_get_data_options4single_phase_config (ARRAY[144], ARRAY[1], '31-Dec-2017', '31-Dec-2017');

-- test for valid range of _cell_coverage_tolerance, exception because tolerance higher than 1 
SELECT * FROM nfiesta.fn_api_get_data_options4single_phase_config (ARRAY[144], ARRAY[1], '1-Jan-2016', '31-Dec-2017', 1E3);

-- test for valid range of _cell_coverage_tolerance, exception because tolerance equals 1 
SELECT * FROM nfiesta.fn_api_get_data_options4single_phase_config (ARRAY[144], ARRAY[1], '1-Jan-2016', '31-Dec-2017', 1);

-- test for valid range of _cell_coverage_tolerance, exception becase tolerance equals 0 
SELECT * FROM nfiesta.fn_api_get_data_options4single_phase_config (ARRAY[144], ARRAY[1], '1-Jan-2016', '31-Dec-2017', 0);

-- test for valid range of _cell_coverage_tolerance, exception because tolerance equals -1 
SELECT * FROM nfiesta.fn_api_get_data_options4single_phase_config (ARRAY[144], ARRAY[1], '1-Jan-2016', '31-Dec-2017', -1);

-- test of no data situation for all estimation cells
SELECT * FROM nfiesta.fn_api_get_data_options4single_phase_config(ARRAY[17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43], ARRAY[1,2,3, -1], '1-Jan-2011', '31-Dec-2014');

------------------------------------------------ testing valid inputs ---------------------------------------------------------------------------------
-- test for NUTS0 CZ in pathfinder_ds_1_5 DB - 52 records returned, warning saying cell ins not fully covered by strata (Slovakia missing)
-- SELECT * FROM nfiesta.fn_api_get_data_options4single_phase_config(ARRAY[144], ARRAY[1], '1-Jan-2016', '31-Dec-2017');

-- test for NUTS2 CZ in pathfinder_ds_1_5 DB - 224 records returned, warnings saying cell ins not fully covered by strata (Slovakia missing)
-- SELECT * FROM nfiesta.fn_api_get_data_options4single_phase_config(ARRAY[146, 147, 148, 149, 150, 151, 152, 153], ARRAY[1], '1-Jan-2016', '31-Dec-2017');

-- test data of nfiesta, test for INSPIRE 50 km cells and forest variables (altogether, coniferous, broadleaved)
SELECT * FROM nfiesta.fn_api_get_data_options4single_phase_config(ARRAY[17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43], ARRAY[1,2,3], '1-Jan-2011', '31-Dec-2014');

-- same as above + the lower _cell_area_tolerance compared to its DEFAULT (-3) eventually leading to a longer list of cells included in the warning.
SELECT * FROM nfiesta.fn_api_get_data_options4single_phase_config(ARRAY[17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43], ARRAY[1,2,3], '1-Jan-2011', '31-Dec-2014', 1E-8);

-------------------------------------------------------------------------------------------------------------------------------------------------------
-- tests for function nfiesta.fn_api_save_single_phase_total_config
-------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------- testing invalid inputs ------------------------------------------------------------------------------
-- test NULL for _estimation_period argument
SELECT * FROM nfiesta.fn_api_save_single_phase_total_config(NULL,1,3,14, ARRAY[14]);

-- test NULL for _estimation_cell argument
SELECT * FROM nfiesta.fn_api_save_single_phase_total_config(1,NULL,3,14, ARRAY[14]);

-- test NULL for _variable argument
SELECT * FROM nfiesta.fn_api_save_single_phase_total_config(1,1,NULL,14, ARRAY[14]);

-- test NULL for _panel_refyearset_group argument
SELECT * FROM nfiesta.fn_api_save_single_phase_total_config(1,1,3,NULL, ARRAY[14]);

-- test NULL for _panel_refyearset_groups_by_strata argument
SELECT * FROM nfiesta.fn_api_save_single_phase_total_config(1, 1, 3, 14, NULL);
SELECT * FROM nfiesta.fn_api_save_single_phase_total_config(1, 1, 3, 14, ARRAY[NULL::int]);

------------------------------------------------ testing valid inputs ---------------------------------------------------------------------------------
-- first delete the existing configurations and results
DELETE FROM nfiesta.t_result WHERE id IN (3, 511);
DELETE FROM nfiesta.t_estimate_conf WHERE id IN (3, 511);
DELETE FROM nfiesta.t_total_estimate_conf WHERE id = 3;

-- save a configuration of single phase total
SELECT * FROM nfiesta.fn_api_save_single_phase_total_config(1,1,3,14, ARRAY[14]);

-- trying to save the same single-phase config the second time suceeds but raises a warning (the insert to t_total_estimate_conf is actually skipped)
SELECT * FROM nfiesta.fn_api_save_single_phase_total_config(1,1,3,14, ARRAY[14]);

-------------------------------------------------------------------------------------------------------------------------------------------------------
-- tests of API nfiesta.fn_api_get_panel_refyearset_group4stratum 
-------------------------------------------------------------------------------------------------------------------------------------------------------
-- testing invalid inputs
-- passing NULL for _stratum
SELECT * FROM nfiesta.fn_api_get_panel_refyearset_groups4stratum(NULL);

-- testing valid inputs
-- get groups for stratum 2 (id), three groups 14, 19, 21
SELECT * FROM nfiesta.fn_api_get_panel_refyearset_groups4stratum(2);
