-- script for the export of csvs from the database (some kind of reverse ingeneering)
-- this will export the data at the last state (after the second ETL - fst_data_generations)

-- temporary views are alive for the current session (not transaction)
\set srcdir `echo $SRC_DIR`

/*tables:
available_datasets.csv
estimation_cell.csv
estimation_cell_hierarchy.csv
plot_auxiliary_data_before.csv
plot_auxiliary_data.csv
plot_cell_associations.csv
plot_target_data_before.csv
plot_target_data.csv
variable_hierarchy.csv*/

CREATE OR REPLACE TEMPORARY VIEW v_estimation_cell AS (
	select
		estimation_cell,
		st_asewkt(geom) as estimation_cell_geometry
	from nfiesta.f_a_cell
	order by gid
);

\set afile :srcdir '/sql/csv/estimation_cell.csv'
COPY (SELECT * FROM v_estimation_cell) TO STDOUT DELIMITER ';' CSV HEADER \g :afile

CREATE OR REPLACE TEMPORARY VIEW v_plot_cell_associations AS (
	select
		c_country.label as country,
		t_strata_set.strata_set,
		t_stratum.stratum,
		t_panel.panel,
		t_cluster.cluster,
		f_p_plot.plot,
		c_estimation_cell_collection.label as cell_collection,
		c_estimation_cell.label as cell,
		NULL::text as comment
	from sdesign.f_p_plot
	inner join sdesign.t_cluster ON t_cluster.id = f_p_plot.cluster
	inner join sdesign.cm_cluster2panel_mapping ON cm_cluster2panel_mapping.cluster = t_cluster.id
	inner join sdesign.t_panel ON t_panel.id = cm_cluster2panel_mapping.panel
	inner join sdesign.t_stratum ON t_stratum.id = t_panel.stratum
	inner join sdesign.t_strata_set ON t_strata_set.id = t_stratum.strata_set
	inner join sdesign.c_country ON c_country.id = t_strata_set.country
	inner join nfiesta.cm_plot2cell_mapping ON cm_plot2cell_mapping.plot = f_p_plot.gid
	inner join nfiesta.c_estimation_cell ON c_estimation_cell.id = cm_plot2cell_mapping.estimation_cell
	inner join nfiesta.c_estimation_cell_collection ON c_estimation_cell_collection.id = c_estimation_cell.estimation_cell_collection
	order by c_country.id, strata_set, stratum, panel, t_cluster.cluster, f_p_plot.plot, c_estimation_cell_collection.id, c_estimation_cell.id);

\set afile :srcdir '/sql/csv/plot_cell_associations.csv'
COPY (SELECT * FROM v_plot_cell_associations) TO STDOUT DELIMITER ';' CSV HEADER \g :afile

CREATE OR REPLACE TEMPORARY VIEW v_available_datasets AS (
	SELECT
		c_country.label AS country,
		t_strata_set.strata_set,
		t_stratum.stratum,
		t2.panel,
		t4.reference_year_set,
		t3.target_variable,
		t5.sub_population,
		t3.sub_population_category,
		t6.area_domain,
		t3.area_domain_category,
		t7.auxiliary_variable,
		t3.auxiliary_variable_category,
		NULL::text AS comment
	FROM
		nfiesta.t_available_datasets AS t1
	INNER JOIN
		sdesign.t_panel AS t2 ON t1.panel = t2.id
	INNER JOIN
		nfiesta.t_variable AS t3 ON t1.variable = t3.id
	LEFT JOIN
		sdesign.t_reference_year_set AS t4 ON t1.reference_year_set = t4.id
	LEFT JOIN
		nfiesta.c_sub_population_category AS t5 ON t3.sub_population_category = t5.id
	LEFT JOIN
		nfiesta.c_area_domain_category AS t6 ON t3.area_domain_category = t6.id
	LEFT JOIN
		nfiesta.c_auxiliary_variable_category AS t7 ON t3.auxiliary_variable_category = t7.id
	INNER JOIN sdesign.t_stratum ON t_stratum.id = t2.stratum
	INNER JOIN sdesign.t_strata_set ON t_strata_set.id = t_stratum.strata_set
	INNER JOIN sdesign.c_country ON c_country.id = t_strata_set.country
	ORDER BY
		c_country.id, t_strata_set.strata_set, t_stratum.stratum, t2.panel, t4.reference_year_set, t3.target_variable, t5.sub_population,
		t3.sub_population_category, t6.area_domain, t3.area_domain_category, t7.auxiliary_variable, t3.auxiliary_variable_category
);

\set afile :srcdir '/sql/csv/available_datasets.csv'
COPY (SELECT * FROM v_available_datasets) TO STDOUT DELIMITER ';' CSV HEADER \g :afile

CREATE OR REPLACE TEMPORARY VIEW v_plot_auxiliary_data AS (
	WITH w_indb AS (
		SELECT	plot,
			available_datasets,
			value,
			is_latest
		FROM	nfiesta.t_auxiliary_data
		WHERE 	is_latest
	)
	select
		c_country.label as country,
		t_strata_set.strata_set,
		t_stratum.stratum,
		t_panel.panel,
		t_cluster.cluster,
		f_p_plot.plot,
		NULL as tile,
		c_auxiliary_variable.label AS auxiliary_variable,
		c_auxiliary_variable_category.label AS auxiliary_variable_category,
		coalesce(t_auxiliary_data.value,0) as value,
		NULL::text as comment
	from sdesign.f_p_plot
	inner join sdesign.t_cluster ON t_cluster.id = f_p_plot.cluster
	inner join sdesign.cm_cluster2panel_mapping ON cm_cluster2panel_mapping.cluster = t_cluster.id
	inner join sdesign.t_panel ON t_panel.id = cm_cluster2panel_mapping.panel
	inner join sdesign.t_stratum ON t_stratum.id = t_panel.stratum
	inner join sdesign.t_strata_set ON t_strata_set.id = t_stratum.strata_set
	inner join sdesign.c_country ON c_country.id = t_strata_set.country
	INNER JOIN 	nfiesta.t_available_datasets ON t_available_datasets.panel = t_panel.id
	LEFT JOIN 	w_indb AS t_auxiliary_data ON t_auxiliary_data.plot = f_p_plot.gid AND t_auxiliary_data.available_datasets = t_available_datasets.id
	INNER JOIN	nfiesta.t_variable ON t_available_datasets.variable = t_variable.id
	INNER JOIN nfiesta.c_auxiliary_variable_category ON c_auxiliary_variable_category.id = t_variable.auxiliary_variable_category
	INNER JOIN  nfiesta.c_auxiliary_variable ON c_auxiliary_variable.id = c_auxiliary_variable_category.auxiliary_variable
	order by c_country.id, strata_set, stratum, panel, t_cluster.cluster, f_p_plot.plot, c_auxiliary_variable.id, c_auxiliary_variable_category.id);

\set afile :srcdir '/sql/csv/plot_auxiliary_data.csv'
COPY (SELECT * FROM v_plot_auxiliary_data) TO STDOUT DELIMITER ';' CSV HEADER \g :afile

CREATE OR REPLACE TEMPORARY VIEW v_plot_target_data AS (
	WITH w_indb AS (
		SELECT	plot,
			t2.variable,
			t2.reference_year_set,
			value,
			is_latest
		FROM		nfiesta.t_target_data AS t1
		inner join 	nfiesta.t_available_datasets as t2 on (t1.available_datasets = t2.id)
		WHERE 	is_latest
	)
	SELECT
		c_country.label			as country,
		t_inventory_campaign.inventory_campaign,
		t9.reference_year_set,
		t7.strata_set,
		t6.stratum,
		t5.panel,
		t3.cluster,
		t2.plot,
		c_target_variable.metadata->'en'->'indicator'->>'label'	as target_variable,
		c_sub_population.label		as sub_population,
		t11.label			AS sub_population_category,
		c_area_domain.label		as area_domain,
		t12.label			AS area_domain_category,
		coalesce(t1.value,0)		AS value,
		NULL::text AS comment
	FROM	sdesign.f_p_plot 				AS t2
	INNER JOIN sdesign.t_cluster 				AS t3 	ON	t2.cluster = t3.id
	INNER JOIN sdesign.cm_cluster2panel_mapping 		AS t4 	ON	t3.id = t4.cluster
	INNER JOIN sdesign.t_panel 				AS t5 	ON	t4.panel = t5.id
	INNER JOIN sdesign.t_stratum 				AS t6	ON	t5.stratum = t6.id
	INNER JOIN sdesign.t_strata_set 			AS t7	ON	t6.strata_set = t7.id
	inner join sdesign.c_country 					ON 	c_country.id = t7.country
	INNER JOIN nfiesta.t_available_datasets 		AS t8	ON	t5.id = t8.panel
	INNER JOIN sdesign.t_reference_year_set 		AS t9	ON	t8.reference_year_set = t9.id
	inner join sdesign.t_inventory_campaign				on	t9.inventory_campaign = t_inventory_campaign.id
	LEFT JOIN w_indb 					AS t1	ON	t1.plot = t2.gid AND t8.variable = t1.variable AND t9.id = t1.reference_year_set
	INNER JOIN nfiesta.t_variable 			AS t10	ON	t8.variable = t10.id
	LEFT JOIN nfiesta.c_target_variable			ON 	t10.target_variable = c_target_variable.id
	LEFT JOIN nfiesta.c_sub_population_category 	AS t11	ON 	t10.sub_population_category = t11.id
	LEFT JOIN nfiesta.c_sub_population				ON 	t11.sub_population = c_sub_population.id
	LEFT JOIN nfiesta.c_area_domain_category 		AS t12	ON	t10.area_domain_category = t12.id
	LEFT JOIN nfiesta.c_area_domain				ON 	t12.area_domain = c_area_domain.id
	ORDER BY country, strata_set, stratum, panel, cluster, plot, target_variable, reference_year_set, sub_population_category, area_domain_category
);

\set afile :srcdir '/sql/csv/plot_target_data.csv'
COPY (SELECT * FROM v_plot_target_data) TO STDOUT DELIMITER ';' CSV HEADER \g :afile

--------------------------------------------------------------------
-- for the sake of completness two hierarchy tables based on lookups:
--------------------------------------------------------------------

CREATE OR REPLACE TEMPORARY VIEW v_variable_hierarchy AS (
SELECT
	t2.target_variable,
	t2.area_domain_category AS area_domain_category_sup,
	t2.sub_population_category AS sub_population_category_sup,
	t3.area_domain_category,
	t3.sub_population_category
FROM
	nfiesta.t_variable_hierarchy AS t1
INNER JOIN
	nfiesta.t_variable AS t2 ON t1.variable_superior = t2.id
INNER JOIN
	nfiesta.t_variable AS t3 ON t1.variable = t3.id
ORDER BY
	target_variable, area_domain_category_sup, sub_population_category_sup, area_domain_category, sub_population_category
);

\set afile :srcdir '/sql/csv/variable_hierarchy.csv'
COPY (SELECT * FROM v_variable_hierarchy) TO STDOUT DELIMITER ';' CSV HEADER \g :afile

CREATE OR REPLACE TEMPORARY VIEW v_estimation_cell_hierarchy AS (
SELECT
	cell,
	cell_superior
FROM
	nfiesta.t_estimation_cell_hierarchy
ORDER BY
	cell, cell_superior
);

\set afile :srcdir '/sql/csv/estimation_cell_hierarchy.csv'
COPY (SELECT * FROM v_estimation_cell_hierarchy) TO STDOUT DELIMITER ';' CSV HEADER \g :afile
