/*
	test for attribute categories selection
*/

--------------
-- fn_get_area_domains4target_variable
--------------
-- test on input parameters
SELECT * FROM nfiesta.fn_get_area_domains4target_variable(NULL);
SELECT * FROM nfiesta.fn_get_area_domains4target_variable(6);
SELECT * FROM nfiesta.fn_get_area_domains4target_variable(1,3);
-- ok
SELECT * FROM nfiesta.fn_get_area_domains4target_variable(1);
SELECT * FROM nfiesta.fn_get_area_domains4target_variable(1,0);
SELECT * FROM nfiesta.fn_get_area_domains4target_variable(1,1);
SELECT * FROM nfiesta.fn_get_area_domains4target_variable(2);
SELECT * FROM nfiesta.fn_get_area_domains4target_variable(2,0);
SELECT * FROM nfiesta.fn_get_area_domains4target_variable(2,1);

--------------
-- fn_get_sub_populations4target_variable
--------------
-- test on input parameters
SELECT * FROM nfiesta.fn_get_sub_populations4target_variable(NULL);
SELECT * FROM nfiesta.fn_get_sub_populations4target_variable(6);
SELECT * FROM nfiesta.fn_get_sub_populations4target_variable(1,3);
-- ok
SELECT * FROM nfiesta.fn_get_sub_populations4target_variable(4);
SELECT * FROM nfiesta.fn_get_sub_populations4target_variable(4,0);
SELECT * FROM nfiesta.fn_get_sub_populations4target_variable(4,1);
SELECT * FROM nfiesta.fn_get_sub_populations4target_variable(2);
SELECT * FROM nfiesta.fn_get_sub_populations4target_variable(2,0);
SELECT * FROM nfiesta.fn_get_sub_populations4target_variable(1,1);

--------------
-- fn_get_area_sub_population_categories
--------------
SELECT * FROM nfiesta.fn_get_area_sub_population_categories(4,200,1);
-- ok
SELECT * FROM nfiesta.fn_get_area_sub_population_categories(1,100,1);
SELECT * FROM nfiesta.fn_get_area_sub_population_categories(1,100,0);
SELECT * FROM nfiesta.fn_get_area_sub_population_categories(1,100,NULL);

--------------
-- fn_get_num_denom_variables
--------------
-- test on input parameters
SELECT * FROM nfiesta.fn_get_num_denom_variables(array[1,10],array[3]);
SELECT * FROM nfiesta.fn_get_num_denom_variables(array[1,2],array[10]);
SELECT * FROM nfiesta.fn_get_num_denom_variables(array[1,2],array[1,2,3]);

-- ok
SELECT * FROM nfiesta.fn_get_num_denom_variables(array[1,2,3],array[3]);
SELECT * FROM nfiesta.fn_get_num_denom_variables(array[1,2,3]);

--------------
-- fn_get_attribute_categories4target_variable
--------------
SELECT * FROM nfiesta.fn_get_attribute_categories4target_variable(NULL,1,NULL,1,NULL,NULL);
SELECT * FROM nfiesta.fn_get_attribute_categories4target_variable(1,6,NULL,1,NULL,NULL);
-- ok
-- total
SELECT * FROM nfiesta.fn_get_attribute_categories4target_variable(1,1,NULL,NULL,NULL,NULL);
SELECT * FROM nfiesta.fn_get_attribute_categories4target_variable(1,0,0,NULL,NULL,NULL);
SELECT * FROM nfiesta.fn_get_attribute_categories4target_variable(1,1,0,NULL,NULL,NULL);
-- ratio
SELECT * FROM nfiesta.fn_get_attribute_categories4target_variable(1,1,NULL,1,NULL,NULL);
SELECT * FROM nfiesta.fn_get_attribute_categories4target_variable(1,1,0,1,0,0);

