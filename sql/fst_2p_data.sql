--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
with w_data as (
	select estimate_conf, (nfiesta.fn_2p_data(total_estimate_conf)).*
		from (select * from nfiesta.v_conf_overview where estimate_type_str = '2p_total'
			and sigma = False
		) as confs
)
, w_agg as (
	select	estimate_conf, stratum, attribute,
		nb_sampling_units, sweight_strata_sum, round(lambda_d_plus::numeric, 10) as buffered_area_ha,
		sum(cluster) as cids_sum,
		sum(ldsity_d) as sum__ldsity_d, sum(ldsity_d_plus) as sum__ldsity_d_plus,
		round(sum(ldsity_res_d)::numeric, 5) as sum__ldsity_res_d, round(sum(ldsity_res_d_plus)::numeric, 5) as sum__ldsity_res_d_plus,
		sum(sweight) as sweights_sum,
		round(sum(sweight_strata_sum / (lambda_d_plus * sweight))::numeric, 10) as pix_sum
	from w_data
	group by estimate_conf, stratum, attribute, nb_sampling_units, sweight_strata_sum, buffered_area_ha
	order by estimate_conf, stratum, attribute
)
select
	estimation_cell_label, t_stratum.stratum,
	(((c_target_variable.metadata->'en')->'indicator')->>'label'::varchar) as target_variable_label,
	c_area_domain_category.label as area_domain_category_label, c_sub_population_category.label as sub_population_category_label,
	c_auxiliary_variable_category.label as auxiliary_variable_category_label,
	param_area_code, model_description, force_synthetic,
	nb_sampling_units, sweight_strata_sum, buffered_area_ha, cids_sum,
	sum__ldsity_d, sum__ldsity_d_plus, sum__ldsity_res_d, sum__ldsity_res_d_plus,
	sweights_sum, pix_sum
from w_agg
inner join nfiesta.v_conf_overview on (w_agg.estimate_conf = v_conf_overview.estimate_conf)
inner join sdesign.t_stratum on (w_agg.stratum = t_stratum.id)
inner join nfiesta.t_variable on (w_agg.attribute = t_variable.id)
left join nfiesta.c_target_variable on (t_variable.target_variable = c_target_variable.id)
left join nfiesta.c_area_domain_category on (t_variable.area_domain_category = c_area_domain_category.id)
left join nfiesta.c_sub_population_category on (t_variable.sub_population_category = c_sub_population_category.id)
left join nfiesta.c_auxiliary_variable_category on (t_variable.auxiliary_variable_category = c_auxiliary_variable_category.id)
order by estimation_cell_label, t_stratum.stratum, target_variable_label, area_domain_category_label, sub_population_category_label, c_auxiliary_variable_category.label, param_area_code, model_description, force_synthetic
;
