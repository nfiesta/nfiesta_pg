--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- <function name="fn_add_res_total_geo" schema="extschema" src="functions/extschema/additivity/fn_add_res_total_geo.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_add_res_total_geo(integer)

-- DROP FUNCTION @extschema@.fn_add_res_total_geo(integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_add_res_total_geo(
	IN est_confs integer[],
	IN min_diff double precision default 0.0, IN include_null_diff boolean default true
)
  RETURNS TABLE(
	variable		integer,
	aux_conf		integer,
	force_synthetic		boolean,
	estimate_conf		integer,
	estimation_cell		integer,
	point_est		double precision,
	point_est_sum		double precision,
	estimation_cells_def	integer[],
	estimation_cells_found	integer[],
	estimate_confs_found	integer[],
	diff			double precision
) AS
$BODY$
DECLARE
	_complete_query text;
	_array_text text;
BEGIN
	--------------------------------QUERY--------------------------------
	_array_text := quote_literal(est_confs::text);
	--raise notice 'est_confs: %',  _array_text;
	_complete_query := '
with w_res_cell_var as not materialized (
	select
		t_result.estimate_conf,
		t_result.point,
		t_total_estimate_conf.estimation_cell,
		t_total_estimate_conf.target_variable as t_variable__id,
		t_total_estimate_conf.estimate_date_begin, t_total_estimate_conf.estimate_date_end,
		json_agg(row_to_json((SELECT d FROM (SELECT est_conf_stratum.stratum, est_conf_stratum.panels) d))
			order by est_conf_stratum.stratum)::jsonb as strata_panels,
		t_total_estimate_conf.aux_conf, force_synthetic
	from @extschema@.t_result
	inner join @extschema@.t_estimate_conf ON t_estimate_conf.id = t_result.estimate_conf
	inner join @extschema@.t_total_estimate_conf ON t_total_estimate_conf.id = t_estimate_conf.total_estimate_conf
	inner join (
		select
			total_estimate_conf, stratum, array_agg(t_panel.id order by t_panel.id) as panels
		from @extschema@.t_panel2total_2ndph_estimate_conf
		inner join @extschema@.t_panel on (t_panel2total_2ndph_estimate_conf.panel = t_panel.id)
		group by total_estimate_conf, stratum
		) as est_conf_stratum
		ON est_conf_stratum.total_estimate_conf = t_total_estimate_conf.id
	where (t_estimate_conf.estimate_type = 1)
	and (t_result.is_latest)
	and (t_result.estimate_conf IN (SELECT UNNEST(' || _array_text || '::integer[])))
	group by
		t_result.estimate_conf,
		t_result.point,
		t_total_estimate_conf.estimation_cell,
		t_total_estimate_conf.target_variable,
		t_total_estimate_conf.estimate_date_begin, t_total_estimate_conf.estimate_date_end,
		t_total_estimate_conf.aux_conf, force_synthetic
)
, w_node_sum as (
	select
		estimation_cell,
		w_res_cell_var.t_variable__id, aux_conf, force_synthetic,
		estimate_date_begin, estimate_date_end, strata_panels,
		estimate_conf as node_estimate_confs,
		sum(point) as node_sum,
		hierarchy.node,
		hierarchy.edges as edges_def
	from w_res_cell_var
	inner join @extschema@.v_estimation_cell_hierarchy	as hierarchy on (hierarchy.node = w_res_cell_var.estimation_cell)
	group by estimation_cell, w_res_cell_var.t_variable__id, aux_conf, force_synthetic, estimate_date_begin, estimate_date_end, strata_panels, estimate_conf, node, edges_def
	order by estimation_cell, w_res_cell_var.t_variable__id, aux_conf, force_synthetic, estimate_date_begin, estimate_date_end, strata_panels, estimate_conf, node, edges_def
)
, w_edge_sum as (
	select
		w_node_sum.t_variable__id as variable,
		w_node_sum.aux_conf,
		w_node_sum.force_synthetic,
		w_node_sum.node_estimate_confs,
		w_node_sum.estimate_date_begin, w_node_sum.estimate_date_end,
		w_node_sum.node,
		w_node_sum.node_sum,
		w_node_sum.edges_def,
		array_agg(w_res_cell_var.estimation_cell order by w_res_cell_var.estimation_cell) as edges_found,
		array_agg(w_res_cell_var.estimate_conf order by w_res_cell_var.estimate_conf) as edges_estimate_confs,
		sum(w_res_cell_var.point) as edges_sum
	from w_node_sum
	left join w_res_cell_var on (
		w_res_cell_var.t_variable__id = w_node_sum.t_variable__id
		and w_res_cell_var.estimate_date_begin = w_node_sum.estimate_date_begin
		and w_res_cell_var.estimate_date_end = w_node_sum.estimate_date_end
		and
		(
		SELECT array_to_json(array_agg(elem order by elem))::jsonb
		FROM   jsonb_array_elements(w_node_sum.strata_panels) elem
		WHERE  (elem->>''stratum'')::int in (select jsonb_path_query(w_res_cell_var.strata_panels, ''$.stratum'')::int)
		) = w_res_cell_var.strata_panels
		and case when
			w_res_cell_var.aux_conf is null and w_node_sum.aux_conf is null then true
			else w_res_cell_var.aux_conf = w_node_sum.aux_conf end
		and case when
			w_res_cell_var.force_synthetic is null and w_node_sum.force_synthetic is null then true
			else w_res_cell_var.force_synthetic = w_node_sum.force_synthetic end
		and w_res_cell_var.estimation_cell = any(w_node_sum.edges_def))
	group by w_node_sum.t_variable__id,
		w_node_sum.aux_conf, w_node_sum.force_synthetic, w_node_sum.node_estimate_confs,
		w_node_sum.estimate_date_begin, w_node_sum.estimate_date_end,
		w_node_sum.node, w_node_sum.node_sum, w_node_sum.edges_def
)
, w_diff as (
	select
		variable,
		aux_conf,
		force_synthetic,
		node_estimate_confs	as estimate_conf,
		node			as estimation_cell,
		node_sum		as point_est,
		edges_sum		as point_est_sum,
		edges_def		as estimation_cells_def,
		edges_found		as estimation_cells_found,
		edges_estimate_confs	as estimate_confs_found,
		case
			when node_sum != 0.0 and edges_sum = 0.0 then 100.0
			when node_sum = 0.0 and edges_sum = 0.0 then 0
			else abs(1 - (node_sum / edges_sum )) * 100.0
		end as diff
	from w_edge_sum
)
select * from w_diff
where (diff > ' || min_diff || ') or (diff is null = ' || include_null_diff || ')
order by diff desc
;
';

	--RAISE NOTICE '%', _complete_query;
	RETURN QUERY EXECUTE _complete_query;
END;
$BODY$
  LANGUAGE plpgsql STABLE
  COST 100
  ROWS 100000;

comment on function @extschema@.fn_add_res_total_geo(integer[], double precision, boolean) is
	'Fnction showing total estimates geographic additivity (aggregated class estimate should be equal to sum of sub-classes estimates). Hierarchy between aggregated classes and its sub-classes is defined in v_estimation_cell_hierarchy.
Function input argument is:
 * Array of estimate configuration ids. FKEY to t_estimate_conf.id. All est. conf. ids to be checked (aggregated class together with sub-classes) need to be passed.
 * Minimal amount of difference [%].
 * Whether include NULL difference -- indicating defined sub-classes missing in data.
Resulting table has following columns:
 * Estimate attribute -- variable. FKEY to t_variable.id.
 * Estimate auxiliary configuration. FKEY to t_aux_conf.id.
 * Parameter showing whether estimate is forced to be synthetic.
 * Estimate configuration id. FKEY to t_estimate_conf.id.
 * Estimate estimation cell. FKEY to c_estimation_cell.id.
 * Aggregated class point estimate.
 * Sum of sub-classes point estimates (belonging to aggregated class).
 * Estimation cells defined in hierarchy (v_estimation_cell_hierarchy). Array of FKEYs to c_estimation_cell.id.
 * Estimation cells found in data (t_result). Array of FKEYs to c_estimation_cell.id.
 * Estimate configurations found in data (t_result). Array of FKEYs to t_estimate_donf.id.
 * Relative difference between aggregated class estimate and sum of sub-classes estimates.';

GRANT EXECUTE ON FUNCTION @extschema@.fn_add_res_total_geo TO PUBLIC;

-- </function>

-- <view name="v_add_res_total_geo" schema="extschema" src="views/extschema/v_add_res_total_geo.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

create or replace view @extschema@.v_add_res_total_geo as
with w_res_cell_var as not materialized (
	select
		t_result.estimate_conf,
		t_result.point,
		t_total_estimate_conf.estimation_cell,
		t_total_estimate_conf.target_variable as t_variable__id,
		t_total_estimate_conf.estimate_date_begin, t_total_estimate_conf.estimate_date_end,
		json_agg(row_to_json((SELECT d FROM (SELECT est_conf_stratum.stratum, est_conf_stratum.panels) d))
			order by est_conf_stratum.stratum)::jsonb as strata_panels,
		t_total_estimate_conf.aux_conf, force_synthetic
	from @extschema@.t_result
	inner join @extschema@.t_estimate_conf ON t_estimate_conf.id = t_result.estimate_conf
	inner join @extschema@.t_total_estimate_conf ON t_total_estimate_conf.id = t_estimate_conf.total_estimate_conf
	inner join (
		select
			total_estimate_conf, stratum, array_agg(t_panel.id order by t_panel.id) as panels
		from @extschema@.t_panel2total_2ndph_estimate_conf
		inner join @extschema@.t_panel on (t_panel2total_2ndph_estimate_conf.panel = t_panel.id)
		group by total_estimate_conf, stratum
		) as est_conf_stratum
		ON est_conf_stratum.total_estimate_conf = t_total_estimate_conf.id
	where t_estimate_conf.estimate_type = 1
	and t_result.is_latest
	group by
		t_result.estimate_conf,
		t_result.point,
		t_total_estimate_conf.estimation_cell,
		t_total_estimate_conf.target_variable,
		t_total_estimate_conf.estimate_date_begin, t_total_estimate_conf.estimate_date_end,
		t_total_estimate_conf.aux_conf, force_synthetic
)
, w_node_sum as (
	select
		estimation_cell,
		w_res_cell_var.t_variable__id, aux_conf, force_synthetic,
		estimate_date_begin, estimate_date_end, strata_panels,
		estimate_conf as node_estimate_confs,
		sum(point) as node_sum,
		hierarchy.node,
		hierarchy.edges as edges_def
	from w_res_cell_var
	inner join @extschema@.v_estimation_cell_hierarchy	as hierarchy on (hierarchy.node = w_res_cell_var.estimation_cell)
	group by estimation_cell, w_res_cell_var.t_variable__id, aux_conf, force_synthetic, estimate_date_begin, estimate_date_end, strata_panels, estimate_conf, node, edges_def
	order by estimation_cell, w_res_cell_var.t_variable__id, aux_conf, force_synthetic, estimate_date_begin, estimate_date_end, strata_panels, estimate_conf, node, edges_def
)
, w_edge_sum as (
	select
		w_node_sum.t_variable__id as variable,
		w_node_sum.aux_conf,
		w_node_sum.force_synthetic,
		w_node_sum.node_estimate_confs,
		w_node_sum.estimate_date_begin, w_node_sum.estimate_date_end,
		w_node_sum.node,
		w_node_sum.node_sum,
		w_node_sum.edges_def,
		array_agg(w_res_cell_var.estimation_cell order by w_res_cell_var.estimation_cell) as edges_found,
		array_agg(w_res_cell_var.estimate_conf order by w_res_cell_var.estimate_conf) as edges_estimate_confs,
		sum(w_res_cell_var.point) as edges_sum
	from w_node_sum
	left join w_res_cell_var on (
		w_res_cell_var.t_variable__id = w_node_sum.t_variable__id
		and w_res_cell_var.estimate_date_begin = w_node_sum.estimate_date_begin
		and w_res_cell_var.estimate_date_end = w_node_sum.estimate_date_end
		and
		(
		SELECT array_to_json(array_agg(elem order by elem))::jsonb
		FROM   jsonb_array_elements(w_node_sum.strata_panels) elem
		WHERE  (elem->>'stratum')::int in (select jsonb_path_query(w_res_cell_var.strata_panels, '$.stratum')::int)
		) = w_res_cell_var.strata_panels
		and case when
			w_res_cell_var.aux_conf is null and w_node_sum.aux_conf is null then true
			else w_res_cell_var.aux_conf = w_node_sum.aux_conf end
		and case when
			w_res_cell_var.force_synthetic is null and w_node_sum.force_synthetic is null then true
			else w_res_cell_var.force_synthetic = w_node_sum.force_synthetic end
		and w_res_cell_var.estimation_cell = any(w_node_sum.edges_def))
	group by w_node_sum.t_variable__id,
		w_node_sum.aux_conf, w_node_sum.force_synthetic, w_node_sum.node_estimate_confs,
		w_node_sum.estimate_date_begin, w_node_sum.estimate_date_end,
		w_node_sum.node, w_node_sum.node_sum, w_node_sum.edges_def
)
, w_diff as (
	select
		variable,
		aux_conf,
		force_synthetic,
		node_estimate_confs	as estimate_conf,
		node			as estimation_cell,
		node_sum		as point_est,
		edges_sum		as point_est_sum,
		edges_def		as estimation_cells_def,
		edges_found		as estimation_cells_found,
		edges_estimate_confs	as estimate_confs_found,
		case
			when node_sum != 0.0 and edges_sum = 0.0 then 100.0
			when node_sum = 0.0 and edges_sum = 0.0 then 0
			else abs(1 - (node_sum / edges_sum )) * 100.0
		end as diff
	from w_edge_sum
)
select * from w_diff order by diff desc
;

comment on view @extschema@.v_add_res_total_geo is
	'View showing total estimates geographic additivity (aggregated class estimate should be equal to sum of sub-classes estimates). Hierarchy between aggregated classes and its sub-classes is defined in v_estimation_cell_hierarchy.';

comment on column @extschema@.v_add_res_total_geo.variable			is 'Estimate attribute -- variable. FKEY to t_variable.id.';
comment on column @extschema@.v_add_res_total_geo.aux_conf 			is 'Estimate auxiliary configuration. FKEY to t_aux_conf.id.';
comment on column @extschema@.v_add_res_total_geo.force_synthetic 		is 'Parameter showing whether estimate is forced to be synthetic.';
comment on column @extschema@.v_add_res_total_geo.estimate_conf			is 'Estimate configuration id. FKEY to t_estimate_conf.id.';
comment on column @extschema@.v_add_res_total_geo.estimation_cell 		is 'Estimate estimation cell. FKEY to c_estimation_cell.id.';
comment on column @extschema@.v_add_res_total_geo.point_est			is 'Aggregated class point estimate.';
comment on column @extschema@.v_add_res_total_geo.point_est_sum			is 'Sum of sub-classes point estimates (belonging to aggregated class).';
comment on column @extschema@.v_add_res_total_geo.estimation_cells_def		is 'Estimation cells defined in hierarchy (v_estimation_cell_hierarchy). Array of FKEYs to c_estimation_cell.id.';
comment on column @extschema@.v_add_res_total_geo.estimation_cells_found	is 'Estimation cells found in data (t_result). Array of FKEYs to c_estimation_cell.id.';
comment on column @extschema@.v_add_res_total_geo.estimate_confs_found		is 'Estimate configurations found in data (t_result). Array of FKEYs to t_estimate_donf.id.';
comment on column @extschema@.v_add_res_total_geo.diff				is 'Relative difference between aggregated class estimate and sum of sub-classes estimates.';

GRANT SELECT ON TABLE @extschema@.v_add_res_total_geo TO PUBLIC;

-- </view>
