--
-- Copyright 2017, 2023 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--



-- <function name="fn_etl_check_area_domains4update" schema="extschema" src="functions/extschema/etl/fn_etl_check_area_domains4update.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_check_area_domains4update(json)

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_check_area_domains4update(json) CASCADE;

create or replace function @extschema@.fn_etl_check_area_domains4update
(
	_metadatas json
)
returns table
(
	area_domain				integer,
	label_source			varchar,
	description_source		text,
	label_en_source			varchar,
	description_en_source	text,
	label_target			varchar,
	description_target		text,
	label_en_target			varchar,
	description_en_target	text,
	check_label				boolean,
	check_description		boolean,
	check_label_en			boolean,
	check_description_en	boolean
)
as
$$
declare
begin
		if _metadatas is null
		then
			raise exception 'Error 01: fn_etl_check_area_domains4update: Input argument _metadatas must not be NULL!';
		end if;
		-----------------------------------------
		return query
		with
		w1 as 	(
				select _metadatas as metadatas
				)
		,w2 as	(
				select json_array_elements(w1.metadatas) as metadatas from w1
				)
		,w3 as	(
				select
						(w2.metadatas->>'area_domain')::integer as area_domain_target,
						(w2.metadatas->>'label')::varchar as label_source,
						(w2.metadatas->>'description')::text as description_source,
						(w2.metadatas->>'label_en')::varchar as label_en_source,
						(w2.metadatas->>'description_en')::text as description_en_source
				from
						w2
				)
		,w4 as	(
				select
						w3.*,
						cad.label as label_target,
						cad.description as description_target,
						cad.label_en as label_en_target,
						cad.description_en as description_en_target
				from
						w3
						inner join @extschema@.c_area_domain as cad
						on w3.area_domain_target = cad.id
				)
		,w5 as	(
				select
						w4.*,
						string_to_array(w4.label_source,';') as array_label_source,
						string_to_array(w4.description_source,';') as array_description_source,
						string_to_array(w4.label_en_source,';') as array_label_en_source,
						string_to_array(w4.description_en_source,';') as array_description_en_source,
						string_to_array(w4.label_target,';') as array_label_target,
						string_to_array(w4.description_target,';') as array_description_target,
						string_to_array(w4.label_en_target,';') as array_label_en_target,
						string_to_array(w4.description_en_target,';') as array_description_en_target
				from
						w4
				)
		,w6 as	(
				select
						w5.*,
						@extschema@.fn_etl_array_compare(w5.array_label_source,w5.array_label_target) as check_label,
						@extschema@.fn_etl_array_compare(w5.array_description_source,w5.array_description_target) as check_description,
						@extschema@.fn_etl_array_compare(w5.array_label_en_source,w5.array_label_en_target) as check_label_en,
						@extschema@.fn_etl_array_compare(w5.array_description_en_source,w5.array_description_en_target) as check_description_en
				from
						w5
				)
		select
				w6.area_domain_target as area_domain,
				w6.label_source,
				w6.description_source,
				w6.label_en_source,
				w6.description_en_source,
				w6.label_target,
				w6.description_target,
				w6.label_en_target,
				w6.description_en_target,
				w6.check_label,
				w6.check_description,
				w6.check_label_en,
				w6.check_description_en
		from
				w6
		where
				w6.check_label = false or
				w6.check_description = false or
				w6.check_label_en = false or
				w6.check_description_en = false;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_check_area_domains4update(json) is
'The tunction returns records of area domains with informations whether their source labels or descriptions are different from target.';

grant execute on function @extschema@.fn_etl_check_area_domains4update(json) to public;
-- </function>



-- <function name="fn_etl_update_area_domain_label" schema="extschema" src="functions/extschema/etl/fn_etl_update_area_domain_label.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_update_area_domain_label(integer, character varying, varchar)

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_update_area_domain_label(integer, character varying, varchar) CASCADE;

create or replace function @extschema@.fn_etl_update_area_domain_label
(
	_area_domain		integer,
	_national_language	character varying(2),
	_label				varchar
)
returns text
as
$$
declare
		_res	text;
begin
		if _area_domain is null
		then
			raise exception 'Error 01: fn_etl_update_area_domain_label: Input argument _area_domain must not be NULL!';
		end if;
	
		if _national_language is null
		then
			raise exception 'Error 02: fn_etl_update_area_domain_label: Input argument _national_language must not be NULL!';
		end if;
	
		if _label is null
		then
			raise exception 'Error 03: fn_etl_update_area_domain_label: Input argument _label must not be NULL!';
		end if;
		-----------------------------------------
		if	(
			select count(cad.*) is distinct from 1
			from @extschema@.c_area_domain as cad
			where cad.id = _area_domain
			)
		then
			raise exception 'Error 04: fn_etl_update_area_domain_label: Input argument _area_domain (ID) = % is not present in c_area_domain table!',_area_domain;
		end if;
		-----------------------------------------
		if _national_language = 'en'::varchar
		then
			update @extschema@.c_area_domain set label_en = _label where id = _area_domain;
		else
			update @extschema@.c_area_domain set label = _label where id = _area_domain;
		end if;
		-----------------------------------------
		_res := concat('The label of area domain [c_area_domain.id = ',_area_domain,'] for language element = "',_national_language,'" was changed.');
		-----------------------------------------
		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_update_area_domain_label(integer, character varying, varchar) is
'The tunction updates an area domain label in c_area_domain table for given area domain.';

grant execute on function @extschema@.fn_etl_update_area_domain_label(integer, character varying, varchar) to public;
-- </function>



-- <function name="fn_etl_update_area_domain_description" schema="extschema" src="functions/extschema/etl/fn_etl_update_area_domain_description.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_update_area_domain_description(integer, character varying, text)

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_update_area_domain_description(integer, character varying, text) CASCADE;

create or replace function @extschema@.fn_etl_update_area_domain_description
(
	_area_domain		integer,
	_national_language	character varying(2),
	_description		text
)
returns text
as
$$
declare
		_res	text;
begin
		if _area_domain is null
		then
			raise exception 'Error 01: fn_etl_update_area_domain_description: Input argument _area_domain must not be NULL!';
		end if;
	
		if _national_language is null
		then
			raise exception 'Error 02: fn_etl_update_area_domain_description: Input argument _national_language must not be NULL!';
		end if;
	
		if _description is null
		then
			raise exception 'Error 03: fn_etl_update_area_domain_description: Input argument _description must not be NULL!';
		end if;
		-----------------------------------------
		if	(
			select count(cad.*) is distinct from 1
			from @extschema@.c_area_domain as cad
			where cad.id = _area_domain
			)
		then
			raise exception 'Error 04: fn_etl_update_area_domain_description: Input argument _area_domain (ID) = % is not present in c_area_domain table!',_area_domain;
		end if;
		-----------------------------------------
		if _national_language = 'en'::varchar
		then
			update @extschema@.c_area_domain set description_en = _description where id = _area_domain;
		else
			update @extschema@.c_area_domain set description = _description where id = _area_domain;
		end if;
		-----------------------------------------
		_res := concat('The description of area domain [c_area_domain.id = ',_area_domain,'] for language element = "',_national_language,'" was changed.');
		-----------------------------------------
		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_update_area_domain_description(integer, character varying, text) is
'The tunction updates an area domain description in c_area_domain table for given area domain.';

grant execute on function @extschema@.fn_etl_update_area_domain_description(integer, character varying, text) to public;
-- </function>



-- <function name="fn_etl_check_area_domain_categories4update" schema="extschema" src="functions/extschema/etl/fn_etl_check_area_domain_categories4update.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_check_area_domain_categories4update(json)

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_check_area_domain_categories4update(json) CASCADE;

create or replace function @extschema@.fn_etl_check_area_domain_categories4update
(
	_metadatas json
)
returns table
(
	area_domain_category	integer,
	label_source			varchar,
	description_source		text,
	label_en_source			varchar,
	description_en_source	text,
	label_target			varchar,
	description_target		text,
	label_en_target			varchar,
	description_en_target	text,
	check_label				boolean,
	check_description		boolean,
	check_label_en			boolean,
	check_description_en	boolean
)
as
$$
declare
begin
		if _metadatas is null
		then
			raise exception 'Error 01: fn_etl_check_area_domain_categories4update: Input argument _metadatas must not be NULL!';
		end if;
		-----------------------------------------
		return query
		with
		w1 as 	(
				select _metadatas as metadatas
				)
		,w2 as	(
				select json_array_elements(w1.metadatas) as metadatas from w1
				)
		,w3 as	(
				select
						(w2.metadatas->>'area_domain')::integer as area_domain_target,
						(w2.metadatas->>'area_domain_category')::integer as area_domain_category_target,
						(w2.metadatas->>'label')::varchar as label_source,
						(w2.metadatas->>'description')::text as description_source,
						(w2.metadatas->>'label_en')::varchar as label_en_source,
						(w2.metadatas->>'description_en')::text as description_en_source
				from
						w2
				)
		,w4 as	(
				select
						w3.*,
						t.label as label_target,
						t.description as description_target,
						t.label_en as label_en_target,
						t.description_en as description_en_target
				from
						w3
						inner join	(
									select cadc.* from @extschema@.c_area_domain_category as cadc
									where cadc.area_domain = (select distinct w3.area_domain_target from w3)
									) as t
						on w3.area_domain_target = t.area_domain
						and w3.area_domain_category_target = t.id
				)
		,w5 as	(
				select
						w4.*,
						string_to_array(w4.label_source,';') as array_label_source,
						string_to_array(w4.description_source,';') as array_description_source,
						string_to_array(w4.label_en_source,';') as array_label_en_source,
						string_to_array(w4.description_en_source,';') as array_description_en_source,
						string_to_array(w4.label_target,';') as array_label_target,
						string_to_array(w4.description_target,';') as array_description_target,
						string_to_array(w4.label_en_target,';') as array_label_en_target,
						string_to_array(w4.description_en_target,';') as array_description_en_target
				from
						w4
				)
		,w6 as	(
				select
						w5.*,
						@extschema@.fn_etl_array_compare(w5.array_label_source,w5.array_label_target) as check_label,
						@extschema@.fn_etl_array_compare(w5.array_description_source,w5.array_description_target) as check_description,
						@extschema@.fn_etl_array_compare(w5.array_label_en_source,w5.array_label_en_target) as check_label_en,
						@extschema@.fn_etl_array_compare(w5.array_description_en_source,w5.array_description_en_target) as check_description_en
				from
						w5
				)
		select
				w6.area_domain_category_target as area_domain_category,
				w6.label_source,
				w6.description_source,
				w6.label_en_source,
				w6.description_en_source,
				w6.label_target,
				w6.description_target,
				w6.label_en_target,
				w6.description_en_target,
				w6.check_label,
				w6.check_description,
				w6.check_label_en,
				w6.check_description_en
		from
				w6
		where
				w6.check_label = false or
				w6.check_description = false or
				w6.check_label_en = false or
				w6.check_description_en = false;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_check_area_domain_categories4update(json) is
'The tunction returns records of area domain categories with informations whether their source labels or descriptions are different from target.';

grant execute on function @extschema@.fn_etl_check_area_domain_categories4update(json) to public;
-- </function>



-- <function name="fn_etl_update_area_domain_category_label" schema="extschema" src="functions/extschema/etl/fn_etl_update_area_domain_category_label.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_update_area_domain_category_label(integer, character varying, varchar)

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_update_area_domain_category_label(integer, character varying, varchar) CASCADE;

create or replace function @extschema@.fn_etl_update_area_domain_category_label
(
	_area_domain_category	integer,
	_national_language		character varying(2),
	_label					varchar
)
returns text
as
$$
declare
		_res	text;
begin
		if _area_domain_category is null
		then
			raise exception 'Error 01: fn_etl_update_area_domain_category_label: Input argument _area_domain_category must not be NULL!';
		end if;
	
		if _national_language is null
		then
			raise exception 'Error 02: fn_etl_update_area_domain_category_label: Input argument _national_language must not be NULL!';
		end if;
	
		if _label is null
		then
			raise exception 'Error 03: fn_etl_update_area_domain_category_label: Input argument _label must not be NULL!';
		end if;
		-----------------------------------------
		if	(
			select count(cadc.*) is distinct from 1
			from @extschema@.c_area_domain_category as cadc
			where cadc.id = _area_domain_category
			)
		then
			raise exception 'Error 04: fn_etl_update_area_domain_category_label: Input argument _area_domain_category (ID) = % is not present in c_area_domain_category table!',_area_domain_category;
		end if;
		-----------------------------------------
		if _national_language = 'en'::varchar
		then
			update @extschema@.c_area_domain_category set label_en = _label where id = _area_domain_category;
		else
			update @extschema@.c_area_domain_category set label = _label where id = _area_domain_category;
		end if;
		-----------------------------------------
		_res := concat('The label of area domain category [c_area_domain_category.id = ',_area_domain_category,'] for language element = "',_national_language,'" was changed.');
		-----------------------------------------
		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_update_area_domain_category_label(integer, character varying, varchar) is
'The tunction updates an area domain category label in c_area_domain_category table for given area domain category.';

grant execute on function @extschema@.fn_etl_update_area_domain_category_label(integer, character varying, varchar) to public;
-- </function>



-- <function name="fn_etl_update_area_domain_category_description" schema="extschema" src="functions/extschema/etl/fn_etl_update_area_domain_category_description.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_update_area_domain_category_description(integer, character varying, text)

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_update_area_domain_category_description(integer, character varying, text) CASCADE;

create or replace function @extschema@.fn_etl_update_area_domain_category_description
(
	_area_domain_category	integer,
	_national_language		character varying(2),
	_description			text
)
returns text
as
$$
declare
		_res	text;
begin
		if _area_domain_category is null
		then
			raise exception 'Error 01: fn_etl_update_area_domain_category_description: Input argument _area_domain_category must not be NULL!';
		end if;
	
		if _national_language is null
		then
			raise exception 'Error 02: fn_etl_update_area_domain_category_description: Input argument _national_language must not be NULL!';
		end if;
	
		if _description is null
		then
			raise exception 'Error 03: fn_etl_update_area_domain_category_description: Input argument _description must not be NULL!';
		end if;
		-----------------------------------------
		if	(
			select count(cadc.*) is distinct from 1
			from @extschema@.c_area_domain_category as cadc
			where cadc.id = _area_domain_category
			)
		then
			raise exception 'Error 04: fn_etl_update_area_domain_category_description: Input argument _area_domain_category (ID) = % is not present in c_area_domain_category table!',_area_domain_category;
		end if;
		-----------------------------------------
		if _national_language = 'en'::varchar
		then
			update @extschema@.c_area_domain_category set description_en = _description where id = _area_domain_category;
		else
			update @extschema@.c_area_domain_category set description = _description where id = _area_domain_category;
		end if;
		-----------------------------------------
		_res := concat('The description of area domain category [c_area_domain_category.id = ',_area_domain_category,'] for language element = "',_national_language,'" was changed.');
		-----------------------------------------
		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_update_area_domain_category_description(integer, character varying, text) is
'The tunction updates an area domain category description in c_area_domain_category table for given area domain category.';

grant execute on function @extschema@.fn_etl_update_area_domain_category_description(integer, character varying, text) to public;
-- </function>



-- <function name="fn_etl_check_sub_populations4update" schema="extschema" src="functions/extschema/etl/fn_etl_check_sub_populations4update.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_check_sub_populations4update(json)

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_check_sub_populations4update(json) CASCADE;

create or replace function @extschema@.fn_etl_check_sub_populations4update
(
	_metadatas json
)
returns table
(
	sub_population			integer,
	label_source			varchar,
	description_source		text,
	label_en_source			varchar,
	description_en_source	text,
	label_target			varchar,
	description_target		text,
	label_en_target			varchar,
	description_en_target	text,
	check_label				boolean,
	check_description		boolean,
	check_label_en			boolean,
	check_description_en	boolean
)
as
$$
declare
begin
		if _metadatas is null
		then
			raise exception 'Error 01: fn_etl_check_sub_populations4update: Input argument _metadatas must not be NULL!';
		end if;
		-----------------------------------------
		return query
		with
		w1 as 	(
				select _metadatas as metadatas
				)
		,w2 as	(
				select json_array_elements(w1.metadatas) as metadatas from w1
				)
		,w3 as	(
				select
						(w2.metadatas->>'sub_population')::integer as sub_population_target,
						(w2.metadatas->>'label')::varchar as label_source,
						(w2.metadatas->>'description')::text as description_source,
						(w2.metadatas->>'label_en')::varchar as label_en_source,
						(w2.metadatas->>'description_en')::text as description_en_source
				from
						w2
				)
		,w4 as	(
				select
						w3.*,
						csp.label as label_target,
						csp.description as description_target,
						csp.label_en as label_en_target,
						csp.description_en as description_en_target
				from
						w3
						inner join @extschema@.c_sub_population as csp
						on w3.sub_population_target = csp.id
				)
		,w5 as	(
				select
						w4.*,
						string_to_array(w4.label_source,';') as array_label_source,
						string_to_array(w4.description_source,';') as array_description_source,
						string_to_array(w4.label_en_source,';') as array_label_en_source,
						string_to_array(w4.description_en_source,';') as array_description_en_source,
						string_to_array(w4.label_target,';') as array_label_target,
						string_to_array(w4.description_target,';') as array_description_target,
						string_to_array(w4.label_en_target,';') as array_label_en_target,
						string_to_array(w4.description_en_target,';') as array_description_en_target
				from
						w4
				)
		,w6 as	(
				select
						w5.*,
						@extschema@.fn_etl_array_compare(w5.array_label_source,w5.array_label_target) as check_label,
						@extschema@.fn_etl_array_compare(w5.array_description_source,w5.array_description_target) as check_description,
						@extschema@.fn_etl_array_compare(w5.array_label_en_source,w5.array_label_en_target) as check_label_en,
						@extschema@.fn_etl_array_compare(w5.array_description_en_source,w5.array_description_en_target) as check_description_en
				from
						w5
				)
		select
				w6.sub_population_target as sub_population,
				w6.label_source,
				w6.description_source,
				w6.label_en_source,
				w6.description_en_source,
				w6.label_target,
				w6.description_target,
				w6.label_en_target,
				w6.description_en_target,
				w6.check_label,
				w6.check_description,
				w6.check_label_en,
				w6.check_description_en
		from
				w6
		where
				w6.check_label = false or
				w6.check_description = false or
				w6.check_label_en = false or
				w6.check_description_en = false;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_check_sub_populations4update(json) is
'The tunction returns records of sub populations with informations whether their source labels or descriptions are different from target.';

grant execute on function @extschema@.fn_etl_check_sub_populations4update(json) to public;
-- </function>



-- <function name="fn_etl_update_sub_population_label" schema="extschema" src="functions/extschema/etl/fn_etl_update_sub_population_label.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_update_sub_population_label(integer, character varying, varchar)

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_update_sub_population_label(integer, character varying, varchar) CASCADE;

create or replace function @extschema@.fn_etl_update_sub_population_label
(
	_sub_population		integer,
	_national_language	character varying(2),
	_label				varchar
)
returns text
as
$$
declare
		_res	text;
begin
		if _sub_population is null
		then
			raise exception 'Error 01: fn_etl_update_sub_population_label: Input argument _sub_population must not be NULL!';
		end if;
	
		if _national_language is null
		then
			raise exception 'Error 02: fn_etl_update_sub_population_label: Input argument _national_language must not be NULL!';
		end if;
	
		if _label is null
		then
			raise exception 'Error 03: fn_etl_update_sub_population_label: Input argument _label must not be NULL!';
		end if;
		-----------------------------------------
		if	(
			select count(csp.*) is distinct from 1
			from @extschema@.c_sub_population as csp
			where csp.id = _sub_population
			)
		then
			raise exception 'Error 04: fn_etl_update_sub_population_label: Input argument _sub_population (ID) = % is not present in c_sub_population table!',_sub_population;
		end if;
		-----------------------------------------
		if _national_language = 'en'::varchar
		then
			update @extschema@.c_sub_population set label_en = _label where id = _sub_population;
		else
			update @extschema@.c_sub_population set label = _label where id = _sub_population;
		end if;
		-----------------------------------------
		_res := concat('The label of sub population [c_sub_population.id = ',_sub_population,'] for language element = "',_national_language,'" was changed.');
		-----------------------------------------
		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_update_sub_population_label(integer, character varying, varchar) is
'The tunction updates a sub population label in c_sub_population table for given sub population.';

grant execute on function @extschema@.fn_etl_update_sub_population_label(integer, character varying, varchar) to public;
-- </function>



-- <function name="fn_etl_update_sub_population_description" schema="extschema" src="functions/extschema/etl/fn_etl_update_sub_population_description.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_update_sub_population_description(integer, character varying, text)

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_update_sub_population_description(integer, character varying, text) CASCADE;

create or replace function @extschema@.fn_etl_update_sub_population_description
(
	_sub_population		integer,
	_national_language	character varying(2),
	_description		text
)
returns text
as
$$
declare
		_res	text;
begin
		if _sub_population is null
		then
			raise exception 'Error 01: fn_etl_update_sub_population_description: Input argument _sub_population must not be NULL!';
		end if;
	
		if _national_language is null
		then
			raise exception 'Error 02: fn_etl_update_sub_population_description: Input argument _national_language must not be NULL!';
		end if;
	
		if _description is null
		then
			raise exception 'Error 03: fn_etl_update_sub_population_description: Input argument _description must not be NULL!';
		end if;
		-----------------------------------------
		if	(
			select count(csp.*) is distinct from 1
			from @extschema@.c_sub_population as csp
			where csp.id = _sub_population
			)
		then
			raise exception 'Error 04: fn_etl_update_sub_population_description: Input argument _sub_population (ID) = % is not present in c_sub_population table!',_sub_population;
		end if;
		-----------------------------------------
		if _national_language = 'en'::varchar
		then
			update @extschema@.c_sub_population set description_en = _description where id = _sub_population;
		else
			update @extschema@.c_sub_population set description = _description where id = _sub_population;
		end if;
		-----------------------------------------
		_res := concat('The description of sub population [c_sub_population.id = ',_sub_population,'] for language element = "',_national_language,'" was changed.');
		-----------------------------------------
		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_update_sub_population_description(integer, character varying, text) is
'The tunction updates a sub population description in c_sub_population table for given sub population.';

grant execute on function @extschema@.fn_etl_update_sub_population_description(integer, character varying, text) to public;
-- </function>



-- <function name="fn_etl_check_sub_population_categories4update" schema="extschema" src="functions/extschema/etl/fn_etl_check_sub_population_categories4update.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_check_sub_population_categories4update(json)

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_check_sub_population_categories4update(json) CASCADE;

create or replace function @extschema@.fn_etl_check_sub_population_categories4update
(
	_metadatas json
)
returns table
(
	sub_population_category	integer,
	label_source			varchar,
	description_source		text,
	label_en_source			varchar,
	description_en_source	text,
	label_target			varchar,
	description_target		text,
	label_en_target			varchar,
	description_en_target	text,
	check_label				boolean,
	check_description		boolean,
	check_label_en			boolean,
	check_description_en	boolean
)
as
$$
declare
begin
		if _metadatas is null
		then
			raise exception 'Error 01: fn_etl_check_sub_population_categories4update: Input argument _metadatas must not be NULL!';
		end if;
		-----------------------------------------
		return query
		with
		w1 as 	(
				select _metadatas as metadatas
				)
		,w2 as	(
				select json_array_elements(w1.metadatas) as metadatas from w1
				)
		,w3 as	(
				select
						(w2.metadatas->>'sub_population')::integer as sub_population_target,
						(w2.metadatas->>'sub_population_category')::integer as sub_population_category_target,
						(w2.metadatas->>'label')::varchar as label_source,
						(w2.metadatas->>'description')::text as description_source,
						(w2.metadatas->>'label_en')::varchar as label_en_source,
						(w2.metadatas->>'description_en')::text as description_en_source
				from
						w2
				)
		,w4 as	(
				select
						w3.*,
						t.label as label_target,
						t.description as description_target,
						t.label_en as label_en_target,
						t.description_en as description_en_target
				from
						w3
						inner join	(
									select cspc.* from @extschema@.c_sub_population_category as cspc
									where cspc.sub_population = (select distinct w3.sub_population_target from w3)
									) as t
						on w3.sub_population_target = t.sub_population
						and w3.sub_population_category_target = t.id
				)
		,w5 as	(
				select
						w4.*,
						string_to_array(w4.label_source,';') as array_label_source,
						string_to_array(w4.description_source,';') as array_description_source,
						string_to_array(w4.label_en_source,';') as array_label_en_source,
						string_to_array(w4.description_en_source,';') as array_description_en_source,
						string_to_array(w4.label_target,';') as array_label_target,
						string_to_array(w4.description_target,';') as array_description_target,
						string_to_array(w4.label_en_target,';') as array_label_en_target,
						string_to_array(w4.description_en_target,';') as array_description_en_target
				from
						w4
				)
		,w6 as	(
				select
						w5.*,
						@extschema@.fn_etl_array_compare(w5.array_label_source,w5.array_label_target) as check_label,
						@extschema@.fn_etl_array_compare(w5.array_description_source,w5.array_description_target) as check_description,
						@extschema@.fn_etl_array_compare(w5.array_label_en_source,w5.array_label_en_target) as check_label_en,
						@extschema@.fn_etl_array_compare(w5.array_description_en_source,w5.array_description_en_target) as check_description_en
				from
						w5
				)
		select
				w6.sub_population_category_target as sub_population_category,
				w6.label_source,
				w6.description_source,
				w6.label_en_source,
				w6.description_en_source,
				w6.label_target,
				w6.description_target,
				w6.label_en_target,
				w6.description_en_target,
				w6.check_label,
				w6.check_description,
				w6.check_label_en,
				w6.check_description_en
		from
				w6
		where
				w6.check_label = false or
				w6.check_description = false or
				w6.check_label_en = false or
				w6.check_description_en = false;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_check_sub_population_categories4update(json) is
'The tunction returns records of sub population categories with informations whether their source labels or descriptions are different from target.';

grant execute on function @extschema@.fn_etl_check_sub_population_categories4update(json) to public;
-- </function>



-- <function name="fn_etl_update_sub_population_category_label" schema="extschema" src="functions/extschema/etl/fn_etl_update_sub_population_category_label.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_update_sub_population_category_label(integer, character varying, varchar)

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_update_sub_population_category_label(integer, character varying, varchar) CASCADE;

create or replace function @extschema@.fn_etl_update_sub_population_category_label
(
	_sub_population_category	integer,
	_national_language			character varying(2),
	_label						varchar
)
returns text
as
$$
declare
		_res	text;
begin
		if _sub_population_category is null
		then
			raise exception 'Error 01: fn_etl_update_sub_population_category_label: Input argument _sub_population_category must not be NULL!';
		end if;
	
		if _national_language is null
		then
			raise exception 'Error 02: fn_etl_update_sub_population_category_label: Input argument _national_language must not be NULL!';
		end if;
	
		if _label is null
		then
			raise exception 'Error 03: fn_etl_update_sub_population_category_label: Input argument _label must not be NULL!';
		end if;
		-----------------------------------------
		if	(
			select count(cspc.*) is distinct from 1
			from @extschema@.c_sub_population_category as cspc
			where cspc.id = _sub_population_category
			)
		then
			raise exception 'Error 04: fn_etl_update_sub_population_category_label: Input argument _sub_population_category (ID) = % is not present in c_sub_population_category table!',_sub_population_category;
		end if;
		-----------------------------------------
		if _national_language = 'en'::varchar
		then
			update @extschema@.c_sub_population_category set label_en = _label where id = _sub_population_category;
		else
			update @extschema@.c_sub_population_category set label = _label where id = _sub_population_category;
		end if;
		-----------------------------------------
		_res := concat('The label of sub population category [c_sub_population_category.id = ',_sub_population_category,'] for language element = "',_national_language,'" was changed.');
		-----------------------------------------
		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_update_sub_population_category_label(integer, character varying, varchar) is
'The tunction updates a sub population category label in c_sub_population_category table for given sub population category.';

grant execute on function @extschema@.fn_etl_update_sub_population_category_label(integer, character varying, varchar) to public;
-- </function>



-- <function name="fn_etl_update_sub_population_category_description" schema="extschema" src="functions/extschema/etl/fn_etl_update_sub_population_category_description.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_update_sub_population_category_description(integer, character varying, text)

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_update_sub_population_category_description(integer, character varying, text) CASCADE;

create or replace function @extschema@.fn_etl_update_sub_population_category_description
(
	_sub_population_category	integer,
	_national_language			character varying(2),
	_description				text
)
returns text
as
$$
declare
		_res	text;
begin
		if _sub_population_category is null
		then
			raise exception 'Error 01: fn_etl_update_sub_population_category_description: Input argument _sub_population_category must not be NULL!';
		end if;
	
		if _national_language is null
		then
			raise exception 'Error 02: fn_etl_update_sub_population_category_description: Input argument _national_language must not be NULL!';
		end if;
	
		if _description is null
		then
			raise exception 'Error 03: fn_etl_update_sub_population_category_description: Input argument _description must not be NULL!';
		end if;
		-----------------------------------------
		if	(
			select count(cspc.*) is distinct from 1
			from @extschema@.c_sub_population_category as cspc
			where cspc.id = _sub_population_category
			)
		then
			raise exception 'Error 04: fn_etl_update_sub_population_category_description: Input argument _sub_population_category (ID) = % is not present in c_sub_population_category table!',_sub_population_category;
		end if;
		-----------------------------------------
		if _national_language = 'en'::varchar
		then
			update @extschema@.c_sub_population_category set description_en = _description where id = _sub_population_category;
		else
			update @extschema@.c_sub_population_category set description = _description where id = _sub_population_category;
		end if;
		-----------------------------------------
		_res := concat('The description of area domain category [c_sub_population_category.id = ',_sub_population_category,'] for language element = "',_national_language,'" was changed.');
		-----------------------------------------
		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_update_sub_population_category_description(integer, character varying, text) is
'The tunction updates a sub population category description in c_sub_population table for given sub population category.';

grant execute on function @extschema@.fn_etl_update_sub_population_category_description(integer, character varying, text) to public;
-- </function>



-- <function name="fn_etl_check_area_domain_categories4area_domains" schema="extschema" src="functions/extschema/etl/fn_etl_check_area_domain_categories4area_domains.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_check_area_domain_categories4area_domains(json)

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_check_area_domain_categories4area_domains(json) CASCADE;

create or replace function @extschema@.fn_etl_check_area_domain_categories4area_domains
(
	_metadatas json
)
returns integer[]
as
$$
declare
	_res integer[];
begin
		if _metadatas is null
		then
			raise exception 'Error 01: fn_etl_check_area_domain_categories4area_domains: Input argument _metadatas must not be NULL!';
		end if;
		-----------------------------------------
		with
		w1 as 	(
				select _metadatas as metadatas
				)
		,w2 as	(
				select json_array_elements(w1.metadatas) as metadatas from w1
				)
		,w3 as	(
				select
						(w2.metadatas->>'area_domain')::integer as area_domain_target,
						(w2.metadatas->>'area_domain_category')::integer as area_domain_category_target,
						(w2.metadatas->>'label')::varchar as label_source,
						(w2.metadatas->>'description')::text as description_source,
						(w2.metadatas->>'label_en')::varchar as label_en_source,
						(w2.metadatas->>'description_en')::text as description_en_source
				from
						w2
				)
		,w4 as	(
				select
						w3.*,
						t.label as label_target,
						t.description as description_target,
						t.label_en as label_en_target,
						t.description_en as description_en_target
				from
						w3
						inner join	(
									select cadc.* from @extschema@.c_area_domain_category as cadc
									where cadc.area_domain in (select distinct w3.area_domain_target from w3)
									) as t
						on w3.area_domain_target = t.area_domain
						and w3.area_domain_category_target = t.id
				)
		,w5 as	(
				select
						w4.*,
						string_to_array(w4.label_source,';') as array_label_source,
						string_to_array(w4.description_source,';') as array_description_source,
						string_to_array(w4.label_en_source,';') as array_label_en_source,
						string_to_array(w4.description_en_source,';') as array_description_en_source,
						string_to_array(w4.label_target,';') as array_label_target,
						string_to_array(w4.description_target,';') as array_description_target,
						string_to_array(w4.label_en_target,';') as array_label_en_target,
						string_to_array(w4.description_en_target,';') as array_description_en_target
				from
						w4
				)
		,w6 as	(
				select
						w5.*,
						@extschema@.fn_etl_array_compare(w5.array_label_source,w5.array_label_target) as check_label,
						@extschema@.fn_etl_array_compare(w5.array_description_source,w5.array_description_target) as check_description,
						@extschema@.fn_etl_array_compare(w5.array_label_en_source,w5.array_label_en_target) as check_label_en,
						@extschema@.fn_etl_array_compare(w5.array_description_en_source,w5.array_description_en_target) as check_description_en
				from
						w5
				)
		select array_agg(t.area_domain_target order by t.area_domain_target)
		from	(
				select distinct w6.area_domain_target from w6
				where w6.check_label = false or w6.check_description = false
				or w6.check_label_en = false or w6.check_description_en = false
				) as t
		into _res;

		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_check_area_domain_categories4area_domains(json) is
'The tunction returns array of IDs of area domains if their source label or description categories are different from target.';

grant execute on function @extschema@.fn_etl_check_area_domain_categories4area_domains(json) to public;
-- </function>



-- <function name="fn_etl_check_sub_population_categories4sub_populations" schema="extschema" src="functions/extschema/etl/fn_etl_check_sub_population_categories4sub_populations.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_check_sub_population_categories4sub_populations(json)

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_check_sub_population_categories4sub_populations(json) CASCADE;

create or replace function @extschema@.fn_etl_check_sub_population_categories4sub_populations
(
	_metadatas json
)
returns integer[]
as
$$
declare
	_res integer[];
begin
		if _metadatas is null
		then
			raise exception 'Error 01: fn_etl_check_sub_population_categories4sub_populations: Input argument _metadatas must not be NULL!';
		end if;
		-----------------------------------------
		with
		w1 as 	(
				select _metadatas as metadatas
				)
		,w2 as	(
				select json_array_elements(w1.metadatas) as metadatas from w1
				)
		,w3 as	(
				select
						(w2.metadatas->>'sub_population')::integer as sub_population_target,
						(w2.metadatas->>'sub_population_category')::integer as sub_population_category_target,
						(w2.metadatas->>'label')::varchar as label_source,
						(w2.metadatas->>'description')::text as description_source,
						(w2.metadatas->>'label_en')::varchar as label_en_source,
						(w2.metadatas->>'description_en')::text as description_en_source
				from
						w2
				)
		,w4 as	(
				select
						w3.*,
						t.label as label_target,
						t.description as description_target,
						t.label_en as label_en_target,
						t.description_en as description_en_target
				from
						w3
						inner join	(
									select cspc.* from @extschema@.c_sub_population_category as cspc
									where cspc.sub_population in (select distinct w3.sub_population_target from w3)
									) as t
						on w3.sub_population_target = t.sub_population
						and w3.sub_population_category_target = t.id
				)
		,w5 as	(
				select
						w4.*,
						string_to_array(w4.label_source,';') as array_label_source,
						string_to_array(w4.description_source,';') as array_description_source,
						string_to_array(w4.label_en_source,';') as array_label_en_source,
						string_to_array(w4.description_en_source,';') as array_description_en_source,
						string_to_array(w4.label_target,';') as array_label_target,
						string_to_array(w4.description_target,';') as array_description_target,
						string_to_array(w4.label_en_target,';') as array_label_en_target,
						string_to_array(w4.description_en_target,';') as array_description_en_target
				from
						w4
				)
		,w6 as	(
				select
						w5.*,
						@extschema@.fn_etl_array_compare(w5.array_label_source,w5.array_label_target) as check_label,
						@extschema@.fn_etl_array_compare(w5.array_description_source,w5.array_description_target) as check_description,
						@extschema@.fn_etl_array_compare(w5.array_label_en_source,w5.array_label_en_target) as check_label_en,
						@extschema@.fn_etl_array_compare(w5.array_description_en_source,w5.array_description_en_target) as check_description_en
				from
						w5
				)
		select array_agg(t.sub_population_target order by t.sub_population_target)
		from	(
				select distinct w6.sub_population_target from w6
				where w6.check_label = false or w6.check_description = false
				or w6.check_label_en = false or w6.check_description_en = false
				) as t
		into _res;

		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_check_sub_population_categories4sub_populations(json) is
'The tunction returns array of IDs of sub populations if their source label or description categories are different from target.';

grant execute on function @extschema@.fn_etl_check_sub_population_categories4sub_populations(json) to public;
-- </function>

