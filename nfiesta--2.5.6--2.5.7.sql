--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- <view name="v_variable_hierarchy" schema="extschema" src="views/extschema/v_variable_hierarchy.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

CREATE OR REPLACE VIEW @extschema@.v_variable_hierarchy AS
SELECT
	variable_superior as node,
	array_agg(variable order by variable) AS edges
FROM @extschema@.t_variable_hierarchy
INNER JOIN @extschema@.t_variable 			AS edge_var	ON (t_variable_hierarchy.variable		=	edge_var.id)
LEFT JOIN @extschema@.c_sub_population_category 	AS edge_spc	ON (edge_var.sub_population_category		=	edge_spc.id)
LEFT JOIN @extschema@.c_area_domain_category 		AS edge_adc	ON (edge_var.area_domain_category		=	edge_adc.id)
LEFT JOIN @extschema@.c_auxiliary_variable_category	AS edge_avc	ON (edge_var.auxiliary_variable_category	=	edge_avc.id)
GROUP BY
	t_variable_hierarchy.variable_superior,
	edge_spc.sub_population,
	edge_adc.area_domain,
	edge_avc.auxiliary_variable
ORDER BY
	t_variable_hierarchy.variable_superior,
	edge_spc.sub_population,
	edge_adc.area_domain,
	edge_avc.auxiliary_variable
;

-- </view>

-- <view name="v_add_plot_target_attr" schema="extschema" src="views/extschema/v_add_plot_target_attr.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

create or replace view @extschema@.v_add_plot_target_attr as
with w_plot_var as not materialized (
	select
		f_p_plot.gid as plot, t_available_datasets.variable, t_available_datasets.reference_year_set,
		coalesce(t_target_data.value, 0) as value
	from @extschema@.f_p_plot
	inner join @extschema@.t_cluster on (f_p_plot.cluster = t_cluster.id)
	inner join @extschema@.cm_cluster2panel_mapping on (t_cluster.id = cm_cluster2panel_mapping.cluster)
	inner join @extschema@.t_panel on (cm_cluster2panel_mapping.panel = t_panel.id)
	inner join @extschema@.t_available_datasets on (t_panel.id = t_available_datasets.panel)
	inner join @extschema@.t_variable on (t_available_datasets.variable = t_variable.id and t_variable.auxiliary_variable_category is null)
	left join @extschema@.t_target_data on (
		f_p_plot.gid = t_target_data.plot
		and t_available_datasets.variable = t_target_data.variable
		and t_available_datasets.reference_year_set = t_target_data.reference_year_set
		and t_target_data.is_latest)
)
, w_node_sum as (
	select
		plot,
		w_plot_var.reference_year_set,
		w_plot_var.variable,
		value as node_sum,
		v_variable_hierarchy.node,
		v_variable_hierarchy.edges as edges_def
	from w_plot_var
	inner join @extschema@.v_variable_hierarchy on (v_variable_hierarchy.node = w_plot_var.variable)
)
, w_edge_sum as (
	select
		w_node_sum.plot,
		w_node_sum.reference_year_set,
		w_node_sum.node,
		w_node_sum.node_sum,
		w_node_sum.edges_def,
		array_agg(w_plot_var.variable order by w_plot_var.variable) as edges_found,
		sum(w_plot_var.value) as edges_sum
	from w_node_sum
	left join w_plot_var on (
		w_plot_var.plot = w_node_sum.plot
		and w_plot_var.reference_year_set = w_node_sum.reference_year_set
		and w_plot_var.variable = any(w_node_sum.edges_def))
	group by w_node_sum.plot, w_node_sum.reference_year_set,
		w_node_sum.node, w_node_sum.node_sum, w_node_sum.edges_def
)
, w_diff as (
	select
		plot,
		reference_year_set,
		node		as variable,
		node_sum	as ldsity,
		edges_sum	as ldsity_sum,
		edges_def	as variables_def,
		edges_found	as variables_found,
		case
			when node_sum != 0.0 and edges_sum = 0.0 then 100.0
			when node_sum = 0.0 and edges_sum = 0.0 then 0
			else abs(1 - (node_sum / edges_sum )) * 100.0
		end as diff
	from w_edge_sum
)
select * from w_diff order by diff desc
;

comment on view @extschema@.v_add_plot_target_attr is
	'View showing plot level target local density attribute additivity (aggregated class estimate should be equal to sum of sub-classes estimates) of target local densities. Hierarchy between aggregated classes and its sub-classes is defined in v_variable_hierarchy.';

comment on column @extschema@.v_add_plot_target_attr.plot		is 'Plot. FKEY to f_p_plot.gid.';
comment on column @extschema@.v_add_plot_target_attr.reference_year_set	is 'Reference year set. FKEY to t_reference_year_set.id.';
comment on column @extschema@.v_add_plot_target_attr.variable		is 'Auxiliary total attribute -- variable. FKEY to t_variable.id.';
comment on column @extschema@.v_add_plot_target_attr.ldsity		is 'Aggregated class local density.';
comment on column @extschema@.v_add_plot_target_attr.ldsity_sum		is 'Sum of sub-classes local densities (belonging to aggregated class).';
comment on column @extschema@.v_add_plot_target_attr.variables_def	is 'Attributes -- variables defined in hierarchy (v_variable_hierarchy). Array of FKEYs to t_variable.id.';
comment on column @extschema@.v_add_plot_target_attr.variables_found	is 'Attributes -- variables found in data (t_auxiliary_data). Array of FKEYs to t_variable.id.';
comment on column @extschema@.v_add_plot_target_attr.diff		is 'Relative difference between aggregated class local densities and sum of sub-classes local densities.';

-- </view>

-- <view name="v_add_plot_aux_attr" schema="extschema" src="views/extschema/v_add_plot_aux_attr.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

create or replace view @extschema@.v_add_plot_aux_attr as
with w_plot_var as not materialized (
	select
		f_p_plot.gid as plot, t_available_datasets.variable, t_available_datasets.reference_year_set,
		coalesce(t_auxiliary_data.value, 0) as value
	from @extschema@.f_p_plot
	inner join @extschema@.t_cluster on (f_p_plot.cluster = t_cluster.id)
	inner join @extschema@.cm_cluster2panel_mapping on (t_cluster.id = cm_cluster2panel_mapping.cluster)
	inner join @extschema@.t_panel on (cm_cluster2panel_mapping.panel = t_panel.id)
	inner join @extschema@.t_available_datasets on (t_panel.id = t_available_datasets.panel)
	inner join @extschema@.t_variable on (t_available_datasets.variable = t_variable.id and t_variable.auxiliary_variable_category is not null)
	left join @extschema@.t_auxiliary_data on (
		f_p_plot.gid = t_auxiliary_data.plot
		and t_available_datasets.variable = t_auxiliary_data.variable
		and t_auxiliary_data.is_latest)

)
, w_node_sum as (
	select
		plot,

		w_plot_var.variable,
		value as node_sum,
		v_variable_hierarchy.node,
		v_variable_hierarchy.edges as edges_def
	from w_plot_var
	inner join @extschema@.v_variable_hierarchy on (v_variable_hierarchy.node = w_plot_var.variable)
)
, w_edge_sum as (
	select
		w_node_sum.plot,

		w_node_sum.node,
		w_node_sum.node_sum,
		w_node_sum.edges_def,
		array_agg(w_plot_var.variable order by w_plot_var.variable) as edges_found,
		sum(w_plot_var.value) as edges_sum
	from w_node_sum
	left join w_plot_var on (
		w_plot_var.plot = w_node_sum.plot

		and w_plot_var.variable = any(w_node_sum.edges_def))
	group by w_node_sum.plot,
		w_node_sum.node, w_node_sum.node_sum, w_node_sum.edges_def
)
, w_diff as (
	select
		plot,

		node		as variable,
		node_sum	as ldsity,
		edges_sum	as ldsity_sum,
		edges_def	as variables_def,
		edges_found	as variables_found,
		case
			when node_sum != 0.0 and edges_sum = 0.0 then 100.0
			when node_sum = 0.0 and edges_sum = 0.0 then 0
			else abs(1 - (node_sum / edges_sum )) * 100.0
		end as diff
	from w_edge_sum
)
select * from w_diff order by diff desc
;

comment on view @extschema@.v_add_plot_aux_attr is
	'View showing plot level auxiliary local density attribute additivity (aggregated class estimate should be equal to sum of sub-classes estimates) of auxiliary local densities. Hierarchy between aggregated classes and its sub-classes is defined in v_variable_hierarchy.';

comment on column @extschema@.v_add_plot_aux_attr.plot			is 'Plot. FKEY to f_p_plot.gid.';

comment on column @extschema@.v_add_plot_aux_attr.variable		is 'Auxiliary total attribute -- variable. FKEY to t_variable.id.';
comment on column @extschema@.v_add_plot_aux_attr.ldsity		is 'Aggregated class local density.';
comment on column @extschema@.v_add_plot_aux_attr.ldsity_sum		is 'Sum of sub-classes local densities (belonging to aggregated class).';
comment on column @extschema@.v_add_plot_aux_attr.variables_def	is 'Attributes -- variables defined in hierarchy (v_variable_hierarchy). Array of FKEYs to t_variable.id.';
comment on column @extschema@.v_add_plot_aux_attr.variables_found	is 'Attributes -- variables found in data (t_auxiliary_data). Array of FKEYs to t_variable.id.';
comment on column @extschema@.v_add_plot_aux_attr.diff			is 'Relative difference between aggregated class local densities and sum of sub-classes local densities.';

-- </view>

-- <view name="v_add_aux_total_attr" schema="extschema" src="views/extschema/v_add_aux_total_attr.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

create or replace view @extschema@.v_add_aux_total_attr as
with w_auxtotal_cell_var as not materialized (
	select
		t_aux_total.estimation_cell,
		t_aux_total.aux_total,
		t_aux_total.variable
	from @extschema@.t_aux_total
	where t_aux_total.is_latest
)
, w_node_sum as (
	select
		estimation_cell,
		variable,
		sum(aux_total) as node_sum,
		hierarchy.node,
		hierarchy.edges as edges_def
	from w_auxtotal_cell_var
	inner join @extschema@.v_variable_hierarchy		as hierarchy on (hierarchy.node = w_auxtotal_cell_var.variable)
	group by estimation_cell, variable, node, edges
	order by estimation_cell, variable, node, edges
)
, w_edge_sum as (
	select
		w_node_sum.estimation_cell,
		w_node_sum.node,
		w_node_sum.node_sum,
		w_node_sum.edges_def,
		array_agg(w_auxtotal_cell_var.variable order by w_auxtotal_cell_var.variable) as edges_found,
		sum(w_auxtotal_cell_var.aux_total) as edges_sum
	from w_node_sum
	left join w_auxtotal_cell_var on (
		w_auxtotal_cell_var.estimation_cell = w_node_sum.estimation_cell
		and w_auxtotal_cell_var.variable = any(w_node_sum.edges_def))
	group by w_node_sum.estimation_cell, w_node_sum.node, w_node_sum.node_sum, w_node_sum.edges_def
)
, w_diff as (
	select
		estimation_cell,
		node			as variable,
		node_sum		as aux_total,
		edges_sum		as aux_total_sum,
		edges_def		as variables_def,
		edges_found		as variables_found,
		case
			when node_sum != 0.0 and edges_sum = 0.0 then 100.0
			when node_sum = 0.0 and edges_sum = 0.0 then 0
			else abs(1 - (node_sum / edges_sum )) * 100.0
		end as diff
	from w_edge_sum
)
select * from w_diff order by diff desc
;

comment on view @extschema@.v_add_aux_total_attr is
	'View showing auxiliary total attribute additivity (aggregated class estimate should be equal to sum of sub-classes estimates). Hierarchy between aggregated classes and its sub-classes is defined in v_variable_hierarchy.';

comment on column @extschema@.v_add_aux_total_attr.estimation_cell 	is 'Auxiliary total estimation cell. FKEY to c_estimation_cell.id.';
comment on column @extschema@.v_add_aux_total_attr.variable		is 'Auxiliary total attribute -- variable. FKEY to t_variable.id.';
comment on column @extschema@.v_add_aux_total_attr.aux_total		is 'Aggregated class auxiliary total.';
comment on column @extschema@.v_add_aux_total_attr.aux_total_sum	is 'Sum of sub-classes auxiliary totals (belonging to aggregated class).';
comment on column @extschema@.v_add_aux_total_attr.variables_def	is 'Attributes -- variables defined in hierarchy (v_variable_hierarchy). Array of FKEYs to t_variable.id.';
comment on column @extschema@.v_add_aux_total_attr.variables_found	is 'Attributes -- variables found in data (t_result). Array of FKEYs to t_variable.id.';
comment on column @extschema@.v_add_aux_total_attr.diff			is 'Relative difference between aggregated class auxiliary total and sum of sub-classes auxiliary totals.';

-- </view>

-- <view name="v_add_aux_total_geo" schema="extschema" src="views/extschema/v_add_aux_total_geo.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

create or replace view @extschema@.v_add_aux_total_geo as
with w_auxtotal_cell_var as not materialized (
	select
		t_aux_total.estimation_cell,
		t_aux_total.aux_total,
		t_aux_total.variable
	from @extschema@.t_aux_total
	where t_aux_total.is_latest
)
, w_node_sum as (
	select
		estimation_cell,
		variable,
		sum(aux_total) as node_sum,
		hierarchy.node,
		hierarchy.edges as edges_def
	from w_auxtotal_cell_var
	inner join @extschema@.v_estimation_cell_hierarchy	as hierarchy on (hierarchy.node = w_auxtotal_cell_var.estimation_cell)
	group by estimation_cell, variable, node, edges
	order by estimation_cell, variable, node, edges
)
, w_edge_sum as (
	select
		w_node_sum.variable,
		w_node_sum.node,
		w_node_sum.node_sum,
		w_node_sum.edges_def,
		array_agg(w_auxtotal_cell_var.estimation_cell order by w_auxtotal_cell_var.estimation_cell) as edges_found,
		sum(w_auxtotal_cell_var.aux_total) as edges_sum
	from w_node_sum
	left join w_auxtotal_cell_var on (
		w_auxtotal_cell_var.variable = w_node_sum.variable
		and w_auxtotal_cell_var.estimation_cell = any(w_node_sum.edges_def))
	group by w_node_sum.variable, w_node_sum.node, w_node_sum.node_sum, w_node_sum.edges_def
)
, w_diff as (
	select
		variable,
		node		as estimation_cell,
		node_sum	as aux_total,
		edges_sum	as aux_total_sum,
		edges_def	as estimation_cells_def,
		edges_found	as estimation_cells_found,
		case
			when node_sum != 0.0 and edges_sum = 0.0 then 100.0
			when node_sum = 0.0 and edges_sum = 0.0 then 0
			else abs(1 - (node_sum / edges_sum )) * 100.0
		end as diff
	from w_edge_sum
)
select * from w_diff order by diff desc
;

comment on view @extschema@.v_add_aux_total_geo is
	'View showing auxiliary total geographic additivity (aggregated class estimate should be equal to sum of sub-classes estimates). Hierarchy between aggregated classes and its sub-classes is defined in v_estimation_cell_hierarchy.';

comment on column @extschema@.v_add_aux_total_geo.variable			is 'Auxiliary total attribute -- variable. FKEY to t_variable.id.';
comment on column @extschema@.v_add_aux_total_geo.estimation_cell 		is 'Auxiliary total estimation cell. FKEY to c_estimation_cell.id.';
comment on column @extschema@.v_add_aux_total_geo.aux_total			is 'Aggregated class auxiliary total.';
comment on column @extschema@.v_add_aux_total_geo.aux_total_sum			is 'Sum of sub-classes auxiliary total. (belonging to aggregated class).';
comment on column @extschema@.v_add_aux_total_geo.estimation_cells_def		is 'Estimation cells defined in hierarchy (v_estimation_cell_hierarchy). Array of FKEYs to c_estimation_cell.id.';
comment on column @extschema@.v_add_aux_total_geo.estimation_cells_found	is 'Estimation cells found in data (t_result). Array of FKEYs to c_estimation_cell.id.';
comment on column @extschema@.v_add_aux_total_geo.diff				is 'Relative difference between aggregated class auxiliary total and sum of sub-classes auxiliary totals.';

-- </view>

-- <view name="v_add_res_ratio_attr" schema="extschema" src="views/extschema/v_add_res_ratio_attr.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

create or replace view @extschema@.v_add_res_ratio_attr as
with w_res_cell_var as not materialized (
	select
		t_result.estimate_conf,
		t_result.point,
		t_total_estimate_conf.estimation_cell,
		t_total_estimate_conf.target_variable as t_variable__id,
		t_total_estimate_conf.aux_conf, force_synthetic, t_estimate_conf.denominator
	from @extschema@.t_result
	inner join @extschema@.t_estimate_conf ON t_estimate_conf.id = t_result.estimate_conf
	inner join @extschema@.t_total_estimate_conf ON t_total_estimate_conf.id = t_estimate_conf.total_estimate_conf
	where t_estimate_conf.estimate_type = 2
	and t_result.is_latest
)
, w_node_sum as (
	select
		estimation_cell,
		w_res_cell_var.t_variable__id, aux_conf, force_synthetic, denominator,
		estimate_conf as node_estimate_confs,
		sum(point) as node_sum,
		hierarchy.node,
		hierarchy.edges as edges_def
	from w_res_cell_var
	inner join @extschema@.v_variable_hierarchy 		as hierarchy on (hierarchy.node = w_res_cell_var.t_variable__id)
	group by estimation_cell, w_res_cell_var.t_variable__id, aux_conf, force_synthetic, estimate_conf, node, edges_def, denominator
	order by estimation_cell, w_res_cell_var.t_variable__id, aux_conf, force_synthetic, estimate_conf, node, edges_def, denominator
)
, w_edge_sum as (
	select
		w_node_sum.estimation_cell,
		w_node_sum.aux_conf,
		w_node_sum.force_synthetic,
		w_node_sum.node_estimate_confs, w_node_sum.denominator,
		w_node_sum.node,
		w_node_sum.node_sum,
		w_node_sum.edges_def,
		array_agg(w_res_cell_var.t_variable__id order by w_res_cell_var.t_variable__id) as edges_found,
		array_agg(w_res_cell_var.estimate_conf order by w_res_cell_var.estimate_conf) as edges_estimate_confs,
		sum(w_res_cell_var.point) as edges_sum
	from w_node_sum
	left join w_res_cell_var on (
		w_res_cell_var.estimation_cell = w_node_sum.estimation_cell
		and case when
			w_res_cell_var.aux_conf is null and w_node_sum.aux_conf is null then true
			else w_res_cell_var.aux_conf = w_node_sum.aux_conf end
		and case when
			w_res_cell_var.force_synthetic is null and w_node_sum.force_synthetic is null then true
			else w_res_cell_var.force_synthetic = w_node_sum.force_synthetic end
		and w_res_cell_var.t_variable__id = any(w_node_sum.edges_def)) and w_res_cell_var.denominator = w_node_sum.denominator
	group by w_node_sum.estimation_cell, w_node_sum.aux_conf, w_node_sum.force_synthetic, w_node_sum.node_estimate_confs, w_node_sum.denominator,
		w_node_sum.node, w_node_sum.node_sum, w_node_sum.edges_def
)
, w_diff as (
	select
		estimation_cell,
		aux_conf,
		force_synthetic,
		node_estimate_confs	as estimate_conf, denominator,
		node			as variable,
		node_sum		as point_est,
		edges_sum		as point_est_sum,
		edges_def		as variables_def,
		edges_found		as variables_found,
		edges_estimate_confs	as estimate_confs_found,
		case
			when node_sum != 0.0 and edges_sum = 0.0 then 100.0
			when node_sum = 0.0 and edges_sum = 0.0 then 0
			else abs(1 - (node_sum / edges_sum )) * 100.0
		end as diff
	from w_edge_sum
)
select * from w_diff order by diff desc
;

comment on view @extschema@.v_add_res_ratio_attr is
	'View showing ratio estimates attribute additivity (aggregated class estimate should be equal to sum of sub-classes estimates). Hierarchy between aggregated classes and its sub-classes is defined in v_variable_hierarchy.';

comment on column @extschema@.v_add_res_ratio_attr.estimation_cell 	is 'Estimate estimation cell. FKEY to c_estimation_cell.id.';
comment on column @extschema@.v_add_res_ratio_attr.aux_conf 		is 'Estimate auxiliary configuration. FKEY to t_aux_conf.id.';
comment on column @extschema@.v_add_res_ratio_attr.force_synthetic 	is 'Parameter showing whether estimate is forced to be synthetic.';
comment on column @extschema@.v_add_res_ratio_attr.estimate_conf	is 'Estimate configuration id. FKEY to t_estimate_conf.id.';
comment on column @extschema@.v_add_res_ratio_attr.variable		is 'Estimate attribute -- variable. FKEY to t_variable.id.';
comment on column @extschema@.v_add_res_ratio_attr.point_est		is 'Aggregated class point estimate.';
comment on column @extschema@.v_add_res_ratio_attr.point_est_sum	is 'Sum of sub-classes point estimates (belonging to aggregated class).';
comment on column @extschema@.v_add_res_ratio_attr.variables_def	is 'Attributes -- variables defined in hierarchy (v_variable_hierarchy). Array of FKEYs to t_variable.id.';
comment on column @extschema@.v_add_res_ratio_attr.variables_found	is 'Attributes -- variables found in data (t_result). Array of FKEYs to t_variable.id.';
comment on column @extschema@.v_add_res_ratio_attr.estimate_confs_found	is 'Estimate configurations found in data (t_result). Array of FKEYs to t_estimate_donf.id.';
comment on column @extschema@.v_add_res_ratio_attr.diff			is 'Relative difference between aggregated class estimate and sum of sub-classes estimates.';
comment on column @extschema@.v_add_res_ratio_attr.denominator		is 'Estimate configuration id of denominator. FKEY to t_total_estimate_conf.id.';

-- </view>

-- <view name="v_add_res_total_attr" schema="extschema" src="views/extschema/v_add_res_total_attr.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

create or replace view @extschema@.v_add_res_total_attr as
with w_res_cell_var as not materialized (
	select
		t_result.estimate_conf,
		t_result.point,
		t_total_estimate_conf.estimation_cell,
		t_total_estimate_conf.target_variable as t_variable__id,
		t_total_estimate_conf.aux_conf, force_synthetic
	from @extschema@.t_result
	inner join @extschema@.t_estimate_conf ON t_estimate_conf.id = t_result.estimate_conf
	inner join @extschema@.t_total_estimate_conf ON t_total_estimate_conf.id = t_estimate_conf.total_estimate_conf
	where t_estimate_conf.estimate_type = 1
	and t_result.is_latest
)
, w_node_sum as (
	select
		estimation_cell,
		w_res_cell_var.t_variable__id, aux_conf, force_synthetic,
		estimate_conf as node_estimate_confs,
		sum(point) as node_sum,
		hierarchy.node,
		hierarchy.edges as edges_def
	from w_res_cell_var
	inner join @extschema@.v_variable_hierarchy		as hierarchy on (hierarchy.node = w_res_cell_var.t_variable__id)
	group by estimation_cell, w_res_cell_var.t_variable__id, aux_conf, force_synthetic, estimate_conf, node, edges_def
	order by estimation_cell, w_res_cell_var.t_variable__id, aux_conf, force_synthetic, estimate_conf, node, edges_def
)
, w_edge_sum as (
	select
		w_node_sum.estimation_cell,
		w_node_sum.aux_conf,
		w_node_sum.force_synthetic,
		w_node_sum.node_estimate_confs,
		w_node_sum.node,
		w_node_sum.node_sum,
		w_node_sum.edges_def,
		array_agg(w_res_cell_var.t_variable__id order by w_res_cell_var.t_variable__id) as edges_found,
		array_agg(w_res_cell_var.estimate_conf order by w_res_cell_var.estimate_conf) as edges_estimate_confs,
		sum(w_res_cell_var.point) as edges_sum
	from w_node_sum
	left join w_res_cell_var on (
		w_res_cell_var.estimation_cell = w_node_sum.estimation_cell
		and case when
			w_res_cell_var.aux_conf is null and w_node_sum.aux_conf is null then true
			else w_res_cell_var.aux_conf = w_node_sum.aux_conf end
		and case when
			w_res_cell_var.force_synthetic is null and w_node_sum.force_synthetic is null then true
			else w_res_cell_var.force_synthetic = w_node_sum.force_synthetic end
		and w_res_cell_var.t_variable__id = any(w_node_sum.edges_def))
	group by w_node_sum.estimation_cell, w_node_sum.aux_conf, w_node_sum.force_synthetic, w_node_sum.node_estimate_confs,
		w_node_sum.node, w_node_sum.node_sum, w_node_sum.edges_def
)
, w_diff as (
	select
		estimation_cell,
		aux_conf,
		force_synthetic,
		node_estimate_confs	as estimate_conf,
		node			as variable,
		node_sum		as point_est,
		edges_sum		as point_est_sum,
		edges_def		as variables_def,
		edges_found		as variables_found,
		edges_estimate_confs	as estimate_confs_found,
		case
			when node_sum != 0.0 and edges_sum = 0.0 then 100.0
			when node_sum = 0.0 and edges_sum = 0.0 then 0
			else abs(1 - (node_sum / edges_sum )) * 100.0
		end as diff
	from w_edge_sum
)
select * from w_diff order by diff desc
;

comment on view @extschema@.v_add_res_total_attr is
	'View showing total estimates attribute additivity (aggregated class estimate should be equal to sum of sub-classes estimates). Hierarchy between aggregated classes and its sub-classes is defined in v_variable_hierarchy.';

comment on column @extschema@.v_add_res_total_attr.estimation_cell 	is 'Estimate estimation cell. FKEY to c_estimation_cell.id.';
comment on column @extschema@.v_add_res_total_attr.aux_conf 		is 'Estimate auxiliary configuration. FKEY to t_aux_conf.id.';
comment on column @extschema@.v_add_res_total_attr.force_synthetic 	is 'Parameter showing whether estimate is forced to be synthetic.';
comment on column @extschema@.v_add_res_total_attr.estimate_conf	is 'Estimate configuration id. FKEY to t_estimate_conf.id.';
comment on column @extschema@.v_add_res_total_attr.variable		is 'Estimate attribute -- variable. FKEY to t_variable.id.';
comment on column @extschema@.v_add_res_total_attr.point_est		is 'Aggregated class point estimate.';
comment on column @extschema@.v_add_res_total_attr.point_est_sum	is 'Sum of sub-classes point estimates (belonging to aggregated class).';
comment on column @extschema@.v_add_res_total_attr.variables_def	is 'Attributes -- variables defined in hierarchy (v_variable_hierarchy). Array of FKEYs to t_variable.id.';
comment on column @extschema@.v_add_res_total_attr.variables_found	is 'Attributes -- variables found in data (t_result). Array of FKEYs to t_variable.id.';
comment on column @extschema@.v_add_res_total_attr.estimate_confs_found	is 'Estimate configurations found in data (t_result). Array of FKEYs to t_estimate_donf.id.';
comment on column @extschema@.v_add_res_total_attr.diff			is 'Relative difference between aggregated class estimate and sum of sub-classes estimates.';

-- </view>

-- <view name="v_add_res_total_geo" schema="extschema" src="views/extschema/v_add_res_total_geo.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

create or replace view @extschema@.v_add_res_total_geo as
with w_res_cell_var as not materialized (
	select
		t_result.estimate_conf,
		t_result.point,
		t_total_estimate_conf.estimation_cell,
		t_total_estimate_conf.target_variable as t_variable__id,
		t_total_estimate_conf.aux_conf, force_synthetic
	from @extschema@.t_result
	inner join @extschema@.t_estimate_conf ON t_estimate_conf.id = t_result.estimate_conf
	inner join @extschema@.t_total_estimate_conf ON t_total_estimate_conf.id = t_estimate_conf.total_estimate_conf
	where t_estimate_conf.estimate_type = 1
	and t_result.is_latest
)
, w_node_sum as (
	select
		estimation_cell,
		w_res_cell_var.t_variable__id, aux_conf, force_synthetic,
		estimate_conf as node_estimate_confs,
		sum(point) as node_sum,
		hierarchy.node,
		hierarchy.edges as edges_def
	from w_res_cell_var
	inner join @extschema@.v_estimation_cell_hierarchy	as hierarchy on (hierarchy.node = w_res_cell_var.estimation_cell)
	group by estimation_cell, w_res_cell_var.t_variable__id, aux_conf, force_synthetic, estimate_conf, node, edges_def
	order by estimation_cell, w_res_cell_var.t_variable__id, aux_conf, force_synthetic, estimate_conf, node, edges_def
)
, w_edge_sum as (
	select
		w_node_sum.t_variable__id as variable,
		w_node_sum.aux_conf,
		w_node_sum.force_synthetic,
		w_node_sum.node_estimate_confs,
		w_node_sum.node,
		w_node_sum.node_sum,
		w_node_sum.edges_def,
		array_agg(w_res_cell_var.estimation_cell order by w_res_cell_var.estimation_cell) as edges_found,
		array_agg(w_res_cell_var.estimate_conf order by w_res_cell_var.estimate_conf) as edges_estimate_confs,
		sum(w_res_cell_var.point) as edges_sum
	from w_node_sum
	left join w_res_cell_var on (
		w_res_cell_var.t_variable__id = w_node_sum.t_variable__id
		and case when
			w_res_cell_var.aux_conf is null and w_node_sum.aux_conf is null then true
			else w_res_cell_var.aux_conf = w_node_sum.aux_conf end
		and case when
			w_res_cell_var.force_synthetic is null and w_node_sum.force_synthetic is null then true
			else w_res_cell_var.force_synthetic = w_node_sum.force_synthetic end
		and w_res_cell_var.estimation_cell = any(w_node_sum.edges_def))
	group by w_node_sum.t_variable__id, w_node_sum.aux_conf, w_node_sum.force_synthetic, w_node_sum.node_estimate_confs,
		w_node_sum.node, w_node_sum.node_sum, w_node_sum.edges_def
)
, w_diff as (
	select
		variable,
		aux_conf,
		force_synthetic,
		node_estimate_confs	as estimate_conf,
		node			as estimation_cell,
		node_sum		as point_est,
		edges_sum		as point_est_sum,
		edges_def		as estimation_cells_def,
		edges_found		as estimation_cells_found,
		edges_estimate_confs	as estimate_confs_found,
		case
			when node_sum != 0.0 and edges_sum = 0.0 then 100.0
			when node_sum = 0.0 and edges_sum = 0.0 then 0
			else abs(1 - (node_sum / edges_sum )) * 100.0
		end as diff
	from w_edge_sum
)
select * from w_diff order by diff desc
;

comment on view @extschema@.v_add_res_total_geo is
	'View showing total estimates geographic additivity (aggregated class estimate should be equal to sum of sub-classes estimates). Hierarchy between aggregated classes and its sub-classes is defined in v_estimation_cell_hierarchy.';

comment on column @extschema@.v_add_res_total_geo.variable			is 'Estimate attribute -- variable. FKEY to t_variable.id.';
comment on column @extschema@.v_add_res_total_geo.aux_conf 			is 'Estimate auxiliary configuration. FKEY to t_aux_conf.id.';
comment on column @extschema@.v_add_res_total_geo.force_synthetic 		is 'Parameter showing whether estimate is forced to be synthetic.';
comment on column @extschema@.v_add_res_total_geo.estimate_conf			is 'Estimate configuration id. FKEY to t_estimate_conf.id.';
comment on column @extschema@.v_add_res_total_geo.estimation_cell 		is 'Estimate estimation cell. FKEY to c_estimation_cell.id.';
comment on column @extschema@.v_add_res_total_geo.point_est			is 'Aggregated class point estimate.';
comment on column @extschema@.v_add_res_total_geo.point_est_sum			is 'Sum of sub-classes point estimates (belonging to aggregated class).';
comment on column @extschema@.v_add_res_total_geo.estimation_cells_def		is 'Estimation cells defined in hierarchy (v_estimation_cell_hierarchy). Array of FKEYs to c_estimation_cell.id.';
comment on column @extschema@.v_add_res_total_geo.estimation_cells_found	is 'Estimation cells found in data (t_result). Array of FKEYs to c_estimation_cell.id.';
comment on column @extschema@.v_add_res_total_geo.estimate_confs_found		is 'Estimate configurations found in data (t_result). Array of FKEYs to t_estimate_donf.id.';
comment on column @extschema@.v_add_res_total_geo.diff				is 'Relative difference between aggregated class estimate and sum of sub-classes estimates.';

-- </view>

-- <function name="fn_1p_est_configuration" schema="extschema" src="functions/extschema/configuration/fn_1p_est_configuration.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_1p_est_configuration(integer, regclass)
--DROP FUNCTION @extschema@.fn_1p_est_configuration(integer,date,date,character varying,integer,integer);
CREATE OR REPLACE FUNCTION @extschema@.fn_1p_est_configuration(_estimation_cell integer, _estimate_date_begin date, _estimate_date_end date, _note varchar, _target_variable integer, _panels integer[] DEFAULT NULL)
RETURNS integer
AS
$function$
DECLARE
_total_estimate_conf		integer;
_stratas			integer[];
_stratas_wp			integer[];
_panels_used			integer[];
_refyearsets			integer[];
_target_label			varchar;
_cell				varchar;
_change_variable		boolean;
BEGIN
_target_label := (
		SELECT		replace(
					replace(
						concat(coalesce(t2.label,'x'), ',', coalesce(t3.label,'x'), ',', coalesce(t4.label,'x'), ',', coalesce(t5.label,'x')),
					'x,',''),
			       	',x','') AS label
		FROM		@extschema@.t_variable AS t1
		LEFT JOIN 	@extschema@.c_target_variable AS t2 ON t1.target_variable = t2.id
		LEFT JOIN	@extschema@.c_sub_population_category AS t3 ON t1.sub_population_category = t3.id
		LEFT JOIN	@extschema@.c_area_domain_category AS t4 ON t1.area_domain_category = t4.id
		LEFT JOIN	@extschema@.c_auxiliary_variable_category AS t5 ON t1.auxiliary_variable_category = t5.id
		WHERE
			t1.id = $5
		);


_change_variable := (
		SELECT		CASE WHEN t3.id = 2 THEN true ELSE false END
		FROM 		@extschema@.t_variable AS t1
		LEFT JOIN 	@extschema@.c_target_variable AS t2 ON t1.target_variable = t2.id
		LEFT JOIN	@extschema@.c_state_or_change AS t3 ON t2.state_or_change = t3.id
		WHERE t1.id = $5
		);
	

_cell := (SELECT label FROM @extschema@.c_estimation_cell WHERE id = $1);

-- reset sequence (usually when previous atempt to configure fails, the sequence remains shifted)
PERFORM setval('@extschema@.t_total_estimate_conf_id_seq', (SELECT coalesce(max(id),0)+1 FROM @extschema@.t_total_estimate_conf), FALSE);

-- insert into table t_total_estimate_conf
INSERT INTO @extschema@.t_total_estimate_conf (estimation_cell, estimate_date_begin, estimate_date_end, total_estimate_conf, target_variable, phase_estimate_type, aux_conf)
VALUES
	($1, $2, $3, concat('1p;T=',_target_label,';Cell=',_cell,_note), $5, 1, NULL)
RETURNING id
INTO _total_estimate_conf;


-- panels order
IF _panels IS NOT NULL THEN _panels := (SELECT array_agg(panel ORDER BY panel) FROM unnest(_panels) AS t(panel));
END IF;

-- test on cell coverage

	-- which stratas covers the cell (fully?)
	SELECT
		array_agg(t1.id ORDER BY t1.id)
	FROM
		@extschema@.t_stratum AS t1
	INNER JOIN
		@extschema@.f_a_cell AS t2
	ON
		-- buffered stratum?
		-- no, if only buffer of the stratum would intersect the cell, 
		-- no consequences on estimate would be introduced, the buffer is in general used only for the edge effect compensation (the compensated area is within non-buffered stratum)
		ST_Intersects(t1.geom, t2.geom) AND NOT ST_Touches(t1.geom, t2.geom)
	WHERE
		t2.estimation_cell = $1
	INTO _stratas;

	IF _stratas IS NULL
	THEN
		RAISE EXCEPTION 'The specified cell is not intersected by any stratum. Choose another estimation cell.';
	END IF;

	-- panels with target variable in specified stratas (panels are the ones with the less granularity, hence 1 stratum can have e.g. 4 panels which together results in 1 big panel)

	WITH w_data AS MATERIALIZED (
		SELECT
			t1.id AS stratum, t2.id AS panel, t9.id AS reference_year_set, t2.plot_count AS total
		FROM
			@extschema@.t_stratum AS t1
		INNER JOIN
			@extschema@.t_panel AS t2
		ON
			t1.id = t2.stratum
		INNER JOIN
			@extschema@.cm_refyearset2panel_mapping AS t8
		ON
			t2.id = t8.panel --AND
			--t8.id = t9.reference_year_set
		INNER JOIN
			@extschema@.t_reference_year_set AS t9
		ON
			t8.reference_year_set = t9.id
		INNER JOIN
			@extschema@.t_available_datasets AS t6
		ON
			t2.id = t6.panel AND
			t9.id = t6.reference_year_set
		INNER JOIN
			@extschema@.t_variable AS t7
		ON
			t6.variable = t7.id
		WHERE
			array[t1.id] <@ _stratas AND
			t7.id = $5 AND 
			(t9.reference_date_begin >= $2 AND
			t9.reference_date_end <= $3)
		GROUP BY
			t1.id, t2.id, t9.id
	)
	SELECT
		array_agg(DISTINCT stratum ORDER BY stratum) AS stratas,
		array_agg(panel ORDER BY panel) AS panels,
		array_agg(reference_year_set ORDER BY panel) AS refyearsets
	FROM
		(SELECT
			stratum, panel, reference_year_set,
			total,
			max(total) OVER(PARTITION BY stratum, panel, reference_year_set) AS max_total
		FROM
			w_data
		) AS t1
	WHERE
		-- pick up the most dense panel with target variable
		CASE WHEN _panels IS NOT NULL THEN ARRAY[panel] <@ _panels ELSE
		total = max_total
		END
	INTO _stratas_wp, _panels_used, _refyearsets;

	-- if it is change variable, the panels resulted in previous query are those who have the target variable available,
	-- that, they have been measured at least twice, otherwise the change target variable would not be available
	-- but there must be also reference for the beginning of the reference period of sample panel
	-- in other words, the sample panel must be measured twice WITHIN the given period

	IF _change_variable = true
	THEN
		SELECT
			array_agg(DISTINCT stratum ORDER BY stratum) AS stratas,
			array_agg(t1.panel ORDER BY t1.panel) AS panels
		FROM
			@extschema@.t_panel AS t1
		INNER JOIN
			@extschema@.cm_refyearset2panel_mapping AS t2
		ON	t1.id = t2.panel
		INNER JOIN
			@extschema@.t_reference_year_set AS t3
		ON	t2.reference_year_set = t3.id
		WHERE
			t2.panel = ANY(_panels_used) AND	-- panels measured twice
			NOT t3.id = ANY(_refyearsets) AND	-- give away reference year sets already accounted
			t3.reference_date_begin >= $2 AND	-- condition for the given period
			t3.reference_date_end <= $3
		INTO _stratas_wp, _panels_used;			-- resulted list of panels used for calculation
	END IF;

	-- simple check if panels found are the same as panels required
	IF _panels IS NOT NULL AND (_panels != _panels_used OR _panels_used IS NULL)
	THEN
		RAISE EXCEPTION 'Required panels does not meet the computation criteria (measured target variable for given estimation period)!
Only these panels from specified array can be used: (%). Or You can try to not specify panels, the function will try to find the maximum of possible panels.', _panels_used;
	END IF;

	-- estimate cannot be computed (if panels are not specified)
	-- for some reason the target variable is not available for all stratas
	-- the estimation period is not compatible with the measured period of target variable
	-- or the target variable is not available itself
	IF _stratas != _stratas_wp OR _stratas_wp IS NULL
	THEN
		RAISE EXCEPTION 'Not all stratas are covered with the specified target variable for given estimation period!';
	END IF;

-- insert into table t_panel2total_2ndph_estimate_conf
INSERT INTO @extschema@.t_panel2total_2ndph_estimate_conf (total_estimate_conf, panel, reference_year_set)
SELECT
	_total_estimate_conf, panel, reference_year_set
FROM
	unnest(_panels_used) WITH ORDINALITY AS t1(panel, id)
INNER JOIN
	unnest(_refyearsets) WITH ORDINALITY AS t2(reference_year_set,id)
ON
	t1.id = t2.id;

-- insert into table t_estimate_conf
INSERT INTO @extschema@.t_estimate_conf (estimate_type, total_estimate_conf, denominator)
SELECT 1, _total_estimate_conf, NULL;

RETURN _total_estimate_conf;


END;
$function$
LANGUAGE plpgsql
PARALLEL SAFE;

--COMMENT ON FUNCTION @extschema@.fn_1p_est_configuration() IS '.';

-- </function>

-- <function name="fn_2p_est_configuration" schema="extschema" src="functions/extschema/configuration/fn_2p_est_configuration.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_2p_est_configuration(integer, regclass)
--DROP FUNCTION @extschema@.fn_2p_est_configuration(integer,date,date,character varying,integer,integer);
CREATE OR REPLACE FUNCTION @extschema@.fn_2p_est_configuration(_estimation_cell integer, _estimate_date_begin date, _estimate_date_end date, _note varchar, _target_variable integer, _aux_conf integer, _force_synthetic boolean default False)
RETURNS integer
AS
$function$
DECLARE
_total_estimate_conf		integer;
_stratas			integer[];
_stratas_wp			integer[];
_panels				integer[];
_refyearsets			integer[];
_panels_aux			integer[];
_param_area			integer;
_param_area_code		varchar;
_target_label			varchar;
_model				integer;
_cell				varchar;
BEGIN

-- test for existing g_betas
-- otherwise the configuration cannot be done (sometimes the g_betas cannot be computed)
-- so this prevents to configure non-computable estimates

IF (SELECT count(*) FROM @extschema@.t_g_beta WHERE aux_conf = $6) = 0
THEN
	RAISE EXCEPTION 'G-betas for required aux_conf are not available. The computation of it was not run or is not able to compute (mostly the problem of matrix inversion).';
END IF;

-- create the label of estimate
_target_label := (
		SELECT		replace(
					replace(
						concat(coalesce(t2.label,'x'), ',', coalesce(t3.label,'x'), ',', coalesce(t4.label,'x'), ',', coalesce(t5.label,'x')),
					'x,',''),
			       	',x','') AS label
		FROM		@extschema@.t_variable AS t1
		LEFT JOIN 	@extschema@.c_target_variable AS t2 ON t1.target_variable = t2.id
		LEFT JOIN	@extschema@.c_sub_population_category AS t3 ON t1.sub_population_category = t3.id
		LEFT JOIN	@extschema@.c_area_domain_category AS t4 ON t1.area_domain_category = t4.id
		LEFT JOIN	@extschema@.c_auxiliary_variable_category AS t5 ON t1.auxiliary_variable_category = t5.id
		WHERE
			t1.id = $5
		);

_param_area := (SELECT param_area FROM @extschema@.t_aux_conf WHERE id = $6);
_model := (SELECT model FROM @extschema@.t_aux_conf WHERE id = $6);
_param_area_code := (SELECT param_area_code FROM @extschema@.f_a_param_area WHERE gid = _param_area);
_cell := (SELECT label FROM @extschema@.c_estimation_cell WHERE id = $1);

-- reset sequence (usually when previous atempt to configure fails, the sequence remains shifted)
PERFORM setval('@extschema@.t_total_estimate_conf_id_seq', (SELECT coalesce(max(id),0)+1 FROM @extschema@.t_total_estimate_conf), FALSE);

-- insert into table t_total_estimate_conf
INSERT INTO @extschema@.t_total_estimate_conf (estimation_cell, estimate_date_begin, estimate_date_end, total_estimate_conf, target_variable, phase_estimate_type, force_synthetic, aux_conf)
VALUES
	($1, $2, $3, concat('2p;T=',_target_label,';C=',_cell,';PA=',_param_area_code, ';m=',_model,_note), $5, 2, $7, $6)
ON CONFLICT (estimation_cell, estimate_date_begin, estimate_date_end, target_variable, phase_estimate_type, force_synthetic, coalesce(aux_conf,0))
DO NOTHING
RETURNING id
INTO _total_estimate_conf;

IF _total_estimate_conf IS NOT NULL THEN

	-- test on param_area_coverage
		SELECT
			array_agg(t1.id ORDER BY t1.id)
		FROM
			@extschema@.t_stratum AS t1
		INNER JOIN
			@extschema@.f_a_param_area AS t2
		ON
			-- buffered stratum?
			-- no, if only buffer of the stratum would intersect the cell, 
			-- no consequences on estimate would be introduced, the buffer is in general used only for the edge effect compensation (the compensated area is within non-buffered stratum)
			ST_Intersects(t1.geom, t2.geom) AND NOT ST_Touches(t1.geom, t2.geom)
		WHERE
			t2.gid = _param_area
		INTO _stratas;

		IF _stratas IS NULL
		THEN
			RAISE EXCEPTION 'The specified cell is not intersected by any stratum. Choose another estimation cell.';
		END IF;

	-- existing panels configured in panel2aux_conf
		SELECT
			array_agg(panel ORDER BY panel)
		FROM
			@extschema@.t_panel2aux_conf AS t1
		WHERE
			t1.aux_conf = $6
		INTO _panels_aux;


	-- check of panel2total_2ndph
	-- and addition of panels from param_area - is the target variable available not only in cell?

		WITH w_data AS MATERIALIZED (
			SELECT
				t1.id AS stratum, t2.id AS panel, t9.id AS reference_year_set, t2.plot_count AS total
			FROM
				@extschema@.t_stratum AS t1
			INNER JOIN
				@extschema@.t_panel AS t2
			ON
				t1.id = t2.stratum
			INNER JOIN
				@extschema@.cm_refyearset2panel_mapping AS t8
			ON
				t2.id = t8.panel --AND
				--t8.id = t9.reference_year_set
			INNER JOIN
				@extschema@.t_reference_year_set AS t9
			ON
				t8.reference_year_set = t9.id
			INNER JOIN
				@extschema@.t_available_datasets AS t6
			ON
				t2.id = t6.panel AND
				t9.id = t6.reference_year_set
			INNER JOIN
				@extschema@.t_variable AS t7
			ON
				t6.variable = t7.id
			WHERE
				array[t1.id] <@ _stratas AND
				t7.id = $5 AND 
				(t9.reference_date_begin >= $2 AND
				t9.reference_date_end <= $3)
			GROUP BY
				t1.id, t2.id, t9.id
		)
		SELECT
			array_agg(DISTINCT stratum ORDER BY stratum) AS stratas,
			array_agg(panel ORDER BY panel) AS panels,
			array_agg(reference_year_set ORDER BY panel) AS refyearsets
		FROM
			(SELECT
				stratum, panel, reference_year_set,
				total,
				max(total) OVER(PARTITION BY stratum, panel, reference_year_set) AS max_total
			FROM
				w_data
			) AS t1
		WHERE
			-- pick up the most dense panel with target variable
			total = max_total
		INTO _stratas_wp, _panels, _refyearsets;

		IF _panels != _panels_aux OR _panels IS NULL
		THEN
			RAISE EXCEPTION 'Not all panels coming from g_beta have available target variable! total_estimate_conf: %, panels: %, panels_aux: %', _total_estimate_conf, _panels, _panels_aux;
		END IF;

	-- insert into table t_panel2total_2ndph_estimate_conf
	INSERT INTO @extschema@.t_panel2total_2ndph_estimate_conf (total_estimate_conf, panel, reference_year_set)
	SELECT
		_total_estimate_conf, panel, reference_year_set
	FROM
		unnest(_panels) WITH ORDINALITY AS t1(panel, id)
	INNER JOIN
		unnest(_refyearsets) WITH ORDINALITY AS t2(reference_year_set,id)
	ON
		t1.id = t2.id;

	-- insert into table t_estimate_conf
	INSERT INTO @extschema@.t_estimate_conf (estimate_type, total_estimate_conf, denominator)
	SELECT 1, _total_estimate_conf, NULL;

	IF	(
			SELECT
				array_agg(t_variable.id ORDER BY t_variable.id)
			FROM @extschema@.t_aux_total
			INNER JOIN @extschema@.t_variable 		ON (t_aux_total.variable = t_variable.id)
			INNER JOIN @extschema@.c_estimation_cell 	ON (t_aux_total.estimation_cell = c_estimation_cell.id)
			INNER JOIN @extschema@.t_model_variables 	ON t_model_variables.variable = t_variable.id
			INNER JOIN @extschema@.t_model 			ON t_model.id = t_model_variables.model
			INNER JOIN @extschema@.t_aux_conf 		ON t_aux_conf.model = t_model_variables.model
			WHERE 	c_estimation_cell.id = $1 AND
				t_aux_conf.id = $6 AND
				t_aux_total.is_latest

		)
		!= (
			SELECT
				array_agg(t_model_variables.variable order by variable)
			FROM @extschema@.t_aux_conf
			INNER JOIN @extschema@.t_model ON t_model.id = t_aux_conf.model
			INNER JOIN @extschema@.t_model_variables ON t_model_variables.model = t_model.id
			WHERE t_aux_conf.id = $6
		)
	THEN
		RAISE EXCEPTION 'fn_2p_est_configuration: t_aux_total not found! (total_estimate_conf: %)', _total_estimate_conf;
	END IF;

ELSE
	RAISE NOTICE 'Required configuration already exists!';
END IF;

RETURN _total_estimate_conf;


END;
$function$
LANGUAGE plpgsql
PARALLEL SAFE;

--COMMENT ON FUNCTION @extschema@.fn_2p_est_configuration() IS '.';

-- </function>
