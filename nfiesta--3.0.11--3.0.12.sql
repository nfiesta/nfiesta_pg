--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--


DROP FUNCTION IF EXISTS @extschema@.fn_etl_check_variables(json) CASCADE;


-- <function name="fn_etl_check_variables" schema="extschema" src="functions/extschema/etl/fn_etl_check_variables.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_check_variables(json, character varying)

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_check_variables(json, character varying) CASCADE;

create or replace function @extschema@.fn_etl_check_variables
(
	_variables	json,
	_national_language	character varying(2) default 'en'::character varying(2)
)
returns json
as
$$
declare
		_country									varchar;
		_target_variable							integer;
		_count_comb_panels_and_reference_year_sets	integer;
		_refyearset2panel_mapping					integer[];
		_res										json;
begin
		if _national_language is null
		then
			raise exception 'Error 01: fn_etl_check_variables: Input argument _national_language must not by NULL!';
		end if;

		if _variables is null
		then
			raise exception 'Error 02: fn_etl_check_variables: Input argument _variables must not by NULL!';
		end if;

		_country := (_variables->>'country')::varchar;
		_target_variable := (_variables->>'target_variable')::integer;

		-- check country --
		if _country is null
		then
			raise exception 'Error 02: fn_etl_check_variables: Internal argument _country must not by NULL!';
		end if;

		if	(select count(id) is distinct from 1 from sdesign.c_country where label = _country)
		then
			raise exception 'Error 03: fn_etl_check_variables: Input JSON element "country = %" is not present in table sdesign.c_country!',_country;
		end if;

		------------
		-- raise notice '_country: %',_country;
		------------

		-- check target_variable
		if _target_variable is null
		then
			raise exception 'Error 04: fn_etl_check_variables: Internal argument _target_variable must not by NULL!';
		end if;

		if	(select count(id) is distinct from 1 from @extschema@.c_target_variable where id = _target_variable)
		then
			raise exception 'Error 05: fn_etl_check_variables: Input JSON element "target_variable = %" is not present in table c_target_variable!',_target_variable;
		end if;

		------------
		-- raise notice '_target_variable: %',_target_variable;
		------------			

		-- check 1
		with
		w1 as	(
				select json_array_elements(_variables->'panels_and_reference_year_sets') as s
				)
		,w2 as	(
				select
						(s->>'panel')::varchar				as panel,
						(s->>'reference_year_set')::varchar as reference_year_set
				from w1
				)
		select count(*) from w2
		into _count_comb_panels_and_reference_year_sets;

		if _count_comb_panels_and_reference_year_sets is null
		then
			raise exception 'Error 06: fn_etl_check_variables: Internal argument _count_comb_panels_and_reference_year_sets must not by NULL!';
		end if;

		------------
		-- raise notice '_count_comb_panels_and_reference_year_sets: %',_count_comb_panels_and_reference_year_sets;
		------------	

		-- check 2
		with
		w1 as	(
				select json_array_elements(_variables->'panels_and_reference_year_sets') as s
				)
		,w2 as	(
				select
						(s->>'panel')::varchar				as panel,
						(s->>'reference_year_set')::varchar	as reference_year_set
				from w1
				)
		,w3 as	(
				select
					t1.id as panel_id,
					t1.panel,
					t3.id as reference_year_set_id,
					t3.reference_year_set,
					t2.id as refyearset2panel_mapping
				from
					(
					select id, panel from sdesign.t_panel where stratum in
					(select id from sdesign.t_stratum where strata_set in
					(select id from sdesign.t_strata_set where country = 
					(select id from sdesign.c_country where label = _country)))
					) as t1
				inner join sdesign.cm_refyearset2panel_mapping as t2 on t1.id = t2.panel
				inner join sdesign.t_reference_year_set as t3 on t2.reference_year_set = t3.id
				)
		,w4 as	(
				select w3.refyearset2panel_mapping from w2 inner join w3
				on w2.panel = w3.panel and w2.reference_year_set = w3.reference_year_set
				)
		select array_agg(w4.refyearset2panel_mapping order by w4.refyearset2panel_mapping)
		from w4
		into _refyearset2panel_mapping;

		------------
		-- raise notice '_refyearset2panel_mapping: %',_refyearset2panel_mapping;
		------------			

		if _refyearset2panel_mapping is null
		then
			raise exception 'Error 07: fn_etl_check_variables: Internal argument _refyearset2panel_mapping must not by NULL!';
		end if;	

		if _count_comb_panels_and_reference_year_sets != array_length(_refyearset2panel_mapping,1)
		then
			raise exception 'Error 08: fn_etl_check_variables: Some input combination of panel and reference year set is not present in sampling design!';
		end if;			

		with
		w1 as	(
				select
						a.*
				from
							(select * from @extschema@.t_available_datasets) as a
				inner join	(		
							select * from sdesign.cm_refyearset2panel_mapping
							where id in (select unnest(_refyearset2panel_mapping))
							) as b
									on a.panel = b.panel and a.reference_year_set = b.reference_year_set
				)
		,w2 as	(
				select distinct variable from w1 where variable in
					(select id from @extschema@.t_variable where target_variable = _target_variable)
				order by variable
				)
		,w3 as	(
				select
					t1.target_variable,
					t1.sub_population_category,
					t1.area_domain_category,
					coalesce(t2.label,'null'::varchar) as spc_label,
					coalesce(t3.label,'null'::varchar) as adc_label,
					coalesce(t4.label,'null'::varchar) as spt_label,
					coalesce(t5.label,'null'::varchar) as adt_label,
					coalesce(t2.label_en,'null'::varchar) as spc_label_en,
					coalesce(t3.label_en,'null'::varchar) as adc_label_en,
					coalesce(t4.label_en,'null'::varchar) as spt_label_en,
					coalesce(t5.label_en,'null'::varchar) as adt_label_en,					
					t2.id as spc_id,
					t3.id as adc_id,
					t4.id as spt_id,
					t5.id as adt_id
		 		from
					(select * from @extschema@.t_variable where id in (select variable from w2)) as t1
					left join @extschema@.c_sub_population_category as t2 on t1.sub_population_category = t2.id
					left join @extschema@.c_area_domain_category as t3 on t1.area_domain_category = t3.id
					left join @extschema@.c_sub_population as t4 on t2.sub_population = t4.id
					left join @extschema@.c_area_domain as t5 on t3.area_domain = t5.id
				order by t1.id 
				)
		,w4 as	(
				select distinct spt_label, spc_label, adt_label, adc_label, spt_label_en, spc_label_en, adt_label_en, adc_label_en, spc_id, adc_id, spt_id, adt_id from w3
				)
		,w5 as	(-- list of attribute variables for target variable [target DB]
				select
						row_number() over() as new_id,
						spt_label,
						spc_label,
						adt_label,
						adc_label,
						spt_label_en,
						spc_label_en,
						adt_label_en,
						adc_label_en,
						string_to_array(spt_label_en,';') as spt_label_en_array,
						string_to_array(spc_label_en,';') as spc_label_en_array,
						string_to_array(adt_label_en,';') as adt_label_en_array,
						string_to_array(adc_label_en,';') as adc_label_en_array,
						case when spt_id is null then 0 else spt_id end as spt_id,
						case when spc_id is null then 0 else spc_id end as spc_id,
						case when adt_id is null then 0 else adt_id end as adt_id,
						case when adc_id is null then 0 else adc_id end as adc_id
				from w4
				)
		,w5a as	(
				select w5.* from w5 where w5.spt_id = 0 and w5.spc_id = 0 and w5.adt_id = 0 and w5.adc_id = 0	-- row without distinction
				)
		,w5b as	(
				select w5.* from w5 where w5.new_id != (select w5a.new_id from w5a) -- attribute variables without row without distinction
				)
		------------------------------------------------------------------
		,w6 as	(-- list of attribute variables [source DB]
				select json_array_elements(_variables->'variables') as s
				)
		,w7 as	(
				select
						string_to_array((s->>'sub_population')::varchar,';')			as vsp,			-- EN
						string_to_array((s->>'sub_population_category')::varchar,';')	as vspc,		-- EN
						string_to_array((s->>'area_domain')::varchar,';')				as vad,			-- EN
						string_to_array((s->>'area_domain_category')::varchar,';')		as vadc,		-- EN
						(s->>'sub_population_etl_id')::integer							as vsp_id,
						(s->>'sub_population_category_etl_id')::integer					as vspc_id,
						(s->>'area_domain_etl_id')::integer								as vad_id,
						(s->>'area_domain_category_etl_id')::integer					as vadc_id
				from w6
				)
		,w8 as	(-- list of attribute variables that were ETLed
				select
						w5b.*,
						w7.*

				from
						w5b inner join w7
				on
						(
						w5b.spt_id = w7.vsp_id	and
						w5b.spc_id = w7.vspc_id and
						w5b.adt_id = w7.vad_id	and
						w5b.adc_id = w7.vadc_id
						)
				)
		,w9 as	(
				select w5b.* from w5b where w5b.new_id not in (select w8.new_id from w8)
				)				
		,w10 as	(
				select
						w9.*,
						w7.*,
						case when (w7.vsp is null or w7.vspc is null or w7.vad is null or w7.vadc is null) then true
						else false
						end as is_missing
				from
						w9 left join w7
				on
					(
					@extschema@.fn_etl_array_compare(w9.spt_label_en_array,w7.vsp)  and
					@extschema@.fn_etl_array_compare(w9.spc_label_en_array,w7.vspc) and
					@extschema@.fn_etl_array_compare(w9.adt_label_en_array,w7.vad)  and
					@extschema@.fn_etl_array_compare(w9.adc_label_en_array,w7.vadc)
					)
				)
		select
				case
				when _national_language = 'en'
				then
					json_build_object
									(
									'en',json_agg(json_build_object
											(
											'sub_population', w10.spt_label_en,
											'sub_population_category', w10.spc_label_en,
											'area_domain', w10.adt_label_en,
											'area_domain_category',	w10.adc_label_en
											))
									)

				else
					json_build_object
									(
									_national_language,json_agg(json_build_object
																(
																'sub_population', w10.spt_label,
																'sub_population_category', w10.spc_label,
																'area_domain', w10.adt_label,
																'area_domain_category',	w10.adc_label
																))
									,
									'en',json_agg(json_build_object
											(
											'sub_population', w10.spt_label_en,
											'sub_population_category', w10.spc_label_en,
											'area_domain', w10.adt_label_en,
											'area_domain_category',	w10.adc_label_en
											))
									)
				end				
		from
				w10 where is_missing = true
		into
				_res;

		if _national_language = 'en'::varchar
		then
				if ((_res)->>'en') is null
				then
					_res := null::json;
				else
					_res := _res;
				end if;
		else
				if (((_res)->>'cs') is null or ((_res)->>'en') is null)
				then
					_res := null::json;
				else
					_res := _res;
				end if;
		end if;
		
		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_check_variables(json, character varying) is
'Function checks that input variables are contained in target database.';

grant execute on function @extschema@.fn_etl_check_variables(json, character varying) to public;
-- </function>
