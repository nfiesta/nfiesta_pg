--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
-- DATA DISCLAIMER
-- Any results produced on the basis of openly published Czech National Forest Inventory (CZNFI) sample data
-- do not reflect the true status or changes within any geographical area of the Czech Republic,
-- at, during or between any time occasion(s).
-- In particular, any such results must not be presented or interpreted as an alternative to any information published by CZNFI,
-- be it a past or future CZNFI publication.
--
\set srcdir `echo $SRC_DIR`
--
-- Data for Name: c_topic; Type: TABLE DATA; Schema: nfiesta; Owner: vagrant
--
COPY nfiesta.c_topic  FROM stdin;
--
-- Data for Name: c_target_variable; Type: TABLE DATA; Schema: nfiesta; Owner: vagrant
--
INSERT INTO nfiesta.c_target_variable(metadata)
select '{"cs":
	{"indicator": {"label": "les", "description": "Plocha lesa."},
	"state or change": {"label": "state variable", "description": "State variable."},
	"unit": {"label": "ha", "description": "Hektar."},
	"local densities": [
		{"object type label": "core",
		"object type description": "Contribution to local density which will be aggregated and is the input into the estimate of total.",
		"object label": "střed inventarizační plochy",
		"object description": "střed inventarizační plochy",
		"label": "plocha lesa", "description": "Plocha lesa.",
		"version": {"label": "1. verze", "description": "1. verze."},
		"definition variant": {"label": null, "description": null},
		"area domain": {"label": ["celá plocha geografické domény"], "description": ["Celá plocha geografické domény (celá zájmová oblast)."]},
		"population": {"label": null, "description": null}, "use_negative": false}]
	},
"en":
	{"indicator": {"label": "forest", "description": "Area of forests."},
	"state or change": {"label": "state variable", "description": "State variable."},
	"unit": {"label": "ha", "description": "Hectare."},
	"local densities": [
		{"object type label": "core",
		"object type description": "Contribution to local density which will be aggregated and is the input into the estimate of total.",
		"object label": "the center of the inventory plot",
		"object description": "the center of the inventory plot",
		"label": "area of forests", "description": "The area of forests.",
		"version": {"label": "version 1", "description": "Version 1."},
		"definition variant": {"label": null, "description": null},
		"area domain": {"label": ["entire area of geographical domain"], "description": ["The entire area of the geographical domain (the whole area of interest)."]},
		"population": {"label": null, "description": null}, "use_negative": false}]
	}
}'::json
union all
select '{"cs":
	{"indicator": {"label": "plocha", "description": "Rozloha celé geografické domény (např. rozloha kategorií pozemků)."},
	"state or change": {"label": "state variable", "description": "State variable."},
	"unit": {"label": "ha", "description": "Hektar."},
	"local densities": [
		{"object type label": "core",
		"object type description": "Contribution to local density which will be aggregated and is the input into the estimate of total.",
		"object label": "střed inventarizační plochy",
		"object description": "střed inventarizační plochy",
		"label": "rozloha území", "description": "Rozloha území.",
		"version": {"label": "1. verze", "description": "1. verze."},
		"definition variant": {"label": null, "description": null},
		"area domain": {"label": null, "description": null},
		"population": {"label": null, "description": null}, "use_negative": false}]
	},
"en":
	{"indicator": {"label": "area", "description": "Area of geographical domain (e.g. extent of land categories)."},
	"state or change": {"label": "state variable", "description": "State variable."},
	"unit": {"label": "ha", "description": "Hectare."},
	"local densities": [
		{"object type label": "core",
		"object type description": "Contribution to local density which will be aggregated and is the input into the estimate of total.",
		"object label": "the center of the inventory plot",
		"object description": "the center of the inventory plot",
		"label": "whole domain area", "description": "Whole domain area extent.",
		"version": {"label": "version 1", "description": "Version 1."},
		"definition variant": {"label": null, "description": null},
		"area domain": {"label": null, "description": null},
		"population": {"label": null, "description": null}, "use_negative": false}]
	}
}'::json
union all
select '{"cs":
	{"indicator": {"label": "nb", "description": "Hmotnost nadzemní biomasy dřevin."},
	"state or change": {"label": "state variable", "description": "State variable."},
	"unit": {"label": "t", "description": "Tuna."},
	"local densities": [
		{"object type label": "core",
		"object type description": "Contribution to local density which will be aggregated and is the input into the estimate of total.",
		"object label": "jedinci hroubí",
		"object description": "Kmeny jedinců hroubí.",
		"label": "nadzemní biomasa jedinců hroubí", "description": "Hmotnost nadzemní biomasy dřevin jedinců hroubí.",
		"version": {"label": "1. verze", "description": "1. verze."},
		"definition variant": {"label": null, "description": null},
		"area domain": {"label": ["kategorie Les","přístupný a schůdný"], "description": ["Kategorie pozemku FAO FRA - Les.", "Přístupný a schůdný střed."]},
		"population": {"label": ["živí jedinci hroubí"], "description": ["Živí jedinci hroubí včetně živých vývratů."]}, "use_negative": false}]
	},
"en":
	{"indicator": {"label": "agb", "description": "Above ground biomass."},
	"state or change": {"label": "state variable", "description": "State variable."},
	"unit": {"label": "t", "description": "Tons."},
	"local densities": [
		{"object type label": "core",
		"object type description": "Contribution to local density which will be aggregated and is the input into the estimate of total.",
		"object label": "merchantable wood stems",
		"object description": "Merchantable wood stems.",
		"label": "above ground biomass of merch. wood stems", "description": "Above ground biomass of merchantable wood stems.",
		"version": {"label": "version 1", "description": "Version 1."},
		"definition variant": {"label": null, "description": null},
		"area domain": {"label": ["forest category","accessible"], "description": ["FAO FRA forest category","Accessible plot center."]},
		"population": {"label": ["living stems"], "description": ["Living merchantable wood stems including living wind falls."]}, "use_negative": false}]
	}
}'::json
union all
select '{"cs":
	{"indicator": {"label": "zasoba", "description": "Zásoba hroubí."},
	"state or change": {"label": "state variable", "description": "State variable."},
	"unit": {"label": "m3 b.k.", "description": "Metry krychlové bez kůry."},
	"local densities": [
		{"object type label": "core",
		"object type description": "Contribution to local density which will be aggregated and is the input into the estimate of total.",
		"object label": "jedinci hroubí",
		"object description": "Kmeny jedinců hroubí.",
		"label": "objem ÚLT b.k.", "description": "Objem kmene hroubí podle tabulek ÚLT. Bez kůry.",
		"version": {"label": "1. verze", "description": "1. verze."},
		"definition variant": {"label": null, "description": null},
		"area domain": {"label": ["kategorie Les","přístupný a schůdný"], "description": ["Kategorie pozemku FAO FRA - Les.", "Přístupný a schůdný střed."]},
		"population": {"label": ["živí jedinci hroubí"], "description": ["Živí jedinci hroubí včetně živých vývratů."]}, "use_negative": false}]
	},
"en":
	{"indicator": {"label": "vol", "description": "Growing stock of merchantable wood stems."},
	"state or change": {"label": "state variable", "description": "State variable."},
	"unit": {"label": "m3 u.b.", "description": "Cubic meters under bark."},
	"local densities": [
		{"object type label": "core",
		"object type description": "Contribution to local density which will be aggregated and is the input into the estimate of total.",
		"object label": "merchantable wood stems",
		"object description": "Merchantable wood stems.",
		"label": "stem volume ULT u.b.", "description": "Stem volume ULT under bark.",
		"version": {"label": "version 1", "description": "Version 1."},
		"definition variant": {"label": null, "description": null},
		"area domain": {"label": ["forest category","accessible"], "description": ["FAO FRA forest category","Accessible plot center."]},
		"population": {"label": ["living stems"], "description": ["Living merchantable wood stems including living wind falls."]}, "use_negative": false}]
	}
}'::json
;
COPY nfiesta.cm_tvariable2topic  FROM stdin;
--
-- Data for Name: c_sub_population; Type: TABLE DATA; Schema: nfiesta; Owner: vagrant
--
COPY nfiesta.c_sub_population  FROM stdin;
--
-- Data for Name: c_sub_population_category; Type: TABLE DATA; Schema: nfiesta; Owner: vagrant
--
COPY nfiesta.c_sub_population_category  FROM stdin;
--
-- Data for Name: c_area_domain; Type: TABLE DATA; Schema: nfiesta; Owner: vagrant
--
COPY nfiesta.c_area_domain  FROM stdin;
--
-- Data for Name: c_area_domain_category; Type: TABLE DATA; Schema: nfiesta; Owner: vagrant
--
COPY nfiesta.c_area_domain_category  FROM stdin;
--
-- Data for Name: c_auxiliary_variable; Type: TABLE DATA; Schema: nfiesta; Owner: vagrant
--
COPY nfiesta.c_auxiliary_variable  FROM stdin;
--
-- Data for Name: c_auxiliary_variable_category; Type: TABLE DATA; Schema: nfiesta; Owner: vagrant
--
COPY nfiesta.c_auxiliary_variable_category  FROM stdin;
--
-- Data for Name: c_estimation_cell_collection; Type: TABLE DATA; Schema: nfiesta; Owner: vagrant
--
COPY nfiesta.c_estimation_cell_collection  FROM stdin;
--
-- Data for Name: c_estimation_cell; Type: TABLE DATA; Schema: nfiesta; Owner: vagrant
--
COPY nfiesta.c_estimation_cell  FROM stdin;
--
-- Data for Name: cm_plot2cell_mapping; Type: TABLE DATA; Schema: nfiesta; Owner: vagrant
--
\set afile :srcdir '/sql/csv/plot_cell_associations.csv'
CREATE FOREIGN TABLE csv.plot_cell_associations (
	country				character varying(20)		not null,
	strata_set			character varying(20)		not null,
	stratum				character varying(20)		not null,
	panel				character varying(20)		not null,
	cluster				character varying(20)		not null,
	plot				character varying(20)		not null,
	cell_collection			character varying(20)		not null,
	cell				character varying(20)		not null,
	comment				text
) SERVER csv_files
OPTIONS ( header 'true', format 'csv', delimiter ';', filename :'afile' );
--CREATE UNIQUE INDEX cm_plot2cell_mapping_plot_estimation_cell_idx ON nfiesta.cm_plot2cell_mapping (plot, estimation_cell);
--ALTER TABLE nfiesta.cm_plot2cell_mapping ADD CONSTRAINT cm_plot2cell_mapping__unique_estimation_cell UNIQUE USING INDEX cm_plot2cell_mapping_plot_estimation_cell_idx;
with w_data as (
	select
		f_p_plot.gid,
		c_estimation_cell.id
	from sdesign.c_country
	join sdesign.t_strata_set 			on t_strata_set.country = c_country.id
	join sdesign.t_stratum 				on t_stratum.strata_set = t_strata_set.id
	join sdesign.t_panel				on t_panel.stratum = t_stratum.id
	join sdesign.cm_cluster2panel_mapping 		on cm_cluster2panel_mapping.panel = t_panel.id
	join sdesign.t_cluster 				on t_cluster.id = cm_cluster2panel_mapping.cluster
	join sdesign.f_p_plot 				on f_p_plot.cluster = t_cluster.id
	join sdesign.t_cluster_configuration 		on t_cluster_configuration.id = t_panel.cluster_configuration
	join sdesign.cm_plot2cluster_config_mapping 	on (
		cm_plot2cluster_config_mapping.cluster_configuration = t_cluster_configuration.id and
		cm_plot2cluster_config_mapping.plot = f_p_plot.gid)
	----------------------------------------
	join csv.plot_cell_associations as t		on (
		c_country.label = t.country and
		t_strata_set.strata_set = t.strata_set and
		t_stratum.stratum = t.stratum and
		t_panel.panel = t.panel and
		t_cluster.cluster = t.cluster and
		f_p_plot.plot = t.plot)
	----------------------------------------
	join nfiesta.c_estimation_cell		on (c_estimation_cell.label = t.cell)
	join nfiesta.c_estimation_cell_collection 	on (
		c_estimation_cell.estimation_cell_collection = c_estimation_cell_collection.id and
		c_estimation_cell_collection.label = t.cell_collection)
)
insert into nfiesta.cm_plot2cell_mapping(plot, estimation_cell)
select
	gid, id
from w_data
order by gid, id
;
--
-- Data for Name: t_variable; Type: TABLE DATA; Schema: nfiesta; Owner: vagrant
--
COPY nfiesta.t_variable  FROM stdin;
--
-- Data for Name: t_variable_hierarchy; Type: TABLE DATA; Schema: nfiesta; Owner: vagrant
--
\set afile :srcdir '/sql/csv/variable_hierarchy.csv'
CREATE FOREIGN TABLE csv.variable_hierarchy (
	target_variable			integer		not null,
	area_domain_category_sup	integer,
	sub_population_category_sup	integer,
	area_domain_category		integer,
	sub_population_category		integer
) SERVER csv_files
OPTIONS ( header 'true', format 'csv', delimiter ';', filename :'afile' );
with w_data as (
	select
		(select id from nfiesta.t_variable
			where t_variable.target_variable                = variable_hierarchy.target_variable and
			coalesce(t_variable.area_domain_category,0)     = coalesce(variable_hierarchy.area_domain_category,0) AND
			coalesce(t_variable.sub_population_category,0)  = coalesce(variable_hierarchy.sub_population_category,0)
		) as variable,
		(select id from nfiesta.t_variable
			where t_variable.target_variable                = variable_hierarchy.target_variable and
			coalesce(t_variable.area_domain_category,0)     = coalesce(variable_hierarchy.area_domain_category_sup,0) AND
			coalesce(t_variable.sub_population_category,0)  = coalesce(variable_hierarchy.sub_population_category_sup,0)
		) as variable_superior
	from csv.variable_hierarchy
)
INSERT INTO nfiesta.t_variable_hierarchy (variable, variable_superior)
select variable, variable_superior from w_data
where variable is not null and variable_superior is not null
;
--
-- Data for Name: t_available_datasets; Type: TABLE DATA; Schema: nfiesta; Owner: vagrant
--
\set afile :srcdir '/sql/csv/available_datasets.csv'
CREATE FOREIGN TABLE csv.available_datasets (
	country				character varying(20)		not null,
	strata_set			character varying(20)		not null,
	stratum				character varying(20)		not null,
	panel				character varying(20)		not null,
	reference_year_set		character varying(20),
	target_variable			integer,
	sub_population			integer,
	sub_population_category		integer,
	area_domain			integer,
	area_domain_category		integer,
	auxiliary_variable		integer,
	auxiliary_variable_category	integer,
	comment				text
) SERVER csv_files
OPTIONS ( header 'true', format 'csv', delimiter ';', filename :'afile' );
with w_data as (
	select
		t_panel.id as panel,
		t_reference_year_set.id as reference_year_set,
		t_variable.id as variable
	from sdesign.c_country
	join sdesign.t_strata_set 			on t_strata_set.country = c_country.id
	join sdesign.t_stratum 				on t_stratum.strata_set = t_strata_set.id
	join sdesign.t_panel				on t_panel.stratum = t_stratum.id
	join sdesign.cm_refyearset2panel_mapping	on cm_refyearset2panel_mapping.panel = t_panel.id
	join sdesign.t_reference_year_set		on cm_refyearset2panel_mapping.reference_year_set = t_reference_year_set.id
	----------------------------------------
	join csv.available_datasets as t		on (
		c_country.label = t.country and
		t_strata_set.strata_set = t.strata_set and
		t_stratum.stratum = t.stratum and
		t_panel.panel = t.panel and
		t_reference_year_set.reference_year_set = t.reference_year_set
	)
	----------------------------------------
	join nfiesta.t_variable 				on (
			t_variable.target_variable = t.target_variable and
			coalesce(t_variable.sub_population_category, 0) = coalesce(t.sub_population_category, 0) and
			coalesce(t_variable.area_domain_category, 0) = coalesce(t.area_domain_category, 0)
		)
)
, w_data_aux as (
	select
		t_panel.id as panel,
		t_variable.id as variable
	from sdesign.c_country
	join sdesign.t_strata_set 			on t_strata_set.country = c_country.id
	join sdesign.t_stratum 				on t_stratum.strata_set = t_strata_set.id
	join sdesign.t_panel				on t_panel.stratum = t_stratum.id
	----------------------------------------
	join csv.available_datasets as t		on (
		c_country.label = t.country and
		t_strata_set.strata_set = t.strata_set and
		t_stratum.stratum = t.stratum and
		t_panel.panel = t.panel
	)
	----------------------------------------
	join nfiesta.t_variable 				on (
			t_variable.auxiliary_variable_category = t.auxiliary_variable_category
		)
)
insert into nfiesta.t_available_datasets(panel, reference_year_set, variable, ldsity_threshold, last_change)
select
	panel, reference_year_set, variable, 0.0000000001 as ldsity_threshold, now() as last_change
from w_data
union all
select
	panel, null as reference_year_set, variable, 0.0000000001 as ldsity_threshold, now() as last_change
from w_data_aux
order by panel, reference_year_set, variable, ldsity_threshold, last_change
;
--
-- Data for Name: t_auxiliary_data; Type: TABLE DATA; Schema: nfiesta; Owner: vagrant
--
\set afile :srcdir '/sql/csv/plot_auxiliary_data_before.csv'
CREATE FOREIGN TABLE csv.plot_auxiliary_data (
	country				character varying(20)		not null,
	strata_set			character varying(20)		not null,
	stratum				character varying(20)		not null,
	panel				character varying(20)		not null,
	cluster				character varying(20)		not null,
	plot				character varying(20)		not null,
	tile				character varying(20),
	auxiliary_variable		character varying(20)		not null,
	auxiliary_variable_category	character varying(20)		not null,
	value				double precision		not null,
	comment				text
) SERVER csv_files
OPTIONS ( header 'true', format 'csv', delimiter ';', filename :'afile' );
with w_data as (
	select
		f_p_plot.gid	as plot,
		t_panel.id 	as panel,
		t_variable.id 	as variable,
		t.value
	from sdesign.c_country
	join sdesign.t_strata_set 			on t_strata_set.country = c_country.id
	join sdesign.t_stratum 				on t_stratum.strata_set = t_strata_set.id
	join sdesign.t_panel				on t_panel.stratum = t_stratum.id
	join sdesign.cm_cluster2panel_mapping 		on cm_cluster2panel_mapping.panel = t_panel.id
	join sdesign.t_cluster 				on t_cluster.id = cm_cluster2panel_mapping.cluster
	join sdesign.f_p_plot 				on f_p_plot.cluster = t_cluster.id
	join sdesign.t_cluster_configuration 		on t_cluster_configuration.id = t_panel.cluster_configuration
	join sdesign.cm_plot2cluster_config_mapping 	on (
		cm_plot2cluster_config_mapping.cluster_configuration = t_cluster_configuration.id and
		cm_plot2cluster_config_mapping.plot = f_p_plot.gid)
	----------------------------------------
	join csv.plot_auxiliary_data as t		on (
		c_country.label = t.country and
		t_strata_set.strata_set = t.strata_set and
		t_stratum.stratum = t.stratum and
		t_panel.panel = t.panel and
		t_cluster.cluster = t.cluster and
		f_p_plot.plot = t.plot)
	----------------------------------------
	join nfiesta.c_auxiliary_variable		on (c_auxiliary_variable.label = t.auxiliary_variable)
	join nfiesta.c_auxiliary_variable_category	on (
		c_auxiliary_variable_category.auxiliary_variable = c_auxiliary_variable.id and
		c_auxiliary_variable_category.label = t.auxiliary_variable_category)
	join nfiesta.t_variable on (t_variable.auxiliary_variable_category = c_auxiliary_variable_category.id)
)
insert into nfiesta.t_auxiliary_data(plot, available_datasets, value)
select
	plot,
	(	select
			id as available_datasets
		from nfiesta.t_available_datasets
		where t_available_datasets.panel = w_data.panel
		and t_available_datasets.variable = w_data.variable
	),
	value
from w_data
where value != 0;
--
-- Data for Name: t_target_data; Type: TABLE DATA; Schema: nfiesta; Owner: vagrant
--
\set afile :srcdir '/sql/csv/plot_target_data_before.csv'
CREATE FOREIGN TABLE csv.plot_target_data (
	country				character varying(20)		not null,
	inventory_campaign		character varying(20)		not null,
	reference_year_set		character varying(20)		not null,
	strata_set			character varying(20)		not null,
	stratum				character varying(20)		not null,
	panel				character varying(20)		not null,
	cluster				character varying(20)		not null,
	plot				character varying(20)		not null,
	target_variable			character varying(20)		not null,
	sub_population			character varying(20)		not null,
	sub_population_category		character varying(20)		not null,
	area_domain			character varying(20)		not null,
	area_domain_category		character varying(20)		not null,
	value				double precision		not null,
	comment				text
) SERVER csv_files
OPTIONS ( header 'true', format 'csv', delimiter ';', filename :'afile' );
with w_data as (
	select
		f_p_plot.gid		as plot,
		t_panel.id 		as panel,
		t_variable.id 		as variable,
		t_reference_year_set.id	as reference_year_set,
		t.value
	from sdesign.c_country
	join sdesign.t_strata_set 			on t_strata_set.country = c_country.id
	join sdesign.t_stratum 				on t_stratum.strata_set = t_strata_set.id
	join sdesign.t_panel				on t_panel.stratum = t_stratum.id
	join sdesign.cm_cluster2panel_mapping 		on cm_cluster2panel_mapping.panel = t_panel.id
	join sdesign.t_cluster 				on t_cluster.id = cm_cluster2panel_mapping.cluster
	join sdesign.f_p_plot 				on f_p_plot.cluster = t_cluster.id
	join sdesign.t_cluster_configuration 		on t_cluster_configuration.id = t_panel.cluster_configuration
	join sdesign.cm_plot2cluster_config_mapping 	on (
		cm_plot2cluster_config_mapping.cluster_configuration = t_cluster_configuration.id and
		cm_plot2cluster_config_mapping.plot = f_p_plot.gid)
	join sdesign.cm_refyearset2panel_mapping	on cm_refyearset2panel_mapping.panel = t_panel.id
	join sdesign.t_reference_year_set		on t_reference_year_set.id = cm_refyearset2panel_mapping.reference_year_set
	join sdesign.t_inventory_campaign		on t_inventory_campaign.id = t_reference_year_set.inventory_campaign
	----------------------------------------
	join csv.plot_target_data as t			on (
		c_country.label = t.country and
		t_strata_set.strata_set = t.strata_set and
		t_stratum.stratum = t.stratum and
		t_panel.panel = t.panel and
		t_cluster.cluster = t.cluster and
		f_p_plot.plot = t.plot and
		t_reference_year_set.reference_year_set = t.reference_year_set and
		t_inventory_campaign.inventory_campaign = t.inventory_campaign)
	----------------------------------------
	join nfiesta.c_target_variable		on (c_target_variable.metadata->'en'->'indicator'->>'label' = t.target_variable)
	left join nfiesta.c_sub_population			on (c_sub_population.label = t.sub_population)
	left join nfiesta.c_sub_population_category	on (
		c_sub_population_category.sub_population = c_sub_population.id and
		c_sub_population_category.label = t.sub_population_category)
	left join nfiesta.c_area_domain			on (c_area_domain.label = t.area_domain)
	left join nfiesta.c_area_domain_category		on (
		c_area_domain_category.area_domain = c_area_domain.id and
		c_area_domain_category.label = t.area_domain_category)
	join nfiesta.t_variable 				on (
		t_variable.target_variable = c_target_variable.id and
		coalesce(t_variable.sub_population_category, 0) = coalesce(c_sub_population_category.id, 0) and
		coalesce(t_variable.area_domain_category, 0) = coalesce(c_area_domain_category.id, 0))
)
insert into nfiesta.t_target_data(plot, available_datasets, value)
select
	plot,
	(	select
			id as available_datasets
		from nfiesta.t_available_datasets
		where t_available_datasets.panel = w_data.panel
		and t_available_datasets.variable = w_data.variable
		and t_available_datasets.reference_year_set = w_data.reference_year_set
	),
	value
from w_data
where value != 0;
--
-- Name: t_available_datasets_id_seq; Type: SEQUENCE SET; Schema: nfiesta; Owner: vagrant
--
SELECT pg_catalog.setval('nfiesta.t_available_datasets_id_seq', (select max(id) from nfiesta.t_available_datasets), true);
 setval 
--------
     34
(1 row)

--
-- Name: c_area_domain_category_id_seq; Type: SEQUENCE SET; Schema: nfiesta; Owner: vagrant
--
SELECT pg_catalog.setval('nfiesta.c_area_domain_category_id_seq', (select max(id) from nfiesta.c_area_domain_category), true);
 setval 
--------
      2
(1 row)

--
-- Name: c_area_domain_id_seq; Type: SEQUENCE SET; Schema: nfiesta; Owner: vagrant
--
SELECT pg_catalog.setval('nfiesta.c_area_domain_id_seq', (select max(id) from nfiesta.c_area_domain), true);
 setval 
--------
      1
(1 row)

--
-- Name: c_auxiliary_variable_category_id_seq; Type: SEQUENCE SET; Schema: nfiesta; Owner: vagrant
--
SELECT pg_catalog.setval('nfiesta.c_auxiliary_variable_category_id_seq', (select max(id) from nfiesta.c_auxiliary_variable_category), true);
 setval 
--------
      2
(1 row)

--
-- Name: c_auxiliary_variable_id_seq; Type: SEQUENCE SET; Schema: nfiesta; Owner: vagrant
--
SELECT pg_catalog.setval('nfiesta.c_auxiliary_variable_id_seq', (select max(id) from nfiesta.c_auxiliary_variable), true);
 setval 
--------
      1
(1 row)

--
-- Name: c_estimation_cell_collection_id_seq; Type: SEQUENCE SET; Schema: nfiesta; Owner: vagrant
--
SELECT pg_catalog.setval('nfiesta.c_estimation_cell_collection_id_seq', (select max(id) from nfiesta.c_estimation_cell_collection), true);
 setval 
--------
      4
(1 row)

--
-- Name: c_estimation_cell_id_seq; Type: SEQUENCE SET; Schema: nfiesta; Owner: vagrant
--
SELECT pg_catalog.setval('nfiesta.c_estimation_cell_id_seq', (select max(id) from nfiesta.c_estimation_cell), true);
 setval 
--------
    127
(1 row)

--
-- Name: c_sub_population_category_id_seq; Type: SEQUENCE SET; Schema: nfiesta; Owner: vagrant
--
SELECT pg_catalog.setval('nfiesta.c_sub_population_category_id_seq', (select max(id) from nfiesta.c_sub_population_category), true);
 setval 
--------
      2
(1 row)

--
-- Name: c_sub_population_id_seq; Type: SEQUENCE SET; Schema: nfiesta; Owner: vagrant
--
SELECT pg_catalog.setval('nfiesta.c_sub_population_id_seq', (select max(id) from nfiesta.c_sub_population), true);
 setval 
--------
      1
(1 row)

--
-- Name: c_target_variable_id_seq; Type: SEQUENCE SET; Schema: nfiesta; Owner: vagrant
--
SELECT pg_catalog.setval('nfiesta.c_target_variable_id_seq', (select max(id) from nfiesta.c_target_variable), true);
 setval 
--------
      4
(1 row)

--
-- Name: c_topic_id_seq; Type: SEQUENCE SET; Schema: nfiesta; Owner: vagrant
--
SELECT pg_catalog.setval('nfiesta.c_topic_id_seq', (select max(id) from nfiesta.c_topic), true);
 setval 
--------
      2
(1 row)

--
-- Name: cm_tvariable2topic_id_seq; Type: SEQUENCE SET; Schema: nfiesta; Owner: vagrant
--
SELECT pg_catalog.setval('nfiesta.cm_tvariable2topic_id_seq', (select max(id) from nfiesta.cm_tvariable2topic), true);
 setval 
--------
      4
(1 row)

--
-- Name: cm_plot2cell_mapping_id_seq; Type: SEQUENCE SET; Schema: nfiesta; Owner: vagrant
--
SELECT pg_catalog.setval('nfiesta.cm_plot2cell_mapping_id_seq', (select max(id) from nfiesta.cm_plot2cell_mapping), true);
 setval 
--------
  45292
(1 row)

--
-- Name: t_auxiliary_data_id_seq; Type: SEQUENCE SET; Schema: nfiesta; Owner: vagrant
--
SELECT pg_catalog.setval('nfiesta.t_auxiliary_data_id_seq', (select max(id) from nfiesta.t_auxiliary_data), true);
 setval 
--------
  15115
(1 row)

--
-- Name: t_target_data_id_seq; Type: SEQUENCE SET; Schema: nfiesta; Owner: vagrant
--
SELECT pg_catalog.setval('nfiesta.t_target_data_id_seq', (select max(id) from nfiesta.t_target_data), true);
 setval 
--------
  21050
(1 row)

--
-- Name: t_variable_id_seq; Type: SEQUENCE SET; Schema: nfiesta; Owner: vagrant
--
SELECT pg_catalog.setval('nfiesta.t_variable_id_seq', (select max(id) from nfiesta.t_variable), true);
 setval 
--------
      6
(1 row)

--
-- PostgreSQL database dump complete
--
