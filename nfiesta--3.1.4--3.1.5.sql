--
-- Copyright 2017, 2023 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--



ALTER TABLE @extschema@.cm_tvariable2topic ADD CONSTRAINT ukey__cm_tvariable2topic UNIQUE (target_variable,topic);
ALTER TABLE @extschema@.c_topic ADD CONSTRAINT ukey__c_topic__label UNIQUE (label);
ALTER TABLE @extschema@.c_topic ADD CONSTRAINT ukey__c_topic__label_en UNIQUE (label_en);


DROP FUNCTION IF EXISTS @extschema@.fn_etl_save_topic(varchar, text, varchar, text, integer) CASCADE;



-- <function name="fn_etl_save_topic" schema="extschema" src="functions/extschema/etl/fn_etl_save_topic.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_save_topic(varchar, text, varchar, text)

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_save_topic(varchar, text, varchar, text) CASCADE;

create or replace function @extschema@.fn_etl_save_topic
(
	_label				varchar(200),
	_description		text,
	_label_en			varchar(200),
	_description_en		text
)
returns integer
as
$$
declare
		_id integer;
begin
	if _label is null or _description is null or _label_en is null or _description_en is null
	then
		raise exception 'Error 01: fn_etl_save_topic: Mandatory input of label, description, label_en or description_en is null (%, %, %, %).', _label, _description, _label_en, _description_en;
	end if; 

	insert into @extschema@.c_topic(label, description, label_en, description_en)
	select _label, _description, _label_en, _description_en
	returning id
	into _id;

	return _id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_save_topic(varchar, text, varchar, text) is
'Function inserts a record into c_topic table.';

grant execute on function @extschema@.fn_etl_save_topic(varchar, text, varchar, text) to public;
-- </function>



-- <function name="fn_etl_update_topic" schema="extschema" src="functions/extschema/etl/fn_etl_update_topic.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_update_topic(integer, varchar, text, varchar, text)

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_update_topic(integer, varchar, text, varchar, text) CASCADE;

create or replace function @extschema@.fn_etl_update_topic
(
	_id					integer,
	_label				varchar(200),
	_description		text,
	_label_en			varchar(200),
	_description_en		text
)
returns integer
as
$$
begin
	if _id is null
	then
		raise exception 'Error 01: fn_etl_update_topic: Input argument _id must not be null!';
	end if;

	if _label is null and _description is null and _label_en is null and _description_en is null
	then
		raise exception 'Error 02: fn_etl_update_topic: All input arguments _label, _description, _label_en and _description_en must not be null!';
	end if;

	if _label is not null
	then
		update @extschema@.c_topic set label = _label where id = _id;
	end if;

	if _description is not null
	then
		update @extschema@.c_topic set description = _description where id = _id;
	end if;

	if _label_en is not null
	then
		update @extschema@.c_topic set label_en = _label_en where id = _id;
	end if;

	if _description_en is not null
	then
		update @extschema@.c_topic set description_en = _description_en where id = _id;
	end if;

	return _id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_update_topic(integer, varchar, text, varchar, text) is
'The function updates a record in c_topic table.';

grant execute on function @extschema@.fn_etl_update_topic(integer, varchar, text, varchar, text) to public;
-- </function>



-- <function name="fn_etl_try_delete_topic" schema="extschema" src="functions/extschema/etl/fn_etl_try_delete_topic.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_try_delete_topic(integer) CASCADE;

CREATE OR REPLACE FUNCTION @extschema@.fn_etl_try_delete_topic(_id integer)
RETURNS boolean
AS
$$
BEGIN
	IF NOT EXISTS (SELECT t1.* FROM @extschema@.c_topic as t1 where t1.id = _id)
	THEN
		RAISE EXCEPTION 'Error 01: fn_etl_try_delete_topic: Given topic (%) does not exist in c_topic table!',_id;
	END IF;

	return not exists (
	select t2.id from @extschema@.cm_tvariable2topic as t2 where t2.topic = _id
	);

END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION @extschema@.fn_etl_try_delete_topic(integer) IS
'Function provides test if it is possible to delete record from c_topic table.';

GRANT EXECUTE ON FUNCTION @extschema@.fn_etl_try_delete_topic(integer) TO public;
-- </function>



-- <function name="fn_etl_delete_topic" schema="extschema" src="functions/extschema/etl/fn_etl_delete_topic.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- DROP FUNCTION @extschema@.fn_etl_delete_topic(integer) CASCADE;

CREATE OR REPLACE FUNCTION @extschema@.fn_etl_delete_topic(_id integer)
RETURNS void
AS
$$
BEGIN

	IF _id IS NULL THEN 
		RAISE EXCEPTION 'Error: 01: fn_etl_delete_topic: Parameter _id must not be null!';
	END IF;

	IF NOT EXISTS (SELECT * FROM @extschema@.c_topic AS t1 WHERE t1.id = _id)
		THEN RAISE EXCEPTION 'Error: 02: fn_etl_delete_topic: Given topic (%) does not exist in c_topic table.', _id;
	END IF;
	
	DELETE FROM @extschema@.c_topic 
	WHERE id  = _id;

END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION @extschema@.fn_etl_delete_topic(integer) IS
'Function delete record from c_topic table.';

GRANT EXECUTE ON FUNCTION @extschema@.fn_etl_delete_topic(integer) TO public;
-- </function>



-- <function name="fn_etl_delete_target_variable2topic" schema="extschema" src="functions/extschema/etl/fn_etl_delete_target_variable2topic.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- DROP FUNCTION @extschema@.fn_etl_delete_target_variable2topic(integer, integer[]) CASCADE;

CREATE OR REPLACE FUNCTION @extschema@.fn_etl_delete_target_variable2topic
(
	_target_variable integer,
	_topics integer[]
)
RETURNS void
AS
$$
BEGIN

	IF _target_variable IS NULL
	THEN 
		RAISE EXCEPTION 'Error: 01: fn_etl_delete_target_variable2topic: Parameter _target_variable must not be null!';
	END IF;

	IF _topics IS NULL
	THEN 
		RAISE EXCEPTION 'Error: 02: fn_etl_delete_target_variable2topic: Parameter _topics must not be null!';
	END IF;	

	for i in 1..array_length(_topics,1)
	loop
		IF NOT EXISTS (SELECT * FROM @extschema@.cm_tvariable2topic AS t1 WHERE t1.target_variable = _target_variable and t1.topic = _topics[i])
			THEN RAISE EXCEPTION 'Error: 03: fn_etl_delete_target_variable2topic: Given mapping between target variable (%) and topic (%) does not exist in cm_tvariable2topic table.', _target_variable, _topics[i];
		END IF;
	end loop;
	
	DELETE FROM @extschema@.cm_tvariable2topic 
	WHERE target_variable = _target_variable
	and topic in (select unnest(_topics));

END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION @extschema@.fn_etl_delete_target_variable2topic(integer, integer[]) IS
'Function delete record from cm_tvariable2topic table.';

GRANT EXECUTE ON FUNCTION @extschema@.fn_etl_delete_target_variable2topic(integer, integer[]) TO public;
-- </function>