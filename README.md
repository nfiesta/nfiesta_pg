# About

This PostgreSQL extension containins database objects and functionality to compute forest inventory estimates. 

# Documentation

* wiki [homepage](https://gitlab.com/nfiesta/nfiesta_pg/-/wikis/home)
* for brief installation manual see [Installation page](https://gitlab.com/nfiesta/nfiesta_pg/-/wikis/Installation)
* [Minimal Working Example](Minimal-Working-Example) page
