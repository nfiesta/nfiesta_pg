--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--


-- ETL FUNCTIONS --


-- <function name="fn_etl_check_target_variable" schema="extschema" src="functions/extschema/etl/fn_etl_check_target_variable.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_check_target_variable(json)

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_check_target_variable(json) CASCADE;

create or replace function @extschema@.fn_etl_check_target_variable
(
	_metadata json
)
returns integer
as
$$
declare
		_id integer;
begin
		if _metadata is null
		then
			raise exception 'Error 01: fn_etl_check_target_variable: Input argument _metadata must not by NULL!';
		end if; 
	
		select id from @extschema@.c_target_variable
		where	(
				(metadata->'en')::jsonb @> (_metadata->'en')::jsonb
				and
				(_metadata->'en')::jsonb @> (metadata->'en')::jsonb
				)
		into _id;

		return _id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_check_target_variable(json) is
'Function returns record ID from table c_target_variable based on given parameter.';

grant execute on function @extschema@.fn_etl_check_target_variable(json) to public;
-- </function>



-- <function name="fn_etl_get_target_variable" schema="extschema" src="functions/extschema/etl/fn_etl_get_target_variable.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_get_target_variable(character varying)

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_get_target_variable(character varying) CASCADE;

create or replace function @extschema@.fn_etl_get_target_variable
(
	_national_language	character varying(2) default 'en'::character varying
)
returns table
(
	id			integer,
	metadata	json
)
as
$$
declare
begin
		if _national_language = 'en'
		then
			return query
			with
			w1 as	(
					select
							ctv.id,
							ctv.metadata->'en' as metadata
					from
							 @extschema@.c_target_variable as ctv
					)
			select
					w1.id,
					json_build_object('en',w1.metadata) as metadata
			from
					w1 order by w1.id;
		else
			return query
			with
			w1 as	(
					select
							ctv.id,
							ctv.metadata->'en' as metadata
					from
							 @extschema@.c_target_variable as ctv
					)
			,w2 as	(
					select
							ctv.id,
							ctv.metadata->_national_language as metadata
					from
							 @extschema@.c_target_variable as ctv
					)
			,w3 as	(
					select
							w1.id,
							w1.metadata as metadata_en,
							w2.metadata as metadata_nl
					from
								w1
					inner join	w2 on w1.id = w2.id
					)
			select
					w3.id,
					json_build_object
						(
						_national_language,w3.metadata_nl,
						'en',w3.metadata_en
						) as metadata
			from
					w3 order by w3.id;
		end if;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_get_target_variable(character varying) is
'Function returns records from table c_target_variable.';

grant execute on function @extschema@.fn_etl_get_target_variable(character varying) to public;
-- </function>



-- <function name="fn_etl_import_target_variable" schema="extschema" src="functions/extschema/etl/fn_etl_import_target_variable.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_import_target_variable(json)

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_import_target_variable(json) CASCADE;

create or replace function @extschema@.fn_etl_import_target_variable
(
	_metadata json
)
returns integer
as
$$
declare
		_id integer;
begin
	if _metadata is null
	then
		raise exception 'Error 01: fn_etl_import_target_variable: Input argument _metadata must not by NULL!';
	end if; 

	insert into @extschema@.c_target_variable(metadata)
	select _metadata
	returning id
	into _id;

	return _id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_import_target_variable(json) is
'Function inserts a record into table c_target_variable based on given parameters.';

grant execute on function @extschema@.fn_etl_import_target_variable(json) to public;
-- </function>



-- <function name="fn_etl_import_target_variable_metadata" schema="extschema" src="functions/extschema/etl/fn_etl_import_target_variable_metadata.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_import_target_variable_metadata(integer, json)

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_import_target_variable_metadata(integer, json) CASCADE;

create or replace function @extschema@.fn_etl_import_target_variable_metadata
(
	_id			integer,
	_metadata	json
)
returns text
as
$$
declare
		_json_key_input			text;	-- key of national language for adding to metadata
		_json_keys_target_db	text[]; -- list of languages in target metadata
		_res					text;
		_query_i_select			text;
		_query_i_join			text;
		_query_i_metadata		text;
		_query_i_object			text;
		_query_select			text;
		_query_join				text;
		_query_metadata			text;
		_query_object			text;
		_query_res				text;
		_json4update			json;
begin
	if _id is null
	then
		raise exception 'Error 01: fn_etl_import_target_variable_metadata: Input argument _id must not be NULL!';
	end if; 

	if _metadata is null
	then
		raise exception 'Error 02: fn_etl_import_target_variable_metadata: Input argument _metadata must not be NULL!';
	end if;

	with
	w1 as	(
			select json_object_keys(_metadata) as json_key
			)
	select w1.json_key from w1 where w1.json_key is distinct from 'en'
	into _json_key_input;

	if _json_key_input is null -- input JSON contains only national language EN
	then
		_res := concat('The metadata of target variable [c_target_variable.id = ',_id,'] was unchanged.');
	else
		with
		w1 as	(
				select json_object_keys(ctv.metadata) as json_key
				from @extschema@.c_target_variable as ctv
				where ctv.id = _id
				)
		,w2 as	(
				select distinct w1.json_key from w1
				)
		select array_agg(w2.json_key) from w2
		into _json_keys_target_db;
	
		if	(-- if json_key_input is not contained in metadata => adding metadatas for national language is needed
			select count(t.keys) = 0 from
				(select unnest(_json_keys_target_db) as keys) as t
			where t.keys = _json_key_input
			)
		then		
			for i in 1..array_length(_json_keys_target_db,1)
			loop
				_query_i_select := concat
				(
				',w',i,' as (select 1 as id4join, ctv',i,'.metadata->''',_json_keys_target_db[i],''' as metadata from @extschema@.c_target_variable as ctv',i,' where ctv',i,'.id = $2)'
				);

				_query_i_join := concat
				(
				' inner join w',i,' on w.id4join = w',i,'.id4join'
				);

				_query_i_metadata := concat
				(
				',w',i,'.metadata as metadata_',_json_keys_target_db[i]
				);

				_query_i_object := concat
				(
				',''',_json_keys_target_db[i],''',w_inner.metadata_',_json_keys_target_db[i]
				);
				
				if i = 1
				then
					_query_select := _query_i_select;
					_query_join := _query_i_join;
					_query_metadata := _query_i_metadata;
					_query_object := _query_i_object;
				else
					_query_select := _query_select || _query_i_select;
					_query_join := _query_join || _query_i_join;
					_query_metadata := _query_metadata || _query_i_metadata;
					_query_object := _query_object || _query_i_object;
				end if;
			end loop;

			_query_res := concat
			(
			'
			with
			w as	(
					select 1 as id4join, ($1->''',_json_key_input,''') as metadata
					)
			'
			,_query_select,
			'
			,w_inner as	(
						select
								w.id4join,w.metadata'
								,_query_metadata,
						' from w'
						,_query_join,
						')'
			'
			select
					--w_inner.id4join,
					json_build_object
						(
						''',_json_key_input,''',w_inner.metadata
						',_query_object,'
						) as metadata
			from
				w_inner;'
			);
		
			execute ''||_query_res||'' using _metadata, _id into _json4update;
		
			update @extschema@.c_target_variable as ctv set metadata = _json4update
			where ctv.id = _id;
		
			_res := concat('The metadata of target variable [c_target_variable.id = ',_id,'] was changed. Added metadata for national language "',_json_key_input,'".');
			
		else
			_res := concat('The metadata of target variable [c_target_variable.id = ',_id,'] was unchanged.');
		end if;
	end if;	

	return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_import_target_variable_metadata(integer, json) is
'Function inserts a record into table c_target_variable based on given parameters.';

grant execute on function @extschema@.fn_etl_import_target_variable_metadata(integer, json) to public;
-- </function>



-- <function name="fn_etl_array_compare" schema="extschema" src="functions/extschema/etl/fn_etl_array_compare.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_array_compare(anyarray,anyarray)

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_array_compare(anyarray,anyarray) CASCADE;

create or replace function @extschema@.fn_etl_array_compare(anyarray,anyarray) 
returns boolean as $$
declare
		_res	boolean;
begin
	_res := (	
			select
				case
			  		when array_dims($1) <> array_dims($2) then 'f'
			 		when array_length($1,1) <> array_length($2,1) then 'f'
			  	else
				    not exists	(
						        select 1
						        from unnest($1) a 
						        full join unnest($2) b on (a=b) 
						        where a is null or b is null
				   				)
		  		end
		  	);
	
	return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_array_compare(anyarray, anyarray) is
'Function returns true (the number of elements and the elements are identical) or false (the number of elements and the elements are not identical).';

grant execute on function @extschema@.fn_etl_array_compare(anyarray, anyarray) to public;
-- </function>



-- <function name="fn_etl_check_variables" schema="extschema" src="functions/extschema/etl/fn_etl_check_variables.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_check_variables(json)

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_check_variables(json) CASCADE;

create or replace function @extschema@.fn_etl_check_variables
(
	_variables	json
)
returns json
as
$$
declare
		_country									varchar;
		_target_variable							integer;
		_count_comb_panels_and_reference_year_sets	integer;
		_refyearset2panel_mapping					integer[];
		_res										json;
begin
		if _variables is null
		then
			raise exception 'Error 01: fn_etl_check_variables: Input argument _variables must not by NULL!';
		end if;

		_country := (_variables->>'country')::varchar;
		_target_variable := (_variables->>'target_variable')::integer;

		-- check country --
		if _country is null
		then
			raise exception 'Error 02: fn_etl_check_variables: Internal argument _country must not by NULL!';
		end if;

		if	(select count(id) is distinct from 1 from sdesign.c_country where label = _country)
		then
			raise exception 'Error 03: fn_etl_check_variables: Input JSON element "country = %" is not present in table sdesign.c_country!',_country;
		end if;

		------------
		-- raise notice '_country: %',_country;
		------------

		-- check target_variable
		if _target_variable is null
		then
			raise exception 'Error 04: fn_etl_check_variables: Internal argument _target_variable must not by NULL!';
		end if;

		if	(select count(id) is distinct from 1 from @extschema@.c_target_variable where id = _target_variable)
		then
			raise exception 'Error 05: fn_etl_check_variables: Input JSON element "target_variable = %" is not present in table c_target_variable!',_target_variable;
		end if;

		------------
		-- raise notice '_target_variable: %',_target_variable;
		------------			

		-- check 1
		with
		w1 as	(
				select json_array_elements(_variables->'panels_and_reference_year_sets') as s
				)
		,w2 as	(
				select
						(s->>'panel')::varchar				as panel,
						(s->>'reference_year_set')::varchar as reference_year_set
				from w1
				)
		select count(*) from w2
		into _count_comb_panels_and_reference_year_sets;

		if _count_comb_panels_and_reference_year_sets is null
		then
			raise exception 'Error 06: fn_etl_check_variables: Internal argument _count_comb_panels_and_reference_year_sets must not by NULL!';
		end if;

		------------
		-- raise notice '_count_comb_panels_and_reference_year_sets: %',_count_comb_panels_and_reference_year_sets;
		------------	

		-- check 2
		with
		w1 as	(
				select json_array_elements(_variables->'panels_and_reference_year_sets') as s
				)
		,w2 as	(
				select
						(s->>'panel')::varchar				as panel,
						(s->>'reference_year_set')::varchar	as reference_year_set
				from w1
				)
		,w3 as	(
				select
					t1.id as panel_id,
					t1.panel,
					t3.id as reference_year_set_id,
					t3.reference_year_set,
					t2.id as refyearset2panel_mapping
				from
					(
					select id, panel from sdesign.t_panel where stratum in
					(select id from sdesign.t_stratum where strata_set in
					(select id from sdesign.t_strata_set where country = 
					(select id from sdesign.c_country where label = _country)))
					) as t1
				inner join sdesign.cm_refyearset2panel_mapping as t2 on t1.id = t2.panel
				inner join sdesign.t_reference_year_set as t3 on t2.reference_year_set = t3.id
				)
		,w4 as	(
				select w3.refyearset2panel_mapping from w2 inner join w3
				on w2.panel = w3.panel and w2.reference_year_set = w3.reference_year_set
				)
		select array_agg(w4.refyearset2panel_mapping order by w4.refyearset2panel_mapping)
		from w4
		into _refyearset2panel_mapping;

		------------
		-- raise notice '_refyearset2panel_mapping: %',_refyearset2panel_mapping;
		------------			

		if _refyearset2panel_mapping is null
		then
			raise exception 'Error 07: fn_etl_check_variables: Internal argument _refyearset2panel_mapping must not by NULL!';
		end if;	

		if _count_comb_panels_and_reference_year_sets != array_length(_refyearset2panel_mapping,1)
		then
			raise exception 'Error 08: fn_etl_check_variables: Some input combination of panel and reference year set is not present in sampling design!';
		end if;			

		with
		w1 as	(
				select
						a.*
				from
							(select * from @extschema@.t_available_datasets) as a
				inner join	(		
							select * from sdesign.cm_refyearset2panel_mapping
							where id in (select unnest(_refyearset2panel_mapping))
							) as b
									on a.panel = b.panel and a.reference_year_set = b.reference_year_set
				)
		,w2 as	(
				select distinct variable from w1 where variable in
					(select id from @extschema@.t_variable where target_variable = _target_variable)
				order by variable
				)
		,w3 as	(
				select
					t1.target_variable,
					t1.sub_population_category,
					t1.area_domain_category,
					coalesce(t2.label_en,'null'::varchar) as spc_label,
					coalesce(t3.label_en,'null'::varchar) as adc_label,
					coalesce(t4.label_en,'null'::varchar) as spt_label,
					coalesce(t5.label_en,'null'::varchar) as adt_label,
					t2.id as spc_id,
					t3.id as adc_id,
					t4.id as spt_id,
					t5.id as adt_id
		 		from
					(select * from @extschema@.t_variable where id in (select variable from w2)) as t1
					left join @extschema@.c_sub_population_category as t2 on t1.sub_population_category = t2.id
					left join @extschema@.c_area_domain_category as t3 on t1.area_domain_category = t3.id
					left join @extschema@.c_sub_population as t4 on t2.sub_population = t4.id
					left join @extschema@.c_area_domain as t5 on t3.area_domain = t5.id
				order by t1.id 
				)
		,w4 as	(
				select distinct spt_label, spc_label, adt_label, adc_label, spc_id, adc_id, spt_id, adt_id from w3
				)
		,w5 as	(-- list of attribute variables for target variable [target DB]
				select
						row_number() over() as new_id,
						spt_label,
						spc_label,
						adt_label,
						adc_label,
						string_to_array(spt_label,';') as spt_label_array,
						string_to_array(spc_label,';') as spc_label_array,
						string_to_array(adt_label,';') as adt_label_array,
						string_to_array(adc_label,';') as adc_label_array,
						case when spt_id is null then 0 else spt_id end as spt_id,
						case when spc_id is null then 0 else spc_id end as spc_id,
						case when adt_id is null then 0 else adt_id end as adt_id,
						case when adc_id is null then 0 else adc_id end as adc_id
				from w4
				)
		,w5a as	(
				select w5.* from w5 where w5.spt_id = 0 and w5.spc_id = 0 and w5.adt_id = 0 and w5.adc_id = 0	-- row without distinction
				)
		,w5b as	(
				select w5.* from w5 where w5.new_id != (select w5a.new_id from w5a) -- attribute variables without row without distinction
				)
		------------------------------------------------------------------
		,w6 as	(-- list of attribute variables [source DB]
				select json_array_elements(_variables->'variables') as s
				)
		,w7 as	(
				select
						string_to_array((s->>'sub_population')::varchar,';')			as vsp,
						string_to_array((s->>'sub_population_category')::varchar,';')	as vspc,
						string_to_array((s->>'area_domain')::varchar,';')				as vad,
						string_to_array((s->>'area_domain_category')::varchar,';')		as vadc,
						(s->>'sub_population_etl_id')::integer							as vsp_id,
						(s->>'sub_population_category_etl_id')::integer					as vspc_id,
						(s->>'area_domain_etl_id')::integer								as vad_id,
						(s->>'area_domain_category_etl_id')::integer					as vadc_id
				from w6
				)
		,w8 as	(-- list of attribute variables that were ETLed
				select
						w5b.*,
						w7.*

				from
						w5b inner join w7
				on
						(
						w5b.spt_id = w7.vsp_id	and
						w5b.spc_id = w7.vspc_id and
						w5b.adt_id = w7.vad_id	and
						w5b.adc_id = w7.vadc_id
						)
				)
		,w9 as	(
				select w5b.* from w5b where w5b.new_id not in (select w8.new_id from w8)
				)				
		,w10 as	(
				select
						w9.*,
						w7.*,
						case when (w7.vsp is null or w7.vspc is null or w7.vad is null or w7.vadc is null) then true
						else false
						end as is_missing
				from
						w9 left join w7
				on
					(
					@extschema@.fn_etl_array_compare(w9.spt_label_array,w7.vsp)  and
					@extschema@.fn_etl_array_compare(w9.spc_label_array,w7.vspc) and
					@extschema@.fn_etl_array_compare(w9.adt_label_array,w7.vad)  and
					@extschema@.fn_etl_array_compare(w9.adc_label_array,w7.vadc)
					)
				)
		select
				json_agg(json_build_object(
					'sub_population', w10.spt_label,
					'sub_population_category', w10.spc_label,
					'area_domain', w10.adt_label,
					'area_domain_category',	w10.adc_label))
		from
				w10 where is_missing = true
		into
				_res;
	
		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_check_variables(json) is
'Function checks that input variables are contained in target database.';

grant execute on function @extschema@.fn_etl_check_variables(json) to public;
-- </function>



-- <function name="fn_etl_check_sub_population" schema="extschema" src="functions/extschema/etl/fn_etl_check_sub_population.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_check_sub_population(integer, varchar)

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_check_sub_population(integer, varchar) CASCADE;

create or replace function @extschema@.fn_etl_check_sub_population
(
	_id			integer,
	_label_en	varchar
)
returns table
(
	id				integer,
	etl_id			integer,
	label			character varying,
	description		text,
	label_en		character varying,
	description_en	text	
)
as
$$
declare
		_res_etl_id				integer;
		_res_label				character varying;
		_res_description		text;
		_res_label_en			character varying;
		_res_description_en		text;
begin
		if _id is null
		then
			raise exception 'Error 01: fn_etl_check_sub_population: Input argument _id must not by NULL!';
		end if; 
	
		if _label_en is null
		then
			raise exception 'Error 02: fn_etl_check_sub_population: Input argument _label_en must not by NULL!';
		end if;
	
		select
				csp.id,
				csp.label,
				csp.description,
				csp.label_en,
				csp.description_en
		from
				@extschema@.c_sub_population as csp
		where
				@extschema@.fn_etl_array_compare
					(
					string_to_array(replace(lower(csp.label_en),' ',''),';'),
					string_to_array(replace(lower(_label_en),' ',''),';')
					)
		into
				_res_etl_id,
				_res_label,
				_res_description,
				_res_label_en,
				_res_description_en;

		return query select _id, _res_etl_id, _res_label, _res_description, _res_label_en, _res_description_en;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_check_sub_population(integer, varchar) is
'Function returns record ID from table c_sup_population based on given parameters.';

grant execute on function @extschema@.fn_etl_check_sub_population(integer, varchar) to public;
-- </function>



-- <function name="fn_etl_get_sub_populations" schema="extschema" src="functions/extschema/etl/fn_etl_get_sub_populations.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_get_sub_populations(integer, integer[])

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_get_sub_populations(integer, integer[]) CASCADE;

create or replace function @extschema@.fn_etl_get_sub_populations
(
	_id			integer,
	_etl_id		integer[] default null::integer[]
)
returns table
(
	id				integer,
	etl_id			integer,
	label			varchar,
	description		text,
	label_en		varchar,
	description_en	text
)
as
$$
declare
begin
		if _id is null
		then
			raise exception 'Error 01: fn_etl_get_sub_populations: Input argument _id must not be null!';
		end if;

		if _etl_id is null
		then
			return query
			select
					_id as id,
					sp.id as etl_id,
					sp.label,
					sp.description,
					sp.label_en,
					sp.description_en
			from
					@extschema@.c_sub_population as sp
			order
					by sp.id;
		else
			return query
			select
					_id as id,
					sp.id as etl_id,
					sp.label,
					sp.description,
					sp.label_en,
					sp.description_en
			from
					@extschema@.c_sub_population as sp
			where
					sp.id not in (select unnest(_etl_id))					
			order
					by sp.id;		
		end if;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_get_sub_populations(integer, integer[]) is
'Function returns all record from table c_sup_population.';

grant execute on function @extschema@.fn_etl_get_sub_populations(integer, integer[]) to public;
-- </function>



-- <function name="fn_etl_import_sub_population" schema="extschema" src="functions/extschema/etl/fn_etl_import_sub_population.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_import_sub_population(integer, varchar, text, varchar, text)

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_import_sub_population(integer, varchar, text, varchar, text) CASCADE;

create or replace function @extschema@.fn_etl_import_sub_population
(
	_id					integer,
	_label				varchar,
	_description		text,
	_label_en			varchar,
	_description_en		text
)
returns table
(
	id		integer,
	etl_id	integer
)
as
$$
declare
		_etl_id		integer;
begin
	if _id is null
	then
		raise exception 'Error 01: fn_etl_import_sub_population: Input argument _id must not by NULL!';
	end if;

	if _label is null
	then
		raise exception 'Error 02: fn_etl_import_sub_population: Input argument _label must not by NULL!';
	end if;

	if _description is null
	then
		raise exception 'Error 03: fn_etl_import_sub_population: Input argument _description must not by NULL!';
	end if;

	if _label_en is null
	then
		raise exception 'Error 04: fn_etl_import_sub_population: Input argument _label_en must not by NULL!';
	end if;

	if _description_en is null
	then
		raise exception 'Error 05: fn_etl_import_sub_population: Input argument _description_en must not by NULL!';
	end if;  

	insert into @extschema@.c_sub_population(label, description, label_en, description_en)
	select _label, _description, _label_en, _description_en
	returning c_sub_population.id
	into _etl_id;

	return query select _id, _etl_id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_import_sub_population(integer, varchar, text, varchar, text) is
'Function inserts a record into table c_sub_population based on given parameters.';

grant execute on function @extschema@.fn_etl_import_sub_population(integer, varchar, text, varchar, text) to public;
-- </function>



-- <function name="fn_etl_check_sub_population_category" schema="extschema" src="functions/extschema/etl/fn_etl_check_sub_population_category.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_check_sub_population_category(integer, integer, varchar)

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_check_sub_population_category(integer, integer, varchar) CASCADE;

create or replace function @extschema@.fn_etl_check_sub_population_category
(
	_id					integer,
	_sub_population		integer,
	_label_en			varchar
)
returns table
(
	id				integer,
	etl_id			integer,
	label			character varying,
	description		text,
	label_en		character varying,
	description_en	text	
)
as
$$
declare
		_res_etl_id				integer;
		_res_label				character varying;
		_res_description		text;
		_res_label_en			character varying;
		_res_description_en		text;
begin
		if _id is null
		then
			raise exception 'Error 01: fn_etl_check_sub_population_category: Input argument _id must not by NULL!';
		end if; 
	
		if _sub_population is null
		then
			raise exception 'Error 02: fn_etl_check_sub_population_category: Input argument _sub_population must not by NULL!';
		end if;
	
		if _label_en is null
		then
			raise exception 'Error 03: fn_etl_check_sub_population_category: Input argument _label_en must not by NULL!';
		end if;	
	
		select
				cspc.id,
				cspc.label,
				cspc.description,
				cspc.label_en,
				cspc.description_en
		from
				@extschema@.c_sub_population_category as cspc
		where
				cspc.sub_population = _sub_population
		and
				@extschema@.fn_etl_array_compare
					(
						string_to_array(replace(lower(cspc.label_en),' ',''),';'),
						string_to_array(replace(lower(_label_en),' ',''),';')					
					)
		into
				_res_etl_id,
				_res_label,
				_res_description,
				_res_label_en,
				_res_description_en;
	
		return query select _id, _res_etl_id, _res_label, _res_description, _res_label_en, _res_description_en;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_check_sub_population_category(integer, integer, varchar) is
'Function returns record ID from table c_sup_population_category based on given parameters.';

grant execute on function @extschema@.fn_etl_check_sub_population_category(integer, integer, varchar) to public;
-- </function>



-- <function name="fn_etl_get_sub_population_categories" schema="extschema" src="functions/extschema/etl/fn_etl_get_sub_population_categories.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_get_sub_population_categories(integer, integer, integer[])

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_get_sub_population_categories(integer, integer, integer[]) CASCADE;

create or replace function @extschema@.fn_etl_get_sub_population_categories
(
	_id					integer,
	_sub_population		integer,
	_etl_id				integer[] default null::integer[]
)
returns table
(
	id				integer,
	etl_id			integer,
	label			varchar,
	description		text,
	label_en		varchar,
	description_en	text
)
as
$$
declare
begin
		if _id is null
		then
			raise exception 'Error 01: fn_etl_get_sub_population_categories: Input argument _id must not be null!';
		end if;

		if _etl_id is null
		then
			return query
			select
					_id as id,
					spc.id as etl_id,
					spc.label,
					spc.description,
					spc.label_en,
					spc.description_en
			from
					@extschema@.c_sub_population_category as spc
			where
					spc.sub_population = _sub_population
			order
					by spc.id;
		else
			return query
			select
					_id as id,
					spc.id as etl_id,
					spc.label,
					spc.description,
					spc.label_en,
					spc.description_en
			from
					@extschema@.c_sub_population_category as spc
			where
					spc.sub_population = _sub_population
			and
					spc.id not in (select unnest(_etl_id))
			order
					by spc.id;		
		end if;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_get_sub_population_categories(integer, integer, integer[]) is
'Function returns all record from table c_sup_population.';

grant execute on function @extschema@.fn_etl_get_sub_population_categories(integer, integer, integer[]) to public;
-- </function>



-- <function name="fn_etl_import_sub_population_category" schema="extschema" src="functions/extschema/etl/fn_etl_import_sub_population_category.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_import_sub_population_category(integer, integer, varchar, text, varchar, text)

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_import_sub_population_category(integer, integer, varchar, text, varchar, text) CASCADE;

create or replace function @extschema@.fn_etl_import_sub_population_category
(
	_id					integer,
	_sub_population		integer,
	_label				varchar,
	_description		text,
	_label_en			varchar,
	_description_en		text
)
returns table
(
	id		integer,
	etl_id	integer
)
as
$$
declare
		_etl_id		integer;
begin
	if _id is null
	then
		raise exception 'Error 01: fn_etl_import_sub_population_category: Input argument _id must not by NULL!';
	end if;

	if _sub_population is null
	then
		raise exception 'Error 02: fn_etl_import_sub_population_category: Input argument _sub_population must not by NULL!';
	end if;

	if _label is null
	then
		raise exception 'Error 03: fn_etl_import_sub_population_category: Input argument _label must not by NULL!';
	end if;

	if _description is null
	then
		raise exception 'Error 04: fn_etl_import_sub_population_category: Input argument _description must not by NULL!';
	end if; 

	insert into @extschema@.c_sub_population_category(sub_population, label, description, label_en, description_en)
	select _sub_population, _label, _description, _label_en, _description_en
	returning c_sub_population_category.id
	into _etl_id;

	return query select _id, _etl_id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_import_sub_population_category(integer, integer, varchar, text, varchar, text) is
'Function inserts a record into table c_sub_population_category based on given parameters.';

grant execute on function @extschema@.fn_etl_import_sub_population_category(integer, integer, varchar, text, varchar, text) to public;
-- </function>



-- <function name="fn_etl_get_area_domains" schema="extschema" src="functions/extschema/etl/fn_etl_get_area_domains.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_get_area_domains(integer, integer[])

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_get_area_domains(integer, integer[]) CASCADE;

create or replace function @extschema@.fn_etl_get_area_domains
(
	_id			integer,
	_etl_id		integer[] default null::integer[]
)
returns table
(
	id				integer,
	etl_id			integer,
	label			varchar,
	description		text,
	label_en		varchar,
	description_en	text
)
as
$$
declare
begin
		if _id is null
		then
			raise exception 'Error 01: fn_etl_get_area_domains: Input argument _id must not be null!';
		end if;

		if _etl_id is null
		then
			return query
			select
					_id as id,
					ad.id as etl_id,
					ad.label,
					ad.description,
					ad.label_en,
					ad.description_en
			from
					@extschema@.c_area_domain as ad
			order
					by ad.id;
		else
			return query
			select
					_id as id,
					ad.id as etl_id,
					ad.label,
					ad.description,
					ad.label_en,
					ad.description_en
			from
					@extschema@.c_area_domain as ad
			where
					ad.id not in (select unnest(_etl_id))
			order
					by ad.id;
		end if;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_get_area_domains(integer, integer[]) is
'Function returns all record from table c_area_domain.';

grant execute on function @extschema@.fn_etl_get_area_domains(integer, integer[]) to public;
-- </function>



-- <function name="fn_etl_check_area_domain" schema="extschema" src="functions/extschema/etl/fn_etl_check_area_domain.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_check_area_domain(integer, varchar)

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_check_area_domain(integer, varchar) CASCADE;

create or replace function @extschema@.fn_etl_check_area_domain
(
	_id			integer,
	_label_en	varchar
)
returns table
(
	id				integer,
	etl_id			integer,
	label			character varying,
	description		text,
	label_en		character varying,
	description_en	text
)
as
$$
declare
		_res_etl_id				integer;
		_res_label				character varying;
		_res_description		text;
		_res_label_en			character varying;
		_res_description_en		text;
begin
		if _id is null
		then
			raise exception 'Error 01: fn_etl_check_area_domain: Input argument _id must not be NULL!';
		end if; 
	
		if _label_en is null
		then
			raise exception 'Error 02: fn_etl_check_area_domain: Input argument _label_en must not be NULL!';
		end if;
	
		select
				cad.id,
				cad.label,
				cad.description,
				cad.label_en,
				cad.description_en
		from
				@extschema@.c_area_domain as cad
		where
				@extschema@.fn_etl_array_compare
					(
					string_to_array(replace(lower(cad.label_en),' ',''),';'),
					string_to_array(replace(lower(_label_en),' ',''),';')					
					)
		into
				_res_etl_id,
				_res_label,
				_res_description,
				_res_label_en,
				_res_description_en;

		return query select _id, _res_etl_id, _res_label, _res_description, _res_label_en, _res_description_en;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_check_area_domain(integer, varchar) is
'Function returns record ID from table c_area_domain based on given parameters.';

grant execute on function @extschema@.fn_etl_check_area_domain(integer, varchar) to public;
-- </function>



-- <function name="fn_etl_import_area_domain" schema="extschema" src="functions/extschema/etl/fn_etl_import_area_domain.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_import_area_domain(integer, varchar, text, varchar, text)

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_import_area_domain(integer, varchar, text, varchar, text) CASCADE;

create or replace function @extschema@.fn_etl_import_area_domain
(
	_id					integer,
	_label				varchar,
	_description		text,
	_label_en			varchar,
	_description_en		text
)
returns table
(
	id		integer,
	etl_id	integer
)
as
$$
declare
		_etl_id		integer;
begin
	if _id is null
	then
		raise exception 'Error 01: fn_etl_import_area_domain: Input argument _id must not by NULL!';
	end if;

	if _label is null
	then
		raise exception 'Error 02: fn_etl_import_area_domain: Input argument _label must not by NULL!';
	end if;

	if _description is null
	then
		raise exception 'Error 03: fn_etl_import_area_domain: Input argument _description must not by NULL!';
	end if; 

	insert into @extschema@.c_area_domain(label, description, label_en, description_en)
	select _label, _description, _label_en, _description_en
	returning c_area_domain.id
	into _etl_id;

	return query select _id, _etl_id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_import_area_domain(integer, varchar, text, varchar, text) is
'Function inserts a record into table c_area_domain based on given parameters.';

grant execute on function @extschema@.fn_etl_import_area_domain(integer, varchar, text, varchar, text) to public;
-- </function>



-- <function name="fn_etl_check_area_domain_category" schema="extschema" src="functions/extschema/etl/fn_etl_check_area_domain_category.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_check_area_domain_category(integer, integer, varchar)

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_check_area_domain_category(integer, integer, varchar) CASCADE;

create or replace function @extschema@.fn_etl_check_area_domain_category
(
	_id					integer,
	_area_domain		integer,
	_label_en			varchar
)
returns table
(
	id				integer,
	etl_id			integer,
	label			character varying,
	description		text,
	label_en		character varying,
	description_en	text
)
as
$$
declare
		_res_etl_id				integer;
		_res_label				character varying;
		_res_description		text;
		_res_label_en			character varying;
		_res_description_en		text;		
begin
		if _id is null
		then
			raise exception 'Error 01: fn_etl_check_area_domain_category: Input argument _id must not by NULL!';
		end if; 
	
		if _area_domain is null
		then
			raise exception 'Error 02: fn_etl_check_area_domain_category: Input argument _area_domain must not by NULL!';
		end if;
	
		if _label_en is null
		then
			raise exception 'Error 03: fn_etl_check_area_domain_category: Input argument _label_en must not by NULL!';
		end if;	
	
		select
				cadc.id,
				cadc.label,
				cadc.description,
				cadc.label_en,
				cadc.description_en
		from
				@extschema@.c_area_domain_category as cadc
		where
				cadc.area_domain = _area_domain
		and
				@extschema@.fn_etl_array_compare
					(
						string_to_array(replace(lower(cadc.label_en),' ',''),';'),
						string_to_array(replace(lower(_label_en),' ',''),';')					
					)
		into
				_res_etl_id,
				_res_label,
				_res_description,
				_res_label_en,
				_res_description_en;
	
		return query select _id, _res_etl_id, _res_label, _res_description, _res_label_en, _res_description_en;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_check_area_domain_category(integer, integer, varchar) is
'Function returns record ID from table fn_etl_check_area_domain_category based on given parameters.';

grant execute on function @extschema@.fn_etl_check_area_domain_category(integer, integer, varchar) to public;
-- </function>



-- <function name="fn_etl_get_area_domain_categories" schema="extschema" src="functions/extschema/etl/fn_etl_get_area_domain_categories.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_get_area_domain_categories(integer, integer, integer[])

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_get_area_domain_categories(integer, integer, integer[] CASCADE;

create or replace function @extschema@.fn_etl_get_area_domain_categories
(
	_id				integer,
	_area_domain	integer,
	_etl_id			integer[] default null::integer[]
)
returns table
(
	id				integer,
	etl_id			integer,
	label			varchar,
	description		text,
	label_en		varchar,
	description_en	text
)
as
$$
declare
begin
		if _id is null
		then
			raise exception 'Error 01: fn_etl_get_area_domain_categories: Input argument _id must not be null!';
		end if;

		if _etl_id is null
		then
			return query
			select
					_id as id,
					adc.id as etl_id,
					adc.label,
					adc.description,
					adc.label_en,
					adc.description_en
			from
					@extschema@.c_area_domain_category as adc
			where
					adc.area_domain = _area_domain
			order
					by adc.id;
		else
			return query
			select
					_id as id,
					adc.id as etl_id,
					adc.label,
					adc.description,
					adc.label_en,
					adc.description_en
			from
					@extschema@.c_area_domain_category as adc
			where
					adc.area_domain = _area_domain
			and
					adc.id not in (select unnest(_etl_id))
			order
					by adc.id;		
		end if;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_get_area_domain_categories(integer, integer, integer[]) is
'Function returns all record from table c_area_domain_category.';

grant execute on function @extschema@.fn_etl_get_area_domain_categories(integer, integer, integer[]) to public;
-- </function>



-- <function name="fn_etl_import_area_domain_category" schema="extschema" src="functions/extschema/etl/fn_etl_import_area_domain_category.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_import_area_domain_category(integer, integer, varchar, text, varchar, text)

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_import_area_domain_category(integer, integer, varchar, text, varchar, text) CASCADE;

create or replace function @extschema@.fn_etl_import_area_domain_category
(
	_id					integer,
	_area_domain		integer,
	_label				varchar,
	_description		text,
	_label_en			varchar,
	_description_en		text
)
returns table
(
	id		integer,
	etl_id	integer
)
as
$$
declare
		_etl_id		integer;
begin
	if _id is null
	then
		raise exception 'Error 01: fn_etl_import_area_domain_category: Input argument _id must not by NULL!';
	end if;

	if _area_domain is null
	then
		raise exception 'Error 02: fn_etl_import_area_domain_category: Input argument _area_domain must not by NULL!';
	end if;

	if _label is null
	then
		raise exception 'Error 03: fn_etl_import_area_domain_category: Input argument _label must not by NULL!';
	end if;

	if _description is null
	then
		raise exception 'Error 04: fn_etl_import_area_domain_category: Input argument _description must not by NULL!';
	end if; 

	insert into @extschema@.c_area_domain_category(area_domain, label, description, label_en, description_en)
	select _area_domain, _label, _description, _label_en, _description_en
	returning c_area_domain_category.id
	into _etl_id;

	return query select _id, _etl_id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_import_area_domain_category(integer, integer, varchar, text, varchar, text) is
'Function inserts a record into table c_area_domain_category based on given parameters.';

grant execute on function @extschema@.fn_etl_import_area_domain_category(integer, integer, varchar, text, varchar, text) to public;
-- </function>



-- <function name="fn_etl_get_topics" schema="extschema" src="functions/extschema/etl/fn_etl_get_topics.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_get_topics()

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_get_topics() CASCADE;

create or replace function @extschema@.fn_etl_get_topics()
returns table
(
	id				integer,
	label			character varying(200),
	description		text,
	label_en		character varying(200),
	description_en	text
)
as
$$
begin
	return query
	select t.id, t.label, t.description, t.label_en, t.description_en
	from @extschema@.c_topic as t;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_get_topics() is
'Function returns records from c_topic table.';

grant execute on function @extschema@.fn_etl_get_topics() to public;
-- </function>



-- <function name="fn_etl_save_topic" schema="extschema" src="functions/extschema/etl/fn_etl_save_topic.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_save_topic(varchar, text, varchar, text, integer)

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_save_topic(varchar, text, varchar, text, integer) CASCADE;

create or replace function @extschema@.fn_etl_save_topic
(
	_label				varchar(200),
	_description		text,
	_label_en			varchar(200),
	_description_en		text,
	_id					integer default null::integer
)
returns integer
as
$$
begin
	if _label is null or _description is null
	then
		raise exception 'Error 01: fn_etl_save_topic: Mandatory input of label or description is null (%, %, %, %).', _label, _description, _label_en, _description_en;
	end if; 

	if _id is null
	then
		insert into @extschema@.c_topic(label, description, label_en, description_en)
		select _label, _description, _label_en, _description_en
		returning id
		into _id;
	else
		update @extschema@.c_topic
		set
			label = _label,
			description = _description,
			label_en = _label_en,
			description_en = _description_en
		where
			@extschema@.id = _id;
	end if;

	return _id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_save_topic(varchar, text, varchar, text, integer) is
'Function inserts a record into table c_topic based on given parameters.';

grant execute on function @extschema@.fn_etl_save_topic(varchar, text, varchar, text, integer) to public;
-- </function>



-- <function name="fn_etl_get_target_variable2topic" schema="extschema" src="functions/extschema/etl/fn_etl_get_target_variable2topic.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_get_target_variable2topic(integer)

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_get_target_variable2topic(integer) CASCADE;

create or replace function @extschema@.fn_etl_get_target_variable2topic
(
	_target_variable	integer
)
returns table
(
	id				integer,
	label			varchar,
	description		text,
	label_en		varchar,
	description_en	text
)
as
$$
begin
		if _target_variable is null
		then
			raise exception 'Error 01: fn_etl_get_target_variable2topic: Input argument _target_variable must not be null!';
		end if;
	
		return query
		select t.id, t.label, t.description, t.label_en, t.description_en
		from @extschema@.c_topic as t where t.id in
		(select topic from @extschema@.cm_tvariable2topic
		where target_variable = _target_variable)
		order by label;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_get_target_variable2topic(integer) is
'Function returns list of topics for input target variable.';

grant execute on function @extschema@.fn_etl_get_target_variable2topic(integer) to public;
-- </function>



-- <function name="fn_etl_save_target_variable2topic" schema="extschema" src="functions/extschema/etl/fn_etl_save_target_variable2topic.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_save_target_variable2topic(integer, integer)

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_save_target_variable2topic(integer, integer) CASCADE;

create or replace function @extschema@.fn_etl_save_target_variable2topic
(
	_target_variable	integer,
	_topic				integer
)
returns integer
as
$$
declare
		_id	integer;
begin
		if _target_variable is null
		then
			raise exception 'Error 01: fn_etl_save_target_variable2topic: Input argument _target_variable must not be null!';
		end if;

		if _topic is null
		then
			raise exception 'Error 02: fn_etl_save_target_variable2topic: Input argument _topic must not be null!';
		end if;
	
		insert into @extschema@.cm_tvariable2topic(target_variable,topic)
		select _target_variable, _topic
		returning id
		into _id;
	
		return _id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_save_target_variable2topic(integer, integer) is
'Function saves new mapping between target variable and topic.';

grant execute on function @extschema@.fn_etl_save_target_variable2topic(integer, integer) to public;
-- </function>



-- <function name="fn_etl_import_variable" schema="extschema" src="functions/extschema/etl/fn_etl_import_variable.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_import_variable(json)

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_import_variable(json) CASCADE;

create or replace function @extschema@.fn_etl_import_variable
(
	_variables	json
)
returns text
as
$$
declare
		_max_id_tv		integer;
		_res			text;
begin
		if _variables is null
		then
			raise exception 'Error 01: fn_etl_import_variable: Input argument _variables must not by NULL!';
		end if;
	
		_max_id_tv := (select coalesce(max(id),0) from @extschema@.t_variable);

		with
		w1 as	(
				select json_array_elements(_variables) as s
				)
		,w2 as	(
				select
						(s->>'target_variable')::integer			as target_variable,
						(s->>'sub_population_category')::integer	as sub_population_category,
						(s->>'area_domain_category')::integer		as area_domain_category
				from w1
				)
		,w3 as	(
				select
						w2.target_variable,
						case when w2.sub_population_category = 0 then null::integer else w2.sub_population_category end as sub_population_category,
						case when w2.area_domain_category = 0 then null::integer else w2.area_domain_category end as area_domain_category,
						null::integer as auxiliary_variable_category
				from
						w2
				)
		,w4 as	(
				select
						target_variable,
						sub_population_category,
						area_domain_category ,
						null::integer as auxiliary_variable_category
				from
						@extschema@.t_variable where target_variable = (select distinct target_variable from w2)
				)
		,w5 as	(
				select w3.target_variable, w3.sub_population_category, w3.area_domain_category, w3.auxiliary_variable_category from w3 except
				select w4.target_variable, w4.sub_population_category, w4.area_domain_category, w4.auxiliary_variable_category from w4
				)
		insert into @extschema@.t_variable(target_variable, sub_population_category, area_domain_category, auxiliary_variable_category)
		select
				target_variable,
				sub_population_category,
				area_domain_category,
				auxiliary_variable_category
		from w5
		order by target_variable, sub_population_category, area_domain_category, auxiliary_variable_category;

		_res := concat('The ',(select count(*) from @extschema@.t_variable where id > _max_id_tv),' new rows were inserted into t_variable.');
	
		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_import_variable(json) IS
'The function solves ETL proces for t_variable table.';

grant execute on function @extschema@.fn_etl_import_variable(json) to public;
-- </function>



-- <function name="fn_etl_import_variable_hierarchy" schema="extschema" src="functions/extschema/etl/fn_etl_import_variable_hierarchy.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_import_variable_hierarchy(json)

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_import_variable_hierarchy(json) CASCADE;

create or replace function @extschema@.fn_etl_import_variable_hierarchy
(
	_variables	json
)
returns text
as
$$
declare
		_max_id_tvh		integer;
		_res			text;
begin
		if _variables is null
		then
			raise exception 'Error 01: fn_etl_import_variable_hierarchy: Input argument _variables must not by NULL!';
		end if;
	
		_max_id_tvh := (select coalesce(max(id),0) from @extschema@.t_variable_hierarchy);
	
		with
		w1 as	(
				select json_array_elements(_variables) as s
				)
		,w2 as	(
				select
						(s->>'target_variable')::integer					as target_variable,
						(s->>'sub_population_category')::integer			as sub_population_category,
						(s->>'area_domain_category')::integer				as area_domain_category,
						(s->>'sub_population_category_superior')::integer	as sub_population_category_superior,
						(s->>'area_domain_category_superior')::integer		as area_domain_category_superior
				from w1
				)
		,w3 as	(
				select
						id,
						target_variable,
						case when sub_population_category is null then 0 else sub_population_category end as sub_population_category,
						case when area_domain_category is null then 0 else area_domain_category end as area_domain_category
				from
						@extschema@.t_variable
				where
						target_variable = (select distinct target_variable from w2)
				)
		,w4 as	(
				select
						w2.*,
						w3.id as variable,
						w3_sup.id as variable_superior
				from
						w2
						
				inner join w3			on ((w2.sub_population_category = w3.sub_population_category) and (w2.area_domain_category = w3.area_domain_category))
				inner join w3 as w3_sup	on ((w2.sub_population_category_superior = w3_sup.sub_population_category) and (w2.area_domain_category_superior = w3_sup.area_domain_category))
				)
		,w5 as	(
				select w4.variable, w4.variable_superior from w4 except
				select tvh.variable, tvh.variable_superior from @extschema@.t_variable_hierarchy as tvh
				)
		insert into @extschema@.t_variable_hierarchy(variable,variable_superior)
		select variable, variable_superior from w5 order by variable, variable_superior;
				
		_res := concat('The ',(select count(*) from @extschema@.t_variable_hierarchy where id > _max_id_tvh),' new rows were inserted into t_variable_hierarchy.');
	
		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_import_variable_hierarchy(json) IS
'The function solves ETL proces for t_variable_hierarchy table.';

grant execute on function @extschema@.fn_etl_import_variable_hierarchy(json) to public;
-- </function>



-- <function name="fn_etl_import_ldsity_values" schema="extschema" src="functions/extschema/etl/fn_etl_import_ldsity_values.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_import_ldsity_values(json, double precision)

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_import_ldsity_values(json, double precision) CASCADE;

create or replace function @extschema@.fn_etl_import_ldsity_values
(
	_ldsity_values json,
	_threshold double precision default 0.000001
)
returns text
as
$$
declare
		_max_id_ttd						integer;
		_max_id_tad						integer;
		_target_variable				integer;
		_country						varchar;
		_panels							varchar[];
		_reference_year_sets			varchar[];
		_array_target_variable			integer[];
		_array_sub_population_category	integer[];
		_array_area_domain_category		integer[];
		_array_panel					varchar[];
		_array_reference_year_set		varchar[];
		_check_cmrys2pm					integer;
		_array_cmrys2pm					integer[];
		_res_ttd						text;
		_res_tad						text;
		_res							text;
begin
		if _ldsity_values is null
		then
			raise exception 'Error 01: fn_etl_import_ldsity_values: Input argument _ldsity_values must not by NULL!';
		end if;
	
		_max_id_ttd := (select coalesce(max(id),0) from @extschema@.t_target_data);
		_max_id_tad := (select coalesce(max(id),0) from @extschema@.t_available_datasets);

		-- check target variable --
		with
		w1 as	(select json_array_elements(_ldsity_values) as s)
		,w2 as	(select	(s->>'target_variable')::integer as target_variable	from w1)
		select distinct w2.target_variable from w2
		into _target_variable;

		if (select count(ctv.*) is distinct from 1 from @extschema@.c_target_variable as ctv where ctv.id = _target_variable)
		then
			raise exception 'Error 02: fn_etl_import_ldsity_values: Input JSON element "target_variable = %" is not present in table c_target_variable!',_target_variable;
		end if;

		-- check country --
		with
		w1 as	(select json_array_elements(_ldsity_values) as s)
		,w2 as	(select	(s->>'country')::varchar as country	from w1)
		select distinct w2.country from w2
		into _country;

		if (select count(cc.*) is distinct from 1 from sdesign.c_country as cc where cc.label = _country)
		then
			raise exception 'Error 03: fn_etl_import_ldsity_values: Input JSON element "country = %" is not present in table sdesign.c_country!',_country;
		end if;

		-- check panels --
		with
		w1 as	(select json_array_elements(_ldsity_values) as s)
		,w2 as	(select	(s->>'panel')::varchar as panel	from w1)
		,w3 as	(select distinct w2.panel from w2)
		select array_agg(w3.panel order by w3.panel) from w3
		into _panels;

		for i in 1..array_length(_panels,1)
		loop
			if	(
				select count(tp.*) is distinct from 1 from sdesign.t_panel as tp where tp.stratum in
				(select ts.id from sdesign.t_stratum as ts where ts.strata_set in
				(select tss.id from sdesign.t_strata_set as tss where tss.country = 
				(select cc.id from sdesign.c_country as cc where cc.label = _country)))
				and tp.panel = _panels[i]
				)
			then
				raise exception 'Error 04: fn_etl_import_ldsity_values: Some of input JSON element "panel = %" is not present in table sdesign.t_panel for input JSON element "country = %"!',_panel[i], _country;
			end if;		
		end loop;

		-- check reference year sets --
		with
		w1 as	(select json_array_elements(_ldsity_values) as s)
		,w2 as	(select	(s->>'reference_year_set')::varchar as reference_year_set from w1)
		,w3 as	(select distinct w2.reference_year_set from w2)
		select array_agg(w3.reference_year_set order by w3.reference_year_set) from w3
		into _reference_year_sets;

		for i in 1..array_length(_reference_year_sets,1)
		loop
			if	(
				select count(trys.*) is distinct from 1 from sdesign.t_reference_year_set as trys where trys.id in
				(select cmr.reference_year_set from sdesign.cm_refyearset2panel_mapping as cmr where cmr.panel in
				(select tp.id from sdesign.t_panel as tp where tp.stratum in
				(select ts.id from sdesign.t_stratum as ts where ts.strata_set in
				(select tss.id from sdesign.t_strata_set as tss where tss.country = 
				(select cc.id from sdesign.c_country as cc where cc.label = _country)))))
				and trys.reference_year_set = _reference_year_sets[i]
				)
			then
				raise exception 'Error 05: fn_etl_import_ldsity_values: Some of input JSON element "reference_year_set = %" is not present in table sdesign.t_reference_year_set for input JSON element "country = %"!',_reference_year_sets[i], _country;
			end if;		
		end loop;

		-- check combinations of target_variable, sub_population_category and area_domain_category
		with
		w1 as	(
				select json_array_elements(_ldsity_values) as s
				)
		,w2 as	(
				select
						(s->>'target_variable')::integer			as target_variable,
						(s->>'sub_population_category')::integer	as sub_population_category,
						(s->>'area_domain_category')::integer		as area_domain_category	
				from w1
				)
		,w3 as	(select distinct target_variable, sub_population_category, area_domain_category from w2)
		select
				array_agg(w3.target_variable order by w3.sub_population_category, w3.area_domain_category) as target_variable,
				array_agg(w3.sub_population_category order by w3.sub_population_category, w3.area_domain_category) as sub_population_category,
				array_agg(w3.area_domain_category order by w3.sub_population_category, w3.area_domain_category) as area_domain_category
		from
				w3
		into
				_array_target_variable,
				_array_sub_population_category,
				_array_area_domain_category;

		for i in 1..array_length(_array_sub_population_category,1)
		loop
			if	(
				with
				w1 as	(
						select
								id,
								target_variable,
								case when sub_population_category is null then 0 else sub_population_category end as sub_population_category,
								case when area_domain_category is null then 0 else area_domain_category end as area_domain_category
						from
								@extschema@.t_variable where target_variable = _array_target_variable[i]
						)
				select count(w1.*) is distinct from 1 from w1
				where w1.sub_population_category = _array_sub_population_category[i]
				and w1.area_domain_category = _array_area_domain_category[i]
				)
			then
				raise exception 'Error 06: fn_etl_import_ldsity_values: Some of input combination JSON elements "target_variable = %", "sub_population_category = %" and "area_domain_category = %" is not present in table t_variable!',_array_target_variable[i], _array_sub_population_category[i], _array_area_domain_category[i];
			end if;
		end loop;

		-- check combinations of panel and reference year set
		with
		w1 as	(
				select json_array_elements(_ldsity_values) as s
				)
		,w2 as	(
				select
						(s->>'panel')::varchar				as panel,
						(s->>'reference_year_set')::varchar	as reference_year_set
				from w1
				)
		,w3 as	(select distinct panel, reference_year_set from w2)
		select
				array_agg(w3.panel order by w3.panel, w3.reference_year_set) as panel,
				array_agg(w3.reference_year_set order by w3.panel, w3.reference_year_set) as reference_year_set
		from
				w3
		into
				_array_panel,
				_array_reference_year_set;

		for i in 1..array_length(_array_panel,1)
		loop
			select cmrys.id from sdesign.cm_refyearset2panel_mapping as cmrys
			where cmrys.panel =	(
							select tp.id from sdesign.t_panel as tp where tp.stratum in
							(select ts.id from sdesign.t_stratum as ts where ts.strata_set in
							(select tss.id from sdesign.t_strata_set as tss where tss.country = 
							(select cc.id from sdesign.c_country as cc where cc.label = _country)))
							and tp.panel = _array_panel[i]
							)
			and cmrys.reference_year_set =	(
											select trys.id from sdesign.t_reference_year_set as trys where trys.id in
											(select cmr.reference_year_set from sdesign.cm_refyearset2panel_mapping as cmr where cmr.panel in
											(select tp.id from sdesign.t_panel as tp where tp.stratum in
											(select ts.id from sdesign.t_stratum as ts where ts.strata_set in
											(select tss.id from sdesign.t_strata_set as tss where tss.country = 
											(select cc.id from sdesign.c_country as cc where cc.label = _country)))))
											and trys.reference_year_set = _array_reference_year_set[i]
											)
			into _check_cmrys2pm;

			if _check_cmrys2pm is null
			then
				raise exception 'Error 07: fn_etl_import_ldsity_values: Some of input combination JSON elements "panel = %" and "reference_year_set = %" is not present in table sdesign.cm_refyearset2panel_mapping!',_array_panel[i], _array_reference_year_set[i];
			end if;

			if i = 1
			then
				_array_cmrys2pm := array[_check_cmrys2pm];
			else
				_array_cmrys2pm := _array_cmrys2pm || array[_check_cmrys2pm];
			end if;
		end loop;

		-- RES QUERY --
		with
		w1 as	(
				select json_array_elements(_ldsity_values) as s
				)
		,w2 as	(
				select
						(s->>'target_variable')::integer			as target_variable,
						(s->>'plot')::integer						as plot,
						(s->>'value')::float						as value,
						(s->>'panel')::varchar						as panel,
						(s->>'reference_year_set')::varchar			as reference_year_set,
						(s->>'country')::varchar					as country,
						(s->>'sub_population_category')::integer	as sub_population_category,
						(s->>'area_domain_category')::integer		as area_domain_category	
				from w1
				)
		,w3 as	(
				select
						a.id,
						b.id as id_panel,
						b.panel as panel_label,
						c.id as id_reference_year_set,
						c.reference_year_set as reference_year_set_label
				from
							(select cmr2pm.* from sdesign.cm_refyearset2panel_mapping as cmr2pm where cmr2pm.id in (select unnest(_array_cmrys2pm))) as a
				inner join	sdesign.t_panel as b on a.panel = b.id
				inner join	sdesign.t_reference_year_set as c on a.reference_year_set = c.id
				)
		,w4 as	(
				select
						id,
						target_variable,
						case when sub_population_category is null then 0 else sub_population_category end as sub_population_category,
						case when area_domain_category is null then 0 else area_domain_category end as area_domain_category
				from
						@extschema@.t_variable where target_variable = (select distinct w2.target_variable from w2)
				)	
		,w5 as	(
				select
						w2.*,
						w3.id_panel,
						w3.id_reference_year_set,
						w4.id as id_variable
				from
							w2
				inner join	w3 on (w2.panel = w3.panel_label and w2.reference_year_set = w3.reference_year_set_label)
				inner join 	w4 on (w2.sub_population_category = w4.sub_population_category and w2.area_domain_category = w4.area_domain_category)
				)
		,w6 as	(		
				select distinct target_variable, plot, value, id_reference_year_set, id_variable from w5
				)				
		,w7 as	(
				select distinct id_panel as panel, id_reference_year_set as reference_year_set, id_variable as variable from w5
				except
				select panel, reference_year_set, variable from @extschema@.t_available_datasets
				)
		,w8 as	(
				insert into @extschema@.t_available_datasets(panel,reference_year_set,variable)
				select panel, reference_year_set, variable from w7
				order by panel, reference_year_set, variable
				)
		,w_ttd as	(
					select
							ttd.id,
							ttd.plot,
							ttd.value,
							ttd.reference_year_set,
							ttd.variable,
							coalesce(w6.plot,ttd.plot) as plot_upr,
							coalesce(w6.id_reference_year_set,ttd.reference_year_set) as reference_year_set_upr,
							coalesce(w6.id_variable,ttd.variable) as variable_upr,
							case
								when ttd.value is     null and w6.value is not null then w6.value
								when ttd.value is not null and ttd.value is distinct from 0.0 and w6.value is null then 0.0
								when ttd.value is not null and ttd.value = 0.0 and w6.value is null then null::double precision
								else w6.value
							end
								as value_upr,
							case
								when ttd.value is     null and w6.value is not null then false
								when ttd.value is not null and ttd.value is distinct from 0.0 and w6.value is null then false
								when ttd.value is not null and ttd.value = 0.0 and w6.value is null then null::boolean									
								else
									case
										when ttd.value = 0.0 and w6.value = 0.0 then true
										when ttd.value = 0.0 and w6.value is distinct from 0.0
												then
													case
														when (abs(1 - (ttd.value / w6.value)) * 100.0) <= _threshold then true
														else false
													end
										when ttd.value is distinct from 0.0 and w6.value = 0.0
												then
													case
														when (abs(1 - (w6.value / ttd.value)) * 100.0) <= _threshold then true
														else false
													end
										else
											case
												when (abs(1 - (ttd.value / w6.value)) * 100.0) <= _threshold then true
												else false
											end
									end									
							end
								as value_identic				
					from
						(
						select * from @extschema@.t_target_data
						where variable in	(
											select id from @extschema@.t_variable where target_variable = (select distinct w2.target_variable from w2)
											)
						and reference_year_set in (select w3.id_reference_year_set from w3)
						and plot in (select distinct w6.plot from w6)
						and is_latest = true
						) as ttd
					full outer join w6
							on (ttd.plot = w6.plot and ttd.reference_year_set = w6.id_reference_year_set and ttd.variable = w6.id_variable)
					)
		,w_update as	(
						update @extschema@.t_target_data set is_latest = false where id in
						(select id from w_ttd where value_identic = false and id is not null)
						returning t_target_data.plot, t_target_data.reference_year_set, t_target_data.variable
						)
		insert into @extschema@.t_target_data(plot,value,reference_year_set,variable,value_inserted,is_latest)
		select
				w_ttd.plot_upr as plot,
				w_ttd.value_upr as value,
				w_ttd.reference_year_set_upr as reference_year_set,
				w_ttd.variable_upr as variable,
				now() as value_inserted,
				true as is_latest
		from
				w_ttd
		where
				w_ttd.value_identic = false
		order
				by w_ttd.plot_upr, w_ttd.reference_year_set_upr, w_ttd.variable_upr;
			
	_res_ttd := concat('The ',(select count(*) from @extschema@.t_target_data where id > _max_id_ttd),' new local densities were inserted for ',(select count(t.plot) from (select distinct plot from @extschema@.t_target_data where id > _max_id_ttd) as t),' plots.');
	_res_tad := concat('The ',(select count(*) from @extschema@.t_available_datasets where id > _max_id_tad),' new rows were inserted into t_available_datasets.');
			
	_res := _res_ttd || ' ' || _res_tad;

	return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_import_ldsity_values(json, double precision) IS
'The function for the specified list of input arguments inserts data into the t_available_datasets table and inserts data into the t_target_data table (aggregated local density at the plot level).';

grant execute on function @extschema@.fn_etl_import_ldsity_values(json, double precision) to public;
-- </function>