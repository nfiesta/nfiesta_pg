--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

CREATE TABLE @extschema@.t_variable_hierarchy
(
	id		serial NOT NULL,
	var		integer NOT NULL,
	var_sup		integer,
	CONSTRAINT pkey__t_variable_hierarchy PRIMARY KEY (id),
	CONSTRAINT fkey__t_variable_hierarchy__variable FOREIGN KEY (var) REFERENCES @extschema@.t_variable (id),
	CONSTRAINT fkey__t_variable_hierarchy__variable_sup FOREIGN KEY (var_sup) REFERENCES @extschema@.t_variable (id)
);

COMMENT ON TABLE  @extschema@.t_variable_hierarchy IS 'Maping table for table f_a_cell. Storing hierarchy of cells.';
COMMENT ON COLUMN @extschema@.t_variable_hierarchy.var IS 'Foreign key to gid of f_a_cell.';
COMMENT ON COLUMN @extschema@.t_variable_hierarchy.var_sup IS 'Foreign key to gid of f_a_cell which is superior in hierarchy.';

SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_variable_hierarchy', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_variable_hierarchy_id_seq', '');

-- <view name="v_variable_hierarchy" schema="extschema" src="views/extschema/v_variable_hierarchy.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

CREATE VIEW @extschema@.v_variable_hierarchy AS
SELECT var_sup as node, array_agg(var order by var) AS edges
FROM @extschema@.t_variable_hierarchy
GROUP BY var_sup
ORDER BY var_sup;

-- </view>

-- <view name="v_add_plot_target_attr" schema="extschema" src="views/extschema/v_add_plot_target_attr.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

create view @extschema@.v_add_plot_target_attr as
with w_target_data_variable as not materialized (
	select t_target_data.*, t_variable.id as t_variable__id
	from @extschema@.t_target_data
	inner join @extschema@.t_variable on (t_target_data.variable = t_variable.id)
)
, w_node_sum as (
	select
		plot,
		w_target_data_variable.reference_year_set,
		w_target_data_variable.t_variable__id,
		value as node_sum
	from w_target_data_variable
	inner join @extschema@.v_variable_hierarchy on (v_variable_hierarchy.node = w_target_data_variable.t_variable__id)
)
, w_edge_sum as (
	select
		w_node_sum.plot,
		w_node_sum.reference_year_set,
		v_variable_hierarchy.node,
		w_node_sum.node_sum,
		v_variable_hierarchy.edges as edges_def,
		array_agg(w_target_data_variable.t_variable__id order by w_target_data_variable.t_variable__id) as edges_found,
		sum(w_target_data_variable.value) as edges_sum
	from w_node_sum
	inner join @extschema@.v_variable_hierarchy on (v_variable_hierarchy.node = w_node_sum.t_variable__id)
	left join w_target_data_variable on (
		w_target_data_variable.plot = w_node_sum.plot
		and w_target_data_variable.reference_year_set = w_node_sum.reference_year_set
		and w_target_data_variable.t_variable__id = any(v_variable_hierarchy.edges))
	group by w_node_sum.plot, w_node_sum.reference_year_set, v_variable_hierarchy.node, w_node_sum.node_sum, v_variable_hierarchy.edges
)
, w_diff as (
	select
		*,
		case
			when node_sum != 0.0 and edges_sum = 0.0 then 100.0
			when node_sum = 0.0 and edges_sum = 0.0 then 0
			else abs(1 - (node_sum / edges_sum )) * 100.0
		end as diff
	from w_edge_sum
)
select * from w_diff order by diff desc
;

-- </view>

-- <view name="v_add_plot_aux_attr" schema="extschema" src="views/extschema/v_add_plot_aux_attr.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

create view @extschema@.v_add_plot_aux_attr as
with w_auxiliary_data_variable as not materialized (
	select t_auxiliary_data.*, t_variable.id as t_variable__id
	from @extschema@.t_auxiliary_data
	inner join @extschema@.t_variable on (t_auxiliary_data.variable = t_variable.id)
)
, w_node_sum as (
	select
		plot,
		w_auxiliary_data_variable.t_variable__id,
		value as node_sum
	from w_auxiliary_data_variable
	inner join @extschema@.v_variable_hierarchy on (v_variable_hierarchy.node = w_auxiliary_data_variable.t_variable__id)
)
, w_edge_sum as (
	select
		w_node_sum.plot,
		v_variable_hierarchy.node,
		w_node_sum.node_sum,
		v_variable_hierarchy.edges as edges_def,
		array_agg(w_auxiliary_data_variable.t_variable__id order by w_auxiliary_data_variable.t_variable__id) as edges_found,
		sum(w_auxiliary_data_variable.value) as edges_sum
	from w_node_sum
	inner join @extschema@.v_variable_hierarchy on (v_variable_hierarchy.node = w_node_sum.t_variable__id)
	left join w_auxiliary_data_variable on (
		w_auxiliary_data_variable.plot = w_node_sum.plot
		and w_auxiliary_data_variable.t_variable__id = any(v_variable_hierarchy.edges))
	group by w_node_sum.plot, v_variable_hierarchy.node, w_node_sum.node_sum, v_variable_hierarchy.edges
)
, w_diff as (
	select
		*,
		case
			when node_sum != 0.0 and edges_sum = 0.0 then 100.0
			when node_sum = 0.0 and edges_sum = 0.0 then 0
			else abs(1 - (node_sum / edges_sum )) * 100.0
		end as diff
	from w_edge_sum
)
select * from w_diff order by diff desc
;

-- </view>

---------------------------------------------------------------------------------------------------

CREATE TABLE @extschema@.t_estimation_cell_hierarchy
(
	id		serial NOT NULL,
	cell		integer NOT NULL,
	cell_sup	integer,
	CONSTRAINT pkey__t_estimation_cell_hierarchy PRIMARY KEY (id),
	CONSTRAINT fkey__t_estimation_cell_hierarchy__c_estimation_cell FOREIGN KEY (cell) REFERENCES @extschema@.c_estimation_cell (id),
	CONSTRAINT fkey__t_estimation_cell_hierarchy_sup__c_estimation_cell FOREIGN KEY (cell_sup) REFERENCES @extschema@.c_estimation_cell (id)
);

COMMENT ON TABLE  @extschema@.t_estimation_cell_hierarchy IS 'Maping table for table c_estimation_cell. Storing hierarchy of cells.';
COMMENT ON COLUMN @extschema@.t_estimation_cell_hierarchy.cell IS 'Foreign key to gid of c_estimation_cell.';
COMMENT ON COLUMN @extschema@.t_estimation_cell_hierarchy.cell_sup IS 'Foreign key to gid of c_estimation_cell which is superior in hierarchy.';

SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_estimation_cell_hierarchy', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_estimation_cell_hierarchy_id_seq', '');

-- <view name="v_estimation_cell_hierarchy" schema="extschema" src="views/extschema/v_estimation_cell_hierarchy.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

CREATE VIEW @extschema@.v_estimation_cell_hierarchy AS
SELECT cell_sup as node, array_agg(cell order by cell) AS edges
FROM @extschema@.t_estimation_cell_hierarchy
GROUP BY cell_sup
ORDER BY cell_sup;

-- </view>

-- <view name="v_add_aux_total_attr" schema="extschema" src="views/extschema/v_add_aux_total_attr.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

create view @extschema@.v_add_aux_total_attr as
with w_aux_total_variable as not materialized (
	select t_aux_total.*, t_variable.id as t_variable__id
	from @extschema@.t_aux_total
	inner join @extschema@.t_variable on (t_aux_total.variable = t_variable.id)
)
, w_node_sum as (
	select
		estimation_cell,
		w_aux_total_variable.t_variable__id,
		sum(aux_total) as node_sum
	from w_aux_total_variable
	inner join @extschema@.v_variable_hierarchy on (v_variable_hierarchy.node = w_aux_total_variable.t_variable__id)
	group by estimation_cell, w_aux_total_variable.t_variable__id
)
, w_edge_sum as (
	select
		w_node_sum.estimation_cell,
		v_variable_hierarchy.node,
		w_node_sum.node_sum,
		v_variable_hierarchy.edges as edges_def,
		array_agg(w_aux_total_variable.t_variable__id order by w_aux_total_variable.t_variable__id) as edges_found,
		sum(w_aux_total_variable.aux_total) as edges_sum

	from w_node_sum
	inner join @extschema@.v_variable_hierarchy on (v_variable_hierarchy.node = w_node_sum.t_variable__id)
	left join w_aux_total_variable on (
		w_aux_total_variable.estimation_cell = w_node_sum.estimation_cell
		and w_aux_total_variable.t_variable__id = any(v_variable_hierarchy.edges))
	group by w_node_sum.estimation_cell, v_variable_hierarchy.node, w_node_sum.node_sum, v_variable_hierarchy.edges
)
, w_diff as (
	select
		*,
		case
			when node_sum != 0.0 and edges_sum = 0.0 then 100.0
			when node_sum = 0.0 and edges_sum = 0.0 then 0
			else abs(1 - (node_sum / edges_sum )) * 100.0
		end as diff
	from w_edge_sum
)
select * from w_diff order by diff desc
;

-- </view>

-- <view name="v_add_aux_total_geo" schema="extschema" src="views/extschema/v_add_aux_total_geo.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

create view @extschema@.v_add_aux_total_geo as
with w_aux_total_est_cell as not materialized (
	select t_aux_total.*, c_estimation_cell.id as c_estimation_cell__id
	from @extschema@.t_aux_total
	inner join @extschema@.c_estimation_cell on (t_aux_total.estimation_cell = c_estimation_cell.id)
)
, w_node_sum as (
	select
		w_aux_total_est_cell.estimation_cell, w_aux_total_est_cell.variable,
		sum(aux_total) as node_sum
	from w_aux_total_est_cell
	inner join @extschema@.v_estimation_cell_hierarchy on (v_estimation_cell_hierarchy.node = w_aux_total_est_cell.c_estimation_cell__id)
	group by w_aux_total_est_cell.estimation_cell, w_aux_total_est_cell.variable
	order by w_aux_total_est_cell.estimation_cell, w_aux_total_est_cell.variable
)
, w_edge_sum as (
	select
		w_node_sum.estimation_cell, w_node_sum.variable,
		v_estimation_cell_hierarchy.node,
		w_node_sum.node_sum,
		v_estimation_cell_hierarchy.edges as edges_def,
		array_agg(w_aux_total_est_cell.c_estimation_cell__id order by w_aux_total_est_cell.c_estimation_cell__id) as edges_found,
		sum(w_aux_total_est_cell.aux_total) as edges_sum
	from w_node_sum
	inner join @extschema@.v_estimation_cell_hierarchy on (v_estimation_cell_hierarchy.node = w_node_sum.estimation_cell)
	left join w_aux_total_est_cell on (
		w_aux_total_est_cell.variable = w_node_sum.variable
		and w_aux_total_est_cell.c_estimation_cell__id = any(v_estimation_cell_hierarchy.edges))
	group by w_node_sum.estimation_cell, w_node_sum.variable, v_estimation_cell_hierarchy.node, w_node_sum.node_sum, v_estimation_cell_hierarchy.edges
)
, w_diff as (
	select
		*,
		case
			when node_sum != 0.0 and edges_sum = 0.0 then 100.0
			when node_sum = 0.0 and edges_sum = 0.0 then 0
			else abs(1 - (node_sum / edges_sum )) * 100.0
		end as diff
	from w_edge_sum
)
select * from w_diff order by diff desc
;

-- </view>

-- <view name="v_add_res_total_attr" schema="extschema" src="views/extschema/v_add_res_total_attr.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

create view @extschema@.v_add_res_total_attr as
with w_result_variable as not materialized (
	select
		t_result.point,
		t_total_estimate_conf.estimation_cell,
		t_total_estimate_conf.target_variable as t_variable__id,
		t_total_estimate_conf.aux_conf, force_synthetic
	from @extschema@.t_result
	inner join @extschema@.t_estimate_conf ON t_estimate_conf.id = t_result.estimate_conf
	inner join @extschema@.t_total_estimate_conf ON t_total_estimate_conf.id = t_estimate_conf.total_estimate_conf
	where t_estimate_conf.estimate_type = 1
)
, w_node_sum as (
	select
		estimation_cell,
		w_result_variable.t_variable__id, aux_conf, force_synthetic,
		sum(point) as node_sum
	from w_result_variable
	inner join @extschema@.v_variable_hierarchy on (v_variable_hierarchy.node = w_result_variable.t_variable__id)
	group by estimation_cell, w_result_variable.t_variable__id, aux_conf, force_synthetic
	order by estimation_cell, w_result_variable.t_variable__id, aux_conf, force_synthetic
)
, w_edge_sum as (
	select
		w_node_sum.estimation_cell, w_node_sum.aux_conf, w_node_sum.force_synthetic,
		v_variable_hierarchy.node,
		w_node_sum.node_sum,
		v_variable_hierarchy.edges as edges_def,
		array_agg(w_result_variable.t_variable__id order by w_result_variable.t_variable__id) as edges_found,
		sum(w_result_variable.point) as edges_sum
	from w_node_sum
	inner join @extschema@.v_variable_hierarchy on (v_variable_hierarchy.node = w_node_sum.t_variable__id)
	left join w_result_variable on (
		w_result_variable.estimation_cell = w_node_sum.estimation_cell
		and case when
			w_result_variable.aux_conf is null and w_node_sum.aux_conf is null then true
			else w_result_variable.aux_conf = w_node_sum.aux_conf end
		and case when
			w_result_variable.force_synthetic is null and w_node_sum.force_synthetic is null then true
			else w_result_variable.force_synthetic = w_node_sum.force_synthetic end
		and w_result_variable.t_variable__id = any(v_variable_hierarchy.edges))
	group by w_node_sum.estimation_cell, w_node_sum.aux_conf, w_node_sum.force_synthetic, v_variable_hierarchy.node, w_node_sum.node_sum, v_variable_hierarchy.edges
)
, w_diff as (
	select
		*,
		case
			when node_sum != 0.0 and edges_sum = 0.0 then 100.0
			when node_sum = 0.0 and edges_sum = 0.0 then 0
			else abs(1 - (node_sum / edges_sum )) * 100.0
		end as diff
	from w_edge_sum
)
select * from w_diff order by diff desc
;

-- </view>

-- <view name="v_add_res_total_geo" schema="extschema" src="views/extschema/v_add_res_total_geo.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

create view @extschema@.v_add_res_total_geo as
with w_result_est_cell as not materialized (
	select
		t_result.point,
		t_total_estimate_conf.estimation_cell,
		t_total_estimate_conf.target_variable as t_variable__id,
		t_total_estimate_conf.aux_conf, force_synthetic
	from @extschema@.t_result
	inner join @extschema@.t_estimate_conf ON t_estimate_conf.id = t_result.estimate_conf
	inner join @extschema@.t_total_estimate_conf ON t_total_estimate_conf.id = t_estimate_conf.total_estimate_conf
	where t_estimate_conf.estimate_type = 1
)
, w_node_sum as (
	select
		estimation_cell,
		w_result_est_cell.t_variable__id, aux_conf, force_synthetic,
		sum(point) as node_sum
	from w_result_est_cell
	inner join @extschema@.v_estimation_cell_hierarchy on (v_estimation_cell_hierarchy.node = w_result_est_cell.estimation_cell)
	group by estimation_cell, w_result_est_cell.t_variable__id, aux_conf, force_synthetic
	order by estimation_cell, w_result_est_cell.t_variable__id, aux_conf, force_synthetic
)
, w_edge_sum as (
	select
		w_node_sum.estimation_cell, w_node_sum.t_variable__id as variable, w_node_sum.aux_conf, w_node_sum.force_synthetic,
		v_estimation_cell_hierarchy.node,
		w_node_sum.node_sum,
		v_estimation_cell_hierarchy.edges as edges_def,
		array_agg(w_result_est_cell.estimation_cell order by w_result_est_cell.estimation_cell) as edges_found,
		sum(w_result_est_cell.point) as edges_sum
	from w_node_sum
	inner join @extschema@.v_estimation_cell_hierarchy on (v_estimation_cell_hierarchy.node = w_node_sum.estimation_cell)
	left join w_result_est_cell on (
		w_result_est_cell.t_variable__id = w_node_sum.t_variable__id
		and case when
			w_result_est_cell.aux_conf is null and w_node_sum.aux_conf is null then true
			else w_result_est_cell.aux_conf = w_node_sum.aux_conf end
		and case when
			w_result_est_cell.force_synthetic is null and w_node_sum.force_synthetic is null then true
			else w_result_est_cell.force_synthetic = w_node_sum.force_synthetic end
		and w_result_est_cell.estimation_cell = any(v_estimation_cell_hierarchy.edges))
	group by w_node_sum.estimation_cell, w_node_sum.t_variable__id, w_node_sum.aux_conf, w_node_sum.force_synthetic, v_estimation_cell_hierarchy.node, w_node_sum.node_sum, v_estimation_cell_hierarchy.edges
)
, w_diff as (
	select
		*,
		case
			when node_sum != 0.0 and edges_sum = 0.0 then 100.0
			when node_sum = 0.0 and edges_sum = 0.0 then 0
			else abs(1 - (node_sum / edges_sum )) * 100.0
		end as diff
	from w_edge_sum
)
select * from w_diff order by diff desc
;

-- </view>

-- <view name="v_add_res_ratio_attr" schema="extschema" src="views/extschema/v_add_res_ratio_attr.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

create view @extschema@.v_add_res_ratio_attr as
with w_result_variable as not materialized (
	select
		t_result.point,
		t_total_estimate_conf.estimation_cell,
		t_total_estimate_conf.target_variable as t_variable__id,
		t_total_estimate_conf.aux_conf, force_synthetic, t_estimate_conf.denominator
	from @extschema@.t_result
	inner join @extschema@.t_estimate_conf ON t_estimate_conf.id = t_result.estimate_conf
	inner join @extschema@.t_total_estimate_conf ON t_total_estimate_conf.id = t_estimate_conf.total_estimate_conf
	where t_estimate_conf.estimate_type = 2
)
, w_node_sum as (
	select
		estimation_cell,
		w_result_variable.t_variable__id, aux_conf, force_synthetic, denominator,
		sum(point) as node_sum
	from w_result_variable
	inner join @extschema@.v_variable_hierarchy on (v_variable_hierarchy.node = w_result_variable.t_variable__id)
	group by estimation_cell, w_result_variable.t_variable__id, aux_conf, force_synthetic, denominator
	order by estimation_cell, w_result_variable.t_variable__id, aux_conf, force_synthetic, denominator
)
, w_edge_sum as (
	select
		w_node_sum.estimation_cell, w_node_sum.aux_conf, w_node_sum.force_synthetic, w_node_sum.denominator,
		v_variable_hierarchy.node,
		w_node_sum.node_sum,
		v_variable_hierarchy.edges as edges_def,
		array_agg(w_result_variable.t_variable__id order by w_result_variable.t_variable__id) as edges_found,
		sum(w_result_variable.point) as edges_sum
	from w_node_sum
	inner join @extschema@.v_variable_hierarchy on (v_variable_hierarchy.node = w_node_sum.t_variable__id)
	left join w_result_variable on (
		w_result_variable.estimation_cell = w_node_sum.estimation_cell
		and case when
			w_result_variable.aux_conf is null and w_node_sum.aux_conf is null then true
			else w_result_variable.aux_conf = w_node_sum.aux_conf end
		and case when
			w_result_variable.force_synthetic is null and w_node_sum.force_synthetic is null then true
			else w_result_variable.force_synthetic = w_node_sum.force_synthetic end
		and w_result_variable.denominator = w_node_sum.denominator
		and w_result_variable.t_variable__id = any(v_variable_hierarchy.edges))
	group by w_node_sum.estimation_cell, w_node_sum.aux_conf, w_node_sum.force_synthetic, w_node_sum.denominator, v_variable_hierarchy.node, w_node_sum.node_sum, v_variable_hierarchy.edges
)
, w_diff as (
	select
		*,
		case
			when node_sum != 0.0 and edges_sum = 0.0 then 100.0
			when node_sum = 0.0 and edges_sum = 0.0 then 0
			else abs(1 - (node_sum / edges_sum )) * 100.0
		end as diff
	from w_edge_sum
)
select * from w_diff order by diff desc
;

-- </view>
