EXTENSION = nfiesta # the extensions name
MODULEDIR = nfiesta_pg_dir

DATA =	nfiesta--2.0.4.sql \
	nfiesta--2.0.4--2.0.5.sql \
	nfiesta--2.0.5--2.0.6.sql \
	nfiesta--2.0.6--2.0.7.sql \
	nfiesta--2.0.7--2.0.8.sql \
	nfiesta--2.0.8--2.0.9.sql \
	nfiesta--2.0.9--2.0.10.sql \
	nfiesta--2.0.10--2.0.11.sql \
	nfiesta--2.0.11--2.0.12.sql \
	nfiesta--2.0.12--2.0.13.sql \
	nfiesta--2.0.13--2.0.14.sql \
	nfiesta--2.0.14--2.0.15.sql \
	nfiesta--2.0.15--2.0.16.sql \
	nfiesta--2.0.16--2.0.17.sql \
	nfiesta--2.0.17--2.0.18.sql \
	nfiesta--2.0.18--2.0.19.sql \
	nfiesta--2.0.19--2.1.0.sql \
	nfiesta--2.1.0--2.1.1.sql \
	nfiesta--2.1.1--2.1.2.sql \
	nfiesta--2.1.2--2.1.3.sql \
	nfiesta--2.1.3--2.1.4.sql \
	nfiesta--2.1.4--2.2.0.sql \
	nfiesta--2.2.0--2.2.1.sql \
	nfiesta--2.2.1--2.2.2.sql \
	nfiesta--2.2.2--2.2.3.sql \
	nfiesta--2.2.3--2.2.4.sql \
	nfiesta--2.2.4--2.2.5.sql \
	nfiesta--2.2.5--2.2.6.sql \
	nfiesta--2.2.6--2.2.7.sql \
	nfiesta--2.2.7--2.2.8.sql \
	nfiesta--2.2.8--2.2.9.sql \
	nfiesta--2.2.9--2.2.10.sql \
	nfiesta--2.2.10--2.2.11.sql \
	nfiesta--2.2.11--2.2.12.sql \
	nfiesta--2.2.12--2.2.13.sql \
	nfiesta--2.2.13--2.3.0.sql \
	nfiesta--2.3.0--2.3.1.sql \
	nfiesta--2.3.1--2.3.2.sql \
	nfiesta--2.3.2--2.4.0.sql \
	nfiesta--2.4.0--2.4.1.sql \
	nfiesta--2.4.1--2.4.2.sql \
	nfiesta--2.4.2--2.4.3.sql \
	nfiesta--2.4.3--2.4.4.sql \
	nfiesta--2.4.4--2.4.5.sql \
	nfiesta--2.4.5--2.4.6.sql \
	nfiesta--2.4.6--2.4.7.sql \
	nfiesta--2.4.7--2.4.8.sql \
	nfiesta--2.4.8--2.4.9.sql \
	nfiesta--2.4.9--2.4.9.1.sql \
	nfiesta--2.4.9.1--2.4.9.sql \
	nfiesta--2.4.9--2.4.10.sql \
	nfiesta--2.4.10--2.4.11.sql \
	nfiesta--2.4.11--2.5.0.sql \
	nfiesta--2.5.0--2.5.1.sql \
	nfiesta--2.5.1--2.5.2.sql \
	nfiesta--2.5.2--2.5.3.sql \
	nfiesta--2.5.3--2.5.4.sql \
	nfiesta--2.5.4--2.5.5.sql \
	nfiesta--2.5.5--2.5.6.sql \
	nfiesta--2.5.6--2.5.7.sql \
	nfiesta--2.5.7--2.5.8.sql \
	nfiesta--2.5.8--2.5.9.sql \
	nfiesta--2.5.9--2.6.0.sql \
	nfiesta--2.6.0--2.6.1.sql \
	nfiesta--2.6.1--2.6.2.sql \
	nfiesta--2.6.2--2.6.3.sql \
	nfiesta--2.6.3--2.6.4.sql \
	nfiesta--2.6.4--2.6.5.sql \
	nfiesta--2.6.5--2.6.6.sql \
	nfiesta--2.6.6--2.6.7.sql \
	nfiesta--2.6.7--2.6.8.sql \
	nfiesta--2.6.8--2.6.9.sql \
	nfiesta--2.6.9--2.6.10.sql \
	nfiesta--2.6.10--2.6.11.sql \
	nfiesta--2.6.11--2.6.12.sql \
	nfiesta--2.6.12--2.6.13.sql \
	nfiesta--2.6.13--2.6.14.sql \
	nfiesta--2.6.14--2.6.15.sql \
	nfiesta--2.6.15--2.6.16.sql \
	nfiesta--2.6.16--2.6.17.sql \
	nfiesta--2.6.17--2.6.18.sql \
	nfiesta--2.6.18--2.6.19.sql \
	nfiesta--2.6.19--2.6.20.sql \
	nfiesta--2.6.20--2.7.0.sql \
	nfiesta--2.7.0--2.8.0.sql \
	nfiesta--2.8.0--2.8.1.sql \
	nfiesta--2.8.1--2.8.2.sql \
	nfiesta--2.8.2--2.8.3.sql \
	nfiesta--2.8.3--2.9.0.sql \
	nfiesta--2.9.0--2.9.1.sql \
	nfiesta--2.9.1--2.10.0.sql \
	nfiesta--2.10.0--2.10.1.sql \
	nfiesta--2.10.1--2.10.2.sql \
	nfiesta--2.10.2--2.11.0.sql \
	nfiesta--2.11.0--2.11.1.sql \
	nfiesta--2.11.1--2.12.0.sql \
	nfiesta--2.12.0--3.0.0.sql \
	nfiesta--3.0.0--3.0.1.sql \
	nfiesta--3.0.1--3.0.2.sql \
	nfiesta--3.0.2--3.0.3.sql \
	nfiesta--3.0.3--3.0.4.sql \
	nfiesta--3.0.4--3.0.5.sql \
	nfiesta--3.0.5--3.0.6.sql \
	nfiesta--3.0.6--3.0.7.sql \
	nfiesta--3.0.7--3.0.8.sql \
	nfiesta--3.0.8--3.0.9.sql \
	nfiesta--3.0.9--3.0.10.sql \
	nfiesta--3.0.10--3.0.11.sql \
	nfiesta--3.0.11--3.0.12.sql \
	nfiesta--3.0.12--3.0.13.sql \
	nfiesta--3.0.13--3.0.14.sql \
	nfiesta--3.0.14--3.1.0.sql \
	nfiesta--3.1.0--3.1.1.sql \
	nfiesta--3.1.1--3.1.2.sql \
	nfiesta--3.1.2--3.1.3.sql \
	nfiesta--3.1.3--3.1.4.sql \
	nfiesta--3.1.4--3.1.5.sql \
	nfiesta--3.1.5--3.1.6.sql \
	nfiesta--3.1.6--3.1.7.sql \
	nfiesta--3.1.7--3.1.8.sql \
	nfiesta--3.1.8--3.1.9.sql \
	nfiesta--3.1.9--3.2.0.sql \
	nfiesta--3.2.0--3.2.1.sql \
	nfiesta--3.2.1--3.2.2.sql \
	nfiesta--3.2.2--3.2.3.sql \
	nfiesta--3.2.3--3.2.4.sql \
	nfiesta--3.2.4--3.2.5.sql \
	nfiesta--3.2.5--3.2.6.sql \
	nfiesta--3.2.6--3.2.7.sql \
	nfiesta--3.2.7--3.2.8.sql \
	nfiesta--3.2.8--3.2.9.sql \
	nfiesta--3.2.9--3.2.10.sql \
	nfiesta--3.2.10--3.2.11.sql \
	nfiesta--3.2.11--3.2.12.sql \
	nfiesta--3.2.12--3.3.0.sql \
	nfiesta--3.3.0--3.4.0.sql \
	nfiesta--3.4.0--3.5.0.sql \
	nfiesta--3.5.0--3.5.1.sql \
	nfiesta--3.5.1--3.5.2.sql \
	nfiesta--3.5.2--3.5.3.sql \
	nfiesta--3.5.3--3.5.4.sql \
	nfiesta--3.5.4--3.6.0.sql \
	nfiesta--3.6.0--3.7.0.sql \
	nfiesta--3.7.0--3.8.0.sql \
	nfiesta--3.8.0--3.9.0.sql \
	nfiesta--3.9.0--3.9.1.sql \
	nfiesta--3.9.1--3.9.2.sql \
	nfiesta--3.9.2--3.9.3.sql \
	nfiesta--3.9.3--3.9.4.sql \
	nfiesta--3.9.4--3.9.5.sql \
	nfiesta--3.9.5--3.9.6.sql \
	nfiesta--3.9.6--3.9.7.sql \
	nfiesta--3.9.7--3.9.8.sql \
	nfiesta--3.9.8--3.9.9.sql \
	nfiesta--3.9.9--3.9.10.sql \
	nfiesta--3.9.10--3.10.0.sql \
	nfiesta--3.10.0--3.10.1.sql \
	nfiesta--3.10.1--3.11.0.sql \
	nfiesta--3.11.0--3.11.1.sql \
	nfiesta--3.11.1--3.12.0.sql \
	nfiesta--3.12.0--3.12.1.sql \
	nfiesta--3.12.1--3.13.0.sql \
	nfiesta--3.13.0--3.13.1.sql \
	nfiesta--3.13.1--3.13.2.sql \
	nfiesta--3.13.2--3.13.3.sql \
	nfiesta--3.13.3--3.14.0.sql \
	nfiesta--3.14.0--3.14.1.sql \
	nfiesta--3.14.1--3.15.0.sql \
	nfiesta--3.15.0--3.15.1.sql \
	nfiesta--3.15.1--3.15.2.sql \
	nfiesta--3.15.2--3.15.3.sql \
	nfiesta--3.15.3--3.15.4.sql \
	nfiesta--3.15.4--3.15.5.sql	\
	nfiesta--3.15.5--3.16.0.sql \
	nfiesta--3.16.0--3.16.1.sql \
	nfiesta--3.16.1--3.17.0.sql \
	nfiesta--3.17.0--3.17.1.sql \
	nfiesta--3.17.1--3.18.0.sql

REGRESS = install_test so_version fn_inverse fn_system_utilization;

export SRC_DIR := $(shell pwd)

all:

installcheck-all:
	make installcheck REGRESS_OPTS="--dbname=contrib_regression_nfiesta"
	make installcheck-sdesign
	make installcheck-sdesign-cz
	make installcheck-data
	make installcheck-data-gen
	make csv-export
	make installcheck-upd
	make installcheck-fst-1p
	make installcheck-fst-2p
	make installcheck-rpa
	make installcheck-fst-add
	make installcheck-etl
	make installcheck-gregmap-config-api
	make installcheck-update-panel-group
	make installcheck-manage-estimation-period

installcheck-sdesign:
	psql -d postgres -c "drop database if exists contrib_regression_nfiesta_sdesign;" -c "create database contrib_regression_nfiesta_sdesign template contrib_regression_nfiesta;"
	cd deps/nfiesta_sdesign && make installcheck REGRESS_OPTS="--use-existing --dbname=contrib_regression_nfiesta_sdesign" REGRESS="sdesign4nfiesta/csv sdesign4nfiesta/fn_import_data sdesign4nfiesta/check_data sdesign4nfiesta/check_triggers"

installcheck-sdesign-cz:
	psql -d postgres -c "drop database if exists contrib_regression_nfiesta_sdesign_cz;" -c "create database contrib_regression_nfiesta_sdesign_cz template contrib_regression_nfiesta;"
	cd deps/nfiesta_sdesign && make installcheck REGRESS_OPTS="--use-existing --dbname=contrib_regression_nfiesta_sdesign_cz" REGRESS="csv fn_import_data check_data check_triggers"	

installcheck-data:
	psql -d postgres -c "drop database if exists contrib_regression_nfiesta_data;" -c "create database contrib_regression_nfiesta_data template contrib_regression_nfiesta_sdesign;"
	make installcheck REGRESS_OPTS="--use-existing --dbname=contrib_regression_nfiesta_data" REGRESS="fst_data fst_conf fst_get_num_denom fst_analyze"

installcheck-data-gen:
	psql -d postgres -c "drop database if exists contrib_regression_nfiesta_data_gen;" -c "create database contrib_regression_nfiesta_data_gen template contrib_regression_nfiesta_data;"
	make installcheck REGRESS_OPTS="--use-existing --dbname=contrib_regression_nfiesta_data_gen" REGRESS="fst_data_generations fst_analyze"

installcheck-upd:
	psql -d postgres -c "drop database if exists contrib_regression_nfiesta_upd;" -c "create database contrib_regression_nfiesta_upd template contrib_regression_nfiesta_data_gen;"
	make installcheck REGRESS_OPTS="--use-existing --dbname=contrib_regression_nfiesta_upd" REGRESS="fst_update"

installcheck-fst-1p:
	psql -d postgres -c "drop database if exists contrib_regression_nfiesta_fst_1p;" -c "create database contrib_regression_nfiesta_fst_1p template contrib_regression_nfiesta_upd;"
	make installcheck REGRESS_OPTS="--use-existing --dbname=contrib_regression_nfiesta_fst_1p" REGRESS="fst_1p_est_conf fst_1p_v_conf_overview fst_1p_data fst_1p_total fst_1p_ratio"

installcheck-fst-2p:
	psql -d postgres -c "drop database if exists contrib_regression_nfiesta_fst_2p;" -c "create database contrib_regression_nfiesta_fst_2p template contrib_regression_nfiesta_fst_1p;"
	make installcheck REGRESS_OPTS="--use-existing --dbname=contrib_regression_nfiesta_fst_2p" REGRESS="fst_2p_gbeta fst_2p_est_conf fst_2p_v_conf_overview fst_2p_data fst_2p_total fst_2p_ratio"

installcheck-rpa:
	psql -d postgres -c "drop database if exists contrib_regression_nfiesta_rpa;" -c "create database contrib_regression_nfiesta_rpa template contrib_regression_nfiesta_fst_2p;"
	make installcheck REGRESS_OPTS="--use-existing --dbname=contrib_regression_nfiesta_rpa" REGRESS="fst_remove_param_area"

installcheck-fst-add:
	psql -d postgres -c "drop database if exists contrib_regression_nfiesta_fst_add;" -c "create database contrib_regression_nfiesta_fst_add template contrib_regression_nfiesta_fst_2p;"
	make installcheck REGRESS_OPTS="--use-existing --dbname=contrib_regression_nfiesta_fst_add" REGRESS="fst_result_generations fst_add_plot_level fst_add_aux_total fst_add_result"

installcheck-etl:
	psql -d postgres -c "drop database if exists contrib_regression_nfiesta_etl;" -c "create database contrib_regression_nfiesta_etl template contrib_regression_nfiesta_data_gen;"
	make installcheck REGRESS_OPTS="--use-existing --dbname=contrib_regression_nfiesta_etl" REGRESS="etl_fce"
	
installcheck-gregmap-config-api:
	psql -d postgres -c "drop database if exists contrib_regression_nfiesta_gregmap_total_config_api;" -c "drop database if exists contrib_regression_nfiesta_gregmap_ratio_config_api;" -c "drop database if exists contrib_regression_nfiesta_gregmap_config_api;" -c "create database contrib_regression_nfiesta_gregmap_config_api template contrib_regression_nfiesta_fst_2p;"
	make installcheck REGRESS_OPTS="--use-existing --dbname=contrib_regression_nfiesta_gregmap_config_api" REGRESS="fst_gregmap_total_config_api fst_gregmap_ratio_config_api fst_single_phase_multistrata_config_api"

installcheck-update-panel-group:
	psql -d postgres -c "drop database if exists contrib_regression_nfiesta_update_panel_group;" -c "create database contrib_regression_nfiesta_update_panel_group template contrib_regression_nfiesta_gregmap_config_api;"
	make installcheck REGRESS_OPTS="--use-existing --dbname=contrib_regression_nfiesta_update_panel_group" REGRESS="fst_update_panel_refyearset_group"

installcheck-manage-estimation-period:
	psql -d postgres -c "drop database if exists contrib_regression_nfiesta_manage_estimation_period;" -c "create database contrib_regression_nfiesta_manage_estimation_period template contrib_regression_nfiesta_gregmap_config_api;"
	make installcheck REGRESS_OPTS="--use-existing --dbname=contrib_regression_nfiesta_manage_estimation_period" REGRESS="fst_manage_estimation_period"

csv-export:
	psql --quiet --no-psqlrc contrib_regression_nfiesta_data -f sql/csv/create_csv.sql && \
	cp sql/csv/plot_auxiliary_data.csv sql/csv/plot_auxiliary_data_before.csv && \
	cp sql/csv/plot_target_data.csv sql/csv/plot_target_data_before.csv && \
	psql --quiet --no-psqlrc contrib_regression_nfiesta_data_gen -f sql/csv/create_csv.sql
	git diff --quiet sql/csv/*.csv

purge-db:
	psql -d postgres -c "drop database if exists contrib_regression_nfiesta;"
	psql -d postgres -c "drop database if exists contrib_regression_nfiesta_sdesign;"
	psql -d postgres -c "drop database if exists contrib_regression_nfiesta_sdesign_cz;"
	psql -d postgres -c "drop database if exists contrib_regression_nfiesta_data;"
	psql -d postgres -c "drop database if exists contrib_regression_nfiesta_data_gen;"
	psql -d postgres -c "drop database if exists contrib_regression_nfiesta_upd;"
	psql -d postgres -c "drop database if exists contrib_regression_nfiesta_fst_1p;"
	psql -d postgres -c "drop database if exists contrib_regression_nfiesta_fst_2p;"
	psql -d postgres -c "drop database if exists contrib_regression_nfiesta_rpa;"
	psql -d postgres -c "drop database if exists contrib_regression_nfiesta_fst_add;"
	psql -d postgres -c "drop database if exists contrib_regression_nfiesta_etl;"
	psql -d postgres -c "drop database if exists contrib_regression_nfiesta_api_regrest;"
	psql -d postgres -c "drop database if exists contrib_regression_nfiesta_gregmap_config_api;"

PG_MAJOR_VERSION := $(shell pg_config --version | sed -E 's/PostgreSQL ([0-9]+).*/\1/')
purge:
	rm -rf /usr/share/postgresql/$(PG_MAJOR_VERSION)/extension/nfiesta.control /usr/share/postgresql/$(PG_MAJOR_VERSION)/$(MODULEDIR)

# postgres build stuff
PG_CONFIG = pg_config
PGXS := $(shell $(PG_CONFIG) --pgxs)
include $(PGXS)
