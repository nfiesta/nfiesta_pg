--
-- Copyright 2017, 2023 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- <function name="fn_1p_data" schema="extschema" src="functions/extschema/fn_1p_data.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_1p_data(integer)

-- DROP FUNCTION @extschema@.fn_1p_data(integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_1p_data(
    IN conf_id integer
)
  RETURNS TABLE(
	gid bigint,
	cluster integer,
	attribute integer,
	stratum integer,
	plots_per_cluster integer,
	plcount bigint,
	cluster_is_in_cell boolean,
	ldsity_d double precision,
	ldsity_d_plus double precision,
	is_aux boolean,
	is_target boolean,
	geom geometry(MultiPoint),
	ldsity_res_d double precision,
	ldsity_res_d_plus double precision,
	pix double precision,
	sweight double precision,
	DELTA_T__G_beta double precision,
	nb_sampling_units integer,
	sweight_strata_sum double precision,
	lambda_d_plus double precision
) AS
$BODY$
DECLARE
	_complete_query text;
BEGIN
	--------------------------------QUERY--------------------------------
	_complete_query := '
WITH
---------------------------------------------------------
-- Data block
---------------------------------------------------------
w_ldsity_plot AS MATERIALIZED (
	select
		f_p_plot.gid,
		t_total_estimate_conf.id as conf_id,
		t_panel.stratum,
		t_cluster.id as cluster,
		t_panel_refyearset_group.reference_year_set,
		t_panel_refyearset_group.panel,
		cm_cluster2panel_mapping.sampling_weight_ha as sampling_weight,
		t_total_estimate_conf.variable as attribute,
		plots_per_cluster,
		true AS plot_is_in_cell,
		coalesce(tdads.value, 0) as ldsity,
		f_p_plot.geom
	from @extschema@.t_total_estimate_conf
	inner join @extschema@.c_panel_refyearset_group on t_total_estimate_conf.panel_refyearset_group = c_panel_refyearset_group.id
	inner join @extschema@.t_panel_refyearset_group on t_panel_refyearset_group.panel_refyearset_group = c_panel_refyearset_group.id
	inner join sdesign.t_panel ON t_panel.id = t_panel_refyearset_group.panel
	inner join sdesign.cm_cluster2panel_mapping ON cm_cluster2panel_mapping.panel = t_panel.id
	inner join sdesign.t_cluster ON t_cluster.id = cm_cluster2panel_mapping."cluster"
	inner join sdesign.f_p_plot ON f_p_plot."cluster" = t_cluster.id
	inner join sdesign.cm_plot2cluster_config_mapping on f_p_plot.gid = cm_plot2cluster_config_mapping.plot
	inner join sdesign.t_cluster_configuration ON (t_panel.cluster_configuration = t_cluster_configuration.id
											and cm_plot2cluster_config_mapping.cluster_configuration = t_cluster_configuration.id)
	inner join sdesign.t_stratum ON t_panel.stratum = t_stratum.id
	inner join @extschema@.cm_plot2cell_mapping ON (cm_plot2cell_mapping.estimation_cell = t_total_estimate_conf.estimation_cell
											and cm_plot2cell_mapping.plot = f_p_plot.gid)
	left join (	select 
				plot, reference_year_set, variable, value, is_latest
			from @extschema@.t_target_data
			inner join @extschema@.t_available_datasets on (t_target_data.available_datasets = t_available_datasets.id)
		) as tdads on (
			f_p_plot.gid = tdads.plot and
			t_panel_refyearset_group.reference_year_set = tdads.reference_year_set and
			t_total_estimate_conf.variable = tdads.variable and
			tdads.is_latest
		)
	where t_total_estimate_conf.id = ' || conf_id || '
)
, w_ldsity_cluster AS MATERIALIZED (
	SELECT
		w_ldsity_plot.conf_id,
		concat(w_ldsity_plot.conf_id, ROW_NUMBER() OVER ())::bigint as gid,
		w_ldsity_plot.cluster,
		w_ldsity_plot.attribute,
		w_ldsity_plot.stratum,
                w_ldsity_plot.panel,
		w_ldsity_plot.plots_per_cluster,
		w_ldsity_plot.sampling_weight,
		count(*) as plcount,
		sum(w_ldsity_plot.plot_is_in_cell::int) > 0 as cluster_is_in_cell,
		sum(w_ldsity_plot.ldsity * plot_is_in_cell::int)/w_ldsity_plot.plots_per_cluster AS ldsity_D,
		sum(w_ldsity_plot.ldsity)/w_ldsity_plot.plots_per_cluster AS ldsity_D_plus,
		st_collect(w_ldsity_plot.geom)::geometry(MultiPoint) as geom
	FROM       w_ldsity_plot
	GROUP BY conf_id, stratum, panel, cluster, sampling_weight, plots_per_cluster, attribute
	ORDER BY stratum, cluster, attribute
)
, w_strata_sum AS MATERIALIZED (
	select
		t.conf_id,
		t_panel.stratum,
		case when t_cluster_configuration.cluster_design then
			t_cluster_configuration.frame_area_ha
		else
			t_stratum.area_ha
		end as lambda_d_plus,
		sum(t_panel.cluster_count) as nb_sampling_units,
		sum(t_panel.sweight_panel_sum) as sweight_strata_sum
	from sdesign.t_stratum
	inner join sdesign.t_panel on (t_panel.stratum = t_stratum.id)
	inner join sdesign.t_cluster_configuration on (t_panel.cluster_configuration = t_cluster_configuration.id)
	inner join (select distinct conf_id, panel from w_ldsity_plot) as t on (t_panel.id = t.panel)
	group by conf_id, t_panel.stratum, lambda_d_plus
)
, w_1p_data AS MATERIALIZED (
	select
		w_ldsity_cluster.gid, w_ldsity_cluster.cluster,
		w_ldsity_cluster.attribute, w_ldsity_cluster.stratum, w_ldsity_cluster.plots_per_cluster,
		w_ldsity_cluster.plcount, w_ldsity_cluster.cluster_is_in_cell, w_ldsity_cluster.ldsity_d,
		w_ldsity_cluster.ldsity_d_plus, false::boolean as is_aux, true::boolean as is_target,
		w_ldsity_cluster.geom,
		NULL::double precision as ldsity_res_D, NULL::double precision as ldsity_res_D_plus,
		NULL::double precision as pix,  w_ldsity_cluster.sampling_weight as sweight, NULL::double precision as DELTA_T__G_beta,
		w_strata_sum.nb_sampling_units::integer, w_strata_sum.sweight_strata_sum, w_strata_sum.lambda_d_plus
	from w_ldsity_cluster
	INNER JOIN w_strata_sum ON w_ldsity_cluster.stratum = w_strata_sum.stratum
)
select * from w_1p_data;
';

	--RAISE NOTICE '%', _complete_query;
	RETURN QUERY EXECUTE _complete_query;
END;
$BODY$
  LANGUAGE plpgsql STABLE
  COST 100
  ROWS 100000;

COMMENT ON FUNCTION @extschema@.fn_1p_data(integer) IS 'Function preparing data for regression estimate.';

-- </function>

-- <function name="fn_2p_data" schema="extschema" src="functions/extschema/fn_2p_data.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_2p_data(integer)

-- DROP FUNCTION @extschema@.fn_2p_data(integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_2p_data(
    IN conf_id integer
)
  RETURNS TABLE(
	gid bigint,
	cluster integer,
	attribute integer,
	stratum integer,
	plots_per_cluster integer,
	plcount bigint,
	cluster_is_in_cell boolean,
	ldsity_d double precision,
	ldsity_d_plus double precision,
	is_aux boolean,
	is_target boolean,
	geom geometry(MultiPoint),
	ldsity_res_d double precision,
	ldsity_res_d_plus double precision,
	pix double precision,
	sweight double precision,
	DELTA_T__G_beta double precision,
	nb_sampling_units integer,
	sweight_strata_sum double precision,
	lambda_d_plus double precision,
	sigma boolean
) AS
$$
begin
--------------------------------QUERY--------------------------------
---------------------------------------------------------
-- Configuration block
---------------------------------------------------------
execute '
create temporary table w_configuration ON COMMIT DROP AS (
	with w_a AS NOT MATERIALIZED (
		select
			t_total_estimate_conf.estimation_cell, t_aux_conf.model, 
			t_aux_conf.sigma, t_total_estimate_conf.force_synthetic, t_aux_conf.param_area,
			array_agg(t_total_estimate_conf.id order by t_total_estimate_conf.variable) as t_total_estimate_conf__id,
			array_agg(t_total_estimate_conf.panel_refyearset_group order by t_total_estimate_conf.variable) as t_total_estimate_conf__panel_refyearset_group,
			array_agg(t_aux_conf.id order by t_total_estimate_conf.variable) as t_aux_conf__id,
			array_agg(t_total_estimate_conf.variable order by t_total_estimate_conf.variable) as target_attributes,
			t_total_estimate_conf.aux_conf
		from @extschema@.t_total_estimate_conf
		inner join @extschema@.t_aux_conf ON t_aux_conf.id = t_total_estimate_conf.aux_conf
		where t_total_estimate_conf.id = $1
		group by t_total_estimate_conf.estimation_cell, t_aux_conf.model, t_aux_conf.sigma, t_total_estimate_conf.force_synthetic,
			t_aux_conf.param_area, t_total_estimate_conf.aux_conf
	)
	select
		t_total_estimate_conf__id[1] as id,
		t_total_estimate_conf__panel_refyearset_group[1] AS panel_refyearset_group,
		t_total_estimate_conf__panel_refyearset_group,
		w_a.t_total_estimate_conf__id, w_a.t_aux_conf__id,
		w_a.param_area, w_a.estimation_cell as cell, w_a.model, w_a.sigma, w_a.force_synthetic,
		array_agg(t_model_variables.variable order by t_model_variables.variable) as aux_attributes, w_a.target_attributes
	from w_a
	inner join @extschema@.t_aux_conf ON t_aux_conf.id = w_a.aux_conf
	inner join @extschema@.t_model ON t_model.id = t_aux_conf.model
	inner join @extschema@.t_model_variables ON t_model_variables.model = t_model.id
	group by w_a.estimation_cell, w_a.model, w_a.sigma, w_a.force_synthetic, w_a.param_area, w_a.target_attributes, 
		w_a.t_total_estimate_conf__id, w_a.t_total_estimate_conf__panel_refyearset_group, w_a.t_aux_conf__id
	order by id limit 1
);' using fn_2p_data.conf_id;
analyze w_configuration;

execute '
create temporary table w_cell_selection ON COMMIT DROP AS (
	SELECT
		w_configuration.id as conf_id,
		estimation_cell as estimation_cell,
		geom
	FROM @extschema@.f_a_cell
	INNER JOIN w_configuration ON (w_configuration.cell = f_a_cell.estimation_cell)
);';
analyze w_cell_selection;

execute '
create temporary table w_plot ON COMMIT DROP as (
WITH
w_param_area_selection AS NOT MATERIALIZED (
	SELECT
		w_configuration.id as conf_id,
		gid as param_area_gid,
		geom
	FROM @extschema@.f_a_param_area
	INNER JOIN w_configuration ON (w_configuration.param_area = f_a_param_area.gid)
)
---------------------------------------------------------
-- Data block
---------------------------------------------------------
, w_plot AS NOT MATERIALIZED (
	select distinct
		f_p_plot.gid, w_configuration.id as conf_id, t_cluster.id as cluster, t_panel.stratum,
		case when w_configuration.force_synthetic = True then False else cm_plot2cell_mapping.id IS NOT NULL end AS plot_is_in_cell, 
		-- t_panel2aux_conf.reference_year_set as reference_year_set, t_panel2aux_conf.panel as panel,
		t_panel_refyearset_group.reference_year_set as reference_year_set, t_panel_refyearset_group.panel as panel,
		f_p_plot.geom
	from w_configuration
        inner join @extschema@.t_panel_refyearset_group on w_configuration.panel_refyearset_group = t_panel_refyearset_group.panel_refyearset_group
	--inner join @extschema@.t_panel2aux_conf on w_configuration.t_aux_conf__id[1] = t_panel2aux_conf.aux_conf
	inner join sdesign.t_panel ON (t_panel.id = t_panel_refyearset_group.panel)
	inner join sdesign.cm_cluster2panel_mapping ON cm_cluster2panel_mapping.panel = t_panel.id
	inner join sdesign.t_cluster ON t_cluster.id = cm_cluster2panel_mapping."cluster"
	inner join sdesign.f_p_plot ON f_p_plot."cluster" = t_cluster.id
	left join @extschema@.cm_plot2cell_mapping ON (cm_plot2cell_mapping.estimation_cell = w_configuration.cell and cm_plot2cell_mapping.plot = f_p_plot.gid)
	inner join @extschema@.cm_plot2param_area_mapping ON (cm_plot2param_area_mapping.param_area = w_configuration.param_area and cm_plot2param_area_mapping.plot = f_p_plot.gid)
)
select * from w_plot
);';
analyze w_plot;

execute '
create temporary table w_ldsity_plot ON COMMIT DROP as (
	with w_plot AS NOT MATERIALIZED (select * from w_plot)
		select
			w_plot.gid,
			w_plot.conf_id,
			w_plot.stratum,
			w_plot.panel,
			w_plot.cluster,
			t_variable.id as attribute,
			t_cluster_configuration.plots_per_cluster,
			w_plot.plot_is_in_cell,
			coalesce(adads.value, 0) as ldsity,
			true as is_aux, false as is_target,
			w_plot.geom
		from w_plot as w_plot
		inner join sdesign.cm_plot2cluster_config_mapping on w_plot.gid = cm_plot2cluster_config_mapping.plot
		inner join sdesign.t_cluster_configuration on cm_plot2cluster_config_mapping.cluster_configuration = t_cluster_configuration.id
		inner join w_configuration on w_plot.conf_id = w_configuration.id
		inner join @extschema@.t_variable on t_variable.id = ANY (w_configuration.aux_attributes)
		left join (	select
				plot, variable, value, is_latest
			from @extschema@.t_auxiliary_data 
			inner join @extschema@.t_available_datasets on (t_auxiliary_data.available_datasets = t_available_datasets.id)
		) as adads
		on (
			w_plot.gid = adads.plot and
			t_variable.id = adads.variable and
			adads.is_latest)
	union all
		select
			w_plot.gid,
			w_plot.conf_id,
			w_plot.stratum,
			w_plot.panel,
			w_plot.cluster,
			t_variable.id as attribute,
			t_cluster_configuration.plots_per_cluster,
			w_plot.plot_is_in_cell,
			coalesce(tdads.value, 0) as ldsity,
			false as is_aux, true as is_target,
			w_plot.geom
		from w_plot as w_plot
		inner join sdesign.cm_plot2cluster_config_mapping on w_plot.gid = cm_plot2cluster_config_mapping.plot
		inner join sdesign.t_cluster_configuration on cm_plot2cluster_config_mapping.cluster_configuration = t_cluster_configuration.id
		inner join w_configuration on w_plot.conf_id = w_configuration.id
                inner join @extschema@.t_variable on (t_variable.id = ANY (w_configuration.target_attributes))
		left join (	select 
				plot, reference_year_set, variable, value, is_latest 
			from @extschema@.t_target_data 
			inner join @extschema@.t_available_datasets on (t_target_data.available_datasets = t_available_datasets.id)
		) as tdads
		on (
			w_plot.gid = tdads.plot and
			w_plot.reference_year_set = tdads.reference_year_set and
			t_variable.id = tdads.variable and
			tdads.is_latest)
);';
analyze w_ldsity_plot;

execute '
create temporary table w_ldsity_cluster ON COMMIT DROP as (
	SELECT
		w_ldsity_plot.conf_id,
		concat(w_ldsity_plot.conf_id, ROW_NUMBER() OVER ())::bigint as gid,
		w_ldsity_plot.cluster,
		w_ldsity_plot.attribute,
		w_ldsity_plot.stratum,
		w_ldsity_plot.panel,
		w_ldsity_plot.plots_per_cluster,
		count(*) as plcount,
		sum(w_ldsity_plot.plot_is_in_cell::int) > 0 as cluster_is_in_cell,
		sum(w_ldsity_plot.ldsity * plot_is_in_cell::int)/w_ldsity_plot.plots_per_cluster AS ldsity_D,
		sum(w_ldsity_plot.ldsity)/w_ldsity_plot.plots_per_cluster AS ldsity_D_plus,
		w_ldsity_plot.is_aux, w_ldsity_plot.is_target,
		st_collect(w_ldsity_plot.geom)::geometry(MultiPoint) as geom
	FROM       w_ldsity_plot
	GROUP BY conf_id, stratum, panel, cluster, plots_per_cluster, attribute, is_aux, is_target
	ORDER BY stratum, cluster, attribute
);';
analyze w_ldsity_cluster;

execute '
create temporary table w_clusters ON COMMIT DROP as (
	select distinct conf_id, stratum, panel, cluster, cluster_is_in_cell from w_ldsity_cluster
);';
analyze w_clusters;

execute '
create temporary table w_strata_sum ON COMMIT DROP as (
	select
		w_configuration.id as conf_id,
                t_stratum.id as f_a_sampling_stratum_gid,
		case when t_cluster_configuration.cluster_design then
			t_cluster_configuration.frame_area_ha
		else
			t_stratum.area_ha
		end as lambda_d_plus,
		plots_per_cluster,
		sum(t_panel.cluster_count) as nb_sampling_units,
		sum(t_panel.sweight_panel_sum) as sweight_strata_sum
	from w_configuration
	inner join @extschema@.t_aux_conf on t_aux_conf.id = ANY (w_configuration.t_aux_conf__id)
	inner join @extschema@.t_panel_refyearset_group ON t_panel_refyearset_group.panel_refyearset_group = t_aux_conf.panel_refyearset_group
	inner join sdesign.t_panel ON t_panel.id = t_panel_refyearset_group.panel
	inner join sdesign.t_cluster_configuration ON t_panel.cluster_configuration = t_cluster_configuration.id
	inner join sdesign.t_stratum ON t_stratum.id = t_panel.stratum
	group by w_configuration.id, f_a_sampling_stratum_gid, lambda_d_plus, plots_per_cluster
);';
analyze w_strata_sum;
---------------------------------------------------------
-- Matrix block
---------------------------------------------------------
execute '
create temporary table w_X ON COMMIT DROP AS (
	SELECT
		conf_id,
		attribute AS r,
		cluster AS c,
		ldsity_D as val_D,
		ldsity_D_plus as val
	FROM w_ldsity_cluster
	WHERE is_aux
);';
analyze w_X;

execute '
create temporary table w_Y ON COMMIT DROP AS (
	SELECT
		conf_id,
		attribute AS r,
		cluster AS c,
		ldsity_D as val_D,
		ldsity_D_plus as val
	FROM w_ldsity_cluster
	WHERE is_target
);';
analyze w_Y;

execute '
create temporary table w_pix ON COMMIT DROP as (
with
w_pix AS NOT MATERIALIZED (-------------------------INCLUSION DENSITY PIX
	SELECT
		w_clusters.conf_id,
		w_clusters.stratum,
		w_clusters.cluster,
		1.0 / (cm_cluster2panel_mapping.sampling_weight_ha * (w_strata_sum.lambda_d_plus / w_strata_sum.sweight_strata_sum)) as pix,
		cm_cluster2panel_mapping.sampling_weight_ha as sweight_ha
	FROM w_clusters
	INNER JOIN sdesign.cm_cluster2panel_mapping ON (w_clusters.cluster = cm_cluster2panel_mapping.cluster
								and w_clusters.panel = cm_cluster2panel_mapping.panel)
	INNER JOIN w_strata_sum as w_strata_sum ON w_clusters.stratum = w_strata_sum.f_a_sampling_stratum_gid
)
select * from w_pix
);';
analyze w_pix;

execute '
create temporary table w_t_G_beta ON COMMIT DROP as (
        select
                w_configuration.id as conf_id,
                t_g_beta.variable as r, t_g_beta.cluster as c, t_g_beta.val
        from @extschema@.t_g_beta
        inner join w_configuration on (t_g_beta.aux_conf = w_configuration.t_aux_conf__id[1])
	where t_g_beta.is_latest
);';
analyze w_t_G_beta;

execute '
create temporary table w_PI ON COMMIT DROP as (
	SELECT
		conf_id,
		1 as r,
		cluster as c,
		1.0 / pix as val
	from w_pix
	order by r, c
);';
analyze w_PI;

execute '
create temporary table w_DELTA_G_beta ON COMMIT DROP as (
with w_I AS NOT MATERIALIZED (
	SELECT 
		conf_id,
		1 AS r,
		cluster AS c,
		cluster_is_in_cell::int AS val
	FROM	w_clusters
	order by r, c
)
, w_SIGMA AS NOT MATERIALIZED (
	SELECT
		conf_id,
		1 as r,
		cluster as c,
		case when w_configuration.sigma then (plots_per_cluster^2)::float / plcount::float 
		else 1::float end
		as val
	from (select conf_id, cluster, plots_per_cluster, plcount 
		from w_ldsity_cluster 
		group by conf_id, cluster, plots_per_cluster, plcount) as m
	inner join w_configuration on (m.conf_id = w_configuration.id)
	order by r, c
)
, w_SIGMA_PI AS NOT MATERIALIZED (
	SELECT
		A.conf_id,
		1 as r,
		A.c as c,
		A.val * B.val as val
	from w_SIGMA as A 
	inner join w_PI as B on (A.c = B.c and A.conf_id = B.conf_id) 
	order by r, c
)
, w_t_hat AS NOT MATERIALIZED (  -- eq. 18
	select 
		A.conf_id,
		A.r,
		1 AS c,
		sum(A.val_D * B.val * C.val) as val
	from w_X 		AS A 
	inner join w_PI     	AS B 	on (A.c = B.c and A.conf_id = B.conf_id)
	inner join w_I		AS C 	on (A.c = C.c and A.conf_id = C.conf_id)
	group by A.r, A.conf_id
	order by A.r
)
, w_t AS NOT MATERIALIZED (
	with w_aux_total as (
		select estimation_cell as cell, t_variable.id as attribute, aux_total
		from @extschema@.t_aux_total
		inner join @extschema@.t_variable on (t_aux_total.variable = t_variable.id)
		where t_aux_total.is_latest
	)
	, w_aux_ldsity as (
		select distinct conf_id, r as attribute from w_X
	)
	select
		w_cell_selection.conf_id,
		w_aux_total.attribute as r,
		1 as c,
		coalesce(w_aux_total.aux_total,
			@extschema@.fn_raise_notice(
				format(''fn_2p_data.w_t: t_aux_total not found! (conf_id:%s attribute:%s)'',
					w_cell_selection.conf_id,
					w_aux_ldsity.attribute),
				''exception''
			)::int::float
		) as val
	from
	w_aux_ldsity
	inner join w_cell_selection on (w_aux_ldsity.conf_id = w_cell_selection.conf_id)
	left join w_aux_total on (w_aux_ldsity.attribute = w_aux_total.attribute and w_cell_selection.estimation_cell = w_aux_total.cell)
)
, w_DELTA_T AS NOT MATERIALIZED (
	select
		conf_id,
		c as r, 
		r as c, 
		w_t.val - w_t_hat.val as val 
	from w_t 
	inner join w_t_hat using (r, c, conf_id)
)
, w_DELTA_G_beta AS NOT MATERIALIZED (
	select
		A.conf_id,
		A.r,
		B.c, 
		sum(A.val * B.val) as val
	FROM 	w_DELTA_T as A
	inner join w_t_G_beta as B ON (A.c = B.r and A.conf_id = B.conf_id)
	GROUP BY A.r, B.c, A.conf_id
	order by r, c
)
select * from w_DELTA_G_beta
);';
analyze w_DELTA_G_beta;

execute '
create temporary table w_X_beta ON COMMIT DROP as (
with w_Y_T AS NOT MATERIALIZED (
	SELECT
		conf_id,
		c as r,
		r as c,
		val,
		val_D -- used for 1p estimete
	from w_Y
	order by r, c
)
, w_G_PI AS NOT MATERIALIZED (
	select 
		A.conf_id,
		A.r, 
		A.c AS c,
		A.val * B.val as val
	from w_t_G_beta AS A, w_PI AS B 
	where A.c = B.c and A.conf_id = B.conf_id
	order by r, c
)
, w_beta_hat AS NOT MATERIALIZED (
	select
		A.conf_id,
		A.r,
		B.c, 
		sum(A.val * B.val) as val
	FROM w_G_PI as A, w_Y_T as B
	WHERE A.c = B.r and A.conf_id = B.conf_id
	GROUP BY A.r, B.c, A.conf_id
	order by r, c
)
, w_X_plots_T AS NOT MATERIALIZED (
	SELECT conf_id, gid AS r, cluster, attribute AS c, plots_per_cluster, ldsity AS val 
	FROM w_ldsity_plot WHERE is_aux
)
, w_X_beta AS NOT MATERIALIZED (
	select
		A.conf_id,
		A.r,
		B.c, 
		sum(A.val * B.val) as val
	FROM w_X_plots_T as A, w_beta_hat as B
	WHERE A.c = B.r and A.conf_id = B.conf_id
	GROUP BY A.r, B.c, A.conf_id
	order by r, c 
)
select * from w_X_beta
);';
analyze w_X_beta;

execute '
create temporary table w_ldsity_residuals_cluster ON COMMIT DROP as (
with w_Y_plots_T AS NOT MATERIALIZED (
	SELECT conf_id, gid AS r, cluster, attribute AS c, plots_per_cluster, ldsity AS val 
	FROM w_ldsity_plot WHERE is_target
)
, w_residuals_plot AS NOT MATERIALIZED ( -------------------------RESIDUAL LOCAL DENSITY ON TRACT LEVEL
	select
		w_Y_plots_T.conf_id,
		w_Y_plots_T.r AS plot, 
		w_Y_plots_T.c AS attribute, 
		(w_Y_plots_T.val - w_X_beta.val) AS ldsity 
	from w_Y_plots_T 
	INNER JOIN w_X_beta USING (r, c, conf_id)
)
, w_residuals_cluster AS NOT MATERIALIZED (
	with
	w_residuals_plot AS NOT MATERIALIZED (select * from w_residuals_plot),
	w_ldsity_plot AS NOT MATERIALIZED (select * from w_ldsity_plot)
	SELECT
		w_ldsity_plot.conf_id,
		w_ldsity_plot.cluster,
		w_residuals_plot.attribute,
		w_ldsity_plot.stratum,
		w_ldsity_plot.plots_per_cluster,
		count(*) as plcount,
		sum(plot_is_in_cell::int) > 0 as cluster_is_in_cell,
		sum(w_residuals_plot.ldsity * plot_is_in_cell::int)/w_ldsity_plot.plots_per_cluster AS ldsity_res_D, --eq 38
		sum(w_residuals_plot.ldsity)/w_ldsity_plot.plots_per_cluster AS ldsity_res_D_plus -- eq 39
	FROM w_ldsity_plot AS w_ldsity_plot
	INNER JOIN w_residuals_plot AS w_residuals_plot ON w_ldsity_plot.gid = w_residuals_plot.plot AND w_ldsity_plot.attribute = w_residuals_plot.attribute and w_ldsity_plot.conf_id = w_residuals_plot.conf_id
	GROUP BY stratum, w_ldsity_plot.conf_id, w_ldsity_plot.cluster, w_ldsity_plot.plots_per_cluster, w_residuals_plot.attribute
	ORDER BY stratum, w_ldsity_plot.conf_id, w_ldsity_plot.cluster, w_residuals_plot.attribute
)
, w_ldsity_residuals_cluster AS NOT MATERIALIZED (
	select 
		w_ldsity_cluster.conf_id, w_ldsity_cluster.gid, w_ldsity_cluster.cluster, 
		w_ldsity_cluster.attribute, w_ldsity_cluster.stratum, w_ldsity_cluster.plots_per_cluster, 
		w_ldsity_cluster.plcount, w_ldsity_cluster.cluster_is_in_cell, w_ldsity_cluster.ldsity_d, w_ldsity_cluster.ldsity_d_plus, 
		w_ldsity_cluster.is_aux, w_ldsity_cluster.is_target, w_ldsity_cluster.geom,
		w_residuals_cluster.ldsity_res_D, w_residuals_cluster.ldsity_res_D_plus
	from w_ldsity_cluster
	LEFT JOIN w_residuals_cluster USING (conf_id, cluster, attribute, stratum)
)
select * from w_ldsity_residuals_cluster
);';
analyze w_ldsity_residuals_cluster;

--EXPLAIN (ANALYZE, COSTS, VERBOSE, BUFFERS/*, FORMAT JSON*/)
return query execute '
with w_2p_data AS NOT MATERIALIZED (
	with w_ldsity_residuals_cluster AS NOT MATERIALIZED (select * from w_ldsity_residuals_cluster)
	select 
		w_ldsity_residuals_cluster.conf_id, w_ldsity_residuals_cluster.gid, w_ldsity_residuals_cluster.cluster, 
		w_ldsity_residuals_cluster.attribute, w_ldsity_residuals_cluster.stratum, w_ldsity_residuals_cluster.plots_per_cluster, 
		w_ldsity_residuals_cluster.plcount, w_ldsity_residuals_cluster.cluster_is_in_cell, w_ldsity_residuals_cluster.ldsity_d, 
		w_ldsity_residuals_cluster.ldsity_d_plus, 
		w_ldsity_residuals_cluster.is_aux, w_ldsity_residuals_cluster.is_target, w_ldsity_residuals_cluster.geom,
		w_ldsity_residuals_cluster.ldsity_res_D, w_ldsity_residuals_cluster.ldsity_res_D_plus,
		w_pix.pix, w_pix.sweight_ha, w_DELTA_G_beta.val as DELTA_T__G_beta, 
		w_strata_sum.nb_sampling_units::integer, w_strata_sum.sweight_strata_sum, w_strata_sum.lambda_d_plus
	from w_ldsity_residuals_cluster
	INNER JOIN w_pix USING (conf_id, cluster, stratum)
	INNER JOIN w_DELTA_G_beta ON (w_pix.conf_id = w_DELTA_G_beta.conf_id and cluster = w_DELTA_G_beta.c)
	inner join w_strata_sum on (stratum = f_a_sampling_stratum_gid)
)
, w_2p_data_sigma AS NOT MATERIALIZED (
	select 
		w_2p_data.gid, w_2p_data.cluster, w_2p_data.attribute, w_2p_data.stratum, w_2p_data.plots_per_cluster, w_2p_data.plcount,
		w_2p_data.cluster_is_in_cell as cluster_is_in_cell, 
		w_2p_data.ldsity_d, w_2p_data.ldsity_d_plus, w_2p_data.is_aux, w_2p_data.is_target,
		w_2p_data.geom, w_2p_data.ldsity_res_d, w_2p_data.ldsity_res_d_plus, w_2p_data.pix, w_2p_data.sweight_ha as sweight, w_2p_data.DELTA_T__G_beta,
		w_2p_data.nb_sampling_units, w_2p_data.sweight_strata_sum, w_2p_data.lambda_d_plus, w_configuration.sigma
	from w_2p_data 
	inner join w_configuration on (w_2p_data.conf_id = w_configuration.id)
)
select * from w_2p_data_sigma;';

drop table w_configuration;
drop table w_cell_selection;
drop table w_plot;
drop table w_ldsity_plot;
drop table w_ldsity_cluster;
drop table w_clusters;
drop table w_strata_sum;
drop table w_X;
drop table w_Y;
drop table w_pix;
drop table w_t_G_beta;
drop table w_PI;
drop table w_DELTA_G_beta;
drop table w_X_beta;
drop table w_ldsity_residuals_cluster;
end;
$$
  LANGUAGE plpgsql
  COST 100
  ROWS 100000;

COMMENT ON FUNCTION @extschema@.fn_2p_data(integer) IS 'Function preparing data for regression estimate.';

-- </function>

-- <function name="fn_g_beta" schema="extschema" src="functions/extschema/fn_g_beta.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_G_beta(integer)

-- DROP FUNCTION @extschema@.fn_G_beta(integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_G_beta(
    IN conf_id integer
)
  RETURNS TABLE(
	variable integer,
	cluster integer,
	val double precision
) AS
$BODY$
DECLARE
	_complete_query text;
BEGIN
	--------------------------------QUERY--------------------------------
	_complete_query := '
WITH
w_configuration AS MATERIALIZED (
	select
		t_aux_conf.id, t_aux_conf.param_area, t_aux_conf.model, t_aux_conf.sigma, t_model.description,
		array_agg(t_model_variables.variable order by t_model_variables.variable) as aux_attributes,
		t_aux_conf.panel_refyearset_group
	from @extschema@.t_aux_conf
	inner join @extschema@.t_model ON t_model.id = t_aux_conf.model
	inner join @extschema@.t_model_variables ON t_model_variables.model = t_model.id
        where t_aux_conf.id = ' || conf_id || '
	group by t_aux_conf.id, t_aux_conf.param_area, t_aux_conf.model, t_model.description
)
, w_param_area_selection AS MATERIALIZED (
	SELECT
		w_configuration.id as conf_id,
		gid as param_area_gid,
		geom,
		panel_refyearset_group
	FROM @extschema@.f_a_param_area
	INNER JOIN w_configuration ON (w_configuration.param_area = f_a_param_area.gid)
)
, w_plot AS MATERIALIZED (-------------------------LIST OF PLOTS IN PARAMETRIZATION AREA
	select distinct
		w_param_area_selection.conf_id, f_p_plot.gid, t_cluster.id as cluster, t_panel.stratum,
		t_panel_refyearset_group.panel,
		f_p_plot.geom
	from w_param_area_selection
	inner join @extschema@.t_panel_refyearset_group on w_param_area_selection.panel_refyearset_group = t_panel_refyearset_group.panel_refyearset_group
	inner join sdesign.t_panel ON t_panel.id = t_panel_refyearset_group.panel
	inner join sdesign.cm_cluster2panel_mapping ON cm_cluster2panel_mapping.panel = t_panel.id
	inner join sdesign.t_cluster ON t_cluster.id = cm_cluster2panel_mapping."cluster"
	inner join sdesign.f_p_plot ON f_p_plot."cluster" = t_cluster.id
	inner join @extschema@.cm_plot2param_area_mapping ON cm_plot2param_area_mapping.plot = f_p_plot.gid
	inner join @extschema@.f_a_param_area ON (f_a_param_area.gid = cm_plot2param_area_mapping.param_area and w_param_area_selection.param_area_gid = f_a_param_area.gid)
)
, w_ldsity_plot AS MATERIALIZED (
	SELECT
		w_plot.conf_id,
		w_plot.gid,
		w_plot.stratum,
                w_plot.panel,
		w_plot.cluster,
		t_variable.id as attribute,
		t_cluster_configuration.plots_per_cluster,
		coalesce(adads.value, 0) as ldsity,
		w_plot.geom,
		true as is_aux
	FROM w_plot
	inner join sdesign.cm_plot2cluster_config_mapping on w_plot.gid = cm_plot2cluster_config_mapping.plot
	inner join sdesign.t_cluster_configuration on cm_plot2cluster_config_mapping.cluster_configuration = t_cluster_configuration.id
	inner join w_configuration on w_plot.conf_id = w_configuration.id
	inner join @extschema@.t_variable on t_variable.id = ANY (w_configuration.aux_attributes)
	left join (	select
			plot, variable, value, is_latest
		from @extschema@.t_auxiliary_data
		inner join @extschema@.t_available_datasets on (t_auxiliary_data.available_datasets = t_available_datasets.id)
	) as adads
	on (
		w_plot.gid = adads.plot and
		t_variable.id = adads.variable and
		adads.is_latest)
)
, w_ldsity_cluster AS MATERIALIZED (
 	SELECT
		w_ldsity_plot.conf_id,
		concat(w_ldsity_plot.conf_id, ROW_NUMBER() OVER ())::bigint as gid,
		w_ldsity_plot.cluster,
		w_ldsity_plot.attribute,
		w_ldsity_plot.stratum,
                w_ldsity_plot.panel,
		w_ldsity_plot.plots_per_cluster,
		count(*) as plcount,
		sum(w_ldsity_plot.ldsity)/w_ldsity_plot.plots_per_cluster AS ldsity_D_plus, -- eq 15,
		w_ldsity_plot.is_aux, --
		st_collect(w_ldsity_plot.geom)::geometry(MultiPoint) as geom
	FROM       w_ldsity_plot
	GROUP BY conf_id, stratum, panel, cluster, plots_per_cluster, attribute, is_aux
	ORDER BY stratum, cluster, attribute
)
, w_X AS MATERIALIZED ( -------------------------AUX LOCAL DENSITY ON TRACT LEVEL
	SELECT
		conf_id,
		attribute AS r,
		cluster AS c,
		ldsity_D_plus as val
	FROM w_ldsity_cluster
	WHERE is_aux
)
, w_clusters AS MATERIALIZED (-------------------------LIST OF TRACTS
	select distinct conf_id, stratum, panel, cluster from w_ldsity_cluster
)
, w_strata_sum AS MATERIALIZED (
	select
		w_configuration.id as conf_id,
                t_stratum.id as f_a_sampling_stratum_gid,
		case when t_cluster_configuration.cluster_design then
			t_cluster_configuration.frame_area_ha
		else
			t_stratum.area_ha
		end as lambda_d_plus,
		plots_per_cluster,
		sum(t_panel.cluster_count) as nb_sampling_units,
		sum(t_panel.sweight_panel_sum) as sweight_strata_sum
	from w_configuration
	inner join @extschema@.t_aux_conf on t_aux_conf.id = w_configuration.id
	inner join @extschema@.t_panel_refyearset_group ON t_panel_refyearset_group.panel_refyearset_group = t_aux_conf.panel_refyearset_group
	inner join sdesign.t_panel ON t_panel.id = t_panel_refyearset_group.panel
	inner join sdesign.t_cluster_configuration ON t_panel.cluster_configuration = t_cluster_configuration.id
	inner join sdesign.t_stratum ON t_stratum.id = t_panel.stratum
	group by w_configuration.id, f_a_sampling_stratum_gid, lambda_d_plus, plots_per_cluster
)
, w_pix AS MATERIALIZED (-------------------------INCLUSION DENSITY PIX
	SELECT
		w_clusters.conf_id,
		w_clusters.stratum,
		w_clusters.cluster,
		1.0 / (cm_cluster2panel_mapping.sampling_weight_ha * (v_strata_sum.lambda_d_plus / v_strata_sum.sweight_strata_sum)) as pix
	FROM w_clusters
        INNER JOIN sdesign.cm_cluster2panel_mapping ON (w_clusters.cluster = cm_cluster2panel_mapping.cluster
								and w_clusters.panel = cm_cluster2panel_mapping.panel)
	INNER JOIN w_strata_sum as v_strata_sum ON w_clusters.stratum = v_strata_sum.f_a_sampling_stratum_gid
)
, w_SIGMA AS MATERIALIZED (
	SELECT
		conf_id,
		1 as r,
		cluster as c,
		case when w_configuration.sigma then (plots_per_cluster^2)::float / plcount::float 
		else 1::float end
		as val
	from (select conf_id, cluster, plots_per_cluster, plcount 
		from w_ldsity_cluster 
		group by conf_id, cluster, plots_per_cluster, plcount) as m
	inner join w_configuration on (m.conf_id = w_configuration.id)
	order by r, c
)
, w_PI AS MATERIALIZED (
	SELECT
		conf_id,
		1 as r,
		cluster as c,
		1.0 / pix as val
	from w_pix
	order by r, c
)
, w_SIGMA_PI AS MATERIALIZED (
	SELECT
		A.conf_id,
		1 as r,
		A.c as c,
		A.val * B.val as val
	from w_SIGMA as A inner join w_PI as B on (A.c = B.c and A.conf_id = B.conf_id) 
	order by r, c
)
, w_XT AS MATERIALIZED (
	SELECT
		conf_id,
		c as r,
		r as c,
		val as val
	from w_X
	order by r, c
)
, w_X_SIGMA_PI AS MATERIALIZED ( -- element-wise multiplication
	select 
		A.conf_id,
		A.r, 
		A.c,
		A.val * B.val as val
	from w_X as A
	inner join w_SIGMA_PI as B on (A.c = B.c and A.conf_id = B.conf_id)
	ORDER BY r, c
)
, w_X_SIGMA_PI_XT AS MATERIALIZED ( -- matrix multiplication
	SELECT 
		A.conf_id,
		ROW_NUMBER() OVER (partition by A.conf_id order by A.r, B.c) AS mid,
		A.r, 
		B.c, 
		sum(A.val * B.val) as val
	FROM w_X_SIGMA_PI as A, w_XT as B
	WHERE A.c = B.r and A.conf_id = B.conf_id
	GROUP BY A.r, B.c, A.conf_id
	ORDER BY r, c
)
, w_agg AS MATERIALIZED (
	select conf_id, r, array_agg(val order by c) as val from w_X_SIGMA_PI_XT group by conf_id, r
)
, w_aggagg AS MATERIALIZED (
	select conf_id, array_agg(val order by r) as val from w_agg group by conf_id
)
, w_inv AS MATERIALIZED (
	select conf_id, @extschema@.fn_inverse(val) AS val from w_aggagg
)
, w_inv_id AS MATERIALIZED (
	select conf_id, mid, invval from w_inv, unnest(w_inv.val) WITH ORDINALITY AS t(invval, mid)
)
, w_X_SIGMA_PI_XT_inv AS MATERIALIZED (
	SELECT 
		conf_id, r, c, invval as val 
	FROM w_X_SIGMA_PI_XT 
	inner join w_inv_id using (conf_id, mid)
	ORDER BY r, c
)
, w_X_SIGMA_PI_XT_inv_X AS MATERIALIZED (
	select
		A.conf_id,
		A.r,
		B.c,
		sum(A.val * B.val) as val
	FROM
		w_X_SIGMA_PI_XT_inv as A, w_X as B
	WHERE A.c = B.r and A.conf_id = B.conf_id
	GROUP BY A.r, B.c, A.conf_id
	ORDER BY r, c
)
, w_G_beta AS MATERIALIZED (
	select
		A.conf_id,
		A.r,
		A.c,
		A.val * B.val as val
	FROM
		w_X_SIGMA_PI_XT_inv_X as A, w_SIGMA as B
	WHERE A.c = B.c and A.conf_id = B.conf_id
	ORDER BY r, c
)
select r as variable, c as cluster, val from w_G_beta;
        ';

	--RAISE NOTICE '%', _complete_query;
	RETURN QUERY EXECUTE _complete_query;
END;
$BODY$
  LANGUAGE plpgsql STABLE
  COST 100
  ROWS 100000;
;

COMMENT ON FUNCTION @extschema@.fn_G_beta(integer) IS 'Function computing matrix G_beta used for regression estimators. G_beta is dependent on model and parametrization domain (not cell). Matrix is represented by variable (row) and cluster (column) indices.';

-- </function>

