-- Copyright 2017, 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.

DROP FUNCTION IF EXISTS @extschema@.fn_api_get_panel_refyearset_combinations4groups(integer[]);
DROP FUNCTION IF EXISTS @extschema@.fn_api_show_panel_refyearset_groups();
DROP FUNCTION IF EXISTS @extschema@.fn_api_get_panel_refyearset_group(integer);
-- <function name="fn_api_get_panel_refyearset_combinations4groups" schema="extschema" src="functions/extschema/configuration/fn_api_get_panel_refyearset_combinations4groups.sql">
--
-- Copyright 2017, 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European CommISsion - subsequent versions of the EUPL (the "Licence");
-- You may not use thIS work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software dIStributed under the Licence IS dIStributed on an "AS IS" basIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permISsions and limitations under the Licence.
--

-- Function: @extschema@.fn_api_get_panel_refyearset_combinations4groups(integer[])
--DROP FUNCTION @extschema@.fn_api_get_panel_refyearset_combinations4groups(integer[]);

CREATE OR REPLACE FUNCTION @extschema@.fn_api_get_panel_refyearset_combinations4groups(_panel_refyearset_groups INT[])
RETURNS TABLE (
	panel INT, 
	panel_label VARCHAR(200), 
	panel_label_en VARCHAR(200), 
	panel_description TEXT, 
	panel_description_en TEXT, 
	reference_year_set INT, 
	refyearset_label VARCHAR(200), 
	refyearset_label_en VARCHAR(200), 
	refyearset_description TEXT, 
	refyearset_description_en TEXT,
	cluster_count INT, 
	plot_count INT
)
AS
$function$
BEGIN

IF _panel_refyearset_groups IS NULL THEN
	RAISE EXCEPTION 'Error 01: fn_api_get_panel_refyearset_combinations4groups: Function argument _panel_refyearset_groups INT[] must not be NULL!';
END IF;

-- checking _panel_refyearset_groups array if does not contain NULL
IF (SELECT array_position(_panel_refyearset_groups, NULL) IS NOT NULL) THEN
	RAISE EXCEPTION 'Error 02: fn_api_get_panel_refyearset_combinations4groups: Function argument _panel_refyearset_groups INT[] must not be an array containing NULL!';
END IF;

RETURN QUERY EXECUTE '
WITH w AS (
	SELECT DISTINCT panel,reference_year_set
	FROM @extschema@.t_panel_refyearset_group
	WHERE ARRAY[panel_refyearset_group] <@ $1
)
SELECT 
	t1.id AS panel,
	t1.panel AS panel_label,
	t1.panel AS panel_label_en, 
	t1.label::text AS panel_description, 
	t1.label::text AS panel_description_en, 
	t2.id AS reference_year_set,
	t2.reference_year_set AS refyearset_label,
	t2.reference_year_set AS refyearset_label_en, 
	t2.label::text AS refyearset_description, 
	t2.label::text AS refyearset_description_en, 	
	t1.cluster_count,
	t1.plot_count
FROM sdesign.t_panel AS t1
JOIN w ON t1.id = w.panel
LEFT JOIN sdesign.t_reference_year_set AS t2 ON t2.id = w.reference_year_set;' USING _panel_refyearset_groups;

END;
$function$
LANGUAGE plpgsql
PARALLEL SAFE;

COMMENT ON FUNCTION @extschema@.fn_api_get_panel_refyearset_combinations4groups(integer[]) IS 
'The function returns ids, labels and description of panels and reference-year sets belonging '
'to the group(s) entered as an input argument. It also returns the number of clusters (cluster_count) '
'and plots (plot_count) from sdesign.t_panel of all panels in the group(s).';

/*
-- testing false inputs

-- passing NULL for _panel_refyearset_groups
SELECT * FROM @extschema@.fn_api_get_panel_refyearset_combinations4groups(NULL);

-- passing an _panel_refyearset_groups containing NULL
SELECT * FROM @extschema@.fn_api_get_panel_refyearset_combinations4groups(ARRAY[23, 10, NULL]);

-- testing valid inputs

-- group 23
SELECT * FROM @extschema@.fn_api_get_panel_refyearset_combinations4groups(ARRAY[23]);

-- group 10 (no reference year sets)
SELECT * FROM @extschema@.fn_api_get_panel_refyearset_combinations4groups(ARRAY[10]);

-- group 20
SELECT * FROM @extschema@.fn_api_get_panel_refyearset_combinations4groups(ARRAY[20]);

-- groups 20 and 23, note combination 1 (panel) and 2 (refyearset) included only once in the function output
SELECT * FROM @extschema@.fn_api_get_panel_refyearset_combinations4groups(ARRAY[20,23]);

-- non-exISting group, no records
SELECT * FROM @extschema@.fn_api_get_panel_refyearset_combinations4groups(ARRAY[-20]);

 -- combination of non-exISting and exISting group, records of the exISting returned
SELECT * FROM @extschema@.fn_api_get_panel_refyearset_combinations4groups(ARRAY[-20, 23]);
*/
-- </function>

-- <function name="fn_api_get_list_of_panel_refyearset_groups" schema="extschema" src="functions/extschema/configuration/fn_api_get_list_of_panel_refyearset_groups.sql">
--
-- Copyright 2017, 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_api_get_list_of_panel_refyearset_groups()
--DROP FUNCTION @extschema@.fn_api_get_list_of_panel_refyearset_groups();

CREATE OR REPLACE FUNCTION @extschema@.fn_api_get_list_of_panel_refyearset_groups()
 RETURNS TABLE(id INT, label VARCHAR(200), description TEXT, label_en VARCHAR(200), description_en TEXT)
 LANGUAGE plpgsql
AS $function$
BEGIN

RETURN QUERY EXECUTE ' 
SELECT 
	id,
	label,
	description,
	label_en,
	description_en
FROM @extschema@.c_panel_refyearset_group
ORDER BY label, label_en;';
END;
$function$
;

COMMENT ON FUNCTION @extschema@.fn_api_get_list_of_panel_refyearset_groups() IS 
'The function returns id, label, description, label_en, description_en from nfiesta.c_panel_refyearset_group '
'of all groups of panel and reference-year set combinations.';

/*
-- testing
SELECT * FROM @extschema@.fn_api_get_list_of_panel_refyearset_groups();
*/
-- </function>

-- <function name="fn_api_before_delete_panel_refyearset_group" schema="extschema" src="functions/extschema/configuration/fn_api_before_delete_panel_refyearset_group.sql">
--
-- Copyright 2017, 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_api_before_delete_panel_refyearset_group(integer)
--DROP FUNCTION @extschema@.fn_api_before_delete_panel_refyearset_group(integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_api_before_delete_panel_refyearset_group (
	_id INT, 
	out _t_aux_conf_exi boolean, out _t_total_estimate_conf_exi boolean
)
 RETURNS record
 LANGUAGE plpgsql
AS $function$
BEGIN

IF _id IS NULL THEN
	RAISE EXCEPTION 'Error 01: fn_api_before_delete_panel_refyearset_group: Function argument _id INT must not be NULL!';
END IF;

IF NOT EXISTS (SELECT * FROM @extschema@.c_panel_refyearset_group WHERE id = _id) THEN
	RAISE EXCEPTION 'Error 02: fn_api_before_delete_panel_refyearset_group: Panel with reference year set group id = % does not exist in table @extschema@.c_panel_refyearset_group and cannot be deleted.', $1;
END IF;

SELECT EXISTS (SELECT * FROM @extschema@.t_aux_conf WHERE panel_refyearset_group = _id) INTO _t_aux_conf_exi;
SELECT EXISTS (SELECT * FROM @extschema@.t_total_estimate_conf WHERE panel_refyearset_group = _id) INTO _t_total_estimate_conf_exi;

END;
$function$
;

COMMENT ON FUNCTION @extschema@.fn_api_before_delete_panel_refyearset_group(integer) IS 
'The function controls if there is a row in tables @extschema@.t_aux_conf and @extschema@.t_total_estimate_conf, '
'where panel_refyearset_group = _id passed as an argument. If at least one result column is TRUE, '
'the row with id = _id in table @extschema@.c_panel_refyearset_group cannot be deleted.';

/*
-- testing false inputs

-- passing NULL for _id
SELECT * FROM @extschema@.fn_api_before_delete_panel_refyearset_group(NULL);

-- non-existing group, no records
SELECT * FROM @extschema@.fn_api_before_delete_panel_refyearset_group(-20);

-- testing valid inputs
-- panel group 1
SELECT * FROM @extschema@.fn_api_before_delete_panel_refyearset_group(1);

-- panel group 26
SELECT * FROM @extschema@.fn_api_before_delete_panel_refyearset_group(26);
*/
-- </function>

-- <function name="fn_api_delete_panel_refyearset_group" schema="extschema" src="functions/extschema/configuration/fn_api_delete_panel_refyearset_group.sql">
--
-- Copyright 2017, 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_api_delete_panel_refyearset_group(integer)
--DROP FUNCTION @extschema@.fn_api_delete_panel_refyearset_group(integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_api_delete_panel_refyearset_group (_id INT)
 RETURNS VOID
 LANGUAGE plpgsql
AS $function$
BEGIN

IF _id IS NULL THEN
	RAISE EXCEPTION 'Error 01: fn_api_delete_panel_refyearset_group: Function argument _id INT must not be NULL!';
END IF;

IF NOT EXISTS (SELECT * FROM @extschema@.c_panel_refyearset_group WHERE id = _id) THEN
	RAISE EXCEPTION 'Error 02: fn_api_delete_panel_refyearset_group: Panel with reference year set group id = % does not exist in table @extschema@.c_panel_refyearset_group and cannot be deleted.', $1;
END IF;

IF EXISTS (SELECT * FROM @extschema@.t_aux_conf WHERE panel_refyearset_group = _id) THEN
	RAISE EXCEPTION 'Error 03: fn_api_delete_panel_refyearset_group: Panel with reference year set group id = % is referenced from table @extschema@.t_aux_conf and cannot be deleted.', $1;
END IF;

IF EXISTS (SELECT * FROM @extschema@.t_total_estimate_conf WHERE panel_refyearset_group = _id) THEN
	RAISE EXCEPTION 'Error 04: fn_api_delete_panel_refyearset_group: Panel with reference year set group id = % is referenced from table @extschema@.t_total_estimate_conf and cannot be deleted.', $1;
END IF;

DELETE FROM @extschema@.t_panel_refyearset_group WHERE id = _id;
RAISE NOTICE 'Deleting rows from @extschema@.t_panel_refyearset_group with panel_refyearset_group = %.', $1;

DELETE FROM @extschema@.c_panel_refyearset_group WHERE id = _id;
RAISE NOTICE 'Deleting row from @extschema@.c_panel_refyearset_group with id = %.', $1;

END;
$function$
;

COMMENT ON FUNCTION @extschema@.fn_api_delete_panel_refyearset_group(integer) IS 
'The function deletes row from table @extschema@.c_panel_refyearset_group and lookup table '
'@extschema@.c_panel_refyearset_group, where id = _id passed as an argument.';

/*
-- testing false inputs

-- passing NULL for _id
SELECT * FROM @extschema@.fn_api_delete_panel_refyearset_group(NULL);

-- non-existing group, no records
SELECT * FROM @extschema@.fn_api_delete_panel_refyearset_group(-20);

-- testing valid inputs
*/
-- </function>

-- <function name="fn_api_update_panel_refyearset_group" schema="extschema" src="functions/extschema/configuration/fn_api_update_panel_refyearset_group.sql">
--
-- Copyright 2017, 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_api_update_panel_refyearset_group(INT, VARCHAR(200), TEXT, VARCHAR(200), TEXT)
--DROP FUNCTION @extschema@.fn_api_update_panel_refyearset_group(INT, VARCHAR(200), TEXT, VARCHAR(200), TEXT);

CREATE OR REPLACE FUNCTION @extschema@.fn_api_update_panel_refyearset_group (_id INT, _label VARCHAR(200), _description TEXT, _label_en VARCHAR(200), _description_en TEXT)
 RETURNS VOID
 LANGUAGE plpgsql
AS $function$

DECLARE
_label_exi		BOOLEAN;
_description_exi		BOOLEAN;
_label_en_exi		BOOLEAN;
_description_en_exi		BOOLEAN;

BEGIN

IF _id IS NULL THEN
	RAISE EXCEPTION 'Error 01: fn_api_update_panel_refyearset_group: Function argument _id INT must not be NULL!';
END IF;

IF _label IS NULL THEN
	RAISE EXCEPTION 'Error 02: fn_api_update_panel_refyearset_group: Function argument _label VARCHAR(200) must not be NULL!';
END IF;

IF _description IS NULL THEN
	RAISE EXCEPTION 'Error 03: fn_api_update_panel_refyearset_group: Function argument _description TEXT must not be NULL!';
END IF;

IF _label_en IS NULL THEN
	RAISE EXCEPTION 'Error 04: fn_api_update_panel_refyearset_group: Function argument _label_en VARCHAR(200) must not be NULL!';
END IF;

IF _description_en IS NULL THEN
	RAISE EXCEPTION 'Error 05: fn_api_update_panel_refyearset_group: Function argument _description_en TEXT must not be NULL!';
END IF;

IF NOT EXISTS (SELECT * FROM @extschema@.c_panel_refyearset_group WHERE id = _id) THEN
	RAISE EXCEPTION 'Error 06: fn_api_update_panel_refyearset_group: Panel with reference year set group id = % does not exist in table @extschema@.c_panel_refyearset_group and cannot be updated.', $1;
END IF;

SELECT _label_exists, _description_exists, _label_en_exists, _description_en_exists
FROM @extschema@.fn_api_do_labels_and_descriptions_of_panel_refyearset_group_exi(_label, _label_en, _description, _description_en) 
INTO _label_exi, _description_exi, _label_en_exi, _description_en_exi;

IF 
	_label_exi = TRUE 
THEN
	RAISE EXCEPTION 'Error 07: fn_api_update_panel_refyearset_group: Function argument _label VARCHAR(200) already exists in table @extschema@.c_panel_refyearset_group.';
END IF;

IF
	_description_exi = TRUE
THEN
	RAISE EXCEPTION 'Error 08: fn_api_update_panel_refyearset_group: Function argument _description TEXT already exists in table @extschema@.c_panel_refyearset_group.';
END IF;

IF
	_label_en_exi = TRUE
THEN
	RAISE EXCEPTION 'Error 09: fn_api_update_panel_refyearset_group: Function argument _label_en VARCHAR(200) already exists in table @extschema@.c_panel_refyearset_group.';
END IF;

IF
	_description_en_exi = TRUE 
THEN
	RAISE EXCEPTION 'Error 10: fn_api_update_panel_refyearset_group: Function argument _description_en TEXT already exists in table @extschema@.c_panel_refyearset_group.';
END IF;

UPDATE @extschema@.c_panel_refyearset_group 
SET 
	label = _label,
	description = _description,
	label_en = _label_en,
	description_en = _description_en
WHERE id = _id;
RAISE NOTICE 'Updating row in @extschema@.c_panel_refyearset_group with id = %.', _id;

END;
$function$
;

COMMENT ON FUNCTION @extschema@.fn_api_update_panel_refyearset_group(INT, VARCHAR(200), TEXT, VARCHAR(200), TEXT) IS 
'The function updates label, description, label_en, description_en (passed as arguments 2-5) in lookup '
'table @extschema@.c_panel_refyearset_group, where id = _id passed as an argument 1.';

/*
-- testing false inputs

-- passing NULL for arguments
SELECT * FROM @extschema@.fn_api_update_panel_refyearset_group(NULL,NULL,NULL,NULL,NULL);
SELECT * FROM @extschema@.fn_api_update_panel_refyearset_group(10,NULL,NULL,NULL,NULL);

-- existing _label and _description
SELECT * FROM @extschema@.fn_api_update_panel_refyearset_group(1,'no_of_panels: 1;{1}','no_of_panels: 1;{1}','new_label_en','new_description_en');

-- non-existing group, no records
SELECT * FROM @extschema@.fn_api_update_panel_refyearset_group(-20,'new_label','new_description','new_label_en','new_description_en');

-- testing valid inputs

-- group 25
SELECT * FROM @extschema@.fn_api_update_panel_refyearset_group(25,'new_label_35','new_description_35','new_label_en_35','new_description_en_35');

*/
-- </function>