--
-- Copyright 2017, 2023 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--


DROP FUNCTION IF EXISTS @extschema@.fn_etl_import_ldsity_values(json, double precision) CASCADE;


-- <function name="fn_etl_import_ldsity_values" schema="extschema" src="functions/extschema/etl/fn_etl_import_ldsity_values.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_import_ldsity_values(json, double precision)

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_import_ldsity_values(json, double precision) CASCADE;

create or replace function @extschema@.fn_etl_import_ldsity_values
(
	_available_datasets_and_ldsity_values json,
	_threshold double precision default 0.000001
)
returns text
as
$$
declare
		_max_id_ttd								integer;
		_max_id_tad								integer;
		_target_variable						integer;
		_target_variable_ad						integer;
		_country								varchar;
		_country_ad								varchar;
		_strata_sets							varchar[];
		_strata_sets_ad							varchar[];
		_stratums								varchar[];
		_stratums_ad							varchar[];
		_panels									varchar[];
		_panels_ad								varchar[];
		_reference_year_sets					varchar[];
		_reference_year_sets_ad					varchar[];
		_inventory_campaigns					varchar[];
		_inventory_campaigns_ad					varchar[];
		_clusters								varchar[];
		_cluster_configurations					varchar[];
		_cluster_configurations_ad				varchar[];
		_array_target_variable					integer[];
		_array_target_variable_ad				integer[];
		_array_sub_population_category			integer[];
		_array_sub_population_category_ad		integer[];
		_array_area_domain_category				integer[];
		_array_area_domain_category_ad			integer[];
		_array_panel							varchar[];
		_array_panel_ad							varchar[];
		_array_reference_year_set				varchar[];
		_array_reference_year_set_ad			varchar[];
		_check_cmrys2pm							integer;
		_check_cmrys2pm_ad						integer;
		_array_cmrys2pm							integer[];
		_array_cmrys2pm_ad						integer[];
		_check_count_records_input_json			integer;
		_check_count_records_after_inner_join	integer;
		_res_ttd								text;
		_res_tad								text;
		_res									text;
begin
		if _available_datasets_and_ldsity_values is null
		then
			raise exception 'Error 01: fn_etl_import_ldsity_values: Input argument _available_datasets_and_ldsity_values must not by NULL!';
		end if;
	
		_max_id_ttd := (select coalesce(max(id),0) from @extschema@.t_target_data);
		_max_id_tad := (select coalesce(max(id),0) from @extschema@.t_available_datasets);

		---------------------------------------------------------------------------------
		-- check target variable [element "available_datasets"] --
		with
		w1 as	(select json_array_elements((_available_datasets_and_ldsity_values->>'available_datasets')::json) as s)
		,w2 as	(select	(s->>'target_variable')::integer as target_variable	from w1)
		select distinct w2.target_variable from w2
		into _target_variable_ad;

		if (select count(ctv.*) is distinct from 1 from @extschema@.c_target_variable as ctv where ctv.id = _target_variable_ad)
		then
			raise exception 'Error 02a: fn_etl_import_ldsity_values: Input JSON element "target_variable = %" in element "available_datasets" is not present in table c_target_variable!',_target_variable_ad;
		end if;

		-- check target variable [element "ldsity_values"] --
		with
		w1 as	(select json_array_elements((_available_datasets_and_ldsity_values->>'ldsity_values')::json) as s)
		,w2 as	(select	(s->>'target_variable')::integer as target_variable	from w1)
		select distinct w2.target_variable from w2
		into _target_variable;

		if (select count(ctv.*) is distinct from 1 from @extschema@.c_target_variable as ctv where ctv.id = _target_variable)
		then
			raise exception 'Error 02b: fn_etl_import_ldsity_values: Input JSON element "target_variable = %" in element "ldsity_values" is not present in table c_target_variable!',_target_variable;
		end if;
		---------------------------------------------------------------------------------
		-- check country [element "available_datasets"] --
		with
		w1 as	(select json_array_elements((_available_datasets_and_ldsity_values->>'available_datasets')::json) as s)
		,w2 as	(select	(s->>'country')::varchar as country	from w1)
		select distinct w2.country from w2
		into _country_ad;

		if (select count(cc.*) is distinct from 1 from sdesign.c_country as cc where cc.label = _country_ad)
		then
			raise exception 'Error 03a: fn_etl_import_ldsity_values: Input JSON element "country = %" in element "available_datasets" is not present in table sdesign.c_country!',_country_ad;
		end if;

		-- check country [element "ldsity_values"] --
		with
		w1 as	(select json_array_elements((_available_datasets_and_ldsity_values->>'ldsity_values')::json) as s)
		,w2 as	(select	(s->>'country')::varchar as country	from w1)
		select distinct w2.country from w2
		into _country;

		if (select count(cc.*) is distinct from 1 from sdesign.c_country as cc where cc.label = _country)
		then
			raise exception 'Error 03b: fn_etl_import_ldsity_values: Input JSON element "country = %" in element "ldsity_values" is not present in table sdesign.c_country!',_country;
		end if;
		---------------------------------------------------------------------------------
		-- check strata sets [element "available_datasets"] --
		with
		w1 as	(select json_array_elements((_available_datasets_and_ldsity_values->>'available_datasets')::json) as s)
		,w2 as	(select	(s->>'strata_set')::varchar as strata_set from w1)
		,w3 as	(select distinct w2.strata_set from w2)
		select array_agg(w3.strata_set order by w3.strata_set) from w3
		into _strata_sets_ad;

		for i in 1..array_length(_strata_sets_ad,1)
		loop
			if	(
				select count(tss.*) is distinct from 1 from sdesign.t_strata_set as tss where tss.country =
				(select cc.id from sdesign.c_country as cc where cc.label = _country_ad)
				and tss.strata_set = _strata_sets_ad[i]
				)
			then
				raise exception 'Error 04a: fn_etl_import_ldsity_values: Some of input JSON element "strata_set = %" in element "available_datasets" is not present in table sdesign.t_strata for input JSON element "country = %"!',_strata_sets_ad[i], _country_ad;
			end if;		
		end loop;

		-- check strata sets [element "ldsity_values"] --
		with
		w1 as	(select json_array_elements((_available_datasets_and_ldsity_values->>'ldsity_values')::json) as s)
		,w2 as	(select	(s->>'strata_set')::varchar as strata_set from w1)
		,w3 as	(select distinct w2.strata_set from w2)
		select array_agg(w3.strata_set order by w3.strata_set) from w3
		into _strata_sets;

		for i in 1..array_length(_strata_sets,1)
		loop
			if	(
				select count(tss.*) is distinct from 1 from sdesign.t_strata_set as tss where tss.country =
				(select cc.id from sdesign.c_country as cc where cc.label = _country)
				and tss.strata_set = _strata_sets[i]
				)
			then
				raise exception 'Error 04b: fn_etl_import_ldsity_values: Some of input JSON element "strata_set = %" in element "ldsity_values" is not present in table sdesign.t_strata for input JSON element "country = %"!',_strata_sets[i], _country;
			end if;		
		end loop;
		---------------------------------------------------------------------------------
		-- check stratums [element "available_datasets"] --
		with
		w1 as	(select json_array_elements((_available_datasets_and_ldsity_values->>'available_datasets')::json) as s)
		,w2 as	(select	(s->>'stratum')::varchar as stratum from w1)
		,w3 as	(select distinct w2.stratum from w2)
		select array_agg(w3.stratum order by w3.stratum) from w3
		into _stratums_ad;

		for i in 1..array_length(_stratums_ad,1)
		loop
			if	(
				select count(ts.*) is distinct from 1 from sdesign.t_stratum as ts where ts.strata_set in
				(select tss.id from sdesign.t_strata_set as tss where tss.country =
				(select cc.id from sdesign.c_country as cc where cc.label = _country_ad))
				and ts.stratum = _stratums_ad[i]
				)
			then
				raise exception 'Error 05a: fn_etl_import_ldsity_values: Some of input JSON element "stratum = %" in element "available_datasets" is not present in table sdesign.t_stratum for input JSON element "country = %"!',_stratums_ad[i], _country_ad;
			end if;	
		end loop;

		-- check stratums [element "ldsity_values"] --
		with
		w1 as	(select json_array_elements((_available_datasets_and_ldsity_values->>'ldsity_values')::json) as s)
		,w2 as	(select	(s->>'stratum')::varchar as stratum from w1)
		,w3 as	(select distinct w2.stratum from w2)
		select array_agg(w3.stratum order by w3.stratum) from w3
		into _stratums;

		for i in 1..array_length(_stratums,1)
		loop
			if	(
				select count(ts.*) is distinct from 1 from sdesign.t_stratum as ts where ts.strata_set in
				(select tss.id from sdesign.t_strata_set as tss where tss.country =
				(select cc.id from sdesign.c_country as cc where cc.label = _country))
				and ts.stratum = _stratums[i]
				)
			then
				raise exception 'Error 05b: fn_etl_import_ldsity_values: Some of input JSON element "stratum = %" in element "ldsity_values" is not present in table sdesign.t_stratum for input JSON element "country = %"!',_stratums[i], _country;
			end if;	
		end loop;
		---------------------------------------------------------------------------------
		-- check panels [element "available_datasets"] --
		with
		w1 as	(select json_array_elements((_available_datasets_and_ldsity_values->>'available_datasets')::json) as s)
		,w2 as	(select	(s->>'panel')::varchar as panel	from w1)
		,w3 as	(select distinct w2.panel from w2)
		select array_agg(w3.panel order by w3.panel) from w3
		into _panels_ad;

		for i in 1..array_length(_panels_ad,1)
		loop
			if	(
				select count(tp.*) is distinct from 1 from sdesign.t_panel as tp where tp.stratum in
				(select ts.id from sdesign.t_stratum as ts where ts.strata_set in
				(select tss.id from sdesign.t_strata_set as tss where tss.country = 
				(select cc.id from sdesign.c_country as cc where cc.label = _country_ad)))
				and tp.panel = _panels_ad[i]
				)
			then
				raise exception 'Error 06a: fn_etl_import_ldsity_values: Some of input JSON element "panel = %" in element "available_datasets" is not present in table sdesign.t_panel for input JSON element "country = %"!',_panels_ad[i], _country_ad;
			end if;		
		end loop;

		-- check panels [element "ldsity_values"] --
		with
		w1 as	(select json_array_elements((_available_datasets_and_ldsity_values->>'ldsity_values')::json) as s)
		,w2 as	(select	(s->>'panel')::varchar as panel	from w1)
		,w3 as	(select distinct w2.panel from w2)
		select array_agg(w3.panel order by w3.panel) from w3
		into _panels;

		for i in 1..array_length(_panels,1)
		loop
			if	(
				select count(tp.*) is distinct from 1 from sdesign.t_panel as tp where tp.stratum in
				(select ts.id from sdesign.t_stratum as ts where ts.strata_set in
				(select tss.id from sdesign.t_strata_set as tss where tss.country = 
				(select cc.id from sdesign.c_country as cc where cc.label = _country)))
				and tp.panel = _panels[i]
				)
			then
				raise exception 'Error 06b: fn_etl_import_ldsity_values: Some of input JSON element "panel = %" in element "ldsity_values" is not present in table sdesign.t_panel for input JSON element "country = %"!',_panels[i], _country;
			end if;		
		end loop;
		---------------------------------------------------------------------------------
		-- check reference year sets [element "available_datasets"] --
		with
		w1 as	(select json_array_elements((_available_datasets_and_ldsity_values->>'available_datasets')::json) as s)
		,w2 as	(select	(s->>'reference_year_set')::varchar as reference_year_set from w1)
		,w3 as	(select distinct w2.reference_year_set from w2)
		select array_agg(w3.reference_year_set order by w3.reference_year_set) from w3
		into _reference_year_sets_ad;

		for i in 1..array_length(_reference_year_sets_ad,1)
		loop
			if	(
				select count(trys.*) is distinct from 1 from sdesign.t_reference_year_set as trys where trys.id in
				(select cmr.reference_year_set from sdesign.cm_refyearset2panel_mapping as cmr where cmr.panel in
				(select tp.id from sdesign.t_panel as tp where tp.stratum in
				(select ts.id from sdesign.t_stratum as ts where ts.strata_set in
				(select tss.id from sdesign.t_strata_set as tss where tss.country = 
				(select cc.id from sdesign.c_country as cc where cc.label = _country_ad)))))
				and trys.reference_year_set = _reference_year_sets_ad[i]
				)
			then
				raise exception 'Error 07a: fn_etl_import_ldsity_values: Some of input JSON element "reference_year_set = %" in element "available_datasets" is not present in table sdesign.t_reference_year_set for input JSON element "country = %"!',_reference_year_sets_ad[i], _country_ad;
			end if;		
		end loop;

		-- check reference year sets [element "ldsity_values"] --
		with
		w1 as	(select json_array_elements((_available_datasets_and_ldsity_values->>'ldsity_values')::json) as s)
		,w2 as	(select	(s->>'reference_year_set')::varchar as reference_year_set from w1)
		,w3 as	(select distinct w2.reference_year_set from w2)
		select array_agg(w3.reference_year_set order by w3.reference_year_set) from w3
		into _reference_year_sets;

		for i in 1..array_length(_reference_year_sets,1)
		loop
			if	(
				select count(trys.*) is distinct from 1 from sdesign.t_reference_year_set as trys where trys.id in
				(select cmr.reference_year_set from sdesign.cm_refyearset2panel_mapping as cmr where cmr.panel in
				(select tp.id from sdesign.t_panel as tp where tp.stratum in
				(select ts.id from sdesign.t_stratum as ts where ts.strata_set in
				(select tss.id from sdesign.t_strata_set as tss where tss.country = 
				(select cc.id from sdesign.c_country as cc where cc.label = _country)))))
				and trys.reference_year_set = _reference_year_sets[i]
				)
			then
				raise exception 'Error 07b: fn_etl_import_ldsity_values: Some of input JSON element "reference_year_set = %" in element "ldsity_values" is not present in table sdesign.t_reference_year_set for input JSON element "country = %"!',_reference_year_sets[i], _country;
			end if;		
		end loop;
		---------------------------------------------------------------------------------
		-- check inventory campaigns [element "available_datasets"] --
		with
		w1 as	(select json_array_elements((_available_datasets_and_ldsity_values->>'available_datasets')::json) as s)
		,w2 as	(select	(s->>'inventory_campaign')::varchar as inventory_campaign from w1)
		,w3 as	(select distinct w2.inventory_campaign from w2)
		select array_agg(w3.inventory_campaign order by w3.inventory_campaign) from w3
		into _inventory_campaigns_ad;

		for i in 1..array_length(_inventory_campaigns_ad,1)
		loop
			if	(
				select count(tic.*) is distinct from 1 from sdesign.t_inventory_campaign as tic where tic.id in
				(select trys.inventory_campaign from sdesign.t_reference_year_set as trys where trys.id in
				(select cmr.reference_year_set from sdesign.cm_refyearset2panel_mapping as cmr where cmr.panel in
				(select tp.id from sdesign.t_panel as tp where tp.stratum in
				(select ts.id from sdesign.t_stratum as ts where ts.strata_set in
				(select tss.id from sdesign.t_strata_set as tss where tss.country = 
				(select cc.id from sdesign.c_country as cc where cc.label = _country_ad))))))
				and tic.inventory = _inventory_campaigns_ad[i]
				)
			then
				raise exception 'Error 08a: fn_etl_import_ldsity_values: Some of input JSON element "inventory_campaign = %" in element "available_datasets" is not present in table sdesign.t_inventory_campaign for input JSON element "country = %"!',_inventory_campaigns_ad[i], _country_ad;
			end if;		
		end loop;

		-- check inventory campaigns [element "ldsity_values"] --
		with
		w1 as	(select json_array_elements((_available_datasets_and_ldsity_values->>'ldsity_values')::json) as s)
		,w2 as	(select	(s->>'inventory_campaign')::varchar as inventory_campaign from w1)
		,w3 as	(select distinct w2.inventory_campaign from w2)
		select array_agg(w3.inventory_campaign order by w3.inventory_campaign) from w3
		into _inventory_campaigns;

		for i in 1..array_length(_inventory_campaigns,1)
		loop
			if	(
				select count(tic.*) is distinct from 1 from sdesign.t_inventory_campaign as tic where tic.id in
				(select trys.inventory_campaign from sdesign.t_reference_year_set as trys where trys.id in
				(select cmr.reference_year_set from sdesign.cm_refyearset2panel_mapping as cmr where cmr.panel in
				(select tp.id from sdesign.t_panel as tp where tp.stratum in
				(select ts.id from sdesign.t_stratum as ts where ts.strata_set in
				(select tss.id from sdesign.t_strata_set as tss where tss.country = 
				(select cc.id from sdesign.c_country as cc where cc.label = _country))))))
				and tic.inventory = _inventory_campaigns[i]
				)
			then
				raise exception 'Error 08b: fn_etl_import_ldsity_values: Some of input JSON element "inventory_campaign = %" in element "ldsity_values" is not present in table sdesign.t_inventory_campaign for input JSON element "country = %"!',_inventory_campaigns[i], _country;
			end if;		
		end loop;
		---------------------------------------------------------------------------------
		-- check clusters [element "ldsity_values"] --
		with
		w1 as	(select json_array_elements((_available_datasets_and_ldsity_values->>'ldsity_values')::json) as s)
		,w2 as	(select	(s->>'cluster')::varchar as cluster from w1)
		,w3 as	(select distinct w2.cluster from w2)
		select array_agg(w3.cluster order by w3.cluster) from w3
		into _clusters;

		for i in 1..array_length(_clusters,1)
		loop
			if	(
				select count(tc.*) is distinct from 1 from sdesign.t_cluster as tc where tc.id in
				(select ccpm.cluster from sdesign.cm_cluster2panel_mapping as ccpm where ccpm.panel in
				(select tp.id from sdesign.t_panel as tp where tp.stratum in
				(select ts.id from sdesign.t_stratum as ts where ts.strata_set in
				(select tss.id from sdesign.t_strata_set as tss where tss.country = 
				(select cc.id from sdesign.c_country as cc where cc.label = _country)))))
				and tc.cluster = _clusters[i]
				)
			then
				raise exception 'Error 09: fn_etl_import_ldsity_values: Some of input JSON element "cluster = %" is not present in table sdesign.t_cluster for input JSON element "country = %"!',_clusters[i], _country;
			end if;		
		end loop;
		---------------------------------------------------------------------------------
		-- check cluster configurations [element "available_datasets"] --
		with
		w1 as	(select json_array_elements((_available_datasets_and_ldsity_values->>'available_datasets')::json) as s)
		,w2 as	(select	(s->>'cluster_configuration')::varchar as cluster_configuration from w1)
		,w3 as	(select distinct w2.cluster_configuration from w2)
		select array_agg(w3.cluster_configuration order by w3.cluster_configuration) from w3
		into _cluster_configurations_ad;

		for i in 1..array_length(_cluster_configurations_ad,1)
		loop
			if	(
				select count(tcc.*) is distinct from 1 from sdesign.t_cluster_configuration as tcc where tcc.id in
				(select tp.cluster_configuration from sdesign.t_panel as tp where tp.stratum in
				(select ts.id from sdesign.t_stratum as ts where ts.strata_set in
				(select tss.id from sdesign.t_strata_set as tss where tss.country = 
				(select cc.id from sdesign.c_country as cc where cc.label = _country_ad))))
				and tcc.cluster_configuration = _cluster_configurations_ad[i]
				)
			then
				raise exception 'Error 10b: fn_etl_import_ldsity_values: Some of input JSON element "cluster_configuration = %" in element "available_datasets" is not present in table sdesign.t_cluster_configuration for input JSON element "country = %"!',_cluster_configurations_ad[i], _country_ad;
			end if;		
		end loop;

		-- check cluster configurations [element "ldsity_values"] --
		with
		w1 as	(select json_array_elements((_available_datasets_and_ldsity_values->>'ldsity_values')::json) as s)
		,w2 as	(select	(s->>'cluster_configuration')::varchar as cluster_configuration from w1)
		,w3 as	(select distinct w2.cluster_configuration from w2)
		select array_agg(w3.cluster_configuration order by w3.cluster_configuration) from w3
		into _cluster_configurations;

		for i in 1..array_length(_cluster_configurations,1)
		loop
			if	(
				select count(tcc.*) is distinct from 1 from sdesign.t_cluster_configuration as tcc where tcc.id in
				(select tp.cluster_configuration from sdesign.t_panel as tp where tp.stratum in
				(select ts.id from sdesign.t_stratum as ts where ts.strata_set in
				(select tss.id from sdesign.t_strata_set as tss where tss.country = 
				(select cc.id from sdesign.c_country as cc where cc.label = _country))))
				and tcc.cluster_configuration = _cluster_configurations[i]
				)
			then
				raise exception 'Error 10b: fn_etl_import_ldsity_values: Some of input JSON element "cluster_configuration = %" in element "ldsity_values" is not present in table sdesign.t_cluster_configuration for input JSON element "country = %"!',_cluster_configurations[i], _country;
			end if;		
		end loop;
		---------------------------------------------------------------------------------
		-- check combinations of target_variable, sub_population_category and area_domain_category [element "available_datasets"] --
		with
		w1 as	(
				select json_array_elements((_available_datasets_and_ldsity_values->>'available_datasets')::json) as s
				)
		,w2 as	(
				select
						(s->>'target_variable')::integer			as target_variable,
						(s->>'sub_population_category')::integer	as sub_population_category,
						(s->>'area_domain_category')::integer		as area_domain_category	
				from w1
				)
		,w3 as	(select distinct target_variable, sub_population_category, area_domain_category from w2)
		select
				array_agg(w3.target_variable order by w3.sub_population_category, w3.area_domain_category) as target_variable,
				array_agg(w3.sub_population_category order by w3.sub_population_category, w3.area_domain_category) as sub_population_category,
				array_agg(w3.area_domain_category order by w3.sub_population_category, w3.area_domain_category) as area_domain_category
		from
				w3
		into
				_array_target_variable_ad,
				_array_sub_population_category_ad,
				_array_area_domain_category_ad;

		for i in 1..array_length(_array_sub_population_category_ad,1)
		loop
			if	(
				with
				w1 as	(
						select
								id,
								target_variable,
								case when sub_population_category is null then 0 else sub_population_category end as sub_population_category,
								case when area_domain_category is null then 0 else area_domain_category end as area_domain_category
						from
								@extschema@.t_variable where target_variable = _array_target_variable_ad[i]
						)
				select count(w1.*) is distinct from 1 from w1
				where w1.sub_population_category = _array_sub_population_category_ad[i]
				and w1.area_domain_category = _array_area_domain_category_ad[i]
				)
			then
				raise exception 'Error 11a: fn_etl_import_ldsity_values: Some of input combination JSON elements "target_variable = %", "sub_population_category = %" and "area_domain_category = %" in element "available_datasets" is not present in table t_variable!',_array_target_variable_ad[i], _array_sub_population_category_ad[i], _array_area_domain_category_ad[i];
			end if;
		end loop;

		-- check combinations of target_variable, sub_population_category and area_domain_category [element "ldsity_values"] --
		with
		w1 as	(
				select json_array_elements((_available_datasets_and_ldsity_values->>'ldsity_values')::json) as s
				)
		,w2 as	(
				select
						(s->>'target_variable')::integer			as target_variable,
						(s->>'sub_population_category')::integer	as sub_population_category,
						(s->>'area_domain_category')::integer		as area_domain_category	
				from w1
				)
		,w3 as	(select distinct target_variable, sub_population_category, area_domain_category from w2)
		select
				array_agg(w3.target_variable order by w3.sub_population_category, w3.area_domain_category) as target_variable,
				array_agg(w3.sub_population_category order by w3.sub_population_category, w3.area_domain_category) as sub_population_category,
				array_agg(w3.area_domain_category order by w3.sub_population_category, w3.area_domain_category) as area_domain_category
		from
				w3
		into
				_array_target_variable,
				_array_sub_population_category,
				_array_area_domain_category;

		for i in 1..array_length(_array_sub_population_category,1)
		loop
			if	(
				with
				w1 as	(
						select
								id,
								target_variable,
								case when sub_population_category is null then 0 else sub_population_category end as sub_population_category,
								case when area_domain_category is null then 0 else area_domain_category end as area_domain_category
						from
								@extschema@.t_variable where target_variable = _array_target_variable[i]
						)
				select count(w1.*) is distinct from 1 from w1
				where w1.sub_population_category = _array_sub_population_category[i]
				and w1.area_domain_category = _array_area_domain_category[i]
				)
			then
				raise exception 'Error 11b: fn_etl_import_ldsity_values: Some of input combination JSON elements "target_variable = %", "sub_population_category = %" and "area_domain_category = %" in element "ldsity_values" is not present in table t_variable!',_array_target_variable[i], _array_sub_population_category[i], _array_area_domain_category[i];
			end if;
		end loop;
		---------------------------------------------------------------------------------
		-- check combinations of panel and reference year set [element "available_datasets"] --
		with
		w1 as	(
				select json_array_elements((_available_datasets_and_ldsity_values->>'available_datasets')::json) as s
				)
		,w2 as	(
				select
						(s->>'panel')::varchar				as panel,
						(s->>'reference_year_set')::varchar	as reference_year_set
				from w1
				)
		,w3 as	(select distinct panel, reference_year_set from w2)
		select
				array_agg(w3.panel order by w3.panel, w3.reference_year_set) as panel,
				array_agg(w3.reference_year_set order by w3.panel, w3.reference_year_set) as reference_year_set
		from
				w3
		into
				_array_panel_ad,
				_array_reference_year_set_ad;

		for i in 1..array_length(_array_panel_ad,1)
		loop
			select cmrys.id from sdesign.cm_refyearset2panel_mapping as cmrys
			where cmrys.panel =	(
							select tp.id from sdesign.t_panel as tp where tp.stratum in
							(select ts.id from sdesign.t_stratum as ts where ts.strata_set in
							(select tss.id from sdesign.t_strata_set as tss where tss.country = 
							(select cc.id from sdesign.c_country as cc where cc.label = _country_ad)))
							and tp.panel = _array_panel_ad[i]
							)
			and cmrys.reference_year_set =	(
											select trys.id from sdesign.t_reference_year_set as trys where trys.id in
											(select cmr.reference_year_set from sdesign.cm_refyearset2panel_mapping as cmr where cmr.panel in
											(select tp.id from sdesign.t_panel as tp where tp.stratum in
											(select ts.id from sdesign.t_stratum as ts where ts.strata_set in
											(select tss.id from sdesign.t_strata_set as tss where tss.country = 
											(select cc.id from sdesign.c_country as cc where cc.label = _country_ad)))))
											and trys.reference_year_set = _array_reference_year_set_ad[i]
											)
			into _check_cmrys2pm_ad;

			if _check_cmrys2pm_ad is null
			then
				raise exception 'Error 12a: fn_etl_import_ldsity_values: Some of input combination JSON elements "panel = %" and "reference_year_set = %" in element "available_datasets" is not present in table sdesign.cm_refyearset2panel_mapping for input JSON element "country = %"!',_array_panel_ad[i], _array_reference_year_set_ad[i], _country_ad;
			end if;

			if i = 1
			then
				_array_cmrys2pm_ad := array[_check_cmrys2pm_ad];
			else
				_array_cmrys2pm_ad := _array_cmrys2pm_ad || array[_check_cmrys2pm_ad];
			end if;
		end loop;

		-- check combinations of panel and reference year set [element "ldsity_values"] --
		with
		w1 as	(
				select json_array_elements((_available_datasets_and_ldsity_values->>'ldsity_values')::json) as s
				)
		,w2 as	(
				select
						(s->>'panel')::varchar				as panel,
						(s->>'reference_year_set')::varchar	as reference_year_set
				from w1
				)
		,w3 as	(select distinct panel, reference_year_set from w2)
		select
				array_agg(w3.panel order by w3.panel, w3.reference_year_set) as panel,
				array_agg(w3.reference_year_set order by w3.panel, w3.reference_year_set) as reference_year_set
		from
				w3
		into
				_array_panel,
				_array_reference_year_set;

		for i in 1..array_length(_array_panel,1)
		loop
			select cmrys.id from sdesign.cm_refyearset2panel_mapping as cmrys
			where cmrys.panel =	(
							select tp.id from sdesign.t_panel as tp where tp.stratum in
							(select ts.id from sdesign.t_stratum as ts where ts.strata_set in
							(select tss.id from sdesign.t_strata_set as tss where tss.country = 
							(select cc.id from sdesign.c_country as cc where cc.label = _country)))
							and tp.panel = _array_panel[i]
							)
			and cmrys.reference_year_set =	(
											select trys.id from sdesign.t_reference_year_set as trys where trys.id in
											(select cmr.reference_year_set from sdesign.cm_refyearset2panel_mapping as cmr where cmr.panel in
											(select tp.id from sdesign.t_panel as tp where tp.stratum in
											(select ts.id from sdesign.t_stratum as ts where ts.strata_set in
											(select tss.id from sdesign.t_strata_set as tss where tss.country = 
											(select cc.id from sdesign.c_country as cc where cc.label = _country)))))
											and trys.reference_year_set = _array_reference_year_set[i]
											)
			into _check_cmrys2pm;

			if _check_cmrys2pm is null
			then
				raise exception 'Error 12b: fn_etl_import_ldsity_values: Some of input combination JSON elements "panel = %" and "reference_year_set = %" in element "ldsity_values" is not present in table sdesign.cm_refyearset2panel_mapping for input JSON element "country = %"!',_array_panel[i], _array_reference_year_set[i], _country;
			end if;

			if i = 1
			then
				_array_cmrys2pm := array[_check_cmrys2pm];
			else
				_array_cmrys2pm := _array_cmrys2pm || array[_check_cmrys2pm];
			end if;
		end loop;
		---------------------------------------------------------------------------------
		---------------------------------------------------------------------------------
		-- get count of records in input JSON in part ldsity_values
		with
		w1 as	(
				select json_array_elements((_available_datasets_and_ldsity_values->>'ldsity_values')::json) as s
				)
		,w2 as	(
				select
						(s->>'target_variable')::integer			as target_variable,
						(s->>'country')::varchar					as country,
						(s->>'strata_set')::varchar					as strata_set,
						(s->>'stratum')::varchar					as stratum,
						(s->>'reference_year_set')::varchar			as reference_year_set,
						(s->>'panel')::varchar						as panel,
						(s->>'cluster')::varchar					as cluster,
						(s->>'cluster_configuration')::varchar		as cluster_configuration,
						(s->>'inventory_campaign')::varchar			as inventory_campaign,
						(s->>'plot')::varchar						as plot
				from w1
				)
		select count(w2.*) from w2
		into _check_count_records_input_json;
		---------------------------------------------------------------------------------
		-- get count of records after inner join with sdesign for given combinations of panels and reference year sets
		with
		w1 as	(
				select json_array_elements((_available_datasets_and_ldsity_values->>'ldsity_values')::json) as s
				)
		,w2 as	(
				select
						(s->>'target_variable')::integer			as target_variable,
						(s->>'country')::varchar					as country,
						(s->>'strata_set')::varchar					as strata_set,
						(s->>'stratum')::varchar					as stratum,
						(s->>'reference_year_set')::varchar			as reference_year_set,
						(s->>'panel')::varchar						as panel,
						(s->>'cluster')::varchar					as cluster,
						(s->>'cluster_configuration')::varchar		as cluster_configuration,
						(s->>'inventory_campaign')::varchar			as inventory_campaign,
						(s->>'plot')::varchar						as plot
				from w1
				)
		,w3 as	(
				select		
						t_panel.id as t_panel__id,
						t_panel.panel as t_panel__panel,
						t_cluster.id as t_cluster__id,
						t_cluster.cluster as t_cluster__cluster,
						f_p_plot.gid as f_p_plot__gid,
						f_p_plot.plot as f_p_plot__plot,
						t_cluster_configuration.id as t_cluster_configuration__id,
						t_cluster_configuration.cluster_configuration as t_cluster_configuration__cluster_configuration,
						t_stratum.id as t_stratum__id,
						t_stratum.stratum as t_stratum__stratum,
						t_strata_set.id as t_strata_set__id,
						t_strata_set.strata_set as t_strata_set__strata_set,
						c_country.id as c_country__id,
						c_country.label as c_country__label,
						t_reference_year_set.id as t_reference_year_set__id,
						t_reference_year_set.reference_year_set as t_reference_year_set__reference_year_set,
						t_inventory_campaign.id as t_inventory_campaign__id,
						t_inventory_campaign.inventory as t_inventory_campaign__inventory
				from
							sdesign.cm_refyearset2panel_mapping
				inner join	sdesign.t_panel on t_panel.id = cm_refyearset2panel_mapping.panel
				inner join	sdesign.cm_cluster2panel_mapping on cm_cluster2panel_mapping.panel = t_panel.id
				inner join	sdesign.t_cluster on t_cluster.id = cm_cluster2panel_mapping."cluster"
				inner join	sdesign.f_p_plot on f_p_plot."cluster" = t_cluster.id
				inner join	sdesign.cm_plot2cluster_config_mapping on f_p_plot.gid = cm_plot2cluster_config_mapping.plot
				inner join	sdesign.t_cluster_configuration on (t_panel.cluster_configuration = t_cluster_configuration.id and cm_plot2cluster_config_mapping.cluster_configuration = t_cluster_configuration.id)
				inner join	sdesign.t_stratum on t_panel.stratum = t_stratum.id
				inner join	sdesign.t_strata_set on t_stratum.strata_set = t_strata_set.id
				inner join	sdesign.c_country on t_strata_set.country = c_country.id
				inner join	sdesign.t_reference_year_set on cm_refyearset2panel_mapping.reference_year_set = t_reference_year_set.id
				inner join	sdesign.t_inventory_campaign on t_reference_year_set.inventory_campaign = t_inventory_campaign.id
				where
						cm_refyearset2panel_mapping.id in (select unnest(_array_cmrys2pm))
				)
		,w4 as	(
				select
						w2.*, w3.f_p_plot__gid
				from
						w2 inner join w3
						on w2.plot = w3.f_p_plot__plot
						and w2.cluster = w3.t_cluster__cluster
						and w2.panel = w3.t_panel__panel
						and w2.stratum = w3.t_stratum__stratum
						and w2.reference_year_set = w3.t_reference_year_set__reference_year_set
						and w2.inventory_campaign = w3.t_inventory_campaign__inventory
						and w2.cluster_configuration = w3.t_cluster_configuration__cluster_configuration
						and w2.strata_set = w3.t_strata_set__strata_set
						and w2.country = w3.c_country__label
				)
		select count(w4.*) from w4
		into _check_count_records_after_inner_join;
		---------------------------------------------------------------------------------
		-- check records
		if _check_count_records_input_json is distinct from _check_count_records_after_inner_join
		then
				raise exception 'Error 13: fn_etl_import_ldsity_values: Composite key in input JSON elements is not compatible with SDESIGN on target DB!';
		end if;
		---------------------------------------------------------------------------------
		---------------------------------------------------------------------------------
		-- INSERT available datasets INTO t_available_datasets table
		with
		w1 as	(
				select json_array_elements((_available_datasets_and_ldsity_values->>'available_datasets')::json) as s
				)
		,w2 as	(
				select
						(s->>'target_variable')::integer			as target_variable,
						(s->>'country')::varchar					as country,
						(s->>'strata_set')::varchar					as strata_set,
						(s->>'stratum')::varchar					as stratum,
						(s->>'reference_year_set')::varchar			as reference_year_set,
						(s->>'panel')::varchar						as panel,
						(s->>'cluster_configuration')::varchar		as cluster_configuration,
						(s->>'inventory_campaign')::varchar			as inventory_campaign,
						(s->>'sub_population_category')::integer	as sub_population_category,
						(s->>'area_domain_category')::integer		as area_domain_category
				from w1
				)
		,w3 as	(
				select distinct t.t_panel__id, t.t_panel__panel, t.t_cluster_configuration__id, t.t_cluster_configuration__cluster_configuration,
				t.t_stratum__id, t.t_stratum__stratum, t.t_strata_set__id, t.t_strata_set__strata_set, t.c_country__id, t.c_country__label,
				t.t_reference_year_set__id, t.t_reference_year_set__reference_year_set, t.t_inventory_campaign__id, t.t_inventory_campaign__inventory
				from
						(
						select		
								t_panel.id as t_panel__id,
								t_panel.panel as t_panel__panel,
								t_cluster.id as t_cluster__id,
								t_cluster.cluster as t_cluster__cluster,
								f_p_plot.gid as f_p_plot__gid,
								f_p_plot.plot as f_p_plot__plot,
								t_cluster_configuration.id as t_cluster_configuration__id,
								t_cluster_configuration.cluster_configuration as t_cluster_configuration__cluster_configuration,
								t_stratum.id as t_stratum__id,
								t_stratum.stratum as t_stratum__stratum,
								t_strata_set.id as t_strata_set__id,
								t_strata_set.strata_set as t_strata_set__strata_set,
								c_country.id as c_country__id,
								c_country.label as c_country__label,
								t_reference_year_set.id as t_reference_year_set__id,
								t_reference_year_set.reference_year_set as t_reference_year_set__reference_year_set,
								t_inventory_campaign.id as t_inventory_campaign__id,
								t_inventory_campaign.inventory as t_inventory_campaign__inventory
						from
									sdesign.cm_refyearset2panel_mapping
						inner join	sdesign.t_panel on t_panel.id = cm_refyearset2panel_mapping.panel
						inner join	sdesign.cm_cluster2panel_mapping on cm_cluster2panel_mapping.panel = t_panel.id
						inner join	sdesign.t_cluster on t_cluster.id = cm_cluster2panel_mapping."cluster"
						inner join	sdesign.f_p_plot on f_p_plot."cluster" = t_cluster.id
						inner join	sdesign.cm_plot2cluster_config_mapping on f_p_plot.gid = cm_plot2cluster_config_mapping.plot
						inner join	sdesign.t_cluster_configuration on (t_panel.cluster_configuration = t_cluster_configuration.id and cm_plot2cluster_config_mapping.cluster_configuration = t_cluster_configuration.id)
						inner join	sdesign.t_stratum on t_panel.stratum = t_stratum.id
						inner join	sdesign.t_strata_set on t_stratum.strata_set = t_strata_set.id
						inner join	sdesign.c_country on t_strata_set.country = c_country.id
						inner join	sdesign.t_reference_year_set on cm_refyearset2panel_mapping.reference_year_set = t_reference_year_set.id
						inner join	sdesign.t_inventory_campaign on t_reference_year_set.inventory_campaign = t_inventory_campaign.id
						where
								cm_refyearset2panel_mapping.id in (select unnest(_array_cmrys2pm_ad))
						) as t
				)
		,w4 as	(
				select
						w2.*,
						w3.t_panel__id,
						w3.t_reference_year_set__id
				from
						w2 inner join w3
						on w2.panel = w3.t_panel__panel
						and w2.stratum = w3.t_stratum__stratum
						and w2.reference_year_set = w3.t_reference_year_set__reference_year_set
						and w2.inventory_campaign = w3.t_inventory_campaign__inventory
						and w2.cluster_configuration = w3.t_cluster_configuration__cluster_configuration
						and w2.strata_set = w3.t_strata_set__strata_set
						and w2.country = w3.c_country__label
				)
		,w5 as	(
				select
						id,
						target_variable,
						case when sub_population_category is null then 0 else sub_population_category end as sub_population_category,
						case when area_domain_category is null then 0 else area_domain_category end as area_domain_category
				from
						@extschema@.t_variable 
				where target_variable = (select distinct w2.target_variable from w2)
				)
		,w6 as	(
				select
						w4.target_variable,
						w4.t_panel__id as panel,
						w4.t_reference_year_set__id as reference_year_set,
						w5.id as variable
				from
						w4 inner join w5 on (w4.sub_population_category = w5.sub_population_category and w4.area_domain_category = w5.area_domain_category)
				)
		,w7 as	(
				select distinct w6.panel, w6.reference_year_set, w6.variable from w6
				except
				select panel, reference_year_set, variable from @extschema@.t_available_datasets
				)
		insert into @extschema@.t_available_datasets(panel,reference_year_set,variable,last_change)
		select w7.panel, w7.reference_year_set, w7.variable, now() from w7
		order by w7.panel, w7.reference_year_set, w7.variable;
		---------------------------------------------------------------------------------
		---------------------------------------------------------------------------------
		-- INSERT ldsity values INTO t_target_data table
		with
		w1 as	(
				select json_array_elements((_available_datasets_and_ldsity_values->>'ldsity_values')::json) as s
				)
		,w2 as	(
				select
						(s->>'target_variable')::integer			as target_variable,
						(s->>'country')::varchar					as country,
						(s->>'strata_set')::varchar					as strata_set,
						(s->>'stratum')::varchar					as stratum,
						(s->>'reference_year_set')::varchar			as reference_year_set,
						(s->>'panel')::varchar						as panel,
						(s->>'cluster')::varchar					as cluster,
						(s->>'cluster_configuration')::varchar		as cluster_configuration,
						(s->>'inventory_campaign')::varchar			as inventory_campaign,
						(s->>'plot')::varchar						as plot,
						(s->>'sub_population_category')::integer	as sub_population_category,
						(s->>'area_domain_category')::integer		as area_domain_category,
						(s->>'value')::float						as value
				from w1
				)
		,w3 as	(
				select		
						t_panel.id as t_panel__id,
						t_panel.panel as t_panel__panel,
						t_cluster.id as t_cluster__id,
						t_cluster.cluster as t_cluster__cluster,
						f_p_plot.gid as f_p_plot__gid,
						f_p_plot.plot as f_p_plot__plot,
						t_cluster_configuration.id as t_cluster_configuration__id,
						t_cluster_configuration.cluster_configuration as t_cluster_configuration__cluster_configuration,
						t_stratum.id as t_stratum__id,
						t_stratum.stratum as t_stratum__stratum,
						t_strata_set.id as t_strata_set__id,
						t_strata_set.strata_set as t_strata_set__strata_set,
						c_country.id as c_country__id,
						c_country.label as c_country__label,
						t_reference_year_set.id as t_reference_year_set__id,
						t_reference_year_set.reference_year_set as t_reference_year_set__reference_year_set,
						t_inventory_campaign.id as t_inventory_campaign__id,
						t_inventory_campaign.inventory as t_inventory_campaign__inventory
				from
							sdesign.cm_refyearset2panel_mapping
				inner join	sdesign.t_panel on t_panel.id = cm_refyearset2panel_mapping.panel
				inner join	sdesign.cm_cluster2panel_mapping on cm_cluster2panel_mapping.panel = t_panel.id
				inner join	sdesign.t_cluster on t_cluster.id = cm_cluster2panel_mapping."cluster"
				inner join	sdesign.f_p_plot on f_p_plot."cluster" = t_cluster.id
				inner join	sdesign.cm_plot2cluster_config_mapping on f_p_plot.gid = cm_plot2cluster_config_mapping.plot
				inner join	sdesign.t_cluster_configuration on (t_panel.cluster_configuration = t_cluster_configuration.id and cm_plot2cluster_config_mapping.cluster_configuration = t_cluster_configuration.id)
				inner join	sdesign.t_stratum on t_panel.stratum = t_stratum.id
				inner join	sdesign.t_strata_set on t_stratum.strata_set = t_strata_set.id
				inner join	sdesign.c_country on t_strata_set.country = c_country.id
				inner join	sdesign.t_reference_year_set on cm_refyearset2panel_mapping.reference_year_set = t_reference_year_set.id
				inner join	sdesign.t_inventory_campaign on t_reference_year_set.inventory_campaign = t_inventory_campaign.id
				where
						cm_refyearset2panel_mapping.id in (select unnest(_array_cmrys2pm))
				)		
		,w4 as	(
				select
						w2.*,
						w3.f_p_plot__gid,
						w3.t_panel__id,
						w3.t_reference_year_set__id
				from
						w2 inner join w3
						on w2.plot = w3.f_p_plot__plot
						and w2.cluster = w3.t_cluster__cluster
						and w2.panel = w3.t_panel__panel
						and w2.stratum = w3.t_stratum__stratum
						and w2.reference_year_set = w3.t_reference_year_set__reference_year_set
						and w2.inventory_campaign = w3.t_inventory_campaign__inventory
						and w2.cluster_configuration = w3.t_cluster_configuration__cluster_configuration
						and w2.strata_set = w3.t_strata_set__strata_set
						and w2.country = w3.c_country__label
				)
		,w5 as	(
				select
						id,
						target_variable,
						case when sub_population_category is null then 0 else sub_population_category end as sub_population_category,
						case when area_domain_category is null then 0 else area_domain_category end as area_domain_category
				from
						@extschema@.t_variable 
				where target_variable = (select distinct w2.target_variable from w2)
				)
		,w6 as	(
				select
						w4.target_variable,
						w4.f_p_plot__gid as plot,
						w4.value,
						w4.t_panel__id as panel,
						w4.t_reference_year_set__id as reference_year_set,
						w5.id as variable
				from
						w4 inner join w5 on (w4.sub_population_category = w5.sub_population_category and w4.area_domain_category = w5.area_domain_category)
				)
		,w7 as	(
				select distinct w6.target_variable, w6.plot, w6.panel, w6.value, w6.reference_year_set, w6.variable from w6
				)
		,w10 as	(
				select w7.*, tad.id as available_datasets
				from w7 inner join @extschema@.t_available_datasets as tad
				on	(
					w7.panel = tad.panel and
					w7.reference_year_set = tad.reference_year_set and
					w7.variable = tad.variable
					)
				)
		,w_ttd as	(
					select
							ttd.id,
							ttd.plot,
							(	select t_panel.id as panel_upr
								from 		sdesign.f_p_plot
								inner join 	sdesign.t_cluster on (f_p_plot.cluster = t_cluster.id)
								inner join	sdesign.cm_cluster2panel_mapping on (t_cluster.id = cm_cluster2panel_mapping.cluster)
								inner join	sdesign.t_panel on (cm_cluster2panel_mapping.panel = t_panel.id)
								inner join	sdesign.t_cluster_configuration on (t_panel.cluster_configuration = t_cluster_configuration.id)
								inner join	sdesign.cm_plot2cluster_config_mapping on (t_cluster_configuration.id = cm_plot2cluster_config_mapping.cluster_configuration and
																f_p_plot.gid = cm_plot2cluster_config_mapping.plot)
								where f_p_plot.gid = coalesce(w10.plot,ttd.plot)
							),
							ttd.value,
							coalesce(w10.plot,ttd.plot) as plot_upr,
							coalesce(w10.available_datasets, ttd.available_datasets) as available_datasets_upr,
							case
								when ttd.value is     null and w10.value is not null then w10.value
								when ttd.value is not null and ttd.value is distinct from 0.0 and w10.value is null then 0.0
								when ttd.value is not null and ttd.value = 0.0 and w10.value is null then null::double precision
								else w10.value
							end
								as value_upr,
							case
								when ttd.value is     null and w10.value is not null then false
								when ttd.value is not null and ttd.value is distinct from 0.0 and w10.value is null then false
								when ttd.value is not null and ttd.value = 0.0 and w10.value is null then null::boolean									
								else
									case
										when ttd.value = 0.0 and w10.value = 0.0 then true
										when ttd.value = 0.0 and w10.value is distinct from 0.0
												then
													case
														when (abs(1 - (ttd.value / w10.value)) * 100.0) <= _threshold then true
														else false
													end
										when ttd.value is distinct from 0.0 and w10.value = 0.0
												then
													case
														when (abs(1 - (w10.value / ttd.value)) * 100.0) <= _threshold then true
														else false
													end
										else
											case
												when (abs(1 - (ttd.value / w10.value)) * 100.0) <= _threshold then true
												else false
											end
									end									
							end
								as value_identic				
					from
						(
						select * 
						from @extschema@.t_target_data
						where available_datasets in (
							select id 
							from @extschema@.t_available_datasets 
							where panel in (select distinct panel from w10)
							and variable in (select id from @extschema@.t_variable where target_variable = (select distinct w2.target_variable from w2))
							and reference_year_set in (select distinct w4.t_reference_year_set__id from w4)
						)
						and plot in (select distinct w10.plot from w10)
						and is_latest = true
						) as ttd
					full outer join w10
							on (ttd.plot = w10.plot and ttd.available_datasets = w10.available_datasets)
					)
		,w_update as	(
						update @extschema@.t_target_data set is_latest = false where id in
						(select id from w_ttd where value_identic = false and id is not null)
						returning t_target_data.plot, t_target_data.available_datasets
						)
		insert into @extschema@.t_target_data(plot,value,value_inserted,is_latest,available_datasets)
		select
				w_ttd.plot_upr as plot,
				w_ttd.value_upr as value,
				now() as value_inserted,
				true as is_latest,
				available_datasets_upr
		from
				w_ttd
				left join w_update on w_ttd.plot_upr = w_update.plot and w_ttd.available_datasets_upr = w_update.available_datasets
		where
				w_ttd.value_identic = false
		order
				by w_ttd.plot_upr, w_ttd.available_datasets_upr;

	_res_ttd := concat('The ',(select count(*) from @extschema@.t_target_data where id > _max_id_ttd),' new local densities were inserted for ',(select count(t.plot) from (select distinct plot from @extschema@.t_target_data where id > _max_id_ttd) as t),' plots.');
	_res_tad := concat('The ',(select count(*) from @extschema@.t_available_datasets where id > _max_id_tad),' new rows were inserted into t_available_datasets.');
			
	_res := _res_ttd || ' ' || _res_tad;

	return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_import_ldsity_values(json, double precision) IS
'The function for the specified list of input arguments inserts data into the t_available_datasets table and inserts data into the t_target_data table (aggregated local density at the plot level).';

grant execute on function @extschema@.fn_etl_import_ldsity_values(json, double precision) to public;
-- </function>