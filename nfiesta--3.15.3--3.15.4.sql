-- Copyright 2017, 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.




-- <function name="fn_etl_check_area_domains4update" schema="extschema" src="functions/extschema/etl/fn_etl_check_area_domains4update.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_check_area_domains4update(json)

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_check_area_domains4update(json) CASCADE;

create or replace function @extschema@.fn_etl_check_area_domains4update
(
	_metadatas json
)
returns table
(
	area_domain				integer,
	label_source			varchar,
	description_source		text,
	label_en_source			varchar,
	description_en_source	text,
	label_target			varchar,
	description_target		text,
	label_en_target			varchar,
	description_en_target	text,
	check_label				boolean,
	check_description		boolean,
	check_label_en			boolean,
	check_description_en	boolean
)
as
$$
declare
begin
		if _metadatas is null
		then
			raise exception 'Error 01: fn_etl_check_area_domains4update: Input argument _metadatas must not be NULL!';
		end if;
		-----------------------------------------
		return query
		with
		w1 as 	(
				select _metadatas as metadatas
				)
		,w2 as	(
				select json_array_elements(w1.metadatas) as metadatas from w1
				)
		,w3 as	(
				select
						(w2.metadatas->>'area_domain')::integer as area_domain_target,
						(w2.metadatas->>'label')::varchar as label_source,
						(w2.metadatas->>'description')::text as description_source,
						(w2.metadatas->>'label_en')::varchar as label_en_source,
						(w2.metadatas->>'description_en')::text as description_en_source
				from
						w2
				)
		,w4 as	(
				select
						w3.*,
						cad.label as label_target,
						cad.description as description_target,
						cad.label_en as label_en_target,
						cad.description_en as description_en_target
				from
						w3
						inner join @extschema@.c_area_domain as cad
						on w3.area_domain_target = cad.id
				)				
		,w5 as	(
				select
						w4.*,
						case when w4.label_source = w4.label_target then true else false end as check_label,
						case when w4.description_source = w4.description_target then true else false end as check_description,
						case when w4.label_en_source = w4.label_en_target then true else false end as check_label_en,
						case when w4.description_en_source = w4.description_en_target then true else false end as check_description_en
				from
						w4
				)
		select
				w5.area_domain_target as area_domain,
				w5.label_source,
				w5.description_source,
				w5.label_en_source,
				w5.description_en_source,
				w5.label_target,
				w5.description_target,
				w5.label_en_target,
				w5.description_en_target,
				w5.check_label,
				w5.check_description,
				w5.check_label_en,
				w5.check_description_en
		from
				w5
		where
				w5.check_label = false or
				w5.check_description = false or
				w5.check_label_en = false or
				w5.check_description_en = false;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_check_area_domains4update(json) is
'The tunction returns records of area domains with informations whether their source labels or descriptions are different from target.';

grant execute on function @extschema@.fn_etl_check_area_domains4update(json) to public;
-- </function>



-- <function name="fn_etl_check_sub_populations4update" schema="extschema" src="functions/extschema/etl/fn_etl_check_sub_populations4update.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_check_sub_populations4update(json)

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_check_sub_populations4update(json) CASCADE;

create or replace function @extschema@.fn_etl_check_sub_populations4update
(
	_metadatas json
)
returns table
(
	sub_population			integer,
	label_source			varchar,
	description_source		text,
	label_en_source			varchar,
	description_en_source	text,
	label_target			varchar,
	description_target		text,
	label_en_target			varchar,
	description_en_target	text,
	check_label				boolean,
	check_description		boolean,
	check_label_en			boolean,
	check_description_en	boolean
)
as
$$
declare
begin
		if _metadatas is null
		then
			raise exception 'Error 01: fn_etl_check_sub_populations4update: Input argument _metadatas must not be NULL!';
		end if;
		-----------------------------------------
		return query
		with
		w1 as 	(
				select _metadatas as metadatas
				)
		,w2 as	(
				select json_array_elements(w1.metadatas) as metadatas from w1
				)
		,w3 as	(
				select
						(w2.metadatas->>'sub_population')::integer as sub_population_target,
						(w2.metadatas->>'label')::varchar as label_source,
						(w2.metadatas->>'description')::text as description_source,
						(w2.metadatas->>'label_en')::varchar as label_en_source,
						(w2.metadatas->>'description_en')::text as description_en_source
				from
						w2
				)
		,w4 as	(
				select
						w3.*,
						csp.label as label_target,
						csp.description as description_target,
						csp.label_en as label_en_target,
						csp.description_en as description_en_target
				from
						w3
						inner join @extschema@.c_sub_population as csp
						on w3.sub_population_target = csp.id
				)				
		,w5 as	(
				select
						w4.*,
						case when w4.label_source = w4.label_target then true else false end as check_label,
						case when w4.description_source = w4.description_target then true else false end as check_description,
						case when w4.label_en_source = w4.label_en_target then true else false end as check_label_en,
						case when w4.description_en_source = w4.description_en_target then true else false end as check_description_en
				from
						w4
				)
		select
				w5.sub_population_target as sub_population,
				w5.label_source,
				w5.description_source,
				w5.label_en_source,
				w5.description_en_source,
				w5.label_target,
				w5.description_target,
				w5.label_en_target,
				w5.description_en_target,
				w5.check_label,
				w5.check_description,
				w5.check_label_en,
				w5.check_description_en
		from
				w5
		where
				w5.check_label = false or
				w5.check_description = false or
				w5.check_label_en = false or
				w5.check_description_en = false;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_check_sub_populations4update(json) is
'The tunction returns records of sub populations with informations whether their source labels or descriptions are different from target.';

grant execute on function @extschema@.fn_etl_check_sub_populations4update(json) to public;
-- </function>



-- <function name="fn_etl_update_area_domain_label" schema="extschema" src="functions/extschema/etl/fn_etl_update_area_domain_label.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_update_area_domain_label(integer, character varying, varchar)

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_update_area_domain_label(integer, character varying, varchar) CASCADE;

create or replace function @extschema@.fn_etl_update_area_domain_label
(
	_area_domain		integer,
	_national_language	character varying(2),
	_label				varchar
)
returns text
as
$$
declare
		_column							text;
		_atomic_area_domain_category	integer;
		_res							text;
begin
		if _area_domain is null
		then
			raise exception 'Error 01: fn_etl_update_area_domain_label: Input argument _area_domain must not be NULL!';
		end if;
	
		if _national_language is null
		then
			raise exception 'Error 02: fn_etl_update_area_domain_label: Input argument _national_language must not be NULL!';
		end if;
	
		if _label is null
		then
			raise exception 'Error 03: fn_etl_update_area_domain_label: Input argument _label must not be NULL!';
		end if;
		-----------------------------------------
		if	(
			select count(cad.*) is distinct from 1
			from @extschema@.c_area_domain as cad
			where cad.id = _area_domain
			)
		then
			raise exception 'Error 04: fn_etl_update_area_domain_label: Input argument _area_domain (ID) = % is not present in c_area_domain table!',_area_domain;
		end if;
		-----------------------------------------
		if	(
			select cad.atomic = false
			from @extschema@.c_area_domain as cad
			where cad.id = _area_domain
			)
		then
			raise exception 'Error 05: fn_etl_update_area_domain_label: Input argument _area_domain (ID) = % is not ATOMIC area domain!',_area_domain;
		end if;			
		-----------------------------------------
		-- UPDATE label of ATOMIC area domain
		if _national_language = 'en'::varchar
		then
			update @extschema@.c_area_domain set label_en = _label where id = _area_domain;

			_column := 'label_en';
		else
			update @extschema@.c_area_domain set label = _label where id = _area_domain;

			_column := 'label';
		end if;
		-----------------------------------------
		-- UPDATE label of NON-ATOMIC area domain
		select cadc.id from @extschema@.c_area_domain_category as cadc
		where cadc.area_domain = _area_domain order by cadc.id limit 1
		into _atomic_area_domain_category;

		if _atomic_area_domain_category is null
		then
			raise exception 'Error 06: fn_etl_update_area_domain_label: For input argument _area_domain (ID) = % not exists any category in c_area_domain_category table!',_area_domain;
		end if;
	
		execute
		'
		with
		w1 as	(
				select * from @extschema@.cm_area_domain_category where area_domain_category in
				(select area_domain_category from @extschema@.cm_area_domain_category where atomic_category = $1)
				)
		,w2 as	(
				select
						w1.id,
						w1.area_domain_category, 			-- id_target_db of non-atomic category 
						w1.atomic_category,						-- id_target_db of atomic category
						t1.area_domain, 						-- id_target_db of atomic type
						t1.'|| _column ||' as label_category,	-- label atomic category
						t2.'|| _column ||' as label_type		-- label atomic type
				from
						w1
						inner join @extschema@.c_area_domain_category as t1 on w1.atomic_category = t1.id
						inner join @extschema@.c_area_domain as t2 on t1.area_domain = t2.id
				)
		---------
		,w3 as	(
				select
						id,
						area_domain,
						string_to_array('|| _column ||','';'') as label_category 
				from
						@extschema@.c_area_domain_category
				where
						id in (select w2.area_domain_category from w2)
				)
		,w4 as	(
				select
						w3.*,
						(select array_agg(t.res order by t.res) from (select generate_series(1,array_length(w3.label_category,1)) as res) as t) as id_order
				from
						w3
				)
		,w5 as	(
				select
						w4.id,
						w4.area_domain,
						unnest(w4.label_category) as label_category,
						unnest(w4.id_order) as id_order
				from
						w4
				)
		--------
		,w6 as	(
				select w2.*, w5.area_domain as area_domain_non_atomic, w5.id_order from w2 inner join w5
				on w2.area_domain_category = w5.id and w2.label_category = w5.label_category
				)
		,w7 as	(
				select distinct w6.area_domain_non_atomic, w6.label_type, w6.id_order from w6
				)
		,w8 as	(
				select
						w7.area_domain_non_atomic,
						array_agg(w7.label_type order by w7.id_order) as label_type,
						array_agg(w7.id_order order by w7.id_order) as id_order
				from
						w7 group by w7.area_domain_non_atomic
				)
		,w9 as	(
				select
						w8.area_domain_non_atomic,
						array_to_string(w8.label_type,'';'') as label4update
				from
						w8
				)
		update @extschema@.c_area_domain set '|| _column ||' = w9.label4update
		from w9 where c_area_domain.id = w9.area_domain_non_atomic;
		'
		using _atomic_area_domain_category;
		-----------------------------------------
		_res := concat('The label of area domain [c_area_domain.id = ',_area_domain,'] for language element = "',_national_language,'" was changed. The update was done for ATOMIC and NON-ATOMIC area domains.');
		-----------------------------------------
		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_update_area_domain_label(integer, character varying, varchar) is
'The tunction updates a area domain label in c_area_domain table for given area domain.';

grant execute on function @extschema@.fn_etl_update_area_domain_label(integer, character varying, varchar) to public;
-- </function>



-- <function name="fn_etl_update_sub_population_label" schema="extschema" src="functions/extschema/etl/fn_etl_update_sub_population_label.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_update_sub_population_label(integer, character varying, varchar)

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_update_sub_population_label(integer, character varying, varchar) CASCADE;

create or replace function @extschema@.fn_etl_update_sub_population_label
(
	_sub_population		integer,
	_national_language	character varying(2),
	_label				varchar
)
returns text
as
$$
declare
		_column							text;
		_atomic_sub_population_category	integer;
		_res							text;
begin
		if _sub_population is null
		then
			raise exception 'Error 01: fn_etl_update_sub_population_label: Input argument _sub_population must not be NULL!';
		end if;
	
		if _national_language is null
		then
			raise exception 'Error 02: fn_etl_update_sub_population_label: Input argument _national_language must not be NULL!';
		end if;
	
		if _label is null
		then
			raise exception 'Error 03: fn_etl_update_sub_population_label: Input argument _label must not be NULL!';
		end if;
		-----------------------------------------
		if	(
			select count(csp.*) is distinct from 1
			from @extschema@.c_sub_population as csp
			where csp.id = _sub_population
			)
		then
			raise exception 'Error 04: fn_etl_update_sub_population_label: Input argument _sub_population (ID) = % is not present in c_sub_population table!',_sub_population;
		end if;
		-----------------------------------------
		if	(
			select csp.atomic = false
			from @extschema@.c_sub_population as csp
			where csp.id = _sub_population
			)
		then
			raise exception 'Error 05: fn_etl_update_sub_population_label: Input argument _sub_population (ID) = % is not ATOMIC sub population!',_sub_population;
		end if;			
		-----------------------------------------
		-- UPDATE label of ATOMIC sub population
		if _national_language = 'en'::varchar
		then
			update @extschema@.c_sub_population set label_en = _label where id = _sub_population;

			_column := 'label_en';
		else
			update @extschema@.c_sub_population set label = _label where id = _sub_population;

			_column := 'label';
		end if;
		-----------------------------------------
		-- UPDATE label of NON-ATOMIC sub population
		select cspc.id from @extschema@.c_sub_population_category as cspc
		where cspc.sub_population = _sub_population order by cspc.id limit 1
		into _atomic_sub_population_category;

		if _atomic_sub_population_category is null
		then
			raise exception 'Error 06: fn_etl_update_sub_population_label: For input argument _sub_population (ID) = % not exists any category in c_sub_population_category table!',_sub_population;
		end if;
	
		execute
		'
		with
		w1 as	(
				select * from @extschema@.cm_sub_population_category where sub_population_category in
				(select sub_population_category from @extschema@.cm_sub_population_category where atomic_category = $1)
				)
		,w2 as	(
				select
						w1.id,
						w1.sub_population_category, 			-- id_target_db of non-atomic category 
						w1.atomic_category,						-- id_target_db of atomic category
						t1.sub_population, 						-- id_target_db of atomic type
						t1.'|| _column ||' as label_category,	-- label atomic category
						t2.'|| _column ||' as label_type		-- label atomic type
				from
						w1
						inner join @extschema@.c_sub_population_category as t1 on w1.atomic_category = t1.id
						inner join @extschema@.c_sub_population as t2 on t1.sub_population = t2.id
				)
		---------
		,w3 as	(
				select
						id,
						sub_population,
						string_to_array('|| _column ||','';'') as label_category 
				from
						@extschema@.c_sub_population_category
				where
						id in (select w2.sub_population_category from w2)
				)
		,w4 as	(
				select
						w3.*,
						(select array_agg(t.res order by t.res) from (select generate_series(1,array_length(w3.label_category,1)) as res) as t) as id_order
				from
						w3
				)
		,w5 as	(
				select
						w4.id,
						w4.sub_population,
						unnest(w4.label_category) as label_category,
						unnest(w4.id_order) as id_order
				from
						w4
				)
		--------
		,w6 as	(
				select w2.*, w5.sub_population as sub_population_non_atomic, w5.id_order from w2 inner join w5
				on w2.sub_population_category = w5.id and w2.label_category = w5.label_category
				)
		,w7 as	(
				select distinct w6.sub_population_non_atomic, w6.label_type, w6.id_order from w6
				)
		,w8 as	(
				select
						w7.sub_population_non_atomic,
						array_agg(w7.label_type order by w7.id_order) as label_type,
						array_agg(w7.id_order order by w7.id_order) as id_order
				from
						w7 group by w7.sub_population_non_atomic
				)
		,w9 as	(
				select
						w8.sub_population_non_atomic,
						array_to_string(w8.label_type,'';'') as label4update
				from
						w8
				)
		update @extschema@.c_sub_population set '|| _column ||' = w9.label4update
		from w9 where c_sub_population.id = w9.sub_population_non_atomic;
		'
		using _atomic_sub_population_category;
		-----------------------------------------
		_res := concat('The label of sub population [c_sub_population.id = ',_sub_population,'] for language element = "',_national_language,'" was changed. The update was done for ATOMIC and NON-ATOMIC sub populations.');
		-----------------------------------------
		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_update_sub_population_label(integer, character varying, varchar) is
'The tunction updates a sub population label in c_sub_population table for given sub population.';

grant execute on function @extschema@.fn_etl_update_sub_population_label(integer, character varying, varchar) to public;
-- </function>



-- <function name="fn_etl_update_area_domain_description" schema="extschema" src="functions/extschema/etl/fn_etl_update_area_domain_description.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_update_area_domain_description(integer, character varying, text)

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_update_area_domain_description(integer, character varying, text) CASCADE;

create or replace function @extschema@.fn_etl_update_area_domain_description
(
	_area_domain		integer,
	_national_language	character varying(2),
	_description		text
)
returns text
as
$$
declare
		_column							text;
		_atomic_area_domain_category	integer;
		_res							text;
begin
		if _area_domain is null
		then
			raise exception 'Error 01: fn_etl_update_area_domain_description: Input argument _area_domain must not be NULL!';
		end if;
	
		if _national_language is null
		then
			raise exception 'Error 02: fn_etl_update_area_domain_description: Input argument _national_language must not be NULL!';
		end if;
	
		if _description is null
		then
			raise exception 'Error 03: fn_etl_update_area_domain_description: Input argument _description must not be NULL!';
		end if;
		-----------------------------------------
		if	(
			select count(cad.*) is distinct from 1
			from @extschema@.c_area_domain as cad
			where cad.id = _area_domain
			)
		then
			raise exception 'Error 04: fn_etl_update_area_domain_description: Input argument _area_domain (ID) = % is not present in c_area_domain table!',_area_domain;
		end if;
		-----------------------------------------
		if	(
			select cad.atomic = false
			from @extschema@.c_area_domain as cad
			where cad.id = _area_domain
			)
		then
			raise exception 'Error 05: fn_etl_update_area_domain_description: Input argument _area_domain (ID) = % is not ATOMIC area domain!',_area_domain;
		end if;		
		-----------------------------------------
		-- UPDATE description of ATOMIC area domain
		if _national_language = 'en'::varchar
		then
			update @extschema@.c_area_domain set description_en = _description where id = _area_domain;

			_column := 'description_en';
		else
			update @extschema@.c_area_domain set description = _description where id = _area_domain;

			_column := 'description';
		end if;
		-----------------------------------------
		-- UPDATE description of NON-ATOMIC area domain
		select cadc.id from @extschema@.c_area_domain_category as cadc
		where cadc.area_domain = _area_domain order by cadc.id limit 1
		into _atomic_area_domain_category;

		if _atomic_area_domain_category is null
		then
			raise exception 'Error 06: fn_etl_update_area_domain_description: For input argument _area_domain (ID) = % not exists any category in c_area_domain_category table!',_area_domain;
		end if;
	
		execute
		'
		with
		w1 as	(
				select * from @extschema@.cm_area_domain_category where area_domain_category in
				(select area_domain_category from @extschema@.cm_area_domain_category where atomic_category = $1)
				)
		,w2 as	(
				select
						w1.id,
						w1.area_domain_category, 					-- id_target_db of non-atomic category 
						w1.atomic_category,							-- id_target_db of atomic category
						t1.area_domain, 							-- id_target_db of atomic type
						t1.'|| _column ||' as description_category,	-- description atomic category
						t2.'|| _column ||' as description_type		-- description atomic type
				from
						w1
						inner join @extschema@.c_area_domain_category as t1 on w1.atomic_category = t1.id
						inner join @extschema@.c_area_domain as t2 on t1.area_domain = t2.id
				)
		---------
		,w3 as	(
				select
						id,
						area_domain,
						string_to_array('|| _column ||','';'') as description_category 
				from
						@extschema@.c_area_domain_category
				where
						id in (select w2.area_domain_category from w2)
				)
		,w4 as	(
				select
						w3.*,
						(select array_agg(t.res order by t.res) from (select generate_series(1,array_length(w3.description_category,1)) as res) as t) as id_order
				from
						w3
				)
		,w5 as	(
				select
						w4.id,
						w4.area_domain,
						unnest(w4.description_category) as description_category,
						unnest(w4.id_order) as id_order
				from
						w4
				)
		--------
		,w6 as	(
				select w2.*, w5.area_domain as area_domain_non_atomic, w5.id_order from w2 inner join w5
				on w2.area_domain_category = w5.id and w2.description_category = w5.description_category
				)
		,w7 as	(
				select distinct w6.area_domain_non_atomic, w6.description_type, w6.id_order from w6
				)
		,w8 as	(
				select
						w7.area_domain_non_atomic,
						array_agg(w7.description_type order by w7.id_order) as description_type,
						array_agg(w7.id_order order by w7.id_order) as id_order
				from
						w7 group by w7.area_domain_non_atomic
				)
		,w9 as	(
				select
						w8.area_domain_non_atomic,
						array_to_string(w8.description_type,'';'') as description4update
				from
						w8
				)
		update @extschema@.c_area_domain set '|| _column ||' = w9.description4update
		from w9 where c_area_domain.id = w9.area_domain_non_atomic;
		'
		using _atomic_area_domain_category;
		-----------------------------------------
		_res := concat('The description of area domain [c_area_domain.id = ',_area_domain,'] for language element = "',_national_language,'" was changed. The update was done for ATOMIC and NON-ATOMIC area domains.');
		-----------------------------------------
		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_update_area_domain_description(integer, character varying, text) is
'The tunction updates an area domain description in c_area_domain table for given area domain.';

grant execute on function @extschema@.fn_etl_update_area_domain_description(integer, character varying, text) to public;
-- </function>




-- <function name="fn_etl_update_sub_population_description" schema="extschema" src="functions/extschema/etl/fn_etl_update_sub_population_description.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_update_sub_population_description(integer, character varying, text)

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_update_sub_population_description(integer, character varying, text) CASCADE;

create or replace function @extschema@.fn_etl_update_sub_population_description
(
	_sub_population		integer,
	_national_language	character varying(2),
	_description		text
)
returns text
as
$$
declare
		_column							text;
		_atomic_sub_population_category	integer;
		_res							text;
begin
		if _sub_population is null
		then
			raise exception 'Error 01: fn_etl_update_sub_population_description: Input argument _sub_population must not be NULL!';
		end if;
	
		if _national_language is null
		then
			raise exception 'Error 02: fn_etl_update_sub_population_description: Input argument _national_language must not be NULL!';
		end if;
	
		if _description is null
		then
			raise exception 'Error 03: fn_etl_update_sub_population_description: Input argument _description must not be NULL!';
		end if;
		-----------------------------------------
		if	(
			select count(csp.*) is distinct from 1
			from @extschema@.c_sub_population as csp
			where csp.id = _sub_population
			)
		then
			raise exception 'Error 04: fn_etl_update_sub_population_description: Input argument _sub_population (ID) = % is not present in c_sub_population table!',_sub_population;
		end if;
		-----------------------------------------
		if	(
			select csp.atomic = false
			from @extschema@.c_sub_population as csp
			where csp.id = _sub_population
			)
		then
			raise exception 'Error 05: fn_etl_update_sub_population_description: Input argument _sub_population (ID) = % is not ATOMIC sub population!',_sub_population;
		end if;			
		-----------------------------------------
		-- UPDATE description of ATOMIC sub population	
		if _national_language = 'en'::varchar
		then
			update @extschema@.c_sub_population set description_en = _description where id = _sub_population;

			_column := 'description_en';
		else
			update @extschema@.c_sub_population set description = _description where id = _sub_population;

			_column := 'description';
		end if;
		-----------------------------------------
		-- UPDATE description of NON-ATOMIC sub population
		select cspc.id from @extschema@.c_sub_population_category as cspc
		where cspc.sub_population = _sub_population order by cspc.id limit 1
		into _atomic_sub_population_category;

		if _atomic_sub_population_category is null
		then
			raise exception 'Error 06: fn_etl_update_sub_population_description: For input argument _sub_population (ID) = % not exists any category in c_sub_population_category table!',_sub_population;
		end if;
	
		execute
		'
		with
		w1 as	(
				select * from @extschema@.cm_sub_population_category where sub_population_category in
				(select sub_population_category from @extschema@.cm_sub_population_category where atomic_category = $1)
				)
		,w2 as	(
				select
						w1.id,
						w1.sub_population_category, 				-- id_target_db of non-atomic category 
						w1.atomic_category,							-- id_target_db of atomic category
						t1.sub_population, 							-- id_target_db of atomic type
						t1.'|| _column ||' as description_category,	-- description atomic category
						t2.'|| _column ||' as description_type		-- description atomic type
				from
						w1
						inner join @extschema@.c_sub_population_category as t1 on w1.atomic_category = t1.id
						inner join @extschema@.c_sub_population as t2 on t1.sub_population = t2.id
				)
		---------
		,w3 as	(
				select
						id,
						sub_population,
						string_to_array('|| _column ||','';'') as description_category 
				from
						@extschema@.c_sub_population_category
				where
						id in (select w2.sub_population_category from w2)
				)
		,w4 as	(
				select
						w3.*,
						(select array_agg(t.res order by t.res) from (select generate_series(1,array_length(w3.description_category,1)) as res) as t) as id_order
				from
						w3
				)
		,w5 as	(
				select
						w4.id,
						w4.sub_population,
						unnest(w4.description_category) as description_category,
						unnest(w4.id_order) as id_order
				from
						w4
				)
		--------
		,w6 as	(
				select w2.*, w5.sub_population as sub_population_non_atomic, w5.id_order from w2 inner join w5
				on w2.sub_population_category = w5.id and w2.description_category = w5.description_category
				)
		,w7 as	(
				select distinct w6.sub_population_non_atomic, w6.description_type, w6.id_order from w6
				)
		,w8 as	(
				select
						w7.sub_population_non_atomic,
						array_agg(w7.description_type order by w7.id_order) as description_type,
						array_agg(w7.id_order order by w7.id_order) as id_order
				from
						w7 group by w7.sub_population_non_atomic
				)
		,w9 as	(
				select
						w8.sub_population_non_atomic,
						array_to_string(w8.description_type,'';'') as description4update
				from
						w8
				)
		update @extschema@.c_sub_population set '|| _column ||' = w9.description4update
		from w9 where c_sub_population.id = w9.sub_population_non_atomic;
		'
		using _atomic_sub_population_category;
		-----------------------------------------
		_res := concat('The description of sub population [c_sub_population.id = ',_sub_population,'] for language element = "',_national_language,'" was changed. The update was done for ATOMIC and NON-ATOMIC sub populations.');
		-----------------------------------------
		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_update_sub_population_description(integer, character varying, text) is
'The tunction updates a sub population description in c_sub_population table for given sub population.';

grant execute on function @extschema@.fn_etl_update_sub_population_description(integer, character varying, text) to public;
-- </function>



-- <function name="fn_etl_update_area_domain_category_label" schema="extschema" src="functions/extschema/etl/fn_etl_update_area_domain_category_label.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_update_area_domain_category_label(integer, character varying, varchar)

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_update_area_domain_category_label(integer, character varying, varchar) CASCADE;

create or replace function @extschema@.fn_etl_update_area_domain_category_label
(
	_area_domain_category	integer,
	_national_language		character varying(2),
	_label					varchar
)
returns text
as
$$
declare
		_column						text;
		_label_category_original	varchar;
		_res						text;
begin
		if _area_domain_category is null
		then
			raise exception 'Error 01: fn_etl_update_area_domain_category_label: Input argument _area_domain_category must not be NULL!';
		end if;
	
		if _national_language is null
		then
			raise exception 'Error 02: fn_etl_update_area_domain_category_label: Input argument _national_language must not be NULL!';
		end if;
	
		if _label is null
		then
			raise exception 'Error 03: fn_etl_update_area_domain_category_label: Input argument _label must not be NULL!';
		end if;
		-----------------------------------------
		if	(
			select count(cadc.*) is distinct from 1
			from @extschema@.c_area_domain_category as cadc
			where cadc.id = _area_domain_category
			)
		then
			raise exception 'Error 04: fn_etl_update_area_domain_category_label: Input argument _area_domain_category (ID) = % is not present in c_area_domain_category table!',_area_domain_category;
		end if;
		-----------------------------------------
		if	(
			select cad.atomic = false
			from @extschema@.c_area_domain as cad
			where cad.id = (select cadc.area_domain from @extschema@.c_area_domain_category as cadc where cadc.id = _area_domain_category)
			)
		then
			raise exception 'Error 05: fn_etl_update_area_domain_category_label: Input argument _area_domain_category (ID) = % is not category from ATOMIC area domain!',_area_domain_category;
		end if;
		-----------------------------------------
		-- UPDATE label of ATOMIC area domain category
		if _national_language = 'en'::varchar
		then
			_column := 'label_en';
			select cadc.label_en from @extschema@.c_area_domain_category as cadc where cadc.id = _area_domain_category into _label_category_original;
			update @extschema@.c_area_domain_category set label_en = _label where id = _area_domain_category;
		else
			_column := 'label';
			select cadc.label from @extschema@.c_area_domain_category as cadc where cadc.id = _area_domain_category into _label_category_original;
			update @extschema@.c_area_domain_category set label = _label where id = _area_domain_category;
		end if;
		-----------------------------------------
		-- UPDATE label of NON-ATOMIC area domain category
		execute
		'
		with
		w1 as	(
				select * from @extschema@.cm_area_domain_category where area_domain_category in
				(select area_domain_category from @extschema@.cm_area_domain_category where atomic_category = $1)
				)
		,w2 as	(
				select
						w1.id,
						w1.area_domain_category,	 																		-- id_target_db of non-atomic category 
						w1.atomic_category,																					-- id_target_db of atomic category
						t1.'|| _column ||' as label_category_updated,														-- label atomic category updated
						case when w1.atomic_category = $1 then $2 else t1.'|| _column ||' end as label_category_original	-- label atomic category original
				from
						w1
						inner join @extschema@.c_area_domain_category as t1 on w1.atomic_category = t1.id
				)				
		---------
		,w3 as	(
				select
						id,
						string_to_array('|| _column ||','';'') as label_category 
				from
						@extschema@.c_area_domain_category
				where
						id in (select w2.area_domain_category from w2)
				)
		,w4 as	(
				select
						w3.*,
						(select array_agg(t.res order by t.res) from (select generate_series(1,array_length(w3.label_category,1)) as res) as t) as id_order
				from
						w3
				)
		,w5 as	(
				select
						w4.id,
						unnest(w4.label_category) as label_category,
						unnest(w4.id_order) as id_order
				from
						w4
				)
		--------
		,w6 as	(
				select w2.*, w5.id_order from w2 inner join w5
				on w2.area_domain_category = w5.id and w2.label_category_original = w5.label_category
				)		
		,w7 as	(
				select
						w6.area_domain_category,
						array_agg(w6.label_category_updated order by w6.id_order) as label_category,
						array_agg(w6.id_order order by w6.id_order) as id_order
				from
						w6 group by w6.area_domain_category
				)
		,w8 as	(
				select
						w7.area_domain_category,
						array_to_string(w7.label_category,'';'') as label4update
				from
						w7
				)
		update @extschema@.c_area_domain_category set '|| _column ||' = w8.label4update
		from w8 where c_area_domain_category.id = w8.area_domain_category;
		'
		using _area_domain_category, _label_category_original;
		-----------------------------------------
		_res := concat('The label of area domain category [c_area_domain_category.id = ',_area_domain_category,'] for language element = "',_national_language,'" was changed. The update was done for ATOMIC and NON-ATOMIC area domain categories.');
		-----------------------------------------
		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_update_area_domain_category_label(integer, character varying, varchar) is
'The tunction updates an area domain category label in c_area_domain_category table for given area domain category.';

grant execute on function @extschema@.fn_etl_update_area_domain_category_label(integer, character varying, varchar) to public;
-- </function>



-- <function name="fn_etl_update_area_domain_category_description" schema="extschema" src="functions/extschema/etl/fn_etl_update_area_domain_category_description.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_update_area_domain_category_description(integer, character varying, text)

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_update_area_domain_category_description(integer, character varying, text) CASCADE;

create or replace function @extschema@.fn_etl_update_area_domain_category_description
(
	_area_domain_category	integer,
	_national_language		character varying(2),
	_description			text
)
returns text
as
$$
declare
		_column							text;
		_description_category_original	text;
		_res							text;
begin
		if _area_domain_category is null
		then
			raise exception 'Error 01: fn_etl_update_area_domain_category_description: Input argument _area_domain_category must not be NULL!';
		end if;
	
		if _national_language is null
		then
			raise exception 'Error 02: fn_etl_update_area_domain_category_description: Input argument _national_language must not be NULL!';
		end if;
	
		if _description is null
		then
			raise exception 'Error 03: fn_etl_update_area_domain_category_description: Input argument _description must not be NULL!';
		end if;
		-----------------------------------------
		if	(
			select count(cadc.*) is distinct from 1
			from @extschema@.c_area_domain_category as cadc
			where cadc.id = _area_domain_category
			)
		then
			raise exception 'Error 04: fn_etl_update_area_domain_category_description: Input argument _area_domain_category (ID) = % is not present in c_area_domain_category table!',_area_domain_category;
		end if;
		-----------------------------------------
		if	(
			select cad.atomic = false
			from @extschema@.c_area_domain as cad
			where cad.id = (select cadc.area_domain from @extschema@.c_area_domain_category as cadc where cadc.id = _area_domain_category)
			)
		then
			raise exception 'Error 05: fn_etl_update_area_domain_category_description: Input argument _area_domain_category (ID) = % is not category from ATOMIC area domain!',_area_domain_category;
		end if;
		-----------------------------------------
		-- UPDATE description of ATOMIC area domain category		
		if _national_language = 'en'::varchar
		then
			_column := 'description_en';
			select cadc.description_en from @extschema@.c_area_domain_category as cadc where cadc.id = _area_domain_category into _description_category_original;
			update @extschema@.c_area_domain_category set description_en = _description where id = _area_domain_category;
		else
			_column := 'description';
			select cadc.description from @extschema@.c_area_domain_category as cadc where cadc.id = _area_domain_category into _description_category_original;
			update @extschema@.c_area_domain_category set description = _description where id = _area_domain_category;
		end if;
		-----------------------------------------
		-- UPDATE description of NON-ATOMIC area domain category
		execute
		'
		with
		w1 as	(
				select * from @extschema@.cm_area_domain_category where area_domain_category in
				(select area_domain_category from @extschema@.cm_area_domain_category where atomic_category = $1)
				)
		,w2 as	(
				select
						w1.id,
						w1.area_domain_category,	 																			-- id_target_db of non-atomic category 
						w1.atomic_category,																						-- id_target_db of atomic category
						t1.'|| _column ||' as description_category_updated,														-- description atomic category updated
						case when w1.atomic_category = $1 then $2 else t1.'|| _column ||' end as description_category_original	-- description atomic category
				from
						w1
						inner join @extschema@.c_area_domain_category as t1 on w1.atomic_category = t1.id
				)				
		---------
		,w3 as	(
				select
						id,
						string_to_array('|| _column ||','';'') as description_category 
				from
						@extschema@.c_area_domain_category
				where
						id in (select w2.area_domain_category from w2)
				)
		,w4 as	(
				select
						w3.*,
						(select array_agg(t.res order by t.res) from (select generate_series(1,array_length(w3.description_category,1)) as res) as t) as id_order
				from
						w3
				)
		,w5 as	(
				select
						w4.id,
						unnest(w4.description_category) as description_category,
						unnest(w4.id_order) as id_order
				from
						w4
				)
		--------
		,w6 as	(
				select w2.*, w5.id_order from w2 inner join w5
				on w2.area_domain_category = w5.id and w2.description_category_original = w5.description_category
				)		
		,w7 as	(
				select
						w6.area_domain_category,
						array_agg(w6.description_category_updated order by w6.id_order) as description_category,
						array_agg(w6.id_order order by w6.id_order) as id_order
				from
						w6 group by w6.area_domain_category
				)
		,w8 as	(
				select
						w7.area_domain_category,
						array_to_string(w7.description_category,'';'') as description4update
				from
						w7
				)
		update @extschema@.c_area_domain_category set '|| _column ||' = w8.description4update
		from w8 where c_area_domain_category.id = w8.area_domain_category;
		'
		using _area_domain_category, _description_category_original;		
		-----------------------------------------
		_res := concat('The description of area domain category [c_area_domain_category.id = ',_area_domain_category,'] for language element = "',_national_language,'" was changed. The update was done for ATOMIC and NON-ATOMIC area domain categories.');
		-----------------------------------------
		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_update_area_domain_category_description(integer, character varying, text) is
'The tunction updates an area domain category description in c_area_domain_category table for given area domain category.';

grant execute on function @extschema@.fn_etl_update_area_domain_category_description(integer, character varying, text) to public;
-- </function>



-- <function name="fn_etl_update_sub_population_category_label" schema="extschema" src="functions/extschema/etl/fn_etl_update_sub_population_category_label.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_update_sub_population_category_label(integer, character varying, varchar)

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_update_sub_population_category_label(integer, character varying, varchar) CASCADE;

create or replace function @extschema@.fn_etl_update_sub_population_category_label
(
	_sub_population_category	integer,
	_national_language			character varying(2),
	_label						varchar
)
returns text
as
$$
declare
		_column						text;
		_label_category_original	varchar;
		_res						text;
begin
		if _sub_population_category is null
		then
			raise exception 'Error 01: fn_etl_update_sub_population_category_label: Input argument _sub_population_category must not be NULL!';
		end if;
	
		if _national_language is null
		then
			raise exception 'Error 02: fn_etl_update_sub_population_category_label: Input argument _national_language must not be NULL!';
		end if;
	
		if _label is null
		then
			raise exception 'Error 03: fn_etl_update_sub_population_category_label: Input argument _label must not be NULL!';
		end if;
		-----------------------------------------
		if	(
			select count(cspc.*) is distinct from 1
			from @extschema@.c_sub_population_category as cspc
			where cspc.id = _sub_population_category
			)
		then
			raise exception 'Error 04: fn_etl_update_sub_population_category_label: Input argument _sub_population_category (ID) = % is not present in c_sub_population_category table!',_sub_population_category;
		end if;
		-----------------------------------------
		if	(
			select csp.atomic = false
			from @extschema@.c_sub_population as csp
			where csp.id = (select cspc.sub_population from @extschema@.c_sub_population_category as cspc where cspc.id = _sub_population_category)
			)
		then
			raise exception 'Error 05: fn_etl_update_sub_population_category_label: Input argument _sub_population_category (ID) = % is not category from ATOMIC sub population!',_sub_population_category;
		end if;
		-----------------------------------------
		-- UPDATE label of ATOMIC sub population category
		if _national_language = 'en'::varchar
		then
			_column := 'label_en';
			select cspc.label_en from @extschema@.c_sub_population_category as cspc where cspc.id = _sub_population_category into _label_category_original;
			update @extschema@.c_sub_population_category set label_en = _label where id = _sub_population_category;
		else
			_column := 'label';
			select cspc.label from @extschema@.c_sub_population_category as cspc where cspc.id = _sub_population_category into _label_category_original;
			update @extschema@.c_sub_population_category set label = _label where id = _sub_population_category;
		end if;
		-----------------------------------------
		-- UPDATE label of NON-ATOMIC sub population category
		execute
		'
		with
		w1 as	(
				select * from @extschema@.cm_sub_population_category where sub_population_category in
				(select sub_population_category from @extschema@.cm_sub_population_category where atomic_category = $1)
				)
		,w2 as	(
				select
						w1.id,
						w1.sub_population_category, 																		-- id_target_db of non-atomic category 
						w1.atomic_category,																					-- id_target_db of atomic category
						t1.'|| _column ||' as label_category_updated,														-- label atomic category updated
						case when w1.atomic_category = $1 then $2 else t1.'|| _column ||' end as label_category_original	-- label atomic category original
				from
						w1
						inner join @extschema@.c_sub_population_category as t1 on w1.atomic_category = t1.id
				)				
		---------
		,w3 as	(
				select
						id,
						string_to_array('|| _column ||','';'') as label_category 
				from
						@extschema@.c_sub_population_category
				where
						id in (select w2.sub_population_category from w2)
				)
		,w4 as	(
				select
						w3.*,
						(select array_agg(t.res order by t.res) from (select generate_series(1,array_length(w3.label_category,1)) as res) as t) as id_order
				from
						w3
				)
		,w5 as	(
				select
						w4.id,
						unnest(w4.label_category) as label_category,
						unnest(w4.id_order) as id_order
				from
						w4
				)
		--------
		,w6 as	(
				select w2.*, w5.id_order from w2 inner join w5
				on w2.sub_population_category = w5.id and w2.label_category_original = w5.label_category
				)		
		,w7 as	(
				select
						w6.sub_population_category,
						array_agg(w6.label_category_updated order by w6.id_order) as label_category,
						array_agg(w6.id_order order by w6.id_order) as id_order
				from
						w6 group by w6.sub_population_category
				)
		,w8 as	(
				select
						w7.sub_population_category,
						array_to_string(w7.label_category,'';'') as label4update
				from
						w7
				)
		update @extschema@.c_sub_population_category set '|| _column ||' = w8.label4update
		from w8 where c_sub_population_category.id = w8.sub_population_category;
		'
		using _sub_population_category, _label_category_original;
		-----------------------------------------
		_res := concat('The label of sub population category [c_sub_population_category.id = ',_sub_population_category,'] for language element = "',_national_language,'" was changed. The update was done for ATOMIC and NON-ATOMIC sub population categories.');
		-----------------------------------------
		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_update_sub_population_category_label(integer, character varying, varchar) is
'The tunction updates a sub population category label in c_sub_population_category table for given sub population category.';

grant execute on function @extschema@.fn_etl_update_sub_population_category_label(integer, character varying, varchar) to public;
-- </function>



-- <function name="fn_etl_update_sub_population_category_description" schema="extschema" src="functions/extschema/etl/fn_etl_update_sub_population_category_description.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_update_sub_population_category_description(integer, character varying, text)

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_update_sub_population_category_description(integer, character varying, text) CASCADE;

create or replace function @extschema@.fn_etl_update_sub_population_category_description
(
	_sub_population_category	integer,
	_national_language			character varying(2),
	_description				text
)
returns text
as
$$
declare
		_column							text;
		_description_category_original	text;
		_res							text;
begin
		if _sub_population_category is null
		then
			raise exception 'Error 01: fn_etl_update_sub_population_category_description: Input argument _sub_population_category must not be NULL!';
		end if;
	
		if _national_language is null
		then
			raise exception 'Error 02: fn_etl_update_sub_population_category_description: Input argument _national_language must not be NULL!';
		end if;
	
		if _description is null
		then
			raise exception 'Error 03: fn_etl_update_sub_population_category_description: Input argument _description must not be NULL!';
		end if;
		-----------------------------------------
		if	(
			select count(cspc.*) is distinct from 1
			from @extschema@.c_sub_population_category as cspc
			where cspc.id = _sub_population_category
			)
		then
			raise exception 'Error 04: fn_etl_update_sub_population_category_description: Input argument _sub_population_category (ID) = % is not present in c_sub_population_category table!',_sub_population_category;
		end if;
		-----------------------------------------
		if	(
			select csp.atomic = false
			from @extschema@.c_sub_population as csp
			where csp.id = (select cspc.sub_population from @extschema@.c_sub_population_category as cspc where cspc.id = _sub_population_category)
			)
		then
			raise exception 'Error 05: fn_etl_update_sub_population_category_description: Input argument _sub_population_category (ID) = % is not category from ATOMIC sub population!',_sub_population_category;
		end if;
		-----------------------------------------
		-- UPDATE description of ATOMIC sub population category
		if _national_language = 'en'::varchar
		then
			_column := 'description_en';
			select cspc.description_en from @extschema@.c_sub_population_category as cspc where cspc.id = _sub_population_category into _description_category_original;
			update @extschema@.c_sub_population_category set description_en = _description where id = _sub_population_category;
		else
			_column := 'description';
			select cspc.description from @extschema@.c_sub_population_category as cspc where cspc.id = _sub_population_category into _description_category_original;
			update @extschema@.c_sub_population_category set description = _description where id = _sub_population_category;
		end if;
		-----------------------------------------
		-- UPDATE description of NON-ATOMIC sub population category
		execute
		'
		with
		w1 as	(
				select * from @extschema@.cm_sub_population_category where sub_population_category in
				(select sub_population_category from @extschema@.cm_sub_population_category where atomic_category = $1)
				)
		,w2 as	(
				select
						w1.id,
						w1.sub_population_category, 																			-- id_target_db of non-atomic category 
						w1.atomic_category,																						-- id_target_db of atomic category
						t1.'|| _column ||' as description_category_updated,														-- description atomic category updated
						case when w1.atomic_category = $1 then $2 else t1.'|| _column ||' end as description_category_original	-- description atomic category
				from
						w1
						inner join @extschema@.c_sub_population_category as t1 on w1.atomic_category = t1.id
				)				
		---------
		,w3 as	(
				select
						id,
						string_to_array('|| _column ||','';'') as description_category 
				from
						@extschema@.c_sub_population_category
				where
						id in (select w2.sub_population_category from w2)
				)
		,w4 as	(
				select
						w3.*,
						(select array_agg(t.res order by t.res) from (select generate_series(1,array_length(w3.description_category,1)) as res) as t) as id_order
				from
						w3
				)
		,w5 as	(
				select
						w4.id,
						unnest(w4.description_category) as description_category,
						unnest(w4.id_order) as id_order
				from
						w4
				)
		--------
		,w6 as	(
				select w2.*, w5.id_order from w2 inner join w5
				on w2.sub_population_category = w5.id and w2.description_category_original = w5.description_category
				)		
		,w7 as	(
				select
						w6.sub_population_category,
						array_agg(w6.description_category_updated order by w6.id_order) as description_category,
						array_agg(w6.id_order order by w6.id_order) as id_order
				from
						w6 group by w6.sub_population_category
				)
		,w8 as	(
				select
						w7.sub_population_category,
						array_to_string(w7.description_category,'';'') as description4update
				from
						w7
				)
		update @extschema@.c_sub_population_category set '|| _column ||' = w8.description4update
		from w8 where c_sub_population_category.id = w8.sub_population_category;
		'
		using _sub_population_category, _description_category_original;
		-----------------------------------------
		_res := concat('The description of area domain category [c_sub_population_category.id = ',_sub_population_category,'] for language element = "',_national_language,'" was changed. The update was done for ATOMIC and NON-ATOMIC sub population categories.');
		-----------------------------------------
		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_update_sub_population_category_description(integer, character varying, text) is
'The tunction updates a sub population category description in c_sub_population table for given sub population category.';

grant execute on function @extschema@.fn_etl_update_sub_population_category_description(integer, character varying, text) to public;
-- </function>