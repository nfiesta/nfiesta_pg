-- Copyright 2017, 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: nfiesta.fn_1p_total_var(integer)

-- DROP FUNCTION nfiesta.fn_1p_total_var(integer);

CREATE OR REPLACE FUNCTION nfiesta.fn_1p_total_var(_conf_id INT)
  RETURNS TABLE(
	point1p DOUBLE PRECISION,
	var1p DOUBLE PRECISION,
	min_ssize DOUBLE PRECISION,
	act_ssize BIGINT,
	est_info JSON
) AS
$BODY$
DECLARE
_complete_query TEXT;
_no_plots_in_cell INT;
_variable INT;
BEGIN

-- checking if _conf_id was provided 
IF _conf_id IS NULL THEN
	RAISE EXCEPTION 'fn_1p_total_var: Function argument conf_id INT must not be NULL!';
END IF;


-- checking if there are some plots in the estimation cell (belonging to the panel refyearset group)
SELECT
	count(*)
FROM 
	nfiesta.t_total_estimate_conf AS t1
INNER JOIN
	nfiesta.t_panel_refyearset_group AS t2
	ON t1.id = $1 AND t1.panel_refyearset_group = t2.panel_refyearset_group
INNER JOIN
	sdesign.t_panel AS t3
	ON t2.panel = t3.id
INNER JOIN 
	nfiesta.cm_plot2cell_mapping AS t4
	ON t1.estimation_cell = t4.estimation_cell
INNER JOIN 
	sdesign.cm_plot2cluster_config_mapping AS t5
	ON t4.plot = t5.plot AND t3.cluster_configuration = t5.cluster_configuration 
INNER JOIN 
	sdesign.f_p_plot AS t6 
	ON t4.plot = t6.gid 
INNER JOIN 
	sdesign.cm_cluster2panel_mapping AS t7
	ON t7."cluster" = t6."cluster"  AND t7.panel = t3.id INTO _no_plots_in_cell;
	
SELECT variable FROM nfiesta.t_total_estimate_conf WHERE id = $1 INTO _variable;

-- building the return query
IF _no_plots_in_cell = 0 THEN 
	_complete_query := 	'
	SELECT
		0::DOUBLE PRECISION AS point1p, 
		0::DOUBLE PRECISION AS var1p, 
		-5::DOUBLE PRECISION AS min_ssize,
		0::BIGINT AS act_ssize, 
		(SELECT array_to_json(array_agg(row_to_json(t))) FROM (SELECT ' || quote_literal('all') || ' AS stratum, ' || _variable || ' AS variable, 0 AS sunits_in_cell, 0 AS sunits_ldsity_nonzero) AS t) AS est_info';
ELSE 
	_complete_query := '
WITH w_npanels AS MATERIALIZED (
	SELECT 
		t3.stratum,
		count(DISTINCT t2.panel) AS n_panels
	FROM
		nfiesta.t_total_estimate_conf AS t1
	INNER JOIN nfiesta.t_panel_refyearset_group AS t2 ON t1.panel_refyearset_group = t2.panel_refyearset_group AND t1.id = '|| _conf_id || '
	INNER JOIN sdesign.t_panel AS t3 ON t2.panel = t3.id
	GROUP BY t3.stratum
), w_config_summary AS MATERIALIZED (
	SELECT 
		t1.estimation_cell,
		t1.variable AS variable,
		t2.reference_year_set,
		t3.stratum AS stratum, 
		t3.id AS panel,
		t5.n_panels,
		t4.id AS cluster_configuration,
		t4.plots_per_cluster
	FROM
		nfiesta.t_total_estimate_conf AS t1
	INNER JOIN nfiesta.t_panel_refyearset_group AS t2 ON t1.panel_refyearset_group = t2.panel_refyearset_group AND t1.id = '|| _conf_id || '
	INNER JOIN sdesign.t_panel AS t3 ON t2.panel = t3.id
	INNER JOIN sdesign.t_cluster_configuration AS t4 ON t3.cluster_configuration = t4.id
	INNER JOIN w_npanels AS t5 ON t3.stratum = t5.stratum
), w_plots AS MATERIALIZED (
	SELECT
		t1.stratum,
		t2."cluster",
		t2.sampling_weight_ha/t1.n_panels::numeric as sweight,
		t3.gid AS plot,
		CASE 
			WHEN t5.plot IS NOT NULL THEN 1 
			ELSE 0
		END AS plot_in_cell,
		coalesce(t7.value,0) AS ldsity
	FROM w_config_summary AS t1
	INNER JOIN sdesign.cm_cluster2panel_mapping AS t2 ON t1.panel = t2.panel 
	INNER JOIN sdesign.f_p_plot AS t3 ON t2."cluster" = t3."cluster" 
	INNER JOIN sdesign.cm_plot2cluster_config_mapping AS t4 ON t3.gid = t4.plot AND t1.cluster_configuration = t4.cluster_configuration 
	LEFT JOIN nfiesta.cm_plot2cell_mapping AS t5 ON t1.estimation_cell = t5.estimation_cell AND t3.gid = t5.plot
	INNER JOIN nfiesta.t_available_datasets AS t6 ON t1.variable  = t6.variable AND t1.panel = t6.panel AND t1.reference_year_set = t6.reference_year_set 
	LEFT JOIN nfiesta.t_target_data AS t7 ON t7.available_datasets = t6.id AND t7.plot = t3.gid AND t7.is_latest
), w_clusters AS MATERIALIZED (
	SELECT 
		stratum,
		"cluster",
		sweight,
		sum(plot_in_cell*ldsity)/count(*)::numeric AS ldsity, -- dividing by count means averageing over reference yearsets (if cluster and panel is included more than once) and nominal number of plots per cluster
		max(plot_in_cell) AS cluster_in_cell
	FROM 
		w_plots 
	GROUP BY stratum, "cluster", sweight
), w_1p_data AS MATERIALIZED (
	SELECT 
		stratum,
		array_agg("cluster" ORDER BY "cluster") FILTER (WHERE ldsity != 0) AS cid,
		array_agg(sweight ORDER BY "cluster") FILTER (WHERE ldsity != 0) AS sweight,
		array_agg(ldsity ORDER BY "cluster") FILTER (WHERE ldsity != 0) AS ldsity,
		count(*) AS ssize
	FROM w_clusters
GROUP BY stratum
), w_est1p_stratum AS MATERIALIZED (
	SELECT
		stratum, 
		ssize,
		htc_compute_sweight_ha(coalesce(cid, ARRAY[1]), coalesce(ldsity, ARRAY[0]), coalesce(sweight, ARRAY[1]), ssize::int) AS res
	FROM w_1p_data
), w_est1p AS (
	SELECT
		coalesce(sum((res).total), 0.0) as point1p, 
		coalesce(sum((res).var), 0.0) AS var1p,
		sum(ssize) AS ssize
	FROM w_est1p_stratum
), w_json AS (
		SELECT array_to_json(array_agg(row_to_json(t))) as est_info from
		(
			SELECT 
				t1.stratum AS stratum_id,
				t2.stratum, ' 
				|| _variable || ' AS variable,
				sum(cluster_in_cell) AS sunits_in_cell,
				count(*) FILTER (WHERE ldsity != 0)  as sunits_ldsity_nonzero
			FROM w_clusters AS t1 
			INNER JOIN sdesign.t_stratum AS t2 ON t1.stratum = t2.id
			GROUP BY t1.stratum, t2.stratum
		) AS t
), w_means_sigma AS (
	SELECT 
		sum(sweight*ldsity)/sum(sweight) AS mean,
		sum(sweight*power(ldsity,2))/sum(sweight) AS mean2,
		sum(sweight*power(ldsity,3))/sum(sweight) AS mean3,
		power(sum(sweight*power(ldsity,2))/sum(sweight) - power(sum(sweight*ldsity)/sum(sweight), 2), 0.5) AS sigma
	FROM 
		w_clusters		
)
SELECT
	point1p, 
	var1p,
	CASE 
		WHEN sigma != 0 THEN ceil(25*power((mean3 - 3*mean*power(sigma,2) - power(mean,3))/power(sigma,3),2))::DOUBLE PRECISION
		ELSE -1::DOUBLE PRECISION -- all cluster local densities are equal, no variance of local densities at all
	END AS min_ssize, 
	ssize::BIGINT AS act_ssize,
	est_info
FROM w_est1p, w_json, w_means_sigma;';

END IF;

-- RAISE NOTICE '%', _complete_query;
RETURN QUERY EXECUTE _complete_query;
END;
$BODY$
LANGUAGE plpgsql STABLE;

COMMENT ON FUNCTION nfiesta.fn_1p_total_var(integer) IS
'For the input argument corresponding to the id of single-phase total configuration '
'the function calculates the single-phase total, its variance and minimum sample size. '
'Negative mimimum sample size indicates one of the specific situations, see '
'https://gitlab.com/nfiesta/nfiesta_pg/-/wikis/Estimates-Calculation#minimal-sample-size-error-codes.';
