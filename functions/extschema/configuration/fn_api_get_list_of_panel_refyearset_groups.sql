-- Copyright 2017, 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_api_get_list_of_panel_refyearset_groups()
--DROP FUNCTION @extschema@.fn_api_get_list_of_panel_refyearset_groups();

CREATE OR REPLACE FUNCTION @extschema@.fn_api_get_list_of_panel_refyearset_groups()
 RETURNS TABLE(id INT, label VARCHAR(200), description TEXT, label_en VARCHAR(200), description_en TEXT)
 LANGUAGE plpgsql
AS $function$
BEGIN

RETURN QUERY EXECUTE ' 
SELECT 
	id,
	label,
	description,
	label_en,
	description_en
FROM @extschema@.c_panel_refyearset_group
ORDER BY label, label_en;';
END;
$function$
;

COMMENT ON FUNCTION @extschema@.fn_api_get_list_of_panel_refyearset_groups() IS 
'The function returns id, label, description, label_en, description_en from nfiesta.c_panel_refyearset_group '
'of all groups of panel and reference-year set combinations.';

/*
-- testing
SELECT * FROM @extschema@.fn_api_get_list_of_panel_refyearset_groups();
*/