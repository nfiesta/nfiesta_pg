--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_get_area_domains4target_variable(integer, integer)
--DROP FUNCTION @extschema@.fn_get_area_domains4target_variable(integer, integer);
CREATE OR REPLACE FUNCTION @extschema@.fn_get_area_domains4target_variable(
		_target_variable integer, 
		_numerator_area_domain integer DEFAULT NULL::int)
RETURNS TABLE (
	id integer,
	label varchar,
	description text, 
	label_en varchar,
	description_en text
)
AS
$function$
BEGIN

IF _target_variable IS NULL THEN
	RAISE WARNING 'Target variable is NULL.';
	RETURN QUERY SELECT NULL::int, NULL::varchar, NULL::text, NULL::varchar, NULL::text;
END IF;

IF _target_variable IS NOT NULL 
THEN
	IF NOT EXISTS (SELECT * FROM @extschema@.c_target_variable AS t1 WHERE t1.id = _target_variable)
	THEN
		RAISE EXCEPTION 'Specified target_variable (%) does not exist in table c_target_variable!', _target_variable;
	END IF;
END IF;

IF _numerator_area_domain IS NOT NULL AND _numerator_area_domain != 0 
THEN
	IF NOT EXISTS (SELECT * FROM @extschema@.c_area_domain AS t1 WHERE t1.id = _numerator_area_domain)
	THEN RAISE EXCEPTION 'Given area domain (%) does not exist in table c_area_domain!', _numerator_area_domain;
	END IF;
END IF;

RETURN QUERY EXECUTE
	'WITH RECURSIVE w_variables (target_variable, variable_superior, variable) AS (
			SELECT
				t1.target_variable,
				variable_superior,
				variable
			FROM @extschema@.t_variable AS t1
			LEFT JOIN @extschema@.t_variable_hierarchy AS t2
			ON t1.id = t2.variable
			LEFT JOIN @extschema@.c_area_domain_category AS t3
			ON t1.area_domain_category = t3.id
			WHERE 	t1.target_variable = $1 AND 		-- first used as a nominator and second as denominator
				-- this condition is applied when the target variable is selected as a denominator
				-- and the user defined an area_domain for nominator
				-- the principle is to find a common sorting for both variables (nom and denom)
				CASE 	WHEN $2 IS NOT NULL AND $2 != 0 THEN t3.area_domain = $2
					WHEN $2 = 0 THEN t1.area_domain_category IS NULL 
				ELSE true 
				END
			UNION ALL
			SELECT
				t1.target_variable,
				t2.variable_superior,
				t2.variable
			FROM w_variables AS t1
			LEFT JOIN @extschema@.t_variable_hierarchy AS t2
			ON t1.variable_superior = t2.variable
			WHERE t2.variable IS NOT NULL
		)
		, w_attr_types AS (
			SELECT
				t1.target_variable,
				t1.variable_superior, t1.variable, 
				t3.area_domain AS area_domain_sup, t5.area_domain, 
				t3.id AS id_cat_sup, t3.label AS category_sup,
				t5.id AS id_cat, t5.label AS category
			FROM w_variables AS t1
			LEFT JOIN
				@extschema@.t_variable AS t2
			ON 
				t1.variable_superior = t2.id
			LEFT JOIN
				@extschema@.c_area_domain_category AS t3
			ON t2.area_domain_category = t3.id
			LEFT JOIN
				@extschema@.t_variable AS t4
			ON 
				t1.variable = t4.id
			LEFT JOIN
				@extschema@.c_area_domain_category AS t5
			ON t4.area_domain_category = t5.id
		),
		w_union AS (
			SELECT	target_variable, variable_superior AS variable,  area_domain_sup AS area_domain
			FROM	w_attr_types
			UNION ALL
			SELECT	target_variable, variable,  area_domain
			FROM	w_attr_types
		)
		,w_distinct AS (
			SELECT DISTINCT target_variable, area_domain
			FROM w_union
		)
		,w_final AS (
			SELECT 
				coalesce(t2.id, t3.id) AS id,
				coalesce(t2.label, t3.label) AS label,
				coalesce(t2.description, t3.description) AS description,
				coalesce(t2.label_en, t3.label_en) AS label_en,
				coalesce(t2.description_en, t3.description_en) AS description_en
			FROM w_distinct AS t1
			LEFT JOIN @extschema@.c_area_domain AS t2
			ON t1.area_domain = t2.id
			LEFT JOIN
				(SELECT
					0 AS id,
					''bez rozlišení'' AS label, 
					''Bez rozlišení.'' AS description,
					''altogether'' AS label_en,
					''Altogether.'' AS description_en
				) AS t3
			ON coalesce(t1.area_domain,0) = t3.id
		)
		SELECT id, label, description, label_en, description_en 
		FROM w_final
		ORDER BY id
		'
		USING _target_variable, _numerator_area_domain;
END;
$function$
LANGUAGE plpgsql
PARALLEL SAFE;

COMMENT ON FUNCTION @extschema@.fn_get_area_domains4target_variable(integer,integer) IS 'Function returns table with area domains constrained by the availability for a given target variable and optionally by a given area domain of numerator if the target variable comes in the meaning of the denominator.';
