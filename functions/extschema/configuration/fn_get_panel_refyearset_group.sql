--
-- Copyright 2017, 2021 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_get_panel_refyearset_group(integer[], integer[])
--DROP FUNCTION @extschema@.fn_get_panel_refyearset_group(integer[], integer[]);
CREATE OR REPLACE FUNCTION @extschema@.fn_get_panel_refyearset_group(
		_panels integer[], _refyearsets integer[]) 
RETURNS integer
AS
$function$
DECLARE
_panel_refyearset_group		integer;
BEGIN

IF _panels IS NULL THEN
	RAISE NOTICE 'Input array of panels is NULL!';
END IF;
-- test on the same length of each array (except estimation_cells)
IF
	array_length(_panels,1) != array_length(_refyearsets,1)
THEN
	RAISE EXCEPTION 'Input arrays of panels (%) and refyearsets (%) are not the same length.', _panels, _refyearsets;
END IF; 

	WITH w_lookup AS (
		SELECT 	t1.panel_refyearset_group,
			array_agg(panel ORDER BY panel, reference_year_set) AS panels, 
			array_agg(reference_year_set ORDER BY panel, reference_year_set) AS refyearsets
		FROM @extschema@.t_panel_refyearset_group AS t1
		GROUP BY t1.panel_refyearset_group
	)
	SELECT panel_refyearset_group
	FROM w_lookup
	WHERE panels = _panels AND refyearsets = _refyearsets
	INTO _panel_refyearset_group;

	RETURN _panel_refyearset_group;
END;
$function$
LANGUAGE plpgsql
PARALLEL SAFE;

COMMENT ON FUNCTION @extschema@.fn_get_panel_refyearset_group(integer[],integer[]) IS 'Returns identificator of panel_refyearset_group.';
