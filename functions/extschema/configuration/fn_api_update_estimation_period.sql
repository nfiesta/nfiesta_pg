-- Copyright 2017, 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: nfiesta.fn_api_update_estimation_period(INT, VARCHAR(200), TEXT, VARCHAR(200), TEXT, BOOLEAN)
--DROP FUNCTION nfiesta.fn_api_update_estimation_period(INT, VARCHAR(200), TEXT, VARCHAR(200), TEXT, BOOLEAN);

CREATE OR REPLACE FUNCTION nfiesta.fn_api_update_estimation_period (_id INT, _label VARCHAR(200), _description TEXT, 
_label_en VARCHAR(200), _description_en TEXT, _default_in_olap BOOLEAN)
 RETURNS VOID
 LANGUAGE plpgsql
AS $function$

BEGIN

IF _id IS NULL THEN
	RAISE EXCEPTION 'Error 01: fn_api_update_estimation_period: Function argument _id INT must not be NULL!';
END IF;

IF _label IS NULL THEN
	RAISE EXCEPTION 'Error 02: fn_api_update_estimation_period: Function argument _label VARCHAR(200) must not be NULL!';
END IF;

IF _description IS NULL THEN
	RAISE EXCEPTION 'Error 03: fn_api_update_estimation_period: Function argument _description TEXT must not be NULL!';
END IF;

IF _label_en IS NULL THEN
	RAISE EXCEPTION 'Error 04: fn_api_update_estimation_period: Function argument _label_en VARCHAR(200) must not be NULL!';
END IF;

IF _description_en IS NULL THEN
	RAISE EXCEPTION 'Error 05: fn_api_update_estimation_period: Function argument _description_en TEXT must not be NULL!';
END IF;

IF NOT EXISTS (SELECT * FROM nfiesta.c_estimation_period WHERE id = _id) THEN
	RAISE EXCEPTION 'Error 06: fn_api_update_estimation_period: Estimation period with id = % does not exist in table nfiesta.c_estimation_period and cannot be updated.', $1;
END IF;

IF _id <> (SELECT id FROM nfiesta.c_estimation_period WHERE label = _label) THEN
	RAISE EXCEPTION 'Error 07: fn_api_update_estimation_period: Function argument _label VARCHAR(200) already exists in table nfiesta.c_estimation_period.';
END IF;

IF _id <> (SELECT id FROM nfiesta.c_estimation_period WHERE description = _description) THEN
	RAISE EXCEPTION 'Error 08: fn_api_update_estimation_period: Function argument _description TEXT already exists in table nfiesta.c_estimation_period.';
END IF;

IF _id <> (SELECT id FROM nfiesta.c_estimation_period WHERE label_en = _label_en) THEN
	RAISE EXCEPTION 'Error 09: fn_api_update_estimation_period: Function argument _label_en VARCHAR(200) already exists in table nfiesta.c_estimation_period.';
END IF;

IF _id <> (SELECT id FROM nfiesta.c_estimation_period WHERE description_en = _description_en) THEN
	RAISE EXCEPTION 'Error 10: fn_api_update_estimation_period: Function argument _description_en TEXT already exists in table nfiesta.c_estimation_period.';
END IF;

UPDATE nfiesta.c_estimation_period 
SET 
	label = _label,
	description = _description,
	label_en = _label_en,
	description_en = _description_en,
    default_in_olap = _default_in_olap
WHERE id = _id;
RAISE NOTICE 'Updating row in nfiesta.c_estimation_period with id = %.', _id;

END;
$function$
;

COMMENT ON FUNCTION nfiesta.fn_api_update_estimation_period(INT, VARCHAR(200), TEXT, VARCHAR(200), TEXT, BOOLEAN) IS 
'The function updates label, description, label_en, description_en, default_in_olap (passed as arguments 2-6) in lookup '
'table nfiesta.c_estimation_period, where id = _id passed as an argument 1.';

/*
-- testing false inputs

-- passing NULL for arguments
SELECT * FROM nfiesta.fn_api_update_estimation_period(NULL,NULL,NULL,NULL,NULL,NULL);
SELECT * FROM nfiesta.fn_api_update_estimation_period(10,NULL,NULL,NULL,NULL,NULL);

-- non-existing group, no records
SELECT * FROM nfiesta.fn_api_update_estimation_period(-20,'new_label','new_description','new_label_en','new_description_en',false);

-- testing valid inputs

-- existing _label and _description of the same id as input _id
SELECT * FROM nfiesta.fn_api_update_estimation_period(1,'NFI2','NFI2','new_label_en','new_description_en',true);

-- group 1
SELECT * FROM nfiesta.fn_api_update_estimation_period(1,'new_label_1','new_description_1','new_label_en_1','new_description_en_1',true);
*/
