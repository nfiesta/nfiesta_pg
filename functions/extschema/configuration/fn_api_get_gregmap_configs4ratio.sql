-- Copyright 2017, 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: nfiesta.fn_api_get_gregmap_configs4ratio(INT, INT[], INT[], INT[])
-- DROP FUNCTION nfiesta.fn_api_get_gregmap_configs4ratio(INT, INT[], INT[], INT[]);

CREATE OR REPLACE FUNCTION nfiesta.fn_api_get_gregmap_configs4ratio(_estimation_period INT, _estimation_cells INT[], _variables_nom INT[], _variables_denom INT[])
RETURNS TABLE(
 			panel_refyearset_group_id INT,
			panel_refyearset_group_label VARCHAR(200),  
			panel_refyearset_group_label_en VARCHAR(200),
			panel_refyearset_group_description TEXT,
			panel_refyearset_group_description_en TEXT,
			param_area_type_id INT,
			param_area_type_label VARCHAR(200),
			param_area_type_label_en VARCHAR(200),
			param_area_type_description TEXT,
			param_area_type_description_en TEXT,
			force_synthetic BOOLEAN,
			nominator_model_id INT, 
			nominator_model_label VARCHAR(200),
			nominator_model_label_en VARCHAR(200),
			nominator_model_description TEXT,
			nominator_model_description_en TEXT,
			nominator_model_sigma BOOLEAN,
			denominator_model_id INT, 
			denominator_model_label VARCHAR(200),
			denominator_model_label_en VARCHAR(200),
			denominator_model_description TEXT,
			denominator_model_description_en TEXT,
			denominator_model_sigma BOOLEAN,
			all_estimates_configurable BOOLEAN,
			total_estimate_conf_nominator INT[],
			total_estimate_conf_denominator INT[]
	)
AS
$function$
BEGIN

-- testing input arguments if not NULL
IF _estimation_period IS NULL THEN
	RAISE EXCEPTION 'fn_api_get_gregmap_configs4ratio: Function argument _estimation_period INT must not be NULL!';
END IF;

IF _estimation_cells IS NULL THEN
	RAISE EXCEPTION 'fn_api_get_gregmap_configs4ratio: Function argument _estimation_cells INT[] must not be NULL!';
END IF;

IF _variables_nom IS NULL THEN
	RAISE EXCEPTION 'fn_api_get_gregmap_configs4ratio: Function argument _variables_nom INT[] must not be NULL!';
END IF;

IF _variables_denom IS NULL THEN
	RAISE EXCEPTION 'fn_api_get_gregmap_configs4ratio: Function argument _variables_denom INT[] must not be NULL!';
END IF;

-- checking input arrays if they do not contain NULL
IF (SELECT array_position(_estimation_cells, NULL) IS NOT NULL) THEN
	RAISE EXCEPTION 'fn_api_get_gregmap_configs4ratio: Function argument _estimation_cells INT[] must not be an array containing NULL!';
END IF;

IF (SELECT array_position(_variables_nom, NULL) IS NOT NULL) THEN
	RAISE EXCEPTION 'fn_api_get_gregmap_configs4ratio: Function argument _variables_nom INT[] must not be an array containing NULL!';
END IF;

IF (SELECT array_position(_variables_denom, NULL) IS NOT NULL) THEN
	RAISE EXCEPTION 'fn_api_get_gregmap_configs4ratio: Function argument _variables_denom INT[] must not be an array containing NULL!';
END IF;

-- checking one cell collection within input argument _estimation_cells 
IF (SELECT count(DISTINCT estimation_cell_collection) FROM nfiesta.c_estimation_cell WHERE ARRAY[id] <@ _estimation_cells) IS DISTINCT FROM 1 THEN 
	RAISE EXCEPTION 'fn_api_get_gregmap_configs4ratio: Function argument _estimation_cells INT[] must contain cells corresponding to one and the only estimation cell collection!';
END IF;

-- checking input arrays if they do not contain NULL
IF (SELECT array_position(_estimation_cells, NULL) IS NOT NULL) THEN
	RAISE EXCEPTION 'fn_api_get_gregmap_configs4ratio: Function argument _estimation_cells INT[] must not be an array containing NULL!';
END IF; 

IF (SELECT array_position(_variables_nom, NULL) IS NOT NULL) THEN
	RAISE EXCEPTION 'fn_api_get_gregmap_configs4ratio: Function argument _variables_nom INT[] must not be an array containing NULL!';
END IF; 

IF (SELECT array_position(_variables_denom, NULL) IS NOT NULL) THEN
	RAISE EXCEPTION 'fn_api_get_gregmap_configs4ratio: Function argument _variables_denom INT[] must not be an array containing NULL!';
END IF; 

-- check equality of lengths of _variables_nom and _variables_denom
IF (SELECT array_length(_variables_nom, 1) != array_length(_variables_denom, 1)) THEN
	RAISE EXCEPTION 'fn_api_get_gregmap_configs4ratio: Function arguments _variables_nom INT[] and _variables_denom INT[] must be arrays of the same lengths!';
END IF; 

RETURN QUERY EXECUTE '
	WITH w_vars AS MATERIALIZED ( -- combinations of variables in nominator and denominator
		SELECT 
			vnom.vid AS variable_nominator, 
			vdenom.vid AS variable_denominator
		FROM 
			unnest($3) WITH ORDINALITY vnom (vid,ord) 
		INNER JOIN 
			unnest($4) WITH ORDINALITY vdenom (vid, ord) 
		ON vnom.ord = vdenom.ord
	), w_greg_map_totals_conf AS MATERIALIZED (
		SELECT 
			array_agg(t1.id) OVER (PARTITION BY t1.panel_refyearset_group, t1.force_synthetic, t6.param_area_type, t4.model, t4.sigma, t5.model, t5.sigma) AS total_estimate_conf_nominator,
			array_agg(t2.id) OVER (PARTITION BY t1.panel_refyearset_group, t1.force_synthetic, t6.param_area_type, t4.model, t4.sigma, t5.model, t5.sigma) AS total_estimate_conf_denominator,
			t1.estimation_period,
			t1.estimation_cell,
			t1.panel_refyearset_group,
			t10.label AS panel_refyearset_group_label,
			t10.label_en AS panel_refyearset_group_label_en,
			t10.description AS panel_refyearset_group_description,
			t10.description_en AS panel_refyearset_group_description_en,
			t1.force_synthetic,
			t1.variable AS nominator_variable,
			t2.variable AS denominator_variable,
			t6.gid AS param_area,
			t6.label AS param_area_label,
			t7.id AS param_area_type,
			t7.label AS param_area_type_label,
			t7.label_en AS param_area_type_label_en,
			t7.description AS param_area_type_description,
			t7.description_en AS param_area_type_description_en,
			t8.id AS nominator_model,
			t8.label AS nominator_model_label,
			t8.label_en AS nominator_model_label_en,
			t8.description AS nominator_model_description,
			t8.description_en AS nominator_model_description_en,
			t4.sigma AS nominator_model_sigma,
			t9.id AS denominator_model,
			t9.label AS denominator_model_label,
			t9.label_en AS denominator_model_label_en,
			t9.description AS denominator_model_description,
			t9.description_en AS denominator_model_description_en,
			t5.sigma AS denominator_model_sigma,
			count(*) OVER (PARTITION BY t1.panel_refyearset_group, t1.force_synthetic, t6.param_area_type, t4.model, t4.sigma, t5.model, t5.sigma) AS n_configuarations,
			count(*) OVER (PARTITION BY t1.panel_refyearset_group, t1.force_synthetic, t6.param_area_type, t4.model, t4.sigma, t5.model, t5.sigma) = array_length($2,1) * array_length($3,1) AS all_estimates_configurable
		FROM
			nfiesta.t_total_estimate_conf AS t1 -- search GREG-map total configs for the nominator of the ratio
		INNER JOIN
			nfiesta.t_total_estimate_conf AS t2 -- search GREG-map total configs for the denominator of the ratio
		ON 
			t1.estimation_period = $1 AND t2.estimation_period = $1  AND
			ARRAY[t1.estimation_cell] <@ $2 AND
			ARRAY[t2.estimation_cell] <@ $2 AND
			ARRAY[t1.variable] <@ $3 AND
			ARRAY[t2.variable] <@ $4 AND
			t1.phase_estimate_type = 2 AND t2.phase_estimate_type = 2 AND -- nom and denom total must be of type GREG-map
			t1.estimation_cell = t2.estimation_cell AND 
			t1.panel_refyearset_group = t2.panel_refyearset_group AND -- group of panels and reference year sets must match between nom and denom
			t1.force_synthetic = t2.force_synthetic -- force synthetic must also match between nom and denom totals
		INNER JOIN 
			w_vars AS t3
		ON 
			t1.variable = t3.variable_nominator AND
			t2.variable = t3.variable_denominator
		INNER JOIN 
			nfiesta.t_aux_conf AS t4 
			ON t1.aux_conf = t4.id
		INNER JOIN 
			nfiesta.t_aux_conf AS t5
			ON t2.aux_conf = t5.id AND t4.param_area = t5.param_area -- parametrisation areas between nom and denom must match
		INNER JOIN 
			nfiesta.f_a_param_area AS t6
			ON t4.param_area = t6.gid
		INNER JOIN 
			nfiesta.c_param_area_type AS t7
			ON t6.param_area_type = t7.id
		INNER JOIN 
			nfiesta.t_model AS t8 
			ON t4.model = t8.id
		INNER JOIN 
			nfiesta.t_model AS t9
			ON t5.model = t9.id
		 INNER JOIN 
		 nfiesta.c_panel_refyearset_group AS t10
		 ON t1.panel_refyearset_group = t10.id
	)
	SELECT DISTINCT 
			panel_refyearset_group AS panel_refyearset_group_id,
			panel_refyearset_group_label,  
			panel_refyearset_group_label_en,
			panel_refyearset_group_description,
			panel_refyearset_group_description_en,
			param_area_type AS param_area_type_id,
			param_area_type_label,
			param_area_type_label_en,
			param_area_type_description,
			param_area_type_description_en,
			force_synthetic,
			nominator_model AS nominator_model_id, 
			nominator_model_label,
			nominator_model_label_en,
			nominator_model_description,
			nominator_model_description_en,
			nominator_model_sigma,
			denominator_model AS denominator_model_id, 
			denominator_model_label,
			denominator_model_label_en,
			denominator_model_description,
			denominator_model_description_en,
			denominator_model_sigma,
			all_estimates_configurable,
			total_estimate_conf_nominator,
			total_estimate_conf_denominator
	FROM 
		 w_greg_map_totals_conf' USING _estimation_period, _estimation_cells, _variables_nom, _variables_denom;
END;
$function$
LANGUAGE plpgsql
PARALLEL SAFE;

COMMENT ON FUNCTION  nfiesta.fn_api_get_gregmap_configs4ratio(INT, INT[], INT[], INT[]) IS 
'For input estimation period, estimation cells, nominator and denominator variables the function returns '
'valid combinations of panel-refyearset group, param. area type, force synthetic, model and sigma of already '
'configured GREG-map totals that can be combined in the denominator and nominator of the ratio. Only ratios '
'of GREG-map totals using the same group of panel and reference-year set combinations, an identical value of ' 
'force_synthetic (both the nominator and denominator must use the same inference, either model-based or design-based), '
'and the same type of parametrisation region (meaning that the actual parametrisation regions of nominator '
'and denominator of the ratio always match). Note that the function combines the elements of the input arrays ' 
'_variables_nom and _variables_denom (considering their order) to obtain the desired set of estimates. These two ' 
'input arrays must have an equal length, and their elements must be ordered, so each combination of their elements '
'on the same position corresponds to one desired combination of variables in the nominator and the denominator of ' 
'the ratio. The output parameter all_estimates_configurable is TRUE if such a ratio can be configured for all input '
'combinations of estimation cell, nominator variable and denominator variable. The output parameters '
'total_estimate_conf_nominator and total_estimate_conf_denominator are ordered arrays of identifiers of existing ' 
'GREG-map configurations (record ids of the table nfiesta.t_total_estimate_conf) for the nominator and denominator ' 
'respectively.';

/* tests
------------------------------------------------- testing invalid inputs ------------------------------------------------------------------------------
-- test NULL for _estimation_period argument
SELECT * FROM nfiesta.fn_api_get_gregmap_configs4ratio(NULL, ARRAY[100], ARRAY[1,2,3], ARRAY[3,3,3]);

-- test NULL for _estimation_cell argument
SELECT * FROM nfiesta.fn_api_get_gregmap_configs4ratio(1, NULL, ARRAY[1,2,3], ARRAY[3,3,3]);

-- test NULL for _variables_nom argument
SELECT * FROM nfiesta.fn_api_get_gregmap_configs4ratio(1, ARRAY[100], NULL, ARRAY[3,3,3]);

-- test NULL for _variables_denom argument
SELECT * FROM nfiesta.fn_api_get_gregmap_configs4ratio(1, ARRAY[100], ARRAY[1,2,3], NULL);

-- test for NULL as an element of _estimation_cell argument
SELECT * FROM nfiesta.fn_api_get_gregmap_configs4ratio(1, ARRAY[100, NULL], ARRAY[1,2,3], ARRAY[3,3,3]);
SELECT * FROM nfiesta.fn_api_get_gregmap_configs4ratio(1, ARRAY[NULL]::int[], ARRAY[1,2,3], ARRAY[3,3,3]);

-- test for NULL as an element of _variables_nom argument
SELECT * FROM nfiesta.fn_api_get_gregmap_configs4ratio(1, ARRAY[100], ARRAY[1,NULL,3], ARRAY[3,3,3]);
SELECT * FROM nfiesta.fn_api_get_gregmap_configs4ratio(1, ARRAY[100], ARRAY[NULL]::int[], ARRAY[3,3,3]);

-- test for NULL as an element of _variables_denom argument
SELECT * FROM nfiesta.fn_api_get_gregmap_configs4ratio(1, ARRAY[100], ARRAY[1,2,3], ARRAY[3,3,NULL]);
SELECT * FROM nfiesta.fn_api_get_gregmap_configs4ratio(1, ARRAY[100], ARRAY[1,2,3]::int[], ARRAY[NULL]::int[]);

-- test if estimation cells belong to one estimation cell collection (adding 1 to the valid array _estimation_cells, 1 
-- belongs to another collection but the same stratum
SELECT * FROM nfiesta.fn_api_get_gregmap_configs4ratio(1, ARRAY[1,44,45,46,47,48,49,50,51,52,53,56,57,58,62], ARRAY[1,2,3], ARRAY[3,3,3]);

-- test if _variables_nom and _variables_denom have equal lengths
SELECT FROM nfiesta.fn_api_get_gregmap_configs4ratio(1, ARRAY[100], ARRAY[1,2,3], ARRAY[3,3]);

------------------------------------------------ testing valid inputs ---------------------------------------------------------------------------------
-- two complete configs, one force true, the other false, INSPIRE 50x50 km param area type
SELECT * FROM nfiesta.fn_api_get_gregmap_configs4ratio(1, ARRAY[55,57,69], ARRAY[1,2,3], ARRAY[3,3,3]);

-- two complete configs, one force true, the other false, two param area types: INSPIRE 50x50 km and strata set 
SELECT * FROM nfiesta.fn_api_get_gregmap_configs4ratio(1, ARRAY[100], ARRAY[1,2,3], ARRAY[3,3,3]);
*/
