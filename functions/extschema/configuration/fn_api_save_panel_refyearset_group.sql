-- Copyright 2017, 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.

-- Function: nfiesta.fn_api_save_panel_refyearset_group(INT[], INT[], VARCHAR(200), TEXT, VARCHAR(200), TEXT)
-- DROP FUNCTION nfiesta.fn_api_save_panel_refyearset_group(INT[], INT[], VARCHAR(200), TEXT, VARCHAR(200), TEXT);

CREATE OR REPLACE FUNCTION nfiesta.fn_api_save_panel_refyearset_group(_panels INT[], _refyearsets INT[], _label VARCHAR(200), _description TEXT, 
	_label_en VARCHAR(200), _description_en TEXT)
RETURNS INT
AS
$function$
BEGIN
-- testing input parameters if not NULL
IF _panels IS NULL THEN
	RAISE EXCEPTION 'fn_api_save_panel_refyearset_group: Function argument _panels INT[] must not be NULL!';
END IF;

IF _refyearsets IS NULL THEN
	RAISE EXCEPTION 'fn_api_save_panel_refyearset_group: Function argument _refyearsets INT[] must not be NULL!';
END IF;

IF _label IS NULL THEN
	RAISE EXCEPTION 'fn_api_save_panel_refyearset_group: Function argument _label VARCHAR(200) must not be NULL!';
END IF;

IF _description IS NULL THEN
	RAISE EXCEPTION 'fn_api_save_panel_refyearset_group: Function argument _description TEXT must not be NULL!';
END IF;


IF _label_en IS NULL THEN
	RAISE EXCEPTION 'fn_api_save_panel_refyearset_group: Function argument _label_en VARCHAR(200) must not be NULL!';
END IF;

IF _description_en IS NULL THEN
	RAISE EXCEPTION 'fn_api_save_panel_refyearset_group: Function argument _description_en TEXT must not be NULL!';
END IF;

-- checking input arrays if they do not contain NULL
IF (SELECT array_position(_panels, NULL) IS NOT NULL) THEN
	RAISE EXCEPTION 'fn_api_save_panel_refyearset_group:  Function argument _panels INT[] must not be an array containing NULL!';
END IF;

IF (SELECT array_position(_refyearsets, NULL) IS NOT NULL) THEN
	RAISE EXCEPTION 'fn_api_save_panel_refyearset_group:  Function argument _refyearsets INT[] must not be an array containing NULL!';
END IF;


RETURN nfiesta.fn_save_panel_refyearset_group(_panels, _refyearsets, _label, _description, _label_en, _description_en); 

END;
$function$
LANGUAGE plpgsql
PARALLEL SAFE;

COMMENT ON FUNCTION  nfiesta.fn_api_save_panel_refyearset_group(INT[], INT[], VARCHAR(200), TEXT, VARCHAR(200), TEXT) IS 
'This is a wrapper API function. Unlike the internally called fn_save_panel_refyearset_group '
'this API has no defaults. More specifically, labels and descriptions must always be provided. '
'The function returns a unique identifier of the newly inserted group';

/* tests
-------------------------------------------------------------------------------------------------------------------------------------------------------
-- tests for function nfiesta.fn_api_save_panel_refyearset_group(_panels INT[], _refyearsets INT[], _label VARCHAR(200), _description TEXT, 
--	_label_en VARCHAR(200), _description_en TEXT
-------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------- testing invalid inputs ------------------------------------------------------------------------------
-- test NULL for _panels argument
SELECT * FROM nfiesta.fn_api_save_panel_refyearset_group(NULL, ARRAY[2,3,3], 'nejaky label', 'nejaky description', 'some label en', 'some description_en');

-- test NULL for _refyearsets argument
SELECT * FROM nfiesta.fn_api_save_panel_refyearset_group(ARRAY[1,5,7], NULL, 'nejaky label', 'nejaky description', 'some label en', 'some description_en');

-- passing NULL for _label
SELECT * FROM nfiesta.fn_api_save_panel_refyearset_group(ARRAY[1,5,7], ARRAY[2,3,3], NULL, 'nejaky description', 'some label en', 'some description_en')

-- passing NULL for _description
SELECT * FROM nfiesta.fn_api_save_panel_refyearset_group(ARRAY[1,5,7], ARRAY[2,3,3], 'nejaky label', NULL, 'some label en', 'some description_en');

-- passing NULL for _label_en
SELECT * FROM nfiesta.fn_api_save_panel_refyearset_group(ARRAY[1,5,7], ARRAY[2,3,3], 'nejaky label', 'nejaky description', NULL, 'some description_en');

-- passing NULL for _description
SELECT * FROM nfiesta.fn_api_save_panel_refyearset_group(ARRAY[1,5,7], ARRAY[2,3,3], 'nejaky label', 'nejaky description', 'some label en', NULL);

-- test for NULL as an element of _panels
SELECT * FROM nfiesta.fn_api_save_panel_refyearset_group(ARRAY[1,NULL,7], ARRAY[2,3,3], 'nejaky label', 'nejaky description', 'some label en', 'some description_en');
SELECT * FROM nfiesta.fn_api_save_panel_refyearset_group(ARRAY[NULL], ARRAY[2,3,3], 'nejaky label', 'nejaky description', 'some label en', 'some description_en');
SELECT * FROM nfiesta.fn_api_save_panel_refyearset_group(ARRAY[NULL]::int[], ARRAY[2,3,3], 'nejaky label', 'nejaky description', 'some label en', 'some description_en');

-- test for NULL as an element of _refyearsets
SELECT * FROM nfiesta.fn_api_save_panel_refyearset_group(ARRAY[1,5,7], ARRAY[1,NULL,7], 'nejaky label', 'nejaky description', 'some label en', 'some description_en');
SELECT * FROM nfiesta.fn_api_save_panel_refyearset_group(ARRAY[1,5,7], ARRAY[NULL], 'nejaky label', 'nejaky description', 'some label en', 'some description_en');
SELECT * FROM nfiesta.fn_api_save_panel_refyearset_group(ARRAY[1,5,7], ARRAY[NULL]::int[], 'nejaky label', 'nejaky description', 'some label en', 'some description_en');

-- test if saving a group with non-existing panel fails
SELECT * FROM nfiesta.fn_api_save_panel_refyearset_group(ARRAY[-1,5,7], ARRAY[2,3,3], 'nejaky label', 'nejaky description', 'some label en', 'some description_en');

-- test if saving a group with non-existing reference yearset fails
SELECT * FROM nfiesta.fn_api_save_panel_refyearset_group(ARRAY[1,5,7], ARRAY[-1,3,3], 'nejaky label', 'nejaky description', 'some label en', 'some description_en');

-- test if saving a group with a list of panels and refyearsets equal to an existing group fails
SELECT * FROM nfiesta.fn_api_save_panel_refyearset_group(ARRAY[1,5,7], ARRAY[2,3,3], 'nejaky label', 'nejaky description', 'some label en', 'some description_en');

------------------------------------------------ testing valid inputs ---------------------------------------------------------------------------------
SELECT * FROM nfiesta.fn_api_save_panel_refyearset_group(ARRAY[1,7], ARRAY[2,3], 'nejaky label', 'nejaky description', 'some label en', 'some description_en');

-- helping code
SELECT 
	t1.id, t1.label, 
	array_agg(t2.panel), 
	array_agg(t2.reference_year_set) 
FROM 
	nfiesta.c_panel_refyearset_group AS t1 
INNER JOIN 
	nfiesta.t_panel_refyearset_group AS t2 
	ON t1.id = t2.panel_refyearset_group 
GROUP BY t1.id, t1.label;
*/