--
-- Copyright 2017, 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: nfiesta.fn_api_save_ratio_config(INT, INT)

--DROP FUNCTION nfiesta.fn_api_save_ratio_config(INT, INT);

CREATE OR REPLACE FUNCTION nfiesta.fn_api_save_ratio_config(_total_estimate_conf_numerator INT, _total_estimate_conf_denominator INT)
RETURNS INT
AS
$function$
DECLARE
_estimate_conf INT;
_numerator_panels INT[];
_denom_panels INT[];

BEGIN
	
	-- testing input parameters if not NULL
	IF _total_estimate_conf_numerator IS NULL THEN
		RAISE EXCEPTION 'fn_api_save_ratio_config: Function argument _total_estimate_conf_numerator INT must not be NULL!';
	END IF;	
	
	IF _total_estimate_conf_denominator IS NULL THEN
		RAISE EXCEPTION 'fn_api_save_ratio_config: Function argument _total_estimate_conf_denominator INT must not be NULL!';
	END IF;	
	
	-- testing for exsistence of total configurations
	IF NOT EXISTS (SELECT id FROM nfiesta.t_total_estimate_conf WHERE id = $1) THEN
			RAISE EXCEPTION 'fn_api_save_ratio_config: There is no total configuration corresponding to the function argument _total_estimate_conf_numerator: % !', _total_estimate_conf_numerator;
	END IF;	

	IF NOT EXISTS (SELECT id FROM nfiesta.t_total_estimate_conf WHERE id = $2) THEN
			RAISE EXCEPTION 'fn_api_save_ratio_config: There is no total configuration corresponding to the function argument _total_estimate_conf_denominator: %!', _total_estimate_conf_denominator;
	END IF;	


	-- testing the equality of panel groups between numerator and denominator (can be relased provided the ratio estimator functions are updated)
    -- to permit for unequal sets of panels in the numerator and denominator.
	SELECT 
		array_agg(panel ORDER BY panel ASC) 
	FROM
		nfiesta.t_total_estimate_conf AS t1
	INNER JOIN
		nfiesta.t_panel_refyearset_group AS t2
	ON t1.id  = $1 AND t1.panel_refyearset_group = t2.id
	INTO _numerator_panels;

		SELECT 
		array_agg(panel ORDER BY panel ASC) 
	FROM
		nfiesta.t_total_estimate_conf AS t1
	INNER JOIN
		nfiesta.t_panel_refyearset_group AS t2
	ON t1.id  = $2 AND t1.panel_refyearset_group = t2.id
	INTO _denom_panels;

	IF _numerator_panels <> _denom_panels THEN
	RAISE EXCEPTION 'fn_api_save_ratio_config: The set of panels between the numerator and the denominator do not match! '
					'Such ratio estimators have not yet benn implemented!';
	END IF;
	
	-- insert into table t_estimate_conf
	INSERT INTO nfiesta.t_estimate_conf (estimate_type, total_estimate_conf, denominator)
	VALUES(2, _total_estimate_conf_numerator, _total_estimate_conf_denominator)
	ON CONFLICT (total_estimate_conf, coalesce(denominator,0)) DO NOTHING
	RETURNING id INTO _estimate_conf;
	
	IF _estimate_conf IS NULL THEN
		RAISE NOTICE 'fn_api_save_ratio_config: An identical ratio configuration already exists! Saving the same configuration has been skipped.';
	END IF;

RETURN _estimate_conf;

END;
$function$
LANGUAGE plpgsql
PARALLEL SAFE;

COMMENT ON FUNCTION nfiesta.fn_api_save_ratio_config(INT, INT) IS 
'The function saves configuration of a ratio estimator into the nfiesta.t_estimate_conf table. '
'It presumes existence of total configurations for the numerator as well as the denominator of ' 
'the ratio. Any type of total estimators can be combined in the numerator and the denominator as '
'long as they use the same group of panels.';

/* tests
-------------------------------------------------------------------------------------------------------------------------------------------------------
-- tests for function nfiesta.fn_api_save_ratio_config(_total_estimate_conf_numerator INT, _total_estimate_conf_denominator INT)
-------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------- testing invalid inputs ------------------------------------------------------------------------------
-- test NULL for _total_estimate_conf_numerator
SELECT * FROM nfiesta.fn_api_save_ratio_config(NULL,1);

-- test NULL for _total_estimate_conf_denominator
SELECT * FROM nfiesta.fn_api_save_ratio_config(1,NULL);

-- test for non existing _total_estimate_conf_numerator (among records of t_total_estimate_conf)
SELECT * FROM nfiesta.fn_api_save_ratio_config(-1,1);

-- test for non existing _total_estimate_conf_denominator (among records of t_total_estimate_conf)
SELECT * FROM nfiesta.fn_api_save_ratio_config(1,-1);

-- trying to configure a ratio of totals, with different sets of panels (panel_refyearset_groups 14 and 15)
SELECT * FROM nfiesta.fn_api_save_ratio_config(1,5);

------------------------------------------------ testing valid inputs ---------------------------------------------------------------------------------
-- save a configuration of a ratio of forest to non-forest within the same cell and using the same panel_refyearset_group and return new config id
SELECT * FROM nfiesta.fn_api_save_ratio_config(1,2);

-- skip already existing configuration and return NULL
SELECT * FROM nfiesta.fn_api_save_ratio_config(1,2);

------------------------------------------------ helping code ---------------------------------------------------------------------------------
-- helping code
SELECT * FROM nfiesta.t_total_estimate_conf;

SELECT * FROM nfiesta.t_estimate_conf WHERE estimate_type = 2 ORDER BY total_estimate_conf, denominator;

*/
