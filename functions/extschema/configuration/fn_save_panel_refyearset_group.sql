-- Copyright 2017, 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.

-- Function: @extschema@.fn_save_panel_refyearset_group(integer[], integer[], varchar, text, varchar, text)
--DROP FUNCTION @extschema@.fn_save_panel_refyearset_group(integer[], integer[], varchar, text, varchar, text);
CREATE OR REPLACE FUNCTION @extschema@.fn_save_panel_refyearset_group(
		_panels integer[], _refyearsets integer[], 
		_label varchar DEFAULT NULL, _description text DEFAULT NULL, _label_en varchar DEFAULT NULL, _description_en text DEFAULT NULL)
RETURNS integer
AS
$function$
DECLARE
_panel_refyearset_group		integer;
BEGIN

IF _panels IS NULL THEN
	RAISE NOTICE 'Input array of panels is NULL!';
END IF;
-- test on the same length of each array (except estimation_cells)
IF
	array_length(_panels,1) != array_length(_refyearsets,1)
THEN
	RAISE EXCEPTION 'Input arrays of panels (%) and refyearsets (%) are not the same length.', _panels, _refyearsets;
END IF; 

	SELECT @extschema@.fn_get_panel_refyearset_group (_panels, _refyearsets)
	INTO _panel_refyearset_group;

	IF _panel_refyearset_group IS NULL
	THEN
		IF _label IS NULL THEN
			_label := concat('no_of_panels: ',
					(SELECT count(id)::text FROM sdesign.t_panel
					WHERE array[id] <@ _panels),
					';',
					(SELECT concat(min(reference_date_begin)::text,'-',max(reference_date_end)::text) 
					FROM sdesign.t_reference_year_set
					WHERE array[id] <@ _refyearsets)
					);
		END IF;

		IF _description IS NULL THEN
			_description := _label;
		END IF;

		INSERT INTO @extschema@.c_panel_refyearset_group (label, description, label_en, description_en)
		SELECT _label, _description, _label_en, _description_en
		RETURNING id INTO _panel_refyearset_group;

		INSERT INTO @extschema@.t_panel_refyearset_group (panel_refyearset_group, panel, reference_year_set)
		SELECT
			_panel_refyearset_group, t1.panel, t2.reference_year_set
		FROM
			unnest(_panels) WITH ORDINALITY AS t1(panel,id)
		INNER JOIN
			unnest(_refyearsets) WITH ORDINALITY AS t2(reference_year_set,id)
		ON t1.id = t2.id;
	ELSE
		RAISE EXCEPTION 'Specified combination of panels and reference year sets is already present in the DB! (panel_refyear_set_group = %)', _panel_refyearset_group;
	END IF;

	RETURN _panel_refyearset_group;
END;
$function$
LANGUAGE plpgsql
PARALLEL SAFE;

COMMENT ON FUNCTION @extschema@.fn_save_panel_refyearset_group(integer[],integer[],varchar,text,varchar,text) IS 'Returns panel_refyearset_group identificator for newly inserted group.';
