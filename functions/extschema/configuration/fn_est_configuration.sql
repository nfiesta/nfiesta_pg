--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_est_configuration(integer, regclass)
--DROP FUNCTION @extschema@.fn_est_configuration(integer,integer,character varying,integer,integer[]);
CREATE OR REPLACE FUNCTION @extschema@.fn_est_configuration(_estimation_cell integer, _estimation_period integer, _note varchar, _target_variable integer, _panels integer[] DEFAULT NULL)
RETURNS setof integer
AS
$function$
DECLARE
_total_estimate_conf		integer;
_estimate_date_begin		date;
_estimate_date_end		date;
_stratas			integer[];
_panels_used			integer[];
_refyearsets			integer[];
_estimation_cells		integer[];
BEGIN

_estimation_cells := array[_estimation_cell];
-- panels order
IF _panels IS NOT NULL THEN _panels := (SELECT array_agg(panel ORDER BY panel) FROM unnest(_panels) AS t(panel));
END IF;

SELECT estimate_date_begin, estimate_date_end
FROM @extschema@.c_estimation_period
WHERE id = _estimation_period
INTO _estimate_date_begin, _estimate_date_end;

IF _estimate_date_begin IS NULL OR _estimate_date_end IS NULL
THEN
	RAISE EXCEPTION 'At leats one of the estimate period dates (%, %) is NULL!', _estimate_date_begin, _estimate_date_end;
END IF;

	-- panels with target variable in specified stratas (panels are the ones with the less granularity, hence 1 stratum can have e.g. 4 panels which together results in 1 big panel)
	WITH w_data AS MATERIALIZED (
		SELECT
			stratum, panel, reference_year_set, reference_year_set_fit, total, is_max
		FROM
			@extschema@.fn_get_panels_in_estimation_cells(_estimation_cells, _estimate_date_begin, _estimate_date_end, _target_variable)
		)
	SELECT
		array_agg(DISTINCT stratum ORDER BY stratum) AS stratas,
		array_agg(panel ORDER BY panel) AS panels,
		array_agg(reference_year_set ORDER BY panel) AS refyearsets
	FROM
		w_data
	WHERE
		-- pick up the most dense panel with target variable
		CASE WHEN _panels IS NOT NULL THEN ARRAY[panel] <@ _panels ELSE
		is_max = true
		END
	INTO _stratas, _panels_used, _refyearsets;

	-- simple check if panels found are the same as panels required
	IF _panels IS NOT NULL AND (_panels != _panels_used OR _panels_used IS NULL)
	THEN
		RAISE EXCEPTION 'Required panels does not meet the computation criteria (measured target variable for given estimation period)!
Only these panels from specified array can be used: (%). Or You can try to not specify panels, the function will try to find the maximum of possible panels.', _panels_used;
	END IF;

RETURN QUERY
	SELECT
		@extschema@.fn_1p_est_configuration(panel_refyearset_group, estimation_cell, _estimation_period, _note, _target_variable)
	FROM
		@extschema@.fn_get_panel_refyearset_group4cells(_panels_used, _refyearsets, _estimation_cells);
END;
$function$
LANGUAGE plpgsql
PARALLEL SAFE;

COMMENT ON FUNCTION @extschema@.fn_est_configuration(integer, integer, varchar, integer, integer[]) IS 'Wrapper function. Handles data between fn_get_panels_in_estimation_cells and fn_1p_est_configuration.';
