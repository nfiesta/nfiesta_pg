--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: nfiesta.fn_1p_est_configuration(integer, integer, integer, varchar, integer)

--DROP FUNCTION nfiesta.fn_1p_est_configuration(integer, integer, integer, character varying, integer);

CREATE OR REPLACE FUNCTION nfiesta.fn_1p_est_configuration(
		_panel_refyearset_group integer, 
		_estimation_cell integer, _estimation_period integer, 
		_note varchar, _variable integer)
RETURNS integer
AS
$function$
DECLARE
_total_estimate_conf		integer;
_stratas_wp			integer[];
_target_label			varchar;
_cell				varchar;
_change_variable		boolean;
BEGIN

	_target_label := (
			SELECT		replace(
						replace(
							concat(coalesce(((t2.metadata->'en')->'indicator')->>'label'::varchar,'x'), ',', coalesce(t3.label,'x'), ',', coalesce(t4.label,'x'), ',', coalesce(t5.label,'x')),
						'x,',''),
					',x','') AS label
			FROM		nfiesta.t_variable AS t1
			LEFT JOIN 	nfiesta.c_target_variable AS t2 ON t1.target_variable = t2.id
			LEFT JOIN	nfiesta.c_sub_population_category AS t3 ON t1.sub_population_category = t3.id
			LEFT JOIN	nfiesta.c_area_domain_category AS t4 ON t1.area_domain_category = t4.id
			LEFT JOIN	nfiesta.c_auxiliary_variable_category AS t5 ON t1.auxiliary_variable_category = t5.id
			WHERE
				t1.id = _variable
			);

	/*
	_change_variable := (
			SELECT		CASE WHEN t3.id = 2 THEN true ELSE false END
			FROM 		nfiesta.t_variable AS t1
			LEFT JOIN 	nfiesta.c_target_variable AS t2 ON t1.target_variable = t2.id
			LEFT JOIN	nfiesta.c_state_or_change AS t3 ON t2.state_or_change = t3.id
			WHERE t1.id = _variable
			);
	*/
		

	_cell := (SELECT label FROM nfiesta.c_estimation_cell WHERE id = _estimation_cell);
	
	-- insert into table t_total_estimate_conf
	INSERT INTO nfiesta.t_total_estimate_conf (estimation_cell, estimation_period, total_estimate_conf, 
							variable, phase_estimate_type, aux_conf, panel_refyearset_group)
	VALUES
		(_estimation_cell, _estimation_period, concat('1p;T=',_target_label,';Cell=',_cell,_note), _variable, 1, NULL, _panel_refyearset_group)
	RETURNING id
	INTO _total_estimate_conf;

	-- insert into table t_estimate_conf
	INSERT INTO nfiesta.t_estimate_conf (estimate_type, total_estimate_conf, denominator)
	SELECT 1, _total_estimate_conf, NULL;

	RETURN _total_estimate_conf;
END;
$function$
LANGUAGE plpgsql
PARALLEL SAFE;

COMMENT ON FUNCTION nfiesta.fn_1p_est_configuration(integer,integer,integer,varchar,integer) IS 'Function makes the estimate configuration present in the database - provides inserts into the all necessary tables.';
