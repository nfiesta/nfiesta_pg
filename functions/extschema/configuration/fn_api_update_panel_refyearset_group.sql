-- Copyright 2017, 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_api_update_panel_refyearset_group(INT, VARCHAR(200), TEXT, VARCHAR(200), TEXT)
--DROP FUNCTION @extschema@.fn_api_update_panel_refyearset_group(INT, VARCHAR(200), TEXT, VARCHAR(200), TEXT);

CREATE OR REPLACE FUNCTION @extschema@.fn_api_update_panel_refyearset_group (_id INT, _label VARCHAR(200), _description TEXT, _label_en VARCHAR(200), _description_en TEXT)
 RETURNS VOID
 LANGUAGE plpgsql
AS $function$

BEGIN

IF _id IS NULL THEN
	RAISE EXCEPTION 'Error 01: fn_api_update_panel_refyearset_group: Function argument _id INT must not be NULL!';
END IF;

IF _label IS NULL THEN
	RAISE EXCEPTION 'Error 02: fn_api_update_panel_refyearset_group: Function argument _label VARCHAR(200) must not be NULL!';
END IF;

IF _description IS NULL THEN
	RAISE EXCEPTION 'Error 03: fn_api_update_panel_refyearset_group: Function argument _description TEXT must not be NULL!';
END IF;

IF _label_en IS NULL THEN
	RAISE EXCEPTION 'Error 04: fn_api_update_panel_refyearset_group: Function argument _label_en VARCHAR(200) must not be NULL!';
END IF;

IF _description_en IS NULL THEN
	RAISE EXCEPTION 'Error 05: fn_api_update_panel_refyearset_group: Function argument _description_en TEXT must not be NULL!';
END IF;

IF NOT EXISTS (SELECT * FROM @extschema@.c_panel_refyearset_group WHERE id = _id) THEN
	RAISE EXCEPTION 'Error 06: fn_api_update_panel_refyearset_group: Panel with reference year set group id = % does not exist in table @extschema@.c_panel_refyearset_group and cannot be updated.', $1;
END IF;

IF _id <> (SELECT id FROM @extschema@.c_panel_refyearset_group WHERE label = _label) THEN
	RAISE EXCEPTION 'Error 07: fn_api_update_panel_refyearset_group: Function argument _label VARCHAR(200) already exists in table @extschema@.c_panel_refyearset_group.';
END IF;

IF _id <> (SELECT id FROM @extschema@.c_panel_refyearset_group WHERE description = _description) THEN
	RAISE EXCEPTION 'Error 08: fn_api_update_panel_refyearset_group: Function argument _description TEXT already exists in table @extschema@.c_panel_refyearset_group.';
END IF;

IF _id <> (SELECT id FROM @extschema@.c_panel_refyearset_group WHERE label_en = _label_en) THEN
	RAISE EXCEPTION 'Error 09: fn_api_update_panel_refyearset_group: Function argument _label_en VARCHAR(200) already exists in table @extschema@.c_panel_refyearset_group.';
END IF;

IF _id <> (SELECT id FROM @extschema@.c_panel_refyearset_group WHERE description_en = _description_en) THEN
	RAISE EXCEPTION 'Error 10: fn_api_update_panel_refyearset_group: Function argument _description_en TEXT already exists in table @extschema@.c_panel_refyearset_group.';
END IF;

UPDATE @extschema@.c_panel_refyearset_group 
SET 
	label = _label,
	description = _description,
	label_en = _label_en,
	description_en = _description_en
WHERE id = _id;
RAISE NOTICE 'Updating row in @extschema@.c_panel_refyearset_group with id = %.', _id;

END;
$function$
;

COMMENT ON FUNCTION @extschema@.fn_api_update_panel_refyearset_group(INT, VARCHAR(200), TEXT, VARCHAR(200), TEXT) IS 
'The function updates label, description, label_en, description_en (passed as arguments 2-5) in lookup '
'table @extschema@.c_panel_refyearset_group, where id = _id passed as an argument 1.';

/*
-- testing false inputs

-- passing NULL for arguments
SELECT * FROM @extschema@.fn_api_update_panel_refyearset_group(NULL,NULL,NULL,NULL,NULL);
SELECT * FROM @extschema@.fn_api_update_panel_refyearset_group(10,NULL,NULL,NULL,NULL);

-- existing _label and _description of the same id as input _id
SELECT * FROM @extschema@.fn_api_update_panel_refyearset_group(1,'no_of_panels: 1;{1}','no_of_panels: 1;{1}','new_label_en','new_description_en');

-- non-existing group, no records
SELECT * FROM @extschema@.fn_api_update_panel_refyearset_group(-20,'new_label','new_description','new_label_en','new_description_en');

-- testing valid inputs

-- group 25
SELECT * FROM @extschema@.fn_api_update_panel_refyearset_group(25,'new_label_35','new_description_35','new_label_en_35','new_description_en_35');

*/

