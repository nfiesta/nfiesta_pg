-- Copyright 2017, 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: nfiesta.fn_api_get_list_of_estimation_periods()
--DROP FUNCTION nfiesta.fn_api_get_list_of_estimation_periods();

CREATE OR REPLACE FUNCTION nfiesta.fn_api_get_list_of_estimation_periods()
 RETURNS TABLE(id INT, estimate_date_begin DATE, estimate_date_end DATE, label VARCHAR(200), description TEXT, 
 label_en VARCHAR(200), description_en TEXT, default_in_olap BOOLEAN)
 LANGUAGE plpgsql
AS $function$
BEGIN

RETURN QUERY EXECUTE ' 
SELECT 
	id,
	estimate_date_begin,
	estimate_date_end,
	label,
	description,
	label_en,
	description_en,
    default_in_olap
FROM nfiesta.c_estimation_period
ORDER BY label, label_en;';
END;
$function$
;

COMMENT ON FUNCTION nfiesta.fn_api_get_list_of_estimation_periods() IS 
'The function returns id, estimate_date_begin, estimate_date_end, label, description, label_en, description_en, default_in_olap '
'from nfiesta.c_estimation_period of all estimation periods.';

/*
-- testing
SELECT * FROM nfiesta.fn_api_get_list_of_estimation_periods();
*/
