-- Copyright 2017, 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: nfiesta.fn_api_delete_estimation_period(integer)
--DROP FUNCTION nfiesta.fn_api_delete_estimation_period(integer);

CREATE OR REPLACE FUNCTION nfiesta.fn_api_delete_estimation_period (_id INT)
 RETURNS VOID
 LANGUAGE plpgsql
AS $function$
BEGIN

IF _id IS NULL THEN
	RAISE EXCEPTION 'Error 01: fn_api_delete_estimation_period: Function argument _id INT must not be NULL!';
END IF;

IF NOT EXISTS (SELECT * FROM nfiesta.c_estimation_period WHERE id = _id) THEN
	RAISE EXCEPTION 'Error 02: fn_api_delete_estimation_period: Estimation period with id = % does not exist in table nfiesta.c_estimation_period and cannot be deleted.', $1;
END IF;

IF EXISTS (SELECT * FROM nfiesta.t_total_estimate_conf WHERE estimation_period = _id) THEN
	RAISE EXCEPTION 'Error 03: fn_api_delete_estimation_period: Period with reference year set group id = % is referenced from table nfiesta.t_total_estimate_conf and cannot be deleted.', $1;
END IF;

DELETE FROM nfiesta.c_estimation_period WHERE id = _id;
RAISE NOTICE 'Deleting row from nfiesta.c_estimation_period with id = %.', $1;

END;
$function$
;

COMMENT ON FUNCTION nfiesta.fn_api_delete_estimation_period(integer) IS 
'The function deletes row from lookup table nfiesta.c_estimation_period, where id = _id passed as an argument.';

/*
-- testing false inputs

-- passing NULL for _id
SELECT * FROM nfiesta.fn_api_delete_estimation_period(NULL);

-- non-existing group, no records
SELECT * FROM nfiesta.fn_api_delete_estimation_period(-20);

-- testing valid inputs
SELECT * FROM nfiesta.fn_api_delete_estimation_period(1);
*/
