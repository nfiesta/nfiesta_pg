-- Copyright 2017, 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: nfiesta.fn_api_get_group4panel_refyearset_combinations(integer[], integer[])
--DROP FUNCTION nfiesta.fn_api_get_group4panel_refyearset_combinations(integer[], integer[]);
CREATE OR REPLACE FUNCTION nfiesta.fn_api_get_group4panel_refyearset_combinations(_panels integer[], _refyearsets integer[]) 
RETURNS TABLE (
	id INT, 
	label VARCHAR(200),
	label_en VARCHAR(200),
	description TEXT,
	description_en TEXT)
AS
$function$
BEGIN

IF _panels IS NULL THEN
	RAISE EXCEPTION 'fn_api_get_group4panel_refyearset_combinations: Function argument _panels INT[] must not be NULL!';
END IF;

IF _refyearsets IS NULL THEN
	RAISE EXCEPTION 'fn_api_get_group4panel_refyearset_combinations: Function argument _refyearsets INT[] must not be NULL!';
END IF;

-- test on the same length of each array (except estimation_cells)
IF
	array_length(_panels,1) != array_length(_refyearsets,1)
THEN
	RAISE EXCEPTION 'fn_api_get_group4panel_refyearset_combinations: Lengths of arrays of function arguments _panels INT[] and _refyearsets INT[] do not match!';
END IF; 

-- checking input arrays if they do not contain NULL
IF (SELECT array_position(_panels, NULL) IS NOT NULL) THEN
	RAISE EXCEPTION 'fn_api_get_group4panel_refyearset_combinations: Function argument _panels INT[] must not be an array containing NULL!';
END IF;

IF (SELECT array_position(_refyearsets, NULL) IS NOT NULL) AND (SELECT array_length(array_positions(_refyearsets, NULL),1) != array_length(_refyearsets,1)) THEN
	RAISE EXCEPTION 'fn_api_get_1pgroups4regtotal: Function argument _reyearsets INT[] must contain either NULLs only or NOT NULLs only!';
END IF;

RETURN QUERY EXECUTE ' 
WITH w_groups_agg AS MATERIALIZED (
	SELECT 	
		t1.panel_refyearset_group,
		array_agg(panel ORDER BY panel, reference_year_set) AS panels, 
		array_agg(reference_year_set ORDER BY panel, reference_year_set) AS refyearsets
	FROM 
		nfiesta.t_panel_refyearset_group AS t1
	GROUP BY t1.panel_refyearset_group
), w_input_arrays_ordered AS MATERIALIZED (
SELECT 
	array_agg(panels.id ORDER BY panels.id, refyearsets.id) AS panels,
	array_agg(refyearsets.id ORDER BY panels.id, refyearsets.id) AS refyearsets
FROM 
	unnest($1) WITH ORDINALITY panels (id, ord)
FULL JOIN 
	unnest($2) WITH ORDINALITY refyearsets (id, ord)
ON panels.ord = refyearsets.ord
)
SELECT 
	t3.id,
	t3.label,
	t3.label_en,
	t3.description,
	t3.description_en
FROM 
	w_groups_agg AS t1
INNER JOIN
	w_input_arrays_ordered AS t2
ON t1.panels = t2.panels AND t1.refyearsets = t2.refyearsets
INNER JOIN
	nfiesta.c_panel_refyearset_group AS t3
ON t1.panel_refyearset_group = t3.id;' USING _panels, _refyearsets; -- comparison of arrays independent on their ordering
END;
$function$
LANGUAGE plpgsql
PARALLEL SAFE;

COMMENT ON FUNCTION nfiesta.fn_api_get_group4panel_refyearset_combinations(integer[],integer[]) IS 
'The function returns the group of panel and reference year sets corresponding  '
'to the input arrays. If there is no such group, the function returns no records. '
'It also finds groups with no reference-year sets attached (NULLs in the '
'function argument _refyearsets) that are used for auxiliary configurations '
'within nfiesta.t_aux_conf. If the input argument _refyearsets contains only NULLs, it '
'must be explicitly cast to ::int[] within the function call.';

/*
-- testing false inputs

-- passing NULL for _panels
SELECT * FROM nfiesta.fn_api_get_group4panel_refyearset_combinations(NULL, ARRAY[2,3,3]);

-- passing NULL for _refyearsets
SELECT * FROM nfiesta.fn_api_get_group4panel_refyearset_combinations(ARRAY[1,2,5], NULL);

-- passing _panels and _refyearsets of unequal length
SELECT * FROM nfiesta.fn_api_get_group4panel_refyearset_combinations(ARRAY[1,2,5], ARRAY[2]);

-- pasisng NULL within _panels
SELECT * FROM nfiesta.fn_api_get_group4panel_refyearset_combinations(ARRAY[1,NULL,5], ARRAY[2,3,3]);

-- passing NULL within _refyearsets
SELECT * FROM nfiesta.fn_api_get_group4panel_refyearset_combinations(ARRAY[1,2,5], ARRAY[2,NULL,3]);

-- valid inputs

-- existing group id = 23
SELECT * FROM nfiesta.fn_api_get_group4panel_refyearset_combinations(ARRAY[1,5,7], ARRAY[2,3,3]);

-- testing the dependence on the order of arrays, should give the same group id = 23
SELECT * FROM nfiesta.fn_api_get_group4panel_refyearset_combinations(ARRAY[7,5,1], ARRAY[3,3,2]);

-- should give no group as the ordered pairs of panel and refyearset do not match group id = 23
SELECT * FROM nfiesta.fn_api_get_group4panel_refyearset_combinations(ARRAY[1,5,7], ARRAY[3,2,3]);

-- existing group id = 10
SELECT * FROM nfiesta.fn_api_get_group4panel_refyearset_combinations(ARRAY[1,2,5], ARRAY[NULL,NULL,NULL]::int[]);

-- non existing group, no records retrieved
SELECT * FROM nfiesta.fn_api_get_group4panel_refyearset_combinations(ARRAY[1,2,5], ARRAY[2,3,3]);

-- helping code
SELECT 
	array_agg(panels.id ORDER BY panels.id, refyearsets.id) AS panels,
	array_agg(refyearsets.id ORDER BY panels.id, refyearsets.id) AS refyearsets
FROM 
	unnest(ARRAY[3,1,5]) WITH ORDINALITY panels (id, ord)
FULL JOIN 
	unnest(ARRAY[30, 10, 50]) WITH ORDINALITY refyearsets (id, ord)
ON panels.ord = refyearsets.ord 
*/
