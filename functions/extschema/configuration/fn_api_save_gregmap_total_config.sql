--
-- Copyright 2017, 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.

-- Function: nfiesta.fn_api_save_gregmap_total_config(INT, INT, INT, INT, INT, BOOLEAN, INT, BOOLEAN)
-- DROP FUNCTION nfiesta.fn_api_save_gregmap_total_config(INT, INT, INT, INT, INT, BOOLEAN, INT, BOOLEAN);

CREATE OR REPLACE FUNCTION nfiesta.fn_api_save_gregmap_total_config(_estimation_period INT, _estimation_cell INT, _variable INT,  
	_panel_refyearset_group INT, _working_model INT, _sigma BOOLEAN, _param_area_type INT, _force_synthetic BOOLEAN)
RETURNS INT
AS
$function$
DECLARE 
_panels INT[];
_null_refyearsets INT[]; 
_panel_group_aux INT[];
_param_area INT[];
_param_area_code VARCHAR(100);
_aux_conf INT[];
_estimation_cell_label VARCHAR(20);
_target_label VARCHAR;
_total_estimate_conf INT;

BEGIN
-- testing input parameters if not NULL
IF _estimation_period IS NULL THEN
	RAISE EXCEPTION 'fn_api_save_gregmap_total_config: Function argument _estimation_period INT must not be NULL!';
END IF;	
	
IF _estimation_cell IS NULL THEN
	RAISE EXCEPTION 'fn_api_save_gregmap_total_config: Function argument _estimation_cell INT must not be NULL!';
END IF;

IF _variable IS NULL THEN
	RAISE EXCEPTION 'fn_api_save_gregmap_total_config: Function argument _variable INT must not be NULL!';
END IF;

IF _panel_refyearset_group IS NULL THEN
	RAISE EXCEPTION 'fn_api_save_gregmap_total_config: Function argument _panel_refyearset_group INT must not be NULL!';
END IF;

IF _working_model IS NULL THEN
	RAISE EXCEPTION 'fn_api_save_gregmap_total_config: Function argument _working_model INT must not be NULL!';
END IF;

IF _sigma IS NULL THEN
	RAISE EXCEPTION 'fn_api_save_gregmap_total_config: Function argument _sigma BOOLEAN must not be NULL!';
END IF;

IF _param_area_type IS NULL THEN
	RAISE EXCEPTION 'fn_api_save_gregmap_total_config: Function argument _param_area_type INT must not be NULL!';
END IF;

IF _force_synthetic IS NULL THEN
	RAISE EXCEPTION 'fn_api_save_gregmap_total_config: Function argument _force_synthetic BOOLEAN must not be NULL!';
END IF;

-- find out panels of the input _panel_refyearset_group
SELECT array_agg(panel) FROM nfiesta.fn_api_get_panel_refyearset_combinations4groups(ARRAY[_panel_refyearset_group]) INTO _panels; -- panels 1,2

IF _panels IS NULL THEN
	RAISE EXCEPTION 'fn_api_save_gregmap_total_config: No panels found for the chosen group of panels and '
	'reference-year set combinations (function argument _panel_refyearset_group). The configuration process cannot proceed!';
END IF;


-- construct array of NULLs with the length corresponfing to _panels
SELECT 
	array_agg(NULL::int)::int[]
FROM 
	generate_series(1, array_length(_panels,1)) AS a(i)
INTO _null_refyearsets;

-- determine the panel group having the same set of panel but no reference-year sets, this should be found in the matching records of t_aux_conf 
SELECT array_agg(id) FROM nfiesta.fn_api_get_group4panel_refyearset_combinations(_panels, _null_refyearsets) INTO _panel_group_aux; -- panel group 6

IF _panel_group_aux IS NULL THEN
	RAISE EXCEPTION 'fn_api_save_gregmap_total_config: No auxiliary group of panels matching the chosen group of panels and '
	'reference-year set combinations (function argument _panel_refyearset_group) found! The configuration process cannot proceed!';
END IF;

IF array_length(_panel_group_aux,1) > 1 THEN
	RAISE EXCEPTION 'fn_api_save_gregmap_total_config: More than one auxiliary group of panels matching the chosen group of panels and '
	'reference-year set combinations (function argument _panel_refyearset_group) found! The configuration process cannot proceed!';
END IF;

-- determine parameterisation areas beonging to the input _param_area_type and containing the cell  
SELECT 
	array_agg(gid)
FROM 
	nfiesta.f_a_param_area AS t1
INNER JOIN 
	nfiesta.cm_cell2param_area_mapping AS t2
	ON t1.gid = t2.param_area
WHERE 
	t1.param_area_type = _param_area_type AND 
	t2.estimation_cell = _estimation_cell
INTO _param_area;

IF _param_area IS NULL THEN
	RAISE EXCEPTION 'fn_api_save_gregmap_total_config: No parameteristion area matching the combination of estimation cell (_estimation_cell) '
	'and paramatrisation area type (_param_area_type) found! The configuration process cannot proceed!';
END IF;

IF array_length(_param_area,1) > 1 THEN
	RAISE EXCEPTION 'fn_api_save_gregmap_total_config: More than one parameteristion area matching the combination of estimation cell '
	'(_estimation_cell) and paramatrisation area type (_param_area_type) found! The configuration process cannot proceed!';
END IF;

SELECT 
	array_agg(id)
FROM 
	nfiesta.t_aux_conf AS t1
WHERE 
	sigma = _sigma AND	
	model = _working_model AND 
	panel_refyearset_group = _panel_group_aux[1] AND
	param_area = _param_area[1]
INTO _aux_conf;

IF _aux_conf IS NULL THEN
	RAISE EXCEPTION 'fn_api_save_gregmap_total_config: No matching auxiliary configuration found! The configuration process cannot proceed!';
END IF;

IF array_length(_aux_conf,1) > 1 THEN
	RAISE EXCEPTION 'fn_api_save_gregmap_total_config: More than one matching auxiliary configuration found! The configuration process cannot proceed!';
END IF;

-- RETURN nfiesta.fn_2p_est_configuration(_estimation_cell, _estimation_period, 'API-GUI configuration', _variable, _aux_conf[1], _force_synthetic);

_param_area_code := (SELECT param_area_code FROM nfiesta.f_a_param_area WHERE gid = _param_area[1]);
_estimation_cell_label := (SELECT label FROM nfiesta.c_estimation_cell WHERE id = _estimation_cell);

-- create the label of estimate
_target_label := (
		SELECT		replace(
					replace(
						concat(coalesce(((t2.metadata->'en')->'indicator')->>'label'::varchar,'x'), ',', coalesce(t3.label,'x'), ',', coalesce(t4.label,'x'), ',', coalesce(t5.label,'x')),
					'x,',''),
			       	',x','') AS label
		FROM		nfiesta.t_variable AS t1
		LEFT JOIN 	nfiesta.c_target_variable AS t2 ON t1.target_variable = t2.id
		LEFT JOIN	nfiesta.c_sub_population_category AS t3 ON t1.sub_population_category = t3.id
		LEFT JOIN	nfiesta.c_area_domain_category AS t4 ON t1.area_domain_category = t4.id
		LEFT JOIN	nfiesta.c_auxiliary_variable_category AS t5 ON t1.auxiliary_variable_category = t5.id
		WHERE
			t1.id = _variable
		);

-- insert into table t_total_estimate_conf
INSERT INTO nfiesta.t_total_estimate_conf (
	estimation_cell, 
	estimation_period, 
	total_estimate_conf, 
	variable, 
	phase_estimate_type, 
	force_synthetic, 
	aux_conf, 
	panel_refyearset_group)
VALUES (
	_estimation_cell,  
	_estimation_period, 
	concat('GREG-map;T=',_target_label,';C=',_estimation_cell_label,';PA=',_param_area_code, ';m=', _working_model, 'API-GUI configuration'), 
	_variable, 
	2, 
	_force_synthetic, 
	_aux_conf[1], 
	_panel_refyearset_group)
ON CONFLICT (estimation_cell, estimation_period, variable, phase_estimate_type, coalesce(force_synthetic, false), coalesce(aux_conf, 0), panel_refyearset_group)
DO NOTHING
RETURNING id
INTO _total_estimate_conf;

IF _total_estimate_conf IS NOT NULL
THEN
	-- insert into table t_estimate_conf
	INSERT INTO nfiesta.t_estimate_conf (estimate_type, total_estimate_conf, denominator)
	SELECT 1, _total_estimate_conf, NULL;
	
	-- check for existance of auxiliary total in t_aux_total
	IF	(
			SELECT
				array_agg(t_variable.id ORDER BY t_variable.id)
			FROM nfiesta.t_aux_total
			INNER JOIN nfiesta.t_variable ON (t_aux_total.variable = t_variable.id AND t_aux_total.is_latest)
			INNER JOIN nfiesta.c_estimation_cell ON (t_aux_total.estimation_cell = c_estimation_cell.id AND c_estimation_cell.id = _estimation_cell)
			INNER JOIN nfiesta.t_model_variables ON t_model_variables.variable = t_variable.id
			INNER JOIN nfiesta.t_model ON t_model.id = t_model_variables.model
			INNER JOIN nfiesta.t_aux_conf ON t_aux_conf.model = t_model_variables.model AND t_aux_conf.id = _aux_conf[1]
		)
		!= (
			SELECT
				array_agg(t_model_variables.variable ORDER BY variable)
			FROM nfiesta.t_aux_conf
			INNER JOIN nfiesta.t_model ON t_model.id = t_aux_conf.model AND t_aux_conf.id = _aux_conf[1]
			INNER JOIN nfiesta.t_model_variables ON t_model_variables.model = t_model.id
		)
	THEN
		RAISE EXCEPTION 'fn_api_save_gregmap_total_config:: The total of one or more auxiliary variables is not available! (_estimation_cell: %, model: %)',
			_estimation_cell, _model;
	END IF;
ELSE
	RAISE NOTICE 'fn_api_save_gregmap_total_config: This GREG-map configuration already exists (estimation_period: %, estimation_cell: %, variable: %, 
	panel_refyearset_group: %, working_model: %, sigma: %, param_are_type: %, force_synthetic: %)!', 
	_estimation_period, _estimation_cell, _variable, _panel_refyearset_group, _working_model, _sigma, _param_area_type, _force_synthetic;
END IF;

RETURN _total_estimate_conf;

END;
$function$
LANGUAGE plpgsql
PARALLEL SAFE;

COMMENT ON FUNCTION  nfiesta.fn_api_save_gregmap_total_config(INT, INT, INT, INT, INT, BOOLEAN, INT, BOOLEAN) IS 
'This API function saves the configuration of GREG-map total estimates.';

/* tests
-------------------------------------------------------------------------------------------------------------------------------------------------------
-- tests for function nfiesta.fn_api_save_gregmap_total_config(_estimation_period INT, _estimation_cell INT, _variable INT,  
--	_panel_refyearset_group INT, _working_model INT, _sigma BOOLEAN, _param_area_type INT, _force_synthetic BOOLEAN)
-------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------- testing invalid inputs ------------------------------------------------------------------------------
-- test NULL for _estimation_period argument
SELECT * FROM nfiesta.fn_api_save_gregmap_total_config(NULL,55,1,19,1,FALSE,3,FALSE);

-- test NULL for _estimation_cell argument
SELECT * FROM nfiesta.fn_api_save_gregmap_total_config(1,NULL,1,19,1,FALSE,3,FALSE);

-- test NULL for _variable argument
SELECT * FROM nfiesta.fn_api_save_gregmap_total_config(1,55,NULL,19,1,FALSE,3,FALSE);

-- test NULL for _panel_refyearset_group argument
SELECT * FROM nfiesta.fn_api_save_gregmap_total_config(1,55,1,NULL,1,FALSE,3,FALSE);

-- test NULL for _working_model argument
SELECT * FROM nfiesta.fn_api_save_gregmap_total_config(1,55,1,19,NULL,FALSE,3,FALSE);

-- test NULL for _sigma argument
SELECT * FROM nfiesta.fn_api_save_gregmap_total_config(1,55,1,19,1,NULL,3,FALSE);

-- test NULL for _param_area_type argument
SELECT * FROM nfiesta.fn_api_save_gregmap_total_config(1,55,1,19,1,FALSE,NULL,FALSE);

-- test NULL for _force_synthetic argument
SELECT * FROM nfiesta.fn_api_save_gregmap_total_config(1,55,1,19,1,FALSE,3,NULL);

-- test for an invalid group of panel and reference-year set combinations - no panels  found
SELECT * FROM nfiesta.fn_api_save_gregmap_total_config(1,55,1,-1,1,FALSE,3,FALSE);

-- test for a group of panel and reference-year set combinations that has no equivaent in t_aux_conf (with no reference year sets attached)
SELECT * FROM nfiesta.fn_api_save_gregmap_total_config(1,55,1,33,1,FALSE,3,FALSE);

-- test for a parametrisation area not matching the estimation cell
SELECT * FROM nfiesta.fn_api_save_gregmap_total_config(1,55,1,19,1,FALSE,2,FALSE);

-- test for missing aux configuration - no one has sigma TRUE
SELECT * FROM nfiesta.fn_api_save_gregmap_total_config(1,55,1,19,1,TRUE,3,FALSE);

------------------------------------------------ testing valid inputs ---------------------------------------------------------------------------------
-- save a configuration with force_synthetic = FALSE  
SELECT * FROM nfiesta.fn_api_save_gregmap_total_config(1,55,1,19,1,FALSE,3,FALSE);

-- save a configuration with force_synthetic = TRUE  
SELECT * FROM nfiesta.fn_api_save_gregmap_total_config(1,55,1,19,1,FALSE,3,TRUE);


-- helping code
-- aux_conf corresponding to group 19
SELECT * FROM nfiesta.fn_api_get_wmodels_sigma_paramtypes(ARRAY[1,2], ARRAY[55,56,60,61,67,68,69,70,75,76,77,83]); -- model 1, sigma FALSE, param area type 3

_estimation_cell = 55,
_panel_refyearset_group = 19
_working_model = 1
_sigma = FALSE
_param_area_type = 3
_param_area = 28
_estimation_period = 1,
_variable = 1
aux_panel_refyearset_group = 6

SELECT * FROM 
 (SELECT panel_refyearset_group, array_agg(panel ORDER BY panel) AS p, array_agg(reference_year_set) FROM nfiesta.t_panel_refyearset_group WHERE reference_year_set IS NOT NULL GROUP BY panel_refyearset_group) AS t1  
LEFT JOIN 
 (SELECT panel_refyearset_group, array_agg(panel ORDER BY panel) AS p, array_agg(reference_year_set) FROM nfiesta.t_panel_refyearset_group WHERE reference_year_set IS NULL GROUP BY panel_refyearset_group) AS t2
ON t1.p = t2.p

*/
