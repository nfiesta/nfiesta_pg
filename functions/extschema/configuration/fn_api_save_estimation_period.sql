-- Copyright 2017, 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.

-- Function: nfiesta.fn_api_save_estimation_period(DATE, DATE, VARCHAR(200), TEXT, VARCHAR(200), TEXT, BOOLEAN)
-- DROP FUNCTION nfiesta.fn_api_save_estimation_period(DATE, DATE, VARCHAR(200), TEXT, VARCHAR(200), TEXT, BOOLEAN);

CREATE OR REPLACE FUNCTION nfiesta.fn_api_save_estimation_period(_estimate_date_begin DATE, _estimate_date_end DATE, _label VARCHAR(200), _description TEXT, 
	_label_en VARCHAR(200), _description_en TEXT, _default_in_olap BOOLEAN)
RETURNS INT
AS
$function$
DECLARE
_estimation_period		integer;
BEGIN

-- testing input parameters if not NULL
IF _estimate_date_begin IS NULL THEN
	RAISE EXCEPTION 'Error 01: fn_api_save_estimation_period: Function argument _estimate_date_begin DATE must not be NULL!';
END IF;

IF _estimate_date_end IS NULL THEN
	RAISE EXCEPTION 'Error 02: fn_api_save_estimation_period: Function argument _estimate_date_end DATE must not be NULL!';
END IF;

IF _label IS NULL THEN
	RAISE EXCEPTION 'Error 03: fn_api_save_estimation_period: Function argument _label VARCHAR(200) must not be NULL!';
END IF;

IF _description IS NULL THEN
	RAISE EXCEPTION 'Error 04: fn_api_save_estimation_period: Function argument _description TEXT must not be NULL!';
END IF;


IF _label_en IS NULL THEN
	RAISE EXCEPTION 'Error 05: fn_api_save_estimation_period: Function argument _label_en VARCHAR(200) must not be NULL!';
END IF;

IF _description_en IS NULL THEN
	RAISE EXCEPTION 'Error 06: fn_api_save_estimation_period: Function argument _description_en TEXT must not be NULL!';
END IF;

INSERT INTO nfiesta.c_estimation_period (estimate_date_begin, estimate_date_end, label, description, label_en, description_en, default_in_olap)
SELECT _estimate_date_begin, _estimate_date_end, _label, _description, _label_en, _description_en, _default_in_olap
RETURNING id INTO _estimation_period;

RETURN _estimation_period;

END;
$function$
LANGUAGE plpgsql
PARALLEL SAFE;

COMMENT ON FUNCTION  nfiesta.fn_api_save_estimation_period(DATE, DATE, VARCHAR(200), TEXT, VARCHAR(200), TEXT, BOOLEAN) IS 
'The function insert row into lookup table nfiesta.c_estimation_period. The columns estimate_date_begin, '
'estimate_date_end, label, description, label_en, description_en, default_in_olap are passed as input arguments '
'of the function. The function returns a unique identifier of the newly inserted estimation period.';

/* tests
-------------------------------------------------------------------------------------------------------------------------------------------------------
-- tests for function nfiesta.fn_api_save_estimation_period(_estimate_date_begin DATE, _estimate_date_end DATE, _label VARCHAR(200), _description TEXT, 
--	_label_en VARCHAR(200), _description_en TEXT, default_in_olap BOOLEAN
-------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------- testing invalid inputs ------------------------------------------------------------------------------
-- test NULL for _estimate_date_begin argument
SELECT * FROM nfiesta.fn_api_save_estimation_period(NULL, '2015-12-31', 'nejaky label', 'nejaky description', 'some label en', 'some description_en', NULL);

-- test NULL for _estimate_date_end argument
SELECT * FROM nfiesta.fn_api_save_estimation_period('2011-01-01', NULL, 'nejaky label', 'nejaky description', 'some label en', 'some description_en', NULL);

-- passing NULL for _label
SELECT * FROM nfiesta.fn_api_save_estimation_period('2011-01-01', '2015-12-31', NULL, 'nejaky description', 'some label en', 'some description_en', NULL);

-- passing NULL for _description
SELECT * FROM nfiesta.fn_api_save_estimation_period('2011-01-01', '2015-12-31', 'nejaky label', NULL, 'some label en', 'some description_en', NULL);

-- passing NULL for _label_en
SELECT * FROM nfiesta.fn_api_save_estimation_period('2011-01-01', '2015-12-31', 'nejaky label', 'nejaky description', NULL, 'some description_en', NULL);

-- passing NULL for _description_en
SELECT * FROM nfiesta.fn_api_save_estimation_period('2011-01-01', '2015-12-31', 'nejaky label', 'nejaky description', 'some label en', NULL, NULL);

-- test if saving a estimation period with begin date and end date already used 
SELECT * FROM nfiesta.fn_api_save_estimation_period('2011-01-01', '2015-12-31', 'nejaky label', 'nejaky description', 'some label en', 'some description_en', false);

------------------------------------------------ testing valid inputs ---------------------------------------------------------------------------------
SELECT * FROM nfiesta.fn_api_save_estimation_period('2016-01-01', '2020-12-31', 'NIL3', '3. cyklus NIL', 'NFI3', '3rd cycle of czech NFI', true);
*/