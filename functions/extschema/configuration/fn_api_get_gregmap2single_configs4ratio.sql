-- Copyright 2017, 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: nfiesta.fn_api_get_gregmap2single_configs4ratio(INT, INT[], INT[], INT[])
-- DROP FUNCTION nfiesta.fn_api_get_gregmap2single_configs4ratio(INT, INT[], INT[], INT[]);

CREATE OR REPLACE FUNCTION nfiesta.fn_api_get_gregmap2single_configs4ratio(_estimation_period INT, _estimation_cells INT[], _variables_nom INT[], _variables_denom INT[])
RETURNS TABLE(
 			panel_refyearset_group_id INT,
			panel_refyearset_group_label VARCHAR(200),  
			panel_refyearset_group_label_en VARCHAR(200),
			panel_refyearset_group_description TEXT,
			panel_refyearset_group_description_en TEXT,
			nominator_param_area_type_id INT,
			nominator_param_area_type_label VARCHAR(200),
			nominator_param_area_type_label_en VARCHAR(200),
			nominator_param_area_type_description TEXT,
			nominator_param_area_type_description_en TEXT,
			nominator_force_synthetic BOOLEAN, -- always FALSE, because single-phase nominator is always design-based and we want to avoid mixed inference
			nominator_model_id INT, 
			nominator_model_label VARCHAR(200),
			nominator_model_label_en VARCHAR(200),
			nominator_model_description TEXT,
			nominator_model_description_en TEXT,
			nominator_model_sigma BOOLEAN, 
			all_estimates_configurable BOOLEAN,
			total_estimate_conf_nominator INT[],
			total_estimate_conf_denominator INT[]
	)
AS
$function$
BEGIN

-- testing input arguments if not NULL
IF _estimation_period IS NULL THEN
	RAISE EXCEPTION 'fn_api_get_gregmap2single_configs4ratio: Function argument _estimation_period INT must not be NULL!';
END IF;

IF _estimation_cells IS NULL THEN
	RAISE EXCEPTION 'fn_api_get_gregmap2single_configs4ratio: Function argument _estimation_cells INT[] must not be NULL!';
END IF;

IF _variables_nom IS NULL THEN
	RAISE EXCEPTION 'fn_api_get_gregmap2single_configs4ratio: Function argument _variables_nom INT[] must not be NULL!';
END IF;

IF _variables_denom IS NULL THEN
	RAISE EXCEPTION 'fn_api_get_gregmap2single_configs4ratio: Function argument _variables_denom INT[] must not be NULL!';
END IF;

-- checking input arrays if they do not contain NULL
IF (SELECT array_position(_estimation_cells, NULL) IS NOT NULL) THEN
	RAISE EXCEPTION 'fn_api_get_gregmap2single_configs4ratio: Function argument _estimation_cells INT[] must not be an array containing NULL!';
END IF;

IF (SELECT array_position(_variables_nom, NULL) IS NOT NULL) THEN
	RAISE EXCEPTION 'fn_api_get_gregmap2single_configs4ratio: Function argument _variables_nom INT[] must not be an array containing NULL!';
END IF;

IF (SELECT array_position(_variables_denom, NULL) IS NOT NULL) THEN
	RAISE EXCEPTION 'fn_api_get_gregmap2single_configs4ratio: Function argument _variables_denom INT[] must not be an array containing NULL!';
END IF;

-- checking input arrays if they do not contain NULL
IF (SELECT array_position(_estimation_cells, NULL) IS NOT NULL) THEN
	RAISE EXCEPTION 'fn_api_get_gregmap2single_configs4ratio: Function argument _estimation_cells INT[] must not be an array containing NULL!';
END IF; 

IF (SELECT array_position(_variables_nom, NULL) IS NOT NULL) THEN
	RAISE EXCEPTION 'fn_api_get_gregmap2single_configs4ratio: Function argument _variables_nom INT[] must not be an array containing NULL!';
END IF; 

IF (SELECT array_position(_variables_denom, NULL) IS NOT NULL) THEN
	RAISE EXCEPTION 'fn_api_get_gregmap2single_configs4ratio: Function argument _variables_denom INT[] must not be an array containing NULL!';
END IF; 

-- check equality of lengths of _variables_nom and _variables_denom
IF (SELECT array_length(_variables_nom, 1) != array_length(_variables_denom, 1)) THEN
	RAISE EXCEPTION 'fn_api_get_gregmap2single_configs4ratio: Function arguments _variables_nom INT[] and _variables_denom INT[] must be arrays of the same lengths!';
END IF; 

RETURN QUERY EXECUTE '
	WITH w_vars AS MATERIALIZED ( -- combinations of variables in nominator and denominator
		SELECT 
			vnom.vid AS variable_nominator, 
			vdenom.vid AS variable_denominator
		FROM 
			unnest($3) WITH ORDINALITY vnom (vid,ord) 
		FULL JOIN 
			unnest($4) WITH ORDINALITY vdenom (vid, ord) 
		ON vnom.ord = vdenom.ord
	), w_greg_map_totals_conf AS MATERIALIZED (
		SELECT 
			array_agg(t2.id) OVER (PARTITION BY t1.panel_refyearset_group, t1.force_synthetic, t5.param_area_type, t4.model, t4.sigma) AS total_estimate_conf_nominator,
			array_agg(t1.id) OVER (PARTITION BY t1.panel_refyearset_group, t1.force_synthetic, t5.param_area_type, t4.model, t4.sigma) AS total_estimate_conf_denominator,
			t1.estimation_period,
			t1.estimation_cell,
			t1.panel_refyearset_group,
			t8.label AS panel_refyearset_group_label,
			t8.label_en AS panel_refyearset_group_label_en,
			t8.description AS panel_refyearset_group_description,
			t8.description_en AS panel_refyearset_group_description_en,
			t1.variable AS nominator_variable,
			t2.variable AS denominator_variable,
			t5.gid AS param_area,
			t5.label AS param_area_label,
			t6.id AS param_area_type,
			t6.label AS param_area_type_label,
			t6.label_en AS param_area_type_label_en,
			t6.description AS param_area_type_description,
			t6.description_en AS param_area_type_description_en,
			t7.id AS model,
			t7.label AS model_label,
			t7.label_en AS model_label_en,
			t7.description AS model_description,
			t7.description_en AS model_description_en,
			t4.sigma AS model_sigma,
			count(*) OVER (PARTITION BY t1.panel_refyearset_group, t1.force_synthetic, t5.param_area_type, t4.model, t4.sigma) AS n_configuarations,
			count(*) OVER (PARTITION BY t1.panel_refyearset_group, t1.force_synthetic, t5.param_area_type, t4.model, t4.sigma) = array_length($2,1) * array_length($3,1) AS all_estimates_configurable
		FROM
			nfiesta.t_total_estimate_conf AS t1 -- look up  single-phase total configs for the ratio
		INNER JOIN
			nfiesta.t_total_estimate_conf AS t2 -- look up GREG-map total configs for the ratio
		ON 
			t1.estimation_period = $1 AND t2.estimation_period = $1  AND
			ARRAY[t1.estimation_cell] <@ $2 AND
			ARRAY[t2.estimation_cell] <@ $2 AND
			ARRAY[t1.variable] <@ $4 AND -- single-phase total must have configs for variables listed for denominator
			ARRAY[t2.variable] <@ $3 AND -- GREG-map total must have configs for variables listed for nominator
			t1.phase_estimate_type = 1 AND t2.phase_estimate_type = 2 AND -- the required ratio is a combination of single-phase and GREG-map totals
			t1.estimation_cell = t2.estimation_cell AND 
			t1.panel_refyearset_group = t2.panel_refyearset_group AND -- group of panels and reference year sets must match between nom and denom
			NOT t2.force_synthetic -- combination with single-phase (always design-based) is only possible with design-based GREG-map estimators 
		INNER JOIN 
			w_vars AS t3
		ON 
			t1.variable = t3.variable_denominator AND
			t2.variable = t3.variable_nominator
		INNER JOIN 
			nfiesta.t_aux_conf AS t4 
			ON t2.aux_conf = t4.id
		INNER JOIN 
			nfiesta.f_a_param_area AS t5
			ON t4.param_area = t5.gid
		INNER JOIN 
			nfiesta.c_param_area_type AS t6
			ON t5.param_area_type = t6.id
		INNER JOIN 
			nfiesta.t_model AS t7 
			ON t4.model = t7.id
		 INNER JOIN 
		 nfiesta.c_panel_refyearset_group AS t8
		 ON t1.panel_refyearset_group = t8.id
	)
	SELECT DISTINCT 
			panel_refyearset_group AS panel_refyearset_group_id,
			panel_refyearset_group_label,  
			panel_refyearset_group_label_en,
			panel_refyearset_group_description,
			panel_refyearset_group_description_en,
			param_area_type AS param_area_type_id,
			param_area_type_label,
			param_area_type_label_en,
			param_area_type_description,
			param_area_type_description_en,
			FALSE AS force_synthetic,
			model AS denominator_model_id, 
			model_label AS denominator_model_label,
			model_label_en AS denominator_model_label_en,
			model_description AS denominator_model_description,
			model_description_en AS denominator_model_description_en,
			model_sigma AS denominator_model_sigma,
			all_estimates_configurable,
			total_estimate_conf_nominator,
			total_estimate_conf_denominator
	FROM 
		 w_greg_map_totals_conf' USING _estimation_period, _estimation_cells, _variables_nom, _variables_denom;
END;
$function$
LANGUAGE plpgsql
PARALLEL SAFE;

COMMENT ON FUNCTION  nfiesta.fn_api_get_gregmap2single_configs4ratio(INT, INT[], INT[], INT[]) IS 
'For input estimation period, estimation cells, nominator and denominator variables the function returns ' 
'combinations of panel-refyearset group, type of parametrisation area, force_synthetic (always FALSE due '
'to the design-based single-phase total in the denominator and avoidance of mixed-minference), working model ' 
'and sigma of already configured GREG-map (nominator) and the corresponding single-phase (denominator) total ' 
'estimates. The parameter all_estimates_configurable is TRUE if the corresponding ratio can be configured for '
'all input combinations of estimation cell, nominator variable and denominator variable. The functuion combines ' 
'the elements of the input arrays variables_nom and variables_denom (considering their order) to obtain the ' 
'desired set of estimates. These two input arrays must have an equal length, and their elements must be ordered, ' 
'so each combination of their elements on the same position corresponds to one desired combination of variables ' 
'in the nominator and the denominator of the ratio. The output parameters total_estimate_conf_nominator and ' 
'total_estimate_conf_denominator are ordered arrays of identifiers of existing GREG-map or single-phase ' 
'configurations (record ids of the table nfiesta.t_total_estimate_conf) for the nominator and denominator '
'respectively.';

/* tests
------------------------------------------------- testing invalid inputs ------------------------------------------------------------------------------
-- test NULL for _estimation_period argument
SELECT * FROM nfiesta.fn_api_get_gregmap2single_configs4ratio(NULL, ARRAY[100], ARRAY[1,2,3], ARRAY[3,3,3]);

-- test NULL for _estimation_cell argument
SELECT * FROM nfiesta.fn_api_get_gregmap2single_configs4ratio(1, NULL, ARRAY[1,2,3], ARRAY[3,3,3]);

-- test NULL for _variables_nom argument
SELECT * FROM nfiesta.fn_api_get_gregmap2single_configs4ratio(1, ARRAY[100], NULL, ARRAY[3,3,3]);

-- test NULL for _variables_denom argument
SELECT * FROM nfiesta.fn_api_get_gregmap2single_configs4ratio(1, ARRAY[100], ARRAY[1,2,3], NULL);

-- test for NULL as an element of _estimation_cell argument
SELECT * FROM nfiesta.fn_api_get_gregmap2single_configs4ratio(1, ARRAY[100, NULL], ARRAY[1,2,3], ARRAY[3,3,3]);
SELECT * FROM nfiesta.fn_api_get_gregmap2single_configs4ratio(1, ARRAY[NULL]::int[], ARRAY[1,2,3], ARRAY[3,3,3]);

-- test for NULL as an element of _variables_nom argument
SELECT * FROM nfiesta.fn_api_get_gregmap2single_configs4ratio(1, ARRAY[100], ARRAY[1,NULL,3], ARRAY[3,3,3]);
SELECT * FROM nfiesta.fn_api_get_gregmap2single_configs4ratio(1, ARRAY[100], ARRAY[NULL]::int[], ARRAY[3,3,3]);

-- test for NULL as an element of _variables_denom argument
SELECT * FROM nfiesta.fn_api_get_gregmap2single_configs4ratio(1, ARRAY[100], ARRAY[1,2,3], ARRAY[3,3,NULL]);
SELECT * FROM nfiesta.fn_api_get_gregmap2single_configs4ratio(1, ARRAY[100], ARRAY[1,2,3]::int[], ARRAY[NULL]::int[]);

-- test if _variables_nom and _variables_denom have equal lengths
SELECT FROM nfiesta.fn_api_get_gregmap2single_configs4ratio(1, ARRAY[100], ARRAY[1,2,3], ARRAY[3,3]);

------------------------------------------------ testing valid inputs ---------------------------------------------------------------------------------
-- one uncomplete config, INSPIRE 50x50 km param area type, common panel refyearset group (19) exists for cells 55 and 69, but not including cell 57
-- the other uncomplete config for using strata set as param area
SELECT * FROM nfiesta.fn_api_get_gregmap2single_configs4ratio(1, ARRAY[55,57,69], ARRAY[1,2,3], ARRAY[3,3,3]);

-- one complete and one uncomplete config is obtained if cell 57 is dropped from the above input
SELECT * FROM nfiesta.fn_api_get_gregmap2single_configs4ratio(1, ARRAY[55,69], ARRAY[1,2,3], ARRAY[3,3,3]);

-- two complete configs, two param area types: INSPIRE 50x50 km and the whole strata set 
SELECT * FROM nfiesta.fn_api_get_gregmap2single_configs4ratio(1, ARRAY[100], ARRAY[1,2,3], ARRAY[3,3,3]);

------------------------------------------------ helping code ---------------------------------------------------------------------------------
-- cells 55 and 69 use the same panel refyearset group, but a different one from cell 57, therefore there is no config covering all estimation cells
SELECT estimation_cell, panel_refyearset_group FROM nfiesta.t_total_estimate_conf 
WHERE phase_estimate_type = 1 AND ARRAY[estimation_cell] <@  ARRAY[55,57,69] AND ARRAY[variable] <@ ARRAY[3];

-- all three cells have a configuration using the same common panel refyearset group for 50km param area type,
-- cell 55, but not the other two, has a configuration for the same panel refyearset group and the param area type 
-- correspondong to the whole strata set, herefore not all cells can be configured with this param area type  (note the the second test call using valid inputs)
SELECT * FROM nfiesta.t_total_estimate_conf 
WHERE phase_estimate_type = 2 AND ARRAY[estimation_cell] <@  ARRAY[55,57,69] AND ARRAY[variable] <@ ARRAY[1,2,3] AND NOT force_synthetic;

-- for estimatin cell 100 and variable 3 there is one single phase configuration using panel refyearset group no 25
SELECT estimation_cell, panel_refyearset_group FROM nfiesta.t_total_estimate_conf 
WHERE phase_estimate_type = 1 AND ARRAY[estimation_cell] <@  ARRAY[100] AND ARRAY[variable] <@ ARRAY[3];

-- for the cell 100 and each of the variables (1,2,3) there are two GREG-map total configs, one with param area type 50km INSPIRE, and the other using
-- the whole strataset as param area type
SELECT * FROM nfiesta.t_total_estimate_conf 
WHERE phase_estimate_type = 2 AND ARRAY[estimation_cell] <@  ARRAY[100] AND ARRAY[variable] <@ ARRAY[1,2,3] AND NOT force_synthetic;

*/
