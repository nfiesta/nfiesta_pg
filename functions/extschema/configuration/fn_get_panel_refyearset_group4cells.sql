--
-- Copyright 2017, 2021 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_get_panel_refyearset_group4cells(integer[], integer[], integer[], varchar, text)
--DROP FUNCTION @extschema@.fn_get_panel_refyearset_group4cells(integer[], integer[], integer[], varchar, text);
CREATE OR REPLACE FUNCTION @extschema@.fn_get_panel_refyearset_group4cells(
		_panels integer[], _refyearsets integer[], 
		_estimation_cells integer[], _label varchar DEFAULT NULL, _description text DEFAULT NULL)
RETURNS TABLE (estimation_cell integer, panel_refyearset_group integer)
AS
$function$
DECLARE
_estimation_cell		integer;
_panel_refyearset_group		integer;
_panels_ec			integer[];
_refyearsets_ec			integer[];
BEGIN

IF _panels IS NULL THEN
	RAISE NOTICE 'Input array of panels is NULL!';
END IF;
-- test on the same length of each array (except estimation_cells)
IF
	array_length(_panels,1) != array_length(_refyearsets,1)
THEN
	RAISE EXCEPTION 'Input arrays of panels (%) and refyearsets (%) are not the same length.', _panels, _refyearsets;
END IF; 

-- loop through an array of estimation cells
FOREACH _estimation_cell IN ARRAY _estimation_cells
LOOP
	-- find out which panels belongs to specified estimation_cell
	SELECT
		array_agg(t3.panel ORDER BY t2.id), array_agg(t4.refyearset ORDER BY t2.id)
	FROM
		sdesign.t_panel AS t1
	INNER JOIN
		@extschema@.t_stratum_in_estimation_cell AS t2
	ON
		t1.stratum = t2.stratum
	INNER JOIN
		unnest(_panels) WITH ORDINALITY AS t3(panel, id)
	ON t1.id = t3.panel
	INNER JOIN
		unnest(_refyearsets) WITH ORDINALITY AS t4(refyearset, id)
	ON t3.id = t4.id
	WHERE t2.estimation_cell = _estimation_cell
	INTO _panels_ec, _refyearsets_ec;

	SELECT @extschema@.fn_get_panel_refyearset_group(_panels_ec, _refyearsets_ec)
	INTO _panel_refyearset_group;
	

	IF _panel_refyearset_group IS NULL
	THEN
		SELECT @extschema@.fn_save_panel_refyearset_group(_panels_ec, _refyearsets_ec)
		INTO _panel_refyearset_group;
	END IF;

	estimation_cell := _estimation_cell;
	panel_refyearset_group := _panel_refyearset_group;

	RETURN NEXT;
END LOOP;
END;
$function$
LANGUAGE plpgsql
PARALLEL SAFE;

COMMENT ON FUNCTION @extschema@.fn_get_panel_refyearset_group4cells(integer[],integer[],integer[],varchar,text) IS 'Returns table with estimation cells and belonging panel_refyearset_group (if necessary, provides insert into lookup).';
