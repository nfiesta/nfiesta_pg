-- Copyright 2017, 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.

-- Function: nfiesta.fn_api_get_wmodels_sigma_paramtypes(INT[], INT[])
-- DROP FUNCTION nfiesta.fn_api_get_wmodels_sigma_paramtypes(INT[], INT[]);

CREATE OR REPLACE FUNCTION nfiesta.fn_api_get_wmodels_sigma_paramtypes(_panels INT[], _estimation_cells INT[])
RETURNS TABLE(
	working_model_id INT,
	working_model_label VARCHAR(200),
	working_model_label_en VARCHAR(200),
	working_model_description TEXT,
	working_model_description_en TEXT,
	sigma BOOLEAN,
	param_area_type_id INT,
	param_area_type_label VARCHAR(200),
	param_area_type_label_en VARCHAR(200),
	param_area_type_description TEXT,
	param_area_type_descriptipn_en TEXT,
	no_of_cells_covered BIGINT,
	all_cells_included BOOLEAN
)
AS
$function$
BEGIN

-- testing input parameters if not NULL
IF _panels IS NULL THEN
	RAISE EXCEPTION 'fn_api_get_wmodels_sigma_paramtypes: Function argument _panels INT[] must not be NULL!';
END IF;

IF _estimation_cells IS NULL THEN
	RAISE EXCEPTION 'fn_api_get_wmodels_sigma_paramtypes: Function argument _estimation_cells INT[] must not be NULL!';
END IF;

-- checking input arrays if they do not contain NULL
IF (SELECT array_position(_panels, NULL) IS NOT NULL) THEN
	RAISE EXCEPTION 'fn_api_get_wmodels_sigma_paramtypes:  Function argument _panels INT[] must not be an array containing NULL!';
END IF;

IF (SELECT array_position(_estimation_cells, NULL) IS NOT NULL) THEN
	RAISE EXCEPTION 'fn_api_get_wmodels_sigma_paramtypes:  Function argument _estimation_cells INT[] must not be an array containing NULL!';
END IF;

-- checking one cell collection within input parameter _estimation_cells 
IF (SELECT count(DISTINCT estimation_cell_collection) FROM nfiesta.c_estimation_cell WHERE ARRAY[id] <@ _estimation_cells) IS DISTINCT FROM 1 THEN 
	RAISE EXCEPTION 'fn_api_get_wmodels_sigma_paramtypes: Function argument _estimation_cells INT[] must contain cells corresponding to one and the only estimation cell collection!';
END IF;

RETURN QUERY EXECUTE ' 
WITH w_eligible_panel_groups AS MATERIALIZED (
	SELECT 
		t1.panel_refyearset_group,
		array_agg(t2.panel) AS panels_aux
	FROM 
		(SELECT DISTINCT panel_refyearset_group FROM nfiesta.t_aux_conf) AS t1
	INNER JOIN
		nfiesta.t_panel_refyearset_group AS t2
	ON t1.panel_refyearset_group = t2.panel_refyearset_group  
	GROUP BY t1.panel_refyearset_group
	HAVING array_agg(t2.panel) <@ $1 AND $1 <@ array_agg(t2.panel)
), w_eligible_models_sigma_paramtypes AS MATERIALIZED (
	SELECT 
		t1.model,
		t1.sigma,
		t4.param_area_type,
		t4.gid AS param_area_id,
		t4.param_area_code,
		t3.estimation_cell, 
		count(*) OVER (PARTITION BY t1.model, t1.sigma, t4.param_area_type) AS no_of_cells_covered,
		count(*) OVER (PARTITION BY t1.model, t1.sigma, t4.param_area_type) = array_length($2,1) AS all_cells_covered
	FROM 
		nfiesta.t_aux_conf AS t1
	INNER JOIN
		w_eligible_panel_groups AS t2
		ON t1.panel_refyearset_group = t2.panel_refyearset_group
	INNER JOIN 
		nfiesta.cm_cell2param_area_mapping AS t3
	ON t1.param_area = t3.param_area AND ARRAY[t3.estimation_cell] <@  $2
	INNER JOIN 
		nfiesta.f_a_param_area AS t4
		ON t1.param_area = t4.gid
)
SELECT DISTINCT
		t1.model AS model_id,
		t2.label AS model_label,
		t2.label_en AS model_label_en,
		t2.description AS model_description,
		t2.description_en AS model_description_en,
		t1.sigma,
		t1.param_area_type AS param_area_type_id,
		t3.label AS param_area_type_label,
		t3.label_en AS param_area_type_label,
		t3.description AS param_area_type_description,
		t3.description_en AS param_area_type_description_en,
		t1.no_of_cells_covered,
		t1.all_cells_covered
FROM 
		w_eligible_models_sigma_paramtypes AS t1
INNER JOIN
		nfiesta.t_model AS t2
		ON t1.model = t2.id
INNER JOIN 
		nfiesta.c_param_area_type AS t3
		ON t1.param_area_type = t3.id;' USING _panels, _estimation_cells;

END;
$function$
LANGUAGE plpgsql
PARALLEL SAFE;

COMMENT ON FUNCTION  nfiesta.fn_api_get_wmodels_sigma_paramtypes(INT[], INT[]) IS 
'For the input arrays of panels and estimation cells the function returns '
'a table with alternative combinations of working model, sigma and parameterisation '
'area type, for which the regression estimates of total can be configured. Each ' 
'record in the output table corresponds to a set of existing records in the table ' 
'nfiesta.t_aux_conf.' ;

/* tests
-------------------------------------------------------------------------------------------------------------------------------------------------------
-- tests for function nfiesta.fn_api_get_wmodels_sigma_paramtypes(_panels INT[], _estimation_cells INT[])
-------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------- testing invalid inputs ------------------------------------------------------------------------------
-- test NULL for _panels argument
SELECT * FROM nfiesta.fn_api_get_wmodels_sigma_paramtypes(NULL, ARRAY[51,55,56,60,61,67,68,69,70,75,76,77,83]);

-- test NULL for _estimation_cell argument
SELECT * FROM nfiesta.fn_api_get_wmodels_sigma_paramtypes(ARRAY[1,2], NULL);

-- test for NULL as an element of _panels
SELECT * FROM nfiesta.fn_api_get_wmodels_sigma_paramtypes(ARRAY[1,NULL], ARRAY[51,55,56,60,61,67,68,69,70,75,76,77,83]);
SELECT * FROM nfiesta.fn_api_get_wmodels_sigma_paramtypes(ARRAY[NULL], ARRAY[51,55,56,60,61,67,68,69,70,75,76,77,83]);
SELECT * FROM nfiesta.fn_api_get_wmodels_sigma_paramtypes(ARRAY[NULL]::int[], ARRAY[51,55,56,60,61,67,68,69,70,75,76,77,83]);

-- test for NULL as an element of _estimation_cells
SELECT * FROM nfiesta.fn_api_get_wmodels_sigma_paramtypes(ARRAY[1,2], ARRAY[NULL,55,56,60,61,67,68,69,70,75,76,77,83]);
SELECT * FROM nfiesta.fn_api_get_wmodels_sigma_paramtypes(ARRAY[1,2], ARRAY[NULL]);
SELECT * FROM nfiesta.fn_api_get_wmodels_sigma_paramtypes(ARRAY[1,2], ARRAY[NULL]::int[]);

-- test if estimation cells belong to one estimation cell collection (adding 1 to the valid array _estimation_cells
SELECT * FROM nfiesta.fn_api_get_wmodels_sigma_paramtypes(ARRAY[1,2], ARRAY[1,55,56,60,61,67,68,69,70,75,76,77,83]);

------------------------------------------------ testing valid inputs ---------------------------------------------------------------------------------
SELECT * FROM nfiesta.fn_api_get_wmodels_sigma_paramtypes(ARRAY[1,2], ARRAY[55,56,60,61,67,68,69,70,75,76,77,83]);


-------------------------------------------------------- helping code ---------------------------------------------------------------------------------
	SELECT 
		t1.param_area,
		t2.estimation_cell_collection,
		array_agg(t1.estimation_cell) AS estimation_cells 
	FROM 
		nfiesta.cm_cell2param_area_mapping AS t1
	INNER JOIN 
		nfiesta.c_estimation_cell AS t2
	ON t1.estimation_cell = t2.id
	GROUP BY param_area, t2.estimation_cell_collection

	
	SELECT * FROM nfiesta.fn_api_get_1pgroups4regtotal(1,ARRAY[77,83], ARRAY[1,2,3]) -- 16
	SELECT * FROM nfiesta.fn_api_get_1pgroups4regtotal(1,ARRAY[11,12,13,14,15,16], ARRAY[1,2,3]) -- 18
	SELECT * FROM nfiesta.fn_api_get_1pgroups4regtotal(1,ARRAY[51,55,56,60,61,67,68,69,70,75,76,77,83], ARRAY[1,2,3]) -- 14, 16, 19
	
	SELECT * FROM nfiesta.fn_api_get_panel_refyearset_combinations4groups(ARRAY[16]) -- 1
	SELECT * FROM nfiesta.fn_api_get_panel_refyearset_combinations4groups(ARRAY[18]) -- 7
	SELECT * FROM nfiesta.fn_api_get_panel_refyearset_combinations4groups(ARRAY[14,16,19]) -- 1,2
*/