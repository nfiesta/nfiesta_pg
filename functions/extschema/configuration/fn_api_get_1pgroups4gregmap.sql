-- Copyright 2017, 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: nfiesta.fn_api_get_1pgroups4gregmap(INT, INT[], INT[])
-- DROP FUNCTION nfiesta.fn_api_get_1pgroups4gregmap(INT, INT[], INT[]);

CREATE OR REPLACE FUNCTION nfiesta.fn_api_get_1pgroups4gregmap(_estimation_period INT, _estimation_cells INT[], _variables INT[])
RETURNS TABLE(
	estimation_period_id INT, 
	estimation_period_label VARCHAR(200), 
	estimation_period_label_en VARCHAR(200), 
	estimation_period_description TEXT, 
	estimation_period_description_en TEXT,
	country_id INT[],
	country_label VARCHAR(200)[],
	country_label_en VARCHAR(200)[],
	country_description TEXT[],
	country_description_en TEXT[],
	strata_set_id INT[],
	strata_set_label VARCHAR(200)[],
	strata_set_label_en VARCHAR(200)[],
	strata_set_description TEXT[],
	strata_set_description_en TEXT[],
	stratum_id INT[],
	stratum_label VARCHAR(200)[],
	stratum_label_en VARCHAR(200)[],
	stratum_description TEXT[],
	stratum_description_en TEXT[],
	panel_refyearset_group_id INT, 
	panel_refyearset_group_label VARCHAR(200),
	panel_refyearset_group_label_en VARCHAR(200),
	panel_refyearset_group_description TEXT,
	panel_refyearset_group_description_en TEXT,
	complete_group BOOLEAN)
AS
$function$
BEGIN

-- testing input arguments if not NULL
IF _estimation_period IS NULL THEN
	RAISE EXCEPTION 'fn_api_get_1pgroups4gregmap: Function argument _estimation_period INT must not be NULL!';
END IF;

IF _estimation_cells IS NULL THEN
	RAISE EXCEPTION 'fn_api_get_1pgroups4gregmap: Function argument _estimation_cells INT[] must not be NULL!';
END IF;

IF _variables IS NULL THEN
	RAISE EXCEPTION 'fn_api_get_1pgroups4gregmap: Function argument _variables INT[] must not be NULL!';
END IF;

-- checking input arrays if they do not contain NULL
IF (SELECT array_position(_estimation_cells, NULL) IS NOT NULL) THEN
	RAISE EXCEPTION 'fn_api_get_1pgroups4gregmap: Function argument _estimation_cells INT[] must not be an array containing NULL!';
END IF;

IF (SELECT array_position(_variables, NULL) IS NOT NULL) THEN
	RAISE EXCEPTION 'fn_api_get_1pgroups4gregmap: Function argument _variables INT[] must not be an array containing NULL!';
END IF;

-- checking one cell collection within input argument _estimation_cells 
IF (SELECT count(DISTINCT estimation_cell_collection) FROM nfiesta.c_estimation_cell WHERE ARRAY[id] <@ _estimation_cells) IS DISTINCT FROM 1 THEN 
	RAISE EXCEPTION 'fn_api_get_1pgroups4gregmap: Function argument _estimation_cells INT[] must contain cells corresponding to one and the only estimation cell collection!';
END IF;

RETURN QUERY EXECUTE '
WITH w_reg2conf AS MATERIALIZED (
	SELECT DISTINCT -- distinct filters duplicate records for cells intersectiong more strata (we need to follow strata combinations only) 
		$1 AS estimation_period,
		array_agg(t3.stratum) OVER (PARTITION BY t1.estimation_cell, t2.variable) AS strata_combination, -- partition does not include estimation_period because there is only one, and it include variable to avoid duplicates
		t1.estimation_cell,
		t2.variable
	FROM
	  (SELECT unnest($2) AS estimation_cell) AS t1 
	 CROSS JOIN
	  (SELECT unnest($3) AS variable) AS t2
	 INNER JOIN
	  nfiesta.t_stratum_in_estimation_cell AS t3
	 ON t1.estimation_cell = t3.estimation_cell
), w_no_desired_reg_estimates AS MATERIALIZED (
SELECT
	estimation_period, 
	strata_combination,
	count(*)  AS no_of_desired_reg_estimates
FROM w_reg2conf
GROUP BY estimation_period, strata_combination
), w_conf1p AS MATERIALIZED (
SELECT 
	t1.*,
	t3.panel_refyearset_group,
	t2.no_of_desired_reg_estimates,
	count(*) FILTER (WHERE t3.panel_refyearset_group IS NOT NULL) OVER (PARTITION BY t1.strata_combination, t3.panel_refyearset_group) AS no_configured_1pestimates_per_group
FROM 
	w_reg2conf AS t1
INNER JOIN 
	w_no_desired_reg_estimates AS t2
ON t1.strata_combination = t2.strata_combination
LEFT JOIN nfiesta.t_total_estimate_conf  AS t3
	ON 
	t1.estimation_period = t3.estimation_period AND
	t1.estimation_cell = t3.estimation_cell AND
	t1.variable = t3.variable AND
	t3.phase_estimate_type = 1
), w_conf1p_agg AS MATERIALIZED ( 
SELECT DISTINCT
	estimation_period,
	strata_combination,
	panel_refyearset_group,
	no_of_desired_reg_estimates = no_configured_1pestimates_per_group AS complete_group
FROM 
	w_conf1p
WHERE panel_refyearset_group IS NOT NULL
-- the above could be the final query, but we need to add records for strata with NULL in panel_refyearset_group in case there are no 1p configurations 
-- for any cell and for any variable either, so the WHERE condition filter such records out
-- at the same time the WHERE is needed in order to avoid duplicate records (one with NULL group, the other NOT NULL but still incomplete) for groups with
-- partial configuration for some cells or for some variables only
)
SELECT DISTINCT
-- here by left JOIN we get records for all strata_cominations no matter if 1p configurations exists for the corresponding cells and variables, 
--potentially with NULL groups (no 1p configs at all for any cell and any desired variable within the given stratum)
	t3.id AS estimation_period_id,
	t3.label::VARCHAR(200) AS estimation_period_label,
	coalesce(t3.label_en, t3.label)::VARCHAR(200) AS estimation_period_label_en,
    t3.description AS estimation_period_description,
	coalesce(t3.description_en, t3.description) AS estimation_period_description_en,
	array_agg(t6.id) OVER (PARTITION BY t1.strata_combination) AS country_id,
	array_agg(t6.label::VARCHAR(200)) OVER (PARTITION BY t1.strata_combination) AS country_label,
	array_agg(t6.label_en::VARCHAR(200)) OVER (PARTITION BY t1.strata_combination) AS country_label_en,
	array_agg(t6.description) OVER (PARTITION BY t1.strata_combination) AS country_description,
	array_agg(t6.description_en) OVER (PARTITION BY t1.strata_combination) AS country_description_en,
	array_agg(t5.id) OVER (PARTITION BY t1.strata_combination) AS strata_set_id,
	array_agg(t5.strata_set::VARCHAR(200)) OVER (PARTITION BY t1.strata_combination) AS strata_set_label,
	array_agg(t5.strata_set::VARCHAR(200)) OVER (PARTITION BY t1.strata_combination) AS strata_set_label_en,
	array_agg(t5.label::TEXT) OVER (PARTITION BY t1.strata_combination) AS strata_set_description,
	array_agg(t5.label::TEXT) OVER (PARTITION BY t1.strata_combination) AS strata_set_description_en,
	array_agg(t1.stratum) OVER (PARTITION BY t1.strata_combination) AS stratum,
	array_agg(t4.stratum::VARCHAR(200)) OVER (PARTITION BY t1.strata_combination) AS stratum_label,
	array_agg(t4.stratum::VARCHAR(200)) OVER (PARTITION BY t1.strata_combination) AS stratum_label_en,
	array_agg(t4.label::TEXT) OVER (PARTITION BY t1.strata_combination) AS stratum_description,
	array_agg(t4.label::TEXT) OVER (PARTITION BY t1.strata_combination) AS stratum_description_en,
	t7.id AS panel_refyearset_group_id,
	t7.label AS panel_refyearset_group_label,
	coalesce(t7.label_en, t7.label)  AS panel_refyearset_group_label_en,
	t7.description AS panel_refyearset_group_description,
	coalesce(t7.description_en, t7.description) AS panel_refyearset_group_description_en,
	t2.complete_group
FROM 
	(SELECT estimation_period, strata_combination, unnest(strata_combination) AS stratum FROM w_no_desired_reg_estimates) AS t1 -- the unnest is necessary to get all arrays in the same order
LEFT JOIN
	w_conf1p_agg AS t2
	ON t1.strata_combination = t2.strata_combination
INNER JOIN 
	nfiesta.c_estimation_period AS t3
	ON t1.estimation_period = t3.id
INNER JOIN 
	sdesign.t_stratum AS t4
	ON t1.stratum = t4.id
INNER JOIN 
	sdesign.t_strata_set AS t5
	ON t4.strata_set = t5.id
INNER JOIN 
	sdesign.c_country AS t6
	ON t5.country = t6.id
LEFT JOIN 
	nfiesta.c_panel_refyearset_group AS t7
	ON t2.panel_refyearset_group = t7.id;' USING _estimation_period, _estimation_cells, _variables;
END;
$function$
LANGUAGE plpgsql
PARALLEL SAFE;

COMMENT ON FUNCTION  nfiesta.fn_api_get_1pgroups4gregmap(INT, INT[], INT[]) IS 
'This function returns list of distinct strata combinations and panel refyearset groups ' 
'with a Boolean indicator saying whether there is a single-phase total configuration (using '
'the given panel refyearset group) for each GREG-map total estimate in the set of desired '
'estimates (a cartesian product of the subset of estimation cells intersecting all strata '
'of the given strata combination and the desired list of variables).';

/* tests
-- test NULL for _estimation_period argument
SELECT * FROM nfiesta.fn_api_get_1pgroups4gregmap(NULL, ARRAY[44,45,46,47,48,49,50,51,52,53,56,57,58,62], ARRAY[1,2,3]);

-- test NULL for _estimation_cell argument
SELECT * FROM nfiesta.fn_api_get_1pgroups4gregmap(1, NULL, ARRAY[1,2,3]);

-- test NULL for _variables argument
SELECT * FROM nfiesta.fn_api_get_1pgroups4gregmap(1, ARRAY[44,45,46,47,48,49,50,51,52,53,56,57,58,62], NULL);

-- test for NULL as an element of _estimation_cell argument
SELECT * FROM nfiesta.fn_api_get_1pgroups4gregmap(1, ARRAY[44,45,46,47,NULL,49,50,51,52,53,56,57,58,62], ARRAY[1,2,3]);
SELECT * FROM nfiesta.fn_api_get_1pgroups4gregmap(1, ARRAY[NULL], ARRAY[1,2,3]);
SELECT * FROM nfiesta.fn_api_get_1pgroups4gregmap(1, ARRAY[NULL]::int[], ARRAY[1,2,3]);

-- test for NULL as an element of _variables argument
SELECT * FROM nfiesta.fn_api_get_1pgroups4gregmap(1, ARRAY[44,45,46,47,48,49,50,51,52,53,56,57,58,62], ARRAY[NULL,2,NULL]);
SELECT * FROM nfiesta.fn_api_get_1pgroups4gregmap(1, ARRAY[44,45,46,47,48,49,50,51,52,53,56,57,58,62], ARRAY[NULL]);
SELECT * FROM nfiesta.fn_api_get_1pgroups4gregmap(1, ARRAY[44,45,46,47,48,49,50,51,52,53,56,57,58,62], ARRAY[NULL]::int[]);

-- test if estimation cells belong to one estimation cell collection (adding 1 to the valid array _estimation_cells, 1 
-- belongs to another collection but the same stratum
SELECT * FROM nfiesta.fn_api_get_1pgroups4gregmap(1, ARRAY[1,44,45,46,47,48,49,50,51,52,53,56,57,58,62], ARRAY[1,2,3]);

-- tests on valid input
-- cells of one stratum three variables, complete group
SELECT * FROM nfiesta.fn_api_get_1pgroups4gregmap(1, ARRAY[44,45,46,47,48,49,50,51,52,53,56,57,58,62], ARRAY[1,2,3]);

-- cells of one stratum, one variable without configuration added to the former three making the group incomplete
SELECT * FROM nfiesta.fn_api_get_1pgroups4gregmap(1, ARRAY[44,45,46,47,48,49,50,51,52,53,56,57,58,62], ARRAY[1,2,3,-1]);

-- cells of one stratum, one variable without configuration, no group given (NULL)
SELECT * FROM nfiesta.fn_api_get_1pgroups4gregmap(1, ARRAY[44,45,46,47,48,49,50,51,52,53,56,57,58,62], ARRAY[-1]);
*/