-- Copyright 2017, 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: nfiesta.fn_2p_est_configuration(integer, regclass)

--DROP FUNCTION nfiesta.fn_2p_est_configuration(integer,integer,character varying,integer,integer,boolean);

CREATE OR REPLACE FUNCTION nfiesta.fn_2p_est_configuration(_estimation_cell integer, _estimation_period integer, _note varchar, _target_variable integer, _aux_conf integer, _force_synthetic boolean default False)
RETURNS integer
AS
$function$
DECLARE
_total_estimate_conf		integer;
_estimate_date_begin		date;
_estimate_date_end		date;
_stratas			integer[];
_stratas_wp			integer[];
_panels				integer[];
_refyearsets			integer[];
_panels_aux			integer[];
_param_area			integer;
_param_area_code		varchar;
_target_label			varchar;
_model				integer;
_cell				varchar;
_panel_refyearset_group		integer;
BEGIN

-- test for existing g_betas
-- otherwise the configuration cannot be done (sometimes the g_betas cannot be computed)
-- so this prevents to configure non-computable estimates

IF (SELECT count(*) FROM nfiesta.t_g_beta WHERE aux_conf = $5) = 0
THEN
	RAISE EXCEPTION 'fn_2p_est_configuration: G-betas for required aux_conf (%) are not available (cell=%, period=%). The computation of it was not run or is not able to compute (mostly the problem of matrix inversion).', _aux_conf, _estimation_cell, _estimation_period;
END IF;

SELECT estimate_date_begin, estimate_date_end
FROM nfiesta.c_estimation_period
WHERE id = _estimation_period
INTO _estimate_date_begin, _estimate_date_end;

IF _estimate_date_begin IS NULL OR _estimate_date_end IS NULL
THEN
	RAISE EXCEPTION 'fn_2p_est_configuration: At least one of the estimate period dates (%, %) is NULL!', _estimate_date_begin, _estimate_date_end;
END IF;


-- create the label of estimate
_target_label := (
		SELECT		replace(
					replace(
						concat(coalesce(((t2.metadata->'en')->'indicator')->>'label'::varchar,'x'), ',', coalesce(t3.label,'x'), ',', coalesce(t4.label,'x'), ',', coalesce(t5.label,'x')),
					'x,',''),
			       	',x','') AS label
		FROM		nfiesta.t_variable AS t1
		LEFT JOIN 	nfiesta.c_target_variable AS t2 ON t1.target_variable = t2.id
		LEFT JOIN	nfiesta.c_sub_population_category AS t3 ON t1.sub_population_category = t3.id
		LEFT JOIN	nfiesta.c_area_domain_category AS t4 ON t1.area_domain_category = t4.id
		LEFT JOIN	nfiesta.c_auxiliary_variable_category AS t5 ON t1.auxiliary_variable_category = t5.id
		WHERE
			t1.id = _target_variable
		);

_param_area := (SELECT param_area FROM nfiesta.t_aux_conf WHERE id = $5);
_model := (SELECT model FROM nfiesta.t_aux_conf WHERE id = $5);
_param_area_code := (SELECT param_area_code FROM nfiesta.f_a_param_area WHERE gid = _param_area);
_cell := (SELECT label FROM nfiesta.c_estimation_cell WHERE id = $1);

	-- test on param_area_coverage
		SELECT
			array_agg(t1.id ORDER BY t1.id)
		FROM
			sdesign.t_stratum AS t1
		INNER JOIN
			nfiesta.f_a_param_area AS t2
		ON
			-- buffered stratum?
			-- no, if only buffer of the stratum would intersect the cell, 
			-- no consequences on estimate would be introduced, the buffer is in general used only for the edge effect compensation (the compensated area is within non-buffered stratum)
			ST_Intersects(t1.geom, t2.geom) AND NOT ST_Touches(t1.geom, t2.geom)
		WHERE
			t2.gid = _param_area
		INTO _stratas;

		IF _stratas IS NULL
		THEN
			RAISE EXCEPTION 'The specified cell is not intersected by any stratum. Choose another estimation cell.';
		END IF;

	-- existing panels configured in panel2aux_conf
		SELECT
			array_agg(t1.panel ORDER BY t1.panel)
		FROM
			nfiesta.t_panel_refyearset_group AS t1
		INNER JOIN
			nfiesta.t_aux_conf AS t2
		ON	t1.panel_refyearset_group = t2.panel_refyearset_group
		WHERE
			t2.id = _aux_conf
		INTO _panels_aux;

	-- check of panel2total_2ndph
	-- and addition of panels from param_area - is the target variable available not only in cell?

		WITH w_data AS MATERIALIZED (
			SELECT
				t1.id AS stratum, t2.id AS panel, t9.id AS reference_year_set, t2.plot_count AS total
			FROM
				sdesign.t_stratum AS t1
			INNER JOIN
				sdesign.t_panel AS t2
			ON
				t1.id = t2.stratum
			INNER JOIN
				sdesign.cm_refyearset2panel_mapping AS t8
			ON
				t2.id = t8.panel --AND
				--t8.id = t9.reference_year_set
			INNER JOIN
				sdesign.t_reference_year_set AS t9
			ON
				t8.reference_year_set = t9.id
			INNER JOIN
				nfiesta.t_available_datasets AS t6
			ON
				t2.id = t6.panel AND
				t9.id = t6.reference_year_set
			INNER JOIN
				nfiesta.t_variable AS t7
			ON
				t6.variable = t7.id
			WHERE
				array[t1.id] <@ _stratas AND
				t7.id = _target_variable AND 
				(t9.reference_date_begin >= _estimate_date_begin AND
				t9.reference_date_end <= _estimate_date_end)
			GROUP BY
				t1.id, t2.id, t9.id
		)
		SELECT
			array_agg(DISTINCT stratum ORDER BY stratum) AS stratas,
			array_agg(panel ORDER BY panel) AS panels,
			array_agg(reference_year_set ORDER BY panel) AS refyearsets
		FROM
			(SELECT
				stratum, panel, reference_year_set,
				total,
				max(total) OVER(PARTITION BY stratum, panel, reference_year_set) AS max_total
			FROM
				w_data
			) AS t1
		WHERE
			-- pick up the most dense panel with target variable
			total = max_total
		INTO _stratas_wp, _panels, _refyearsets;

		IF _panels != _panels_aux OR _panels IS NULL
		THEN
			RAISE EXCEPTION 'fn_2p_est_configuration: Not all panels coming from g_beta have available target variable! aux_conf: %, panels: %, panels_aux: %',
			_aux_conf, _panels, _panels_aux;
		END IF;

		_panel_refyearset_group := (SELECT nfiesta.fn_get_panel_refyearset_group(_panels, _refyearsets));

		IF _panel_refyearset_group IS NULL
		THEN
			_panel_refyearset_group := (SELECT nfiesta.fn_save_panel_refyearset_group(_panels, _refyearsets));
		END IF;

-- insert into table t_total_estimate_conf
INSERT INTO nfiesta.t_total_estimate_conf (estimation_cell, estimation_period, total_estimate_conf, variable, phase_estimate_type, force_synthetic, aux_conf, panel_refyearset_group)
VALUES
	($1, $2, concat('GREG-map;T=',_target_label,';C=',_cell,';PA=',_param_area_code, ';m=',_model,_note), $4, 2, $6, $5, _panel_refyearset_group)
ON CONFLICT (estimation_cell, estimation_period, variable, phase_estimate_type, coalesce(force_synthetic,false), coalesce(aux_conf,0), panel_refyearset_group)
DO NOTHING
RETURNING id
INTO _total_estimate_conf;

IF _total_estimate_conf IS NOT NULL
THEN
	-- insert into table t_estimate_conf
	INSERT INTO nfiesta.t_estimate_conf (estimate_type, total_estimate_conf, denominator)
	SELECT 1, _total_estimate_conf, NULL;

	IF	(
			SELECT
				array_agg(t_variable.id ORDER BY t_variable.id)
			FROM nfiesta.t_aux_total
			INNER JOIN nfiesta.t_variable 		ON (t_aux_total.variable = t_variable.id)
			INNER JOIN nfiesta.c_estimation_cell 	ON (t_aux_total.estimation_cell = c_estimation_cell.id)
			INNER JOIN nfiesta.t_model_variables 	ON t_model_variables.variable = t_variable.id
			INNER JOIN nfiesta.t_model 			ON t_model.id = t_model_variables.model
			INNER JOIN nfiesta.t_aux_conf 		ON t_aux_conf.model = t_model_variables.model
			WHERE 	c_estimation_cell.id = $1 AND
				t_aux_conf.id = $5 AND
				t_aux_total.is_latest

		)
		!= (
			SELECT
				array_agg(t_model_variables.variable order by variable)
			FROM nfiesta.t_aux_conf
			INNER JOIN nfiesta.t_model ON t_model.id = t_aux_conf.model
			INNER JOIN nfiesta.t_model_variables ON t_model_variables.model = t_model.id
			WHERE t_aux_conf.id = $5
		)
	THEN
		RAISE EXCEPTION 'fn_2p_est_configuration: t_aux_total not found! (total_estimate_conf: %)', _total_estimate_conf;
	END IF;
ELSE
	RAISE NOTICE 'fn_2p_est_configuration: Required configuration already exists!';
END IF;

RETURN _total_estimate_conf;

END;
$function$
LANGUAGE plpgsql
PARALLEL SAFE;

COMMENT ON FUNCTION nfiesta.fn_2p_est_configuration(integer,integer,character varying,integer,integer,boolean) IS 
'The function saves configuration of GREG-map total into t_total_estimate_conf and into t_estimate_conf.';
