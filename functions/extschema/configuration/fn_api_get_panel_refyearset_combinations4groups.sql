-- Copyright 2017, 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European CommISsion - subsequent versions of the EUPL (the "Licence");
-- You may not use thIS work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software dIStributed under the Licence IS dIStributed on an "AS IS" basIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permISsions and limitations under the Licence.
--

-- Function: @extschema@.fn_api_get_panel_refyearset_combinations4groups(integer[])
--DROP FUNCTION @extschema@.fn_api_get_panel_refyearset_combinations4groups(integer[]);

CREATE OR REPLACE FUNCTION @extschema@.fn_api_get_panel_refyearset_combinations4groups(_panel_refyearset_groups INT[])
RETURNS TABLE (
	panel INT, 
	panel_label VARCHAR(200), 
	panel_label_en VARCHAR(200), 
	panel_description TEXT, 
	panel_description_en TEXT, 
	reference_year_set INT, 
	refyearset_label VARCHAR(200), 
	refyearset_label_en VARCHAR(200), 
	refyearset_description TEXT, 
	refyearset_description_en TEXT,
	cluster_count INT, 
	plot_count INT
)
AS
$function$
BEGIN

IF _panel_refyearset_groups IS NULL THEN
	RAISE EXCEPTION 'Error 01: fn_api_get_panel_refyearset_combinations4groups: Function argument _panel_refyearset_groups INT[] must not be NULL!';
END IF;

-- checking _panel_refyearset_groups array if does not contain NULL
IF (SELECT array_position(_panel_refyearset_groups, NULL) IS NOT NULL) THEN
	RAISE EXCEPTION 'Error 02: fn_api_get_panel_refyearset_combinations4groups: Function argument _panel_refyearset_groups INT[] must not be an array containing NULL!';
END IF;

RETURN QUERY EXECUTE '
WITH w AS (
	SELECT DISTINCT panel,reference_year_set
	FROM @extschema@.t_panel_refyearset_group
	WHERE ARRAY[panel_refyearset_group] <@ $1
)
SELECT 
	t1.id AS panel,
	t1.panel AS panel_label,
	t1.panel AS panel_label_en, 
	t1.label::text AS panel_description, 
	t1.label::text AS panel_description_en, 
	t2.id AS reference_year_set,
	t2.reference_year_set AS refyearset_label,
	t2.reference_year_set AS refyearset_label_en, 
	t2.label::text AS refyearset_description, 
	t2.label::text AS refyearset_description_en, 	
	t1.cluster_count,
	t1.plot_count
FROM sdesign.t_panel AS t1
JOIN w ON t1.id = w.panel
LEFT JOIN sdesign.t_reference_year_set AS t2 ON t2.id = w.reference_year_set;' USING _panel_refyearset_groups;

END;
$function$
LANGUAGE plpgsql
PARALLEL SAFE;

COMMENT ON FUNCTION @extschema@.fn_api_get_panel_refyearset_combinations4groups(integer[]) IS 
'The function returns ids, labels and description of panels and reference-year sets belonging '
'to the group(s) entered as an input argument. It also returns the number of clusters (cluster_count) '
'and plots (plot_count) from sdesign.t_panel of all panels in the group(s).';

/*
-- testing false inputs

-- passing NULL for _panel_refyearset_groups
SELECT * FROM @extschema@.fn_api_get_panel_refyearset_combinations4groups(NULL);

-- passing an _panel_refyearset_groups containing NULL
SELECT * FROM @extschema@.fn_api_get_panel_refyearset_combinations4groups(ARRAY[23, 10, NULL]);

-- testing valid inputs

-- group 23
SELECT * FROM @extschema@.fn_api_get_panel_refyearset_combinations4groups(ARRAY[23]);

-- group 10 (no reference year sets)
SELECT * FROM @extschema@.fn_api_get_panel_refyearset_combinations4groups(ARRAY[10]);

-- group 20
SELECT * FROM @extschema@.fn_api_get_panel_refyearset_combinations4groups(ARRAY[20]);

-- groups 20 and 23, note combination 1 (panel) and 2 (refyearset) included only once in the function output
SELECT * FROM @extschema@.fn_api_get_panel_refyearset_combinations4groups(ARRAY[20,23]);

-- non-exISting group, no records
SELECT * FROM @extschema@.fn_api_get_panel_refyearset_combinations4groups(ARRAY[-20]);

 -- combination of non-exISting and exISting group, records of the exISting returned
SELECT * FROM @extschema@.fn_api_get_panel_refyearset_combinations4groups(ARRAY[-20, 23]);
*/
