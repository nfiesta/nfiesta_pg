-- Copyright 2017, 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_api_before_delete_panel_refyearset_group(integer)
--DROP FUNCTION @extschema@.fn_api_before_delete_panel_refyearset_group(integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_api_before_delete_panel_refyearset_group (
	_id INT, 
	out _t_aux_conf_exi boolean, out _t_total_estimate_conf_exi boolean
)
 LANGUAGE plpgsql
AS $function$
BEGIN

IF _id IS NULL THEN
	RAISE EXCEPTION 'Error 01: fn_api_before_delete_panel_refyearset_group: Function argument _id INT must not be NULL!';
END IF;

IF NOT EXISTS (SELECT * FROM @extschema@.c_panel_refyearset_group WHERE id = _id) THEN
	RAISE EXCEPTION 'Error 02: fn_api_before_delete_panel_refyearset_group: Panel with reference year set group id = % does not exist in table @extschema@.c_panel_refyearset_group and cannot be deleted.', $1;
END IF;

SELECT EXISTS (SELECT * FROM @extschema@.t_aux_conf WHERE panel_refyearset_group = _id) INTO _t_aux_conf_exi;
SELECT EXISTS (SELECT * FROM @extschema@.t_total_estimate_conf WHERE panel_refyearset_group = _id) INTO _t_total_estimate_conf_exi;

END;
$function$
;

COMMENT ON FUNCTION @extschema@.fn_api_before_delete_panel_refyearset_group(integer) IS 
'The function controls if there is a row in tables @extschema@.t_aux_conf and @extschema@.t_total_estimate_conf, '
'where panel_refyearset_group = _id passed as an argument. If at least one result column is TRUE, '
'the row with id = _id in table @extschema@.c_panel_refyearset_group cannot be deleted.';

/*
-- testing false inputs

-- passing NULL for _id
SELECT * FROM @extschema@.fn_api_before_delete_panel_refyearset_group(NULL);

-- non-existing group, no records
SELECT * FROM @extschema@.fn_api_before_delete_panel_refyearset_group(-20);

-- testing valid inputs
-- panel group 1
SELECT * FROM @extschema@.fn_api_before_delete_panel_refyearset_group(1);

-- panel group 26
SELECT * FROM @extschema@.fn_api_before_delete_panel_refyearset_group(26);
*/
