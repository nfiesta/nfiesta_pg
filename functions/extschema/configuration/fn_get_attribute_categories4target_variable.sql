--
-- Copyright 2017, 2023 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_get_attribute_categories4target_variable(integer, integer, integer, integer, integer, integer)
--DROP FUNCTION @extschema@.fn_get_attribute_categories4target_variable(integer, integer, integer, integer, integer, integer);
CREATE OR REPLACE FUNCTION @extschema@.fn_get_attribute_categories4target_variable(
	_numerator_target_variable integer,
	_numerator_area_domain integer DEFAULT NULL::int,
	_numerator_sub_population integer DEFAULT NULL::int,
	_denominator_target_variable integer DEFAULT NULL::int,
	_denominator_area_domain integer DEFAULT NULL::int,
	_denominator_sub_population integer DEFAULT NULL::int)
RETURNS TABLE (
	nominator_variable integer,
	denominator_variable integer, 
	label varchar,
	description text,
	label_en varchar, 
	description_en text
)
AS
$function$
BEGIN
IF _numerator_target_variable IS NULL THEN
	RAISE EXCEPTION 'Target variable for numerator is NULL!';
END IF;

IF NOT EXISTS (SELECT id FROM @extschema@.c_target_variable WHERE id = _numerator_target_variable)
THEN
	RAISE EXCEPTION 'Specified numerator target variable (%) does not exist in table c_target_variable!', _numerator_target_variable;
END IF;

IF _numerator_area_domain IS NOT NULL AND _numerator_area_domain != 0
THEN
	IF NOT EXISTS (SELECT * FROM @extschema@.c_area_domain WHERE id = _numerator_area_domain)
	THEN RAISE EXCEPTION 'Given numerator area domain (%) does not exist in table c_area_domain!', _numerator_area_domain;
	END IF;
END IF;

IF _numerator_sub_population IS NOT NULL AND _numerator_sub_population != 0
THEN
	IF NOT EXISTS (SELECT * FROM @extschema@.c_sub_population WHERE id = _numerator_sub_population)
	THEN RAISE EXCEPTION 'Given numerator sub population (%) does not exist in table c_sub_population!', _numerator_sub_population;
	END IF;
END IF;

IF _denominator_target_variable IS NOT NULL
THEN
	IF NOT EXISTS (SELECT * FROM @extschema@.c_target_variable WHERE id = _denominator_target_variable)
	THEN RAISE EXCEPTION 'Given denominator target variable (%) does not exist in table c_target_variable!', _denominator_targer_variable;
	END IF;
END IF;

IF _denominator_area_domain IS NOT NULL AND _denominator_area_domain != 0
THEN
	IF NOT EXISTS (SELECT * FROM @extschema@.c_area_domain WHERE id = _denominator_area_domain)
	THEN RAISE EXCEPTION 'Given denominator area domain (%) does not exist in table c_area_domain!', _denominator_area_domain;
	END IF;
END IF;

IF _denominator_sub_population IS NOT NULL AND _denominator_sub_population != 0
THEN
	IF NOT EXISTS (SELECT * FROM @extschema@.c_sub_population WHERE id = _denominator_sub_population)
	THEN RAISE EXCEPTION 'Given denominator sub population (%) does not exist in table c_sub_population!', _denominator_sub_population;
	END IF;
END IF;

RETURN QUERY
	WITH w_ad_num AS(
		SELECT t1.variable, t1.category AS adc, t1.label AS adc_label, t1.description AS adc_description, t1.label_en AS adc_label_en, t1.description_en AS adc_description_en
		FROM @extschema@.fn_get_area_sub_population_categories(_numerator_target_variable,100,_numerator_area_domain) AS t1
	), w_sp_num AS (
		SELECT t1.variable, t1.category AS spc, t1.label AS spc_label, t1.description AS spc_description, t1.label_en AS spc_label_en, t1.description_en AS spc_description_en
		FROM @extschema@.fn_get_area_sub_population_categories(_numerator_target_variable,200,_numerator_sub_population) AS t1
	), w_numerator_variables AS (
		SELECT 	t1.variable AS id,
			t1.adc, adc_label, adc_description, adc_label_en, adc_description_en, 
			t2.spc, spc_label, spc_description, spc_label_en, spc_description_en
		FROM w_ad_num AS t1
		INNER JOIN w_sp_num AS t2
		ON t1.variable = t2.variable
	), w_numerator_variables_agg AS (
		SELECT array_agg(id ORDER BY id) AS num_variables
		FROM w_numerator_variables
	), w_ad_denom AS(
		SELECT t1.variable, t1.category AS adc, t1.label AS adc_label, t1.description AS adc_description, t1.label_en AS adc_label_en, t1.description_en AS adc_description_en
		FROM @extschema@.fn_get_area_sub_population_categories(_denominator_target_variable,100,_denominator_area_domain) AS t1
	), w_sp_denom AS (
		SELECT t1.variable, t1.category AS spc, t1.label AS spc_label, t1.description AS spc_description, t1.label_en AS spc_label_en, t1.description_en AS spc_description_en
		FROM @extschema@.fn_get_area_sub_population_categories(_denominator_target_variable,200,_denominator_sub_population) AS t1
	), w_denominator_variables AS (
		SELECT	t1.variable AS id, 
			t1.adc, adc_label, adc_description, adc_label_en, adc_description_en, 
			t2.spc, spc_label, spc_description, spc_label_en, spc_description_en
		FROM w_ad_denom AS t1
		INNER JOIN w_sp_denom AS t2
		ON t1.variable = t2.variable
	), w_denominator_variables_agg AS (
		SELECT array_agg(id ORDER BY id) AS denom_variables
		FROM w_denominator_variables
	),
	w_num_denom AS (
		SELECT t3.variable_numerator, t3.variable_denominator
		FROM 	w_numerator_variables_agg AS t1,
			w_denominator_variables_agg AS t2,
			@extschema@.fn_get_num_denom_variables(t1.num_variables, t2.denom_variables) AS t3
	), w_final AS (
		SELECT 
			t1.variable_numerator,
			t1.variable_denominator,
			coalesce(nullif(concat_ws(' - ', t2.adc_label, t2.spc_label),''),'bez rozlišení')::varchar AS label_num, 
			CASE WHEN t1.variable_denominator IS NOT NULL THEN
				coalesce(nullif(concat_ws(' - ', t3.adc_label, t3.spc_label),''),'bez rozlišení')::varchar 
			ELSE NULL::varchar
			END AS label_denom,
			coalesce(nullif(concat_ws(' - ', t2.adc_description, t2.spc_description),''),'Bez rozlišení.')::text AS description_num, 
			CASE WHEN t1.variable_denominator IS NOT NULL THEN
				coalesce(nullif(concat_ws(' - ', t3.adc_description, t3.spc_description),''),'Bez rozlišení.')::text
			ELSE NULL::text
			END AS description_denom,
			coalesce(nullif(concat_ws(' - ', t2.adc_label_en, t2.spc_label_en),''),'altogether')::varchar AS label_en_num, 
			CASE WHEN t1.variable_denominator IS NOT NULL THEN
				coalesce(nullif(concat_ws(' - ', t3.adc_label_en, t3.spc_label_en),''),'altogether')::varchar 
			ELSE NULL::varchar
			END AS label_en_denom,
			coalesce(nullif(concat_ws(' - ', t2.adc_description_en, t2.spc_description_en),''),'Altogether.')::text AS description_en_num, 
			CASE WHEN t1.variable_denominator IS NOT NULL THEN
				coalesce(nullif(concat_ws(' - ', t3.adc_description_en, t3.spc_description_en),''),'Altogether.')::text
			ELSE NULL::text
			END AS description_en_denom
		FROM
			w_num_denom AS t1
		INNER JOIN 
			w_numerator_variables AS t2
		ON t1.variable_numerator = t2.id
		LEFT JOIN
			w_denominator_variables AS t3
		ON t1.variable_denominator = t3.id
	) 
	SELECT
		variable_numerator,
		variable_denominator,
		concat_ws(' / ', label_num, label_denom)::varchar AS label, 
		concat_ws(' / ', description_num, description_denom)::text AS description, 
		concat_ws(' / ', label_en_num, label_en_denom)::varchar AS label_en, 
		concat_ws(' / ', description_en_num, description_en_denom)::text AS description_en 
	FROM w_final
	ORDER BY variable_numerator, variable_denominator;

/*		@extschema@.t_variable AS t2
	ON t1.variable_numerator = t2.id
	LEFT JOIN @extschema@.c_area_domain_category AS t3
	ON t2.area_domain_category = t3.id
	LEFT JOIN @extschema@.c_sub_population_category AS t4
	ON t2.sub_population_category = t4.id
	INNER JOIN @extschema@.t_variable AS t5
	ON t1.variable_denominator = t5.id;	
	LEFT JOIN @extschema@.c_area_domain_category AS t6
	ON t5.area_domain_category = t6.id
	LEFT JOIN @extschema@.c_sub_population_category AS t7
	ON t5.sub_population_category = t7.id;
*/

END;
$function$
LANGUAGE plpgsql
PARALLEL SAFE;

COMMENT ON FUNCTION @extschema@.fn_get_attribute_categories4target_variable(integer,integer,integer,integer,integer,integer) IS 'Function returns table with attribute categories (area domain and sub population categories) for given target variable and area domain/sub population. It also solves the right combination of numerator and denominator categories.';
