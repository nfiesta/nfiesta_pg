--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_inverse(double precision[])

-- DROP FUNCTION @extschema@.fn_inverse(double precision[]);

CREATE OR REPLACE FUNCTION @extschema@.fn_inverse(input double precision[][])
  RETURNS double precision[][] AS
$BODY$
	import numpy as np
	from scipy import linalg

	A = np.array(input)
	try:
		# using LU decomposition I
		x = linalg.inv(A)

		# using LU decomposition II
		# B = np.identity(A.shape[0])
		# x = linalg.solve(A, B)
		
		# using QR decomposition
		# B = np.identity(A.shape[0])
		# Q, R = linalg.qr(A)
		# continue with Back substitution
		# see algorithm 11.2 and 11.3 (page 208) in https://web.stanford.edu/~boyd/vmls/vmls.pdf
		# maybe it would be appropriate to use https://docs.scipy.org/doc/scipy/reference/generated/scipy.linalg.solve_triangular.html
	except Exception:
		plpy.info("not able to compute inversion of matrix", input)
		return None
	return list(x.tolist())
$BODY$
  LANGUAGE plpython3u VOLATILE STRICT
  COST 100;

COMMENT ON FUNCTION @extschema@.fn_inverse(double precision[][]) IS 'Function for computing inverse of matrix using python3.';
--------------TEST
/*
select @extschema@.fn_inverse ('{{222477.383519094,7489538.83305858},{7489538.83305858,537848864.980823}}');
-- "{{8.46126431190308e-006,-1.17823001528562e-007},{-1.17823001528562e-007,3.49994221042523e-009}}"
*/
