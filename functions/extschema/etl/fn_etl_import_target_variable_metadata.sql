--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_import_target_variable_metadata(integer, json)

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_import_target_variable_metadata(integer, json) CASCADE;

create or replace function @extschema@.fn_etl_import_target_variable_metadata
(
	_id			integer,
	_metadata	json
)
returns text
as
$$
declare
		_json_key_input			text;	-- key of national language for adding to metadata
		_json_keys_target_db	text[]; -- list of languages in target metadata
		_res					text;
		_query_i_select			text;
		_query_i_join			text;
		_query_i_metadata		text;
		_query_i_object			text;
		_query_select			text;
		_query_join				text;
		_query_metadata			text;
		_query_object			text;
		_query_res				text;
		_json4update			json;
begin
	if _id is null
	then
		raise exception 'Error 01: fn_etl_import_target_variable_metadata: Input argument _id must not be NULL!';
	end if; 

	if _metadata is null
	then
		raise exception 'Error 02: fn_etl_import_target_variable_metadata: Input argument _metadata must not be NULL!';
	end if;

	with
	w1 as	(
			select json_object_keys(_metadata) as json_key
			)
	select w1.json_key from w1 where w1.json_key is distinct from 'en'
	into _json_key_input;

	if _json_key_input is null -- input JSON contains only national language EN
	then
		_res := concat('The metadata of target variable [c_target_variable.id = ',_id,'] was unchanged.');
	else
		with
		w1 as	(
				select json_object_keys(ctv.metadata) as json_key
				from @extschema@.c_target_variable as ctv
				where ctv.id = _id
				)
		,w2 as	(
				select distinct w1.json_key from w1
				)
		select array_agg(w2.json_key) from w2
		into _json_keys_target_db;
	
		if	(-- if json_key_input is not contained in metadata => adding metadatas for national language is needed
			select count(t.keys) = 0 from
				(select unnest(_json_keys_target_db) as keys) as t
			where t.keys = _json_key_input
			)
		then		
			for i in 1..array_length(_json_keys_target_db,1)
			loop
				_query_i_select := concat
				(
				',w',i,' as (select 1 as id4join, ctv',i,'.metadata->''',_json_keys_target_db[i],''' as metadata from @extschema@.c_target_variable as ctv',i,' where ctv',i,'.id = $2)'
				);

				_query_i_join := concat
				(
				' inner join w',i,' on w.id4join = w',i,'.id4join'
				);

				_query_i_metadata := concat
				(
				',w',i,'.metadata as metadata_',_json_keys_target_db[i]
				);

				_query_i_object := concat
				(
				',''',_json_keys_target_db[i],''',w_inner.metadata_',_json_keys_target_db[i]
				);
				
				if i = 1
				then
					_query_select := _query_i_select;
					_query_join := _query_i_join;
					_query_metadata := _query_i_metadata;
					_query_object := _query_i_object;
				else
					_query_select := _query_select || _query_i_select;
					_query_join := _query_join || _query_i_join;
					_query_metadata := _query_metadata || _query_i_metadata;
					_query_object := _query_object || _query_i_object;
				end if;
			end loop;

			_query_res := concat
			(
			'
			with
			w as	(
					select 1 as id4join, ($1->''',_json_key_input,''') as metadata
					)
			'
			,_query_select,
			'
			,w_inner as	(
						select
								w.id4join,w.metadata'
								,_query_metadata,
						' from w'
						,_query_join,
						')'
			'
			select
					--w_inner.id4join,
					json_build_object
						(
						''',_json_key_input,''',w_inner.metadata
						',_query_object,'
						) as metadata
			from
				w_inner;'
			);
		
			execute ''||_query_res||'' using _metadata, _id into _json4update;
		
			update @extschema@.c_target_variable as ctv set metadata = _json4update
			where ctv.id = _id;
		
			_res := concat('The metadata of target variable [c_target_variable.id = ',_id,'] was changed. Added metadata for national language "',_json_key_input,'".');
			
		else
			_res := concat('The metadata of target variable [c_target_variable.id = ',_id,'] was unchanged.');
		end if;
	end if;	

	return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_import_target_variable_metadata(integer, json) is
'Function inserts a record into table c_target_variable based on given parameters.';

grant execute on function @extschema@.fn_etl_import_target_variable_metadata(integer, json) to public;