--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- DROP FUNCTION @extschema@.fn_etl_delete_topic(integer) CASCADE;

CREATE OR REPLACE FUNCTION @extschema@.fn_etl_delete_topic(_id integer)
RETURNS void
AS
$$
BEGIN

	IF _id IS NULL THEN 
		RAISE EXCEPTION 'Error: 01: fn_etl_delete_topic: Parameter _id must not be null!';
	END IF;

	IF NOT EXISTS (SELECT * FROM @extschema@.c_topic AS t1 WHERE t1.id = _id)
		THEN RAISE EXCEPTION 'Error: 02: fn_etl_delete_topic: Given topic (%) does not exist in c_topic table.', _id;
	END IF;
	
	DELETE FROM @extschema@.c_topic 
	WHERE id  = _id;

END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION @extschema@.fn_etl_delete_topic(integer) IS
'Function delete record from c_topic table.';

GRANT EXECUTE ON FUNCTION @extschema@.fn_etl_delete_topic(integer) TO public;