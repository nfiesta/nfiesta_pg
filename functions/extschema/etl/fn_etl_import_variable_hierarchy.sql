--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_import_variable_hierarchy(json)

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_import_variable_hierarchy(json) CASCADE;

create or replace function @extschema@.fn_etl_import_variable_hierarchy
(
	_variables	json
)
returns text
as
$$
declare
		_max_id_tvh		integer;
		_res			text;
begin
		if _variables is null
		then
			raise exception 'Error 01: fn_etl_import_variable_hierarchy: Input argument _variables must not by NULL!';
		end if;
	
		_max_id_tvh := (select coalesce(max(id),0) from @extschema@.t_variable_hierarchy);
	
		with
		w1 as	(
				select json_array_elements(_variables) as s
			)
		,w2a as	(
				select
						(s->>'target_variable')::integer					as target_variable,
						(s->>'sub_population_category')::integer		as sub_population_category,
						(s->>'area_domain_category')::integer			as area_domain_category,
						(s->>'sub_population_category_superior')::integer	as sub_population_category_superior,
						(s->>'area_domain_category_superior')::integer		as area_domain_category_superior
				from w1
			)
		,w2 as	(
			-- ignore panel and reference_year_set information, because it will be delivered through t_available_datasets later in ETL process
			select * from w2a
			group by target_variable, sub_population_category, area_domain_category, sub_population_category_superior, area_domain_category_superior
			order by target_variable, sub_population_category, area_domain_category, sub_population_category_superior, area_domain_category_superior
			)
		,w3 as	(
				select
						t_variable.id,
						t_variable.target_variable,
						case when t_variable.sub_population_category is null then 0 else t_variable.sub_population_category end as sub_population_category,
						case when t_variable.area_domain_category is null then 0 else t_variable.area_domain_category end as area_domain_category
				from
						@extschema@.t_variable
				where
						t_variable.target_variable = (select distinct w2.target_variable from w2)
			)
		,w4 as	(
				select
						w2.*,
						w3.id as variable,
						w3_sup.id as variable_superior
				from
						w2
				inner join w3		on (
					(w2.sub_population_category = w3.sub_population_category) and
					(w2.area_domain_category = w3.area_domain_category)
				)
				inner join w3 as w3_sup	on (
					(w2.sub_population_category_superior = w3_sup.sub_population_category) and
					(w2.area_domain_category_superior = w3_sup.area_domain_category))
			)
		,w5 as	(
				select w4.variable, w4.variable_superior from w4 except
				select tvh.variable, tvh.variable_superior from @extschema@.t_variable_hierarchy as tvh
			)
		insert into @extschema@.t_variable_hierarchy(variable,variable_superior)
		select variable, variable_superior from w5 order by variable, variable_superior;
				
		_res := concat('The ',(select count(*) from @extschema@.t_variable_hierarchy where id > _max_id_tvh),' new rows were inserted into t_variable_hierarchy.');
	
		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_import_variable_hierarchy(json) IS
'The function solves ETL proces for t_variable_hierarchy table.';

grant execute on function @extschema@.fn_etl_import_variable_hierarchy(json) to public;
