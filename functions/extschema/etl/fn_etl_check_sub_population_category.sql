--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_check_sub_population_category(integer, integer, varchar)

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_check_sub_population_category(integer, integer, varchar) CASCADE;

create or replace function @extschema@.fn_etl_check_sub_population_category
(
	_id					integer,
	_sub_population		integer,
	_label_en			varchar
)
returns table
(
	id				integer,
	etl_id			integer,
	label			character varying,
	description		text,
	label_en		character varying,
	description_en	text	
)
as
$$
declare
		_res_etl_id				integer;
		_res_label				character varying;
		_res_description		text;
		_res_label_en			character varying;
		_res_description_en		text;
begin
		if _id is null
		then
			raise exception 'Error 01: fn_etl_check_sub_population_category: Input argument _id must not by NULL!';
		end if; 
	
		if _sub_population is null
		then
			raise exception 'Error 02: fn_etl_check_sub_population_category: Input argument _sub_population must not by NULL!';
		end if;
	
		if _label_en is null
		then
			raise exception 'Error 03: fn_etl_check_sub_population_category: Input argument _label_en must not by NULL!';
		end if;	
	
		select
				cspc.id,
				cspc.label,
				cspc.description,
				cspc.label_en,
				cspc.description_en
		from
				@extschema@.c_sub_population_category as cspc
		where
				cspc.sub_population = _sub_population
		and
				@extschema@.fn_etl_array_compare
					(
						string_to_array(replace(lower(cspc.label_en),' ',''),';'),
						string_to_array(replace(lower(_label_en),' ',''),';')					
					)
		into
				_res_etl_id,
				_res_label,
				_res_description,
				_res_label_en,
				_res_description_en;
	
		return query select _id, _res_etl_id, _res_label, _res_description, _res_label_en, _res_description_en;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_check_sub_population_category(integer, integer, varchar) is
'Function returns record ID from table c_sup_population_category based on given parameters.';

grant execute on function @extschema@.fn_etl_check_sub_population_category(integer, integer, varchar) to public;