--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_update_target_variable_metadata(integer, json, boolean, boolean, boolean, boolean, boolean, character varying)

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_update_target_variable_metadata(integer, json, boolean, boolean, boolean, boolean, boolean, character varying) CASCADE;

create or replace function @extschema@.fn_etl_update_target_variable_metadata
(
	_target_variable					integer, 											-- ETL_ID of target variable
	_metadata							json,												-- metadatas from source DB
	_metadata_diff_national_language	boolean,											-- identification if (TRUE) national language metadatas are different between source or target variable
	_metadata_diff_english_language		boolean,											-- identification if (TRUE) english language metadatas are different between source or target variable
	_missing_national_language			boolean,											-- identification if (TRUE) national language metadatas are missing for target variable
	_english_language_boolean			boolean,											-- english element for update
	_national_language_boolean			boolean,											-- national element for update
	_national_language					character varying default 'en'::character varying	-- identification of language mode for this function
)
returns text
as
$$
declare
		_variant				text;
		_national_languages		character varying[];
		_json_keys_target_db	text[];
		_json_objs_target_db	text[];
		_json_keys_source_db	text[];
		_json_objs_source_db	text[];
		_json_keys4update		text[];
		_json_objs4update		text[];
		_query_i_metadata		text;
		_query_i_join			text;
		_query_i_columns		text;
		_query_i_elements		text;
		_query_metadata			text;
		_query_join				text;
		_query_columns			text;
		_query_elements			text;
		_query_res				text;
		_json4update			json;
		_res					text;
begin
	if _target_variable is null
	then
		raise exception 'Error 01: fn_etl_update_target_variable_metadata: Input argument _target_variable must not be NULL!';
	end if; 

	if _metadata is null
	then
		raise exception 'Error 02: fn_etl_update_target_variable_metadata: Input argument _metadata must not be NULL!';
	end if;

	if _metadata_diff_english_language is null
	then
		raise exception 'Error 03: fn_etl_update_target_variable_metadata: Input argument _metadata_diff_english_language must not be NULL!';
	end if;

	if _english_language_boolean is null
	then
		raise exception 'Error 04: fn_etl_update_target_variable_metadata: Input argument _english_language_boolean must not be NULL!';
	end if;	

	if _national_language_boolean is null
	then
		raise exception 'Error 05: fn_etl_update_target_variable_metadata: Input argument _national_language_boolean must not be NULL!';
	end if;

	if _english_language_boolean = false and _national_language_boolean = false
	then
		raise exception 'Error 06: fn_etl_update_target_variable_metadata: One of the input argument _english_language_boolean or _national_language_boolean must be TRUE!';
	end if;
	-------------------------------------------------------
	if _national_language = 'en'
	then
			-- V0
			if (_english_language_boolean = true and _national_language_boolean = false)
			then
				if _metadata_diff_national_language is not null or _missing_national_language is not null
				then
					raise exception 'Error 07: fn_etl_update_target_variable_metadata: If national language is set to "EN" then update is allowed only for english metadata. The both input arguments "_metadata_diff_national_language" and "_missing_national_language" must be NULL!';
				end if;

				raise notice 'UPDATE is allowed';
				_variant := 'V0';
			else
				raise exception 'Error 08: fn_etl_update_target_variable_metadata: If national language is set to "EN" then update is allowed only for english metadata.';
			end if;
	else
		-- branch 'national language'

		-- V1
		if _english_language_boolean = true and _national_language_boolean = false
		then
			if _metadata_diff_english_language = true
			then
				raise notice 'UPDATE is allowed';
				_variant := 'V1';
			else
				raise exception 'Error 09: fn_etl_update_target_variable_metadata: If for language application mode is set national language and user wants to update only english metadata then the input argument _english_language_boolean must be true and the inpurt argument _national_language_boolean must be false!';
			end if;
		end if;

		-- V2
		if _english_language_boolean = true and _national_language_boolean = true
		then
			if 
				_metadata_diff_english_language = true and
				(_metadata_diff_national_language = true and _missing_national_language = false)
			then
				raise notice 'UPDATE is allowed';
				_variant := 'V2';
			else
				raise exception 'Error 10: fn_etl_update_target_variable_metadata: If for language application mode is set national language and user wants to update english and national metadata then the input argument _english_language_boolean must be true and the input argument _national_language_boolean must be true, and in case english updating the input argument _metadata_diff_english_language must be true, and in case national updating the input argument _metadata_diff_national_language must be true and the input argument _missing_national_language must be false!';
			end if;
		end if;

		-- V3
		if _english_language_boolean = false and _national_language_boolean = true
		then
			if	(_metadata_diff_national_language = true and _missing_national_language = false)
			then
				raise notice 'UPDATE is allowed';
				_variant := 'V3';
			else
				raise exception 'Error 11: fn_etl_update_target_variable_metadata: If for language application mode is set national language and user wants to update national metadata then the input argument _english_language_boolean must be false and the input argument _national_language_boolean must be true, and in case national updating the input argument _metadata_diff_national_language must be true and the input argument _missing_national_language must be false!';				
			end if;
		end if;

	end if;
	-------------------------------------------------------


	-------------------------------------------------------
	-------------------------------------------------------
	/*
	case
		when _national_language is     null and _english_language = true then  _national_languages := array['en'::character varying];
		when _national_language is not null and _english_language = true then  _national_languages := array['en'::character varying] || array[_national_language];
		when _national_language is not null and _english_language = false then _national_languages := array[_national_language];
		else
			raise exception 'Error 04: fn_etl_update_target_variable_metadata: If input argument _national_language is null then input argument _english_language must be TRUE!';
	end case;
	*/

	case
		when _variant in ('V0','V1') then  _national_languages := array['en'::character varying];
		when _variant in ('V2') then  _national_languages := array['en'::character varying] || array[_national_language];
		when _variant in ('V3') then _national_languages := array[_national_language];
		else
			raise exception 'Error 12: fn_etl_update_target_variable_metadata: Not defince internal argument _variant';
	end case;	

	/*
	-- distinct of values in _national_languages, becouse national language can be the same as english language
	select array_agg(t2.res) from (select distinct t1.res from (select unnest(_national_languages) as res) as t1) as t2
	into _national_languages;
	*/

	if (array_length(_national_languages,1) > 2 or array_length(_national_languages,1) < 1)
	then
		raise exception 'Error 13: fn_etl_update_target_variable_metadata: Number of elements in internal argument _national_languages must be one or two!';
	end if;

	-- check that elements in internal argument _national_languages are contained in input argument _metadata and in target DB
	for i in 1..array_length(_national_languages,1)
	loop
		-- input argument _metadata
		if	(
			with
			w1 as	(
					select json_object_keys(_metadata) as json_key
					)
			select count(w1.json_key) is distinct from 1 from w1
			where w1.json_key = _national_languages[i]
			)
		then
			raise exception 'Error 14: fn_etl_update_target_variable_metadata: The language = "%" for update in input argument _national_languages is not present in argument _metadata!',_national_languages[i];
		end if;

		-- metadata in target DB
		if	(
			with
			w1 as	(
					select json_object_keys(ctv.metadata) as json_key
					from @extschema@.c_target_variable as ctv
					where ctv.id = _target_variable
					)
			select count(w1.json_key) is distinct from 1 from w1
			where w1.json_key = _national_languages[i]
			)
		then
			raise exception 'Error 15: fn_etl_update_target_variable_metadata: The language = % for update in input argument _national_languages is not present in metadatas for given target_variable in target DB!',_national_languages[i];
		end if;

	end loop;
	-------------------------------------------------------
	-------------------------------------------------------

	-- get ALL json keys in target DB for given target variable that will not be changed
	with
	w1 as	(
			select json_object_keys(ctv.metadata) as json_key
			from @extschema@.c_target_variable as ctv
			where ctv.id = _target_variable
			)
	,w2 as	(
			select distinct w1.json_key, 'tdb' as json_obj from w1
			)
	select
			array_agg(w2.json_key),
			array_agg(w2.json_obj)
	from
			w2 where w2.json_key not in (select unnest(_national_languages))	-- NOT IN !!!
	into
			_json_keys_target_db,	-- list of json keys for no changes
			_json_objs_target_db;
		
	-- json keys source
	with
	w1 as	(
			select
					unnest(_national_languages) as json_key,
					'sdb' as json_obj
			)
	select
			array_agg(w1.json_key),
			array_agg(w1.json_obj)
	from
			w1
	into
			_json_keys_source_db,
			_json_objs_source_db;
		
	if _json_keys_target_db is null
	then
		_json_keys4update := _json_keys_source_db;
		_json_objs4update := _json_objs_source_db;
	else
		_json_keys4update := _json_keys_target_db || _json_keys_source_db;
		_json_objs4update := _json_objs_target_db || _json_objs_source_db;
	end if;

	for i in 1..array_length(_json_keys4update,1)
	loop
		-----------------------------------------
		if _json_objs4update[i] = 'tdb'
		then
			if i = 1
			then
				_query_i_metadata := concat
				(
				'with w',i,' as (select 1 as id4join, ctv',i,'.metadata->''',_json_keys4update[i],''' as metadata from @extschema@.c_target_variable as ctv',i,' where ctv',i,'.id = $1)'
				);
			else
				_query_i_metadata := concat
				(
				',w',i,' as (select 1 as id4join, ctv',i,'.metadata->''',_json_keys4update[i],''' as metadata from @extschema@.c_target_variable as ctv',i,' where ctv',i,'.id = $1)'
				);
			end if;
		else
			if i = 1
			then
				_query_i_metadata := concat
				(
				'with w',i,' as (select 1 as id4join, $2->''',_json_keys4update[i],''' as metadata)'
				);
			else
				_query_i_metadata := concat
				(
				',w',i,' as (select 1 as id4join, $2->''',_json_keys4update[i],''' as metadata)'
				);
			end if;
		end if;
		-----------------------------------------
		if i = 1
		then
			_query_i_join := concat
			(
			' from w',i
			);
		else
			_query_i_join := concat
			(
			' inner join w',i,' on w',i-1,'.id4join = w',i,'.id4join'
			);
		end if;
		-----------------------------------------
		if i = 1
		then
			_query_i_columns := concat
			(
			'w1.id4join, w1.metadata as metadata_1'
			);
		else
			_query_i_columns := concat
			(
			', w',i,'.metadata as metadata_',i,''
			);
		end if;
		-----------------------------------------
		if i = 1
		then
			_query_i_elements := concat
			(
			'''',_json_keys4update[i],''',metadata_',i,''
			);
		else
			_query_i_elements := concat
			(
			',''',_json_keys4update[i],''',metadata_',i,''
			);
		end if;
		-----------------------------------------
		if i = 1
		then
			_query_metadata := _query_i_metadata;
			_query_join := _query_i_join;
			_query_columns := _query_i_columns;
			_query_elements := _query_i_elements;
		else
			_query_metadata := _query_metadata || _query_i_metadata;
			_query_join := _query_join || _query_i_join;
			_query_columns := _query_columns || _query_i_columns;
			_query_elements := _query_elements || _query_i_elements;
		end if;
	end loop;

	_query_res := concat
	(
		_query_metadata,
		',w_inner as (select ',_query_columns,_query_join,')
		select json_build_object(',_query_elements,') from w_inner;
		'
	);

	execute ''||_query_res||'' using _target_variable, _metadata into _json4update;

	update @extschema@.c_target_variable as ctv set metadata = _json4update
	where ctv.id = _target_variable;
		
	_res := concat('The metadata of target variable [c_target_variable.id = ',_target_variable,'] for language elements = ',_national_languages,' was changed.');

	return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_update_target_variable_metadata(integer, json, boolean, boolean, boolean, boolean, boolean, character varying) is
'Function update metadatas of target variable in table c_target_variable for given language elements.';

grant execute on function @extschema@.fn_etl_update_target_variable_metadata(integer, json, boolean, boolean, boolean, boolean, boolean, character varying) to public;