--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_try_delete_topic(integer) CASCADE;

CREATE OR REPLACE FUNCTION @extschema@.fn_etl_try_delete_topic(_id integer)
RETURNS boolean
AS
$$
BEGIN
	IF NOT EXISTS (SELECT t1.* FROM @extschema@.c_topic as t1 where t1.id = _id)
	THEN
		RAISE EXCEPTION 'Error 01: fn_etl_try_delete_topic: Given topic (%) does not exist in c_topic table!',_id;
	END IF;

	return not exists (
	select t2.id from @extschema@.cm_tvariable2topic as t2 where t2.topic = _id
	);

END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION @extschema@.fn_etl_try_delete_topic(integer) IS
'Function provides test if it is possible to delete record from c_topic table.';

GRANT EXECUTE ON FUNCTION @extschema@.fn_etl_try_delete_topic(integer) TO public;