--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_update_sub_population_label(integer, character varying, varchar)

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_update_sub_population_label(integer, character varying, varchar) CASCADE;

create or replace function @extschema@.fn_etl_update_sub_population_label
(
	_sub_population		integer,
	_national_language	character varying(2),
	_label				varchar
)
returns text
as
$$
declare
		_column							text;
		_atomic_sub_population_category	integer;
		_res							text;
begin
		if _sub_population is null
		then
			raise exception 'Error 01: fn_etl_update_sub_population_label: Input argument _sub_population must not be NULL!';
		end if;
	
		if _national_language is null
		then
			raise exception 'Error 02: fn_etl_update_sub_population_label: Input argument _national_language must not be NULL!';
		end if;
	
		if _label is null
		then
			raise exception 'Error 03: fn_etl_update_sub_population_label: Input argument _label must not be NULL!';
		end if;
		-----------------------------------------
		if	(
			select count(csp.*) is distinct from 1
			from @extschema@.c_sub_population as csp
			where csp.id = _sub_population
			)
		then
			raise exception 'Error 04: fn_etl_update_sub_population_label: Input argument _sub_population (ID) = % is not present in c_sub_population table!',_sub_population;
		end if;
		-----------------------------------------
		if	(
			select csp.atomic = false
			from @extschema@.c_sub_population as csp
			where csp.id = _sub_population
			)
		then
			raise exception 'Error 05: fn_etl_update_sub_population_label: Input argument _sub_population (ID) = % is not ATOMIC sub population!',_sub_population;
		end if;			
		-----------------------------------------
		-- UPDATE label of ATOMIC sub population
		if _national_language = 'en'::varchar
		then
			update @extschema@.c_sub_population set label_en = _label where id = _sub_population;

			_column := 'label_en';
		else
			update @extschema@.c_sub_population set label = _label where id = _sub_population;

			_column := 'label';
		end if;
		-----------------------------------------
		-- UPDATE label of NON-ATOMIC sub population
		select cspc.id from @extschema@.c_sub_population_category as cspc
		where cspc.sub_population = _sub_population order by cspc.id limit 1
		into _atomic_sub_population_category;

		if _atomic_sub_population_category is null
		then
			raise exception 'Error 06: fn_etl_update_sub_population_label: For input argument _sub_population (ID) = % not exists any category in c_sub_population_category table!',_sub_population;
		end if;
	
		execute
		'
		with
		w1 as	(
				select * from @extschema@.cm_sub_population_category where sub_population_category in
				(select sub_population_category from @extschema@.cm_sub_population_category where atomic_category = $1)
				)
		,w2 as	(
				select
						w1.id,
						w1.sub_population_category, 			-- id_target_db of non-atomic category 
						w1.atomic_category,						-- id_target_db of atomic category
						t1.sub_population, 						-- id_target_db of atomic type
						t1.'|| _column ||' as label_category,	-- label atomic category
						t2.'|| _column ||' as label_type		-- label atomic type
				from
						w1
						inner join @extschema@.c_sub_population_category as t1 on w1.atomic_category = t1.id
						inner join @extschema@.c_sub_population as t2 on t1.sub_population = t2.id
				)
		---------
		,w3 as	(
				select
						id,
						sub_population,
						string_to_array('|| _column ||','';'') as label_category 
				from
						@extschema@.c_sub_population_category
				where
						id in (select w2.sub_population_category from w2)
				)
		,w4 as	(
				select
						w3.*,
						(select array_agg(t.res order by t.res) from (select generate_series(1,array_length(w3.label_category,1)) as res) as t) as id_order
				from
						w3
				)
		,w5 as	(
				select
						w4.id,
						w4.sub_population,
						unnest(w4.label_category) as label_category,
						unnest(w4.id_order) as id_order
				from
						w4
				)
		--------
		,w6 as	(
				select w2.*, w5.sub_population as sub_population_non_atomic, w5.id_order from w2 inner join w5
				on w2.sub_population_category = w5.id and w2.label_category = w5.label_category
				)
		,w7 as	(
				select distinct w6.sub_population_non_atomic, w6.label_type, w6.id_order from w6
				)
		,w8 as	(
				select
						w7.sub_population_non_atomic,
						array_agg(w7.label_type order by w7.id_order) as label_type,
						array_agg(w7.id_order order by w7.id_order) as id_order
				from
						w7 group by w7.sub_population_non_atomic
				)
		,w9 as	(
				select
						w8.sub_population_non_atomic,
						array_to_string(w8.label_type,'';'') as label4update
				from
						w8
				)
		update @extschema@.c_sub_population set '|| _column ||' = w9.label4update
		from w9 where c_sub_population.id = w9.sub_population_non_atomic;
		'
		using _atomic_sub_population_category;
		-----------------------------------------
		_res := concat('The label of sub population [c_sub_population.id = ',_sub_population,'] for language element = "',_national_language,'" was changed. The update was done for ATOMIC and NON-ATOMIC sub populations.');
		-----------------------------------------
		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_update_sub_population_label(integer, character varying, varchar) is
'The tunction updates a sub population label in c_sub_population table for given sub population.';

grant execute on function @extschema@.fn_etl_update_sub_population_label(integer, character varying, varchar) to public;