--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_check_variables(json)

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_check_variables(json) CASCADE;

create or replace function @extschema@.fn_etl_check_variables
(
	_variables	json
)
returns json
as
$$
declare
		_country									varchar;
		_target_variable							integer;
		_count_comb_panels_and_reference_year_sets	integer;
		_refyearset2panel_mapping					integer[];
		_res										json;

		_check_variables							integer;
begin
		if _variables is null
		then
			raise exception 'Error 01: fn_etl_check_variables: Input argument _variables must not by NULL!';
		end if;

		_country := (_variables->>'country')::varchar;
		_target_variable := (_variables->>'target_variable')::integer;

		-- check country --
		if _country is null
		then
			raise exception 'Error 02: fn_etl_check_variables: Internal argument _country must not by NULL!';
		end if;

		if	(select count(id) is distinct from 1 from sdesign.c_country where label = _country)
		then
			raise exception 'Error 03: fn_etl_check_variables: Input JSON element "country = %" is not present in table sdesign.c_country!',_country;
		end if;

		------------
		-- raise notice '_country: %',_country;
		------------

		-- check target_variable
		if _target_variable is null
		then
			raise exception 'Error 04: fn_etl_check_variables: Internal argument _target_variable must not by NULL!';
		end if;

		if	(select count(id) is distinct from 1 from @extschema@.c_target_variable where id = _target_variable)
		then
			raise exception 'Error 05: fn_etl_check_variables: Input JSON element "target_variable = %" is not present in table c_target_variable!',_target_variable;
		end if;

		------------
		-- raise notice '_target_variable: %',_target_variable;
		------------			

		-- check 1
		with
		w1 as	(
				select json_array_elements(_variables->'variables') as s
				)
		,w2 as	(
				select
						(s->>'panel')::varchar				as panel,
						(s->>'reference_year_set')::varchar as reference_year_set
				from w1
				)
		,w3 as	(
				select distinct w2.panel, w2.reference_year_set from w2
				)
		select count(*) from w3
		into _count_comb_panels_and_reference_year_sets;

		if _count_comb_panels_and_reference_year_sets is null
		then
			raise exception 'Error 06: fn_etl_check_variables: Internal element variables in input JSON not contains any combination of panel and reference year set!';
		end if;

		------------
		-- raise notice '_count_comb_panels_and_reference_year_sets: %',_count_comb_panels_and_reference_year_sets;
		------------	

		-- check 2
		with
		w1 as	(
				select json_array_elements(_variables->'variables') as s
				)
		,w2 as	(
				select
						(s->>'panel')::varchar				as panel,
						(s->>'reference_year_set')::varchar	as reference_year_set
				from w1
				)
		,w3 as	(
				select distinct w2.panel, w2.reference_year_set from w2
				)
		,w4 as	(
				select
					t1.id as panel_id,
					t1.panel,
					t3.id as reference_year_set_id,
					t3.reference_year_set,
					t2.id as refyearset2panel_mapping
				from
					(
					select id, panel from sdesign.t_panel where stratum in
					(select id from sdesign.t_stratum where strata_set in
					(select id from sdesign.t_strata_set where country = 
					(select id from sdesign.c_country where label = _country)))
					) as t1
				inner join sdesign.cm_refyearset2panel_mapping as t2 on t1.id = t2.panel
				inner join sdesign.t_reference_year_set as t3 on t2.reference_year_set = t3.id
				)
		,w5 as	(
				select w4.refyearset2panel_mapping from w3 inner join w4
				on w3.panel = w4.panel and w3.reference_year_set = w4.reference_year_set
				)
		select array_agg(w5.refyearset2panel_mapping order by w5.refyearset2panel_mapping)
		from w5
		into _refyearset2panel_mapping;

		------------
		-- raise notice '_refyearset2panel_mapping: %',_refyearset2panel_mapping;
		------------			

		if _refyearset2panel_mapping is null
		then
			raise exception 'Error 07: fn_etl_check_variables: Internal argument _refyearset2panel_mapping must not by NULL!';
		end if;	

		if _count_comb_panels_and_reference_year_sets != array_length(_refyearset2panel_mapping,1)
		then
			raise exception 'Error 08: fn_etl_check_variables: Some input combination of panel and reference year set in input JSON is not present in sampling design!';
		end if;		

		-- check 3
		with
		w1 as	(
				select json_array_elements(_variables->'variables') as s
				)
		,w2 as	(
				select
						(s->>'check_variables')::boolean as check_variables
				from w1
				)
		select count(w2.*) from w2 where w2.check_variables = false
		into _check_variables;

		if _check_variables > 0
		then
			raise exception 'Error 09: fn_etl_check_variables: Element check_variables in input argument _variables contains value FALSE!';
		end if;

		-------------------------------------------------------------
		-------------------------------------------------------------
		with
		w1 as	(
				select
						a.id,
						a.panel as panel__tdb,
						a.reference_year_set as reference_year_set__tdb,
						a.variable as variable__tdb
				from
							(select * from @extschema@.t_available_datasets) as a
				inner join	(		
							select * from sdesign.cm_refyearset2panel_mapping
							where id in (select unnest(_refyearset2panel_mapping))
							) as b
									on a.panel = b.panel and a.reference_year_set = b.reference_year_set
				)
		,w2 as	(
				select distinct variable__tdb from w1 where variable__tdb in
					(select id from @extschema@.t_variable where target_variable = _target_variable)
				)
		,w3 as	(
				select
						t1.panel__tdb,
						t1.reference_year_set__tdb,
						t1.variable__tdb,
						tv.target_variable,
						tv.sub_population_category,
						tv.area_domain_category,
						coalesce(t2.label,'null'::varchar) as spc_label,
						coalesce(t2.label_en,'null'::varchar) as spc_label_en,
						coalesce(t3.label,'null'::varchar) as adc_label,
						coalesce(t3.label_en,'null'::varchar) as adc_label_en,
						coalesce(t4.label,'null'::varchar) as spt_label,
						coalesce(t4.label_en,'null'::varchar) as spt_label_en,
						coalesce(t5.label,'null'::varchar) as adt_label,						
						coalesce(t5.label_en,'null'::varchar) as adt_label_en,					
						t2.id as spc_id,
						t3.id as adc_id,
						t4.id as spt_id,
						t5.id as adt_id						
				from
						(select w1.* from w1 where w1.variable__tdb in (select w2.variable__tdb from w2)) as t1				
					inner join @extschema@.t_variable as tv on t1.variable__tdb = tv.id
					left join @extschema@.c_sub_population_category as t2 on tv.sub_population_category = t2.id
					left join @extschema@.c_area_domain_category as t3 on tv.area_domain_category = t3.id
					left join @extschema@.c_sub_population as t4 on t2.sub_population = t4.id
					left join @extschema@.c_area_domain as t5 on t3.area_domain = t5.id
				)
		,w4 as	(
				select distinct panel__tdb, reference_year_set__tdb, spt_label, spc_label, adt_label, adc_label, spt_label_en, spc_label_en, adt_label_en, adc_label_en, spc_id, adc_id, spt_id, adt_id from w3
				)		
		,w5 as	(-- list of attribute variables for target variable [target DB]
				select
						row_number() over() as new_id,
						panel__tdb,
						reference_year_set__tdb,
						spt_label,
						spc_label,
						adt_label,
						adc_label,
						spt_label_en,
						spc_label_en,
						adt_label_en,
						adc_label_en,
						case when spt_id is null then 0 else spt_id end as spt_id,
						case when spc_id is null then 0 else spc_id end as spc_id,
						case when adt_id is null then 0 else adt_id end as adt_id,
						case when adc_id is null then 0 else adc_id end as adc_id
				from w4
				)
		,w5c as	( -- adding label of panel and reference year set
				select
						w5.*,
						tp.panel as panel__tdb__label,
						trys.reference_year_set as reference_year_set__tdb__label
				from
						w5
						inner join sdesign.t_panel as tp on w5.panel__tdb = tp.id
						inner join sdesign.t_reference_year_set as trys on w5.reference_year_set__tdb = trys.id
				)
		------------------------------------------------------------------
		,w7a as	(-- list of attribute variables [source DB]
				select json_array_elements(_variables->'variables') as s
				)
		,w7 as	(
				select
						(s->>'panel')::varchar											as panel__sdb,
						(s->>'reference_year_set')::varchar								as reference_year_set__sdb,
						(s->>'sub_population_etl_id')::integer							as vsp_id,
						(s->>'sub_population_category_etl_id')::integer					as vspc_id,
						(s->>'area_domain_etl_id')::integer								as vad_id,
						(s->>'area_domain_category_etl_id')::integer					as vadc_id
				from
						w7a
				)
		------------------------------------------------------------------
		,w8 as	(-- list of attribute variables between target db and source db that were paired
				select
						w5c.*,
						w7.*
				from
						w5c inner join w7
				on
						(
						w5c.panel__tdb__label = w7.panel__sdb and
						w5c.reference_year_set__tdb__label = w7.reference_year_set__sdb and
						w5c.spt_id = w7.vsp_id	and
						w5c.spc_id = w7.vspc_id and
						w5c.adt_id = w7.vad_id	and
						w5c.adc_id = w7.vadc_id
						)
				)
		------------------------------------------------------------------
		,w9 as	(-- list of attribute variables that are missing
				select w5c.* from w5c where w5c.new_id not in (select w8.new_id from w8)
				)
		,w10 as	(
				select
						w9.panel__tdb__label as panel,
						w9.reference_year_set__tdb__label as reference_year_set,
						w9.spt_label as sub_population,
						w9.spc_label as sub_population_category,
						w9.adt_label as area_domain,
						w9.adc_label as area_domain_category,
						w9.spt_label_en as sub_population_en,
						w9.spc_label_en as sub_population_category_en,
						w9.adt_label_en as area_domain_en,
						w9.adc_label_en as area_domain_category_en
				from
						w9
				order
						by	w9.reference_year_set__tdb__label,
							w9.panel__tdb__label,
							w9.spt_id,
							w9.spc_id,
							w9.adt_id,
							w9.adc_id
				)
		select json_agg(json_build_object
							(
							'panel',w10.panel,
							'reference_year_set',w10.reference_year_set,
							'sub_population',w10.sub_population,
							'sub_population_category',w10.sub_population_category,
							'area_domain',w10.area_domain,
							'area_domain_category',w10.area_domain_category,
							'sub_population_en',w10.sub_population_en,
							'sub_population_category_en',w10.sub_population_category_en,
							'area_domain_en',w10.area_domain_en,
							'area_domain_category_en',w10.area_domain_category_en							
							)
						)
		from w10
		into _res;

		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_check_variables(json) is
'Function checks that input variables are contained in target database.';

grant execute on function @extschema@.fn_etl_check_variables(json) to public;