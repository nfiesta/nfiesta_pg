--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- DROP FUNCTION @extschema@.fn_etl_delete_target_variable2topic(integer, integer[]) CASCADE;

CREATE OR REPLACE FUNCTION @extschema@.fn_etl_delete_target_variable2topic
(
	_target_variable integer,
	_topics integer[]
)
RETURNS void
AS
$$
BEGIN

	IF _target_variable IS NULL
	THEN 
		RAISE EXCEPTION 'Error: 01: fn_etl_delete_target_variable2topic: Parameter _target_variable must not be null!';
	END IF;

	IF _topics IS NULL
	THEN 
		RAISE EXCEPTION 'Error: 02: fn_etl_delete_target_variable2topic: Parameter _topics must not be null!';
	END IF;	

	for i in 1..array_length(_topics,1)
	loop
		IF NOT EXISTS (SELECT * FROM @extschema@.cm_tvariable2topic AS t1 WHERE t1.target_variable = _target_variable and t1.topic = _topics[i])
			THEN RAISE EXCEPTION 'Error: 03: fn_etl_delete_target_variable2topic: Given mapping between target variable (%) and topic (%) does not exist in cm_tvariable2topic table.', _target_variable, _topics[i];
		END IF;
	end loop;
	
	DELETE FROM @extschema@.cm_tvariable2topic 
	WHERE target_variable = _target_variable
	and topic in (select unnest(_topics));

END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION @extschema@.fn_etl_delete_target_variable2topic(integer, integer[]) IS
'Function delete record from cm_tvariable2topic table.';

GRANT EXECUTE ON FUNCTION @extschema@.fn_etl_delete_target_variable2topic(integer, integer[]) TO public;