--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_save_topic(varchar, text, varchar, text)

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_save_topic(varchar, text, varchar, text) CASCADE;

create or replace function @extschema@.fn_etl_save_topic
(
	_label				varchar(200),
	_description		text,
	_label_en			varchar(200),
	_description_en		text
)
returns integer
as
$$
declare
		_id integer;
begin
	if _label is null or _description is null or _label_en is null or _description_en is null
	then
		raise exception 'Error 01: fn_etl_save_topic: Mandatory input of label, description, label_en or description_en is null (%, %, %, %).', _label, _description, _label_en, _description_en;
	end if; 

	insert into @extschema@.c_topic(label, description, label_en, description_en)
	select _label, _description, _label_en, _description_en
	returning id
	into _id;

	return _id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_save_topic(varchar, text, varchar, text) is
'Function inserts a record into c_topic table.';

grant execute on function @extschema@.fn_etl_save_topic(varchar, text, varchar, text) to public;