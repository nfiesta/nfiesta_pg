--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_update_area_domain_description(integer, character varying, text)

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_update_area_domain_description(integer, character varying, text) CASCADE;

create or replace function @extschema@.fn_etl_update_area_domain_description
(
	_area_domain		integer,
	_national_language	character varying(2),
	_description		text
)
returns text
as
$$
declare
		_column							text;
		_atomic_area_domain_category	integer;
		_res							text;
begin
		if _area_domain is null
		then
			raise exception 'Error 01: fn_etl_update_area_domain_description: Input argument _area_domain must not be NULL!';
		end if;
	
		if _national_language is null
		then
			raise exception 'Error 02: fn_etl_update_area_domain_description: Input argument _national_language must not be NULL!';
		end if;
	
		if _description is null
		then
			raise exception 'Error 03: fn_etl_update_area_domain_description: Input argument _description must not be NULL!';
		end if;
		-----------------------------------------
		if	(
			select count(cad.*) is distinct from 1
			from @extschema@.c_area_domain as cad
			where cad.id = _area_domain
			)
		then
			raise exception 'Error 04: fn_etl_update_area_domain_description: Input argument _area_domain (ID) = % is not present in c_area_domain table!',_area_domain;
		end if;
		-----------------------------------------
		if	(
			select cad.atomic = false
			from @extschema@.c_area_domain as cad
			where cad.id = _area_domain
			)
		then
			raise exception 'Error 05: fn_etl_update_area_domain_description: Input argument _area_domain (ID) = % is not ATOMIC area domain!',_area_domain;
		end if;		
		-----------------------------------------
		-- UPDATE description of ATOMIC area domain
		if _national_language = 'en'::varchar
		then
			update @extschema@.c_area_domain set description_en = _description where id = _area_domain;

			_column := 'description_en';
		else
			update @extschema@.c_area_domain set description = _description where id = _area_domain;

			_column := 'description';
		end if;
		-----------------------------------------
		-- UPDATE description of NON-ATOMIC area domain
		select cadc.id from @extschema@.c_area_domain_category as cadc
		where cadc.area_domain = _area_domain order by cadc.id limit 1
		into _atomic_area_domain_category;

		if _atomic_area_domain_category is null
		then
			raise exception 'Error 06: fn_etl_update_area_domain_description: For input argument _area_domain (ID) = % not exists any category in c_area_domain_category table!',_area_domain;
		end if;
	
		execute
		'
		with
		w1 as	(
				select * from @extschema@.cm_area_domain_category where area_domain_category in
				(select area_domain_category from @extschema@.cm_area_domain_category where atomic_category = $1)
				)
		,w2 as	(
				select
						w1.id,
						w1.area_domain_category, 					-- id_target_db of non-atomic category 
						w1.atomic_category,							-- id_target_db of atomic category
						t1.area_domain, 							-- id_target_db of atomic type
						t1.'|| _column ||' as description_category,	-- description atomic category
						t2.'|| _column ||' as description_type		-- description atomic type
				from
						w1
						inner join @extschema@.c_area_domain_category as t1 on w1.atomic_category = t1.id
						inner join @extschema@.c_area_domain as t2 on t1.area_domain = t2.id
				)
		---------
		,w3 as	(
				select
						id,
						area_domain,
						string_to_array('|| _column ||','';'') as description_category 
				from
						@extschema@.c_area_domain_category
				where
						id in (select w2.area_domain_category from w2)
				)
		,w4 as	(
				select
						w3.*,
						(select array_agg(t.res order by t.res) from (select generate_series(1,array_length(w3.description_category,1)) as res) as t) as id_order
				from
						w3
				)
		,w5 as	(
				select
						w4.id,
						w4.area_domain,
						unnest(w4.description_category) as description_category,
						unnest(w4.id_order) as id_order
				from
						w4
				)
		--------
		,w6 as	(
				select w2.*, w5.area_domain as area_domain_non_atomic, w5.id_order from w2 inner join w5
				on w2.area_domain_category = w5.id and w2.description_category = w5.description_category
				)
		,w7 as	(
				select distinct w6.area_domain_non_atomic, w6.description_type, w6.id_order from w6
				)
		,w8 as	(
				select
						w7.area_domain_non_atomic,
						array_agg(w7.description_type order by w7.id_order) as description_type,
						array_agg(w7.id_order order by w7.id_order) as id_order
				from
						w7 group by w7.area_domain_non_atomic
				)
		,w9 as	(
				select
						w8.area_domain_non_atomic,
						array_to_string(w8.description_type,'';'') as description4update
				from
						w8
				)
		update @extschema@.c_area_domain set '|| _column ||' = w9.description4update
		from w9 where c_area_domain.id = w9.area_domain_non_atomic;
		'
		using _atomic_area_domain_category;
		-----------------------------------------
		_res := concat('The description of area domain [c_area_domain.id = ',_area_domain,'] for language element = "',_national_language,'" was changed. The update was done for ATOMIC and NON-ATOMIC area domains.');
		-----------------------------------------
		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_update_area_domain_description(integer, character varying, text) is
'The tunction updates an area domain description in c_area_domain table for given area domain.';

grant execute on function @extschema@.fn_etl_update_area_domain_description(integer, character varying, text) to public;