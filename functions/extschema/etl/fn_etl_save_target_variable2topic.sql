--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_save_target_variable2topic(integer, integer)

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_save_target_variable2topic(integer, integer) CASCADE;

create or replace function @extschema@.fn_etl_save_target_variable2topic
(
	_target_variable	integer,
	_topic				integer
)
returns integer
as
$$
declare
		_id	integer;
begin
		if _target_variable is null
		then
			raise exception 'Error 01: fn_etl_save_target_variable2topic: Input argument _target_variable must not be null!';
		end if;

		if _topic is null
		then
			raise exception 'Error 02: fn_etl_save_target_variable2topic: Input argument _topic must not be null!';
		end if;
	
		insert into @extschema@.cm_tvariable2topic(target_variable,topic)
		select _target_variable, _topic
		returning id
		into _id;
	
		return _id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_save_target_variable2topic(integer, integer) is
'Function saves new mapping between target variable and topic.';

grant execute on function @extschema@.fn_etl_save_target_variable2topic(integer, integer) to public;