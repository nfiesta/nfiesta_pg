--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_check_area_domain_categories4area_domains(json)

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_check_area_domain_categories4area_domains(json) CASCADE;

create or replace function @extschema@.fn_etl_check_area_domain_categories4area_domains
(
	_metadatas json
)
returns integer[]
as
$$
declare
	_res integer[];
begin
		if _metadatas is null
		then
			raise exception 'Error 01: fn_etl_check_area_domain_categories4area_domains: Input argument _metadatas must not be NULL!';
		end if;
		-----------------------------------------
		with
		w1 as 	(
				select _metadatas as metadatas
				)
		,w2 as	(
				select json_array_elements(w1.metadatas) as metadatas from w1
				)
		,w3 as	(
				select
						(w2.metadatas->>'area_domain')::integer as area_domain_target,
						(w2.metadatas->>'area_domain_category')::integer as area_domain_category_target,
						(w2.metadatas->>'label')::varchar as label_source,
						(w2.metadatas->>'description')::text as description_source,
						(w2.metadatas->>'label_en')::varchar as label_en_source,
						(w2.metadatas->>'description_en')::text as description_en_source
				from
						w2
				)
		,w4 as	(
				select
						w3.*,
						t.label as label_target,
						t.description as description_target,
						t.label_en as label_en_target,
						t.description_en as description_en_target
				from
						w3
						inner join	(
									select cadc.* from @extschema@.c_area_domain_category as cadc
									where cadc.area_domain in (select distinct w3.area_domain_target from w3)
									) as t
						on w3.area_domain_target = t.area_domain
						and w3.area_domain_category_target = t.id
				)
		,w5 as	(
				select
						w4.*,
						string_to_array(w4.label_source,';') as array_label_source,
						string_to_array(w4.description_source,';') as array_description_source,
						string_to_array(w4.label_en_source,';') as array_label_en_source,
						string_to_array(w4.description_en_source,';') as array_description_en_source,
						string_to_array(w4.label_target,';') as array_label_target,
						string_to_array(w4.description_target,';') as array_description_target,
						string_to_array(w4.label_en_target,';') as array_label_en_target,
						string_to_array(w4.description_en_target,';') as array_description_en_target
				from
						w4
				)
		,w6 as	(
				select
						w5.*,
						@extschema@.fn_etl_array_compare(w5.array_label_source,w5.array_label_target) as check_label,
						@extschema@.fn_etl_array_compare(w5.array_description_source,w5.array_description_target) as check_description,
						@extschema@.fn_etl_array_compare(w5.array_label_en_source,w5.array_label_en_target) as check_label_en,
						@extschema@.fn_etl_array_compare(w5.array_description_en_source,w5.array_description_en_target) as check_description_en
				from
						w5
				)
		select array_agg(t.area_domain_target order by t.area_domain_target)
		from	(
				select distinct w6.area_domain_target from w6
				where w6.check_label = false or w6.check_description = false
				or w6.check_label_en = false or w6.check_description_en = false
				) as t
		into _res;

		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_check_area_domain_categories4area_domains(json) is
'The tunction returns array of IDs of area domains if their source label or description categories are different from target.';

grant execute on function @extschema@.fn_etl_check_area_domain_categories4area_domains(json) to public;