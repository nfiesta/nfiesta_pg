--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_get_sub_population_categories4roller(integer, integer, integer[])

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_get_sub_population_categories4roller(integer, integer, integer[]) CASCADE;

create or replace function @extschema@.fn_etl_get_sub_population_categories4roller
(
	_id_ordering			integer,
	_sub_population			integer,
	_etl_id					integer[] default null::integer[]
)
returns table
(
	id						integer,
	etl_id					integer,
	label					character varying,
	description				text,
	label_en				character varying,
	description_en			text
)
as
$$
declare
begin	
		if _id_ordering is null
		then
			raise exception 'Error 01: fn_etl_get_sub_population_categories4roller: Input argument _id_ordering must not be NULL!';
		end if;
	
		if _sub_population is null
		then
			raise exception 'Error 02: fn_etl_get_sub_population_categories4roller: Input argument _sub_population must not be NULL!';
		end if;
	
		return query execute
		'
		with
		w1 as	(
				select cspc.* from @extschema@.c_sub_population_category as cspc
				where cspc.sub_population = $1
				and cspc.id not in (select unnest($2))
				)
		select
				$3 as id,
				w1.id as etl_id,
				w1.label,
				w1.description,
				w1.label_en,
				w1.description_en
		from
				w1 order by w1.id
		'
		using _sub_population, _etl_id, _id_ordering;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_get_sub_population_categories4roller(integer, integer, integer[]) is
'The function returns records of sub population categories for given sub population into roller.';

grant execute on function @extschema@.fn_etl_get_sub_population_categories4roller(integer, integer, integer[]) to public;