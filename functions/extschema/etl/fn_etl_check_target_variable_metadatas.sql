--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_check_target_variable_metadatas(json, character varying, boolean)

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_check_target_variable_metadatas(json, character varying, boolean) CASCADE;

create or replace function @extschema@.fn_etl_check_target_variable_metadatas
(
	_metadatas			json,
	_national_language	character varying(2) default 'en'::character varying,
	_metadata_diff		boolean default null::boolean
)
returns table
(
	target_variable_source				integer,
	target_variable_target				integer,
	metadata_source						json,
	metadata_target						json,
	metadata_diff						boolean,
	metadata_diff_national_language		boolean,
	metadata_diff_english_language		boolean,
	missing_metadata_national_language	boolean
)
as
$$
declare
		_cond	text;
begin
		if _metadatas is null
		then
			raise exception 'Error 01: fn_etl_check_target_variable_metadatas: Input argument _metadatas must not by NULL!';
		end if;
		-------------------------------
		if _metadata_diff is null
		then
			_cond := 'TRUE';
		else
			if _metadata_diff = true
			then 
				_cond := 'metadata_diff = true';
			else
				_cond := 'metadata_diff = false';
			end if;
		end if;
		-------------------------------
		if _national_language = 'en'
		then
			return query execute
			'
			with
			w1 as	(
					select json_array_elements($1) as s
					)
			,w2 as	(
					select
							(s->>''target_variable'')::integer		as target_variable_source,
							(s->>''etl_id'')::integer				as target_variable_target,
							(s->>''metadata'')::json				as metadata_source
					from w1
					)
			,w3 as	(
					select
							ctv.id,
							ctv.metadata as metadata_target
					from
							@extschema@.c_target_variable as ctv
					where
							ctv.id in (select w2.target_variable_target from w2)
					)
			,w4 as	(
					select
							w2.target_variable_source,
							w2.target_variable_target,
							w2.metadata_source,
							w3.metadata_target,
							null::boolean as metadata_diff_national_language,
							case
								when	(
										(w2.metadata_source->''en'')::jsonb @> (w3.metadata_target->''en'')::jsonb
										and
										(w3.metadata_target->''en'')::jsonb @> (w2.metadata_source->''en'')::jsonb
										)
								then
										false
								else
										true
							end
								as metadata_diff_english_language,
							null::boolean as missing_metadata_national_language
					from
							w2 inner join w3 on w2.target_variable_target = w3.id
					)
			,w5 as	(
					select
							w4.target_variable_source,
							w4.target_variable_target,
							w4.metadata_source,
							json_build_object(''en'',w4.metadata_target->''en'') as metadata_target,
							w4.metadata_diff_english_language as metadata_diff,
							w4.metadata_diff_national_language,
							w4.metadata_diff_english_language,
							w4.missing_metadata_national_language
					from
							w4
					)
			select
					w5.target_variable_source,
					w5.target_variable_target,
					w5.metadata_source,
					w5.metadata_target,
					w5.metadata_diff,
					w5.metadata_diff_national_language,
					w5.metadata_diff_english_language,
					w5.missing_metadata_national_language
			from
					w5
			where
					'|| _cond ||'
			order
					by w5.target_variable_source;
			'
			using _metadatas;
		else -- check national and english metadatas
			return query execute
			'
			with
			w1 as	(
					select json_array_elements($2) as s
					)
			,w2 as	(
					select
							(s->>''target_variable'')::integer		as target_variable_source,
							(s->>''etl_id'')::integer				as target_variable_target,
							(s->>''metadata'')::json				as metadata_source
					from w1
					)
			,w3 as	(
					select
							ctv.id,
							ctv.metadata as metadata_target
					from
							@extschema@.c_target_variable as ctv
					where
							ctv.id in (select w2.target_variable_target from w2)
					)
			,w4 as	(
					select
							w2.target_variable_source,
							w2.target_variable_target,
							w2.metadata_source,
							w3.metadata_target
					from
							w2 inner join w3 on w2.target_variable_target = w3.id
					)
			,w5 as	(
					select
							t.target_variable_target,
							array_agg(t.json_keys_target) as json_keys_target
					from
							(
							select
									w4.target_variable_target,
									json_object_keys(w4.metadata_target) as json_keys_target
							from w4
							) as t
					group
							by t.target_variable_target
					)
			,w6 as	(
					select
							w4.*,
							w5.json_keys_target,
							case when $1 = any(w5.json_keys_target) then false else true end as missing_metadata_national_language
					from
							w4 inner join w5 on w4.target_variable_target = w5.target_variable_target
					)
			,w7 as	(
					select
							w6.target_variable_source,
							w6.target_variable_target,
							w6.metadata_source,
							w6.metadata_target,
							w6.json_keys_target,
							w6.missing_metadata_national_language,
							-------------------------------------
							case
								when w6.missing_metadata_national_language = true
								then
									true
								else
									case
										when	(
												(w6.metadata_source->$1)::jsonb @> (w6.metadata_target->$1)::jsonb
												and
												(w6.metadata_target->$1)::jsonb @> (w6.metadata_source->$1)::jsonb
												)
										then
												false
										else
												true
									end
							end
								as metadata_diff_national_language,
							-------------------------------------
							case
								when	(
										(w6.metadata_source->''en'')::jsonb @> (w6.metadata_target->''en'')::jsonb
										and
										(w6.metadata_target->''en'')::jsonb @> (w6.metadata_source->''en'')::jsonb
										)
								then
										false
								else
										true
							end
								as metadata_diff_english_language
							-------------------------------------
					from
							w6
					)
			,w8 as	(
					select
							w7.*,
							case
								when w7.metadata_diff_national_language = true or w7.metadata_diff_english_language = true
								then true
								else false
							end as metadata_diff
					from w7
					)
			,w9 as	(
					select
							w8.target_variable_source,
							w8.target_variable_target,
							w8.metadata_source,
							w8.metadata_target as metadata_target_original,
							w8.metadata_diff,
							w8.metadata_diff_national_language,
							w8.metadata_diff_english_language,
							w8.missing_metadata_national_language,
							--------------------------------------
							case
								when w8.missing_metadata_national_language = true
								then
									json_build_object(''en'',w8.metadata_target->''en'')
								else
									json_build_object	(
														$1,w8.metadata_target->$1,
														''en'',w8.metadata_target->''en''
														)
							end
								as metadata_target
					from
							w8
					)
			select
					w9.target_variable_source,
					w9.target_variable_target,
					w9.metadata_source,
					w9.metadata_target,
					w9.metadata_diff,
					w9.metadata_diff_national_language,
					w9.metadata_diff_english_language,
					w9.missing_metadata_national_language
			from
					w9
			where
					'|| _cond ||'
			order
					by w9.target_variable_source;
			'
			using _national_language, _metadatas;
		end if;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_check_target_variable_metadatas(json, character varying, boolean) is
'Function returns records of target variables for given input arguments. If input argument _metadata_diff = true
then function returns records of target variables that their metadatas are not different. If input argument
_metadata_diff = false then function returns records of target variables that their metadatas are different.
If input argument _metadata_diff is null (_metadata is true or false) then function returns both cases.';

grant execute on function @extschema@.fn_etl_check_target_variable_metadatas(json, character varying, boolean) to public;