--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_get_topics()

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_get_topics() CASCADE;

create or replace function @extschema@.fn_etl_get_topics()
returns table
(
	id				integer,
	label			character varying(200),
	description		text,
	label_en		character varying(200),
	description_en	text
)
as
$$
begin
	return query
	select t.id, t.label, t.description, t.label_en, t.description_en
	from @extschema@.c_topic as t;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_get_topics() is
'Function returns records from c_topic table.';

grant execute on function @extschema@.fn_etl_get_topics() to public;