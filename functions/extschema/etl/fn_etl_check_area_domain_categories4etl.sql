--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_check_area_domain_categories4etl(integer, json)

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_check_area_domain_categories4etl(integer, json) CASCADE;

create or replace function @extschema@.fn_etl_check_area_domain_categories4etl
(
	_area_domain			integer, -- id from target c_area_domain
	_area_domain_category	json
)
returns table
(
	id						integer,
	area_domain_category	integer[],
	etl_id					integer,
	label					character varying,
	description				text,
	label_en				character varying,
	description_en			text,
	check_label				boolean
)
as
$$
declare
		_label_en_type_source				varchar[];
		_check_label_en_type				integer;
		_check_count_categories				integer;
		_check_count_elements				integer;
		_check_count_categories_distinct	integer;
		_check_count_label_en_distinct		integer;
		_raise_notice_text					text;
		_check_info							integer;
		_check_count_pairing_label_en		integer;
begin	
		if _area_domain is null
		then
			raise exception 'Error 01: fn_etl_check_area_domain_categories4etl: Input argument _area_domain must not be NULL!';
		end if;
	
		if _area_domain_category is null
		then
			raise exception 'Error 02: fn_etl_check_area_domain_categories4etl: Input argument _area_domain_category must not be NULL!';
		end if;
		---------------------------------------------------
		with
		w1 as	(
				select _area_domain_category as res
				)
		,w2 as	(
				select json_array_elements(w1.res) as res from w1
				)
		,w3 as	(
				select (res->>'label_en_type')::varchar as label_en_type_source	from w2
				)
		,w4 as	(
				select replace(lower(w3.label_en_type_source),' ','') as label_en_type_source from w3
				)
		select array_agg(t.label_en_type_source) from (select distinct w4.label_en_type_source from w4) as t
		into _label_en_type_source;

		if array_length(_label_en_type_source,1) is distinct from 1
		then
			raise exception 'Error 03: fn_etl_check_area_domain_categories4etl: English labels of area domain in input argument _area_domain_category are not equal!';
		end if;
		--------------------------------------------------
		-- check if input area domain is equal with target
		with
		w1 as	(
				select
						cad.id,
						string_to_array(replace(lower(cad.label_en),' ',''),';') as label_en_type_target4compare
				from
						@extschema@.c_area_domain as cad
				where
						cad.id = _area_domain
				)
		select count(w1.*) from w1 where @extschema@.fn_etl_array_compare(string_to_array(lower(_label_en_type_source[1]),';'),w1.label_en_type_target4compare) = true
		into _check_label_en_type;
		-- there will be value 0 or 1
		-- 0 => source area domain label_en is different from target area domain label_en
		-----------------------------------------------------------------------
		-----------------------------------------------------------------------
		-- source area domain label_en is equal with target area domain label_en => next step are checks of categories
		---------------------------------------------------
		-- count of categories for target area domain
		select count(cadc.*) from @extschema@.c_area_domain_category as cadc
		where cadc.area_domain = _area_domain
		into _check_count_categories;
		---------------------------------------------------
		-- count of categories for source area domain
		with
		w1 as	(
				select _area_domain_category as res
				)
		,w2 as	(
				select json_array_elements(w1.res) from w1
				)
		select count(w2.*) from w2
		into _check_count_elements;
		---------------------------------------------------
		if _check_count_categories is distinct from _check_count_elements
		then
			--raise exception 'Error 03: fn_etl_check_area_domain_categories4etl: Count elements in input argument _area_domain_category is not equal count of categories of input area domain [ID = %] in target c_area_domain_category table!',_area_domain;
			if _check_label_en_type = 1
			then
				--_raise_notice_text := 'Selected area domain to pair is equal with source but count of categories is not equal!';
				_raise_notice_text := 'Different count of categories, the selected area domain cannot be paired!';
			else
				--_raise_notice_text := 'Selected area domain to pair is not equal with source and count of categories is not equal!';
				_raise_notice_text := 'Different count of categories, the selected area domain cannot be paired!';
			end if;

			_check_info := 1;
		else
			_check_info := 0;
		end if;
		---------------------------------------------------
		select count(t.*) from
		(select distinct cadc.label_en from @extschema@.c_area_domain_category as cadc
		where cadc.area_domain = _area_domain) as t
		into _check_count_categories_distinct;
		---------------------------------------------------
		if _check_count_categories is distinct from _check_count_categories_distinct
		then
			--raise exception 'Error 04: fn_etl_check_area_domain_categories4etl: English label of categories of input area domain [ID = %] in target c_area_domain_category table is not unique!',_area_domain;
			if _check_label_en_type = 1
			then			
				--_raise_notice_text := 'Selected area domain to pair is equal with source but english lables of target are not unique!';
				_raise_notice_text := 'Duplicate categories in the target database, the selected area domain cannot be paired!';
			else
				--_raise_notice_text := 'Selected area domain to pair is not equal with source and english lables of target are not unique!';
				_raise_notice_text := 'Duplicate categories in the target database, the selected area domain cannot be paired!';
			end if;

			_check_info := _check_info + 1;
		else
			_check_info := _check_info + 0;
		end if;
		---------------------------------------------------
		with
		w1 as	(
				select _area_domain_category as res
				)
		,w2 as	(
				select json_array_elements(w1.res) as res from w1
				)
		,w3 as	(
				select (res->>'label_en')::varchar as label_en from	w2
				)
		select count(t.*) from
		(select distinct w3.label_en from w3) as t
		into _check_count_label_en_distinct;
		---------------------------------------------------
		if _check_count_elements is distinct from _check_count_label_en_distinct
		then
			--raise exception 'Error 05: fn_etl_check_area_domain_categories4etl: English label of categories of input argument _area_domain_category = % is not unique!',_area_domain_category;
			if _check_label_en_type = 1
			then
				--_raise_notice_text := 'Selected area domain to pair is equal with source but english lables of source are not unique!';
				_raise_notice_text := 'Duplicate categories in the source database, the selected area domain cannot be paired!';
			else
				--_raise_notice_text := 'Selected area domain to pair is not equal with source and english lables of source are not unique!';
				_raise_notice_text := 'Duplicate categories in the source database, the selected area domain cannot be paired!';
			end if;

			_check_info := _check_info + 1;
		else
			_check_info := _check_info + 0;
		end if;	
		--------------------------------------------------
		-----------------------------------------------------------------------
		-----------------------------------------------------------------------
		if _check_info = 0
		then
			-- OK
			-- check if pairing over international language will have already some record
			with
			w1a as	(
					select _area_domain_category as res
					)
			,w2a as	(
					select json_array_elements(w1a.res) as res from w1a
					)
			,w3a as	(
					select
							(res->>'id')::integer as id_ordering_source,
							replace(replace((res->>'area_domain_category')::varchar,'[',''),']','') as area_domain_category_source,
							(res->>'label_en')::varchar as label_en_source
					from
							w2a
					)
			,w4a as	(
					select
							w3a.id_ordering_source,
							(string_to_array(w3a.area_domain_category_source,','))::integer[] as area_domain_category_source,
							w3a.label_en_source,
							string_to_array(replace(lower(w3a.label_en_source),' ',''),';') as label_en_source4compare
					from
							w3a
					)
			---------------------
			,w1b as	(
					select
							cadc.id as etl_id,
							cadc.area_domain,
							cadc.label as label_target,
							cadc.description as description_target,
							cadc.label_en as label_en_target,
							cadc.description_en as description_en_target,
							string_to_array(replace(lower(cadc.label_en),' ',''),';') as label_en_target4compare
					from
							@extschema@.c_area_domain_category as cadc
					where
							cadc.area_domain = _area_domain
					)
			---------------------
			,w1 as 	(
					select w1b.*, w4a.* from w1b cross join w4a
					)
			,w2 as	(
					select w1.*, true as check_label_en from w1
					where @extschema@.fn_etl_array_compare(w1.label_en_source4compare,w1.label_en_target4compare)
					)
			,w3 as	(
					select
							w2.id_ordering_source,
							w2.area_domain_category_source,
							w2.etl_id,
							w2.label_target as label,
							w2.description_target as description,
							w2.label_en_target as label_en,
							w2.description_en_target as description_en,
							w2.check_label_en
					from
							w2
					)
			,w4 as	(
					select
							w4a.id_ordering_source as id,
							w4a.area_domain_category_source as area_domain_category,
							w3.etl_id,
							w3.label,
							w3.description,
							w3.label_en,
							w3.description_en,
							w3.check_label_en
					from
							w4a left join w3
					on
							w4a.id_ordering_source = w3.id_ordering_source
					and		w4a.area_domain_category_source = w3.area_domain_category_source
					)
			select count(w4.*) from w4 where w4.check_label_en = true
			into _check_count_pairing_label_en;

			if _check_count_pairing_label_en > 0
			then
				return query
				with
				w1a as	(
						select _area_domain_category as res
						)
				,w2a as	(
						select json_array_elements(w1a.res) as res from w1a
						)
				,w3a as	(
						select
								(res->>'id')::integer as id_ordering_source,
								replace(replace((res->>'area_domain_category')::varchar,'[',''),']','') as area_domain_category_source,
								(res->>'label_en')::varchar as label_en_source
						from
								w2a
						)
				,w4a as	(
						select
								w3a.id_ordering_source,
								(string_to_array(w3a.area_domain_category_source,','))::integer[] as area_domain_category_source,
								w3a.label_en_source,
								string_to_array(replace(lower(w3a.label_en_source),' ',''),';') as label_en_source4compare
						from
								w3a
						)
				---------------------
				,w1b as	(
						select
								cadc.id as etl_id,
								cadc.area_domain,
								cadc.label as label_target,
								cadc.description as description_target,
								cadc.label_en as label_en_target,
								cadc.description_en as description_en_target,
								string_to_array(replace(lower(cadc.label_en),' ',''),';') as label_en_target4compare
						from
								@extschema@.c_area_domain_category as cadc
						where
								cadc.area_domain = _area_domain
						)
				---------------------
				,w1 as 	(
						select w1b.*, w4a.* from w1b cross join w4a
						)
				,w2 as	(
						select w1.*, true as check_label from w1
						where @extschema@.fn_etl_array_compare(w1.label_en_source4compare,w1.label_en_target4compare)
						)
				,w3 as	(
						select
								w2.id_ordering_source,
								w2.area_domain_category_source,
								w2.etl_id,
								w2.label_target as label,
								w2.description_target as description,
								w2.label_en_target as label_en,
								w2.description_en_target as description_en,
								w2.check_label
						from
								w2
						)
				,w4 as	(
						select
								w4a.id_ordering_source as id,
								w4a.area_domain_category_source as area_domain_category,
								w3.etl_id,
								w3.label,
								w3.description,
								w3.label_en,
								w3.description_en,
								w3.check_label
						from
								w4a left join w3
						on
								w4a.id_ordering_source = w3.id_ordering_source
						and		w4a.area_domain_category_source = w3.area_domain_category_source
						)
				select w4.* from w4 order by w4.id;
			else
				-- do pairing over national language
				return query
				with
				w1a as	(
						select _area_domain_category as res
						)
				,w2a as	(
						select json_array_elements(w1a.res) as res from w1a
						)
				,w3a as	(
						select
								(res->>'id')::integer as id_ordering_source,
								replace(replace((res->>'area_domain_category')::varchar,'[',''),']','') as area_domain_category_source,
								(res->>'label')::varchar as label_source
						from
								w2a
						)
				,w4a as	(
						select
								w3a.id_ordering_source,
								(string_to_array(w3a.area_domain_category_source,','))::integer[] as area_domain_category_source,
								w3a.label_source,
								string_to_array(replace(lower(w3a.label_source),' ',''),';') as label_source4compare
						from
								w3a
						)
				---------------------
				,w1b as	(
						select
								cadc.id as etl_id,
								cadc.area_domain,
								cadc.label as label_target,
								cadc.description as description_target,
								cadc.label_en as label_en_target,
								cadc.description_en as description_en_target,
								string_to_array(replace(lower(cadc.label),' ',''),';') as label_target4compare
						from
								@extschema@.c_area_domain_category as cadc
						where
								cadc.area_domain = _area_domain
						)
				---------------------
				,w1 as 	(
						select w1b.*, w4a.* from w1b cross join w4a
						)
				,w2 as	(
						select w1.*, true as check_label from w1
						where @extschema@.fn_etl_array_compare(w1.label_source4compare,w1.label_target4compare)
						)
				,w3 as	(
						select
								w2.id_ordering_source,
								w2.area_domain_category_source,
								w2.etl_id,
								w2.label_target as label,
								w2.description_target as description,
								w2.label_en_target as label_en,
								w2.description_en_target as description_en,
								w2.check_label
						from
								w2
						)
				,w4 as	(
						select
								w4a.id_ordering_source as id,
								w4a.area_domain_category_source as area_domain_category,
								w3.etl_id,
								w3.label,
								w3.description,
								w3.label_en,
								w3.description_en,
								w3.check_label
						from
								w4a left join w3
						on
								w4a.id_ordering_source = w3.id_ordering_source
						and		w4a.area_domain_category_source = w3.area_domain_category_source
						)
				select w4.* from w4 order by w4.id;				
			end if;
		else
			raise notice '%',_raise_notice_text;

			return query
			select
					null::integer as id,
					null::integer[] as area_domain_category,
					null::integer as etl_id,
					cadc.label,
					cadc.description,
					cadc.label_en,
					cadc.description_en,
					null::boolean as check_label
			from
					@extschema@.c_area_domain_category as cadc
			where
					cadc.area_domain = _area_domain
			order
					by cadc.id;			
		end if;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_check_area_domain_categories4etl(integer, json) is
'The function returns records of area domain categories for given input area domain with inforamtion if input label of area domain category was paired in target DB or not.';

grant execute on function @extschema@.fn_etl_check_area_domain_categories4etl(integer, json) to public;