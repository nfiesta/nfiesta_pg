--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_insert_target_variable_metadata(integer, json, boolean, character varying)

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_insert_target_variable_metadata(integer, json, boolean, character varying) CASCADE;

create or replace function @extschema@.fn_etl_insert_target_variable_metadata
(
	_target_variable					integer, 				-- ETL_ID of target variable
	_metadata							json,					-- metadatas from source DB
	_missing_national_language			boolean,				-- identification if (TRUE) national language metadatas are missing for target variable
	_national_language					character varying		-- identification of language mode for this function
)
returns text
as
$$
declare
		_json_keys_target_db	text[];
		_query_i_select			text;
		_query_i_join			text;
		_query_i_metadata		text;
		_query_i_object			text;
		_query_select			text;
		_query_join				text;
		_query_metadata			text;
		_query_object			text;
		_query_res				text;
		_json4update			json;
		_res					text;
begin
	if _target_variable is null
	then
		raise exception 'Error 01: fn_etl_insert_target_variable_metadata: Input argument _target_variable must not be NULL!';
	end if; 

	if _metadata is null
	then
		raise exception 'Error 02: fn_etl_insert_target_variable_metadata: Input argument _metadata must not be NULL!';
	end if;

	if _missing_national_language is null
	then
		raise exception 'Error 03: fn_etl_insert_target_variable_metadata: Input argument _missing_national_language must not be NULL!';
	end if;
	-------------------------------------------------------
	if _national_language = 'en'
	then
		raise exception 'Error 04: fn_etl_insert_target_variable_metadata: If national language is set to "EN" then insert is not allowed. Insert is allowed only for national language metadata.';
	else
		-- branch 'national language'

		if _missing_national_language = true
		then
			raise notice 'INSERT is allowed';
		else
			raise exception 'Error 05: fn_etl_insert_target_variable_metadata: If for language application mode is set national language different from "EN" then the input argument _missing_national_language must be true!';
		end if;
	end if;
	-------------------------------------------------------
	if length(_national_language) > 2 or length(_national_language) < 2
	then
		raise exception 'Error 06: fn_etl_insert_target_variable_metadata: The length of input argument _national_language must by two.';
	end if;
	-------------------------------------------------------
	-- check that language in internal argument _national_language is contained in input argument _metadata
	if	(
		with
		w1 as	(
				select json_object_keys(_metadata) as json_key
				)
		select count(w1.json_key) is distinct from 1 from w1
		where w1.json_key = _national_language
		)
	then
		raise exception 'Error 07: fn_etl_insert_target_variable_metadata: The language = "%" for insert is not present in input argument _metadata!',_national_language;
	end if;
	-------------------------------------------------------
	-- check that language in internal argument _national_language is NOT contained in TARGET metadata for given target variable
	if	(
		with
		w1 as	(
				select json_object_keys(ctv.metadata) as json_key
				from @extschema@.c_target_variable as ctv
				where ctv.id = _target_variable
				)
		select count(w1.json_key) is distinct from 0 from w1
		where w1.json_key = _national_language 
		)
	then
		raise exception 'Error 08: fn_etl_insert_target_variable_metadata: The language = % for insert is present in target metadata for given target variable!',_national_language;
	end if;
	-------------------------------------------------------
	-------------------------------------------------------
	with
	w1 as	(
			select json_object_keys(ctv.metadata) as json_key
			from @extschema@.c_target_variable as ctv
			where ctv.id = _target_variable
			)
	,w2 as	(
			select distinct w1.json_key from w1
			)
	select array_agg(w2.json_key) from w2
	into _json_keys_target_db; -- list of language elements without changes
	-------------------------------------------------------
	for i in 1..array_length(_json_keys_target_db,1)
	loop
		_query_i_select := concat
		(
		',w',i,' as (select 1 as id4join, ctv',i,'.metadata->''',_json_keys_target_db[i],''' as metadata from @extschema@.c_target_variable as ctv',i,' where ctv',i,'.id = $2)'
		);

		_query_i_join := concat
		(
		' inner join w',i,' on w.id4join = w',i,'.id4join'
		);

		_query_i_metadata := concat
		(
		',w',i,'.metadata as metadata_',_json_keys_target_db[i]
		);

		_query_i_object := concat
		(
		',''',_json_keys_target_db[i],''',w_inner.metadata_',_json_keys_target_db[i]
		);
		
		if i = 1
		then
			_query_select := _query_i_select;
			_query_join := _query_i_join;
			_query_metadata := _query_i_metadata;
			_query_object := _query_i_object;
		else
			_query_select := _query_select || _query_i_select;
			_query_join := _query_join || _query_i_join;
			_query_metadata := _query_metadata || _query_i_metadata;
			_query_object := _query_object || _query_i_object;
		end if;
	end loop;
	-------------------------------------------------------
	_query_res := concat
	(
	'
	with
	w as	(
			select 1 as id4join, ($1->''',_national_language,''') as metadata
			)
	'
	,_query_select,
	'
	,w_inner as	(
				select
						w.id4join,w.metadata'
						,_query_metadata,
				' from w'
				,_query_join,
				')'
	'
	select
			--w_inner.id4join,
			json_build_object
				(
				''',_national_language,''',w_inner.metadata
				',_query_object,'
				) as metadata
	from
		w_inner;'
	);
	-------------------------------------------------------
	execute ''||_query_res||'' using _metadata, _target_variable into _json4update;
		
	update @extschema@.c_target_variable as ctv set metadata = _json4update
	where ctv.id = _target_variable;
		
	_res := concat('The metadata of target variable [c_target_variable.id = ',_target_variable,'] was changed. Added metadata for national language "',_national_language,'".');
	-------------------------------------------------------
	return _res;

end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_insert_target_variable_metadata(integer, json, boolean, character varying) is
'Function insert missing national language metadatas of target variable in table c_target_variable.';

grant execute on function @extschema@.fn_etl_insert_target_variable_metadata(integer, json, boolean, character varying) to public;