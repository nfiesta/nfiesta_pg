--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_import_target_variable(json)

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_import_target_variable(json) CASCADE;

create or replace function @extschema@.fn_etl_import_target_variable
(
	_metadata json
)
returns integer
as
$$
declare
		_id integer;
begin
	if _metadata is null
	then
		raise exception 'Error 01: fn_etl_import_target_variable: Input argument _metadata must not by NULL!';
	end if; 

	insert into @extschema@.c_target_variable(metadata)
	select _metadata
	returning id
	into _id;

	return _id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_import_target_variable(json) is
'Function inserts a record into table c_target_variable based on given parameters.';

grant execute on function @extschema@.fn_etl_import_target_variable(json) to public;