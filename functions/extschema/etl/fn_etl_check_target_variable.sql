--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_check_target_variable(json, integer[])

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_check_target_variable(json, integer[]) CASCADE;

create or replace function @extschema@.fn_etl_check_target_variable
(
	_metadata json,
	_etl_id integer[] default null::integer[]
)
returns integer
as
$$
declare
		_id integer;
begin
		if _metadata is null
		then
			raise exception 'Error 01: fn_etl_check_target_variable: Input argument _metadata must not by NULL!';
		end if; 
	
		select ctv.id from @extschema@.c_target_variable as ctv
		where ctv.id not in (select unnest(_etl_id))
		and	(
				(ctv.metadata->'en')::jsonb @> (_metadata->'en')::jsonb
				and
				(_metadata->'en')::jsonb @> (ctv.metadata->'en')::jsonb
				)
		into _id;

		return _id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_check_target_variable(json, integer[]) is
'The function returns record ID from table c_target_variable if source metadata of target variable were founded (are the same) in c_target_variable table in target database.';

grant execute on function @extschema@.fn_etl_check_target_variable(json, integer[]) to public;
