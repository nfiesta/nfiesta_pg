--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_get_sub_population_categories(integer, integer, integer[])

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_get_sub_population_categories(integer, integer, integer[]) CASCADE;

create or replace function @extschema@.fn_etl_get_sub_population_categories
(
	_id					integer,
	_sub_population		integer,
	_etl_id				integer[] default null::integer[]
)
returns table
(
	id				integer,
	etl_id			integer,
	label			varchar,
	description		text,
	label_en		varchar,
	description_en	text
)
as
$$
declare
begin
		if _id is null
		then
			raise exception 'Error 01: fn_etl_get_sub_population_categories: Input argument _id must not be null!';
		end if;

		if _etl_id is null
		then
			return query
			select
					_id as id,
					spc.id as etl_id,
					spc.label,
					spc.description,
					spc.label_en,
					spc.description_en
			from
					@extschema@.c_sub_population_category as spc
			where
					spc.sub_population = _sub_population
			order
					by spc.id;
		else
			return query
			select
					_id as id,
					spc.id as etl_id,
					spc.label,
					spc.description,
					spc.label_en,
					spc.description_en
			from
					@extschema@.c_sub_population_category as spc
			where
					spc.sub_population = _sub_population
			and
					spc.id not in (select unnest(_etl_id))
			order
					by spc.id;		
		end if;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_get_sub_population_categories(integer, integer, integer[]) is
'Function returns all record from table c_sup_population.';

grant execute on function @extschema@.fn_etl_get_sub_population_categories(integer, integer, integer[]) to public;