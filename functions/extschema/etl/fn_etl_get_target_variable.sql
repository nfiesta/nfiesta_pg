--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_get_target_variable(character varying, integer[])

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_get_target_variable(character varying, integer[]) CASCADE;

create or replace function @extschema@.fn_etl_get_target_variable
(
	_national_language	character varying(2) default 'en'::character varying,
	_etl_id				integer[] default null::integer[]
)
returns table
(
	id			integer,
	metadata	json
)
as
$$
declare
begin
		if _national_language = 'en'
		then
			return query
			with
			w1 as	(
					select
							ctv.id,
							ctv.metadata->'en' as metadata
					from
							 @extschema@.c_target_variable as ctv
					where
							ctv.id not in (select unnest(_etl_id))
					)
			select
					w1.id,
					json_build_object('en',w1.metadata) as metadata
			from
					w1 order by w1.id;
		else
			return query
			with
			w1 as	(
					select
							ctv.id,
							ctv.metadata->'en' as metadata
					from
							 @extschema@.c_target_variable as ctv
					where
							ctv.id not in (select unnest(_etl_id))
					)
			,w2 as	(
					select
							ctv.id,
							ctv.metadata->_national_language as metadata
					from
							 @extschema@.c_target_variable as ctv
					where
							ctv.id not in (select unnest(_etl_id))
					)
			,w3 as	(
					select
							w1.id,
							w1.metadata as metadata_en,
							w2.metadata as metadata_nl
					from
								w1
					inner join	w2 on w1.id = w2.id
					)
			select
					w3.id,
					json_build_object
						(
						_national_language,w3.metadata_nl,
						'en',w3.metadata_en
						) as metadata
			from
					w3 order by w3.id;
		end if;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_get_target_variable(character varying, integer[]) is
'Function returns records from table c_target_variable.';

grant execute on function @extschema@.fn_etl_get_target_variable(character varying, integer[]) to public;
