--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_get_sub_populations(integer, integer[])

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_get_sub_populations(integer, integer[]) CASCADE;

create or replace function @extschema@.fn_etl_get_sub_populations
(
	_id			integer,
	_etl_id		integer[] default null::integer[]
)
returns table
(
	id				integer,
	etl_id			integer,
	label			varchar,
	description		text,
	label_en		varchar,
	description_en	text,
	atomic			boolean
)
as
$$
declare
begin
		if _id is null
		then
			raise exception 'Error 01: fn_etl_get_sub_populations: Input argument _id must not be null!';
		end if;

		if _etl_id is null
		then
			return query
			select
					_id as id,
					sp.id as etl_id,
					sp.label,
					sp.description,
					sp.label_en,
					sp.description_en,
					sp.atomic
			from
					@extschema@.c_sub_population as sp
			order
					by sp.id;
		else
			return query
			select
					_id as id,
					sp.id as etl_id,
					sp.label,
					sp.description,
					sp.label_en,
					sp.description_en,
					sp.atomic
			from
					@extschema@.c_sub_population as sp
			where
					sp.id not in (select unnest(_etl_id))					
			order
					by sp.id;		
		end if;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_get_sub_populations(integer, integer[]) is
'Function returns all record from table c_sup_population.';

grant execute on function @extschema@.fn_etl_get_sub_populations(integer, integer[]) to public;