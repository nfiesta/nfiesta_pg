--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_check_sub_populations4etl_json(json, integer[])

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_check_sub_populations4etl_json(json, integer[]) CASCADE;

create or replace function @extschema@.fn_etl_check_sub_populations4etl_json
(
	_sub_populations	json,
	_etl_id				integer[] default null::integer[]
)
returns json
as
$$
declare
		_array_id										integer[];
		_check_etl_id									integer;
		_res_id_transfer								integer[];
		_res_id_manually_type_equal						integer[];
		_res_id_manually_type_equal_not					integer[];
		_res_id_manually_type_equal_not_final			integer[];	
		_res_id_pair									integer[];
		_sub_population_i								integer[];
		_label_en_type_i								varchar;
		_sub_population_etl_id							integer[];
		_sub_population_check							boolean[];
		_array_category_label_en_source					text[];
		_count_categories_target						integer;
		_res_loop_iii									integer;
		_do_action_in_loop								integer;
		_id_comulation									integer[];
		_etl_id_i_target_array							integer[];
		_sub_population_ii								integer[];
		_label_en_type_ii								varchar;	
		_sub_population_etl_id_ii						integer[];
		_sub_population_check_ii						boolean[];
		_count_category_label_en_source_type_equal_not	integer;
		_check_count_categories_type_equal_not			integer;
		_res_id_manually								integer[];
		_check_manually_pair							integer;
		_check_manually_transfer						integer;
		_check_pair_transfer							integer;
		_res											json;
begin
		if _sub_populations is null
		then
			raise exception 'Error 01: fn_etl_check_sub_populations4etl_json: Input argument _sub_populations must not be NULL!';
		end if;

		with
		w1 as	(
				select _sub_populations as res
				)
		,w2 as	(
				select json_array_elements(w1.res) as res from w1
				)
		,w3 as	(
				select (res->>'id')::integer as id from	w2
				)
		select array_agg(w3.id order by w3.id) from	w3
		into _array_id;
	
		select count(t.*) from
		(
		select csp.id from @extschema@.c_sub_population as csp except -- EXCEPT input ETL ids with ids in c_sub_population_table
		select unnest(_etl_id)
		) as t
		into _check_etl_id;
	
		if _check_etl_id = 0 -- if IDs in input argument _etl_id removed all IDs from c_sub_population table => then auto transfer
		then
			with
			w1 as	(select unnest(_array_id) as id, true as auto_transfer, false as auto_pair)
			select json_agg(json_build_object('id',w1.id,'auto_transfer',w1.auto_transfer,'auto_pair',w1.auto_pair)) from w1
			into _res;
		else
			_res_id_transfer := array[0];
			_res_id_manually_type_equal := array[0];
			_res_id_manually_type_equal_not := array[0];
			_res_id_pair := array[0];

			for i in 1..array_length(_array_id,1) -- The first cycle over TYPEs
			loop
				-----------------------------------------------------
				with
				w1 as	(
						select _sub_populations as res
						)
				,w2 as	(
						select json_array_elements(w1.res) as res from w1
						)
				,w3 as	(
						select
								(res->>'id')::integer as id,
								replace(replace((res->>'sub_population')::varchar,'[',''),']','') as sub_population,
								(res->>'label_en_type')::varchar as label_en_type
						from
								w2
						)
				,w4 as	(
						select
								w3.id,
								(string_to_array(w3.sub_population,','))::integer[] as sub_population,
								w3.label_en_type
						from
								w3
						)
				select
						w4.sub_population,
						w4.label_en_type
				from
						w4 where w4.id = _array_id[i]
				into
						_sub_population_i,
						_label_en_type_i;
				-----------------------------------------------------
				-- check on level type
				with
				w1 as	(
						select * from @extschema@.fn_etl_check_sub_populations4etl
							(
							_array_id[i],
							_sub_population_i,
							_label_en_type_i,
							'en'::varchar,
							_etl_id
							)
						)
				select
						array_agg(w1.etl_id order by w1.etl_id) as sp_etl_id, -- ID from c_sub_population table
						array_agg(w1.check_label_en order by w1.etl_id) as sp_check
				from
						w1
				into
						_sub_population_etl_id,
						_sub_population_check;
				-----------------------------------------------------					
				if _sub_population_etl_id is null  -- => auto transfer
				then
					_res_id_transfer := _res_id_transfer || _array_id[i];
				else
					if array_length(_sub_population_etl_id,1) = 1 and _sub_population_check[1] = true -- maybe to pair if all categories will be paired
					then
						-- sub population source label and sub population target label is equal
						-- => check categories
						with
						w1 as	(
								select _sub_populations as res
								)
						,w2 as	(
								select json_array_elements(w1.res) as res from w1
								)
						,w3 as	(
								select
										(res->>'id')::integer as id,
										replace(replace((res->>'sub_population')::varchar,'[',''),']','') as sub_population,
										(res->>'label_en_type')::varchar as label_en_type,
										res->'category' as category_json
								from
										w2
								)
						,w4 as	(
								select
										w3.id,
										(string_to_array(w3.sub_population,','))::integer[] as sub_population,
										w3.label_en_type,
										w3.category_json
								from
										w3 where w3.id = _array_id[i]
								)
						,w5 as	(
								select
										w4.id,
										w4.sub_population,
										w4.label_en_type,
										json_array_elements(w4.category_json) as category_json
								from
										w4
								)
						,w6 as	(
								select category_json->>'label_en_category' as category_label_en from w5
								)
						select array_agg(w6.category_label_en) from w6
						into _array_category_label_en_source;
					
						select count(cspc.*) from @extschema@.c_sub_population_category as cspc
						where cspc.sub_population = _sub_population_etl_id[1]
						into _count_categories_target;
					
						if array_length(_array_category_label_en_source,1) is distinct from _count_categories_target
						then
							-- info: Source and target labels of sub population type are equal, but count of their categories are not equal!
							-- => manually pair
							_res_loop_iii := 0; -- <= loop over categories is not needed !!!
						else
							_do_action_in_loop := 1;
							_id_comulation := array[0];

							for iii in 1..array_length(_array_category_label_en_source,1)
							loop						
								if _do_action_in_loop = 1
								then
									select array_agg(cspc.id) from @extschema@.c_sub_population_category as cspc
									where cspc.sub_population = _sub_population_etl_id[1]
									and cspc.id not in (select unnest(_id_comulation))
									and @extschema@.fn_etl_array_compare
										(
										string_to_array(replace(lower(cspc.label_en),' ',''),';'),
										string_to_array(replace(lower(_array_category_label_en_source[iii]),' ',''),';')					
										) = true
									into _etl_id_i_target_array;
									-- founded 1 element in array => ok => auto pair
									-- founded 2 or more elements in array => error
									-- founded 0 => null => ko => manualy pair
								
									if _etl_id_i_target_array is null
									then
										_do_action_in_loop := 0; -- stop action in loop
										_res_loop_iii := 0;
									else
										if array_length(_etl_id_i_target_array,1) is distinct from 1
										then
											raise exception 'Error 02: fn_etl_check_sub_populations4etl_json: For sub population type exists two or more equal categories!';
										else
											_id_comulation := _id_comulation || _etl_id_i_target_array[1];
											_res_loop_iii := 1;
										end if;
									end if;
								end if;						
							end loop;
						end if;							

						if _res_loop_iii = 0
						then
							_res_id_manually_type_equal := _res_id_manually_type_equal || _array_id[i]; -- must stay here
						else
							_res_id_pair := _res_id_pair || _array_id[i];
							--_etl_id := _etl_id || _sub_population_etl_id[1];		
						end if;	
					
						_etl_id := _etl_id || _sub_population_etl_id[1]; -- IDs input + IDs label types are equal
					else
						-- sub population type source label and sub population target label is NOT equal => manually pairing
						_res_id_manually_type_equal_not := _res_id_manually_type_equal_not || _array_id[i]; -- can move to auto transfer
					end if;
				end if;
				-----------------------------------------------------
			end loop;

			if _res_id_manually_type_equal_not is distinct from array[0] -- exists one or more types for manualy pairing that can move to auto transfer
			then
				_res_id_manually_type_equal_not_final := _res_id_manually_type_equal_not; -- here _final is original array

				for ii in 1..array_length(_res_id_manually_type_equal_not,1) -- the second cycle
				loop
					if _res_id_manually_type_equal_not[ii] is distinct from 0
					then
						-----------------------------------------------------
						with
						w1 as	(
								select _sub_populations as res
								)
						,w2 as	(
								select json_array_elements(w1.res) as res from w1
								)
						,w3 as	(
								select
										(res->>'id')::integer as id,
										replace(replace((res->>'sub_population')::varchar,'[',''),']','') as sub_population,
										(res->>'label_en_type')::varchar as label_en_type
								from
										w2
								)
						,w4 as	(
								select
										w3.id,
										(string_to_array(w3.sub_population,','))::integer[] as sub_population,
										w3.label_en_type
								from
										w3
								)
						select
								w4.sub_population,
								w4.label_en_type
						from
								w4 where w4.id = _res_id_manually_type_equal_not[ii]
						into
								_sub_population_ii,
								_label_en_type_ii;
						-----------------------------------------------------
						with
						w1 as	(
								select * from @extschema@.fn_etl_check_sub_populations4etl
									(
									_res_id_manually_type_equal_not[ii],
									_sub_population_ii,
									_label_en_type_ii,
									'en'::varchar,
									_etl_id -- this array is supplemented by (IDs input + IDs label types are equal)
									)
								)
						select
								array_agg(w1.etl_id order by w1.etl_id) as sp_etl_id, -- ID from c_sub_population table
								array_agg(w1.check_label_en order by w1.etl_id) as sp_check
						from
								w1
						into
								_sub_population_etl_id_ii,
								_sub_population_check_ii;
						-----------------------------------------------------
						if _sub_population_etl_id_ii is null  -- => auto transfer
						then
							_res_id_transfer := _res_id_transfer || _res_id_manually_type_equal_not[ii]; -- this manually ID is moved to auto transfer
							_res_id_manually_type_equal_not_final := array_remove(_res_id_manually_type_equal_not_final,_res_id_manually_type_equal_not[ii]); -- this manually ID is removed from manually pairing equal type not
						else
							-- exist one or more different types => then do check count of categories:
							-- 1. => if exists type with equal count of categories => stay in manually pairing
							-- 2. => if NOT exists any type with equal count of categories => ID can move to transfer
						
							-- => check count of source categories
							with
							w1 as	(
									select _sub_populations as res
									)
							,w2 as	(
									select json_array_elements(w1.res) as res from w1
									)
							,w3 as	(
									select
											(res->>'id')::integer as id,
											replace(replace((res->>'sub_population')::varchar,'[',''),']','') as sub_population,
											(res->>'label_en_type')::varchar as label_en_type,
											res->'category' as category_json
									from
											w2
									)
							,w4 as	(
									select
											w3.id,
											(string_to_array(w3.sub_population,','))::integer[] as sub_population,
											w3.label_en_type,
											w3.category_json
									from
											w3 where w3.id = _res_id_manually_type_equal_not[ii]
									)
							,w5 as	(
									select
											w4.id,
											w4.sub_population,
											w4.label_en_type,
											json_array_elements(w4.category_json) as category_json
									from
											w4
									)
							,w6 as	(
									select category_json->>'label_en_category' as category_label_en from w5
									)
							select count(w6.category_label_en) from w6
							into _count_category_label_en_source_type_equal_not;						
						
							with
							w1 as	(
									select * from @extschema@.fn_etl_check_sub_populations4etl
										(
										_res_id_manually_type_equal_not[ii],
										_sub_population_ii,
										_label_en_type_ii,
										'en'::varchar,
										_etl_id -- this array is supplemented by (IDs input + IDs label types are equal)
										)
									)
							,w2 as	(
									select * from @extschema@.c_sub_population_category as cspc
									where cspc.sub_population in (select w1.etl_id from w1)
									)
							,w3 as	(
									select w2.sub_population, count(w2.*) as pocet from w2
									group by w2.sub_population
									)
							select count(w3.*) from w3 where w3.pocet = _count_category_label_en_source_type_equal_not
							into _check_count_categories_type_equal_not;
						
							if _check_count_categories_type_equal_not = 0
							then
								-- ID can move to auto transfer
								_res_id_transfer := _res_id_transfer || _res_id_manually_type_equal_not[ii]; -- this manually ID is moved to auto transfer
								_res_id_manually_type_equal_not_final := array_remove(_res_id_manually_type_equal_not_final,_res_id_manually_type_equal_not[ii]); -- this manually ID is removed from manually pairing equal type not							
							else
								-- ID stay in manually pairing
								_res_id_manually_type_equal_not_final := _res_id_manually_type_equal_not_final;
							end if;
				
						end if;																
					end if;
				end loop;
			end if;
		
			_res_id_manually := _res_id_manually_type_equal || _res_id_manually_type_equal_not_final;

			
			-- check MANUALLY and PAIR
			with
			w1 as	(select unnest(_res_id_manually) as id_manually)
			,w2 as	(select unnest(_res_id_pair) as id_pair)	
			,w3 as	(select w1.* from w1 where w1.id_manually is distinct from 0)
			,w4 as	(select w2.* from w2 where w2.id_pair is distinct from 0)
			,w5 as	(select count(w3.*) as res from w3 where w3.id_manually in (select w4.id_pair from w4))
			,w6 as	(select count(w4.*) as res from w4 where w4.id_pair in (select w3.id_manually from w3))
			select sum(t.res) from (select w5.res from w5 union all	select w6.res from w6) as t
			into _check_manually_pair;

			if _check_manually_pair is distinct from 0
			then
				raise exception 'Error 03: fn_etl_check_sub_populations4etl_json: There must not exist situation auto pair and manually pair for the sub population type at the same time!';
			end if;

			-- check MANUALLY and TRANFER
			with
			w1 as	(select unnest(_res_id_manually) as id_manually)
			,w2 as	(select unnest(_res_id_transfer) as id_transfer)	
			,w3 as	(select w1.* from w1 where w1.id_manually is distinct from 0)
			,w4 as	(select w2.* from w2 where w2.id_transfer is distinct from 0)
			,w5 as	(select count(w3.*) as res from w3 where w3.id_manually in (select w4.id_transfer from w4))
			,w6 as	(select count(w4.*) as res from w4 where w4.id_transfer in (select w3.id_manually from w3))
			select sum(t.res) from (select w5.res from w5 union all	select w6.res from w6) as t
			into _check_manually_transfer;

			if _check_manually_transfer is distinct from 0
			then
				raise exception 'Error 04: fn_etl_check_sub_populations4etl_json: There must not exist situation auto transfer and manually pair for the sub population type at the same time!';
			end if;	

			-- check PAIR and TRANFER
			with
			w1 as	(select unnest(_res_id_pair) as id_pair)
			,w2 as	(select unnest(_res_id_transfer) as id_transfer)	
			,w3 as	(select w1.* from w1 where w1.id_pair is distinct from 0)
			,w4 as	(select w2.* from w2 where w2.id_transfer is distinct from 0)
			,w5 as	(select count(w3.*) as res from w3 where w3.id_pair in (select w4.id_transfer from w4))
			,w6 as	(select count(w4.*) as res from w4 where w4.id_transfer in (select w3.id_pair from w3))
			select sum(t.res) from (select w5.res from w5 union all	select w6.res from w6) as t
			into _check_pair_transfer;

			if _check_pair_transfer is distinct from 0
			then
				raise exception 'Error 05: fn_etl_check_sub_populations4etl_json: There must not exist situation auto transfer and auto pair for the sub population type at the same time!';
			end if;					

			with
			w1 as	(select unnest(_array_id) as id)
			,w2 as	(select unnest(_res_id_pair) as id_pair)
			,w3 as	(select unnest(_res_id_transfer) as id_transfer)
			,w4 as	(select w2.* from w2 where w2.id_pair is distinct from 0)
			,w5 as	(select w3.* from w3 where w3.id_transfer is distinct from 0)
			,w6 as	(
					select
							w1.id,
							case when w5.id_transfer is null then false else true end as auto_transfer,
							case when w4.id_pair is null then false else true end as auto_pair
					from
							w1
							left join w5 on w1.id = w5.id_transfer
							left join w4 on w1.id = w4.id_pair
					order
							by w1.id
					)
			select json_agg(json_build_object('id',w6.id,'auto_transfer',w6.auto_transfer,'auto_pair',w6.auto_pair)) from w6
			into _res;

		end if;
	
		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_check_sub_populations4etl_json(json, integer[]) is
'The function returns one record of sub population if input label of sub population was paired in target DB or the function returns record(s) of sub populations that are possible to pair in target DB. Output is in JSON format.';

grant execute on function @extschema@.fn_etl_check_sub_populations4etl_json(json, integer[]) to public;