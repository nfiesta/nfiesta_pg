--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_import_variable(json)

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_import_variable(json) CASCADE;

create or replace function @extschema@.fn_etl_import_variable
(
	_variables	json
)
returns text
as
$$
declare
		_max_id_tv		integer;
		_res			text;
begin
		if _variables is null
		then
			raise exception 'Error 01: fn_etl_import_variable: Input argument _variables must not by NULL!';
		end if;
	
		_max_id_tv := (select coalesce(max(id),0) from @extschema@.t_variable);

		with
		w1 as	(
				select json_array_elements(_variables) as s
				)
		,w2 as	(
				select
						(s->>'target_variable')::integer			as target_variable,
						(s->>'sub_population_category')::integer	as sub_population_category,
						(s->>'area_domain_category')::integer		as area_domain_category
				from w1
				)
		,w3 as	(
				select
						w2.target_variable,
						case when w2.sub_population_category = 0 then null::integer else w2.sub_population_category end as sub_population_category,
						case when w2.area_domain_category = 0 then null::integer else w2.area_domain_category end as area_domain_category,
						null::integer as auxiliary_variable_category
				from
						w2
				)
		,w4 as	(
				select
						target_variable,
						sub_population_category,
						area_domain_category ,
						null::integer as auxiliary_variable_category
				from
						@extschema@.t_variable where target_variable = (select distinct target_variable from w2)
				)
		,w5 as	(
				select w3.target_variable, w3.sub_population_category, w3.area_domain_category, w3.auxiliary_variable_category from w3 except
				select w4.target_variable, w4.sub_population_category, w4.area_domain_category, w4.auxiliary_variable_category from w4
				)
		insert into @extschema@.t_variable(target_variable, sub_population_category, area_domain_category, auxiliary_variable_category)
		select
				target_variable,
				sub_population_category,
				area_domain_category,
				auxiliary_variable_category
		from w5
		order by target_variable, sub_population_category, area_domain_category, auxiliary_variable_category;

		_res := concat('The ',(select count(*) from @extschema@.t_variable where id > _max_id_tv),' new rows were inserted into t_variable.');
	
		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_import_variable(json) IS
'The function solves ETL proces for t_variable table.';

grant execute on function @extschema@.fn_etl_import_variable(json) to public;