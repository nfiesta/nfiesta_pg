--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_update_area_domain_category_description(integer, character varying, text)

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_update_area_domain_category_description(integer, character varying, text) CASCADE;

create or replace function @extschema@.fn_etl_update_area_domain_category_description
(
	_area_domain_category	integer,
	_national_language		character varying(2),
	_description			text
)
returns text
as
$$
declare
		_column							text;
		_description_category_original	text;
		_res							text;
begin
		if _area_domain_category is null
		then
			raise exception 'Error 01: fn_etl_update_area_domain_category_description: Input argument _area_domain_category must not be NULL!';
		end if;
	
		if _national_language is null
		then
			raise exception 'Error 02: fn_etl_update_area_domain_category_description: Input argument _national_language must not be NULL!';
		end if;
	
		if _description is null
		then
			raise exception 'Error 03: fn_etl_update_area_domain_category_description: Input argument _description must not be NULL!';
		end if;
		-----------------------------------------
		if	(
			select count(cadc.*) is distinct from 1
			from @extschema@.c_area_domain_category as cadc
			where cadc.id = _area_domain_category
			)
		then
			raise exception 'Error 04: fn_etl_update_area_domain_category_description: Input argument _area_domain_category (ID) = % is not present in c_area_domain_category table!',_area_domain_category;
		end if;
		-----------------------------------------
		if	(
			select cad.atomic = false
			from @extschema@.c_area_domain as cad
			where cad.id = (select cadc.area_domain from @extschema@.c_area_domain_category as cadc where cadc.id = _area_domain_category)
			)
		then
			raise exception 'Error 05: fn_etl_update_area_domain_category_description: Input argument _area_domain_category (ID) = % is not category from ATOMIC area domain!',_area_domain_category;
		end if;
		-----------------------------------------
		-- UPDATE description of ATOMIC area domain category		
		if _national_language = 'en'::varchar
		then
			_column := 'description_en';
			select cadc.description_en from @extschema@.c_area_domain_category as cadc where cadc.id = _area_domain_category into _description_category_original;
			update @extschema@.c_area_domain_category set description_en = _description where id = _area_domain_category;
		else
			_column := 'description';
			select cadc.description from @extschema@.c_area_domain_category as cadc where cadc.id = _area_domain_category into _description_category_original;
			update @extschema@.c_area_domain_category set description = _description where id = _area_domain_category;
		end if;
		-----------------------------------------
		-- UPDATE description of NON-ATOMIC area domain category
		execute
		'
		with
		w1 as	(
				select * from @extschema@.cm_area_domain_category where area_domain_category in
				(select area_domain_category from @extschema@.cm_area_domain_category where atomic_category = $1)
				)
		,w2 as	(
				select
						w1.id,
						w1.area_domain_category,	 																			-- id_target_db of non-atomic category 
						w1.atomic_category,																						-- id_target_db of atomic category
						t1.'|| _column ||' as description_category_updated,														-- description atomic category updated
						case when w1.atomic_category = $1 then $2 else t1.'|| _column ||' end as description_category_original	-- description atomic category
				from
						w1
						inner join @extschema@.c_area_domain_category as t1 on w1.atomic_category = t1.id
				)				
		---------
		,w3 as	(
				select
						id,
						string_to_array('|| _column ||','';'') as description_category 
				from
						@extschema@.c_area_domain_category
				where
						id in (select w2.area_domain_category from w2)
				)
		,w4 as	(
				select
						w3.*,
						(select array_agg(t.res order by t.res) from (select generate_series(1,array_length(w3.description_category,1)) as res) as t) as id_order
				from
						w3
				)
		,w5 as	(
				select
						w4.id,
						unnest(w4.description_category) as description_category,
						unnest(w4.id_order) as id_order
				from
						w4
				)
		--------
		,w6 as	(
				select w2.*, w5.id_order from w2 inner join w5
				on w2.area_domain_category = w5.id and w2.description_category_original = w5.description_category
				)		
		,w7 as	(
				select
						w6.area_domain_category,
						array_agg(w6.description_category_updated order by w6.id_order) as description_category,
						array_agg(w6.id_order order by w6.id_order) as id_order
				from
						w6 group by w6.area_domain_category
				)
		,w8 as	(
				select
						w7.area_domain_category,
						array_to_string(w7.description_category,'';'') as description4update
				from
						w7
				)
		update @extschema@.c_area_domain_category set '|| _column ||' = w8.description4update
		from w8 where c_area_domain_category.id = w8.area_domain_category;
		'
		using _area_domain_category, _description_category_original;		
		-----------------------------------------
		_res := concat('The description of area domain category [c_area_domain_category.id = ',_area_domain_category,'] for language element = "',_national_language,'" was changed. The update was done for ATOMIC and NON-ATOMIC area domain categories.');
		-----------------------------------------
		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_update_area_domain_category_description(integer, character varying, text) is
'The tunction updates an area domain category description in c_area_domain_category table for given area domain category.';

grant execute on function @extschema@.fn_etl_update_area_domain_category_description(integer, character varying, text) to public;