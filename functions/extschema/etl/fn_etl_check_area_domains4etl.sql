--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_check_area_domains4etl(integer, integer[], varchar, character varying, integer[])

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_check_area_domains4etl(integer, integer[], varchar, character varying, integer[]) CASCADE;

create or replace function @extschema@.fn_etl_check_area_domains4etl
(
	_id					integer,													-- ordering ID from source DB
	_area_domain		integer[],													-- array of area domains for set count of elements
	_label_en			varchar,													-- label of area domains for check
	_national_language	character varying(2) default 'en'::character varying(2),	-- for result ordering column	
	_etl_id				integer[] default null::integer[]							-- etl ids of area domains that had been etl-ed yet
)
returns table
(
	id				integer,
	etl_id			integer,
	label			character varying,
	description		text,
	label_en		character varying,
	description_en	text,
	atomic			boolean,
	check_label_en	boolean
)
as
$$
declare
		_check_count			integer;
		_set_ordering_column	text;
begin
		if _id is null
		then
			raise exception 'Error 01: fn_etl_check_area_domains4etl: Input argument _id must not be NULL!';
		end if; 
	
		if _area_domain is null
		then
			raise exception 'Error 02: fn_etl_check_area_domains4etl: Input argument _area_domain must not be NULL!';
		end if;
	
		if _label_en is null
		then
			raise exception 'Error 03: fn_etl_check_area_domains4etl: Input argument _label_en must not be NULL!';
		end if;

		if _national_language is null
		then
			raise exception 'Error 04: fn_etl_check_area_domains4etl: Input argument _national_language must not be NULL!';
		end if;
		---------------------------------------------------
		with
		w1 as	(
				select cad.* from @extschema@.c_area_domain as cad
				where cad.id not in (select unnest(_etl_id))
				and array_length(string_to_array(cad.label_en,';'),1) = array_length(_area_domain,1)
				)
		,w2 as	(
				select
						w1.*,
						@extschema@.fn_etl_array_compare
							(
							string_to_array(replace(lower(w1.label_en),' ',''),';'),
							string_to_array(replace(lower(_label_en),' ',''),';')					
							) as check
				from w1
				)
		select count(w2.*) from w2 where w2.check = true
		into _check_count;
		---------------------------------------------------
		if _check_count > 1
		then
			raise exception 'Error 05: fn_etl_check_area_domains4etl: For input argument _label_en exist more than one record in c_area_domain table!';
		end if;
		---------------------------------------------------
		if _national_language = 'en'
		then
			_set_ordering_column := 'w2.label_en';		
		else
			_set_ordering_column := 'w2.label';
		end if;
		---------------------------------------------------
		if _check_count = 1
		then
			return query execute
			'
			with
			w1 as	(
					select cad.* from @extschema@.c_area_domain as cad
					where cad.id not in (select unnest($4))
					and array_length(string_to_array(cad.label_en,'';''),1) = array_length($2,1)
					)
			,w2 as	(
					select
							w1.*,
							@extschema@.fn_etl_array_compare
								(
								string_to_array(replace(lower(w1.label_en),'' '',''''),'';''),
								string_to_array(replace(lower($3),'' '',''''),'';'')					
								) as check
					from w1
					)
			select
					$1 as id,
					w2.id as etl_id,
					w2.label,
					w2.description,
					w2.label_en,
					w2.description_en,
					w2.atomic,
					w2.check
			from
					w2 where w2.check = true
			'
			using _id, _area_domain, _label_en, _etl_id;		
		else
			return query execute
			'
			with
			w1 as	(
					select cad.* from @extschema@.c_area_domain as cad
					where cad.id not in (select unnest($4))
					and array_length(string_to_array(cad.label_en,'';''),1) = array_length($2,1)
					)
			,w2 as	(
					select
							w1.*,
							@extschema@.fn_etl_array_compare
								(
								string_to_array(replace(lower(w1.label_en),'' '',''''),'';''),
								string_to_array(replace(lower($3),'' '',''''),'';'')					
								) as check
					from w1
					)
			select
					$1 as id,
					w2.id as etl_id,
					w2.label,
					w2.description,
					w2.label_en,
					w2.description_en,
					w2.atomic,
					w2.check
			from
					w2 order by '|| _set_ordering_column ||'
			'
			using _id, _area_domain, _label_en, _etl_id;
		end if;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_check_area_domains4etl(integer, integer[], varchar, character varying, integer[]) is
'The function returns one record of area domain if input label of area domain was paired in target DB or the function returns record(s) of area domains that are possible to pair in target DB.';

grant execute on function @extschema@.fn_etl_check_area_domains4etl(integer, integer[], varchar, character varying, integer[]) to public;