--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_import_area_domain_category(integer, integer, varchar, text, varchar, text)

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_import_area_domain_category(integer, integer, varchar, text, varchar, text) CASCADE;

create or replace function @extschema@.fn_etl_import_area_domain_category
(
	_id					integer,
	_area_domain		integer,
	_label				varchar,
	_description		text,
	_label_en			varchar,
	_description_en		text
)
returns table
(
	id		integer,
	etl_id	integer
)
as
$$
declare
		_etl_id		integer;
begin
	if _id is null
	then
		raise exception 'Error 01: fn_etl_import_area_domain_category: Input argument _id must not by NULL!';
	end if;

	if _area_domain is null
	then
		raise exception 'Error 02: fn_etl_import_area_domain_category: Input argument _area_domain must not by NULL!';
	end if;

	if _label is null
	then
		raise exception 'Error 03: fn_etl_import_area_domain_category: Input argument _label must not by NULL!';
	end if;

	if _description is null
	then
		raise exception 'Error 04: fn_etl_import_area_domain_category: Input argument _description must not by NULL!';
	end if; 

	insert into @extschema@.c_area_domain_category(area_domain, label, description, label_en, description_en)
	select _area_domain, _label, _description, _label_en, _description_en
	returning c_area_domain_category.id
	into _etl_id;

	return query select _id, _etl_id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_import_area_domain_category(integer, integer, varchar, text, varchar, text) is
'Function inserts a record into table c_area_domain_category based on given parameters.';

grant execute on function @extschema@.fn_etl_import_area_domain_category(integer, integer, varchar, text, varchar, text) to public;