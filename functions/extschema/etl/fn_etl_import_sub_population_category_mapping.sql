--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_import_sub_population_category_mapping(json)

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_import_sub_population_category_mapping(json) CASCADE;

create or replace function @extschema@.fn_etl_import_sub_population_category_mapping
(
	_spc_mapping	json
)
returns text
as
$$
declare
		_max_id_cm_sub_population_category	integer;
		_check_atomic_id					integer;
		_res								text;
begin
		-------------------------------------------------------------
		_max_id_cm_sub_population_category := (select coalesce(max(id),0) from @extschema@.cm_sub_population_category);
		-------------------------------------------------------------
		-------------------------------------------------------------
		-- adding data into cm_sub_population_category
		with
		w1 as	(
				select json_array_elements(_spc_mapping) as s
				)
		,w2 as	(
				select
						(s->>'sub_population_category_non_atomic')::integer	as sub_population_category,
						(s->>'sub_population_category_atomic')::integer		as atomic_category
				from w1
				)
		,w3 as (
				select w2.sub_population_category, w2.atomic_category from w2 except
				select t1.sub_population_category, t1.atomic_category from @extschema@.cm_sub_population_category as t1
				)
		insert into @extschema@.cm_sub_population_category(sub_population_category,atomic_category)
		select w3.sub_population_category, w3.atomic_category from w3
		order by w3.sub_population_category, w3.atomic_category;
		-------------------------------------------------------------
		-------------------------------------------------------------
		_res := concat('The ',(select count(*) from @extschema@.cm_sub_population_category where id > _max_id_cm_sub_population_category),' new records were inserted into cm_sub_population_category table.');
		-------------------------------------------------------------
		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_import_sub_population_category_mapping(json) is
'Function inserts a new records into table cm_sub_population_category.';

grant execute on function @extschema@.fn_etl_import_sub_population_category_mapping(json) to public;