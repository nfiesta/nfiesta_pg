--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_get_topics4target_variable(integer, character varying, boolean)

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_get_topics4target_variable(integer, character varying, boolean) CASCADE;

create or replace function @extschema@.fn_etl_get_topics4target_variable
(
	_target_variable	integer,
	_national_language	character varying(2) default 'en'::character varying,
	_linked				boolean default null::boolean
)
returns table
(
	id				integer,
	label			varchar,
	description		text,
	label_en		varchar,
	description_en	text,
	linked			boolean
)
as
$$
declare
		_set_ordering_column	text;
		_cond					text;
begin
		if _target_variable is null
		then
			raise exception 'Error 01: fn_etl_get_topics4target_variable: Input argument _target_variable must not be null!';
		end if;
	
		if _national_language is null
		then
			raise exception 'Error 01: fn_etl_get_topics4target_variable: Input argument _national_language must not be null!';
		end if;	
		---------------------------------------------------
		if _national_language = 'en'
		then
			_set_ordering_column := 'w3.label_en';
		else
			_set_ordering_column := 'w3.label';
		end if;
		---------------------------------------------------
		if _linked is null
		then
			_cond := 'TRUE';
		else
			if _linked = true
			then
				_cond := 'w3.linked = true';
			else
				_cond := 'w3.linked = false';
			end if;
		end if;
		---------------------------------------------------	
		return query execute
		'
		with
		w1 as	(
				select ct.id, ct.label, ct.description, ct.label_en, ct.description_en
				from @extschema@.c_topic as ct
				)
		,w2 as	(
				select distinct topic from @extschema@.cm_tvariable2topic
				where target_variable = $1
				)
		,w3 as	(
				select
						w1.id,
						w1.label,
						w1.description,
						w1.label_en,
						w1.description_en,
						case when w2.topic is null then false else true end as linked
				from
						w1 left join w2 on w1.id = w2.topic
				)
		select
				w3.* from w3
		where
				'|| _cond ||'
		order
				by '|| _set_ordering_column ||'
		'
		using _target_variable;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_get_topics4target_variable(integer, character varying, boolean) is
'The function returns all topics and information whitch topic is the given target variable linked.';

grant execute on function @extschema@.fn_etl_get_topics4target_variable(integer, character varying, boolean) to public;