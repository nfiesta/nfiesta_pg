--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_import_sub_population(integer, varchar, text, varchar, text, boolean)

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_import_sub_population(integer, varchar, text, varchar, text, boolean) CASCADE;

create or replace function @extschema@.fn_etl_import_sub_population
(
	_id					integer,
	_label				varchar,
	_description		text,
	_label_en			varchar,
	_description_en		text,
	_atomic				boolean
)
returns table
(
	id		integer,
	etl_id	integer
)
as
$$
declare
		_etl_id		integer;
begin
	if _id is null
	then
		raise exception 'Error 01: fn_etl_import_sub_population: Input argument _id must not be NULL!';
	end if;

	if _label is null
	then
		raise exception 'Error 02: fn_etl_import_sub_population: Input argument _label must not be NULL!';
	end if;

	if _description is null
	then
		raise exception 'Error 03: fn_etl_import_sub_population: Input argument _description must not be NULL!';
	end if;

	if _label_en is null
	then
		raise exception 'Error 04: fn_etl_import_sub_population: Input argument _label_en must not be NULL!';
	end if;

	if _description_en is null
	then
		raise exception 'Error 05: fn_etl_import_sub_population: Input argument _description_en must not be NULL!';
	end if;

	if _atomic is null
	then
		raise exception 'Error 06: fn_etl_import_sub_population: Input argument _atomic must not be NULL!';
	end if;  	  

	insert into @extschema@.c_sub_population(label, description, label_en, description_en, atomic)
	select _label, _description, _label_en, _description_en, _atomic
	returning c_sub_population.id
	into _etl_id;

	return query select _id, _etl_id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_import_sub_population(integer, varchar, text, varchar, text, boolean) is
'Function inserts a record into table c_sub_population based on given parameters.';

grant execute on function @extschema@.fn_etl_import_sub_population(integer, varchar, text, varchar, text, boolean) to public;