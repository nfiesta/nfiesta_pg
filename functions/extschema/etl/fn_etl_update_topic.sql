--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_update_topic(integer, varchar, text, varchar, text)

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_update_topic(integer, varchar, text, varchar, text) CASCADE;

create or replace function @extschema@.fn_etl_update_topic
(
	_id					integer,
	_label				varchar(200),
	_description		text,
	_label_en			varchar(200),
	_description_en		text
)
returns integer
as
$$
begin
	if _id is null
	then
		raise exception 'Error 01: fn_etl_update_topic: Input argument _id must not be null!';
	end if;

	if _label is null and _description is null and _label_en is null and _description_en is null
	then
		raise exception 'Error 02: fn_etl_update_topic: All input arguments _label, _description, _label_en and _description_en must not be null!';
	end if;

	if _label is not null
	then
		update @extschema@.c_topic set label = _label where id = _id;
	end if;

	if _description is not null
	then
		update @extschema@.c_topic set description = _description where id = _id;
	end if;

	if _label_en is not null
	then
		update @extschema@.c_topic set label_en = _label_en where id = _id;
	end if;

	if _description_en is not null
	then
		update @extschema@.c_topic set description_en = _description_en where id = _id;
	end if;

	return _id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_update_topic(integer, varchar, text, varchar, text) is
'The function updates a record in c_topic table.';

grant execute on function @extschema@.fn_etl_update_topic(integer, varchar, text, varchar, text) to public;