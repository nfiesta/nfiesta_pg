--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_check_area_domain_categories4update(json)

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_check_area_domain_categories4update(json) CASCADE;

create or replace function @extschema@.fn_etl_check_area_domain_categories4update
(
	_metadatas json
)
returns table
(
	area_domain_category	integer,
	label_source			varchar,
	description_source		text,
	label_en_source			varchar,
	description_en_source	text,
	label_target			varchar,
	description_target		text,
	label_en_target			varchar,
	description_en_target	text,
	check_label				boolean,
	check_description		boolean,
	check_label_en			boolean,
	check_description_en	boolean
)
as
$$
declare
begin
		if _metadatas is null
		then
			raise exception 'Error 01: fn_etl_check_area_domain_categories4update: Input argument _metadatas must not be NULL!';
		end if;
		-----------------------------------------
		return query
		with
		w1 as 	(
				select _metadatas as metadatas
				)
		,w2 as	(
				select json_array_elements(w1.metadatas) as metadatas from w1
				)
		,w3 as	(
				select
						(w2.metadatas->>'area_domain')::integer as area_domain_target,
						(w2.metadatas->>'area_domain_category')::integer as area_domain_category_target,
						(w2.metadatas->>'label')::varchar as label_source,
						(w2.metadatas->>'description')::text as description_source,
						(w2.metadatas->>'label_en')::varchar as label_en_source,
						(w2.metadatas->>'description_en')::text as description_en_source
				from
						w2
				)
		,w4 as	(
				select
						w3.*,
						t.label as label_target,
						t.description as description_target,
						t.label_en as label_en_target,
						t.description_en as description_en_target
				from
						w3
						inner join	(
									select cadc.* from @extschema@.c_area_domain_category as cadc
									where cadc.area_domain = (select distinct w3.area_domain_target from w3)
									) as t
						on w3.area_domain_target = t.area_domain
						and w3.area_domain_category_target = t.id
				)
		,w5 as	(
				select
						w4.*,
						case when w4.label_source = w4.label_target then true else false end as check_label,
						case when w4.description_source = w4.description_target then true else false end as check_description,
						case when w4.label_en_source = w4.label_en_target then true else false end as check_label_en,
						case when w4.description_en_source = w4.description_en_target then true else false end as check_description_en
				from
						w4
				)
		select
				w5.area_domain_category_target as area_domain_category,
				w5.label_source,
				w5.description_source,
				w5.label_en_source,
				w5.description_en_source,
				w5.label_target,
				w5.description_target,
				w5.label_en_target,
				w5.description_en_target,
				w5.check_label,
				w5.check_description,
				w5.check_label_en,
				w5.check_description_en
		from
				w5
		where
				w5.check_label = false or
				w5.check_description = false or
				w5.check_label_en = false or
				w5.check_description_en = false;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_check_area_domain_categories4update(json) is
'The tunction returns records of area domain categories with informations whether their source labels or descriptions are different from target.';

grant execute on function @extschema@.fn_etl_check_area_domain_categories4update(json) to public;