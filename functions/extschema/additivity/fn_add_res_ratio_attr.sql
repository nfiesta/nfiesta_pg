--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_add_res_ratio_attr(integer)

-- DROP FUNCTION @extschema@.fn_add_res_ratio_attr(integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_add_res_ratio_attr(
	IN est_confs integer[],
	IN min_diff double precision default 0.0, IN include_null_diff boolean default true
)
  RETURNS TABLE(
	estimation_cell		integer,
	aux_conf		integer,
	force_synthetic		boolean,
	estimate_conf		integer,
	denominator		integer,
	variable		integer,
	point_est		double precision,
	point_est_sum		double precision,
	variables_def		integer[],
	variables_found		integer[],
	estimate_confs_found	integer[],
	diff			double precision
) AS
$BODY$
DECLARE
	_complete_query text;
	_array_text text;
BEGIN
	--------------------------------QUERY--------------------------------
	_array_text := concat(quote_literal(est_confs::text), '::integer[]');
	--raise notice 'est_confs: %',  _array_text;
	_complete_query := '
with w_res_cell_var as not materialized (
	select
		t_result.estimate_conf,
		t_result.point,
		t_total_estimate_conf.estimation_cell,
		t_total_estimate_conf.variable as t_variable__id,
		t_total_estimate_conf.panel_refyearset_group,
		c_estimation_period.estimate_date_begin, c_estimation_period.estimate_date_end, 
		array_agg(t_panel_refyearset_group.panel order by panel) as panels,
		t_total_estimate_conf.aux_conf, force_synthetic, t_estimate_conf.denominator
	from @extschema@.t_result
	inner join @extschema@.t_estimate_conf ON t_estimate_conf.id = t_result.estimate_conf
	inner join @extschema@.t_total_estimate_conf ON t_total_estimate_conf.id = t_estimate_conf.total_estimate_conf
	inner join @extschema@.c_estimation_period ON c_estimation_period.id = t_total_estimate_conf.estimation_period
	inner join @extschema@.c_panel_refyearset_group ON c_panel_refyearset_group.id = t_total_estimate_conf.panel_refyearset_group
	inner join @extschema@.t_panel_refyearset_group ON t_panel_refyearset_group.panel_refyearset_group = c_panel_refyearset_group.id
	where (t_estimate_conf.estimate_type = 2)
	and (t_result.is_latest)
	and (t_result.estimate_conf = ANY (' || _array_text || '))
	group by 
		t_result.estimate_conf,
		t_result.point,
		t_total_estimate_conf.estimation_cell,
		t_total_estimate_conf.variable,
		t_total_estimate_conf.panel_refyearset_group,
		c_estimation_period.estimate_date_begin, c_estimation_period.estimate_date_end,
		t_total_estimate_conf.aux_conf, force_synthetic, t_estimate_conf.denominator
)
, w_node_sum as (
	select
		estimation_cell,
		w_res_cell_var.t_variable__id, aux_conf, force_synthetic, denominator,
		estimate_date_begin, estimate_date_end, panels,
		estimate_conf as node_estimate_confs,
		sum(point) as node_sum,
		hierarchy.node,
		hierarchy.edges as edges_def
	from w_res_cell_var
	inner join @extschema@.v_variable_hierarchy_res		as hierarchy on (hierarchy.node = w_res_cell_var.t_variable__id and hierarchy.panel_refyearset_group = w_res_cell_var.panel_refyearset_group)
	group by estimation_cell, w_res_cell_var.t_variable__id, aux_conf, force_synthetic, estimate_date_begin, estimate_date_end, panels, estimate_conf, node, edges_def, denominator
	order by estimation_cell, w_res_cell_var.t_variable__id, aux_conf, force_synthetic, estimate_date_begin, estimate_date_end, panels, estimate_conf, node, edges_def, denominator
)
, w_edge_sum as (
	select
		w_node_sum.estimation_cell,
		w_node_sum.aux_conf,
		w_node_sum.force_synthetic,
		w_node_sum.node_estimate_confs, w_node_sum.denominator,
		w_node_sum.estimate_date_begin, w_node_sum.estimate_date_end, w_node_sum.panels,
		w_node_sum.node,
		w_node_sum.node_sum,
		w_node_sum.edges_def,
		array_agg(w_res_cell_var.t_variable__id order by w_res_cell_var.t_variable__id) as edges_found,
		array_agg(w_res_cell_var.estimate_conf order by w_res_cell_var.estimate_conf) as edges_estimate_confs,
		sum(w_res_cell_var.point) as edges_sum
	from w_node_sum
	left join w_res_cell_var on (
		w_res_cell_var.estimation_cell = w_node_sum.estimation_cell
		and w_res_cell_var.estimate_date_begin = w_node_sum.estimate_date_begin
		and w_res_cell_var.estimate_date_end = w_node_sum.estimate_date_end
		and w_res_cell_var.panels = w_node_sum.panels
		and case when
			w_res_cell_var.aux_conf is null and w_node_sum.aux_conf is null then true
			else w_res_cell_var.aux_conf = w_node_sum.aux_conf end
		and case when
			w_res_cell_var.force_synthetic is null and w_node_sum.force_synthetic is null then true
			else w_res_cell_var.force_synthetic = w_node_sum.force_synthetic end
		and w_res_cell_var.t_variable__id = any(w_node_sum.edges_def)) and w_res_cell_var.denominator = w_node_sum.denominator
	group by w_node_sum.estimation_cell, 
		w_node_sum.aux_conf, w_node_sum.force_synthetic, w_node_sum.node_estimate_confs, w_node_sum.denominator,
		w_node_sum.estimate_date_begin, w_node_sum.estimate_date_end, w_node_sum.panels,
		w_node_sum.node, w_node_sum.node_sum, w_node_sum.edges_def
)
, w_diff as (
	select
		estimation_cell,
		aux_conf,
		force_synthetic,
		node_estimate_confs	as estimate_conf, denominator,
		node			as variable,
		node_sum		as point_est,
		edges_sum		as point_est_sum,
		edges_def		as variables_def,
		edges_found		as variables_found,
		edges_estimate_confs	as estimate_confs_found,
		case
			when (abs(edges_sum) >  1e-12) and (abs(node_sum) <= 1e-12) then 1.0
			when (abs(edges_sum) <= 1e-12) and (abs(node_sum) <= 1e-12) then 0
			else abs( (node_sum - edges_sum) / node_sum )
		end as diff
	from w_edge_sum
)
select * from w_diff
where
	(' || include_null_diff || ' and ((diff >= ' || min_diff || ') or (diff is null)))
	or
	(not ' || include_null_diff || ' and (diff >= ' || min_diff || '))
order by diff desc
;
';

	--RAISE NOTICE '%', _complete_query;
	RETURN QUERY EXECUTE _complete_query;
END;
$BODY$
  LANGUAGE plpgsql STABLE
  COST 100
  ROWS 100000;

comment on function @extschema@.fn_add_res_ratio_attr(integer[], double precision, boolean) is
'Function showing ratio estimates attribute additivity (aggregated class estimate should be equal to sum of sub-classes estimates). Hierarchy between aggregated classes and its sub-classes is defined in v_variable_hierarchy_res.
Function input argument is:
 * Array of attributes -- variables (FKEY to t_variable.id). All variables to be checked (aggregated class together with sub-classes) need to be passed.
 * Minimal amount of difference [%].
 * Whether include NULL difference -- indicating defined sub-classes missing in data.
Resulting table has following columns:
 * Estimate estimation cell. FKEY to c_estimation_cell.id.
 * Estimate auxiliary configuration. FKEY to t_aux_conf.id.
 * Parameter showing whether estimate is forced to be synthetic.
 * Estimate configuration id. FKEY to t_estimate_conf.id.
 * Estimate configuration id of denominator. FKEY to t_estimate_conf.id.
 * Estimate attribute -- variable. FKEY to t_variable.id.
 * Aggregated class point estimate.
 * Sum of sub-classes point estimates (belonging to aggregated class).
 * Attributes -- variables defined in hierarchy (v_variable_hierarchy_res). Array of FKEYs to t_variable.id.
 * Attributes -- variables found in data (t_result). Array of FKEYs to t_variable.id.
 * Estimate configurations found in data (t_result). Array of FKEYs to t_estimate_donf.id.
 * Relative difference between aggregated class estimate and sum of sub-classes estimates.';

GRANT EXECUTE ON FUNCTION @extschema@.fn_add_res_ratio_attr TO PUBLIC;
