--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_add_plot_aux_attr(integer, integer[], integer, double precision, boolean)

-- DROP FUNCTION @extschema@.fn_add_plot_aux_attr(integer, integer[], integer, double precision, boolean);

CREATE OR REPLACE FUNCTION @extschema@.fn_add_plot_aux_attr(
	IN var_sup integer,
	IN var_inf integer[],
	IN panel integer,
	IN min_diff double precision default 0.0,
	IN include_null_diff boolean default true
)
  RETURNS TABLE(
	plot			integer,
	variable		integer,
	ldsity			double precision,
	ldsity_sum		double precision,
	variables_def		integer[],
	variables_found		integer[],
	diff			double precision
) AS
$BODY$
DECLARE
	_complete_query text;
	_array_text_v text;
	_array_text_v_sup text;
	_array_text_v_inf text;
	_array_text_p text;
BEGIN
	--------------------------------QUERY--------------------------------
	_array_text_v := concat(quote_literal(array_prepend(var_sup, var_inf)::text), '::integer[]');
	_array_text_v_sup := concat(var_sup::text, '::integer');
	_array_text_v_inf := concat(quote_literal(var_inf::text), '::integer[]');
	if panel is not null then
		_array_text_p := concat(' AND t_available_datasets.panel = ', panel::text);
	else
		_array_text_p := '';
	end if;
	--raise notice 'variables: %',  _array_text;
	_complete_query := '

with w_plot_var as materialized (
	with w_plot_panel as (
		select
			f_p_plot.gid as plot, t_panel.id as panel 
		from	sdesign.t_panel
		join 	sdesign.cm_cluster2panel_mapping 		on cm_cluster2panel_mapping.panel = t_panel.id
		join 	sdesign.t_cluster 				on t_cluster.id = cm_cluster2panel_mapping."cluster"
		join 	sdesign.f_p_plot 				on f_p_plot."cluster" = t_cluster.id
		join 	sdesign.cm_plot2cluster_config_mapping 		on cm_plot2cluster_config_mapping.plot = f_p_plot.gid
		join 	sdesign.t_cluster_configuration 		on (t_cluster_configuration.id = t_panel.cluster_configuration 
									and cm_plot2cluster_config_mapping.cluster_configuration = t_cluster_configuration.id)
	)
	select 
		w_plot_panel.plot, 
		t_available_datasets.panel, t_available_datasets.reference_year_set, t_available_datasets.variable,
		coalesce(t_auxiliary_data.value, 0) as value
	from w_plot_panel
	join @extschema@.t_available_datasets	on (t_available_datasets.panel = w_plot_panel.panel)
	left join @extschema@.t_auxiliary_data	on (t_auxiliary_data.plot = w_plot_panel.plot 
						and t_auxiliary_data.available_datasets = t_available_datasets.id
						and t_auxiliary_data.is_latest)
	where 	t_available_datasets.variable = any (' || _array_text_v || ')
                        ' || _array_text_p || '

)
, w_node_sum as (
	select
		w_plot_var.plot,
		w_plot_var.panel,

		w_plot_var.variable as node,
		w_plot_var.value as node_sum,
		' || _array_text_v_inf || ' as edges_def
	from w_plot_var
	where w_plot_var.variable = ' || _array_text_v_sup || '
)
, w_edge_sum as (
	select
		w_node_sum.plot,

		w_node_sum.node,
		w_node_sum.node_sum,
		w_node_sum.edges_def,
		array_agg(w_plot_var.variable order by w_plot_var.variable) as edges_found,
		sum(w_plot_var.value) as edges_sum
	from w_node_sum
	left join w_plot_var on (
		w_plot_var.plot = w_node_sum.plot
		and w_plot_var.panel = w_node_sum.panel
		
		and w_plot_var.variable = any(w_node_sum.edges_def))
	group by w_node_sum.plot,
		w_node_sum.node, w_node_sum.node_sum, w_node_sum.edges_def
)
, w_diff as (
	select
		plot,

		node		as variable,
		node_sum	as ldsity,
		edges_sum	as ldsity_sum,
		edges_def	as variables_def,
		edges_found	as variables_found,
		case
			when (abs(edges_sum) >  1e-12) and (abs(node_sum) <= 1e-12) then 1.0
			when (abs(edges_sum) <= 1e-12) and (abs(node_sum) <= 1e-12) then 0
			else abs( (node_sum - edges_sum) / node_sum )
		end as diff
	from w_edge_sum
)
select * from w_diff
where
	(' || include_null_diff || ' and ((diff >= ' || min_diff || ') or (diff is null)))
	or
	(not ' || include_null_diff || ' and (diff >= ' || min_diff || '))
order by diff desc
;
';

	--RAISE NOTICE '%', _complete_query;
	RETURN QUERY EXECUTE _complete_query;
END;
$BODY$
  LANGUAGE plpgsql STABLE
  COST 100
  ROWS 100000;

comment on function @extschema@.fn_add_plot_aux_attr(integer, integer[], integer, double precision, boolean) is
'Function showing plot level auxiliary local density attribute additivity (aggregated class estimate should be equal to sum of sub-classes estimates) of auxiliary local densities. Hierarchy between aggregated classes and its sub-classes is defined in v_variable_hierarchy_plot.
Function input argument is:
 * Superior attribute -- variables (FKEY to t_variable.id). Aggregated class.
 * Array of inferior attributes -- variables (FKEY to t_variable.id). Sub-classes.
 * Array of panels -- panels (FKEY to t_panel.id).
 * Minimal amount of difference [%].
 * Whether include NULL difference -- indicating defined sub-classes missing in data.
Resulting table has following columns:
 * Plot. FKEY to f_p_plot.gid.
 * Auxiliary total attribute -- variable. FKEY to t_variable.id.
 * Aggregated class local density.
 * Sum of sub-classes local densities (belonging to aggregated class).
 * Attributes -- variables defined in hierarchy (v_variable_hierarchy_plot). Array of FKEYs to t_variable.id.
 * Attributes -- variables found in data (t_auxiliary_data). Array of FKEYs to t_variable.id.
 * Relative difference between aggregated class local densities and sum of sub-classes local densities.';

GRANT EXECUTE ON FUNCTION @extschema@.fn_add_plot_aux_attr(integer, integer[], integer, double precision, boolean) TO PUBLIC;
