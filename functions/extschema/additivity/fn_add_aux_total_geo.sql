--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_add_aux_total_geo(integer)

-- DROP FUNCTION @extschema@.fn_add_aux_total_geo(integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_add_aux_total_geo(
	IN estimation_cells integer[],
	IN min_diff double precision default 0.0, IN include_null_diff boolean default true
)
  RETURNS TABLE(
	variable		integer,
	estimation_cell		integer,
	aux_total		double precision,
	aux_total_sum		double precision,
	estimation_cells_def	integer[],
	estimation_cells_found	integer[],
	diff			double precision
) AS
$BODY$
DECLARE
	_complete_query text;
	_array_text text;
BEGIN
	--------------------------------QUERY--------------------------------
	_array_text := concat(quote_literal(estimation_cells::text), '::integer[]');
	--raise notice 'esimation cells: %',  _array_text;
	_complete_query := '
with w_auxtotal_cell_var as not materialized (
	select
		t_aux_total.estimation_cell,
		t_aux_total.aux_total,
		t_aux_total.variable
	from @extschema@.t_aux_total
	where t_aux_total.is_latest
	and t_aux_total.estimation_cell = ANY (' || _array_text || ')
)
, w_node_sum as (
	select
		estimation_cell,
		variable,
		sum(aux_total) as node_sum,
		hierarchy.node,
		hierarchy.edges as edges_def
	from w_auxtotal_cell_var
	inner join @extschema@.v_estimation_cell_hierarchy	as hierarchy on (hierarchy.node = w_auxtotal_cell_var.estimation_cell)
	group by estimation_cell, variable, node, edges
	order by estimation_cell, variable, node, edges
)
, w_edge_sum as (
	select
		w_node_sum.variable,
		w_node_sum.node,
		w_node_sum.node_sum,
		w_node_sum.edges_def,
		array_agg(w_auxtotal_cell_var.estimation_cell order by w_auxtotal_cell_var.estimation_cell) as edges_found,
		sum(w_auxtotal_cell_var.aux_total) as edges_sum
	from w_node_sum
	left join w_auxtotal_cell_var on (
		w_auxtotal_cell_var.variable = w_node_sum.variable
		and w_auxtotal_cell_var.estimation_cell = any(w_node_sum.edges_def))
	group by w_node_sum.variable, w_node_sum.node, w_node_sum.node_sum, w_node_sum.edges_def
)
, w_diff as (
	select
		variable,
		node		as estimation_cell,
		node_sum	as aux_total,
		edges_sum	as aux_total_sum,
		edges_def	as estimation_cells_def,
		edges_found	as estimation_cells_found,
		case
			when (abs(edges_sum) >  1e-12) and (abs(node_sum) <= 1e-12) then 1.0
			when (abs(edges_sum) <= 1e-12) and (abs(node_sum) <= 1e-12) then 0
			else abs( (node_sum - edges_sum) / node_sum )
		end as diff
	from w_edge_sum
)
select * from w_diff
where
	(' || include_null_diff || ' and ((diff >= ' || min_diff || ') or (diff is null)))
	or
	(not ' || include_null_diff || ' and (diff >= ' || min_diff || '))
order by diff desc
;
';

	--RAISE NOTICE '%', _complete_query;
	RETURN QUERY EXECUTE _complete_query;
END;
$BODY$
  LANGUAGE plpgsql STABLE
  COST 100
  ROWS 100000;

comment on function @extschema@.fn_add_aux_total_geo(integer[], double precision, boolean) is
'Function showing auxiliary total geographic additivity (aggregated class estimate should be equal to sum of sub-classes estimates). Hierarchy between aggregated classes and its sub-classes is defined in v_estimation_cell_hierarchy.
Function input argument is:
 * Aarray of esimation cells (FKEY to c_estimation_cell.id). All est. cells to be checked (aggregated class together with sub-classes) need to be passed.
 * Minimal amount of difference [%].
 * Whether include NULL difference -- indicating defined sub-classes missing in data.
Resulting table has following columns:
 * Auxiliary total attribute -- variable. FKEY to t_variable.id.
 * Auxiliary total estimation cell. FKEY to c_estimation_cell.id.
 * Aggregated class auxiliary total.
 * Sum of sub-classes auxiliary totals (belonging to aggregated class).
 * Estimation cells defined in hierarchy (v_estimation_cell_hierarchy). Array of FKEYs to c_estimation_cell.id.
 * Estimation cells found in data (t_result). Array of FKEYs to c_estimation_cell.id.
 * Relative difference between aggregated class auxiliary total and sum of sub-classes auxiliary totals.';

GRANT EXECUTE ON FUNCTION @extschema@.fn_add_aux_total_geo TO PUBLIC;
