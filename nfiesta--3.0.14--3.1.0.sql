--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

alter table @extschema@.t_target_data disable trigger trg__target_data__upd;
alter table @extschema@.t_target_data add column available_datasets integer;

with w_data as (
	select
		t_target_data.id as target_data__id,
		t_panel.id as panel,
		t_target_data.reference_year_set,
		t_target_data.variable
	from @extschema@.t_target_data
	inner join sdesign.f_p_plot on (t_target_data.plot = f_p_plot.gid)
	inner join sdesign.t_cluster on (f_p_plot.cluster = t_cluster.id)
	inner join sdesign.cm_cluster2panel_mapping on (t_cluster.id = cm_cluster2panel_mapping.cluster)
	inner join sdesign.t_panel on (cm_cluster2panel_mapping.panel = t_panel.id)
	inner join sdesign.cm_plot2cluster_config_mapping on (f_p_plot.gid = cm_plot2cluster_config_mapping.plot)
	inner join sdesign.t_cluster_configuration on (cm_plot2cluster_config_mapping.cluster_configuration = t_cluster_configuration.id)
	where t_panel.cluster_configuration = t_cluster_configuration.id
)
, w_data_all as (
	select
		target_data__id, t_available_datasets.id as t_available_datasets__id
	from w_data
	inner join @extschema@.t_available_datasets on (	w_data.panel = t_available_datasets.panel
								and w_data.reference_year_set = t_available_datasets.reference_year_set
								and w_data.variable = t_available_datasets.variable)
)
update @extschema@.t_target_data set available_datasets = w_data_all.t_available_datasets__id
from w_data_all
where t_target_data.id = w_data_all.target_data__id
;

alter table @extschema@.t_target_data alter column available_datasets set not null;
alter table @extschema@.t_target_data add constraint fkey__t_target_data__t_available_datasets FOREIGN KEY (available_datasets) REFERENCES @extschema@.t_available_datasets (id);
alter table @extschema@.t_target_data enable trigger trg__target_data__upd;

alter table @extschema@.t_target_data drop column reference_year_set;
alter table @extschema@.t_target_data drop column variable;

create unique index uidx__t_target_data__all_columns on @extschema@.t_target_data(plot, available_datasets) where is_latest = true;

---------------------------------------------------------------------------------------------------------------------------------

alter table @extschema@.t_auxiliary_data add column available_datasets integer;

with w_data as (
	select
		t_auxiliary_data.id as auxiliary_data__id,
		t_panel.id as panel,
		t_auxiliary_data.variable
	from @extschema@.t_auxiliary_data
	inner join sdesign.f_p_plot on (t_auxiliary_data.plot = f_p_plot.gid)
	inner join sdesign.t_cluster on (f_p_plot.cluster = t_cluster.id)
	inner join sdesign.cm_cluster2panel_mapping on (t_cluster.id = cm_cluster2panel_mapping.cluster)
	inner join sdesign.t_panel on (cm_cluster2panel_mapping.panel = t_panel.id)
	inner join sdesign.cm_plot2cluster_config_mapping on (f_p_plot.gid = cm_plot2cluster_config_mapping.plot)
	inner join sdesign.t_cluster_configuration on (cm_plot2cluster_config_mapping.cluster_configuration = t_cluster_configuration.id)
	where t_panel.cluster_configuration = t_cluster_configuration.id
)
, w_data_all as (
	select
		auxiliary_data__id, t_available_datasets.id as t_available_datasets__id
	from w_data
	inner join @extschema@.t_available_datasets on (	w_data.panel = t_available_datasets.panel
								and w_data.variable = t_available_datasets.variable)
)
update @extschema@.t_auxiliary_data set available_datasets = w_data_all.t_available_datasets__id
from w_data_all
where t_auxiliary_data.id = w_data_all.auxiliary_data__id
;

alter table @extschema@.t_auxiliary_data alter column available_datasets set not null;
alter table @extschema@.t_auxiliary_data add constraint fkey__t_auxiliary_data__t_available_datasets FOREIGN KEY (available_datasets) REFERENCES @extschema@.t_available_datasets (id);

alter table @extschema@.t_auxiliary_data drop column variable;

create unique index uidx__t_auxiliary_data__all_columns on @extschema@.t_auxiliary_data(plot, available_datasets) where is_latest = true;

---------------------------------------------------------------------------------------------------------------------------------

alter table @extschema@.t_available_datasets disable trigger trg__available_datasets__upd;
alter table @extschema@.t_available_datasets add column last_change timestamp with time zone;

with w_ttd as (
	select * from
        (select
                available_datasets,
                max(value_inserted) as last_change
        from @extschema@.t_target_data
        group by available_datasets
        order by available_datasets) as t1
	union all
	select * from
        (select
                available_datasets,
                max(value_inserted) as last_change
        from @extschema@.t_auxiliary_data
        group by available_datasets
        order by available_datasets) as t2
)
update
        @extschema@.t_available_datasets SET last_change = w_ttd.last_change
from w_ttd where t_available_datasets.id = w_ttd.available_datasets;

alter table @extschema@.t_available_datasets alter column last_change set not null;
alter table @extschema@.t_available_datasets enable trigger trg__available_datasets__upd;

---------------------------------------------------------------------------------------------------------------------------------

-- <function name="fn_etl_import_ldsity_values" schema="extschema" src="functions/extschema/etl/fn_etl_import_ldsity_values.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_import_ldsity_values(json, double precision)

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_import_ldsity_values(json, double precision) CASCADE;

create or replace function @extschema@.fn_etl_import_ldsity_values
(
	_ldsity_values json,
	_threshold double precision default 0.000001
)
returns text
as
$$
declare
		_max_id_ttd								integer;
		_max_id_tad								integer;
		_target_variable						integer;
		_country								varchar;
		_strata_sets							varchar[];
		_stratums								varchar[];
		_panels									varchar[];
		_reference_year_sets					varchar[];
		_inventory_campaigns					varchar[];
		_clusters								varchar[];
		_cluster_configurations					varchar[];
		_array_target_variable					integer[];
		_array_sub_population_category			integer[];
		_array_area_domain_category				integer[];
		_array_panel							varchar[];
		_array_reference_year_set				varchar[];
		_check_cmrys2pm							integer;
		_array_cmrys2pm							integer[];
		_check_count_records_input_json			integer;
		_check_count_records_after_inner_join	integer;
		_res_ttd								text;
		_res_tad								text;
		_res									text;
begin
		if _ldsity_values is null
		then
			raise exception 'Error 01: fn_etl_import_ldsity_values: Input argument _ldsity_values must not by NULL!';
		end if;
	
		_max_id_ttd := (select coalesce(max(id),0) from @extschema@.t_target_data);
		_max_id_tad := (select coalesce(max(id),0) from @extschema@.t_available_datasets);

		---------------------------------------------------------------------------------
		-- check target variable --
		with
		w1 as	(select json_array_elements(_ldsity_values) as s)
		,w2 as	(select	(s->>'target_variable')::integer as target_variable	from w1)
		select distinct w2.target_variable from w2
		into _target_variable;

		if (select count(ctv.*) is distinct from 1 from @extschema@.c_target_variable as ctv where ctv.id = _target_variable)
		then
			raise exception 'Error 02: fn_etl_import_ldsity_values: Input JSON element "target_variable = %" is not present in table c_target_variable!',_target_variable;
		end if;
		---------------------------------------------------------------------------------
		-- check country --
		with
		w1 as	(select json_array_elements(_ldsity_values) as s)
		,w2 as	(select	(s->>'country')::varchar as country	from w1)
		select distinct w2.country from w2
		into _country;

		if (select count(cc.*) is distinct from 1 from sdesign.c_country as cc where cc.label = _country)
		then
			raise exception 'Error 03: fn_etl_import_ldsity_values: Input JSON element "country = %" is not present in table sdesign.c_country!',_country;
		end if;
		---------------------------------------------------------------------------------
		-- check strata sets --
		with
		w1 as	(select json_array_elements(_ldsity_values) as s)
		,w2 as	(select	(s->>'strata_set')::varchar as strata_set from w1)
		,w3 as	(select distinct w2.strata_set from w2)
		select array_agg(w3.strata_set order by w3.strata_set) from w3
		into _strata_sets;

		for i in 1..array_length(_strata_sets,1)
		loop
			if	(
				select count(tss.*) is distinct from 1 from sdesign.t_strata_set as tss where tss.country =
				(select cc.id from sdesign.c_country as cc where cc.label = _country)
				and tss.strata_set = _strata_sets[i]
				)
			then
				raise exception 'Error 04: fn_etl_import_ldsity_values: Some of input JSON element "strata_set = %" is not present in table sdesign.t_strata for input JSON element "country = %"!',_strata_sets[i], _country;
			end if;		
		end loop;
		---------------------------------------------------------------------------------
		-- check stratums --
			with
		w1 as	(select json_array_elements(_ldsity_values) as s)
		,w2 as	(select	(s->>'stratum')::varchar as stratum from w1)
		,w3 as	(select distinct w2.stratum from w2)
		select array_agg(w3.stratum order by w3.stratum) from w3
		into _stratums;

		for i in 1..array_length(_stratums,1)
		loop
			if	(
				select count(ts.*) is distinct from 1 from sdesign.t_stratum as ts where ts.strata_set in
				(select tss.id from sdesign.t_strata_set as tss where tss.country =
				(select cc.id from sdesign.c_country as cc where cc.label = _country))
				and ts.stratum = _stratums[i]
				)
			then
				raise exception 'Error 05: fn_etl_import_ldsity_values: Some of input JSON element "stratum = %" is not present in table sdesign.t_stratum for input JSON element "country = %"!',_stratums[i], _country;
			end if;	
		end loop;
		---------------------------------------------------------------------------------
		-- check panels --
		with
		w1 as	(select json_array_elements(_ldsity_values) as s)
		,w2 as	(select	(s->>'panel')::varchar as panel	from w1)
		,w3 as	(select distinct w2.panel from w2)
		select array_agg(w3.panel order by w3.panel) from w3
		into _panels;

		for i in 1..array_length(_panels,1)
		loop
			if	(
				select count(tp.*) is distinct from 1 from sdesign.t_panel as tp where tp.stratum in
				(select ts.id from sdesign.t_stratum as ts where ts.strata_set in
				(select tss.id from sdesign.t_strata_set as tss where tss.country = 
				(select cc.id from sdesign.c_country as cc where cc.label = _country)))
				and tp.panel = _panels[i]
				)
			then
				raise exception 'Error 06: fn_etl_import_ldsity_values: Some of input JSON element "panel = %" is not present in table sdesign.t_panel for input JSON element "country = %"!',_panel[i], _country;
			end if;		
		end loop;
		---------------------------------------------------------------------------------
		-- check reference year sets --
		with
		w1 as	(select json_array_elements(_ldsity_values) as s)
		,w2 as	(select	(s->>'reference_year_set')::varchar as reference_year_set from w1)
		,w3 as	(select distinct w2.reference_year_set from w2)
		select array_agg(w3.reference_year_set order by w3.reference_year_set) from w3
		into _reference_year_sets;

		for i in 1..array_length(_reference_year_sets,1)
		loop
			if	(
				select count(trys.*) is distinct from 1 from sdesign.t_reference_year_set as trys where trys.id in
				(select cmr.reference_year_set from sdesign.cm_refyearset2panel_mapping as cmr where cmr.panel in
				(select tp.id from sdesign.t_panel as tp where tp.stratum in
				(select ts.id from sdesign.t_stratum as ts where ts.strata_set in
				(select tss.id from sdesign.t_strata_set as tss where tss.country = 
				(select cc.id from sdesign.c_country as cc where cc.label = _country)))))
				and trys.reference_year_set = _reference_year_sets[i]
				)
			then
				raise exception 'Error 07: fn_etl_import_ldsity_values: Some of input JSON element "reference_year_set = %" is not present in table sdesign.t_reference_year_set for input JSON element "country = %"!',_reference_year_sets[i], _country;
			end if;		
		end loop;
		---------------------------------------------------------------------------------
		-- check inventory campaigns --
		with
		w1 as	(select json_array_elements(_ldsity_values) as s)
		,w2 as	(select	(s->>'inventory_campaign')::varchar as inventory_campaign from w1)
		,w3 as	(select distinct w2.inventory_campaign from w2)
		select array_agg(w3.inventory_campaign order by w3.inventory_campaign) from w3
		into _inventory_campaigns;

		for i in 1..array_length(_inventory_campaigns,1)
		loop
			if	(
				select count(tic.*) is distinct from 1 from sdesign.t_inventory_campaign as tic where tic.id in
				(select trys.inventory_campaign from sdesign.t_reference_year_set as trys where trys.id in
				(select cmr.reference_year_set from sdesign.cm_refyearset2panel_mapping as cmr where cmr.panel in
				(select tp.id from sdesign.t_panel as tp where tp.stratum in
				(select ts.id from sdesign.t_stratum as ts where ts.strata_set in
				(select tss.id from sdesign.t_strata_set as tss where tss.country = 
				(select cc.id from sdesign.c_country as cc where cc.label = _country))))))
				and tic.inventory = _inventory_campaigns[i]
				)
			then
				raise exception 'Error 08: fn_etl_import_ldsity_values: Some of input JSON element "inventory_campaign = %" is not present in table sdesign.t_inventory_campaign for input JSON element "country = %"!',_inventory_campaigns[i], _country;
			end if;		
		end loop;
		---------------------------------------------------------------------------------
		-- check clusters --
		with
		w1 as	(select json_array_elements(_ldsity_values) as s)
		,w2 as	(select	(s->>'cluster')::varchar as cluster from w1)
		,w3 as	(select distinct w2.cluster from w2)
		select array_agg(w3.cluster order by w3.cluster) from w3
		into _clusters;

		for i in 1..array_length(_clusters,1)
		loop
			if	(
				select count(tc.*) is distinct from 1 from sdesign.t_cluster as tc where tc.id in
				(select ccpm.cluster from sdesign.cm_cluster2panel_mapping as ccpm where ccpm.panel in
				(select tp.id from sdesign.t_panel as tp where tp.stratum in
				(select ts.id from sdesign.t_stratum as ts where ts.strata_set in
				(select tss.id from sdesign.t_strata_set as tss where tss.country = 
				(select cc.id from sdesign.c_country as cc where cc.label = _country)))))
				and tc.cluster = _clusters[i]
				)
			then
				raise exception 'Error 09: fn_etl_import_ldsity_values: Some of input JSON element "cluster = %" is not present in table sdesign.t_cluster for input JSON element "country = %"!',_clusters[i], _country;
			end if;		
		end loop;
		---------------------------------------------------------------------------------
		-- check cluster configurations --
		with
		w1 as	(select json_array_elements(_ldsity_values) as s)
		,w2 as	(select	(s->>'cluster_configuration')::varchar as cluster_configuration from w1)
		,w3 as	(select distinct w2.cluster_configuration from w2)
		select array_agg(w3.cluster_configuration order by w3.cluster_configuration) from w3
		into _cluster_configurations;

		for i in 1..array_length(_cluster_configurations,1)
		loop
			if	(
				select count(tcc.*) is distinct from 1 from sdesign.t_cluster_configuration as tcc where tcc.id in
				(select tp.cluster_configuration from sdesign.t_panel as tp where tp.stratum in
				(select ts.id from sdesign.t_stratum as ts where ts.strata_set in
				(select tss.id from sdesign.t_strata_set as tss where tss.country = 
				(select cc.id from sdesign.c_country as cc where cc.label = _country))))
				and tcc.cluster_configuration = _cluster_configurations[i]
				)
			then
				raise exception 'Error 10: fn_etl_import_ldsity_values: Some of input JSON element "cluster_configuration = %" is not present in table sdesign.t_cluster_configuration for input JSON element "country = %"!',_cluster_configurations[i], _country;
			end if;		
		end loop;
		---------------------------------------------------------------------------------
		-- check combinations of target_variable, sub_population_category and area_domain_category --
		with
		w1 as	(
				select json_array_elements(_ldsity_values) as s
				)
		,w2 as	(
				select
						(s->>'target_variable')::integer			as target_variable,
						(s->>'sub_population_category')::integer	as sub_population_category,
						(s->>'area_domain_category')::integer		as area_domain_category	
				from w1
				)
		,w3 as	(select distinct target_variable, sub_population_category, area_domain_category from w2)
		select
				array_agg(w3.target_variable order by w3.sub_population_category, w3.area_domain_category) as target_variable,
				array_agg(w3.sub_population_category order by w3.sub_population_category, w3.area_domain_category) as sub_population_category,
				array_agg(w3.area_domain_category order by w3.sub_population_category, w3.area_domain_category) as area_domain_category
		from
				w3
		into
				_array_target_variable,
				_array_sub_population_category,
				_array_area_domain_category;

		for i in 1..array_length(_array_sub_population_category,1)
		loop
			if	(
				with
				w1 as	(
						select
								id,
								target_variable,
								case when sub_population_category is null then 0 else sub_population_category end as sub_population_category,
								case when area_domain_category is null then 0 else area_domain_category end as area_domain_category
						from
								@extschema@.t_variable where target_variable = _array_target_variable[i]
						)
				select count(w1.*) is distinct from 1 from w1
				where w1.sub_population_category = _array_sub_population_category[i]
				and w1.area_domain_category = _array_area_domain_category[i]
				)
			then
				raise exception 'Error 11: fn_etl_import_ldsity_values: Some of input combination JSON elements "target_variable = %", "sub_population_category = %" and "area_domain_category = %" is not present in table t_variable!',_array_target_variable[i], _array_sub_population_category[i], _array_area_domain_category[i];
			end if;
		end loop;
		---------------------------------------------------------------------------------
		-- check combinations of panel and reference year set
		with
		w1 as	(
				select json_array_elements(_ldsity_values) as s
				)
		,w2 as	(
				select
						(s->>'panel')::varchar				as panel,
						(s->>'reference_year_set')::varchar	as reference_year_set
				from w1
				)
		,w3 as	(select distinct panel, reference_year_set from w2)
		select
				array_agg(w3.panel order by w3.panel, w3.reference_year_set) as panel,
				array_agg(w3.reference_year_set order by w3.panel, w3.reference_year_set) as reference_year_set
		from
				w3
		into
				_array_panel,
				_array_reference_year_set;

		for i in 1..array_length(_array_panel,1)
		loop
			select cmrys.id from sdesign.cm_refyearset2panel_mapping as cmrys
			where cmrys.panel =	(
							select tp.id from sdesign.t_panel as tp where tp.stratum in
							(select ts.id from sdesign.t_stratum as ts where ts.strata_set in
							(select tss.id from sdesign.t_strata_set as tss where tss.country = 
							(select cc.id from sdesign.c_country as cc where cc.label = _country)))
							and tp.panel = _array_panel[i]
							)
			and cmrys.reference_year_set =	(
											select trys.id from sdesign.t_reference_year_set as trys where trys.id in
											(select cmr.reference_year_set from sdesign.cm_refyearset2panel_mapping as cmr where cmr.panel in
											(select tp.id from sdesign.t_panel as tp where tp.stratum in
											(select ts.id from sdesign.t_stratum as ts where ts.strata_set in
											(select tss.id from sdesign.t_strata_set as tss where tss.country = 
											(select cc.id from sdesign.c_country as cc where cc.label = _country)))))
											and trys.reference_year_set = _array_reference_year_set[i]
											)
			into _check_cmrys2pm;

			if _check_cmrys2pm is null
			then
				raise exception 'Error 12: fn_etl_import_ldsity_values: Some of input combination JSON elements "panel = %" and "reference_year_set = %" is not present in table sdesign.cm_refyearset2panel_mapping for input JSON element "country = %"!',_array_panel[i], _array_reference_year_set[i], _country;
			end if;

			if i = 1
			then
				_array_cmrys2pm := array[_check_cmrys2pm];
			else
				_array_cmrys2pm := _array_cmrys2pm || array[_check_cmrys2pm];
			end if;
		end loop;
		---------------------------------------------------------------------------------
		-- get count of records in input JSON
		with
		w1 as	(
				select json_array_elements(_ldsity_values) as s
				)
		,w2 as	(
				select
						(s->>'target_variable')::integer			as target_variable,
						(s->>'country')::varchar					as country,
						(s->>'strata_set')::varchar					as strata_set,
						(s->>'stratum')::varchar					as stratum,
						(s->>'reference_year_set')::varchar			as reference_year_set,
						(s->>'panel')::varchar						as panel,
						(s->>'cluster')::varchar					as cluster,
						(s->>'cluster_configuration')::varchar		as cluster_configuration,
						(s->>'inventory_campaign')::varchar			as inventory_campaign,
						(s->>'plot')::varchar						as plot
				from w1
				)
		select count(w2.*) from w2
		into _check_count_records_input_json;
		---------------------------------------------------------------------------------
		-- get count of records after inner join with sdesign for given combinations of panels and reference year sets
		with
		w1 as	(
				select json_array_elements(_ldsity_values) as s
				)
		,w2 as	(
				select
						(s->>'target_variable')::integer			as target_variable,
						(s->>'country')::varchar					as country,
						(s->>'strata_set')::varchar					as strata_set,
						(s->>'stratum')::varchar					as stratum,
						(s->>'reference_year_set')::varchar			as reference_year_set,
						(s->>'panel')::varchar						as panel,
						(s->>'cluster')::varchar					as cluster,
						(s->>'cluster_configuration')::varchar		as cluster_configuration,
						(s->>'inventory_campaign')::varchar			as inventory_campaign,
						(s->>'plot')::varchar						as plot
				from w1
				)
		,w3 as	(
				select		
						t_panel.id as t_panel__id,
						t_panel.panel as t_panel__panel,
						t_cluster.id as t_cluster__id,
						t_cluster.cluster as t_cluster__cluster,
						f_p_plot.gid as f_p_plot__gid,
						f_p_plot.plot as f_p_plot__plot,
						t_cluster_configuration.id as t_cluster_configuration__id,
						t_cluster_configuration.cluster_configuration as t_cluster_configuration__cluster_configuration,
						t_stratum.id as t_stratum__id,
						t_stratum.stratum as t_stratum__stratum,
						t_strata_set.id as t_strata_set__id,
						t_strata_set.strata_set as t_strata_set__strata_set,
						c_country.id as c_country__id,
						c_country.label as c_country__label,
						t_reference_year_set.id as t_reference_year_set__id,
						t_reference_year_set.reference_year_set as t_reference_year_set__reference_year_set,
						t_inventory_campaign.id as t_inventory_campaign__id,
						t_inventory_campaign.inventory as t_inventory_campaign__inventory
				from
							sdesign.cm_refyearset2panel_mapping
				inner join	sdesign.t_panel on t_panel.id = cm_refyearset2panel_mapping.panel
				inner join	sdesign.cm_cluster2panel_mapping on cm_cluster2panel_mapping.panel = t_panel.id
				inner join	sdesign.t_cluster on t_cluster.id = cm_cluster2panel_mapping."cluster"
				inner join	sdesign.f_p_plot on f_p_plot."cluster" = t_cluster.id
				inner join	sdesign.cm_plot2cluster_config_mapping on f_p_plot.gid = cm_plot2cluster_config_mapping.plot
				inner join	sdesign.t_cluster_configuration on (t_panel.cluster_configuration = t_cluster_configuration.id and cm_plot2cluster_config_mapping.cluster_configuration = t_cluster_configuration.id)
				inner join	sdesign.t_stratum on t_panel.stratum = t_stratum.id
				inner join	sdesign.t_strata_set on t_stratum.strata_set = t_strata_set.id
				inner join	sdesign.c_country on t_strata_set.country = c_country.id
				inner join	sdesign.t_reference_year_set on cm_refyearset2panel_mapping.reference_year_set = t_reference_year_set.id
				inner join	sdesign.t_inventory_campaign on t_reference_year_set.inventory_campaign = t_inventory_campaign.id
				where
						cm_refyearset2panel_mapping.id in (select unnest(_array_cmrys2pm))
				)
		,w4 as	(
				select
						w2.*, w3.f_p_plot__gid
				from
						w2 inner join w3
						on w2.plot = w3.f_p_plot__plot
						and w2.cluster = w3.t_cluster__cluster
						and w2.panel = w3.t_panel__panel
						and w2.stratum = w3.t_stratum__stratum
						and w2.reference_year_set = w3.t_reference_year_set__reference_year_set
						and w2.inventory_campaign = w3.t_inventory_campaign__inventory
						and w2.cluster_configuration = w3.t_cluster_configuration__cluster_configuration
						and w2.strata_set = w3.t_strata_set__strata_set
						and w2.country = w3.c_country__label
				)
		select count(w4.*) from w4
		into _check_count_records_after_inner_join;
		---------------------------------------------------------------------------------
		-- check records
		if _check_count_records_input_json is distinct from _check_count_records_after_inner_join
		then
				raise exception 'Error 13: fn_etl_import_ldsity_values: Composite key in input JSON elements is not compatible with SDESIGN on target DB!';
		end if;
		---------------------------------------------------------------------------------
		---------------------------------------------------------------------------------
		-- RES QUERY --
		with
		w1 as	(
				select json_array_elements(_ldsity_values) as s
				)
		,w2 as	(
				select
						(s->>'target_variable')::integer			as target_variable,
						(s->>'country')::varchar					as country,
						(s->>'strata_set')::varchar					as strata_set,
						(s->>'stratum')::varchar					as stratum,
						(s->>'reference_year_set')::varchar			as reference_year_set,
						(s->>'panel')::varchar						as panel,
						(s->>'cluster')::varchar					as cluster,
						(s->>'cluster_configuration')::varchar		as cluster_configuration,
						(s->>'inventory_campaign')::varchar			as inventory_campaign,
						(s->>'plot')::varchar						as plot,
						(s->>'sub_population_category')::integer	as sub_population_category,
						(s->>'area_domain_category')::integer		as area_domain_category,
						(s->>'value')::float						as value
				from w1
				)
		,w3 as	(
				select		
						t_panel.id as t_panel__id,
						t_panel.panel as t_panel__panel,
						t_cluster.id as t_cluster__id,
						t_cluster.cluster as t_cluster__cluster,
						f_p_plot.gid as f_p_plot__gid,
						f_p_plot.plot as f_p_plot__plot,
						t_cluster_configuration.id as t_cluster_configuration__id,
						t_cluster_configuration.cluster_configuration as t_cluster_configuration__cluster_configuration,
						t_stratum.id as t_stratum__id,
						t_stratum.stratum as t_stratum__stratum,
						t_strata_set.id as t_strata_set__id,
						t_strata_set.strata_set as t_strata_set__strata_set,
						c_country.id as c_country__id,
						c_country.label as c_country__label,
						t_reference_year_set.id as t_reference_year_set__id,
						t_reference_year_set.reference_year_set as t_reference_year_set__reference_year_set,
						t_inventory_campaign.id as t_inventory_campaign__id,
						t_inventory_campaign.inventory as t_inventory_campaign__inventory
				from
							sdesign.cm_refyearset2panel_mapping
				inner join	sdesign.t_panel on t_panel.id = cm_refyearset2panel_mapping.panel
				inner join	sdesign.cm_cluster2panel_mapping on cm_cluster2panel_mapping.panel = t_panel.id
				inner join	sdesign.t_cluster on t_cluster.id = cm_cluster2panel_mapping."cluster"
				inner join	sdesign.f_p_plot on f_p_plot."cluster" = t_cluster.id
				inner join	sdesign.cm_plot2cluster_config_mapping on f_p_plot.gid = cm_plot2cluster_config_mapping.plot
				inner join	sdesign.t_cluster_configuration on (t_panel.cluster_configuration = t_cluster_configuration.id and cm_plot2cluster_config_mapping.cluster_configuration = t_cluster_configuration.id)
				inner join	sdesign.t_stratum on t_panel.stratum = t_stratum.id
				inner join	sdesign.t_strata_set on t_stratum.strata_set = t_strata_set.id
				inner join	sdesign.c_country on t_strata_set.country = c_country.id
				inner join	sdesign.t_reference_year_set on cm_refyearset2panel_mapping.reference_year_set = t_reference_year_set.id
				inner join	sdesign.t_inventory_campaign on t_reference_year_set.inventory_campaign = t_inventory_campaign.id
				where
						cm_refyearset2panel_mapping.id in (select unnest(_array_cmrys2pm))
				)		
		,w4 as	(
				select
						w2.*,
						w3.f_p_plot__gid,
						w3.t_panel__id,
						w3.t_reference_year_set__id
				from
						w2 inner join w3
						on w2.plot = w3.f_p_plot__plot
						and w2.cluster = w3.t_cluster__cluster
						and w2.panel = w3.t_panel__panel
						and w2.stratum = w3.t_stratum__stratum
						and w2.reference_year_set = w3.t_reference_year_set__reference_year_set
						and w2.inventory_campaign = w3.t_inventory_campaign__inventory
						and w2.cluster_configuration = w3.t_cluster_configuration__cluster_configuration
						and w2.strata_set = w3.t_strata_set__strata_set
						and w2.country = w3.c_country__label
				)
		,w5 as	(
				select
						id,
						target_variable,
						case when sub_population_category is null then 0 else sub_population_category end as sub_population_category,
						case when area_domain_category is null then 0 else area_domain_category end as area_domain_category
				from
						@extschema@.t_variable 
				where target_variable = (select distinct w2.target_variable from w2)
				)
		,w6 as	(
				select
						w4.target_variable,
						w4.f_p_plot__gid as plot,
						w4.value,
						w4.t_panel__id as panel,
						w4.t_reference_year_set__id as reference_year_set,
						w5.id as variable
				from
						w4 inner join w5 on (w4.sub_population_category = w5.sub_population_category and w4.area_domain_category = w5.area_domain_category)
				)
		,w7 as	(
				select distinct w6.target_variable, w6.plot, w6.panel, w6.value, w6.reference_year_set, w6.variable from w6
				)
		,w8 as	(
				select distinct w6.panel, w6.reference_year_set, w6.variable from w6
				except
				select panel, reference_year_set, variable from @extschema@.t_available_datasets
				)
		,w9 as	(
				insert into @extschema@.t_available_datasets(panel,reference_year_set,variable,last_change)
				select w8.panel, w8.reference_year_set, w8.variable, now() from w8
				order by w8.panel, w8.reference_year_set, w8.variable
				returning t_available_datasets.id, t_available_datasets.panel, t_available_datasets.reference_year_set, t_available_datasets.variable
				)
		,w_tad_u_w9 as (
			select id, panel, reference_year_set, variable from @extschema@.t_available_datasets 
			union all 
			select id, panel, reference_year_set, variable from w9
		)
		,w10 as (
			select w7.*, w_tad_u_w9.id as available_datasets
			from w7
			inner join w_tad_u_w9 on (w7.panel = w_tad_u_w9.panel and w7.reference_year_set = w_tad_u_w9.reference_year_set and w7.variable = w_tad_u_w9.variable)
		)
		,w_ttd as	(
					select
							ttd.id,
							ttd.plot,
							(	select t_panel.id as panel_upr
								from 		sdesign.f_p_plot
								inner join 	sdesign.t_cluster on (f_p_plot.cluster = t_cluster.id)
								inner join	sdesign.cm_cluster2panel_mapping on (t_cluster.id = cm_cluster2panel_mapping.cluster)
								inner join	sdesign.t_panel on (cm_cluster2panel_mapping.panel = t_panel.id)
								inner join	sdesign.t_cluster_configuration on (t_panel.cluster_configuration = t_cluster_configuration.id)
								inner join	sdesign.cm_plot2cluster_config_mapping on (t_cluster_configuration.id = cm_plot2cluster_config_mapping.cluster_configuration and
																f_p_plot.gid = cm_plot2cluster_config_mapping.plot)
								where f_p_plot.gid = coalesce(w10.plot,ttd.plot)
							),
							ttd.value,
							coalesce(w10.plot,ttd.plot) as plot_upr,
							coalesce(w10.available_datasets, ttd.available_datasets) as available_datasets_upr,
							case
								when ttd.value is     null and w10.value is not null then w10.value
								when ttd.value is not null and ttd.value is distinct from 0.0 and w10.value is null then 0.0
								when ttd.value is not null and ttd.value = 0.0 and w10.value is null then null::double precision
								else w10.value
							end
								as value_upr,
							case
								when ttd.value is     null and w10.value is not null then false
								when ttd.value is not null and ttd.value is distinct from 0.0 and w10.value is null then false
								when ttd.value is not null and ttd.value = 0.0 and w10.value is null then null::boolean									
								else
									case
										when ttd.value = 0.0 and w10.value = 0.0 then true
										when ttd.value = 0.0 and w10.value is distinct from 0.0
												then
													case
														when (abs(1 - (ttd.value / w10.value)) * 100.0) <= _threshold then true
														else false
													end
										when ttd.value is distinct from 0.0 and w10.value = 0.0
												then
													case
														when (abs(1 - (w10.value / ttd.value)) * 100.0) <= _threshold then true
														else false
													end
										else
											case
												when (abs(1 - (ttd.value / w10.value)) * 100.0) <= _threshold then true
												else false
											end
									end									
							end
								as value_identic				
					from
						(
						select * 
						from @extschema@.t_target_data
						where available_datasets in (
							select id 
							from @extschema@.t_available_datasets 
							where panel in (select distinct panel from w10)
							and variable in (select id from @extschema@.t_variable where target_variable = (select distinct w2.target_variable from w2))
							and reference_year_set in (select distinct w4.t_reference_year_set__id from w4)
						)
						and plot in (select distinct w10.plot from w10)
						and is_latest = true
						) as ttd
					full outer join w10
							on (ttd.plot = w10.plot and ttd.available_datasets = w10.available_datasets)
					)
		,w_update as	(
						update @extschema@.t_target_data set is_latest = false where id in
						(select id from w_ttd where value_identic = false and id is not null)
						returning t_target_data.plot, t_target_data.available_datasets
						)
		insert into @extschema@.t_target_data(plot,value,value_inserted,is_latest,available_datasets)
		select
				w_ttd.plot_upr as plot,
				w_ttd.value_upr as value,
				now() as value_inserted,
				true as is_latest,
				available_datasets_upr
		from
				w_ttd
				left join w_update on w_ttd.plot_upr = w_update.plot and w_ttd.available_datasets_upr = w_update.available_datasets
		where
				w_ttd.value_identic = false
		order
				by w_ttd.plot_upr, w_ttd.available_datasets_upr;

	_res_ttd := concat('The ',(select count(*) from @extschema@.t_target_data where id > _max_id_ttd),' new local densities were inserted for ',(select count(t.plot) from (select distinct plot from @extschema@.t_target_data where id > _max_id_ttd) as t),' plots.');
	_res_tad := concat('The ',(select count(*) from @extschema@.t_available_datasets where id > _max_id_tad),' new rows were inserted into t_available_datasets.');
			
	_res := _res_ttd || ' ' || _res_tad;

	return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_import_ldsity_values(json, double precision) IS
'The function for the specified list of input arguments inserts data into the t_available_datasets table and inserts data into the t_target_data table (aggregated local density at the plot level).';

grant execute on function @extschema@.fn_etl_import_ldsity_values(json, double precision) to public;

-- </function>

drop function @extschema@.fn_check_t_target_data() cascade;

-- <function name="fn_add_triggers" schema="extschema" src="functions/extschema/additivity/fn_add_triggers.sql">
--
-- Copyright 2021, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

CREATE OR REPLACE FUNCTION @extschema@.fn_prevent_update()
  RETURNS trigger AS
$BODY$
    BEGIN
        RAISE EXCEPTION 'fn_prevent_update -- UPDATE -- only update of is_latests is possible';
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

CREATE TRIGGER trg__target_data__pr_upd
BEFORE UPDATE OF id, plot, value, value_inserted, available_datasets -- only update of is_latests is possible
ON @extschema@.t_target_data
FOR EACH ROW
EXECUTE PROCEDURE @extschema@.fn_prevent_update();

--------------------------------------------------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION @extschema@.fn_check_update()
  RETURNS trigger AS
$BODY$
    BEGIN
	IF (old.is_latest = false AND new.is_latest = true) THEN
	        RAISE EXCEPTION 'fn_check_update -- UPDATE -- only set is_latest to false is possible. use INSERT for new valid values';
	END IF;
	RETURN NEW;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

CREATE TRIGGER trg__target_data__ch_upd
AFTER UPDATE OF is_latest
ON @extschema@.t_target_data
FOR EACH ROW
EXECUTE PROCEDURE @extschema@.fn_check_update();

--------------------------------------------------------------------------------------------------------------------------------

--drop function @extschema@.fn_update_last_change() cascade;
CREATE OR REPLACE FUNCTION @extschema@.fn_update_last_change() RETURNS TRIGGER AS $src$
    DECLARE
    BEGIN
	IF (TG_TABLE_NAME = 't_target_data' or TG_TABLE_NAME = 't_auxiliary_data') THEN
        	IF (TG_OP = 'DELETE') THEN
			RAISE EXCEPTION 'fn_update_last_change -- DELETE not permited (UPDATE to is_latest = false instead)';
		ELSIF (TG_OP = 'UPDATE') THEN
			with w_data as (
				select 
					available_datasets, now() as max_val_upd
				from trans_table
				group by available_datasets
			)
			update @extschema@.t_available_datasets 
			set last_change = w_data.max_val_upd
			from w_data
			where t_available_datasets.id = w_data.available_datasets
			;
			/*raise notice '%		fn_update_last_change -- % -- %: () %', 
				clock_timestamp()::timestamp with time zone at time zone 'Europe/Prague', 
				TG_TABLE_NAME, TG_OP, 
				(select json_agg(r) 
					from (
						select
							json_build_object(
								'available_datasets', available_datasets, 
								'max_val_upd', now()
							) as r
						from trans_table
						group by available_datasets
					) as foo
				);*/
		ELSIF (TG_OP = 'INSERT') THEN
			with w_data as (
				select 
					available_datasets, max(value_inserted) as max_val_ins
				from trans_table
				group by available_datasets
			)
			update @extschema@.t_available_datasets 
			set last_change = w_data.max_val_ins
			from w_data
			where t_available_datasets.id = w_data.available_datasets
			;
			/*raise notice '%		fn_update_last_change -- % -- %: () %', 
				clock_timestamp()::timestamp with time zone at time zone 'Europe/Prague', 
				TG_TABLE_NAME, TG_OP, 
				(select json_agg(r)
					from (
						select 
							json_build_object(
								'available_datasets', available_datasets, 
								'max_val_ins', max(value_inserted)
						) as r
						from trans_table
						group by available_datasets
					) as foo
				);*/
		ELSE
			RAISE EXCEPTION 'fn_update_last_change -- trigger operation not known: %', TG_OP;
		END IF;
	ELSE
		RAISE EXCEPTION 'fn_update_last_change -- % -- table name not known: %', TG_OP, TG_TABLE_NAME;
	END IF;
        RETURN NULL; -- result is ignored since this is an AFTER trigger
    END;
$src$ LANGUAGE plpgsql;

CREATE TRIGGER trg__target_data__ins
    AFTER INSERT ON @extschema@.t_target_data
    REFERENCING NEW TABLE AS trans_table
    FOR EACH STATEMENT EXECUTE FUNCTION @extschema@.fn_update_last_change();

CREATE TRIGGER trg__target_data__upd
    AFTER UPDATE ON @extschema@.t_target_data
    REFERENCING NEW TABLE AS trans_table
    FOR EACH STATEMENT EXECUTE FUNCTION @extschema@.fn_update_last_change();

CREATE TRIGGER trg__target_data__del
    AFTER DELETE ON @extschema@.t_target_data
    REFERENCING OLD TABLE AS trans_table
    FOR EACH STATEMENT EXECUTE FUNCTION @extschema@.fn_update_last_change();

---------------------------------------------
CREATE TRIGGER trg__auxiliary_data__ins
    AFTER INSERT ON @extschema@.t_auxiliary_data
    REFERENCING NEW TABLE AS trans_table
    FOR EACH STATEMENT EXECUTE FUNCTION @extschema@.fn_update_last_change();

CREATE TRIGGER trg__auxiliary_data__upd
    AFTER UPDATE ON @extschema@.t_auxiliary_data
    REFERENCING NEW TABLE AS trans_table
    FOR EACH STATEMENT EXECUTE FUNCTION @extschema@.fn_update_last_change();

CREATE TRIGGER trg__auxiliary_data__del
    AFTER DELETE ON @extschema@.t_auxiliary_data
    REFERENCING OLD TABLE AS trans_table
    FOR EACH STATEMENT EXECUTE FUNCTION @extschema@.fn_update_last_change();

--------------------------------------------------------------------------------------------------------------------------------

--drop function @extschema@.fn_launch_add_check() cascade;
CREATE OR REPLACE FUNCTION @extschema@.fn_launch_add_check() RETURNS TRIGGER AS $src$
    DECLARE
		_vars int[];
		_vars_add_set int[];
		_plots int[];
		_refyearsets int[];
		_errp json;
    BEGIN
        IF (TG_OP = 'DELETE' or TG_OP = 'UPDATE' or TG_OP = 'INSERT') THEN
			IF (TG_TABLE_NAME = 't_available_datasets') THEN
				------------------ _vars_add_set
				with w_vars_add_set as (
						select array_prepend(node, edges) as vs
					from @extschema@.t_available_datasets
					inner join @extschema@.v_variable_hierarchy_plot
						on ((t_available_datasets.variable = v_variable_hierarchy_plot.node
							or t_available_datasets.variable    = any (v_variable_hierarchy_plot.edges))
						and t_available_datasets.panel              = v_variable_hierarchy_plot.panel
						and t_available_datasets.reference_year_set = v_variable_hierarchy_plot.reference_year_set
						)
					where t_available_datasets.id = new.id
				)
				, w_vars_add_set_un as (
						select unnest(vs) as v from w_vars_add_set
				)
				select
					array_agg(distinct v)		into _vars_add_set
				from w_vars_add_set_un;
				------------------ _plots
				select 
					array_agg(distinct f_p_plot.gid)	into _plots		
				from @extschema@.t_available_datasets
				inner join sdesign.t_panel 				on (t_panel.id = t_available_datasets.panel)
				inner join sdesign.cm_cluster2panel_mapping 		on (t_panel.id = cm_cluster2panel_mapping.panel)
				inner join sdesign.t_cluster 				on (t_cluster.id = cm_cluster2panel_mapping.cluster)
				inner join sdesign.t_cluster_configuration 		on (t_panel.cluster_configuration = t_cluster_configuration.id)
				inner join sdesign.cm_plot2cluster_config_mapping 	on (t_cluster_configuration.id = cm_plot2cluster_config_mapping.cluster_configuration)
				inner join sdesign.f_p_plot 				on (f_p_plot.cluster = t_cluster.id and cm_plot2cluster_config_mapping.plot = f_p_plot.gid)
				where t_available_datasets.id = new.id;
				------------------ _refyearsets
				select 
					array_agg(distinct reference_year_set) into _refyearsets 
				from @extschema@.t_available_datasets
				where t_available_datasets.id = new.id;
			ELSE
				RAISE EXCEPTION 'fn_launch_add_check -- % -- table name not known: %', TG_OP, TG_TABLE_NAME;
			END IF;

			if _vars_add_set is not null then
				/*raise notice '%		fn_launch_add_check -- % -- % -- CHECK START -- select target_data.fn_add_plot_target_attr(%, %, %, %, %);', 
					clock_timestamp()::timestamp with time zone at time zone 'Europe/Prague', TG_TABLE_NAME, TG_OP, _vars_add_set, _plots, _refyearsets, 1e-6, true;*/
				with w_err as (
					select @extschema@.fn_add_plot_target_attr(_vars_add_set, _plots, _refyearsets, 1e-6, true) as fn_err
				)
				, w_err_t as (
					select (fn_err).* from w_err
				)
				, w_err_json as (
					select 
						json_build_object(
							'plot'				, plot,
							'reference_year_set'		, reference_year_set,
							'variable'			, variable,
							'ldsity'			, ldsity,
							'ldsity_sum'			, ldsity_sum,
							'variables_def'			, variables_def,
							'variables_found'		, variables_found,
							'diff'				, diff
						) as errj
					from w_err_t
				)
				select json_agg(errj) into _errp from w_err_json;
				/*raise notice '%		fn_launch_add_check -- % -- % -- CHECK STOP', 
					clock_timestamp()::timestamp with time zone at time zone 'Europe/Prague', TG_TABLE_NAME, TG_OP;*/
				IF
					(json_array_length(_errp) > 0)
					THEN RAISE EXCEPTION 'fn_launch_add_check -- % -- % -- plot level local densities are not additive:
						variables in additivity set: %, checked plots: %, refyearsets: %,
						found err plots: 
						%', TG_TABLE_NAME, TG_OP, _vars_add_set, _plots, _refyearsets, 
						jsonb_pretty(_errp::jsonb);
				END IF;
			else
				RAISE WARNING 'fn_launch_add_check -- % -- % -- additivity check was skiped (no variables found in hierarchy):
						variables in additivity set: %, checked plots: %, refyearsets: %'
						, TG_TABLE_NAME, TG_OP, 
						_vars_add_set, _plots, _refyearsets;
			end if;
	ELSE
		RAISE EXCEPTION 'fn_launch_add_check -- trigger operation not known: %', TG_OP;
	END IF;
        RETURN NULL; -- result is ignored since this is an AFTER trigger
    END;
$src$ LANGUAGE plpgsql;

CREATE CONSTRAINT TRIGGER trg__available_datasets__ins
    AFTER INSERT ON @extschema@.t_available_datasets
    DEFERRABLE INITIALLY DEFERRED
    FOR EACH ROW EXECUTE FUNCTION @extschema@.fn_launch_add_check();

CREATE CONSTRAINT TRIGGER trg__available_datasets__upd
    AFTER UPDATE ON @extschema@.t_available_datasets
    DEFERRABLE INITIALLY DEFERRED
    FOR EACH ROW EXECUTE FUNCTION @extschema@.fn_launch_add_check();

CREATE CONSTRAINT TRIGGER trg__available_datasets__del
    AFTER DELETE ON @extschema@.t_available_datasets
    DEFERRABLE INITIALLY DEFERRED
    FOR EACH ROW EXECUTE FUNCTION @extschema@.fn_launch_add_check();

-- </function>

-- <function name="fn_add_plot_target_attr" schema="extschema" src="functions/extschema/additivity/fn_add_plot_target_attr.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_add_plot_target_attr(integer)

-- DROP FUNCTION @extschema@.fn_add_plot_target_attr(integer[], integer[], double precision, boolean);
CREATE OR REPLACE FUNCTION @extschema@.fn_add_plot_target_attr(
	IN variables integer[],
	IN plots integer[] default NULL,
	IN refyearsets integer[] default NULL,
	IN min_diff double precision default 0.0, IN include_null_diff boolean default true
)
  RETURNS TABLE(
	plot			integer,
	reference_year_set	integer,
	variable		integer,
	ldsity			double precision,
	ldsity_sum		double precision,
	variables_def		integer[],
	variables_found		integer[],
	diff			double precision
) AS
$BODY$
DECLARE
	_complete_query text;
	_array_text_v text;
	_array_text_p text;
	_array_text_r text;

BEGIN
	--------------------------------QUERY--------------------------------
	_array_text_v := concat(quote_literal(variables::text), '::integer[]');
	if plots is not null then
		_array_text_p := concat(' AND w_plot_panel.plot = ANY (', quote_literal(plots::text), '::integer[])');
	else
		_array_text_p := '';
	end if;

	if refyearsets is not null then
		_array_text_r := concat(' AND t_available_datasets.reference_year_set = ANY (', quote_literal(refyearsets::text), '::integer[])');
	else
		_array_text_r := '';
	end if;

	--raise notice 'variables: %',  _array_text_v;
	--raise notice 'plots: %',  _array_text_p;
	_complete_query := '
with w_plot_var as materialized (
	with w_plot_panel as (
		select
			f_p_plot.gid as plot, t_panel.id as panel 
		from	sdesign.t_panel
		join 	sdesign.cm_cluster2panel_mapping 		on cm_cluster2panel_mapping.panel = t_panel.id
		join 	sdesign.t_cluster 				on t_cluster.id = cm_cluster2panel_mapping."cluster"
		join 	sdesign.f_p_plot 				on f_p_plot."cluster" = t_cluster.id
		join 	sdesign.cm_plot2cluster_config_mapping 		on cm_plot2cluster_config_mapping.plot = f_p_plot.gid
		join 	sdesign.t_cluster_configuration 		on (t_cluster_configuration.id = t_panel.cluster_configuration 
									and cm_plot2cluster_config_mapping.cluster_configuration = t_cluster_configuration.id)
	)
	select 
		w_plot_panel.plot, w_plot_panel.panel,
		t_available_datasets.reference_year_set, t_available_datasets.variable,
		coalesce(t_target_data.value, 0) as value
	from w_plot_panel
	join @extschema@.t_available_datasets	on (t_available_datasets.panel = w_plot_panel.panel)
	left join @extschema@.t_target_data	on (t_target_data.plot = w_plot_panel.plot 
						and t_target_data.available_datasets = t_available_datasets.id
						and t_target_data.is_latest)
	where 	t_available_datasets.variable = any (' || _array_text_v || ')
                        ' || _array_text_p || '
                        ' || _array_text_r || '
)
, w_node_sum as (
	select
		plot,
		w_plot_var.panel,
		w_plot_var.reference_year_set,
		w_plot_var.variable,
		value as node_sum,
		v_variable_hierarchy_plot.node,
		v_variable_hierarchy_plot.edges as edges_def
	from w_plot_var
	inner join @extschema@.v_variable_hierarchy_plot on (v_variable_hierarchy_plot.node = w_plot_var.variable and
							v_variable_hierarchy_plot.panel = w_plot_var.panel and
							v_variable_hierarchy_plot.reference_year_set = w_plot_var.reference_year_set
	)
	where (v_variable_hierarchy_plot.edges <@ ' || _array_text_v || ')
)
, w_edge_sum as (
	select
		w_node_sum.plot,
		w_node_sum.reference_year_set,
		w_node_sum.node,
		w_node_sum.node_sum,
		w_node_sum.edges_def,
		array_agg(w_plot_var.variable order by w_plot_var.variable) as edges_found,
		sum(w_plot_var.value) as edges_sum
	from w_node_sum
	left join w_plot_var on (
		w_plot_var.plot = w_node_sum.plot
		and w_plot_var.panel = w_node_sum.panel
		and w_plot_var.reference_year_set = w_node_sum.reference_year_set
		and w_plot_var.variable = any(w_node_sum.edges_def))
	group by w_node_sum.plot, w_node_sum.reference_year_set,
		w_node_sum.node, w_node_sum.node_sum, w_node_sum.edges_def
)
, w_diff as (
	select
		plot,
		reference_year_set,
		node		as variable,
		node_sum	as ldsity,
		edges_sum	as ldsity_sum,
		edges_def	as variables_def,
		edges_found	as variables_found,
		case
			when edges_sum != 0.0 and node_sum = 0.0 then 1.0
			when node_sum = 0.0 and edges_sum = 0.0 then 0
			else abs( (node_sum - edges_sum) / node_sum )
		end as diff
	from w_edge_sum
)
select * from w_diff
where
	(' || include_null_diff || ' and ((diff >= ' || min_diff || ') or (diff is null)))
	or
	(not ' || include_null_diff || ' and (diff >= ' || min_diff || '))
order by diff desc
;
';

	--RAISE NOTICE '%', _complete_query;
	RETURN QUERY EXECUTE _complete_query;
END;
$BODY$
  LANGUAGE plpgsql STABLE
  COST 100
  ROWS 100000;

comment on function @extschema@.fn_add_plot_target_attr(integer[], integer[], integer[], double precision, boolean) is
'Function showing plot level target local density attribute additivity (aggregated class estimate should be equal to sum of sub-classes estimates) of target local densities. Hierarchy between aggregated classes and its sub-classes is defined in v_variable_hierarchy_plot.
Function input argument is:
 * Array of attributes -- variables (FKEY to t_variable.id). All variables to be checked (aggregated class together with sub-classes) need to be passed.
 * Array of plots -- plots (FKEY to f_p_plot.gid). If NULL all available plots are checked.
 * Array of reference year sets -- reference year sets (FKEY to t_reference_year_set.id). If NULL all available reference year sets are checked.
 * Minimal amount of difference [%].
 * Whether include NULL difference -- indicating defined sub-classes missing in data.
Resulting table has following columns:
 * Plot. FKEY to f_p_plot.gid.
 * Reference year set. FKEY to t_reference_year_set.id.
 * Target total attribute -- variable. FKEY to t_variable.id.
 * Aggregated class local density.
 * Sum of sub-classes local densities (belonging to aggregated class).
 * Attributes -- variables defined in hierarchy (v_variable_hierarchy_plot). Array of FKEYs to t_variable.id.
 * Attributes -- variables found in data (t_auxiliary_data). Array of FKEYs to t_variable.id.
 * Relative difference between aggregated class local densities and sum of sub-classes local densities.';

GRANT EXECUTE ON FUNCTION @extschema@.fn_add_plot_target_attr TO PUBLIC;

-- </function>
-- <function name="fn_add_plot_aux_attr" schema="extschema" src="functions/extschema/additivity/fn_add_plot_aux_attr.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_add_plot_aux_attr(integer)

-- DROP FUNCTION @extschema@.fn_add_plot_aux_attr(integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_add_plot_aux_attr(
	IN variables integer[],
	IN min_diff double precision default 0.0, IN include_null_diff boolean default true
)
  RETURNS TABLE(
	plot			integer,
	variable		integer,
	ldsity			double precision,
	ldsity_sum		double precision,
	variables_def		integer[],
	variables_found		integer[],
	diff			double precision
) AS
$BODY$
DECLARE
	_complete_query text;
	_array_text_v text;
BEGIN
	--------------------------------QUERY--------------------------------
	_array_text_v := concat(quote_literal(variables::text), '::integer[]');
	--raise notice 'variables: %',  _array_text;
	_complete_query := '

with w_plot_var as materialized (
	with w_plot_panel as (
		select
			f_p_plot.gid as plot, t_panel.id as panel 
		from	sdesign.t_panel
		join 	sdesign.cm_cluster2panel_mapping 		on cm_cluster2panel_mapping.panel = t_panel.id
		join 	sdesign.t_cluster 				on t_cluster.id = cm_cluster2panel_mapping."cluster"
		join 	sdesign.f_p_plot 				on f_p_plot."cluster" = t_cluster.id
		join 	sdesign.cm_plot2cluster_config_mapping 		on cm_plot2cluster_config_mapping.plot = f_p_plot.gid
		join 	sdesign.t_cluster_configuration 		on (t_cluster_configuration.id = t_panel.cluster_configuration 
									and cm_plot2cluster_config_mapping.cluster_configuration = t_cluster_configuration.id)
	)
	select 
		w_plot_panel.plot, w_plot_panel.panel,
		t_available_datasets.reference_year_set, t_available_datasets.variable,
		coalesce(t_auxiliary_data.value, 0) as value
	from w_plot_panel
	join @extschema@.t_available_datasets	on (t_available_datasets.panel = w_plot_panel.panel)
	left join @extschema@.t_auxiliary_data	on (t_auxiliary_data.plot = w_plot_panel.plot 
						and t_auxiliary_data.available_datasets = t_available_datasets.id
						and t_auxiliary_data.is_latest)
	where 	t_available_datasets.variable = any (' || _array_text_v || ')


), w_node_sum as (
	select
		plot,
		w_plot_var.panel,

		w_plot_var.variable,
		value as node_sum,
		v_variable_hierarchy_plot_aux.node,
		v_variable_hierarchy_plot_aux.edges as edges_def
	from w_plot_var
	inner join @extschema@.v_variable_hierarchy_plot_aux on (v_variable_hierarchy_plot_aux.node = w_plot_var.variable and
							v_variable_hierarchy_plot_aux.panel = w_plot_var.panel)
)
, w_edge_sum as (
	select
		w_node_sum.plot,

		w_node_sum.node,
		w_node_sum.node_sum,
		w_node_sum.edges_def,
		array_agg(w_plot_var.variable order by w_plot_var.variable) as edges_found,
		sum(w_plot_var.value) as edges_sum
	from w_node_sum
	left join w_plot_var on (
		w_plot_var.plot = w_node_sum.plot
		and w_plot_var.panel = w_node_sum.panel
		
		and w_plot_var.variable = any(w_node_sum.edges_def))
	group by w_node_sum.plot,
		w_node_sum.node, w_node_sum.node_sum, w_node_sum.edges_def
)
, w_diff as (
	select
		plot,

		node		as variable,
		node_sum	as ldsity,
		edges_sum	as ldsity_sum,
		edges_def	as variables_def,
		edges_found	as variables_found,
		case
			when edges_sum != 0.0 and node_sum = 0.0 then 1.0
			when node_sum = 0.0 and edges_sum = 0.0 then 0
			else abs( (node_sum - edges_sum) / node_sum )
		end as diff
	from w_edge_sum
)
select * from w_diff
where
	(' || include_null_diff || ' and ((diff >= ' || min_diff || ') or (diff is null)))
	or
	(not ' || include_null_diff || ' and (diff >= ' || min_diff || '))
order by diff desc
;
';

	--RAISE NOTICE '%', _complete_query;
	RETURN QUERY EXECUTE _complete_query;
END;
$BODY$
  LANGUAGE plpgsql STABLE
  COST 100
  ROWS 100000;

comment on function @extschema@.fn_add_plot_aux_attr(integer[], double precision, boolean) is
'Function showing plot level auxiliary local density attribute additivity (aggregated class estimate should be equal to sum of sub-classes estimates) of auxiliary local densities. Hierarchy between aggregated classes and its sub-classes is defined in v_variable_hierarchy_plot.
Function input argument is:
 * Array of attributes -- variables (FKEY to t_variable.id). All variables to be checked (aggregated class together with sub-classes) need to be passed.
 * Minimal amount of difference [%].
 * Whether include NULL difference -- indicating defined sub-classes missing in data.
Resulting table has following columns:
 * Plot. FKEY to f_p_plot.gid.
 * Auxiliary total attribute -- variable. FKEY to t_variable.id.
 * Aggregated class local density.
 * Sum of sub-classes local densities (belonging to aggregated class).
 * Attributes -- variables defined in hierarchy (v_variable_hierarchy_plot). Array of FKEYs to t_variable.id.
 * Attributes -- variables found in data (t_auxiliary_data). Array of FKEYs to t_variable.id.
 * Relative difference between aggregated class local densities and sum of sub-classes local densities.';

GRANT EXECUTE ON FUNCTION @extschema@.fn_add_plot_aux_attr TO PUBLIC;

-- </function>

-- <function name="fn_1p_data" schema="extschema" src="functions/extschema/fn_1p_data.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_1p_data(integer)

-- DROP FUNCTION @extschema@.fn_1p_data(integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_1p_data(
    IN conf_id integer
)
  RETURNS TABLE(
	gid bigint,
	cluster integer,
	attribute integer,
	stratum integer,
	plots_per_cluster integer,
	plcount bigint,
	cluster_is_in_cell boolean,
	ldsity_d double precision,
	ldsity_d_plus double precision,
	is_aux boolean,
	is_target boolean,
	geom geometry(MultiPoint),
	ldsity_res_d double precision,
	ldsity_res_d_plus double precision,
	pix double precision,
	sweight double precision,
	DELTA_T__G_beta double precision,
	nb_sampling_units integer, 
	sweight_strata_sum double precision, 
	lambda_d_plus double precision
) AS
$BODY$
DECLARE
	_complete_query text;
BEGIN
	--------------------------------QUERY--------------------------------
	_complete_query := '
WITH
---------------------------------------------------------
-- Data block
---------------------------------------------------------
w_ldsity_plot AS MATERIALIZED (
	select
		f_p_plot.gid,
		t_total_estimate_conf.id as conf_id,
		t_panel.stratum,
		t_cluster.id as cluster,
		t_panel_refyearset_group.reference_year_set,
		t_panel_refyearset_group.panel,
		cm_cluster2panel_mapping.sampling_weight,
		t_total_estimate_conf.variable as attribute,
		plots_per_cluster,
		true AS plot_is_in_cell,
		coalesce(tdads.value, 0) as ldsity,
		f_p_plot.geom
	from @extschema@.t_total_estimate_conf
	inner join @extschema@.c_panel_refyearset_group on t_total_estimate_conf.panel_refyearset_group = c_panel_refyearset_group.id
	inner join @extschema@.t_panel_refyearset_group on t_panel_refyearset_group.panel_refyearset_group = c_panel_refyearset_group.id
	inner join sdesign.t_panel ON t_panel.id = t_panel_refyearset_group.panel
	inner join sdesign.cm_cluster2panel_mapping ON cm_cluster2panel_mapping.panel = t_panel.id
	inner join sdesign.t_cluster ON t_cluster.id = cm_cluster2panel_mapping."cluster"
	inner join sdesign.f_p_plot ON f_p_plot."cluster" = t_cluster.id
	inner join sdesign.cm_plot2cluster_config_mapping on f_p_plot.gid = cm_plot2cluster_config_mapping.plot
	inner join sdesign.t_cluster_configuration ON (t_panel.cluster_configuration = t_cluster_configuration.id
											and cm_plot2cluster_config_mapping.cluster_configuration = t_cluster_configuration.id)
	inner join sdesign.t_stratum ON t_panel.stratum = t_stratum.id
	inner join @extschema@.cm_plot2cell_mapping ON (cm_plot2cell_mapping.estimation_cell = t_total_estimate_conf.estimation_cell 
											and cm_plot2cell_mapping.plot = f_p_plot.gid)
	left join (	select 
				plot, reference_year_set, variable, value, is_latest 
			from @extschema@.t_target_data 
			inner join @extschema@.t_available_datasets on (t_target_data.available_datasets = t_available_datasets.id)
		) as tdads on (
			f_p_plot.gid = tdads.plot and
			t_panel_refyearset_group.reference_year_set = tdads.reference_year_set and
			t_total_estimate_conf.variable = tdads.variable and
			tdads.is_latest
		)
	where t_total_estimate_conf.id = ' || conf_id || '
)
, w_ldsity_cluster AS MATERIALIZED (
	SELECT
		w_ldsity_plot.conf_id,
		concat(w_ldsity_plot.conf_id, ROW_NUMBER() OVER ())::bigint as gid,
		w_ldsity_plot.cluster,
		w_ldsity_plot.attribute,
		w_ldsity_plot.stratum,
                w_ldsity_plot.panel,
		w_ldsity_plot.plots_per_cluster,
		w_ldsity_plot.sampling_weight,
		count(*) as plcount,
		sum(w_ldsity_plot.plot_is_in_cell::int) > 0 as cluster_is_in_cell,
		sum(w_ldsity_plot.ldsity * plot_is_in_cell::int)/w_ldsity_plot.plots_per_cluster AS ldsity_D,
		sum(w_ldsity_plot.ldsity)/w_ldsity_plot.plots_per_cluster AS ldsity_D_plus,
		st_collect(w_ldsity_plot.geom)::geometry(MultiPoint) as geom
	FROM       w_ldsity_plot
	GROUP BY conf_id, stratum, panel, cluster, sampling_weight, plots_per_cluster, attribute
	ORDER BY stratum, cluster, attribute
)
, w_strata_sum AS MATERIALIZED (
	select
		t.conf_id,
		t_panel.stratum,
		case when t_cluster_configuration.cluster_design then
			t_stratum.buffered_area_m2/10000
		else
			t_stratum.area_m2/10000
		end as lambda_d_plus,
		sum(t_panel.cluster_count) as nb_sampling_units,
		sum(t_panel.sweight_panel_sum) as sweight_strata_sum
	from sdesign.t_stratum
	inner join sdesign.t_panel on (t_panel.stratum = t_stratum.id)
	inner join sdesign.t_cluster_configuration on (t_panel.cluster_configuration = t_cluster_configuration.id)
	inner join (select distinct conf_id, panel from w_ldsity_plot) as t on (t_panel.id = t.panel)
	group by conf_id, t_panel.stratum, lambda_d_plus
)
, w_1p_data AS MATERIALIZED (
	select
		w_ldsity_cluster.gid, w_ldsity_cluster.cluster,
		w_ldsity_cluster.attribute, w_ldsity_cluster.stratum, w_ldsity_cluster.plots_per_cluster,
		w_ldsity_cluster.plcount, w_ldsity_cluster.cluster_is_in_cell, w_ldsity_cluster.ldsity_d,
		w_ldsity_cluster.ldsity_d_plus, false::boolean as is_aux, true::boolean as is_target,
		w_ldsity_cluster.geom,
		NULL::double precision as ldsity_res_D, NULL::double precision as ldsity_res_D_plus,
		NULL::double precision as pix,  w_ldsity_cluster.sampling_weight as sweight, NULL::double precision as DELTA_T__G_beta,
		w_strata_sum.nb_sampling_units::integer, w_strata_sum.sweight_strata_sum, w_strata_sum.lambda_d_plus
	from w_ldsity_cluster
	INNER JOIN w_strata_sum ON w_ldsity_cluster.stratum = w_strata_sum.stratum
)
select * from w_1p_data;
';

	--RAISE NOTICE '%', _complete_query;
	RETURN QUERY EXECUTE _complete_query;
END;
$BODY$
  LANGUAGE plpgsql STABLE
  COST 100
  ROWS 100000;

COMMENT ON FUNCTION @extschema@.fn_1p_data(integer) IS 'Function preparing data for regression estimate.';

-- </function>

-- <function name="fn_2p_data" schema="extschema" src="functions/extschema/fn_2p_data.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_2p_data(integer)

-- DROP FUNCTION @extschema@.fn_2p_data(integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_2p_data(
    IN conf_id integer
)
  RETURNS TABLE(
	gid bigint,
	cluster integer,
	attribute integer,
	stratum integer,
	plots_per_cluster integer,
	plcount bigint,
	cluster_is_in_cell boolean,
	ldsity_d double precision,
	ldsity_d_plus double precision,
	is_aux boolean,
	is_target boolean,
	geom geometry(MultiPoint),
	ldsity_res_d double precision,
	ldsity_res_d_plus double precision,
	pix double precision,
	sweight double precision,
	DELTA_T__G_beta double precision,
	nb_sampling_units integer,
	sweight_strata_sum double precision,
	lambda_d_plus double precision,
	sigma boolean
) AS
$$
begin
--------------------------------QUERY--------------------------------
---------------------------------------------------------
-- Configuration block
---------------------------------------------------------
execute '
create temporary table w_configuration ON COMMIT DROP AS (
	with w_a AS NOT MATERIALIZED (
		select
			t_total_estimate_conf.estimation_cell, t_aux_conf.model, 
			t_aux_conf.sigma, t_total_estimate_conf.force_synthetic, t_aux_conf.param_area,
			array_agg(t_total_estimate_conf.id order by t_total_estimate_conf.variable) as t_total_estimate_conf__id,
			array_agg(t_total_estimate_conf.panel_refyearset_group order by t_total_estimate_conf.variable) as t_total_estimate_conf__panel_refyearset_group,
			array_agg(t_aux_conf.id order by t_total_estimate_conf.variable) as t_aux_conf__id,
			array_agg(t_total_estimate_conf.variable order by t_total_estimate_conf.variable) as target_attributes,
			t_total_estimate_conf.aux_conf
		from @extschema@.t_total_estimate_conf
		inner join @extschema@.t_aux_conf ON t_aux_conf.id = t_total_estimate_conf.aux_conf
		where t_total_estimate_conf.id = $1
		group by t_total_estimate_conf.estimation_cell, t_aux_conf.model, t_aux_conf.sigma, t_total_estimate_conf.force_synthetic,
			t_aux_conf.param_area, t_total_estimate_conf.aux_conf
	)
	select
		t_total_estimate_conf__id[1] as id,
		t_total_estimate_conf__panel_refyearset_group[1] AS panel_refyearset_group,
		t_total_estimate_conf__panel_refyearset_group,
		w_a.t_total_estimate_conf__id, w_a.t_aux_conf__id,
		w_a.param_area, w_a.estimation_cell as cell, w_a.model, w_a.sigma, w_a.force_synthetic,
		array_agg(t_model_variables.variable order by t_model_variables.variable) as aux_attributes, w_a.target_attributes
	from w_a
	inner join @extschema@.t_aux_conf ON t_aux_conf.id = w_a.aux_conf
	inner join @extschema@.t_model ON t_model.id = t_aux_conf.model
	inner join @extschema@.t_model_variables ON t_model_variables.model = t_model.id
	group by w_a.estimation_cell, w_a.model, w_a.sigma, w_a.force_synthetic, w_a.param_area, w_a.target_attributes, 
		w_a.t_total_estimate_conf__id, w_a.t_total_estimate_conf__panel_refyearset_group, w_a.t_aux_conf__id
	order by id limit 1
);' using fn_2p_data.conf_id;
analyze w_configuration;

execute '
create temporary table w_cell_selection ON COMMIT DROP AS (
	SELECT
		w_configuration.id as conf_id,
		estimation_cell as estimation_cell,
		geom
	FROM @extschema@.f_a_cell
	INNER JOIN w_configuration ON (w_configuration.cell = f_a_cell.estimation_cell)
);';
analyze w_cell_selection;

execute '
create temporary table w_plot ON COMMIT DROP as (
WITH
w_param_area_selection AS NOT MATERIALIZED (
	SELECT
		w_configuration.id as conf_id,
		gid as param_area_gid,
		geom
	FROM @extschema@.f_a_param_area
	INNER JOIN w_configuration ON (w_configuration.param_area = f_a_param_area.gid)
)
---------------------------------------------------------
-- Data block
---------------------------------------------------------
, w_plot AS NOT MATERIALIZED (
	select distinct
		f_p_plot.gid, w_configuration.id as conf_id, t_cluster.id as cluster, t_panel.stratum,
		case when w_configuration.force_synthetic = True then False else cm_plot2cell_mapping.id IS NOT NULL end AS plot_is_in_cell, 
		-- t_panel2aux_conf.reference_year_set as reference_year_set, t_panel2aux_conf.panel as panel,
		t_panel_refyearset_group.reference_year_set as reference_year_set, t_panel_refyearset_group.panel as panel,
		f_p_plot.geom
	from w_configuration
        inner join @extschema@.t_panel_refyearset_group on w_configuration.panel_refyearset_group = t_panel_refyearset_group.panel_refyearset_group
	--inner join @extschema@.t_panel2aux_conf on w_configuration.t_aux_conf__id[1] = t_panel2aux_conf.aux_conf
	inner join sdesign.t_panel ON (t_panel.id = t_panel_refyearset_group.panel)
	inner join sdesign.cm_cluster2panel_mapping ON cm_cluster2panel_mapping.panel = t_panel.id
	inner join sdesign.t_cluster ON t_cluster.id = cm_cluster2panel_mapping."cluster"
	inner join sdesign.f_p_plot ON f_p_plot."cluster" = t_cluster.id
	left join @extschema@.cm_plot2cell_mapping ON (cm_plot2cell_mapping.estimation_cell = w_configuration.cell and cm_plot2cell_mapping.plot = f_p_plot.gid)
	inner join @extschema@.cm_plot2param_area_mapping ON (cm_plot2param_area_mapping.param_area = w_configuration.param_area and cm_plot2param_area_mapping.plot = f_p_plot.gid)
)
select * from w_plot
);';
analyze w_plot;

execute '
create temporary table w_ldsity_plot ON COMMIT DROP as (
	with w_plot AS NOT MATERIALIZED (select * from w_plot)
		select
			w_plot.gid,
			w_plot.conf_id,
			w_plot.stratum,
			w_plot.panel,
			w_plot.cluster,
			t_variable.id as attribute,
			t_cluster_configuration.plots_per_cluster,
			w_plot.plot_is_in_cell,
			coalesce(adads.value, 0) as ldsity,
			true as is_aux, false as is_target,
			w_plot.geom
		from w_plot as w_plot
		inner join sdesign.cm_plot2cluster_config_mapping on w_plot.gid = cm_plot2cluster_config_mapping.plot
		inner join sdesign.t_cluster_configuration on cm_plot2cluster_config_mapping.cluster_configuration = t_cluster_configuration.id
		inner join w_configuration on w_plot.conf_id = w_configuration.id
		inner join @extschema@.t_variable on t_variable.id = ANY (w_configuration.aux_attributes)
		left join (	select
				plot, variable, value, is_latest
			from @extschema@.t_auxiliary_data 
			inner join @extschema@.t_available_datasets on (t_auxiliary_data.available_datasets = t_available_datasets.id)
		) as adads
		on (
			w_plot.gid = adads.plot and
			t_variable.id = adads.variable and
			adads.is_latest)
	union all
		select
			w_plot.gid,
			w_plot.conf_id,
			w_plot.stratum,
			w_plot.panel,
			w_plot.cluster,
			t_variable.id as attribute,
			t_cluster_configuration.plots_per_cluster,
			w_plot.plot_is_in_cell,
			coalesce(tdads.value, 0) as ldsity,
			false as is_aux, true as is_target,
			w_plot.geom
		from w_plot as w_plot
		inner join sdesign.cm_plot2cluster_config_mapping on w_plot.gid = cm_plot2cluster_config_mapping.plot
		inner join sdesign.t_cluster_configuration on cm_plot2cluster_config_mapping.cluster_configuration = t_cluster_configuration.id
		inner join w_configuration on w_plot.conf_id = w_configuration.id
                inner join @extschema@.t_variable on (t_variable.id = ANY (w_configuration.target_attributes))
		left join (	select 
				plot, reference_year_set, variable, value, is_latest 
			from @extschema@.t_target_data 
			inner join @extschema@.t_available_datasets on (t_target_data.available_datasets = t_available_datasets.id)
		) as tdads
		on (
			w_plot.gid = tdads.plot and
			w_plot.reference_year_set = tdads.reference_year_set and
			t_variable.id = tdads.variable and
			tdads.is_latest)
);';
analyze w_ldsity_plot;

execute '
create temporary table w_ldsity_cluster ON COMMIT DROP as (
	SELECT
		w_ldsity_plot.conf_id,
		concat(w_ldsity_plot.conf_id, ROW_NUMBER() OVER ())::bigint as gid,
		w_ldsity_plot.cluster,
		w_ldsity_plot.attribute,
		w_ldsity_plot.stratum,
		w_ldsity_plot.panel,
		w_ldsity_plot.plots_per_cluster,
		count(*) as plcount,
		sum(w_ldsity_plot.plot_is_in_cell::int) > 0 as cluster_is_in_cell,
		sum(w_ldsity_plot.ldsity * plot_is_in_cell::int)/w_ldsity_plot.plots_per_cluster AS ldsity_D,
		sum(w_ldsity_plot.ldsity)/w_ldsity_plot.plots_per_cluster AS ldsity_D_plus,
		w_ldsity_plot.is_aux, w_ldsity_plot.is_target,
		st_collect(w_ldsity_plot.geom)::geometry(MultiPoint) as geom
	FROM       w_ldsity_plot
	GROUP BY conf_id, stratum, panel, cluster, plots_per_cluster, attribute, is_aux, is_target
	ORDER BY stratum, cluster, attribute
);';
analyze w_ldsity_cluster;

execute '
create temporary table w_clusters ON COMMIT DROP as (
	select distinct conf_id, stratum, panel, cluster, cluster_is_in_cell from w_ldsity_cluster
);';
analyze w_clusters;

execute '
create temporary table w_strata_sum ON COMMIT DROP as (
	select
		w_configuration.id as conf_id,
                t_stratum.id as f_a_sampling_stratum_gid,
		case when t_cluster_configuration.cluster_design then
			t_stratum.buffered_area_m2/10000
		else
			t_stratum.area_m2/10000
		end as lambda_d_plus,
		plots_per_cluster,
		count(*) as nb_sampling_units, sum (sampling_weight) as sweight_strata_sum
	from w_configuration
	inner join @extschema@.t_aux_conf on t_aux_conf.id = ANY (w_configuration.t_aux_conf__id)
	inner join @extschema@.t_panel_refyearset_group ON t_panel_refyearset_group.panel_refyearset_group = t_aux_conf.panel_refyearset_group
	inner join sdesign.t_panel ON t_panel.id = t_panel_refyearset_group.panel
	inner join sdesign.t_cluster_configuration ON t_panel.cluster_configuration = t_cluster_configuration.id
	inner join sdesign.t_stratum ON t_stratum.id = t_panel.stratum
	inner join sdesign.cm_cluster2panel_mapping ON cm_cluster2panel_mapping.panel = t_panel.id
	group by w_configuration.id, f_a_sampling_stratum_gid, lambda_d_plus, plots_per_cluster
);';
analyze w_strata_sum;
---------------------------------------------------------
-- Matrix block
---------------------------------------------------------
execute '
create temporary table w_X ON COMMIT DROP AS (
	SELECT
		conf_id,
		attribute AS r,
		cluster AS c,
		ldsity_D as val_D,
		ldsity_D_plus as val
	FROM w_ldsity_cluster
	WHERE is_aux
);';
analyze w_X;

execute '
create temporary table w_Y ON COMMIT DROP AS (
	SELECT
		conf_id,
		attribute AS r,
		cluster AS c,
		ldsity_D as val_D,
		ldsity_D_plus as val
	FROM w_ldsity_cluster
	WHERE is_target
);';
analyze w_Y;

execute '
create temporary table w_pix ON COMMIT DROP as (
with
w_pix AS NOT MATERIALIZED (-------------------------INCLUSION DENSITY PIX
	SELECT
		w_clusters.conf_id,
		w_clusters.stratum,
		w_clusters.cluster,
		w_strata_sum.sweight_strata_sum / (w_strata_sum.lambda_d_plus * cm_cluster2panel_mapping.sampling_weight) as pix,
		cm_cluster2panel_mapping.sampling_weight as sweight
	FROM w_clusters
	INNER JOIN sdesign.cm_cluster2panel_mapping ON (w_clusters.cluster = cm_cluster2panel_mapping.cluster 
								and w_clusters.panel = cm_cluster2panel_mapping.panel)
	INNER JOIN w_strata_sum as w_strata_sum ON w_clusters.stratum = w_strata_sum.f_a_sampling_stratum_gid
)
select * from w_pix
);';
analyze w_pix;

execute '
create temporary table w_t_G_beta ON COMMIT DROP as (
        select
                w_configuration.id as conf_id,
                t_g_beta.variable as r, t_g_beta.cluster as c, t_g_beta.val
        from @extschema@.t_g_beta
        inner join w_configuration on (t_g_beta.aux_conf = w_configuration.t_aux_conf__id[1])
	where t_g_beta.is_latest
);';
analyze w_t_G_beta;

execute '
create temporary table w_PI ON COMMIT DROP as (
	SELECT
		conf_id,
		1 as r,
		cluster as c,
		1.0 / pix as val
	from w_pix
	order by r, c
);';
analyze w_PI;

execute '
create temporary table w_DELTA_G_beta ON COMMIT DROP as (
with w_I AS NOT MATERIALIZED (
	SELECT 
		conf_id,
		1 AS r,
		cluster AS c,
		cluster_is_in_cell::int AS val
	FROM	w_clusters
	order by r, c
)
, w_SIGMA AS NOT MATERIALIZED (
	SELECT
		conf_id,
		1 as r,
		cluster as c,
		case when w_configuration.sigma then (plots_per_cluster^2)::float / plcount::float 
		else 1::float end
		as val
	from (select conf_id, cluster, plots_per_cluster, plcount 
		from w_ldsity_cluster 
		group by conf_id, cluster, plots_per_cluster, plcount) as m
	inner join w_configuration on (m.conf_id = w_configuration.id)
	order by r, c
)
, w_SIGMA_PI AS NOT MATERIALIZED (
	SELECT
		A.conf_id,
		1 as r,
		A.c as c,
		A.val * B.val as val
	from w_SIGMA as A 
	inner join w_PI as B on (A.c = B.c and A.conf_id = B.conf_id) 
	order by r, c
)
, w_t_hat AS NOT MATERIALIZED (  -- eq. 18
	select 
		A.conf_id,
		A.r,
		1 AS c,
		sum(A.val_D * B.val * C.val) as val
	from w_X 		AS A 
	inner join w_PI     	AS B 	on (A.c = B.c and A.conf_id = B.conf_id)
	inner join w_I		AS C 	on (A.c = C.c and A.conf_id = C.conf_id)
	group by A.r, A.conf_id
	order by A.r
)
, w_t AS NOT MATERIALIZED (
	with w_aux_total as (
		select estimation_cell as cell, t_variable.id as attribute, aux_total
		from @extschema@.t_aux_total
		inner join @extschema@.t_variable on (t_aux_total.variable = t_variable.id)
		where t_aux_total.is_latest
	)
	, w_aux_ldsity as (
		select distinct conf_id, r as attribute from w_X
	)
	select
		w_cell_selection.conf_id,
		w_aux_total.attribute as r,
		1 as c,
		coalesce(w_aux_total.aux_total,
			@extschema@.fn_raise_notice(
				format(''fn_2p_data.w_t: t_aux_total not found! (conf_id:%s attribute:%s)'',
					w_cell_selection.conf_id,
					w_aux_ldsity.attribute),
				''exception''
			)::int::float
		) as val
	from
	w_aux_ldsity
	inner join w_cell_selection on (w_aux_ldsity.conf_id = w_cell_selection.conf_id)
	left join w_aux_total on (w_aux_ldsity.attribute = w_aux_total.attribute and w_cell_selection.estimation_cell = w_aux_total.cell)
)
, w_DELTA_T AS NOT MATERIALIZED (
	select
		conf_id,
		c as r, 
		r as c, 
		w_t.val - w_t_hat.val as val 
	from w_t 
	inner join w_t_hat using (r, c, conf_id)
)
, w_DELTA_G_beta AS NOT MATERIALIZED (
	select
		A.conf_id,
		A.r,
		B.c, 
		sum(A.val * B.val) as val
	FROM 	w_DELTA_T as A
	inner join w_t_G_beta as B ON (A.c = B.r and A.conf_id = B.conf_id)
	GROUP BY A.r, B.c, A.conf_id
	order by r, c
)
select * from w_DELTA_G_beta
);';
analyze w_DELTA_G_beta;

execute '
create temporary table w_X_beta ON COMMIT DROP as (
with w_Y_T AS NOT MATERIALIZED (
	SELECT
		conf_id,
		c as r,
		r as c,
		val,
		val_D -- used for 1p estimete
	from w_Y
	order by r, c
)
, w_G_PI AS NOT MATERIALIZED (
	select 
		A.conf_id,
		A.r, 
		A.c AS c,
		A.val * B.val as val
	from w_t_G_beta AS A, w_PI AS B 
	where A.c = B.c and A.conf_id = B.conf_id
	order by r, c
)
, w_beta_hat AS NOT MATERIALIZED (
	select
		A.conf_id,
		A.r,
		B.c, 
		sum(A.val * B.val) as val
	FROM w_G_PI as A, w_Y_T as B
	WHERE A.c = B.r and A.conf_id = B.conf_id
	GROUP BY A.r, B.c, A.conf_id
	order by r, c
)
, w_X_plots_T AS NOT MATERIALIZED (
	SELECT conf_id, gid AS r, cluster, attribute AS c, plots_per_cluster, ldsity AS val 
	FROM w_ldsity_plot WHERE is_aux
)
, w_X_beta AS NOT MATERIALIZED (
	select
		A.conf_id,
		A.r,
		B.c, 
		sum(A.val * B.val) as val
	FROM w_X_plots_T as A, w_beta_hat as B
	WHERE A.c = B.r and A.conf_id = B.conf_id
	GROUP BY A.r, B.c, A.conf_id
	order by r, c 
)
select * from w_X_beta
);';
analyze w_X_beta;

execute '
create temporary table w_ldsity_residuals_cluster ON COMMIT DROP as (
with w_Y_plots_T AS NOT MATERIALIZED (
	SELECT conf_id, gid AS r, cluster, attribute AS c, plots_per_cluster, ldsity AS val 
	FROM w_ldsity_plot WHERE is_target
)
, w_residuals_plot AS NOT MATERIALIZED ( -------------------------RESIDUAL LOCAL DENSITY ON TRACT LEVEL
	select
		w_Y_plots_T.conf_id,
		w_Y_plots_T.r AS plot, 
		w_Y_plots_T.c AS attribute, 
		(w_Y_plots_T.val - w_X_beta.val) AS ldsity 
	from w_Y_plots_T 
	INNER JOIN w_X_beta USING (r, c, conf_id)
)
, w_residuals_cluster AS NOT MATERIALIZED (
	with
	w_residuals_plot AS NOT MATERIALIZED (select * from w_residuals_plot),
	w_ldsity_plot AS NOT MATERIALIZED (select * from w_ldsity_plot)
	SELECT
		w_ldsity_plot.conf_id,
		w_ldsity_plot.cluster,
		w_residuals_plot.attribute,
		w_ldsity_plot.stratum,
		w_ldsity_plot.plots_per_cluster,
		count(*) as plcount,
		sum(plot_is_in_cell::int) > 0 as cluster_is_in_cell,
		sum(w_residuals_plot.ldsity * plot_is_in_cell::int)/w_ldsity_plot.plots_per_cluster AS ldsity_res_D, --eq 38
		sum(w_residuals_plot.ldsity)/w_ldsity_plot.plots_per_cluster AS ldsity_res_D_plus -- eq 39
	FROM w_ldsity_plot AS w_ldsity_plot
	INNER JOIN w_residuals_plot AS w_residuals_plot ON w_ldsity_plot.gid = w_residuals_plot.plot AND w_ldsity_plot.attribute = w_residuals_plot.attribute and w_ldsity_plot.conf_id = w_residuals_plot.conf_id
	GROUP BY stratum, w_ldsity_plot.conf_id, w_ldsity_plot.cluster, w_ldsity_plot.plots_per_cluster, w_residuals_plot.attribute
	ORDER BY stratum, w_ldsity_plot.conf_id, w_ldsity_plot.cluster, w_residuals_plot.attribute
)
, w_ldsity_residuals_cluster AS NOT MATERIALIZED (
	select 
		w_ldsity_cluster.conf_id, w_ldsity_cluster.gid, w_ldsity_cluster.cluster, 
		w_ldsity_cluster.attribute, w_ldsity_cluster.stratum, w_ldsity_cluster.plots_per_cluster, 
		w_ldsity_cluster.plcount, w_ldsity_cluster.cluster_is_in_cell, w_ldsity_cluster.ldsity_d, w_ldsity_cluster.ldsity_d_plus, 
		w_ldsity_cluster.is_aux, w_ldsity_cluster.is_target, w_ldsity_cluster.geom,
		w_residuals_cluster.ldsity_res_D, w_residuals_cluster.ldsity_res_D_plus
	from w_ldsity_cluster
	LEFT JOIN w_residuals_cluster USING (conf_id, cluster, attribute, stratum)
)
select * from w_ldsity_residuals_cluster
);';
analyze w_ldsity_residuals_cluster;

--EXPLAIN (ANALYZE, COSTS, VERBOSE, BUFFERS/*, FORMAT JSON*/)
return query execute '
with w_2p_data AS NOT MATERIALIZED (
	with w_ldsity_residuals_cluster AS NOT MATERIALIZED (select * from w_ldsity_residuals_cluster)
	select 
		w_ldsity_residuals_cluster.conf_id, w_ldsity_residuals_cluster.gid, w_ldsity_residuals_cluster.cluster, 
		w_ldsity_residuals_cluster.attribute, w_ldsity_residuals_cluster.stratum, w_ldsity_residuals_cluster.plots_per_cluster, 
		w_ldsity_residuals_cluster.plcount, w_ldsity_residuals_cluster.cluster_is_in_cell, w_ldsity_residuals_cluster.ldsity_d, 
		w_ldsity_residuals_cluster.ldsity_d_plus, 
		w_ldsity_residuals_cluster.is_aux, w_ldsity_residuals_cluster.is_target, w_ldsity_residuals_cluster.geom,
		w_ldsity_residuals_cluster.ldsity_res_D, w_ldsity_residuals_cluster.ldsity_res_D_plus,
		w_pix.pix, w_pix.sweight, w_DELTA_G_beta.val as DELTA_T__G_beta, 
		w_strata_sum.nb_sampling_units::integer, w_strata_sum.sweight_strata_sum, w_strata_sum.lambda_d_plus
	from w_ldsity_residuals_cluster
	INNER JOIN w_pix USING (conf_id, cluster, stratum)
	INNER JOIN w_DELTA_G_beta ON (w_pix.conf_id = w_DELTA_G_beta.conf_id and cluster = w_DELTA_G_beta.c)
	inner join w_strata_sum on (stratum = f_a_sampling_stratum_gid)
)
, w_2p_data_sigma AS NOT MATERIALIZED (
	select 
		w_2p_data.gid, w_2p_data.cluster, w_2p_data.attribute, w_2p_data.stratum, w_2p_data.plots_per_cluster, w_2p_data.plcount,
		w_2p_data.cluster_is_in_cell as cluster_is_in_cell, 
		w_2p_data.ldsity_d, w_2p_data.ldsity_d_plus, w_2p_data.is_aux, w_2p_data.is_target,
		w_2p_data.geom, w_2p_data.ldsity_res_d, w_2p_data.ldsity_res_d_plus, w_2p_data.pix, w_2p_data.sweight, w_2p_data.DELTA_T__G_beta,
		w_2p_data.nb_sampling_units, w_2p_data.sweight_strata_sum, w_2p_data.lambda_d_plus, w_configuration.sigma
	from w_2p_data 
	inner join w_configuration on (w_2p_data.conf_id = w_configuration.id)
)
select * from w_2p_data_sigma;';

drop table w_configuration;
drop table w_cell_selection;
drop table w_plot;
drop table w_ldsity_plot;
drop table w_ldsity_cluster;
drop table w_clusters;
drop table w_strata_sum;
drop table w_X;
drop table w_Y;
drop table w_pix;
drop table w_t_G_beta;
drop table w_PI;
drop table w_DELTA_G_beta;
drop table w_X_beta;
drop table w_ldsity_residuals_cluster;
end;
$$
  LANGUAGE plpgsql
  COST 100
  ROWS 100000;

COMMENT ON FUNCTION @extschema@.fn_2p_data(integer) IS 'Function preparing data for regression estimate.';

-- </function>

-- <function name="fn_g_beta" schema="extschema" src="functions/extschema/fn_g_beta.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_G_beta(integer)

-- DROP FUNCTION @extschema@.fn_G_beta(integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_G_beta(
    IN conf_id integer
)
  RETURNS TABLE(
	variable integer,
	cluster integer,
	val double precision
) AS
$BODY$
DECLARE
	_complete_query text;
BEGIN
	--------------------------------QUERY--------------------------------
	_complete_query := '
WITH
w_configuration AS MATERIALIZED (
	select 
		t_aux_conf.id, t_aux_conf.param_area, t_aux_conf.model, t_aux_conf.sigma, t_model.description,
		array_agg(t_model_variables.variable order by t_model_variables.variable) as aux_attributes,
		t_aux_conf.panel_refyearset_group
	from @extschema@.t_aux_conf 
	inner join @extschema@.t_model ON t_model.id = t_aux_conf.model
	inner join @extschema@.t_model_variables ON t_model_variables.model = t_model.id
        where t_aux_conf.id = ' || conf_id || '
	group by t_aux_conf.id, t_aux_conf.param_area, t_aux_conf.model, t_model.description
)
, w_param_area_selection AS MATERIALIZED (
	SELECT
		w_configuration.id as conf_id,
		gid as param_area_gid,
		geom,
		panel_refyearset_group
	FROM @extschema@.f_a_param_area
	INNER JOIN w_configuration ON (w_configuration.param_area = f_a_param_area.gid)
)
, w_plot AS MATERIALIZED (-------------------------LIST OF PLOTS IN PARAMETRIZATION AREA
	select distinct 
		w_param_area_selection.conf_id, f_p_plot.gid, t_cluster.id as cluster, t_panel.stratum,
		t_panel_refyearset_group.panel, 
		f_p_plot.geom
	from w_param_area_selection 
	inner join @extschema@.t_panel_refyearset_group on w_param_area_selection.panel_refyearset_group = t_panel_refyearset_group.panel_refyearset_group
	inner join sdesign.t_panel ON t_panel.id = t_panel_refyearset_group.panel
	inner join sdesign.cm_cluster2panel_mapping ON cm_cluster2panel_mapping.panel = t_panel.id
	inner join sdesign.t_cluster ON t_cluster.id = cm_cluster2panel_mapping."cluster"
	inner join sdesign.f_p_plot ON f_p_plot."cluster" = t_cluster.id
	inner join @extschema@.cm_plot2param_area_mapping ON cm_plot2param_area_mapping.plot = f_p_plot.gid
	inner join @extschema@.f_a_param_area ON (f_a_param_area.gid = cm_plot2param_area_mapping.param_area and w_param_area_selection.param_area_gid = f_a_param_area.gid)
)
, w_ldsity_plot AS MATERIALIZED (
	SELECT
		w_plot.conf_id,
		w_plot.gid,
		w_plot.stratum,
                w_plot.panel,
		w_plot.cluster,
		t_variable.id as attribute,
		t_cluster_configuration.plots_per_cluster,
		coalesce(adads.value, 0) as ldsity,
		w_plot.geom,
		true as is_aux
	FROM w_plot
	inner join sdesign.cm_plot2cluster_config_mapping on w_plot.gid = cm_plot2cluster_config_mapping.plot
	inner join sdesign.t_cluster_configuration on cm_plot2cluster_config_mapping.cluster_configuration = t_cluster_configuration.id
	inner join w_configuration on w_plot.conf_id = w_configuration.id
	inner join @extschema@.t_variable on t_variable.id = ANY (w_configuration.aux_attributes)
	left join (	select
			plot, variable, value, is_latest
		from @extschema@.t_auxiliary_data 
		inner join @extschema@.t_available_datasets on (t_auxiliary_data.available_datasets = t_available_datasets.id)
	) as adads
	on (
		w_plot.gid = adads.plot and
		t_variable.id = adads.variable and
		adads.is_latest)
)
, w_ldsity_cluster AS MATERIALIZED (
 	SELECT
		w_ldsity_plot.conf_id,
		concat(w_ldsity_plot.conf_id, ROW_NUMBER() OVER ())::bigint as gid, 
		w_ldsity_plot.cluster,
		w_ldsity_plot.attribute,
		w_ldsity_plot.stratum,
                w_ldsity_plot.panel,
		w_ldsity_plot.plots_per_cluster,
		count(*) as plcount,
		sum(w_ldsity_plot.ldsity)/w_ldsity_plot.plots_per_cluster AS ldsity_D_plus, -- eq 15,
		w_ldsity_plot.is_aux, --
		st_collect(w_ldsity_plot.geom)::geometry(MultiPoint) as geom
	FROM       w_ldsity_plot
	GROUP BY conf_id, stratum, panel, cluster, plots_per_cluster, attribute, is_aux
	ORDER BY stratum, cluster, attribute
)
, w_X AS MATERIALIZED ( -------------------------AUX LOCAL DENSITY ON TRACT LEVEL
	SELECT
		conf_id,
		attribute AS r,
		cluster AS c,
		ldsity_D_plus as val
	FROM w_ldsity_cluster
	WHERE is_aux
)
, w_clusters AS MATERIALIZED (-------------------------LIST OF TRACTS
	select distinct conf_id, stratum, panel, cluster from w_ldsity_cluster
)
, w_strata_sum AS MATERIALIZED (
	select
		w_configuration.id as conf_id,
                t_stratum.id as f_a_sampling_stratum_gid,
		case when t_cluster_configuration.cluster_design then
			t_stratum.buffered_area_m2/10000
		else
			t_stratum.area_m2/10000
		end as lambda_d_plus,
		plots_per_cluster,
		count(*) as nb_sampling_units, sum (sampling_weight) as sweight_strata_sum
	from w_configuration
	inner join @extschema@.t_aux_conf on t_aux_conf.id = w_configuration.id
	inner join @extschema@.t_panel_refyearset_group ON t_panel_refyearset_group.panel_refyearset_group = t_aux_conf.panel_refyearset_group
	inner join sdesign.t_panel ON t_panel.id = t_panel_refyearset_group.panel
	inner join sdesign.t_cluster_configuration ON t_panel.cluster_configuration = t_cluster_configuration.id
	inner join sdesign.t_stratum ON t_stratum.id = t_panel.stratum
	inner join sdesign.cm_cluster2panel_mapping ON cm_cluster2panel_mapping.panel = t_panel.id
	group by w_configuration.id, f_a_sampling_stratum_gid, lambda_d_plus, plots_per_cluster
)
, w_pix AS MATERIALIZED (-------------------------INCLUSION DENSITY PIX
	SELECT
		w_clusters.conf_id,
		w_clusters.stratum,
		w_clusters.cluster,
		v_strata_sum.sweight_strata_sum / (v_strata_sum.lambda_d_plus * cm_cluster2panel_mapping.sampling_weight) as pix
	FROM w_clusters
        INNER JOIN sdesign.cm_cluster2panel_mapping ON (w_clusters.cluster = cm_cluster2panel_mapping.cluster
								and w_clusters.panel = cm_cluster2panel_mapping.panel)
	INNER JOIN w_strata_sum as v_strata_sum ON w_clusters.stratum = v_strata_sum.f_a_sampling_stratum_gid
)
, w_SIGMA AS MATERIALIZED (
	SELECT
		conf_id,
		1 as r,
		cluster as c,
		case when w_configuration.sigma then (plots_per_cluster^2)::float / plcount::float 
		else 1::float end
		as val
	from (select conf_id, cluster, plots_per_cluster, plcount 
		from w_ldsity_cluster 
		group by conf_id, cluster, plots_per_cluster, plcount) as m
	inner join w_configuration on (m.conf_id = w_configuration.id)
	order by r, c
)
, w_PI AS MATERIALIZED (
	SELECT
		conf_id,
		1 as r,
		cluster as c,
		1.0 / pix as val
	from w_pix
	order by r, c
)
, w_SIGMA_PI AS MATERIALIZED (
	SELECT
		A.conf_id,
		1 as r,
		A.c as c,
		A.val * B.val as val
	from w_SIGMA as A inner join w_PI as B on (A.c = B.c and A.conf_id = B.conf_id) 
	order by r, c
)
, w_XT AS MATERIALIZED (
	SELECT
		conf_id,
		c as r,
		r as c,
		val as val
	from w_X
	order by r, c
)
, w_X_SIGMA_PI AS MATERIALIZED ( -- element-wise multiplication
	select 
		A.conf_id,
		A.r, 
		A.c,
		A.val * B.val as val
	from w_X as A
	inner join w_SIGMA_PI as B on (A.c = B.c and A.conf_id = B.conf_id)
	ORDER BY r, c
)
, w_X_SIGMA_PI_XT AS MATERIALIZED ( -- matrix multiplication
	SELECT 
		A.conf_id,
		ROW_NUMBER() OVER (partition by A.conf_id order by A.r, B.c) AS mid,
		A.r, 
		B.c, 
		sum(A.val * B.val) as val
	FROM w_X_SIGMA_PI as A, w_XT as B
	WHERE A.c = B.r and A.conf_id = B.conf_id
	GROUP BY A.r, B.c, A.conf_id
	ORDER BY r, c
)
, w_agg AS MATERIALIZED (
	select conf_id, r, array_agg(val order by c) as val from w_X_SIGMA_PI_XT group by conf_id, r
)
, w_aggagg AS MATERIALIZED (
	select conf_id, array_agg(val order by r) as val from w_agg group by conf_id
)
, w_inv AS MATERIALIZED (
	select conf_id, @extschema@.fn_inverse(val) AS val from w_aggagg
)
, w_inv_id AS MATERIALIZED (
	select conf_id, mid, invval from w_inv, unnest(w_inv.val) WITH ORDINALITY AS t(invval, mid)
)
, w_X_SIGMA_PI_XT_inv AS MATERIALIZED (
	SELECT 
		conf_id, r, c, invval as val 
	FROM w_X_SIGMA_PI_XT 
	inner join w_inv_id using (conf_id, mid)
	ORDER BY r, c
)
, w_X_SIGMA_PI_XT_inv_X AS MATERIALIZED (
	select
		A.conf_id,
		A.r,
		B.c,
		sum(A.val * B.val) as val
	FROM
		w_X_SIGMA_PI_XT_inv as A, w_X as B
	WHERE A.c = B.r and A.conf_id = B.conf_id
	GROUP BY A.r, B.c, A.conf_id
	ORDER BY r, c
)
, w_G_beta AS MATERIALIZED (
	select
		A.conf_id,
		A.r,
		A.c,
		A.val * B.val as val
	FROM
		w_X_SIGMA_PI_XT_inv_X as A, w_SIGMA as B
	WHERE A.c = B.c and A.conf_id = B.conf_id
	ORDER BY r, c
)
select r as variable, c as cluster, val from w_G_beta;
        ';

	--RAISE NOTICE '%', _complete_query;
	RETURN QUERY EXECUTE _complete_query;
END;
$BODY$
  LANGUAGE plpgsql STABLE
  COST 100
  ROWS 100000;
;

COMMENT ON FUNCTION @extschema@.fn_G_beta(integer) IS 'Function computing matrix G_beta used for regression estimators. G_beta is dependent on model and parametrization domain (not cell). Matrix is represented by variable (row) and cluster (column) indices.';

-- </function>
