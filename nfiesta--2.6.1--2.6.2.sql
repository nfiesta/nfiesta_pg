--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

/*
	correct default privileges
*/

GRANT SELECT ON TABLE @extschema@.t_available_datasets TO PUBLIC;
GRANT SELECT, USAGE ON @extschema@.t_available_datasets_id_seq TO PUBLIC;

GRANT SELECT ON TABLE @extschema@.t_stratum_in_estimation_cell TO PUBLIC;
GRANT SELECT, USAGE ON @extschema@.t_stratum_in_estimation_cell_id_seq TO PUBLIC;


COMMENT ON FUNCTION @extschema@.fn_get_panels_in_estimation_cell(integer, date, date, integer) IS 'Function returns list of stratas and panels in the estimation cell. For each panel there is an indicator (reference_year_set_fit) if the panel lies within estimation period and indicator max which tells if the panel is the one with the maximum possible plots.';
COMMENT ON FUNCTION @extschema@.fn_1p_est_configuration(integer[],integer[],integer[],integer,date,date,varchar,integer) IS 'Function makes the estimate configuration present in the database - provides inserts into the all necessary tables.';
COMMENT ON FUNCTION @extschema@.fn_est_configuration(integer, date, date, varchar, integer, integer[]) IS 'Wrapper function. Handles data between fn_get_panels_in_estimation_cell and fn_1p_est_configuration.';
