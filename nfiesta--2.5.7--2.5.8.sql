--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

/*
	addition of metadata column to c_target_variable
	addition of m:n relation between c_target_variable and c_variable_type
*/

ALTER TABLE @extschema@.c_target_variable ADD COLUMN metadata json;

COMMENT ON COLUMN @extschema@.c_target_variable.metadata IS 'Metadata of the target variable (except variable_type). It is intended to be used as a general purpose column to describe the target variable. The json should contain only one level of metadata parameters which can be described in the second level. This two levels will be used in the GUI - first level indicates the checkbox, the second level indicates the content of the checkbox. The main goal is to choose from the miriad of target variables in user-friedly manner. An example can be growing stock of living trees: {"result" : {"id" : 10, "label" : "growing stock"}, "unit_of_measure" : {"id" : 2, "label" : "m3"}, "population" : {"id" : 23, "label" : "living trees abobe 7 cm DBH"}}';

CREATE TABLE @extschema@.cm_tvariable2variable_type (
id		serial NOT NULL,
target_variable	integer NOT NULL,
variable_type	integer NOT NULL,
CONSTRAINT pkey__cm_tvariable2variable_type__id PRIMARY KEY (id)
);

ALTER TABLE @extschema@.cm_tvariable2variable_type 
ADD CONSTRAINT fkey__tvariable2variable_type__c_variable_type
FOREIGN KEY (variable_type) REFERENCES @extschema@.c_variable_type(id);

ALTER TABLE @extschema@.cm_tvariable2variable_type 
ADD CONSTRAINT fkey__tvariable2variable_type__c_target_variable
FOREIGN KEY (target_variable) REFERENCES @extschema@.c_target_variable(id);

COMMENT ON TABLE @extschema@.cm_tvariable2variable_type IS 'Mapping between variable type and target variables.';
COMMENT ON COLUMN @extschema@.cm_tvariable2variable_type.id IS 'Identificator of the record, primary key.';
COMMENT ON COLUMN @extschema@.cm_tvariable2variable_type.target_variable IS 'Target variable, foreign key to c_target_variable.';
COMMENT ON COLUMN @extschema@.cm_tvariable2variable_type.variable_type IS 'Variable type, foreign key to c_variable_type.';

SELECT pg_catalog.pg_extension_config_dump('@extschema@.cm_tvariable2variable_type', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.cm_tvariable2variable_type_id_seq', '');

INSERT INTO @extschema@.cm_tvariable2variable_type(target_variable, variable_type)
SELECT	t1.id, t2.id
FROM @extschema@.c_target_variable AS t1
INNER JOIN @extschema@.c_variable_type AS t2
ON t1.variable_type = t2.id
ORDER BY t1.id, t2.id;

ALTER TABLE @extschema@.c_target_variable
DROP COLUMN variable_type;

-- update of unique
-- label is not enough, each target variable can be different only in some minor metadata parameter
ALTER TABLE @extschema@.c_target_variable DROP CONSTRAINT c_target_variable_label_key;

