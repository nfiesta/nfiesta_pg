--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
DROP TRIGGER trg__available_datasets__ins ON @extschema@.t_available_datasets;
DROP TRIGGER trg__available_datasets__upd ON @extschema@.t_available_datasets;
DROP TRIGGER trg__available_datasets__del ON @extschema@.t_available_datasets;

--------------------------------------------------------------------------------------------------------------------------------

--DROP TABLE IF EXISTS @extschema@.t_additivity_set_plot;

CREATE TABLE IF NOT EXISTS @extschema@.t_additivity_set_plot
(
	id 			serial NOT NULL,
	available_datasets	integer,
	edges 			integer[],
	add_check_treshold 	double precision,
	valid_from 		timestamp with time zone default now(),
	dbg_data		jsonb,
	CONSTRAINT pkey__t_additivity_set_plot PRIMARY KEY (id),
	CONSTRAINT fkey__t_additivity_set_plot__available_datasets FOREIGN KEY (available_datasets) REFERENCES  @extschema@.t_available_datasets(id)
);

COMMENT ON TABLE @extschema@.t_additivity_set_plot IS 'Table for additivity checks triggers.';

GRANT SELECT, INSERT, UPDATE, DELETE 	ON TABLE  	@extschema@.t_additivity_set_plot 		TO app_nfiesta;
GRANT USAGE, SELECT, UPDATE 		ON SEQUENCE	@extschema@.t_additivity_set_plot_id_seq 	TO app_nfiesta;

SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_additivity_set_plot', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_additivity_set_plot_id_seq', '');

DROP SEQUENCE IF EXISTS @extschema@.t_additivity_set_plot_progress_monitor_seq;
CREATE SEQUENCE @extschema@.t_additivity_set_plot_progress_monitor_seq;
GRANT USAGE, SELECT, UPDATE 		ON SEQUENCE	@extschema@.t_additivity_set_plot_progress_monitor_seq 	TO app_nfiesta;
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_additivity_set_plot_progress_monitor_seq', '');

--------------------------------------------------------------------------------------------------------------------------------

alter view @extschema@.v_variable_hierarchy_plot rename to v_variable_hierarchy_plot_deprecated;
alter view @extschema@.v_variable_hierarchy_plot_aux rename to v_variable_hierarchy_plot_aux_deprecated;

-- <function name="fn_add_triggers" schema="extschema" src="functions/extschema/additivity/fn_add_triggers.sql">
--
-- Copyright 2021, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

CREATE OR REPLACE FUNCTION @extschema@.fn_prevent_update()
  RETURNS trigger AS
$BODY$
    BEGIN
        RAISE EXCEPTION 'fn_prevent_update -- UPDATE -- only update of is_latests is possible';
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

drop trigger if exists trg__target_data__pr_upd ON @extschema@.t_target_data;
CREATE TRIGGER trg__target_data__pr_upd
BEFORE UPDATE OF id, plot, value, value_inserted, available_datasets -- only update of is_latests is possible
ON @extschema@.t_target_data
FOR EACH ROW
EXECUTE PROCEDURE @extschema@.fn_prevent_update();

--------------------------------------------------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION @extschema@.fn_check_update()
  RETURNS trigger AS
$BODY$
    BEGIN
	IF (old.is_latest = false AND new.is_latest = true) THEN
	        RAISE EXCEPTION 'fn_check_update -- UPDATE -- only set is_latest to false is possible. use INSERT for new valid values';
	END IF;
	RETURN NEW;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

drop trigger if exists trg__target_data__ch_upd ON @extschema@.t_target_data;
CREATE TRIGGER trg__target_data__ch_upd
AFTER UPDATE OF is_latest
ON @extschema@.t_target_data
FOR EACH ROW
EXECUTE PROCEDURE @extschema@.fn_check_update();

--------------------------------------------------------------------------------------------------------------------------------

--drop function @extschema@.fn_update_last_change() cascade;
CREATE OR REPLACE FUNCTION @extschema@.fn_update_last_change() RETURNS TRIGGER AS $src$
    DECLARE
    BEGIN
	IF (TG_TABLE_NAME = 't_target_data' or TG_TABLE_NAME = 't_auxiliary_data') THEN
        	IF (TG_OP = 'DELETE') THEN
			RAISE EXCEPTION 'fn_update_last_change -- DELETE not permited (UPDATE to is_latest = false instead)';
		ELSIF (TG_OP = 'UPDATE') THEN
			with w_data as (
				select 
					available_datasets, now() as max_val_upd
				from trans_table
				group by available_datasets
			)
			update @extschema@.t_available_datasets 
			set last_change = w_data.max_val_upd
			from w_data
			where t_available_datasets.id = w_data.available_datasets
			;
			/*raise notice '%		fn_update_last_change -- % -- %: () %', 
				clock_timestamp()::timestamp with time zone at time zone 'Europe/Prague', 
				TG_TABLE_NAME, TG_OP, 
				(select json_agg(r) 
					from (
						select
							json_build_object(
								'available_datasets', available_datasets, 
								'max_val_upd', now()
							) as r
						from trans_table
						group by available_datasets
					) as foo
				);*/
		ELSIF (TG_OP = 'INSERT') THEN
			with w_data as (
				select 
					available_datasets, max(value_inserted) as max_val_ins
				from trans_table
				group by available_datasets
			)
			update @extschema@.t_available_datasets 
			set last_change = w_data.max_val_ins
			from w_data
			where t_available_datasets.id = w_data.available_datasets
			;
			/*raise notice '%		fn_update_last_change -- % -- %: () %', 
				clock_timestamp()::timestamp with time zone at time zone 'Europe/Prague', 
				TG_TABLE_NAME, TG_OP, 
				(select json_agg(r)
					from (
						select 
							json_build_object(
								'available_datasets', available_datasets, 
								'max_val_ins', max(value_inserted)
						) as r
						from trans_table
						group by available_datasets
					) as foo
				);*/
		ELSE
			RAISE EXCEPTION 'fn_update_last_change -- trigger operation not known: %', TG_OP;
		END IF;
	ELSE
		RAISE EXCEPTION 'fn_update_last_change -- % -- table name not known: %', TG_OP, TG_TABLE_NAME;
	END IF;
        RETURN NULL; -- result is ignored since this is an AFTER trigger
    END;
$src$ LANGUAGE plpgsql;

drop trigger if exists trg__target_data__ins ON @extschema@.t_target_data;
CREATE TRIGGER trg__target_data__ins
    AFTER INSERT ON @extschema@.t_target_data
    REFERENCING NEW TABLE AS trans_table
    FOR EACH STATEMENT EXECUTE FUNCTION @extschema@.fn_update_last_change();

drop trigger if exists trg__target_data__upd ON @extschema@.t_target_data;
CREATE TRIGGER trg__target_data__upd
    AFTER UPDATE ON @extschema@.t_target_data
    REFERENCING NEW TABLE AS trans_table
    FOR EACH STATEMENT EXECUTE FUNCTION @extschema@.fn_update_last_change();

drop trigger if exists trg__target_data__del ON @extschema@.t_target_data;
CREATE TRIGGER trg__target_data__del
    AFTER DELETE ON @extschema@.t_target_data
    REFERENCING OLD TABLE AS trans_table
    FOR EACH STATEMENT EXECUTE FUNCTION @extschema@.fn_update_last_change();

---------------------------------------------
drop trigger if exists trg__auxiliary_data__ins ON @extschema@.t_auxiliary_data;
CREATE TRIGGER trg__auxiliary_data__ins
    AFTER INSERT ON @extschema@.t_auxiliary_data
    REFERENCING NEW TABLE AS trans_table
    FOR EACH STATEMENT EXECUTE FUNCTION @extschema@.fn_update_last_change();

drop trigger if exists trg__auxiliary_data__upd ON @extschema@.t_auxiliary_data;
CREATE TRIGGER trg__auxiliary_data__upd
    AFTER UPDATE ON @extschema@.t_auxiliary_data
    REFERENCING NEW TABLE AS trans_table
    FOR EACH STATEMENT EXECUTE FUNCTION @extschema@.fn_update_last_change();

drop trigger if exists trg__auxiliary_data__del ON @extschema@.t_auxiliary_data;
CREATE TRIGGER trg__auxiliary_data__del
    AFTER DELETE ON @extschema@.t_auxiliary_data
    REFERENCING OLD TABLE AS trans_table
    FOR EACH STATEMENT EXECUTE FUNCTION @extschema@.fn_update_last_change();


---------------------------------------------

CREATE OR REPLACE FUNCTION @extschema@.fn_refresh_t_additivity_set_plot()
RETURNS trigger LANGUAGE plpgsql AS $$
BEGIN
	--RAISE NOTICE 'fn_refresh_t_additivity_set_plot -- % -- %', TG_OP, TG_TABLE_NAME;
	IF (TG_TABLE_NAME = 't_variable_hierarchy') THEN
    	IF (TG_OP = 'DELETE') THEN
        	RAISE EXCEPTION 'fn_refresh_t_additivity_set_plot -- DELETE on table t_variable_hierarchy not permited';
        ELSIF (TG_OP = 'INSERT' OR TG_OP = 'UPDATE') THEN
			with w_data as (
				SELECT 	node_ads.id as node_ads_id,
					t_variable_hierarchy.variable_superior AS node,
					array_agg(edge_ads.id) as edge_ads_id,
					array_agg(t_variable_hierarchy.variable ORDER BY t_variable_hierarchy.variable) AS edges,
					10e-6 as add_check_treshold
				FROM @extschema@.t_variable_hierarchy
				JOIN @extschema@.t_variable 				AS node_var ON t_variable_hierarchy.variable_superior = node_var.id
				JOIN @extschema@.t_available_datasets 			AS node_ads ON node_var.id = node_ads.variable
				JOIN @extschema@.t_variable 				AS edge_var ON t_variable_hierarchy.variable = edge_var.id
				JOIN @extschema@.t_available_datasets 			AS edge_ads ON edge_var.id = edge_ads.variable
				LEFT JOIN @extschema@.c_sub_population_category		ON edge_var.sub_population_category = c_sub_population_category.id
				LEFT JOIN @extschema@.c_area_domain_category 		ON edge_var.area_domain_category = c_area_domain_category.id
				LEFT JOIN @extschema@.c_auxiliary_variable_category	ON edge_var.auxiliary_variable_category = c_auxiliary_variable_category.id
				WHERE
					node_ads.panel = edge_ads.panel				AND node_ads.reference_year_set = edge_ads.reference_year_set
					AND node_var.auxiliary_variable_category IS NULL	AND edge_var.auxiliary_variable_category IS NULL
				GROUP BY 	node_ads.id, node_ads.panel, node_ads.reference_year_set, t_variable_hierarchy.variable_superior,
						c_sub_population_category.sub_population, c_area_domain_category.area_domain, c_auxiliary_variable_category.auxiliary_variable
				ORDER BY 	node_ads.id, node_ads.panel, node_ads.reference_year_set, t_variable_hierarchy.variable_superior,
						c_sub_population_category.sub_population, c_area_domain_category.area_domain, c_auxiliary_variable_category.auxiliary_variable
			),
			w_distinct_selection as (
				select distinct w_data.node_ads_id, w_data.edges, w_data.add_check_treshold
				from w_data join trans_table on
				(
					trans_table.variable_superior = w_data.node
					OR
					trans_table.variable = any (w_data.edges)
				)
			)
			INSERT INTO @extschema@.t_additivity_set_plot(available_datasets, edges, add_check_treshold, dbg_data)
			select
				node_ads_id, edges, add_check_treshold,
				json_build_object(
					'fn_refresh_t_additivity_set_plot', json_build_object(
						'TG_TABLE_NAME', TG_TABLE_NAME, 
						'TG_OP', TG_OP, 
						'trigerred_time', (clock_timestamp()::timestamp with time zone)::text
						)
					) as dbg_data
			from w_distinct_selection
			;
		ELSE
				RAISE EXCEPTION 'fn_refresh_t_additivity_set_plot -- trigger operation not known: %', TG_OP;
		END IF;
	ELSIF (TG_TABLE_NAME = 't_available_datasets') THEN
	IF (TG_OP = 'DELETE') THEN
		RAISE EXCEPTION 'fn_refresh_t_additivity_set_plot -- DELETE on table t_available_datasets not permited';
	ELSIF (TG_OP = 'INSERT' OR TG_OP = 'UPDATE') THEN
			with w_data as (
				SELECT 	node_ads.id as node_ads_id,
					array_agg(edge_ads.id) as edge_ads_id,
					array_agg(t_variable_hierarchy.variable ORDER BY t_variable_hierarchy.variable) AS edges,
					10e-6 as add_check_treshold
				FROM @extschema@.t_variable_hierarchy
				JOIN @extschema@.t_variable 				AS node_var ON t_variable_hierarchy.variable_superior = node_var.id
				JOIN @extschema@.t_available_datasets 			AS node_ads ON node_var.id = node_ads.variable
				JOIN @extschema@.t_variable 				AS edge_var ON t_variable_hierarchy.variable = edge_var.id
				JOIN @extschema@.t_available_datasets 			AS edge_ads ON edge_var.id = edge_ads.variable
				LEFT JOIN @extschema@.c_sub_population_category		ON edge_var.sub_population_category = c_sub_population_category.id
				LEFT JOIN @extschema@.c_area_domain_category 		ON edge_var.area_domain_category = c_area_domain_category.id
				LEFT JOIN @extschema@.c_auxiliary_variable_category	ON edge_var.auxiliary_variable_category = c_auxiliary_variable_category.id
				WHERE
					node_ads.panel = edge_ads.panel				AND node_ads.reference_year_set = edge_ads.reference_year_set
					AND node_var.auxiliary_variable_category IS NULL	AND edge_var.auxiliary_variable_category IS NULL
				GROUP BY 	node_ads.id, node_ads.panel, node_ads.reference_year_set, t_variable_hierarchy.variable_superior,
						c_sub_population_category.sub_population, c_area_domain_category.area_domain, c_auxiliary_variable_category.auxiliary_variable
				ORDER BY 	node_ads.id, node_ads.panel, node_ads.reference_year_set, t_variable_hierarchy.variable_superior,
						c_sub_population_category.sub_population, c_area_domain_category.area_domain, c_auxiliary_variable_category.auxiliary_variable
			),
			w_distinct_selection as (
				select distinct w_data.node_ads_id, w_data.edges, w_data.add_check_treshold
				from w_data join trans_table on
				(
					trans_table.id = w_data.node_ads_id
					OR
					trans_table.id = any (w_data.edge_ads_id)
				)
			)
			INSERT INTO @extschema@.t_additivity_set_plot(available_datasets, edges, add_check_treshold, dbg_data)
			select
				node_ads_id, edges, add_check_treshold,
				json_build_object(
					'fn_refresh_t_additivity_set_plot', json_build_object(
						'TG_TABLE_NAME', TG_TABLE_NAME, 
						'TG_OP', TG_OP, 
						'trigerred_time', (clock_timestamp()::timestamp with time zone)::text
						)
					) as dbg_data
			from w_distinct_selection
			;
		ELSE
				RAISE EXCEPTION 'fn_refresh_t_additivity_set_plot -- trigger operation not known: %', TG_OP;
		END IF;
	ELSE
		RAISE EXCEPTION 'fn_refresh_t_additivity_set_plot -- % -- table name not known: %', TG_OP, TG_TABLE_NAME;
	END IF;
	RETURN NULL;
END;
$$;

--target_data.t_variable_hierarchy
drop trigger if exists trg__t_variable_hierarchy__refresh_t_additivity_set_plot__upd ON @extschema@.t_variable_hierarchy;
CREATE TRIGGER trg__t_variable_hierarchy__refresh_t_additivity_set_plot__upd
AFTER UPDATE ON @extschema@.t_variable_hierarchy
REFERENCING NEW TABLE AS trans_table
FOR EACH STATEMENT EXECUTE PROCEDURE @extschema@.fn_refresh_t_additivity_set_plot();

drop trigger if exists trg__t_variable_hierarchy__refresh_t_additivity_set_plot__ins ON @extschema@.t_variable_hierarchy;
CREATE TRIGGER trg__t_variable_hierarchy__refresh_t_additivity_set_plot__ins
AFTER INSERT ON @extschema@.t_variable_hierarchy
REFERENCING NEW TABLE AS trans_table
FOR EACH STATEMENT EXECUTE PROCEDURE @extschema@.fn_refresh_t_additivity_set_plot();

drop trigger if exists trg__t_variable_hierarchy__refresh_t_additivity_set_plot__del ON @extschema@.t_variable_hierarchy;
CREATE TRIGGER trg__t_variable_hierarchy__refresh_t_additivity_set_plot__del
AFTER DELETE ON @extschema@.t_variable_hierarchy
REFERENCING OLD TABLE AS trans_table
FOR EACH STATEMENT EXECUTE PROCEDURE @extschema@.fn_refresh_t_additivity_set_plot();

/*
delete from @extschema@.t_additivity_set_plot;
select * from @extschema@.t_additivity_set_plot;
select * from @extschema@.t_variable_hierarchy;
update @extschema@.t_variable_hierarchy set variable = 16 where id = 14;
insert into @extschema@.t_variable_hierarchy (id, variable, variable_superior) values (15, 1, 14);
delete from @extschema@.t_variable_hierarchy where id = 15;
*/

--target_data.t_available_datasets
drop trigger if exists trg__t_available_datasets__refresh_t_additivity_set_plot__upd ON @extschema@.t_available_datasets;
CREATE TRIGGER trg__t_available_datasets__refresh_t_additivity_set_plot__upd
AFTER UPDATE ON @extschema@.t_available_datasets
REFERENCING NEW TABLE AS trans_table
FOR EACH STATEMENT EXECUTE PROCEDURE @extschema@.fn_refresh_t_additivity_set_plot();

drop trigger if exists trg__t_available_datasets__refresh_t_additivity_set_plot__ins ON @extschema@.t_available_datasets;
CREATE TRIGGER trg__t_available_datasets__refresh_t_additivity_set_plot__ins
AFTER INSERT ON @extschema@.t_available_datasets
REFERENCING NEW TABLE AS trans_table
FOR EACH STATEMENT EXECUTE PROCEDURE @extschema@.fn_refresh_t_additivity_set_plot();

drop trigger if exists trg__t_available_datasets__refresh_t_additivity_set_plot__del ON @extschema@.t_available_datasets;
CREATE TRIGGER trg__t_available_datasets__refresh_t_additivity_set_plot__del
AFTER DELETE ON @extschema@.t_available_datasets
REFERENCING OLD TABLE AS trans_table
FOR EACH STATEMENT EXECUTE PROCEDURE @extschema@.fn_refresh_t_additivity_set_plot();

/*
delete from @extschema@.t_additivity_set_plot;
select * from @extschema@.t_additivity_set_plot;
select * from @extschema@.t_available_datasets;
update @extschema@.t_available_datasets set last_change = now() where id = 1;
insert into @extschema@.t_available_datasets (id, panel, reference_year_set, variable, last_change) values (50, 1, 2, 8, now());
delete from @extschema@.t_available_datasets where id = 50;
*/

--------------------------------------------------------------------------------------------------------------------------------

--drop function @extschema@.fn_launch_add_check() cascade;
CREATE OR REPLACE FUNCTION @extschema@.fn_launch_add_check() RETURNS TRIGGER AS $src$
DECLARE
		_var_sup int;
		_var_inf int[];
		_panel int;
		_refyearsets int;
		_treshold double precision;
		_errp json;
		_dbg_data jsonb;
BEGIN
	IF (TG_TABLE_NAME = 't_additivity_set_plot') THEN
		IF (TG_OP = 'DELETE') THEN
			RAISE EXCEPTION 'fn_launch_add_check -- DELETE on table fn_launch_add_check not permited';
		ELSIF (TG_OP = 'UPDATE') THEN
			RETURN NULL; -- result is ignored since this is an AFTER trigger
		ELSIF (TG_OP = 'INSERT') THEN
			------------------ _vars_add_set
			select
				t_available_datasets.variable 	into _var_sup
			from @extschema@.t_available_datasets
			where t_available_datasets.id = new.available_datasets;
			select
				new.edges			into _var_inf
			;
		------------------ _plots
			select
				t_available_datasets.panel	into _panel
			from @extschema@.t_available_datasets
			where t_available_datasets.id = new.available_datasets;
			------------------ _refyearsets
			select
				t_available_datasets.reference_year_set into _refyearsets
			from @extschema@.t_available_datasets
			where t_available_datasets.id = new.available_datasets;
			----------------------------------------------------------------------------------------------
			if array_prepend(_var_sup, _var_inf) is not null then
				/*raise notice '%		fn_launch_add_check -- % -- % -- CHECK START -- select target_data.fn_add_plot_target_attr(%, %, %, %, %);', 
					clock_timestamp()::timestamp with time zone at time zone 'Europe/Prague', TG_TABLE_NAME, TG_OP, _vars_add_set, _plots, _refyearsets, 1e-6, true;*/
				_dbg_data = jsonb_build_object(
							'add_check_start', (clock_timestamp()::timestamp with time zone)::text
						);
				--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!TARGET OR AUX!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
				with w_err as (
					select @extschema@.fn_add_plot_target_attr(_var_sup, _var_inf, _panel, _refyearsets, 1e-6, true) as fn_err
				)
				, w_err_t as (
					select (fn_err).* from w_err
				)
				, w_err_json as (
					select 
						json_build_object(
							'plot'				, plot,
							'reference_year_set'		, reference_year_set,
							'variable'			, variable,
							'ldsity'			, ldsity,
							'ldsity_sum'			, ldsity_sum,
							'variables_def'			, variables_def,
							'variables_found'		, variables_found,
							'diff'				, diff
						) as errj
					from w_err_t
				)
				select json_agg(errj) into _errp from w_err_json;
				/*raise notice '%		fn_launch_add_check -- % -- % -- CHECK STOP', 
					clock_timestamp()::timestamp with time zone at time zone 'Europe/Prague', TG_TABLE_NAME, TG_OP;*/

				_dbg_data = jsonb_set(_dbg_data,
						'{add_check_stop}', to_jsonb((clock_timestamp()::timestamp with time zone)::text)
						);

				update @extschema@.t_additivity_set_plot 
				set dbg_data = jsonb_set(dbg_data, '{fn_launch_additivity_check}', _dbg_data) 
				where id = new.id;
				
				IF
					(json_array_length(_errp) > 0)
					THEN RAISE EXCEPTION 'fn_launch_add_check -- % -- % -- plot level local densities are not additive:
						variables in additivity set: sup: %, inf: % , checked panels: %, refyearsets: %,
						found err plots: 
						%', TG_TABLE_NAME, TG_OP, _var_sup, _var_inf, _panel, _refyearsets, 
						jsonb_pretty(_errp::jsonb);
				END IF;
			else
				RAISE WARNING 'fn_launch_add_check -- % -- % -- additivity check was skiped (no variables found in hierarchy):
						variables in additivity set: %, checked panels: %, refyearsets: %'
						, TG_TABLE_NAME, TG_OP, 
						_vars_add_set, _panel, _refyearsets;
			end if;
		ELSE
			RAISE EXCEPTION 'fn_launch_add_check -- trigger operation not known: %', TG_OP;
		END IF;
	ELSE
		RAISE EXCEPTION 'fn_launch_add_check -- % -- table name not known: %', TG_OP, TG_TABLE_NAME;
	END IF;
RETURN NULL; -- result is ignored since this is an AFTER trigger
END;
$src$ LANGUAGE plpgsql;

drop trigger if exists trg__additivity_set_plot__ins on @extschema@.t_additivity_set_plot;
CREATE CONSTRAINT TRIGGER trg__additivity_set_plot__ins
    AFTER INSERT ON @extschema@.t_additivity_set_plot
    DEFERRABLE INITIALLY DEFERRED
    FOR EACH ROW EXECUTE FUNCTION @extschema@.fn_launch_add_check();

drop trigger if exists trg__additivity_set_plot__upd on @extschema@.t_additivity_set_plot;
CREATE CONSTRAINT TRIGGER trg__additivity_set_plot__upd
    AFTER UPDATE ON @extschema@.t_additivity_set_plot
    DEFERRABLE INITIALLY DEFERRED
    FOR EACH ROW EXECUTE FUNCTION @extschema@.fn_launch_add_check();

drop trigger if exists trg__additivity_set_plot__del on @extschema@.t_additivity_set_plot;
CREATE CONSTRAINT TRIGGER trg__additivity_set_plot__del
    AFTER DELETE ON @extschema@.t_additivity_set_plot
    DEFERRABLE INITIALLY DEFERRED
    FOR EACH ROW EXECUTE FUNCTION @extschema@.fn_launch_add_check();

-- </function>

DROP FUNCTION @extschema@.fn_add_plot_target_attr(integer[], integer[], integer[], double precision, boolean);
-- <function name="fn_add_plot_target_attr" schema="extschema" src="functions/extschema/additivity/fn_add_plot_target_attr.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_add_plot_target_attr(integer, integer[], integer, integer, double precision, boolean)

-- DROP FUNCTION @extschema@.fn_add_plot_target_attr(integer, integer[], integer, integer, double precision, boolean);
CREATE OR REPLACE FUNCTION @extschema@.fn_add_plot_target_attr(
	IN var_sup integer,
	IN var_inf integer[],
	IN panel integer,
	IN refyearsets integer,
	IN min_diff double precision default 0.0, 
	IN include_null_diff boolean default true
)
  RETURNS TABLE(
	plot			integer,
	reference_year_set	integer,
	variable		integer,
	ldsity			double precision,
	ldsity_sum		double precision,
	variables_def		integer[],
	variables_found		integer[],
	diff			double precision
) AS
$BODY$
DECLARE
	_complete_query text;
	_array_text_v text;
	_array_text_v_sup text;
	_array_text_v_inf text;
	_array_text_p text;
	_array_text_r text;

BEGIN
	--------------------------------QUERY--------------------------------
	_array_text_v := concat(quote_literal(array_prepend(var_sup, var_inf)::text), '::integer[]');
	_array_text_v_sup := concat(var_sup::text, '::integer');
	_array_text_v_inf := concat(quote_literal(var_inf::text), '::integer[]');
	if panel is not null then
		_array_text_p := concat(' AND t_available_datasets.panel = ', panel::text);
	else
		_array_text_p := '';
	end if;

	if refyearsets is not null then
		_array_text_r := concat(' AND t_available_datasets.reference_year_set = ', refyearsets::text);
	else
		_array_text_r := '';
	end if;

	--raise notice 'variables: %',  _array_text_v;
	_complete_query := '
with w_plot_var as materialized (
	with w_plot_panel as (
		select
			f_p_plot.gid as plot, t_panel.id as panel 
		from	sdesign.t_panel
		join 	sdesign.cm_cluster2panel_mapping 		on cm_cluster2panel_mapping.panel = t_panel.id
		join 	sdesign.t_cluster 				on t_cluster.id = cm_cluster2panel_mapping."cluster"
		join 	sdesign.f_p_plot 				on f_p_plot."cluster" = t_cluster.id
		join 	sdesign.cm_plot2cluster_config_mapping 		on cm_plot2cluster_config_mapping.plot = f_p_plot.gid
		join 	sdesign.t_cluster_configuration 		on (t_cluster_configuration.id = t_panel.cluster_configuration 
									and cm_plot2cluster_config_mapping.cluster_configuration = t_cluster_configuration.id)
	)
	select 
		w_plot_panel.plot,
		t_available_datasets.panel, t_available_datasets.reference_year_set, t_available_datasets.variable,
		coalesce(t_target_data.value, 0) as value
	from w_plot_panel
	join @extschema@.t_available_datasets	on (t_available_datasets.panel = w_plot_panel.panel)
	left join @extschema@.t_target_data	on (t_target_data.plot = w_plot_panel.plot 
						and t_target_data.available_datasets = t_available_datasets.id
						and t_target_data.is_latest)
	where 	t_available_datasets.variable = any (' || _array_text_v || ')
                        ' || _array_text_p || '
                        ' || _array_text_r || '
)
, w_node_sum as (
	select
		w_plot_var.plot,
		w_plot_var.panel,
		w_plot_var.reference_year_set,
		w_plot_var.variable as node,
		w_plot_var.value as node_sum,
		' || _array_text_v_inf || ' as edges_def
	from w_plot_var
	where w_plot_var.variable = ' || _array_text_v_sup || '
)
, w_edge_sum as (
	select
		w_node_sum.plot,
		w_node_sum.reference_year_set,
		w_node_sum.node,
		w_node_sum.node_sum,
		w_node_sum.edges_def,
		array_agg(w_plot_var.variable order by w_plot_var.variable) as edges_found,
		sum(w_plot_var.value) as edges_sum
	from w_node_sum
	left join w_plot_var on (
		w_plot_var.plot = w_node_sum.plot
		and w_plot_var.panel = w_node_sum.panel
		and w_plot_var.reference_year_set = w_node_sum.reference_year_set
		and w_plot_var.variable = any(w_node_sum.edges_def))
	group by w_node_sum.plot, w_node_sum.reference_year_set,
		w_node_sum.node, w_node_sum.node_sum, w_node_sum.edges_def
)
, w_diff as (
	select
		plot,
		reference_year_set,
		node		as variable,
		node_sum	as ldsity,
		edges_sum	as ldsity_sum,
		edges_def	as variables_def,
		edges_found	as variables_found,
		case
			when edges_sum != 0.0 and node_sum = 0.0 then 1.0
			when node_sum = 0.0 and edges_sum = 0.0 then 0
			else abs( (node_sum - edges_sum) / node_sum )
		end as diff
	from w_edge_sum
)
select * from w_diff
where
	(' || include_null_diff || ' and ((diff >= ' || min_diff || ') or (diff is null)))
	or
	(not ' || include_null_diff || ' and (diff >= ' || min_diff || '))
order by diff desc
;
';

	--RAISE NOTICE '%', _complete_query;
	RETURN QUERY EXECUTE _complete_query;
END;
$BODY$
  LANGUAGE plpgsql STABLE
  COST 100
  ROWS 100000;

comment on function @extschema@.fn_add_plot_target_attr(integer, integer[], integer, integer, double precision, boolean) is
'Function showing plot level target local density attribute additivity (aggregated class estimate should be equal to sum of sub-classes estimates) of target local densities. Hierarchy between aggregated classes and its sub-classes is defined in v_variable_hierarchy_plot.
Function input argument is:
 * Superior attribute -- variables (FKEY to t_variable.id). Aggregated class.
 * Array of inferior attributes -- variables (FKEY to t_variable.id). Sub-classes.
 * Array of panels -- panels (FKEY to t_panel.id).
 * Array of reference year sets -- reference year sets (FKEY to t_reference_year_set.id). If NULL all available reference year sets are checked.
 * Minimal amount of difference [%].
 * Whether include NULL difference -- indicating defined sub-classes missing in data.
Resulting table has following columns:
 * Plot. FKEY to f_p_plot.gid.
 * Reference year set. FKEY to t_reference_year_set.id.
 * Target total attribute -- variable. FKEY to t_variable.id.
 * Aggregated class local density.
 * Sum of sub-classes local densities (belonging to aggregated class).
 * Attributes -- variables defined in hierarchy (v_variable_hierarchy_plot). Array of FKEYs to t_variable.id.
 * Attributes -- variables found in data (t_auxiliary_data). Array of FKEYs to t_variable.id.
 * Relative difference between aggregated class local densities and sum of sub-classes local densities.';

GRANT EXECUTE ON FUNCTION @extschema@.fn_add_plot_target_attr(integer, integer[], integer, integer, double precision, boolean) TO PUBLIC;

-- </function>

DROP FUNCTION @extschema@.fn_add_plot_aux_attr(integer[], double precision, boolean);
-- <function name="fn_add_plot_aux_attr" schema="extschema" src="functions/extschema/additivity/fn_add_plot_aux_attr.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_add_plot_aux_attr(integer, integer[], integer, double precision, boolean)

-- DROP FUNCTION @extschema@.fn_add_plot_aux_attr(integer, integer[], integer, double precision, boolean);

CREATE OR REPLACE FUNCTION @extschema@.fn_add_plot_aux_attr(
	IN var_sup integer,
	IN var_inf integer[],
	IN panel integer,
	IN min_diff double precision default 0.0,
	IN include_null_diff boolean default true
)
  RETURNS TABLE(
	plot			integer,
	variable		integer,
	ldsity			double precision,
	ldsity_sum		double precision,
	variables_def		integer[],
	variables_found		integer[],
	diff			double precision
) AS
$BODY$
DECLARE
	_complete_query text;
	_array_text_v text;
	_array_text_v_sup text;
	_array_text_v_inf text;
	_array_text_p text;
BEGIN
	--------------------------------QUERY--------------------------------
	_array_text_v := concat(quote_literal(array_prepend(var_sup, var_inf)::text), '::integer[]');
	_array_text_v_sup := concat(var_sup::text, '::integer');
	_array_text_v_inf := concat(quote_literal(var_inf::text), '::integer[]');
	if panel is not null then
		_array_text_p := concat(' AND t_available_datasets.panel = ', panel::text);
	else
		_array_text_p := '';
	end if;
	--raise notice 'variables: %',  _array_text;
	_complete_query := '

with w_plot_var as materialized (
	with w_plot_panel as (
		select
			f_p_plot.gid as plot, t_panel.id as panel 
		from	sdesign.t_panel
		join 	sdesign.cm_cluster2panel_mapping 		on cm_cluster2panel_mapping.panel = t_panel.id
		join 	sdesign.t_cluster 				on t_cluster.id = cm_cluster2panel_mapping."cluster"
		join 	sdesign.f_p_plot 				on f_p_plot."cluster" = t_cluster.id
		join 	sdesign.cm_plot2cluster_config_mapping 		on cm_plot2cluster_config_mapping.plot = f_p_plot.gid
		join 	sdesign.t_cluster_configuration 		on (t_cluster_configuration.id = t_panel.cluster_configuration 
									and cm_plot2cluster_config_mapping.cluster_configuration = t_cluster_configuration.id)
	)
	select 
		w_plot_panel.plot, 
		t_available_datasets.panel, t_available_datasets.reference_year_set, t_available_datasets.variable,
		coalesce(t_auxiliary_data.value, 0) as value
	from w_plot_panel
	join @extschema@.t_available_datasets	on (t_available_datasets.panel = w_plot_panel.panel)
	left join @extschema@.t_auxiliary_data	on (t_auxiliary_data.plot = w_plot_panel.plot 
						and t_auxiliary_data.available_datasets = t_available_datasets.id
						and t_auxiliary_data.is_latest)
	where 	t_available_datasets.variable = any (' || _array_text_v || ')
                        ' || _array_text_p || '

)
, w_node_sum as (
	select
		w_plot_var.plot,
		w_plot_var.panel,

		w_plot_var.variable as node,
		w_plot_var.value as node_sum,
		' || _array_text_v_inf || ' as edges_def
	from w_plot_var
	where w_plot_var.variable = ' || _array_text_v_sup || '
)
, w_edge_sum as (
	select
		w_node_sum.plot,

		w_node_sum.node,
		w_node_sum.node_sum,
		w_node_sum.edges_def,
		array_agg(w_plot_var.variable order by w_plot_var.variable) as edges_found,
		sum(w_plot_var.value) as edges_sum
	from w_node_sum
	left join w_plot_var on (
		w_plot_var.plot = w_node_sum.plot
		and w_plot_var.panel = w_node_sum.panel
		
		and w_plot_var.variable = any(w_node_sum.edges_def))
	group by w_node_sum.plot,
		w_node_sum.node, w_node_sum.node_sum, w_node_sum.edges_def
)
, w_diff as (
	select
		plot,

		node		as variable,
		node_sum	as ldsity,
		edges_sum	as ldsity_sum,
		edges_def	as variables_def,
		edges_found	as variables_found,
		case
			when edges_sum != 0.0 and node_sum = 0.0 then 1.0
			when node_sum = 0.0 and edges_sum = 0.0 then 0
			else abs( (node_sum - edges_sum) / node_sum )
		end as diff
	from w_edge_sum
)
select * from w_diff
where
	(' || include_null_diff || ' and ((diff >= ' || min_diff || ') or (diff is null)))
	or
	(not ' || include_null_diff || ' and (diff >= ' || min_diff || '))
order by diff desc
;
';

	--RAISE NOTICE '%', _complete_query;
	RETURN QUERY EXECUTE _complete_query;
END;
$BODY$
  LANGUAGE plpgsql STABLE
  COST 100
  ROWS 100000;

comment on function @extschema@.fn_add_plot_aux_attr(integer, integer[], integer, double precision, boolean) is
'Function showing plot level auxiliary local density attribute additivity (aggregated class estimate should be equal to sum of sub-classes estimates) of auxiliary local densities. Hierarchy between aggregated classes and its sub-classes is defined in v_variable_hierarchy_plot.
Function input argument is:
 * Superior attribute -- variables (FKEY to t_variable.id). Aggregated class.
 * Array of inferior attributes -- variables (FKEY to t_variable.id). Sub-classes.
 * Array of panels -- panels (FKEY to t_panel.id).
 * Minimal amount of difference [%].
 * Whether include NULL difference -- indicating defined sub-classes missing in data.
Resulting table has following columns:
 * Plot. FKEY to f_p_plot.gid.
 * Auxiliary total attribute -- variable. FKEY to t_variable.id.
 * Aggregated class local density.
 * Sum of sub-classes local densities (belonging to aggregated class).
 * Attributes -- variables defined in hierarchy (v_variable_hierarchy_plot). Array of FKEYs to t_variable.id.
 * Attributes -- variables found in data (t_auxiliary_data). Array of FKEYs to t_variable.id.
 * Relative difference between aggregated class local densities and sum of sub-classes local densities.';

GRANT EXECUTE ON FUNCTION @extschema@.fn_add_plot_aux_attr(integer, integer[], integer, double precision, boolean) TO PUBLIC;

-- </function>
