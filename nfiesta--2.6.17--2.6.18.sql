--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- <function name="fn_add_aux_total_attr" schema="extschema" src="functions/extschema/additivity/fn_add_aux_total_attr.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_add_aux_total_attr(integer)

-- DROP FUNCTION @extschema@.fn_add_aux_total_attr(integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_add_aux_total_attr(
	IN variables integer[],
	IN min_diff double precision default 0.0, IN include_null_diff boolean default true
)
  RETURNS TABLE(
	estimation_cell		integer,
	variable		integer,
	aux_total		double precision,
	aux_total_sum		double precision,
	variables_def		integer[],
	variables_found		integer[],
	diff			double precision
) AS
$BODY$
DECLARE
	_complete_query text;
	_array_text text;
BEGIN
	--------------------------------QUERY--------------------------------
	_array_text := concat(quote_literal(variables::text), '::integer[]');
	--raise notice 'variables: %',  _array_text;
	_complete_query := '
with w_auxtotal_cell_var as not materialized (
	select
		t_aux_total.estimation_cell,
		t_aux_total.aux_total,
		t_aux_total.variable
	from @extschema@.t_aux_total
	where t_aux_total.is_latest
	and t_aux_total.variable = ANY (' || _array_text || ')
)
, w_node_sum as (
	select
		estimation_cell,
		variable,
		sum(aux_total) as node_sum,
		hierarchy.node,
		hierarchy.edges as edges_def
	from w_auxtotal_cell_var
	inner join @extschema@.v_variable_hierarchy		as hierarchy on (hierarchy.node = w_auxtotal_cell_var.variable)
	group by estimation_cell, variable, node, edges
	order by estimation_cell, variable, node, edges
)
, w_edge_sum as (
	select
		w_node_sum.estimation_cell,
		w_node_sum.node,
		w_node_sum.node_sum,
		w_node_sum.edges_def,
		array_agg(w_auxtotal_cell_var.variable order by w_auxtotal_cell_var.variable) as edges_found,
		sum(w_auxtotal_cell_var.aux_total) as edges_sum
	from w_node_sum
	left join w_auxtotal_cell_var on (
		w_auxtotal_cell_var.estimation_cell = w_node_sum.estimation_cell
		and w_auxtotal_cell_var.variable = any(w_node_sum.edges_def))
	group by w_node_sum.estimation_cell, w_node_sum.node, w_node_sum.node_sum, w_node_sum.edges_def
)
, w_diff as (
	select
		estimation_cell,
		node			as variable,
		node_sum		as aux_total,
		edges_sum		as aux_total_sum,
		edges_def		as variables_def,
		edges_found		as variables_found,
		case
			when node_sum != 0.0 and edges_sum = 0.0 then 100.0
			when node_sum = 0.0 and edges_sum = 0.0 then 0
			else abs(1 - (node_sum / edges_sum )) * 100.0
		end as diff
	from w_edge_sum
)
select * from w_diff
where
	(' || include_null_diff || ' and ((diff >= ' || min_diff || ') or (diff is null)))
	or
	(not ' || include_null_diff || ' and (diff >= ' || min_diff || '))
order by diff desc
;
';

	--RAISE NOTICE '%', _complete_query;
	RETURN QUERY EXECUTE _complete_query;
END;
$BODY$
  LANGUAGE plpgsql STABLE
  COST 100
  ROWS 100000;

comment on function @extschema@.fn_add_aux_total_attr(integer[], double precision, boolean) is
'Function showing auxiliary total attribute additivity (aggregated class estimate should be equal to sum of sub-classes estimates). Hierarchy between aggregated classes and its sub-classes is defined in v_variable_hierarchy.
Function input argument is:
 * Array of attributes -- variables (FKEY to t_variable.id). All variables to be checked (aggregated class together with sub-classes) need to be passed.
 * Minimal amount of difference [%].
 * Whether include NULL difference -- indicating defined sub-classes missing in data.
Resulting table has following columns:
 * Auxiliary total estimation cell. FKEY to c_estimation_cell.id.
 * Auxiliary total attribute -- variable. FKEY to t_variable.id.
 * Aggregated class auxiliary total.
 * Sum of sub-classes auxiliary totals (belonging to aggregated class).
 * Attributes -- variables defined in hierarchy (v_variable_hierarchy). Array of FKEYs to t_variable.id.
 * Attributes -- variables found in data (t_result). Array of FKEYs to t_variable.id.
 * Relative difference between aggregated class auxiliary total and sum of sub-classes auxiliary totals.';

GRANT EXECUTE ON FUNCTION @extschema@.fn_add_aux_total_attr TO PUBLIC;

-- </function>

-- <function name="fn_add_aux_total_geo" schema="extschema" src="functions/extschema/additivity/fn_add_aux_total_geo.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_add_aux_total_geo(integer)

-- DROP FUNCTION @extschema@.fn_add_aux_total_geo(integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_add_aux_total_geo(
	IN estimation_cells integer[],
	IN min_diff double precision default 0.0, IN include_null_diff boolean default true
)
  RETURNS TABLE(
	variable		integer,
	estimation_cell		integer,
	aux_total		double precision,
	aux_total_sum		double precision,
	estimation_cells_def	integer[],
	estimation_cells_found	integer[],
	diff			double precision
) AS
$BODY$
DECLARE
	_complete_query text;
	_array_text text;
BEGIN
	--------------------------------QUERY--------------------------------
	_array_text := concat(quote_literal(estimation_cells::text), '::integer[]');
	--raise notice 'esimation cells: %',  _array_text;
	_complete_query := '
with w_auxtotal_cell_var as not materialized (
	select
		t_aux_total.estimation_cell,
		t_aux_total.aux_total,
		t_aux_total.variable
	from @extschema@.t_aux_total
	where t_aux_total.is_latest
	and t_aux_total.estimation_cell = ANY (' || _array_text || ')
)
, w_node_sum as (
	select
		estimation_cell,
		variable,
		sum(aux_total) as node_sum,
		hierarchy.node,
		hierarchy.edges as edges_def
	from w_auxtotal_cell_var
	inner join @extschema@.v_estimation_cell_hierarchy	as hierarchy on (hierarchy.node = w_auxtotal_cell_var.estimation_cell)
	group by estimation_cell, variable, node, edges
	order by estimation_cell, variable, node, edges
)
, w_edge_sum as (
	select
		w_node_sum.variable,
		w_node_sum.node,
		w_node_sum.node_sum,
		w_node_sum.edges_def,
		array_agg(w_auxtotal_cell_var.estimation_cell order by w_auxtotal_cell_var.estimation_cell) as edges_found,
		sum(w_auxtotal_cell_var.aux_total) as edges_sum
	from w_node_sum
	left join w_auxtotal_cell_var on (
		w_auxtotal_cell_var.variable = w_node_sum.variable
		and w_auxtotal_cell_var.estimation_cell = any(w_node_sum.edges_def))
	group by w_node_sum.variable, w_node_sum.node, w_node_sum.node_sum, w_node_sum.edges_def
)
, w_diff as (
	select
		variable,
		node		as estimation_cell,
		node_sum	as aux_total,
		edges_sum	as aux_total_sum,
		edges_def	as estimation_cells_def,
		edges_found	as estimation_cells_found,
		case
			when node_sum != 0.0 and edges_sum = 0.0 then 100.0
			when node_sum = 0.0 and edges_sum = 0.0 then 0
			else abs(1 - (node_sum / edges_sum )) * 100.0
		end as diff
	from w_edge_sum
)
select * from w_diff
where
	(' || include_null_diff || ' and ((diff >= ' || min_diff || ') or (diff is null)))
	or
	(not ' || include_null_diff || ' and (diff >= ' || min_diff || '))
order by diff desc
;
';

	--RAISE NOTICE '%', _complete_query;
	RETURN QUERY EXECUTE _complete_query;
END;
$BODY$
  LANGUAGE plpgsql STABLE
  COST 100
  ROWS 100000;

comment on function @extschema@.fn_add_aux_total_geo(integer[], double precision, boolean) is
'Function showing auxiliary total geographic additivity (aggregated class estimate should be equal to sum of sub-classes estimates). Hierarchy between aggregated classes and its sub-classes is defined in v_estimation_cell_hierarchy.
Function input argument is:
 * Aarray of esimation cells (FKEY to c_estimation_cell.id). All est. cells to be checked (aggregated class together with sub-classes) need to be passed.
 * Minimal amount of difference [%].
 * Whether include NULL difference -- indicating defined sub-classes missing in data.
Resulting table has following columns:
 * Auxiliary total attribute -- variable. FKEY to t_variable.id.
 * Auxiliary total estimation cell. FKEY to c_estimation_cell.id.
 * Aggregated class auxiliary total.
 * Sum of sub-classes auxiliary totals (belonging to aggregated class).
 * Estimation cells defined in hierarchy (v_estimation_cell_hierarchy). Array of FKEYs to c_estimation_cell.id.
 * Estimation cells found in data (t_result). Array of FKEYs to c_estimation_cell.id.
 * Relative difference between aggregated class auxiliary total and sum of sub-classes auxiliary totals.';

GRANT EXECUTE ON FUNCTION @extschema@.fn_add_aux_total_geo TO PUBLIC;

-- </function>

-- <function name="fn_add_plot_aux_attr" schema="extschema" src="functions/extschema/additivity/fn_add_plot_aux_attr.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_add_plot_aux_attr(integer)

-- DROP FUNCTION @extschema@.fn_add_plot_aux_attr(integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_add_plot_aux_attr(
	IN variables integer[],
	IN min_diff double precision default 0.0, IN include_null_diff boolean default true
)
  RETURNS TABLE(
	plot			integer,
	variable		integer,
	ldsity			double precision,
	ldsity_sum		double precision,
	variables_def		integer[],
	variables_found		integer[],
	diff			double precision
) AS
$BODY$
DECLARE
	_complete_query text;
	_array_text text;
BEGIN
	--------------------------------QUERY--------------------------------
	_array_text := concat(quote_literal(variables::text), '::integer[]');
	--raise notice 'variables: %',  _array_text;
	_complete_query := '
with w_plot_var as not materialized (
	select
		f_p_plot.gid as plot, t_available_datasets.variable, t_available_datasets.reference_year_set,
		coalesce(t_auxiliary_data.value, 0) as value
	from @extschema@.f_p_plot
	inner join @extschema@.t_cluster on (f_p_plot.cluster = t_cluster.id)
	inner join @extschema@.cm_cluster2panel_mapping on (t_cluster.id = cm_cluster2panel_mapping.cluster)
	inner join @extschema@.t_panel on (cm_cluster2panel_mapping.panel = t_panel.id)
	inner join @extschema@.t_available_datasets on (t_panel.id = t_available_datasets.panel)
	inner join @extschema@.t_variable on (t_available_datasets.variable = t_variable.id and t_variable.auxiliary_variable_category is not null)
	left join @extschema@.t_auxiliary_data on (
		f_p_plot.gid = t_auxiliary_data.plot
		and t_available_datasets.variable = t_auxiliary_data.variable
		and t_auxiliary_data.is_latest)
	WHERE t_available_datasets.variable = ANY (' || _array_text || ')

)
, w_node_sum as (
	select
		plot,

		w_plot_var.variable,
		value as node_sum,
		v_variable_hierarchy.node,
		v_variable_hierarchy.edges as edges_def
	from w_plot_var
	inner join @extschema@.v_variable_hierarchy on (v_variable_hierarchy.node = w_plot_var.variable)
)
, w_edge_sum as (
	select
		w_node_sum.plot,

		w_node_sum.node,
		w_node_sum.node_sum,
		w_node_sum.edges_def,
		array_agg(w_plot_var.variable order by w_plot_var.variable) as edges_found,
		sum(w_plot_var.value) as edges_sum
	from w_node_sum
	left join w_plot_var on (
		w_plot_var.plot = w_node_sum.plot

		and w_plot_var.variable = any(w_node_sum.edges_def))
	group by w_node_sum.plot,
		w_node_sum.node, w_node_sum.node_sum, w_node_sum.edges_def
)
, w_diff as (
	select
		plot,

		node		as variable,
		node_sum	as ldsity,
		edges_sum	as ldsity_sum,
		edges_def	as variables_def,
		edges_found	as variables_found,
		case
			when node_sum != 0.0 and edges_sum = 0.0 then 100.0
			when node_sum = 0.0 and edges_sum = 0.0 then 0
			else abs(1 - (node_sum / edges_sum )) * 100.0
		end as diff
	from w_edge_sum
)
select * from w_diff
where
	(' || include_null_diff || ' and ((diff >= ' || min_diff || ') or (diff is null)))
	or
	(not ' || include_null_diff || ' and (diff >= ' || min_diff || '))
order by diff desc
;
';

	--RAISE NOTICE '%', _complete_query;
	RETURN QUERY EXECUTE _complete_query;
END;
$BODY$
  LANGUAGE plpgsql STABLE
  COST 100
  ROWS 100000;

comment on function @extschema@.fn_add_plot_aux_attr(integer[], double precision, boolean) is
'Function showing plot level auxiliary local density attribute additivity (aggregated class estimate should be equal to sum of sub-classes estimates) of auxiliary local densities. Hierarchy between aggregated classes and its sub-classes is defined in v_variable_hierarchy.
Function input argument is:
 * Array of attributes -- variables (FKEY to t_variable.id). All variables to be checked (aggregated class together with sub-classes) need to be passed.
 * Minimal amount of difference [%].
 * Whether include NULL difference -- indicating defined sub-classes missing in data.
Resulting table has following columns:
 * Plot. FKEY to f_p_plot.gid.
 * Auxiliary total attribute -- variable. FKEY to t_variable.id.
 * Aggregated class local density.
 * Sum of sub-classes local densities (belonging to aggregated class).
 * Attributes -- variables defined in hierarchy (v_variable_hierarchy). Array of FKEYs to t_variable.id.
 * Attributes -- variables found in data (t_auxiliary_data). Array of FKEYs to t_variable.id.
 * Relative difference between aggregated class local densities and sum of sub-classes local densities.';

GRANT EXECUTE ON FUNCTION @extschema@.fn_add_plot_aux_attr TO PUBLIC;

-- </function>

-- <function name="fn_add_plot_target_attr" schema="extschema" src="functions/extschema/additivity/fn_add_plot_target_attr.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_add_plot_target_attr(integer)

-- DROP FUNCTION @extschema@.fn_add_plot_target_attr(integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_add_plot_target_attr(
	IN variables integer[],
	IN min_diff double precision default 0.0, IN include_null_diff boolean default true
)
  RETURNS TABLE(
	plot			integer,
	reference_year_set	integer,
	variable		integer,
	ldsity			double precision,
	ldsity_sum		double precision,
	variables_def		integer[],
	variables_found		integer[],
	diff			double precision
) AS
$BODY$
DECLARE
	_complete_query text;
	_array_text text;
BEGIN
	--------------------------------QUERY--------------------------------
	_array_text := concat(quote_literal(variables::text), '::integer[]');
	--raise notice 'variables: %',  _array_text;
	_complete_query := '
with w_plot_var as not materialized (
	select
		f_p_plot.gid as plot, t_available_datasets.variable, t_available_datasets.reference_year_set,
		coalesce(t_target_data.value, 0) as value
	from @extschema@.f_p_plot
	inner join @extschema@.t_cluster on (f_p_plot.cluster = t_cluster.id)
	inner join @extschema@.cm_cluster2panel_mapping on (t_cluster.id = cm_cluster2panel_mapping.cluster)
	inner join @extschema@.t_panel on (cm_cluster2panel_mapping.panel = t_panel.id)
	inner join @extschema@.t_available_datasets on (t_panel.id = t_available_datasets.panel)
	inner join @extschema@.t_variable on (t_available_datasets.variable = t_variable.id and t_variable.auxiliary_variable_category is null)
	left join @extschema@.t_target_data on (
		f_p_plot.gid = t_target_data.plot
		and t_available_datasets.variable = t_target_data.variable
		and t_available_datasets.reference_year_set = t_target_data.reference_year_set
		and t_target_data.is_latest)
	WHERE t_available_datasets.variable = ANY (' || _array_text || ')
)
, w_node_sum as (
	select
		plot,
		w_plot_var.reference_year_set,
		w_plot_var.variable,
		value as node_sum,
		v_variable_hierarchy.node,
		v_variable_hierarchy.edges as edges_def
	from w_plot_var
	inner join @extschema@.v_variable_hierarchy on (v_variable_hierarchy.node = w_plot_var.variable)
)
, w_edge_sum as (
	select
		w_node_sum.plot,
		w_node_sum.reference_year_set,
		w_node_sum.node,
		w_node_sum.node_sum,
		w_node_sum.edges_def,
		array_agg(w_plot_var.variable order by w_plot_var.variable) as edges_found,
		sum(w_plot_var.value) as edges_sum
	from w_node_sum
	left join w_plot_var on (
		w_plot_var.plot = w_node_sum.plot
		and w_plot_var.reference_year_set = w_node_sum.reference_year_set
		and w_plot_var.variable = any(w_node_sum.edges_def))
	group by w_node_sum.plot, w_node_sum.reference_year_set,
		w_node_sum.node, w_node_sum.node_sum, w_node_sum.edges_def
)
, w_diff as (
	select
		plot,
		reference_year_set,
		node		as variable,
		node_sum	as ldsity,
		edges_sum	as ldsity_sum,
		edges_def	as variables_def,
		edges_found	as variables_found,
		case
			when node_sum != 0.0 and edges_sum = 0.0 then 100.0
			when node_sum = 0.0 and edges_sum = 0.0 then 0
			else abs(1 - (node_sum / edges_sum )) * 100.0
		end as diff
	from w_edge_sum
)
select * from w_diff
where
	(' || include_null_diff || ' and ((diff >= ' || min_diff || ') or (diff is null)))
	or
	(not ' || include_null_diff || ' and (diff >= ' || min_diff || '))
order by diff desc
;
';

	--RAISE NOTICE '%', _complete_query;
	RETURN QUERY EXECUTE _complete_query;
END;
$BODY$
  LANGUAGE plpgsql STABLE
  COST 100
  ROWS 100000;

comment on function @extschema@.fn_add_plot_target_attr(integer[], double precision, boolean) is
'Function showing plot level target local density attribute additivity (aggregated class estimate should be equal to sum of sub-classes estimates) of target local densities. Hierarchy between aggregated classes and its sub-classes is defined in v_variable_hierarchy.
Function input argument is:
 * Array of attributes -- variables (FKEY to t_variable.id). All variables to be checked (aggregated class together with sub-classes) need to be passed.
 * Minimal amount of difference [%].
 * Whether include NULL difference -- indicating defined sub-classes missing in data.
Resulting table has following columns:
 * Plot. FKEY to f_p_plot.gid.
 * Reference year set. FKEY to t_reference_year_set.id.
 * Target total attribute -- variable. FKEY to t_variable.id.
 * Aggregated class local density.
 * Sum of sub-classes local densities (belonging to aggregated class).
 * Attributes -- variables defined in hierarchy (v_variable_hierarchy). Array of FKEYs to t_variable.id.
 * Attributes -- variables found in data (t_auxiliary_data). Array of FKEYs to t_variable.id.
 * Relative difference between aggregated class local densities and sum of sub-classes local densities.';

GRANT EXECUTE ON FUNCTION @extschema@.fn_add_plot_target_attr TO PUBLIC;

-- </function>

-- <function name="fn_add_res_ratio_attr" schema="extschema" src="functions/extschema/additivity/fn_add_res_ratio_attr.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_add_res_ratio_attr(integer)

-- DROP FUNCTION @extschema@.fn_add_res_ratio_attr(integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_add_res_ratio_attr(
	IN est_confs integer[],
	IN min_diff double precision default 0.0, IN include_null_diff boolean default true
)
  RETURNS TABLE(
	estimation_cell		integer,
	aux_conf		integer,
	force_synthetic		boolean,
	estimate_conf		integer,
	denominator		integer,
	variable		integer,
	point_est		double precision,
	point_est_sum		double precision,
	variables_def		integer[],
	variables_found		integer[],
	estimate_confs_found	integer[],
	diff			double precision
) AS
$BODY$
DECLARE
	_complete_query text;
	_array_text text;
BEGIN
	--------------------------------QUERY--------------------------------
	_array_text := concat(quote_literal(est_confs::text), '::integer[]');
	--raise notice 'est_confs: %',  _array_text;
	_complete_query := '
with w_res_cell_var as not materialized (
	select
		t_result.estimate_conf,
		t_result.point,
		t_total_estimate_conf.estimation_cell,
		t_total_estimate_conf.target_variable as t_variable__id,
		t_total_estimate_conf.estimate_date_begin, t_total_estimate_conf.estimate_date_end, 
		array_agg(t_panel2total_2ndph_estimate_conf.panel order by panel) as t_panel2total_2ndph_estimate_conf_panels,
		t_total_estimate_conf.aux_conf, force_synthetic, t_estimate_conf.denominator
	from @extschema@.t_result
	inner join @extschema@.t_estimate_conf ON t_estimate_conf.id = t_result.estimate_conf
	inner join @extschema@.t_total_estimate_conf ON t_total_estimate_conf.id = t_estimate_conf.total_estimate_conf
	inner join @extschema@.t_panel2total_2ndph_estimate_conf ON t_panel2total_2ndph_estimate_conf.total_estimate_conf = t_total_estimate_conf.id
	where (t_estimate_conf.estimate_type = 2)
	and (t_result.is_latest)
	and (t_result.estimate_conf = ANY (' || _array_text || '))
	group by 
		t_result.estimate_conf,
		t_result.point,
		t_total_estimate_conf.estimation_cell,
		t_total_estimate_conf.target_variable,
		t_total_estimate_conf.estimate_date_begin, t_total_estimate_conf.estimate_date_end,
		t_total_estimate_conf.aux_conf, force_synthetic, t_estimate_conf.denominator
)
, w_node_sum as (
	select
		estimation_cell,
		w_res_cell_var.t_variable__id, aux_conf, force_synthetic, denominator,
		estimate_date_begin, estimate_date_end, t_panel2total_2ndph_estimate_conf_panels,
		estimate_conf as node_estimate_confs,
		sum(point) as node_sum,
		hierarchy.node,
		hierarchy.edges as edges_def
	from w_res_cell_var
	inner join @extschema@.v_variable_hierarchy 	as hierarchy on (hierarchy.node = w_res_cell_var.t_variable__id)
	group by estimation_cell, w_res_cell_var.t_variable__id, aux_conf, force_synthetic, estimate_date_begin, estimate_date_end, t_panel2total_2ndph_estimate_conf_panels, estimate_conf, node, edges_def, denominator
	order by estimation_cell, w_res_cell_var.t_variable__id, aux_conf, force_synthetic, estimate_date_begin, estimate_date_end, t_panel2total_2ndph_estimate_conf_panels, estimate_conf, node, edges_def, denominator
)
, w_edge_sum as (
	select
		w_node_sum.estimation_cell,
		w_node_sum.aux_conf,
		w_node_sum.force_synthetic,
		w_node_sum.node_estimate_confs, w_node_sum.denominator,
		w_node_sum.estimate_date_begin, w_node_sum.estimate_date_end, w_node_sum.t_panel2total_2ndph_estimate_conf_panels,
		w_node_sum.node,
		w_node_sum.node_sum,
		w_node_sum.edges_def,
		array_agg(w_res_cell_var.t_variable__id order by w_res_cell_var.t_variable__id) as edges_found,
		array_agg(w_res_cell_var.estimate_conf order by w_res_cell_var.estimate_conf) as edges_estimate_confs,
		sum(w_res_cell_var.point) as edges_sum
	from w_node_sum
	left join w_res_cell_var on (
		w_res_cell_var.estimation_cell = w_node_sum.estimation_cell
		and w_res_cell_var.estimate_date_begin = w_node_sum.estimate_date_begin
		and w_res_cell_var.estimate_date_end = w_node_sum.estimate_date_end
		and w_res_cell_var.t_panel2total_2ndph_estimate_conf_panels = w_node_sum.t_panel2total_2ndph_estimate_conf_panels
		and case when
			w_res_cell_var.aux_conf is null and w_node_sum.aux_conf is null then true
			else w_res_cell_var.aux_conf = w_node_sum.aux_conf end
		and case when
			w_res_cell_var.force_synthetic is null and w_node_sum.force_synthetic is null then true
			else w_res_cell_var.force_synthetic = w_node_sum.force_synthetic end
		and w_res_cell_var.t_variable__id = any(w_node_sum.edges_def)) and w_res_cell_var.denominator = w_node_sum.denominator
	group by w_node_sum.estimation_cell, 
		w_node_sum.aux_conf, w_node_sum.force_synthetic, w_node_sum.node_estimate_confs, w_node_sum.denominator,
		w_node_sum.estimate_date_begin, w_node_sum.estimate_date_end, w_node_sum.t_panel2total_2ndph_estimate_conf_panels,
		w_node_sum.node, w_node_sum.node_sum, w_node_sum.edges_def
)
, w_diff as (
	select
		estimation_cell,
		aux_conf,
		force_synthetic,
		node_estimate_confs	as estimate_conf, denominator,
		node			as variable,
		node_sum		as point_est,
		edges_sum		as point_est_sum,
		edges_def		as variables_def,
		edges_found		as variables_found,
		edges_estimate_confs	as estimate_confs_found,
		case
			when node_sum != 0.0 and edges_sum = 0.0 then 100.0
			when node_sum = 0.0 and edges_sum = 0.0 then 0
			else abs(1 - (node_sum / edges_sum )) * 100.0
		end as diff
	from w_edge_sum
)
select * from w_diff
where
	(' || include_null_diff || ' and ((diff >= ' || min_diff || ') or (diff is null)))
	or
	(not ' || include_null_diff || ' and (diff >= ' || min_diff || '))
order by diff desc
;
';

	--RAISE NOTICE '%', _complete_query;
	RETURN QUERY EXECUTE _complete_query;
END;
$BODY$
  LANGUAGE plpgsql STABLE
  COST 100
  ROWS 100000;

comment on function @extschema@.fn_add_res_ratio_attr(integer[], double precision, boolean) is
'Function showing ratio estimates attribute additivity (aggregated class estimate should be equal to sum of sub-classes estimates). Hierarchy between aggregated classes and its sub-classes is defined in v_variable_hierarchy.
Function input argument is:
 * Array of attributes -- variables (FKEY to t_variable.id). All variables to be checked (aggregated class together with sub-classes) need to be passed.
 * Minimal amount of difference [%].
 * Whether include NULL difference -- indicating defined sub-classes missing in data.
Resulting table has following columns:
 * Estimate estimation cell. FKEY to c_estimation_cell.id.
 * Estimate auxiliary configuration. FKEY to t_aux_conf.id.
 * Parameter showing whether estimate is forced to be synthetic.
 * Estimate configuration id. FKEY to t_estimate_conf.id.
 * Estimate configuration id of denominator. FKEY to t_estimate_conf.id.
 * Estimate attribute -- variable. FKEY to t_variable.id.
 * Aggregated class point estimate.
 * Sum of sub-classes point estimates (belonging to aggregated class).
 * Attributes -- variables defined in hierarchy (v_variable_hierarchy). Array of FKEYs to t_variable.id.
 * Attributes -- variables found in data (t_result). Array of FKEYs to t_variable.id.
 * Estimate configurations found in data (t_result). Array of FKEYs to t_estimate_donf.id.
 * Relative difference between aggregated class estimate and sum of sub-classes estimates.';

GRANT EXECUTE ON FUNCTION @extschema@.fn_add_res_ratio_attr TO PUBLIC;

-- </function>

-- <function name="fn_add_res_total_attr" schema="extschema" src="functions/extschema/additivity/fn_add_res_total_attr.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_add_res_total_attr(integer)

-- DROP FUNCTION @extschema@.fn_add_res_total_attr(integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_add_res_total_attr(
	IN est_confs integer[],
	IN min_diff double precision default 0.0, IN include_null_diff boolean default true
)
  RETURNS TABLE(
	estimation_cell		integer,
	aux_conf		integer,
	force_synthetic		boolean,
	estimate_conf		integer,
	variable		integer,
	point_est		double precision,
	point_est_sum		double precision,
	variables_def		integer[],
	variables_found		integer[],
	estimate_confs_found	integer[],
	diff			double precision
) AS
$BODY$
DECLARE
	_complete_query text;
	_array_text text;
BEGIN
	--------------------------------QUERY--------------------------------
	_array_text := concat(quote_literal(est_confs::text), '::integer[]');
	--raise notice 'est_confs: %',  _array_text;
	_complete_query := '
with w_res_cell_var as not materialized (
	select
		t_result.estimate_conf,
		t_result.point,
		t_total_estimate_conf.estimation_cell,
		t_total_estimate_conf.target_variable as t_variable__id,
		t_total_estimate_conf.estimate_date_begin, t_total_estimate_conf.estimate_date_end, 
		array_agg(t_panel2total_2ndph_estimate_conf.panel order by panel) as t_panel2total_2ndph_estimate_conf_panels,
		t_total_estimate_conf.aux_conf, force_synthetic
	from @extschema@.t_result
	inner join @extschema@.t_estimate_conf ON t_estimate_conf.id = t_result.estimate_conf
	inner join @extschema@.t_total_estimate_conf ON t_total_estimate_conf.id = t_estimate_conf.total_estimate_conf
	inner join @extschema@.t_panel2total_2ndph_estimate_conf ON t_panel2total_2ndph_estimate_conf.total_estimate_conf = t_total_estimate_conf.id
	where (t_estimate_conf.estimate_type = 1)
	and (t_result.is_latest)
	and (t_result.estimate_conf = ANY (' || _array_text || '))
	group by 
		t_result.estimate_conf,
		t_result.point,
		t_total_estimate_conf.estimation_cell,
		t_total_estimate_conf.target_variable,
		t_total_estimate_conf.estimate_date_begin, t_total_estimate_conf.estimate_date_end, 
		t_total_estimate_conf.aux_conf, force_synthetic
)
, w_node_sum as (
	select
		estimation_cell,
		w_res_cell_var.t_variable__id, aux_conf, force_synthetic,
		estimate_date_begin, estimate_date_end, t_panel2total_2ndph_estimate_conf_panels,
		estimate_conf as node_estimate_confs,
		sum(point) as node_sum,
		hierarchy.node,
		hierarchy.edges as edges_def
	from w_res_cell_var
	inner join @extschema@.v_variable_hierarchy		as hierarchy on (hierarchy.node = w_res_cell_var.t_variable__id)
	group by estimation_cell, w_res_cell_var.t_variable__id, aux_conf, force_synthetic, estimate_date_begin, estimate_date_end, t_panel2total_2ndph_estimate_conf_panels, estimate_conf, node, edges_def
	order by estimation_cell, w_res_cell_var.t_variable__id, aux_conf, force_synthetic, estimate_date_begin, estimate_date_end, t_panel2total_2ndph_estimate_conf_panels, estimate_conf, node, edges_def
)
, w_edge_sum as (
	select
		w_node_sum.estimation_cell,
		w_node_sum.aux_conf,
		w_node_sum.force_synthetic,
		w_node_sum.node_estimate_confs,
		w_node_sum.estimate_date_begin, w_node_sum.estimate_date_end, w_node_sum.t_panel2total_2ndph_estimate_conf_panels,
		w_node_sum.node,
		w_node_sum.node_sum,
		w_node_sum.edges_def,
		array_agg(w_res_cell_var.t_variable__id order by w_res_cell_var.t_variable__id) as edges_found,
		array_agg(w_res_cell_var.estimate_conf order by w_res_cell_var.estimate_conf) as edges_estimate_confs,
		sum(w_res_cell_var.point) as edges_sum
	from w_node_sum
	left join w_res_cell_var on (
		w_res_cell_var.estimation_cell = w_node_sum.estimation_cell
		and w_res_cell_var.estimate_date_begin = w_node_sum.estimate_date_begin
		and w_res_cell_var.estimate_date_end = w_node_sum.estimate_date_end
		and w_res_cell_var.t_panel2total_2ndph_estimate_conf_panels = w_node_sum.t_panel2total_2ndph_estimate_conf_panels
		and case when
			w_res_cell_var.aux_conf is null and w_node_sum.aux_conf is null then true
			else w_res_cell_var.aux_conf = w_node_sum.aux_conf end
		and case when
			w_res_cell_var.force_synthetic is null and w_node_sum.force_synthetic is null then true
			else w_res_cell_var.force_synthetic = w_node_sum.force_synthetic end
		and w_res_cell_var.t_variable__id = any(w_node_sum.edges_def))
	group by w_node_sum.estimation_cell, 
		w_node_sum.aux_conf, w_node_sum.force_synthetic, w_node_sum.node_estimate_confs,
		w_node_sum.estimate_date_begin, w_node_sum.estimate_date_end, w_node_sum.t_panel2total_2ndph_estimate_conf_panels,
		w_node_sum.node, w_node_sum.node_sum, w_node_sum.edges_def
)
, w_diff as (
	select
		estimation_cell,
		aux_conf,
		force_synthetic,
		node_estimate_confs	as estimate_conf,
		node			as variable,
		node_sum		as point_est,
		edges_sum		as point_est_sum,
		edges_def		as variables_def,
		edges_found		as variables_found,
		edges_estimate_confs	as estimate_confs_found,
		case
			when node_sum != 0.0 and edges_sum = 0.0 then 100.0
			when node_sum = 0.0 and edges_sum = 0.0 then 0
			else abs(1 - (node_sum / edges_sum )) * 100.0
		end as diff
	from w_edge_sum
)
select * from w_diff
where
	(' || include_null_diff || ' and ((diff >= ' || min_diff || ') or (diff is null)))
	or
	(not ' || include_null_diff || ' and (diff >= ' || min_diff || '))
order by diff desc
;
';

	--RAISE NOTICE '%', _complete_query;
	RETURN QUERY EXECUTE _complete_query;
END;
$BODY$
  LANGUAGE plpgsql STABLE
  COST 100
  ROWS 100000;

comment on function @extschema@.fn_add_res_total_attr(integer[], double precision, boolean) is
	'Function showing total estimates attribute additivity (aggregated class estimate should be equal to sum of sub-classes estimates). Hierarchy between aggregated classes and its sub-classes is defined in v_variable_hierarchy.
Function input argument is:
 * Array of estimate configuration ids. FKEY to t_estimate_conf.id. All est. conf. ids to be checked (aggregated class together with sub-classes) need to be passed.
 * Minimal amount of difference [%].
 * Whether include NULL difference -- indicating defined sub-classes missing in data.
Resulting table has following columns:
 * Estimate estimation cell. FKEY to c_estimation_cell.id.
 * Estimate auxiliary configuration. FKEY to t_aux_conf.id.
 * Parameter showing whether estimate is forced to be synthetic.
 * Estimate configuration id. FKEY to t_estimate_conf.id.
 * Estimate attribute -- variable. FKEY to t_variable.id.
 * Aggregated class point estimate.
 * Sum of sub-classes point estimates (belonging to aggregated class).
 * Attributes -- variables defined in hierarchy (v_variable_hierarchy). Array of FKEYs to t_variable.id.
 * Attributes -- variables found in data (t_result). Array of FKEYs to t_variable.id.
 * Estimate configurations found in data (t_result). Array of FKEYs to t_estimate_donf.id.
 * Relative difference between aggregated class estimate and sum of sub-classes estimates.';

GRANT EXECUTE ON FUNCTION @extschema@.fn_add_res_total_attr TO PUBLIC;

-- </function>

-- <function name="fn_add_res_total_geo" schema="extschema" src="functions/extschema/additivity/fn_add_res_total_geo.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_add_res_total_geo(integer)

-- DROP FUNCTION @extschema@.fn_add_res_total_geo(integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_add_res_total_geo(
	IN est_confs integer[],
	IN min_diff double precision default 0.0, IN include_null_diff boolean default true
)
  RETURNS TABLE(
	variable		integer,
	aux_conf		integer,
	force_synthetic		boolean,
	estimate_conf		integer,
	estimation_cell		integer,
	point_est		double precision,
	point_est_sum		double precision,
	estimation_cells_def	integer[],
	estimation_cells_found	integer[],
	estimate_confs_found	integer[],
	diff			double precision
) AS
$BODY$
DECLARE
	_complete_query text;
	_array_text text;
BEGIN
	--------------------------------QUERY--------------------------------
	_array_text := concat(quote_literal(est_confs::text), '::integer[]');
	--raise notice 'est_confs: %',  _array_text;
	_complete_query := '
with w_res_cell_var as not materialized (
	select
		t_result.estimate_conf,
		t_result.point,
		t_total_estimate_conf.estimation_cell,
		t_total_estimate_conf.target_variable as t_variable__id,
		t_total_estimate_conf.estimate_date_begin, t_total_estimate_conf.estimate_date_end,
		json_agg(row_to_json((SELECT d FROM (SELECT est_conf_stratum.stratum, est_conf_stratum.panels) d))
			order by est_conf_stratum.stratum)::jsonb as strata_panels,
		t_total_estimate_conf.aux_conf, force_synthetic
	from @extschema@.t_result
	inner join @extschema@.t_estimate_conf ON t_estimate_conf.id = t_result.estimate_conf
	inner join @extschema@.t_total_estimate_conf ON t_total_estimate_conf.id = t_estimate_conf.total_estimate_conf
	inner join (
		select
			total_estimate_conf, stratum, array_agg(t_panel.id order by t_panel.id) as panels
		from @extschema@.t_panel2total_2ndph_estimate_conf
		inner join @extschema@.t_panel on (t_panel2total_2ndph_estimate_conf.panel = t_panel.id)
		group by total_estimate_conf, stratum
		) as est_conf_stratum
		ON est_conf_stratum.total_estimate_conf = t_total_estimate_conf.id
	where (t_estimate_conf.estimate_type = 1)
	and (t_result.is_latest)
	and (t_result.estimate_conf = ANY (' || _array_text || '))
	group by
		t_result.estimate_conf,
		t_result.point,
		t_total_estimate_conf.estimation_cell,
		t_total_estimate_conf.target_variable,
		t_total_estimate_conf.estimate_date_begin, t_total_estimate_conf.estimate_date_end,
		t_total_estimate_conf.aux_conf, force_synthetic
)
, w_node_sum as (
	select
		estimation_cell,
		w_res_cell_var.t_variable__id, aux_conf, force_synthetic,
		estimate_date_begin, estimate_date_end, strata_panels,
		estimate_conf as node_estimate_confs,
		sum(point) as node_sum,
		hierarchy.node,
		hierarchy.edges as edges_def
	from w_res_cell_var
	inner join @extschema@.v_estimation_cell_hierarchy	as hierarchy on (hierarchy.node = w_res_cell_var.estimation_cell)
	group by estimation_cell, w_res_cell_var.t_variable__id, aux_conf, force_synthetic, estimate_date_begin, estimate_date_end, strata_panels, estimate_conf, node, edges_def
	order by estimation_cell, w_res_cell_var.t_variable__id, aux_conf, force_synthetic, estimate_date_begin, estimate_date_end, strata_panels, estimate_conf, node, edges_def
)
, w_edge_sum as (
	select
		w_node_sum.t_variable__id as variable,
		w_node_sum.aux_conf,
		w_node_sum.force_synthetic,
		w_node_sum.node_estimate_confs,
		w_node_sum.estimate_date_begin, w_node_sum.estimate_date_end,
		w_node_sum.node,
		w_node_sum.node_sum,
		w_node_sum.edges_def,
		array_agg(w_res_cell_var.estimation_cell order by w_res_cell_var.estimation_cell) as edges_found,
		array_agg(w_res_cell_var.estimate_conf order by w_res_cell_var.estimate_conf) as edges_estimate_confs,
		sum(w_res_cell_var.point) as edges_sum
	from w_node_sum
	left join w_res_cell_var on (
		w_res_cell_var.t_variable__id = w_node_sum.t_variable__id
		and w_res_cell_var.estimate_date_begin = w_node_sum.estimate_date_begin
		and w_res_cell_var.estimate_date_end = w_node_sum.estimate_date_end
		and
		(
		SELECT array_to_json(array_agg(elem order by elem))::jsonb
		FROM   jsonb_array_elements(w_node_sum.strata_panels) elem
		WHERE  (elem->>''stratum'')::int in (select jsonb_path_query(w_res_cell_var.strata_panels, ''$.stratum'')::int)
		) = w_res_cell_var.strata_panels
		and case when
			w_res_cell_var.aux_conf is null and w_node_sum.aux_conf is null then true
			else w_res_cell_var.aux_conf = w_node_sum.aux_conf end
		and case when
			w_res_cell_var.force_synthetic is null and w_node_sum.force_synthetic is null then true
			else w_res_cell_var.force_synthetic = w_node_sum.force_synthetic end
		and w_res_cell_var.estimation_cell = any(w_node_sum.edges_def))
	group by w_node_sum.t_variable__id,
		w_node_sum.aux_conf, w_node_sum.force_synthetic, w_node_sum.node_estimate_confs,
		w_node_sum.estimate_date_begin, w_node_sum.estimate_date_end,
		w_node_sum.node, w_node_sum.node_sum, w_node_sum.edges_def
)
, w_diff as (
	select
		variable,
		aux_conf,
		force_synthetic,
		node_estimate_confs	as estimate_conf,
		node			as estimation_cell,
		node_sum		as point_est,
		edges_sum		as point_est_sum,
		edges_def		as estimation_cells_def,
		edges_found		as estimation_cells_found,
		edges_estimate_confs	as estimate_confs_found,
		case
			when node_sum != 0.0 and edges_sum = 0.0 then 100.0
			when node_sum = 0.0 and edges_sum = 0.0 then 0
			else abs(1 - (node_sum / edges_sum )) * 100.0
		end as diff
	from w_edge_sum
)
select * from w_diff
where
	(' || include_null_diff || ' and ((diff >= ' || min_diff || ') or (diff is null)))
	or
	(not ' || include_null_diff || ' and (diff >= ' || min_diff || '))
order by diff desc
;
';

	--RAISE NOTICE '%', _complete_query;
	RETURN QUERY EXECUTE _complete_query;
END;
$BODY$
  LANGUAGE plpgsql STABLE
  COST 100
  ROWS 100000;

comment on function @extschema@.fn_add_res_total_geo(integer[], double precision, boolean) is
	'Fnction showing total estimates geographic additivity (aggregated class estimate should be equal to sum of sub-classes estimates). Hierarchy between aggregated classes and its sub-classes is defined in v_estimation_cell_hierarchy.
Function input argument is:
 * Array of estimate configuration ids. FKEY to t_estimate_conf.id. All est. conf. ids to be checked (aggregated class together with sub-classes) need to be passed.
 * Minimal amount of difference [%].
 * Whether include NULL difference -- indicating defined sub-classes missing in data.
Resulting table has following columns:
 * Estimate attribute -- variable. FKEY to t_variable.id.
 * Estimate auxiliary configuration. FKEY to t_aux_conf.id.
 * Parameter showing whether estimate is forced to be synthetic.
 * Estimate configuration id. FKEY to t_estimate_conf.id.
 * Estimate estimation cell. FKEY to c_estimation_cell.id.
 * Aggregated class point estimate.
 * Sum of sub-classes point estimates (belonging to aggregated class).
 * Estimation cells defined in hierarchy (v_estimation_cell_hierarchy). Array of FKEYs to c_estimation_cell.id.
 * Estimation cells found in data (t_result). Array of FKEYs to c_estimation_cell.id.
 * Estimate configurations found in data (t_result). Array of FKEYs to t_estimate_donf.id.
 * Relative difference between aggregated class estimate and sum of sub-classes estimates.';

GRANT EXECUTE ON FUNCTION @extschema@.fn_add_res_total_geo TO PUBLIC;

-- </function>
