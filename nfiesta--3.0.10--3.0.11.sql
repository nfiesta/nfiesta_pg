--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- <function name="fn_add_plot_target_attr" schema="extschema" src="functions/extschema/additivity/fn_add_plot_target_attr.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_add_plot_target_attr(integer)

-- DROP FUNCTION @extschema@.fn_add_plot_target_attr(integer[], integer[], double precision, boolean);
CREATE OR REPLACE FUNCTION @extschema@.fn_add_plot_target_attr(
	IN variables integer[],
	IN plots integer[] default NULL,
	IN refyearsets integer[] default NULL,
	IN min_diff double precision default 0.0, IN include_null_diff boolean default true
)
  RETURNS TABLE(
	plot			integer,
	reference_year_set	integer,
	variable		integer,
	ldsity			double precision,
	ldsity_sum		double precision,
	variables_def		integer[],
	variables_found		integer[],
	diff			double precision
) AS
$BODY$
DECLARE
	_complete_query text;
	_array_text_v text;
	_array_text_p text;
	_array_text_r text;

BEGIN
	--------------------------------QUERY--------------------------------
	_array_text_v := concat(quote_literal(variables::text), '::integer[]');
	if plots is not null then
		_array_text_p := concat(' AND f_p_plot.gid = ANY (', quote_literal(plots::text), '::integer[])');
	else
		_array_text_p := '';
	end if;

	if refyearsets is not null then
		_array_text_r := concat(' AND t_available_datasets.reference_year_set = ANY (', quote_literal(refyearsets::text), '::integer[])');
	else
		_array_text_r := '';
	end if;

	--raise notice 'variables: %',  _array_text_v;
	--raise notice 'plots: %',  _array_text_p;
	_complete_query := '
with w_plot_var as materialized (
	select
		f_p_plot.gid as plot, t_available_datasets.variable, t_available_datasets.panel, t_available_datasets.reference_year_set,
		coalesce(t_target_data.value, 0) as value
	from sdesign.f_p_plot
	inner join sdesign.t_cluster on (f_p_plot.cluster = t_cluster.id)
	inner join sdesign.cm_cluster2panel_mapping on (t_cluster.id = cm_cluster2panel_mapping.cluster)
	inner join sdesign.t_panel on (cm_cluster2panel_mapping.panel = t_panel.id)
	inner join sdesign.t_cluster_configuration on (t_panel.cluster_configuration = t_cluster_configuration.id)
	inner join sdesign.cm_plot2cluster_config_mapping on (t_cluster_configuration.id = cm_plot2cluster_config_mapping.cluster_configuration and cm_plot2cluster_config_mapping.plot = f_p_plot.gid)
	inner join @extschema@.t_available_datasets on (t_panel.id = t_available_datasets.panel)
	inner join @extschema@.t_variable on (t_available_datasets.variable = t_variable.id and t_variable.auxiliary_variable_category is null)
	left join @extschema@.t_target_data on (
		f_p_plot.gid = t_target_data.plot
		and t_available_datasets.variable = t_target_data.variable
		and t_available_datasets.reference_year_set = t_target_data.reference_year_set
		and t_target_data.is_latest)
	WHERE 	t_available_datasets.variable = ANY (' || _array_text_v || ') 
			' || _array_text_p || '
			' || _array_text_r || '
)
, w_node_sum as (
	select
		plot,
		w_plot_var.panel,
		w_plot_var.reference_year_set,
		w_plot_var.variable,
		value as node_sum,
		v_variable_hierarchy_plot.node,
		v_variable_hierarchy_plot.edges as edges_def
	from w_plot_var
	inner join @extschema@.v_variable_hierarchy_plot on (v_variable_hierarchy_plot.node = w_plot_var.variable and
							v_variable_hierarchy_plot.panel = w_plot_var.panel and
							v_variable_hierarchy_plot.reference_year_set = w_plot_var.reference_year_set
	)
	where (v_variable_hierarchy_plot.edges <@ ' || _array_text_v || ')
)
, w_edge_sum as (
	select
		w_node_sum.plot,
		w_node_sum.reference_year_set,
		w_node_sum.node,
		w_node_sum.node_sum,
		w_node_sum.edges_def,
		array_agg(w_plot_var.variable order by w_plot_var.variable) as edges_found,
		sum(w_plot_var.value) as edges_sum
	from w_node_sum
	left join w_plot_var on (
		w_plot_var.plot = w_node_sum.plot
		and w_plot_var.panel = w_node_sum.panel
		and w_plot_var.reference_year_set = w_node_sum.reference_year_set
		and w_plot_var.variable = any(w_node_sum.edges_def))
	group by w_node_sum.plot, w_node_sum.reference_year_set,
		w_node_sum.node, w_node_sum.node_sum, w_node_sum.edges_def
)
, w_diff as (
	select
		plot,
		reference_year_set,
		node		as variable,
		node_sum	as ldsity,
		edges_sum	as ldsity_sum,
		edges_def	as variables_def,
		edges_found	as variables_found,
		case
			when edges_sum != 0.0 and node_sum = 0.0 then 1.0
			when node_sum = 0.0 and edges_sum = 0.0 then 0
			else abs( (node_sum - edges_sum) / node_sum )
		end as diff
	from w_edge_sum
)
select * from w_diff
where
	(' || include_null_diff || ' and ((diff >= ' || min_diff || ') or (diff is null)))
	or
	(not ' || include_null_diff || ' and (diff >= ' || min_diff || '))
order by diff desc
;
';

	--RAISE NOTICE '%', _complete_query;
	RETURN QUERY EXECUTE _complete_query;
END;
$BODY$
  LANGUAGE plpgsql STABLE
  COST 100
  ROWS 100000;

comment on function @extschema@.fn_add_plot_target_attr(integer[], integer[], integer[], double precision, boolean) is
'Function showing plot level target local density attribute additivity (aggregated class estimate should be equal to sum of sub-classes estimates) of target local densities. Hierarchy between aggregated classes and its sub-classes is defined in v_variable_hierarchy_plot.
Function input argument is:
 * Array of attributes -- variables (FKEY to t_variable.id). All variables to be checked (aggregated class together with sub-classes) need to be passed.
 * Array of plots -- plots (FKEY to f_p_plot.gid). If NULL all available plots are checked.
 * Array of reference year sets -- reference year sets (FKEY to t_reference_year_set.id). If NULL all available reference year sets are checked.
 * Minimal amount of difference [%].
 * Whether include NULL difference -- indicating defined sub-classes missing in data.
Resulting table has following columns:
 * Plot. FKEY to f_p_plot.gid.
 * Reference year set. FKEY to t_reference_year_set.id.
 * Target total attribute -- variable. FKEY to t_variable.id.
 * Aggregated class local density.
 * Sum of sub-classes local densities (belonging to aggregated class).
 * Attributes -- variables defined in hierarchy (v_variable_hierarchy_plot). Array of FKEYs to t_variable.id.
 * Attributes -- variables found in data (t_auxiliary_data). Array of FKEYs to t_variable.id.
 * Relative difference between aggregated class local densities and sum of sub-classes local densities.';

GRANT EXECUTE ON FUNCTION @extschema@.fn_add_plot_target_attr TO PUBLIC;

-- </function>

-- <function name="fn_add_plot_aux_attr" schema="extschema" src="functions/extschema/additivity/fn_add_plot_aux_attr.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_add_plot_aux_attr(integer)

-- DROP FUNCTION @extschema@.fn_add_plot_aux_attr(integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_add_plot_aux_attr(
	IN variables integer[],
	IN min_diff double precision default 0.0, IN include_null_diff boolean default true
)
  RETURNS TABLE(
	plot			integer,
	variable		integer,
	ldsity			double precision,
	ldsity_sum		double precision,
	variables_def		integer[],
	variables_found		integer[],
	diff			double precision
) AS
$BODY$
DECLARE
	_complete_query text;
	_array_text text;
BEGIN
	--------------------------------QUERY--------------------------------
	_array_text := concat(quote_literal(variables::text), '::integer[]');
	--raise notice 'variables: %',  _array_text;
	_complete_query := '
with w_plot_var as materialized (
	select
		f_p_plot.gid as plot, t_available_datasets.variable, t_available_datasets.panel,
		coalesce(t_auxiliary_data.value, 0) as value
	from sdesign.f_p_plot
	inner join sdesign.t_cluster on (f_p_plot.cluster = t_cluster.id)
	inner join sdesign.cm_cluster2panel_mapping on (t_cluster.id = cm_cluster2panel_mapping.cluster)
	inner join sdesign.t_panel on (cm_cluster2panel_mapping.panel = t_panel.id)
	inner join sdesign.t_cluster_configuration on (t_panel.cluster_configuration = t_cluster_configuration.id)
	inner join sdesign.cm_plot2cluster_config_mapping on (t_cluster_configuration.id = cm_plot2cluster_config_mapping.cluster_configuration and cm_plot2cluster_config_mapping.plot = f_p_plot.gid)
	inner join @extschema@.t_available_datasets on (t_panel.id = t_available_datasets.panel)
	inner join @extschema@.t_variable on (t_available_datasets.variable = t_variable.id and t_variable.auxiliary_variable_category is not null)
	left join @extschema@.t_auxiliary_data on (
		f_p_plot.gid = t_auxiliary_data.plot
		and t_available_datasets.variable = t_auxiliary_data.variable
		and t_auxiliary_data.is_latest)
	WHERE t_available_datasets.variable = ANY (' || _array_text || ')

)
, w_node_sum as (
	select
		plot,
		w_plot_var.panel,

		w_plot_var.variable,
		value as node_sum,
		v_variable_hierarchy_plot_aux.node,
		v_variable_hierarchy_plot_aux.edges as edges_def
	from w_plot_var
	inner join @extschema@.v_variable_hierarchy_plot_aux on (v_variable_hierarchy_plot_aux.node = w_plot_var.variable and
							v_variable_hierarchy_plot_aux.panel = w_plot_var.panel)
)
, w_edge_sum as (
	select
		w_node_sum.plot,

		w_node_sum.node,
		w_node_sum.node_sum,
		w_node_sum.edges_def,
		array_agg(w_plot_var.variable order by w_plot_var.variable) as edges_found,
		sum(w_plot_var.value) as edges_sum
	from w_node_sum
	left join w_plot_var on (
		w_plot_var.plot = w_node_sum.plot
		and w_plot_var.panel = w_node_sum.panel
		
		and w_plot_var.variable = any(w_node_sum.edges_def))
	group by w_node_sum.plot,
		w_node_sum.node, w_node_sum.node_sum, w_node_sum.edges_def
)
, w_diff as (
	select
		plot,

		node		as variable,
		node_sum	as ldsity,
		edges_sum	as ldsity_sum,
		edges_def	as variables_def,
		edges_found	as variables_found,
		case
			when edges_sum != 0.0 and node_sum = 0.0 then 1.0
			when node_sum = 0.0 and edges_sum = 0.0 then 0
			else abs( (node_sum - edges_sum) / node_sum )
		end as diff
	from w_edge_sum
)
select * from w_diff
where
	(' || include_null_diff || ' and ((diff >= ' || min_diff || ') or (diff is null)))
	or
	(not ' || include_null_diff || ' and (diff >= ' || min_diff || '))
order by diff desc
;
';

	--RAISE NOTICE '%', _complete_query;
	RETURN QUERY EXECUTE _complete_query;
END;
$BODY$
  LANGUAGE plpgsql STABLE
  COST 100
  ROWS 100000;

comment on function @extschema@.fn_add_plot_aux_attr(integer[], double precision, boolean) is
'Function showing plot level auxiliary local density attribute additivity (aggregated class estimate should be equal to sum of sub-classes estimates) of auxiliary local densities. Hierarchy between aggregated classes and its sub-classes is defined in v_variable_hierarchy_plot.
Function input argument is:
 * Array of attributes -- variables (FKEY to t_variable.id). All variables to be checked (aggregated class together with sub-classes) need to be passed.
 * Minimal amount of difference [%].
 * Whether include NULL difference -- indicating defined sub-classes missing in data.
Resulting table has following columns:
 * Plot. FKEY to f_p_plot.gid.
 * Auxiliary total attribute -- variable. FKEY to t_variable.id.
 * Aggregated class local density.
 * Sum of sub-classes local densities (belonging to aggregated class).
 * Attributes -- variables defined in hierarchy (v_variable_hierarchy_plot). Array of FKEYs to t_variable.id.
 * Attributes -- variables found in data (t_auxiliary_data). Array of FKEYs to t_variable.id.
 * Relative difference between aggregated class local densities and sum of sub-classes local densities.';

GRANT EXECUTE ON FUNCTION @extschema@.fn_add_plot_aux_attr TO PUBLIC;

-- </function>
