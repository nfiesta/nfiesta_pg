--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

alter table @extschema@.t_available_datasets add CONSTRAINT t_available_datasets__panel_reference_year_set_variable_unique UNIQUE (panel,reference_year_set,variable);

DROP TRIGGER trg__target_data__ins ON @extschema@.t_target_data;
DROP TRIGGER trg__target_data__upd ON @extschema@.t_target_data;
DROP TRIGGER trg__target_data__del ON @extschema@.t_target_data;

-- <function name="fn_check_t_target_data" schema="extschema" src="functions/extschema/fn_check_t_target_data.sql">
--
-- Copyright 2021, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

CREATE OR REPLACE FUNCTION @extschema@.fn_check_t_target_data() RETURNS TRIGGER AS $src$
    DECLARE
		_vars int[];
		_plots int[];
		_refyearsets int[];
		_errp json;
    BEGIN
		IF (TG_OP = 'DELETE') THEN
			with w_vars as (
					select array_prepend(node, edges) as vs
				from old_table
				inner join @extschema@.v_variable_hierarchy
					on (old_table.variable = v_variable_hierarchy.node
						or old_table.variable = any (v_variable_hierarchy.edges))
			)
			, w_vars_un as (
					select unnest(vs) as v from w_vars
			)
			select array_agg(distinct v) into _vars from w_vars_un;

			IF (TG_TABLE_NAME = 't_target_data') THEN
				select array_agg(distinct plot) into _plots from old_table;
			ELSIF (TG_TABLE_NAME = 't_available_datasets') THEN
				select array_agg(distinct f_p_plot.gid) into _plots
					from old_table
					inner join sdesign.t_panel on (old_table.panel = t_panel.id)
					inner join sdesign.cm_cluster2panel_mapping on (t_panel.id = cm_cluster2panel_mapping.panel)
					inner join sdesign.t_cluster on (cm_cluster2panel_mapping.cluster = t_cluster.id)
					inner join sdesign.f_p_plot on (t_cluster.id = f_p_plot.cluster);
			ELSE
				RAISE EXCEPTION 'fn_check_t_target_data -- delete -- table name not known: %', TG_TABLE_NAME;
			END IF;

			select array_agg(distinct reference_year_set) into _refyearsets from old_table;

			if _vars is not null then
				with w_err as (
					select @extschema@.fn_add_plot_target_attr(_vars, _plots, _refyearsets, 1e-6, true) as fn_err
				)
				, w_err_t as (
					select (fn_err).* from w_err
				)
				, w_err_json as (
					select 
						json_build_object(
							'plot'				, plot,
							'reference_year_set'		, reference_year_set,
							'variable'			, variable,
							'ldsity'			, ldsity,
							'ldsity_sum'			, ldsity_sum,
							'variables_def'			, variables_def,
							'variables_found'		, variables_found,
							'diff'				, diff
						) as errj
					from w_err_t
				)
				select json_agg(errj) into _errp from w_err_json;

				IF
					(json_array_length(_errp) > 0)
					THEN RAISE EXCEPTION 'fn_check_t_target_data -- delete -- plot level local densities are not additive:
						checked vars: %, checked plots: %,
						found err plots:
						%', _vars, _plots, jsonb_pretty(_errp::jsonb);
				END IF;
			else
				RAISE NOTICE 'fn_check_t_target_data -- delete -- additivity check was skiped:
						* 0 rows edited (next line displays NULL)
						* corresponding variable hierarchy not found (next line displays array)
						old_table variables: %', (select array_agg(variable) from old_table) as foo;
			end if;
------------------------------------------------
        ELSIF (TG_OP = 'UPDATE') THEN
			with w_vars as (
					select array_prepend(node, edges) as vs
				from new_table
				inner join @extschema@.v_variable_hierarchy
					on (new_table.variable = v_variable_hierarchy.node
						or new_table.variable = any (v_variable_hierarchy.edges))
			)
			, w_vars_un as (
					select unnest(vs) as v from w_vars
			)
			select array_agg(distinct v) into _vars from w_vars_un;

			IF (TG_TABLE_NAME = 't_target_data') THEN
				select array_agg(distinct plot) into _plots from new_table;
			ELSIF (TG_TABLE_NAME = 't_available_datasets') THEN
				select array_agg(distinct f_p_plot.gid) into _plots
					from new_table
					inner join sdesign.t_panel on (new_table.panel = t_panel.id)
					inner join sdesign.cm_cluster2panel_mapping on (t_panel.id = cm_cluster2panel_mapping.panel)
					inner join sdesign.t_cluster on (cm_cluster2panel_mapping.cluster = t_cluster.id)
					inner join sdesign.f_p_plot on (t_cluster.id = f_p_plot.cluster);
			ELSE
				RAISE EXCEPTION 'fn_check_t_target_data -- delete -- table name not known: %', TG_TABLE_NAME;
			END IF;

			select array_agg(distinct reference_year_set) into _refyearsets from new_table;

			if _vars is not null then
				with w_err as (
					select @extschema@.fn_add_plot_target_attr(_vars, _plots, _refyearsets, 1e-6, true) as fn_err
				)
				, w_err_t as (
					select (fn_err).* from w_err
				)
				, w_err_json as (
					select
						json_build_object(
							'plot'				, plot,
							'reference_year_set'		, reference_year_set,
							'variable'			, variable,
							'ldsity'			, ldsity,
							'ldsity_sum'			, ldsity_sum,
							'variables_def'			, variables_def,
							'variables_found'		, variables_found,
							'diff'				, diff
						) as errj
					from w_err_t
				)
				select json_agg(errj) into _errp from w_err_json;

				IF
					(json_array_length(_errp) > 0)
					THEN RAISE EXCEPTION 'fn_check_t_target_data -- update -- plot level local densities are not additive:
						checked vars: %, checked plots: %,
						found err plots:
						%', _vars, _plots, jsonb_pretty(_errp::jsonb);
				END IF;
			else
				RAISE NOTICE 'fn_check_t_target_data -- update -- additivity check was skiped:
						* 0 rows edited (next line displays NULL)
						* corresponding variable hierarchy not found (next line displays array)
						new_table variables: %', (select array_agg(variable) from new_table) as foo;
			end if;

------------------------------------------------
        ELSIF (TG_OP = 'INSERT') THEN
			with w_vars as (
					select array_prepend(node, edges) as vs
				from new_table
				inner join @extschema@.v_variable_hierarchy
					on (new_table.variable = v_variable_hierarchy.node
						or new_table.variable = any (v_variable_hierarchy.edges))
			)
			, w_vars_un as (
					select unnest(vs) as v from w_vars
			)
			select array_agg(distinct v) into _vars from w_vars_un;

			IF (TG_TABLE_NAME = 't_target_data') THEN
				select array_agg(distinct plot) into _plots from new_table;
			ELSIF (TG_TABLE_NAME = 't_available_datasets') THEN
				select array_agg(distinct f_p_plot.gid) into _plots
					from new_table
					inner join sdesign.t_panel on (new_table.panel = t_panel.id)
					inner join sdesign.cm_cluster2panel_mapping on (t_panel.id = cm_cluster2panel_mapping.panel)
					inner join sdesign.t_cluster on (cm_cluster2panel_mapping.cluster = t_cluster.id)
					inner join sdesign.f_p_plot on (t_cluster.id = f_p_plot.cluster);
			ELSE
				RAISE EXCEPTION 'fn_check_t_target_data -- delete -- table name not known: %', TG_TABLE_NAME;
			END IF;

			select array_agg(distinct reference_year_set) into _refyearsets from new_table;

			if _vars is not null then
				with w_err as (
					select @extschema@.fn_add_plot_target_attr(_vars, _plots, _refyearsets, 1e-6, true) as fn_err
				)
				, w_err_t as (
					select (fn_err).* from w_err
				)
				, w_err_json as (
					select
						json_build_object(
							'plot'				, plot,
							'reference_year_set'		, reference_year_set,
							'variable'			, variable,
							'ldsity'			, ldsity,
							'ldsity_sum'			, ldsity_sum,
							'variables_def'			, variables_def,
							'variables_found'		, variables_found,
							'diff'				, diff
						) as errj
					from w_err_t
				)
				select json_agg(errj) into _errp from w_err_json;

				IF
					(json_array_length(_errp) > 0)
					THEN RAISE EXCEPTION 'fn_check_t_target_data -- insert -- plot level local densities are not additive:
						checked vars: %, checked plots: %,
						found err plots:
						%', _vars, _plots, jsonb_pretty(_errp::jsonb);
				END IF;
			else
				RAISE NOTICE 'fn_check_t_target_data -- insert -- additivity check was skiped:
						* 0 rows edited (next line displays NULL)
						* corresponding variable hierarchy not found (next line displays array)
						new_table variables: %', (select array_agg(variable) from new_table) as foo;
			end if;
        END IF;
        RETURN NULL; -- result is ignored since this is an AFTER trigger
    END;
$src$ LANGUAGE plpgsql;

CREATE TRIGGER trg__target_data__ins
    AFTER INSERT ON @extschema@.t_target_data
    REFERENCING NEW TABLE AS new_table
    FOR EACH STATEMENT EXECUTE FUNCTION @extschema@.fn_check_t_target_data();

CREATE TRIGGER trg__target_data__upd
    AFTER UPDATE ON @extschema@.t_target_data
    REFERENCING NEW TABLE AS new_table
    FOR EACH STATEMENT EXECUTE FUNCTION @extschema@.fn_check_t_target_data();

CREATE TRIGGER trg__target_data__del
    AFTER DELETE ON @extschema@.t_target_data
    REFERENCING OLD TABLE AS old_table
    FOR EACH STATEMENT EXECUTE FUNCTION @extschema@.fn_check_t_target_data();

CREATE TRIGGER trg__available_datasets__ins
    AFTER INSERT ON @extschema@.t_available_datasets
    REFERENCING NEW TABLE AS new_table
    FOR EACH STATEMENT EXECUTE FUNCTION @extschema@.fn_check_t_target_data();

CREATE TRIGGER trg__available_datasets__upd
    AFTER UPDATE ON @extschema@.t_available_datasets
    REFERENCING NEW TABLE AS new_table
    FOR EACH STATEMENT EXECUTE FUNCTION @extschema@.fn_check_t_target_data();

CREATE TRIGGER trg__available_datasets__del
    AFTER DELETE ON @extschema@.t_available_datasets
    REFERENCING OLD TABLE AS old_table
    FOR EACH STATEMENT EXECUTE FUNCTION @extschema@.fn_check_t_target_data();

-- </function>
