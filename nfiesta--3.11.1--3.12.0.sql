-- Copyright 2017, 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.

-- <function name="nfiesta.fn_api_get_panel_refyearset_groups4stratum" schema="extschema" src="functions/extschema/configuration/fn_api_get_panel_refyearset_groups4stratum.sql">
-- Copyright 2017, 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
-- Function: nfiesta.fn_api_get_panel_refyearset_groups4stratum(integer)
--DROP FUNCTION nfiesta.fn_api_get_panel_refyearset_groups4stratum(integer);

CREATE OR REPLACE FUNCTION nfiesta.fn_api_get_panel_refyearset_groups4stratum(_stratum INT)
RETURNS TABLE (
_panel_refyearset_group_id INT,
_panel_refyearset_group_label VARCHAR(200),
_panel_refyearset_group_label_en VARCHAR(200),
_panel_refyearset_group_description TEXT,
_panel_refyearset_group_description_en TEXT)
AS
$function$
BEGIN

IF _stratum IS NULL THEN
	RAISE EXCEPTION 'fn_api_get_panel_refyearset_groups4stratum: Function argument _stratum INT must not be NULL!';
END IF;

RETURN QUERY EXECUTE '
SELECT DISTINCT 
	t1.id, 
	t1.label, 
	coalesce(t1.label_en, t1.label),
	t1.description, 
	coalesce(t1.description_en, t1.description)
FROM 
	nfiesta.c_panel_refyearset_group AS t1
INNER JOIN 
	nfiesta.t_panel_refyearset_group AS t2
ON t1.id = t2. panel_refyearset_group AND reference_year_set IS NOT NULL -- if reference year set is NULL the group is used for auxiliaries 
INNER JOIN
	sdesign.t_panel AS t3
ON t2.panel = t3.id AND t3.stratum = $1;' USING _stratum;
END;
$function$
LANGUAGE plpgsql
PARALLEL SAFE;

COMMENT ON FUNCTION nfiesta.fn_api_get_panel_refyearset_groups4stratum(INT) IS 
'The function returns list of groups of panel and reference yearset combinations '
'for the stratum identifier passed as input of the function.';

/*
-- testing false inputs

-- passing NULL for _stratums
SELECT * FROM nfiesta.fn_api_get_panel_refyearset_groups4stratum(NULL);

-- testing valid inputs

-- get groups for stratum 2 (id), three groups 14, 19, 21
SELECT * FROM nfiesta.fn_api_get_panel_refyearset_groups4stratum(2);

-- helping code
SELECT * FROM sdesign.t_stratum

SELECT 
	array_agg(DISTINCT t1.id ORDER BY t1.id) AS panel_refyearset_groups
FROM 
	nfiesta.c_panel_refyearset_group AS t1
INNER JOIN 
	nfiesta.t_panel_refyearset_group AS t2
ON t1.id = t2. panel_refyearset_group AND reference_year_set IS NOT NULL -- if reference year set is NULL the group is used for auxiliaries 
INNER JOIN
	sdesign.t_panel AS t3
ON t2.panel = t3.id AND t3.stratum = 2;

*/
-- </function>


