-- Copyright 2017, 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.

-- delete the function before its new version with renamed output parameters can be defined
DROP FUNCTION nfiesta.fn_api_do_labels_and_descriptions_of_panel_refyearset_group_exist(VARCHAR(200), VARCHAR(200), TEXT, TEXT);

-- <function name="fn_api_do_labels_and_descriptions_of_panel_refyearset_group_exist" schema="extschema" src="functions/extschema/configuration/fn_api_do_labels_and_descriptions_of_panel_refyearset_group_exist.sql">
-- Copyright 2017, 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: nfiesta.fn_api_do_labels_and_descriptions_of_panel_refyearset_group_exist(VARCHAR(200), VARCHAR(200), TEXT, TEXT)
-- DROP FUNCTION nfiesta.fn_api_do_labels_and_descriptions_of_panel_refyearset_group_exist(VARCHAR(200), VARCHAR(200), TEXT, TEXT);

CREATE OR REPLACE FUNCTION nfiesta.fn_api_do_labels_and_descriptions_of_panel_refyearset_group_exist(
	IN _label VARCHAR(200), IN _label_en VARCHAR(200), IN _description TEXT, IN _description_en TEXT,
	OUT _label_exists BOOLEAN, OUT _label_en_exists BOOLEAN, OUT _description_exists BOOLEAN, OUT _description_en_exists BOOLEAN)
AS
$function$
BEGIN

IF _label IS NULL THEN
	RAISE EXCEPTION 'fn_api_do_labels_and_descriptions_of_panel_refyearset_group_exist: Function argument _label VARCHAR(200) must not be NULL!';
END IF;

IF _label_en IS NULL THEN
	RAISE EXCEPTION 'fn_api_do_labels_and_descriptions_of_panel_refyearset_group_exist: Function argument _label_en VARCHAR(200) must not be NULL!';
END IF;

IF _description IS NULL THEN
	RAISE EXCEPTION 'fn_api_do_labels_and_descriptions_of_panel_refyearset_group_exist: Function argument _description TEXT must not be NULL!';
END IF;

IF _description_en IS NULL THEN
	RAISE EXCEPTION 'fn_api_do_labels_and_descriptions_of_panel_refyearset_group_exist: Function argument _description_en TEXT must not be NULL!';
END IF;

SELECT EXISTS(SELECT id FROM nfiesta.c_panel_refyearset_group WHERE label = _label) INTO _label_exists;
SELECT EXISTS(SELECT id FROM nfiesta.c_panel_refyearset_group WHERE label_en = _label_en) INTO _label_en_exists;
SELECT EXISTS(SELECT id FROM nfiesta.c_panel_refyearset_group WHERE description = _description) INTO _description_exists;
SELECT EXISTS(SELECT id FROM nfiesta.c_panel_refyearset_group WHERE description_en = _description_en) INTO _description_en_exists;

END;
$function$
LANGUAGE plpgsql
PARALLEL SAFE;

COMMENT ON FUNCTION nfiesta.fn_api_do_labels_and_descriptions_of_panel_refyearset_group_exist(VARCHAR(200), VARCHAR(200), TEXT, TEXT) IS 
'For each of the function arguments (label, label_en, description, description_en) the function checks '
'whether a record with identical value of the argument exists in the codelis of groups of panel and '
'reference-year combinations. The function returns four Booleans. The TRUE value indicates that a group '
'with the same value of the respective argument was found, otherwise FALSE is returned.';

/*
 
 SELECT * FROM nfiesta.c_panel_refyearset_group;
 
 UPDATE nfiesta.c_panel_refyearset_group 
 SET label_en = label;
 
  UPDATE nfiesta.c_panel_refyearset_group 
 SET description_en = description;
 
 
-- testing false inputs

-- passing NULL for _label
SELECT * FROM nfiesta.fn_api_do_labels_and_descriptions_of_panel_refyearset_group_exist(
	NULL, 'no_of_panels: 2;01-01-2011-12-31-2014', 'no_ofa_panels: 2;01-01-2011-12-31-2014', 'no_of_panels: 2;01-01-2011-12-31-2014');

-- passing NULL for _label_en
SELECT * FROM nfiesta.fn_api_do_labels_and_descriptions_of_panel_refyearset_group_exist(
	'no_of_panels: 2;01-01-2011-12-31-2014', NULL, 'no_of_panels: 2;01-01-2011-12-31-2014', 'no_of_panels: 2;01-01-2011-12-31-2014');

-- passing NULL for _description
SELECT * FROM nfiesta.fn_api_do_labels_and_descriptions_of_panel_refyearset_group_exist(
	'no_of_panels: 2;01-01-2011-12-31-2014', 'no_of_panels: 2;01-01-2011-12-31-2014', NULL, 'no_of_panels: 2;01-01-2011-12-31-2014');

-- passing NULL for _description
SELECT * FROM nfiesta.fn_api_do_labels_and_descriptions_of_panel_refyearset_group_exist(
	'no_of_panels: 2;01-01-2011-12-31-2014', 'no_of_panels: 2;01-01-2011-12-31-2014', 'no_of_panels: 2;01-01-2011-12-31-2014', NULL);

-- testing valid inputs

-- all found, all TRUE
SELECT * FROM nfiesta.fn_api_do_labels_and_descriptions_of_panel_refyearset_group_exist(
	'no_of_panels: 2;01-01-2011-12-31-2014', 'no_of_panels: 2;01-01-2011-12-31-2014', 'no_of_panels: 2;01-01-2011-12-31-2014', 'no_of_panels: 2;01-01-2011-12-31-2014');
	
-- first found (TRUE), the rest FALSE
SELECT * FROM nfiesta.fn_api_do_labels_and_descriptions_of_panel_refyearset_group_exist(
	'no_of_panels: 2;01-01-2011-12-31-2014', 'no_label_en', 'no_description', 'no_description_en');
	
*/
-- </function>


