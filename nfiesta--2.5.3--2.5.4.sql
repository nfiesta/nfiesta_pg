--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

drop view @extschema@.v_conf_overview;
-- <view name="v_conf_overview" schema="extschema" src="views/extschema/v_conf_overview.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- drop view if exists @extschema@.v_conf_overview;
create or replace view @extschema@.v_conf_overview as (
with w_confs AS MATERIALIZED (
	select
		t_estimate_conf.id as estimate_conf,
		t_total_estimate_conf.id as total_estimate_conf,
		t_aux_conf.id as aux_conf,
		t_total_estimate_conf_denom.id as total_estimate_conf__denom,
		t_aux_conf_denom.id as aux_conf__denom,
		case 	when 
				t_total_estimate_conf.id is not null and 
				t_aux_conf.id is null and 
				t_total_estimate_conf_denom.id is null and 
				t_aux_conf_denom.id is null 
			then '1p_total'
			when 
				t_total_estimate_conf.id is not null and 
				t_aux_conf.id is null and 
				t_total_estimate_conf_denom.id is not null and 
				t_aux_conf_denom.id is null 
			then '1p1p_ratio'
			when 
				t_total_estimate_conf.id is not null and 
				t_aux_conf.id is not null and 
				t_total_estimate_conf_denom.id is null and 
				t_aux_conf_denom.id is null 
			then '2p_total'
			when 
				t_total_estimate_conf.id is not null and 
				t_aux_conf.id is not null and 
				t_total_estimate_conf_denom.id is not null and 
				t_aux_conf_denom.id is null 
			then '2p1p_ratio'
			when 
				t_total_estimate_conf.id is not null and 
				t_aux_conf.id is null and 
				t_total_estimate_conf_denom.id is not null and 
				t_aux_conf_denom.id is not null 
			then '1p2p_ratio'
			when 
				t_total_estimate_conf.id is not null and 
				t_aux_conf.id is not null and 
				t_total_estimate_conf_denom.id is not null and 
				t_aux_conf_denom.id is not null 
			then '2p2p_ratio'
			else 'unknown'
		end as estimate_type_str,
		coalesce(t_aux_conf.sigma, t_aux_conf_denom.sigma) as sigma,
		coalesce(t_total_estimate_conf.force_synthetic, t_total_estimate_conf_denom.force_synthetic) as force_synthetic,
------------------------additional info begin-----------------------------------
		coalesce(t_aux_conf.param_area, t_aux_conf_denom.param_area) as param_area, 
		coalesce(f_a_param_area.param_area_code, f_a_param_area_denom.param_area_code) as param_area_code,
		coalesce(t_aux_conf.model, t_aux_conf_denom.model) as model, 
		coalesce(t_model.description, t_model_denom.description) as model_description,
		t_total_estimate_conf.estimation_cell, c_estimation_cell.label as estimation_cell_label, c_estimation_cell.estimation_cell_collection, --f_a_cell.geom,
		t_variable.id as variable, t_variable.target_variable, c_target_variable.label as target_variable_label, 
		t_variable.sub_population_category, c_sub_population_category.label as sub_population_category_label,
		t_variable.area_domain_category, c_area_domain_category.label as area_domain_category_label,
		t_total_estimate_conf.estimate_date_begin, t_total_estimate_conf.estimate_date_end
------------------------additional info end-------------------------------------
	from @extschema@.t_estimate_conf
        inner join @extschema@.t_total_estimate_conf ON t_total_estimate_conf.id = t_estimate_conf.total_estimate_conf
	left join @extschema@.t_aux_conf ON t_aux_conf.id = t_total_estimate_conf.aux_conf
        left join @extschema@.t_total_estimate_conf as t_total_estimate_conf_denom ON t_total_estimate_conf_denom.id = t_estimate_conf.denominator
	left join @extschema@.t_aux_conf as t_aux_conf_denom ON t_aux_conf_denom.id = t_total_estimate_conf_denom.aux_conf
------------------------additional info begin-----------------------------------
	left join @extschema@.f_a_param_area on (t_aux_conf.param_area = f_a_param_area.gid)
	left join @extschema@.t_model on (t_aux_conf.model = t_model.id)
	left join @extschema@.f_a_param_area as f_a_param_area_denom on (t_aux_conf_denom.param_area = f_a_param_area_denom.gid)
	left join @extschema@.t_model as t_model_denom on (t_aux_conf_denom.model = t_model_denom.id)
	inner join @extschema@.c_estimation_cell on (t_total_estimate_conf.estimation_cell = c_estimation_cell.id)
	inner join @extschema@.t_variable on (t_total_estimate_conf.target_variable = t_variable.id)
	inner join @extschema@.c_target_variable on (t_variable.target_variable = c_target_variable.id)
	left join @extschema@.c_sub_population_category on (t_variable.sub_population_category = c_sub_population_category.id)
	left join @extschema@.c_area_domain_category on (t_variable.area_domain_category = c_area_domain_category.id)
------------------------additional info end-------------------------------------
	order by t_estimate_conf.id
)
, w_confs_2nd_panels AS MATERIALIZED (
	select w_confs.*,
		array_agg(t_panel2total_2ndph_estimate_conf.panel order by t_panel2total_2ndph_estimate_conf.panel) as t_panel2total_2ndph_estimate_conf_panels,
		array_agg(t_panel2total_2ndph_estimate_conf.reference_year_set order by t_panel2total_2ndph_estimate_conf.panel) as est_data_2ndph_ref_year_sets
	from w_confs
	inner join @extschema@.t_panel2total_2ndph_estimate_conf on (t_panel2total_2ndph_estimate_conf.total_estimate_conf = w_confs.total_estimate_conf)
	group by w_confs.estimate_conf, w_confs.total_estimate_conf, w_confs.aux_conf, w_confs.total_estimate_conf__denom, w_confs.aux_conf__denom,
			w_confs.estimate_type_str, w_confs.sigma, w_confs.force_synthetic,
			w_confs.param_area, w_confs.param_area_code,
			w_confs.model, w_confs.model_description,
			w_confs.estimation_cell, w_confs.estimation_cell_label, w_confs.estimation_cell_collection,
			w_confs.variable, w_confs.target_variable, w_confs.target_variable_label,
			w_confs.sub_population_category, w_confs.sub_population_category_label, w_confs.area_domain_category, w_confs.area_domain_category_label,
			w_confs.estimate_date_begin, w_confs.estimate_date_end
)
	select w_confs_2nd_panels.*,
		array_agg(t_panel2aux_conf.panel order by t_panel2aux_conf.panel) as t_panel2aux_conf_panels
	from w_confs_2nd_panels
	left join @extschema@.t_panel2aux_conf on (t_panel2aux_conf.aux_conf = w_confs_2nd_panels.aux_conf)
	group by w_confs_2nd_panels.estimate_conf, w_confs_2nd_panels.total_estimate_conf, w_confs_2nd_panels.aux_conf, w_confs_2nd_panels.total_estimate_conf__denom, w_confs_2nd_panels.aux_conf__denom,
			w_confs_2nd_panels.estimate_type_str, w_confs_2nd_panels.sigma, w_confs_2nd_panels.force_synthetic,
			w_confs_2nd_panels.param_area, w_confs_2nd_panels.param_area_code,
			w_confs_2nd_panels.model, w_confs_2nd_panels.model_description,
			w_confs_2nd_panels.estimation_cell, w_confs_2nd_panels.estimation_cell_label, w_confs_2nd_panels.estimation_cell_collection, w_confs_2nd_panels.variable, w_confs_2nd_panels.target_variable, w_confs_2nd_panels.target_variable_label,
			w_confs_2nd_panels.sub_population_category, w_confs_2nd_panels.sub_population_category_label, w_confs_2nd_panels.area_domain_category, w_confs_2nd_panels.area_domain_category_label,
			w_confs_2nd_panels.estimate_date_begin, w_confs_2nd_panels.estimate_date_end,
			w_confs_2nd_panels.t_panel2total_2ndph_estimate_conf_panels, w_confs_2nd_panels.est_data_2ndph_ref_year_sets

);
--select * from @extschema@.v_conf_overview;

-- </view>

drop view @extschema@.v_add_plot_target_attr;
-- <view name="v_add_plot_target_attr" schema="extschema" src="views/extschema/v_add_plot_target_attr.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

create view @extschema@.v_add_plot_target_attr as
with w_plot_var as not materialized (
	select
		f_p_plot.gid as plot, t_available_datasets.variable, t_available_datasets.reference_year_set,
		coalesce(t_target_data.value, 0) as value
	from @extschema@.f_p_plot
	inner join @extschema@.t_cluster on (f_p_plot.cluster = t_cluster.id)
	inner join @extschema@.cm_cluster2panel_mapping on (t_cluster.id = cm_cluster2panel_mapping.cluster)
	inner join @extschema@.t_panel on (cm_cluster2panel_mapping.panel = t_panel.id)
	inner join @extschema@.t_available_datasets on (t_panel.id = t_available_datasets.panel)
	inner join @extschema@.t_variable on (t_available_datasets.variable = t_variable.id and t_variable.auxiliary_variable_category is null)
	left join @extschema@.t_target_data on (
		f_p_plot.gid = t_target_data.plot
		and t_available_datasets.variable = t_target_data.variable
		and t_available_datasets.reference_year_set = t_target_data.reference_year_set
		and t_target_data.is_latest)
)
, w_node_sum as (
	select
		plot,
		w_plot_var.reference_year_set,
		w_plot_var.variable,
		value as node_sum
	from w_plot_var
	inner join @extschema@.v_variable_hierarchy on (v_variable_hierarchy.node = w_plot_var.variable)
)
, w_edge_sum as (
	select
		w_node_sum.plot,
		w_node_sum.reference_year_set,
		v_variable_hierarchy.node,
		w_node_sum.node_sum,
		v_variable_hierarchy.edges as edges_def,
		array_agg(w_plot_var.variable order by w_plot_var.variable) as edges_found,
		sum(w_plot_var.value) as edges_sum
	from w_node_sum
	inner join @extschema@.v_variable_hierarchy on (v_variable_hierarchy.node = w_node_sum.variable)
	left join w_plot_var on (
		w_plot_var.plot = w_node_sum.plot
		and w_plot_var.reference_year_set = w_node_sum.reference_year_set
		and w_plot_var.variable = any(v_variable_hierarchy.edges))
	group by w_node_sum.plot, w_node_sum.reference_year_set, v_variable_hierarchy.node, w_node_sum.node_sum, v_variable_hierarchy.edges
)
, w_diff as (
	select
		plot,
		reference_year_set,
		node		as variable,
		node_sum	as ldsity,
		edges_sum	as ldsity_sum,
		edges_def	as variables_def,
		edges_found	as variables_found,
		case
			when node_sum != 0.0 and edges_sum = 0.0 then 100.0
			when node_sum = 0.0 and edges_sum = 0.0 then 0
			else abs(1 - (node_sum / edges_sum )) * 100.0
		end as diff
	from w_edge_sum
)
select * from w_diff order by diff desc
;

comment on view @extschema@.v_add_plot_target_attr is
	'View showing plot level target local density attribute additivity (aggregated class estimate should be equal to sum of sub-classes estimates) of target local densities. Hierarchy between aggregated classes and its sub-classes is defined in v_variable_hierarchy.';

comment on column @extschema@.v_add_plot_target_attr.plot		is 'Plot. FKEY to f_p_plot.gid.';
comment on column @extschema@.v_add_plot_target_attr.reference_year_set	is 'Reference year set. FKEY to t_reference_year_set.id.';
comment on column @extschema@.v_add_plot_target_attr.variable		is 'Auxiliary total attribute -- variable. FKEY to t_variable.id.';
comment on column @extschema@.v_add_plot_target_attr.ldsity		is 'Aggregated class local density.';
comment on column @extschema@.v_add_plot_target_attr.ldsity_sum		is 'Sum of sub-classes local densities (belonging to aggregated class).';
comment on column @extschema@.v_add_plot_target_attr.variables_def	is 'Attributes -- variables defined in hierarchy (v_variable_hierarchy). Array of FKEYs to t_variable.id.';
comment on column @extschema@.v_add_plot_target_attr.variables_found	is 'Attributes -- variables found in data (t_auxiliary_data). Array of FKEYs to t_variable.id.';
comment on column @extschema@.v_add_plot_target_attr.diff		is 'Relative difference between aggregated class local densities and sum of sub-classes local densities.';

-- </view>

drop view @extschema@.v_add_plot_aux_attr;
-- <view name="v_add_plot_aux_attr" schema="extschema" src="views/extschema/v_add_plot_aux_attr.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

create view @extschema@.v_add_plot_aux_attr as
with w_plot_var as not materialized (
	select
		f_p_plot.gid as plot, t_available_datasets.variable, t_available_datasets.reference_year_set,
		coalesce(t_auxiliary_data.value, 0) as value
	from @extschema@.f_p_plot
	inner join @extschema@.t_cluster on (f_p_plot.cluster = t_cluster.id)
	inner join @extschema@.cm_cluster2panel_mapping on (t_cluster.id = cm_cluster2panel_mapping.cluster)
	inner join @extschema@.t_panel on (cm_cluster2panel_mapping.panel = t_panel.id)
	inner join @extschema@.t_available_datasets on (t_panel.id = t_available_datasets.panel)
	inner join @extschema@.t_variable on (t_available_datasets.variable = t_variable.id and t_variable.auxiliary_variable_category is not null)
	left join @extschema@.t_auxiliary_data on (
		f_p_plot.gid = t_auxiliary_data.plot
		and t_available_datasets.variable = t_auxiliary_data.variable
		and t_auxiliary_data.is_latest)

)
, w_node_sum as (
	select
		plot,

		w_plot_var.variable,
		value as node_sum
	from w_plot_var
	inner join @extschema@.v_variable_hierarchy on (v_variable_hierarchy.node = w_plot_var.variable)
)
, w_edge_sum as (
	select
		w_node_sum.plot,

		v_variable_hierarchy.node,
		w_node_sum.node_sum,
		v_variable_hierarchy.edges as edges_def,
		array_agg(w_plot_var.variable order by w_plot_var.variable) as edges_found,
		sum(w_plot_var.value) as edges_sum
	from w_node_sum
	inner join @extschema@.v_variable_hierarchy on (v_variable_hierarchy.node = w_node_sum.variable)
	left join w_plot_var on (
		w_plot_var.plot = w_node_sum.plot
		and w_plot_var.variable = any(v_variable_hierarchy.edges))

	group by w_node_sum.plot, v_variable_hierarchy.node, w_node_sum.node_sum, v_variable_hierarchy.edges
)
, w_diff as (
	select
		plot,

		node		as variable,
		node_sum	as ldsity,
		edges_sum	as ldsity_sum,
		edges_def	as variables_def,
		edges_found	as variables_found,
		case
			when node_sum != 0.0 and edges_sum = 0.0 then 100.0
			when node_sum = 0.0 and edges_sum = 0.0 then 0
			else abs(1 - (node_sum / edges_sum )) * 100.0
		end as diff
	from w_edge_sum
)
select * from w_diff order by diff desc
;

comment on view @extschema@.v_add_plot_aux_attr is
	'View showing plot level auxiliary local density attribute additivity (aggregated class estimate should be equal to sum of sub-classes estimates) of auxiliary local densities. Hierarchy between aggregated classes and its sub-classes is defined in v_variable_hierarchy.';

comment on column @extschema@.v_add_plot_aux_attr.plot			is 'Plot. FKEY to f_p_plot.gid.';

comment on column @extschema@.v_add_plot_aux_attr.variable		is 'Auxiliary total attribute -- variable. FKEY to t_variable.id.';
comment on column @extschema@.v_add_plot_aux_attr.ldsity		is 'Aggregated class local density.';
comment on column @extschema@.v_add_plot_aux_attr.ldsity_sum		is 'Sum of sub-classes local densities (belonging to aggregated class).';
comment on column @extschema@.v_add_plot_aux_attr.variables_def	is 'Attributes -- variables defined in hierarchy (v_variable_hierarchy). Array of FKEYs to t_variable.id.';
comment on column @extschema@.v_add_plot_aux_attr.variables_found	is 'Attributes -- variables found in data (t_auxiliary_data). Array of FKEYs to t_variable.id.';
comment on column @extschema@.v_add_plot_aux_attr.diff			is 'Relative difference between aggregated class local densities and sum of sub-classes local densities.';

-- </view>

drop view @extschema@.v_add_aux_total_attr;
-- <view name="v_add_aux_total_attr" schema="extschema" src="views/extschema/v_add_aux_total_attr.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

create view @extschema@.v_add_aux_total_attr as
with w_auxtotal_cell_var as not materialized (
	select
		t_aux_total.estimation_cell,
		t_aux_total.aux_total,
		t_aux_total.variable
	from @extschema@.t_aux_total
	where t_aux_total.is_latest
)
, w_node_sum as (
	select
		estimation_cell,
		variable,
		sum(aux_total) as node_sum
	from w_auxtotal_cell_var
	inner join @extschema@.v_variable_hierarchy		as hierarchy on (hierarchy.node = w_auxtotal_cell_var.variable)
	group by estimation_cell, variable
	order by estimation_cell, variable
)
, w_edge_sum as (
	select
		w_node_sum.estimation_cell,
		hierarchy.node,
		w_node_sum.node_sum,
		hierarchy.edges as edges_def,
		array_agg(w_auxtotal_cell_var.variable order by w_auxtotal_cell_var.variable) as edges_found,
		sum(w_auxtotal_cell_var.aux_total) as edges_sum
	from w_node_sum
	inner join @extschema@.v_variable_hierarchy		as hierarchy on (hierarchy.node = w_node_sum.variable)
	left join w_auxtotal_cell_var on (
		w_auxtotal_cell_var.estimation_cell = w_node_sum.estimation_cell
		and w_auxtotal_cell_var.variable = any(hierarchy.edges))
	group by w_node_sum.estimation_cell, hierarchy.node, w_node_sum.node_sum, hierarchy.edges
)
, w_diff as (
	select
		estimation_cell,
		node			as variable,
		node_sum		as aux_total,
		edges_sum		as aux_total_sum,
		edges_def		as variables_def,
		edges_found		as variables_found,
		case
			when node_sum != 0.0 and edges_sum = 0.0 then 100.0
			when node_sum = 0.0 and edges_sum = 0.0 then 0
			else abs(1 - (node_sum / edges_sum )) * 100.0
		end as diff
	from w_edge_sum
)
select * from w_diff order by diff desc
;

comment on view @extschema@.v_add_aux_total_attr is
	'View showing auxiliary total attribute additivity (aggregated class estimate should be equal to sum of sub-classes estimates). Hierarchy between aggregated classes and its sub-classes is defined in v_variable_hierarchy.';

comment on column @extschema@.v_add_aux_total_attr.estimation_cell 	is 'Auxiliary total estimation cell. FKEY to c_estimation_cell.id.';
comment on column @extschema@.v_add_aux_total_attr.variable		is 'Auxiliary total attribute -- variable. FKEY to t_variable.id.';
comment on column @extschema@.v_add_aux_total_attr.aux_total		is 'Aggregated class auxiliary total.';
comment on column @extschema@.v_add_aux_total_attr.aux_total_sum	is 'Sum of sub-classes auxiliary totals (belonging to aggregated class).';
comment on column @extschema@.v_add_aux_total_attr.variables_def	is 'Attributes -- variables defined in hierarchy (v_variable_hierarchy). Array of FKEYs to t_variable.id.';
comment on column @extschema@.v_add_aux_total_attr.variables_found	is 'Attributes -- variables found in data (t_result). Array of FKEYs to t_variable.id.';
comment on column @extschema@.v_add_aux_total_attr.diff			is 'Relative difference between aggregated class auxiliary total and sum of sub-classes auxiliary totals.';

-- </view>

drop view @extschema@.v_add_aux_total_geo;
-- <view name="v_add_aux_total_geo" schema="extschema" src="views/extschema/v_add_aux_total_geo.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

create view @extschema@.v_add_aux_total_geo as
with w_auxtotal_cell_var as not materialized (
	select
		t_aux_total.estimation_cell,
		t_aux_total.aux_total,
		t_aux_total.variable
	from @extschema@.t_aux_total
	where t_aux_total.is_latest
)
, w_node_sum as (
	select
		estimation_cell,
		variable,
		sum(aux_total) as node_sum
	from w_auxtotal_cell_var
	inner join @extschema@.v_estimation_cell_hierarchy	as hierarchy on (hierarchy.node = w_auxtotal_cell_var.estimation_cell)
	group by estimation_cell, variable
	order by estimation_cell, variable
)
, w_edge_sum as (
	select
		w_node_sum.variable,
		hierarchy.node,
		w_node_sum.node_sum,
		hierarchy.edges as edges_def,
		array_agg(w_auxtotal_cell_var.estimation_cell order by w_auxtotal_cell_var.estimation_cell) as edges_found,
		sum(w_auxtotal_cell_var.aux_total) as edges_sum
	from w_node_sum
	inner join @extschema@.v_estimation_cell_hierarchy	as hierarchy on (hierarchy.node = w_node_sum.estimation_cell)
	left join w_auxtotal_cell_var on (
		w_auxtotal_cell_var.variable = w_node_sum.variable
		and w_auxtotal_cell_var.estimation_cell = any(hierarchy.edges))
	group by w_node_sum.variable, hierarchy.node, w_node_sum.node_sum, hierarchy.edges
)
, w_diff as (
	select
		variable,
		node		as estimation_cell,
		node_sum	as aux_total,
		edges_sum	as aux_total_sum,
		edges_def	as estimation_cells_def,
		edges_found	as estimation_cells_found,
		case
			when node_sum != 0.0 and edges_sum = 0.0 then 100.0
			when node_sum = 0.0 and edges_sum = 0.0 then 0
			else abs(1 - (node_sum / edges_sum )) * 100.0
		end as diff
	from w_edge_sum
)
select * from w_diff order by diff desc
;

comment on view @extschema@.v_add_aux_total_geo is
	'View showing auxiliary total geographic additivity (aggregated class estimate should be equal to sum of sub-classes estimates). Hierarchy between aggregated classes and its sub-classes is defined in v_estimation_cell_hierarchy.';

comment on column @extschema@.v_add_aux_total_geo.variable			is 'Auxiliary total attribute -- variable. FKEY to t_variable.id.';
comment on column @extschema@.v_add_aux_total_geo.estimation_cell 		is 'Auxiliary total estimation cell. FKEY to c_estimation_cell.id.';
comment on column @extschema@.v_add_aux_total_geo.aux_total			is 'Aggregated class auxiliary total.';
comment on column @extschema@.v_add_aux_total_geo.aux_total_sum			is 'Sum of sub-classes auxiliary total. (belonging to aggregated class).';
comment on column @extschema@.v_add_aux_total_geo.estimation_cells_def		is 'Estimation cells defined in hierarchy (v_estimation_cell_hierarchy). Array of FKEYs to c_estimation_cell.id.';
comment on column @extschema@.v_add_aux_total_geo.estimation_cells_found	is 'Estimation cells found in data (t_result). Array of FKEYs to c_estimation_cell.id.';
comment on column @extschema@.v_add_aux_total_geo.diff				is 'Relative difference between aggregated class auxiliary total and sum of sub-classes auxiliary totals.';

-- </view>

drop view @extschema@.v_add_res_total_attr;
-- <view name="v_add_res_total_attr" schema="extschema" src="views/extschema/v_add_res_total_attr.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

create view @extschema@.v_add_res_total_attr as
with w_res_cell_var as not materialized (
	select
		t_result.estimate_conf,
		t_result.point,
		t_total_estimate_conf.estimation_cell,
		t_total_estimate_conf.target_variable as t_variable__id,
		t_total_estimate_conf.aux_conf, force_synthetic
	from @extschema@.t_result
	inner join @extschema@.t_estimate_conf ON t_estimate_conf.id = t_result.estimate_conf
	inner join @extschema@.t_total_estimate_conf ON t_total_estimate_conf.id = t_estimate_conf.total_estimate_conf
	where t_estimate_conf.estimate_type = 1
	and t_result.is_latest
)
, w_node_sum as (
	select
		estimation_cell,
		w_res_cell_var.t_variable__id, aux_conf, force_synthetic,
		estimate_conf as node_estimate_confs,
		sum(point) as node_sum
	from w_res_cell_var
	inner join @extschema@.v_variable_hierarchy as hierarchy on (hierarchy.node = w_res_cell_var.t_variable__id)
	group by estimation_cell, w_res_cell_var.t_variable__id, aux_conf, force_synthetic, estimate_conf
	order by estimation_cell, w_res_cell_var.t_variable__id, aux_conf, force_synthetic, estimate_conf
)
, w_edge_sum as (
	select
		w_node_sum.estimation_cell,
		w_node_sum.aux_conf,
		w_node_sum.force_synthetic,
		w_node_sum.node_estimate_confs,
		hierarchy.node,
		w_node_sum.node_sum,
		hierarchy.edges as edges_def,
		array_agg(w_res_cell_var.t_variable__id order by w_res_cell_var.t_variable__id) as edges_found,
		array_agg(w_res_cell_var.estimate_conf order by w_res_cell_var.estimate_conf) as edges_estimate_confs,
		sum(w_res_cell_var.point) as edges_sum
	from w_node_sum
	inner join @extschema@.v_variable_hierarchy as hierarchy on (hierarchy.node = w_node_sum.t_variable__id)
	left join w_res_cell_var on (
		w_res_cell_var.estimation_cell = w_node_sum.estimation_cell
		and case when
			w_res_cell_var.aux_conf is null and w_node_sum.aux_conf is null then true
			else w_res_cell_var.aux_conf = w_node_sum.aux_conf end
		and case when
			w_res_cell_var.force_synthetic is null and w_node_sum.force_synthetic is null then true
			else w_res_cell_var.force_synthetic = w_node_sum.force_synthetic end
		and w_res_cell_var.t_variable__id = any(hierarchy.edges))
	group by w_node_sum.estimation_cell, w_node_sum.aux_conf, w_node_sum.force_synthetic, w_node_sum.node_estimate_confs,
		hierarchy.node, w_node_sum.node_sum, hierarchy.edges
)
, w_diff as (
	select
		estimation_cell,
		aux_conf,
		force_synthetic,
		node_estimate_confs	as estimate_conf,
		node			as variable,
		node_sum		as point_est,
		edges_sum		as point_est_sum,
		edges_def		as variables_def,
		edges_found		as variables_found,
		edges_estimate_confs	as estimate_confs_found,
		case
			when node_sum != 0.0 and edges_sum = 0.0 then 100.0
			when node_sum = 0.0 and edges_sum = 0.0 then 0
			else abs(1 - (node_sum / edges_sum )) * 100.0
		end as diff
	from w_edge_sum
)
select * from w_diff order by diff desc
;

comment on view @extschema@.v_add_res_total_attr is
	'View showing total estimates attribute additivity (aggregated class estimate should be equal to sum of sub-classes estimates). Hierarchy between aggregated classes and its sub-classes is defined in v_variable_hierarchy.';

comment on column @extschema@.v_add_res_total_attr.estimation_cell 	is 'Estimate estimation cell. FKEY to c_estimation_cell.id.';
comment on column @extschema@.v_add_res_total_attr.aux_conf 		is 'Estimate auxiliary configuration. FKEY to t_aux_conf.id.';
comment on column @extschema@.v_add_res_total_attr.force_synthetic 	is 'Parameter showing whether estimate is forced to be synthetic.';
comment on column @extschema@.v_add_res_total_attr.estimate_conf	is 'Estimate configuration id. FKEY to t_estimate_conf.id.';
comment on column @extschema@.v_add_res_total_attr.variable		is 'Estimate attribute -- variable. FKEY to t_variable.id.';
comment on column @extschema@.v_add_res_total_attr.point_est		is 'Aggregated class point estimate.';
comment on column @extschema@.v_add_res_total_attr.point_est_sum	is 'Sum of sub-classes point estimates (belonging to aggregated class).';
comment on column @extschema@.v_add_res_total_attr.variables_def	is 'Attributes -- variables defined in hierarchy (v_variable_hierarchy). Array of FKEYs to t_variable.id.';
comment on column @extschema@.v_add_res_total_attr.variables_found	is 'Attributes -- variables found in data (t_result). Array of FKEYs to t_variable.id.';
comment on column @extschema@.v_add_res_total_attr.estimate_confs_found	is 'Estimate configurations found in data (t_result). Array of FKEYs to t_estimate_donf.id.';
comment on column @extschema@.v_add_res_total_attr.diff			is 'Relative difference between aggregated class estimate and sum of sub-classes estimates.';

-- </view>

drop view @extschema@.v_add_res_total_geo;
-- <view name="v_add_res_total_geo" schema="extschema" src="views/extschema/v_add_res_total_geo.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

create view @extschema@.v_add_res_total_geo as
with w_res_cell_var as not materialized (
	select
		t_result.estimate_conf,
		t_result.point,
		t_total_estimate_conf.estimation_cell,
		t_total_estimate_conf.target_variable as t_variable__id,
		t_total_estimate_conf.aux_conf, force_synthetic
	from @extschema@.t_result
	inner join @extschema@.t_estimate_conf ON t_estimate_conf.id = t_result.estimate_conf
	inner join @extschema@.t_total_estimate_conf ON t_total_estimate_conf.id = t_estimate_conf.total_estimate_conf
	where t_estimate_conf.estimate_type = 1
	and t_result.is_latest
)
, w_node_sum as (
	select
		estimation_cell,
		w_res_cell_var.t_variable__id, aux_conf, force_synthetic,
		estimate_conf as node_estimate_confs,
		sum(point) as node_sum
	from w_res_cell_var
	inner join @extschema@.v_estimation_cell_hierarchy as hierarchy on (hierarchy.node = w_res_cell_var.estimation_cell)
	group by estimation_cell, w_res_cell_var.t_variable__id, aux_conf, force_synthetic, estimate_conf
	order by estimation_cell, w_res_cell_var.t_variable__id, aux_conf, force_synthetic, estimate_conf
)
, w_edge_sum as (
	select
		w_node_sum.t_variable__id as variable,
		w_node_sum.aux_conf,
		w_node_sum.force_synthetic,
		w_node_sum.node_estimate_confs,
		hierarchy.node,
		w_node_sum.node_sum,
		hierarchy.edges as edges_def,
		array_agg(w_res_cell_var.estimation_cell order by w_res_cell_var.estimation_cell) as edges_found,
		array_agg(w_res_cell_var.estimate_conf order by w_res_cell_var.estimate_conf) as edges_estimate_confs,
		sum(w_res_cell_var.point) as edges_sum
	from w_node_sum
	inner join @extschema@.v_estimation_cell_hierarchy as hierarchy on (hierarchy.node = w_node_sum.estimation_cell)
	left join w_res_cell_var on (
		w_res_cell_var.t_variable__id = w_node_sum.t_variable__id
		and case when
			w_res_cell_var.aux_conf is null and w_node_sum.aux_conf is null then true
			else w_res_cell_var.aux_conf = w_node_sum.aux_conf end
		and case when
			w_res_cell_var.force_synthetic is null and w_node_sum.force_synthetic is null then true
			else w_res_cell_var.force_synthetic = w_node_sum.force_synthetic end
		and w_res_cell_var.estimation_cell = any(hierarchy.edges))
	group by w_node_sum.t_variable__id, w_node_sum.aux_conf, w_node_sum.force_synthetic, w_node_sum.node_estimate_confs,
		hierarchy.node, w_node_sum.node_sum, hierarchy.edges
)
, w_diff as (
	select
		variable,
		aux_conf,
		force_synthetic,
		node_estimate_confs	as estimate_conf,
		node			as estimation_cell,
		node_sum		as point_est,
		edges_sum		as point_est_sum,
		edges_def		as estimation_cells_def,
		edges_found		as estimation_cells_found,
		edges_estimate_confs	as estimate_confs_found,
		case
			when node_sum != 0.0 and edges_sum = 0.0 then 100.0
			when node_sum = 0.0 and edges_sum = 0.0 then 0
			else abs(1 - (node_sum / edges_sum )) * 100.0
		end as diff
	from w_edge_sum
)
select * from w_diff order by diff desc
;

comment on view @extschema@.v_add_res_total_geo is
	'View showing total estimates geographic additivity (aggregated class estimate should be equal to sum of sub-classes estimates). Hierarchy between aggregated classes and its sub-classes is defined in v_estimation_cell_hierarchy.';

comment on column @extschema@.v_add_res_total_geo.variable			is 'Estimate attribute -- variable. FKEY to t_variable.id.';
comment on column @extschema@.v_add_res_total_geo.aux_conf 			is 'Estimate auxiliary configuration. FKEY to t_aux_conf.id.';
comment on column @extschema@.v_add_res_total_geo.force_synthetic 		is 'Parameter showing whether estimate is forced to be synthetic.';
comment on column @extschema@.v_add_res_total_geo.estimate_conf			is 'Estimate configuration id. FKEY to t_estimate_conf.id.';
comment on column @extschema@.v_add_res_total_geo.estimation_cell 		is 'Estimate estimation cell. FKEY to c_estimation_cell.id.';
comment on column @extschema@.v_add_res_total_geo.point_est			is 'Aggregated class point estimate.';
comment on column @extschema@.v_add_res_total_geo.point_est_sum			is 'Sum of sub-classes point estimates (belonging to aggregated class).';
comment on column @extschema@.v_add_res_total_geo.estimation_cells_def		is 'Estimation cells defined in hierarchy (v_estimation_cell_hierarchy). Array of FKEYs to c_estimation_cell.id.';
comment on column @extschema@.v_add_res_total_geo.estimation_cells_found	is 'Estimation cells found in data (t_result). Array of FKEYs to c_estimation_cell.id.';
comment on column @extschema@.v_add_res_total_geo.estimate_confs_found		is 'Estimate configurations found in data (t_result). Array of FKEYs to t_estimate_donf.id.';
comment on column @extschema@.v_add_res_total_geo.diff				is 'Relative difference between aggregated class estimate and sum of sub-classes estimates.';

-- </view>

drop view @extschema@.v_add_res_ratio_attr;
-- <view name="v_add_res_ratio_attr" schema="extschema" src="views/extschema/v_add_res_ratio_attr.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

create view @extschema@.v_add_res_ratio_attr as
with w_res_cell_var as not materialized (
	select
		t_result.estimate_conf,
		t_result.point,
		t_total_estimate_conf.estimation_cell,
		t_total_estimate_conf.target_variable as t_variable__id,
		t_total_estimate_conf.aux_conf, force_synthetic, t_estimate_conf.denominator
	from @extschema@.t_result
	inner join @extschema@.t_estimate_conf ON t_estimate_conf.id = t_result.estimate_conf
	inner join @extschema@.t_total_estimate_conf ON t_total_estimate_conf.id = t_estimate_conf.total_estimate_conf
	where t_estimate_conf.estimate_type = 2
	and t_result.is_latest
)
, w_node_sum as (
	select
		estimation_cell,
		w_res_cell_var.t_variable__id, aux_conf, force_synthetic, denominator,
		estimate_conf as node_estimate_confs,
		sum(point) as node_sum
	from w_res_cell_var
	inner join @extschema@.v_variable_hierarchy as hierarchy on (hierarchy.node = w_res_cell_var.t_variable__id)
	group by estimation_cell, w_res_cell_var.t_variable__id, aux_conf, force_synthetic, estimate_conf, denominator
	order by estimation_cell, w_res_cell_var.t_variable__id, aux_conf, force_synthetic, estimate_conf, denominator
)
, w_edge_sum as (
	select
		w_node_sum.estimation_cell,
		w_node_sum.aux_conf,
		w_node_sum.force_synthetic,
		w_node_sum.node_estimate_confs, w_node_sum.denominator,
		hierarchy.node,
		w_node_sum.node_sum,
		hierarchy.edges as edges_def,
		array_agg(w_res_cell_var.t_variable__id order by w_res_cell_var.t_variable__id) as edges_found,
		array_agg(w_res_cell_var.estimate_conf order by w_res_cell_var.estimate_conf) as edges_estimate_confs,
		sum(w_res_cell_var.point) as edges_sum
	from w_node_sum
	inner join @extschema@.v_variable_hierarchy as hierarchy on (hierarchy.node = w_node_sum.t_variable__id)
	left join w_res_cell_var on (
		w_res_cell_var.estimation_cell = w_node_sum.estimation_cell
		and case when
			w_res_cell_var.aux_conf is null and w_node_sum.aux_conf is null then true
			else w_res_cell_var.aux_conf = w_node_sum.aux_conf end
		and case when
			w_res_cell_var.force_synthetic is null and w_node_sum.force_synthetic is null then true
			else w_res_cell_var.force_synthetic = w_node_sum.force_synthetic end
		and w_res_cell_var.t_variable__id = any(hierarchy.edges)) and w_res_cell_var.denominator = w_node_sum.denominator
	group by w_node_sum.estimation_cell, w_node_sum.aux_conf, w_node_sum.force_synthetic, w_node_sum.node_estimate_confs, w_node_sum.denominator,
		hierarchy.node, w_node_sum.node_sum, hierarchy.edges
)
, w_diff as (
	select
		estimation_cell,
		aux_conf,
		force_synthetic,
		node_estimate_confs	as estimate_conf, denominator,
		node			as variable,
		node_sum		as point_est,
		edges_sum		as point_est_sum,
		edges_def		as variables_def,
		edges_found		as variables_found,
		edges_estimate_confs	as estimate_confs_found,
		case
			when node_sum != 0.0 and edges_sum = 0.0 then 100.0
			when node_sum = 0.0 and edges_sum = 0.0 then 0
			else abs(1 - (node_sum / edges_sum )) * 100.0
		end as diff
	from w_edge_sum
)
select * from w_diff order by diff desc
;

comment on view @extschema@.v_add_res_ratio_attr is
	'View showing ratio estimates attribute additivity (aggregated class estimate should be equal to sum of sub-classes estimates). Hierarchy between aggregated classes and its sub-classes is defined in v_variable_hierarchy.';

comment on column @extschema@.v_add_res_ratio_attr.estimation_cell 	is 'Estimate estimation cell. FKEY to c_estimation_cell.id.';
comment on column @extschema@.v_add_res_ratio_attr.aux_conf 		is 'Estimate auxiliary configuration. FKEY to t_aux_conf.id.';
comment on column @extschema@.v_add_res_ratio_attr.force_synthetic 	is 'Parameter showing whether estimate is forced to be synthetic.';
comment on column @extschema@.v_add_res_ratio_attr.estimate_conf	is 'Estimate configuration id. FKEY to t_estimate_conf.id.';
comment on column @extschema@.v_add_res_ratio_attr.denominator		is 'Estimate configuration id of denominator. FKEY to t_total_estimate_conf.id.';
comment on column @extschema@.v_add_res_ratio_attr.variable		is 'Estimate attribute -- variable. FKEY to t_variable.id.';
comment on column @extschema@.v_add_res_ratio_attr.point_est		is 'Aggregated class point estimate.';
comment on column @extschema@.v_add_res_ratio_attr.point_est_sum	is 'Sum of sub-classes point estimates (belonging to aggregated class).';
comment on column @extschema@.v_add_res_ratio_attr.variables_def	is 'Attributes -- variables defined in hierarchy (v_variable_hierarchy). Array of FKEYs to t_variable.id.';
comment on column @extschema@.v_add_res_ratio_attr.variables_found	is 'Attributes -- variables found in data (t_result). Array of FKEYs to t_variable.id.';
comment on column @extschema@.v_add_res_ratio_attr.estimate_confs_found	is 'Estimate configurations found in data (t_result). Array of FKEYs to t_estimate_donf.id.';
comment on column @extschema@.v_add_res_ratio_attr.diff			is 'Relative difference between aggregated class estimate and sum of sub-classes estimates.';

-- </view>
