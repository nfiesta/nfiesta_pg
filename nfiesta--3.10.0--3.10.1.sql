-- Copyright 2017, 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.

COMMENT ON FUNCTION  nfiesta.fn_api_get_1pgroups4gregmap(INT, INT[], INT[]) IS 
'This function returns list of distinct strata combinations and panel refyearset groups ' 
'with a Boolean indicator saying whether there is a single-phase total configuration (using '
'the given panel refyearset group) for each GREG-map total estimate in the set of desired '
'estimates (a cartesian product of the subset of estimation cells intersecting all strata '
'of the given strata combination and the desired list of variables).';

COMMENT ON FUNCTION nfiesta.fn_api_get_panel_refyearset_combinations4groups(integer[]) IS 
'The function returns table of panels and reference-year sets belonging to any group of panels '
'and reference year set combinations passed as an argument. Only distinct pairs of panel and '
'reference-year sets are returned.';

COMMENT ON FUNCTION  nfiesta.fn_api_get_wmodels_sigma_paramtypes(INT[], INT[]) IS 
'For the input arrays of panels and estimation cells the function returns '
'a table with alternative combinations of working model, sigma and parameterisation '
'area type, for which the regression estimates of total can be configured. Each ' 
'record in the output table corresponds to a set of existing records in the table ' 
'nfiesta.t_aux_conf.' ;

COMMENT ON FUNCTION nfiesta.fn_api_get_group4panel_refyearset_combinations(integer[],integer[]) IS 
'The function returns the group of panel and reference year sets corresponding  '
'to the input arrays. If there is no such group, the function returns no records. '
'It also finds groups with no reference-year sets attached (NULLs in the '
'function argument _refyearsets) that are used for auxiliary configurations '
'within nfiesta.t_aux_conf. If the input argument _refyearsets contains only NULLs, it '
'must be explicitly cast to ::int[] within the function call.';

COMMENT ON FUNCTION nfiesta.fn_api_do_labels_and_descriptions_of_panel_refyearset_group_exist(VARCHAR(200), VARCHAR(200), TEXT, TEXT) IS 
'For each of the function arguments (label, label_en, description, description_en) the function checks '
'whether a record with identical value of the argument exists in the codelis of groups of panel and '
'reference-year combinations. The function returns four Booleans. The TRUE value indicate that a group '
'with the same value of the respective argument was found, otherwise FALSE is returned.';

COMMENT ON FUNCTION  nfiesta.fn_api_save_panel_refyearset_group(INT[], INT[], VARCHAR(200), TEXT, VARCHAR(200), TEXT) IS 
'This is a wrapper API function. Unlike the internally called fn_save_panel_refyearset_group '
'this API has no defaults. More specifically, labels and descriptions must always be provided. '
'The function returns a unique identifier of the newly inserted group';

COMMENT ON FUNCTION  nfiesta.fn_api_save_gregmap_total_config(INT, INT, INT, INT, INT, BOOLEAN, INT, BOOLEAN) IS 
'This is a wrapper API function saving the configuration of GREG estimates of total that use auxiliary data in the '
'form of maps.';

COMMENT ON FUNCTION  nfiesta.fn_api_get_gregmap_configs4ratio(INT, INT[], INT[], INT[]) IS 
'For input estimation period, estimation cells, nominator and denominator variables the function returns '
'valid combinations of panel-refyearset group, param. area type, force synthetic, model and sigma of already '
'configured GREG-map totals that can be combined in the denominator and nominator of the ratio. Only ratios '
'of GREG-map totals using the same group of panel and reference-year set combinations, an identical value of ' 
'force_synthetic (both the nominator and denominator must use the same inference, either model-based or design-based), '
'and the same type of parametrisation region (meaning that the actual parametrisation regions of nominator '
'and denominator of the ratio always match). Note that the function combines the elements of the input arrays ' 
'_variables_nom and _variables_denom (considering their order) to obtain the desired set of estimates. These two ' 
'input arrays must have an equal length, and their elements must be ordered, so each combination of their elements '
'on the same position corresponds to one desired combination of variables in the nominator and the denominator of ' 
'the ratio. The output parameter all_estimates_configurable is TRUE if such a ratio can be configured for all input '
'combinations of estimation cell, nominator variable and denominator variable. The output parameters '
'total_estimate_conf_nominator and total_estimate_conf_denominator are ordered arrays of identifiers of existing ' 
'GREG-map configurations (record ids of the table nfiesta.t_total_estimate_conf) for the nominator and denominator ' 
'respectively.';

COMMENT ON FUNCTION nfiesta.fn_api_save_ratio_config(INT, INT) IS 
'The function saves configuration of a ratio estimator into the nfiesta.t_estimate_conf table. '
'It presumes existence of total configurations for the nominator as well as the denominator of ' 
'the ratio. Any type of total estimators can be combined in the nominator and the denominator as '
'long as they use the same group of panels.';

COMMENT ON FUNCTION  nfiesta.fn_api_get_gregmap2single_configs4ratio(INT, INT[], INT[], INT[]) IS 
'For input estimation period, estimation cells, nominator and denominator variables the function returns ' 
'combinations of panel-refyearset group, type of parametrisation area, force_synthetic (always FALSE due '
'to the design-based single-phase total in the denominator and avoidance of mixed-minference), working model ' 
'and sigma of already configured GREG-map (nominator) and the corresponding single-phase (denominator) total ' 
'estimates. The parameter all_estimates_configurable is TRUE if the corresponding ratio can be configured for '
'all input combinations of estimation cell, nominator variable and denominator variable. The functuion combines ' 
'the elements of the input arrays variables_nom and variables_denom (considering their order) to obtain the ' 
'desired set of estimates. These two input arrays must have an equal length, and their elements must be ordered, ' 
'so each combination of their elements on the same position corresponds to one desired combination of variables ' 
'in the nominator and the denominator of the ratio. The output parameters total_estimate_conf_nominator and ' 
'total_estimate_conf_denominator are ordered arrays of identifiers of existing GREG-map or single-phase ' 
'configurations (record ids of the table nfiesta.t_total_estimate_conf) for the nominator and denominator '
'respectively.';

COMMENT ON FUNCTION  nfiesta.fn_api_get_single2gregmap_configs4ratio(INT, INT[], INT[], INT[]) IS 
'For input estimation period, estimation cells, nominator and denominator variables the function returns ' 
'combinations of panel-refyearset group, type of parametrisation area, force_synthetic (always FALSE due '
'to the design-based single-phase total in the denominator and avoidance of mixed-minference), working model ' 
'and sigma of already configured GREG-map (denominator) and the corresponding single-phase (nominator) total ' 
'estimates. The parameter all_estimates_configurable is TRUE if the corresponding ratio can be configured for '
'all input combinations of estimation cell, nominator variable and denominator variable. The functuion combines ' 
'the elements of the input arrays variables_nom and variables_denom (considering their order) to obtain the ' 
'desired set of estimates. These two input arrays must have an equal length, and their elements must be ordered, ' 
'so each combination of their elements on the same position corresponds to one desired combination of variables ' 
'in the nominator and the denominator of the ratio. The output parameters total_estimate_conf_nominator and ' 
'total_estimate_conf_denominator are ordered arrays of identifiers of existing GREG-map or single-phase ' 
'configurations (record ids of the table nfiesta.t_total_estimate_conf) for the nominator and denominator '
'respectively.';



