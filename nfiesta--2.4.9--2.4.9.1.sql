--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

update pg_extension SET extconfig = NULL, extcondition = NULL where extname = 'nfiesta';

SELECT pg_catalog.pg_extension_config_dump('@extschema@.c_area_domain', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.c_area_domain_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.c_area_domain_category', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.c_area_domain_category_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.c_auxiliary_variable', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.c_auxiliary_variable_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.c_auxiliary_variable_category', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.c_auxiliary_variable_category_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.c_country', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.c_country_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.c_estimation_cell', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.c_estimation_cell_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.c_estimation_cell_collection', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.c_estimation_cell_collection_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.cm_cell2param_area_mapping', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.cm_cell2param_area_mapping_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.cm_cluster2panel_mapping', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.cm_cluster2panel_mapping_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.cm_plot2cell_mapping', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.cm_plot2cell_mapping_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.cm_plot2cluster_config_mapping', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.cm_plot2cluster_config_mapping_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.cm_plot2param_area_mapping', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.cm_plot2param_area_mapping_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.cm_refyearset2panel_mapping', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.cm_refyearset2panel_mapping_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.c_param_area_type', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.c_param_area_type_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.c_state_or_change', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.c_state_or_change_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.c_sub_population', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.c_sub_population_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.c_sub_population_category', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.c_sub_population_category_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.c_target_variable', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.c_target_variable_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.c_variable_type', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.c_variable_type_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.f_a_cell', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.f_a_cell_gid_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.f_a_param_area', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.f_a_param_area_gid_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.f_p_plot', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.f_p_plot_gid_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_aux_conf', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_aux_conf_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_auxiliary_data', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_auxiliary_data_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_aux_total', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_aux_total_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_cluster', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_cluster_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_cluster_configuration', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_cluster_configuration_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_estimate_conf', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_estimate_conf_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_g_beta', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_g_beta_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_inventory_campaign', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_inventory_campaign_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_model', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_model_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_model_variables', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_model_variables_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_panel', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_panel_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_panel2aux_conf', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_panel2aux_conf_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_panel2total_1stph_estimate_conf', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_panel2total_1stph_estimate_conf_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_panel2total_2ndph_estimate_conf', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_panel2total_2ndph_estimate_conf_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_plot_measurement_dates', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_plot_measurement_dates_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_reference_year_set', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_reference_year_set_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_result', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_result_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_strata_set', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_strata_set_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_stratum', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_stratum_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_target_data', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_target_data_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_total_estimate_conf', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_total_estimate_conf_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_variable', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_variable_id_seq', '');
