-- Copyright 2017, 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.

-- <function name="@extschema@.fn_1p_total_var(integer)" schema="extschema" src="functions/extschema/fn_1p_total_var.sql">
--
-- Copyright 2017, 2023 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_1p_total_var(integer)

-- DROP FUNCTION @extschema@.fn_1p_total_var(integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_1p_total_var(
    IN conf_id integer
)
  RETURNS TABLE(
	attribute integer,
	point1p double precision,
	var1p double precision,
	point2p double precision,
	var2p double precision,
	min_ssize double precision,
	act_ssize bigint,
	est_info json
) AS
$BODY$
DECLARE
	_complete_query text;
BEGIN
	--------------------------------QUERY--------------------------------
	_complete_query := '
-------------------------------------------------------------------------------
------------------------------------2p TOTAL-----------------------------------
-------------------------------------------------------------------------------

	with w_data AS MATERIALIZED (
		select * from @extschema@.fn_1p_data(' || conf_id || ') where is_target
	)
-------------------------------------------------------------------------------
------------------------------------RESULTS------------------------------------
-------------------------------------------------------------------------------

------------------------------------2p
------------------------------------USING point SQL


------------------------------------2p var using HTC implemented in C function
--DROP EXTENSION htc; CREATE EXTENSION htc;
, w_data_agg AS MATERIALIZED (
	SELECT
		stratum, attribute, nb_sampling_units, sweight_strata_sum, lambda_d_plus as buffered_area_ha,
		array_agg(cluster order by cluster) as cids,
		array_agg(ldsity_d order by cluster) as ldsitys,
		array_agg(sweight * (lambda_d_plus / sweight_strata_sum) order by cluster) as sweights
	FROM
		w_data
		where ldsity_d != 0.0
		group by stratum, attribute, nb_sampling_units, sweight_strata_sum, buffered_area_ha
		order by attribute
)
, w_est1p_stratum AS MATERIALIZED (
	SELECT
		stratum, attribute,
		htc_compute_sweight_ha(cids, ldsitys, sweights, nb_sampling_units) as res
	from w_data_agg
)
, w_est1p AS MATERIALIZED (
	SELECT
		conf.attribute,
		coalesce(sum((res).total), 0.0) as point1p, coalesce(sum((res).var), 0.0) AS var1p
	from w_est1p_stratum
	right join (select distinct attribute from w_data) as conf on (w_est1p_stratum.attribute = conf.attribute)
	group by conf.attribute order by conf.attribute
)
, w_units_json AS MATERIALIZED (
	select attribute, array_to_json(array_agg(row_to_json(t))) as s_units from
	(
		select
			stratum, attribute,
			count(*) as s_units_param_area,
			sum(cluster_is_in_cell::int) as s_units_cell,
			sum((ldsity_d != 0)::int) as s_units_cell_nonzero
		from w_data
		group by stratum, attribute
		order by stratum, attribute
	) as t group by attribute
)
, w_minssize AS MATERIALIZED (
	with
	w_input as materialized (
		select attribute, stratum, sweight, ldsity_d as in_vec from w_data
	), w_check_strata as (
		select count(*) != 1 as more_strata from (select stratum from w_input group by stratum) as t
	), w_sample_mean as (
		select attribute, sum(in_vec) / count(in_vec) as sample_mean_scalar, count(in_vec) as n from w_input group by attribute
	), w_res_pow_2 as (
		select attribute, sum(power(in_vec - sample_mean_scalar, 2)) as res_2 from w_input
			inner join w_sample_mean using (attribute) group by attribute
	), w_res_pow_3 as (
		select attribute, sum(power(in_vec - sample_mean_scalar, 3)) as res_3 from w_input
			inner join w_sample_mean using (attribute) group by attribute
	), w_g_one as (
		select attribute, power(n, 0.5) * (res_3 / power(res_2, 1.5)) as g_one from w_sample_mean
			inner join w_res_pow_2 using (attribute) inner join w_res_pow_3 using (attribute) group by attribute, n, res_2, res_3
	), w_g_one_big as (
		select attribute, n, case when n > 2 then power(n * (n-1), 0.5) / (n - 2) * g_one else 0 end as g_one_big from w_sample_mean
			inner join w_g_one using (attribute) group by attribute, n, g_one
	), w_min_ssize as (
		select attribute, n, 25 * power(g_one_big, 2) as min_ssize from w_g_one_big group by attribute, n, g_one_big
	)
	select
		attribute,
		case when (select n from w_sample_mean) <= 2 then -4 else
			case when more_strata then -3 else
				case when (res_2 = 0.0) then -1 else
					(select min_ssize from w_min_ssize)
				end
			end
		end as min_ssize,
		(select n from w_sample_mean) as act_ssize
	from w_res_pow_2, w_check_strata
)
select
	w_est1p.attribute,
	point1p, var1p,
	NULL::double precision as point2p, NULL::double precision as var2p,
	min_ssize, act_ssize,
	s_units
from w_est1p
inner join w_units_json using (attribute)
inner join w_minssize using (attribute)
        ';

	--RAISE NOTICE '%', _complete_query;
	RETURN QUERY EXECUTE _complete_query;
END;
$BODY$
  LANGUAGE plpgsql STABLE
;

COMMENT ON FUNCTION @extschema@.fn_1p_total_var(integer) IS
'For the input argument corresponding to the id of single-phase total configuration '
'the function calculates the single-phase total, its variance and minimum sample size. '
'Negative mimimum sample size indicates one of the specific situations, see '
'https://gitlab.com/nfiesta/nfiesta_pg/-/wikis/Estimates-Calculation#minimal-sample-size-error-codes.';

-- </function>

-- <function name="@extschema@.fn_1p1p_ratio_var(integer)" schema="extschema" src="functions/extschema/fn_1p1p_ratio_var.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_1p1p_ratio_var(integer, integer)

-- DROP FUNCTION @extschema@.fn_1p1p_ratio_var(integer, integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_1p1p_ratio_var(
    IN conf_id integer,
    IN conf_id__denom integer
)
  RETURNS TABLE(
	attribute integer,
	point1p double precision,
	var1p double precision,
	point2p double precision,
	var2p double precision,
	min_ssize double precision,
	act_ssize bigint,
	est_info json
) AS
$BODY$
DECLARE
	_complete_query text;
BEGIN
	--------------------------------QUERY--------------------------------
	_complete_query := '
-------------------------------------------------------------------------------
------------------------------------2p RATIO-----------------------------------
-------------------------------------------------------------------------------
	with w_conf AS MATERIALIZED (
            select nom.id as nom_conf_id, denom.id as denom_conf_id
            from (select * from @extschema@.t_total_estimate_conf where id = ' || conf_id || ') as nom
            , (select * from @extschema@.t_total_estimate_conf where id = ' || conf_id__denom || ') as denom
        )
------------------------------------nominator-----------------------------------
        , w_data__nom AS MATERIALIZED (
		select
			gid, cluster, cluster_is_in_cell, stratum, attribute, nb_sampling_units, sweight_strata_sum, lambda_d_plus,
			ldsity_d,
			sweight * (lambda_d_plus/sweight_strata_sum) AS sweight 
		from (select (@extschema@.fn_1p_data(nom_conf_id)).* from w_conf) as alldata where is_target
	)
	, w_data_agg__nom  AS MATERIALIZED (
		SELECT
			stratum, attribute, nb_sampling_units, sweight_strata_sum, lambda_d_plus as buffered_area_ha,
			array_agg(cluster order by cluster) as cids,
			array_agg(ldsity_d order by cluster) as ldsitys,
			array_agg(sweight order by cluster) as sweights
		FROM
			w_data__nom
			where ldsity_d != 0.0
			group by stratum, attribute, nb_sampling_units, sweight_strata_sum, buffered_area_ha
			order by attribute
	)
	, w_est1p_stratum__nom  AS MATERIALIZED (
		SELECT
			stratum, attribute,
			htc_compute(cids, ldsitys, sweights, buffered_area_ha, sweight_strata_sum, nb_sampling_units) as res
		from w_data_agg__nom
	)
	, w_est1p__nom  AS MATERIALIZED (
		SELECT
			conf.attribute,
			coalesce(sum((res).total), 0.0) as point1p, coalesce(sum((res).var), 0.0) AS var1p
		from w_est1p_stratum__nom
		right join (select distinct attribute from w_data__nom) as conf on (w_est1p_stratum__nom.attribute = conf.attribute)
		group by conf.attribute order by conf.attribute
	)
------------------------------------denominator-----------------------------------
        , w_data__denom AS MATERIALIZED (
		select
 			gid, cluster, cluster_is_in_cell, stratum, attribute, nb_sampling_units, sweight_strata_sum, lambda_d_plus,
			ldsity_d,
			sweight * (lambda_d_plus/sweight_strata_sum) AS sweight 
		from (select (@extschema@.fn_1p_data(denom_conf_id)).* from w_conf) as alldata where is_target
	)
	, w_data_agg__denom  AS MATERIALIZED (
		SELECT
			stratum, attribute, nb_sampling_units, sweight_strata_sum, lambda_d_plus as buffered_area_ha,
			array_agg(cluster order by cluster) as cids,
			array_agg(ldsity_d order by cluster) as ldsitys,
			array_agg(sweight order by cluster) as sweights
		FROM
			w_data__denom
			where ldsity_d != 0.0
			group by stratum, attribute, nb_sampling_units, sweight_strata_sum, buffered_area_ha
			order by attribute
	)
	, w_est1p_stratum__denom  AS MATERIALIZED (
		SELECT
			stratum, attribute,
			htc_compute(cids, ldsitys, sweights, buffered_area_ha, sweight_strata_sum, nb_sampling_units) as res
		from w_data_agg__denom
	)
	, w_est1p__denom  AS MATERIALIZED (
		SELECT
			conf.attribute,
			coalesce(sum((res).total), 0.0) as point1p, coalesce(sum((res).var), 0.0) AS var1p
		from w_est1p_stratum__denom
		right join (select distinct attribute from w_data__denom) as conf on (w_est1p_stratum__denom.attribute = conf.attribute)
		group by conf.attribute order by conf.attribute
	)
-------------------------------------------------------------------------------
------------------------------------RESULTS------------------------------------
-------------------------------------------------------------------------------
------------------------------------2p var using HTC implemented in C function
, w_ratio_1p AS MATERIALIZED (
        SELECT nom.attribute as c, 
            case when denom.total = 0 then NULL::double precision else nom.total / denom.total end as val,
            denom.total as denom_total from 
        (select attribute, point1p::double precision as total from w_est1p__nom) as nom,
        (select attribute, point1p::double precision as total from w_est1p__denom) as denom
)
, w_data_1pr_agg AS MATERIALIZED (
        SELECT
	    stratum, w_ratio_1p.c as attribute, nb_sampling_units, sweight_strata_sum, lambda_d_plus as buffered_area_ha,
            array_agg(cluster order by cluster) as cids,
            array_agg(nom.ldsity_d - denom.ldsity_d * w_ratio_1p.val order by cluster) as z,
            array_agg(sweight order by cluster) as sweights
        FROM
            (select * from w_data__nom order by cluster) as nom
            inner join (select cluster, ldsity_d from w_data__denom order by cluster) as denom using (cluster)
            inner join w_ratio_1p on (nom.attribute = w_ratio_1p.c)
	group by stratum, w_ratio_1p.c, nb_sampling_units, sweight_strata_sum, buffered_area_ha order by attribute
)
, w_est1pr_stratum AS MATERIALIZED (
	SELECT 
            stratum, attribute, (htc_compute(cids, z, sweights, buffered_area_ha, sweight_strata_sum, nb_sampling_units)).* 
	    from w_data_1pr_agg
)
, w_est1pr AS MATERIALIZED (
	SELECT
	    attribute, sum(total) as point_residual, sum(var) AS var1p 
	    from w_est1pr_stratum group by attribute order by attribute
)
, w_1p_ratio_var AS MATERIALIZED (
	select 
            w_est1pr.attribute, 
	    w_ratio_1p.val as point1p, 
            case when denom_total = 0 then NULL::double precision else (1.0 / denom_total^2) * w_est1pr.var1p end as var1p
	    from     	w_est1pr 
	    inner join 	w_ratio_1p on (w_est1pr.attribute = w_ratio_1p.c)
)
, w_units_json AS MATERIALIZED (
	select attribute_nom as attribute, array_to_json(array_agg(row_to_json(t))) as s_units from
	(
		with w_nom as (select
			stratum, attribute,
			count(*) as s_units_param_area,
			sum(cluster_is_in_cell::int) as s_units_cell,
			sum((ldsity_d != 0)::int) as s_units_cell_nonzero
		from w_data__nom
		group by stratum, attribute
		order by stratum, attribute)
		, w_denom as (select
			stratum, attribute,
			count(*) as s_units_param_area,
			sum(cluster_is_in_cell::int) as s_units_cell,
			sum((ldsity_d != 0)::int) as s_units_cell_nonzero
		from w_data__denom
		group by stratum, attribute
		order by stratum, attribute)
		select
			w_nom.attribute			as attribute_nom,
			w_nom.stratum			as stratum_nom,
			w_nom.s_units_param_area	as s_units_param_area_nom,
			w_nom.s_units_cell		as s_units_cell_nom,
			w_nom.s_units_cell_nonzero	as s_units_cell_nonzero_nom,
			w_denom.attribute		as attribute_denom,
			w_denom.stratum			as stratum_denom,
			w_denom.s_units_param_area	as s_units_param_area_denom,
			w_denom.s_units_cell		as s_units_cell_denom,
			w_denom.s_units_cell_nonzero	as s_units_cell_nonzero_denom
		from w_nom inner join w_denom using (stratum) --!!!depends on condition, that it will be not used with multiple attributes in nom / denom setting
	) as t group by attribute
)
, w_minssize AS MATERIALIZED (
	with
	w_input as materialized (
		select attribute, stratum, unnest(sweights) as sweight, unnest(z) as in_vec from w_data_1pr_agg
	), w_check_strata as (
		select count(*) != 1 as more_strata from (select stratum from w_input group by stratum) as t
	), w_sample_mean as (
		select attribute, sum(in_vec) / count(in_vec) as sample_mean_scalar, count(in_vec) as n from w_input group by attribute
	), w_res_pow_2 as (
		select attribute, sum(power(in_vec - sample_mean_scalar, 2)) as res_2 from w_input
			inner join w_sample_mean using (attribute) group by attribute
	), w_res_pow_3 as (
		select attribute, sum(power(in_vec - sample_mean_scalar, 3)) as res_3 from w_input
			inner join w_sample_mean using (attribute) group by attribute
	), w_g_one as (
		select attribute, power(n, 0.5) * (res_3 / power(res_2, 1.5)) as g_one from w_sample_mean
			inner join w_res_pow_2 using (attribute) inner join w_res_pow_3 using (attribute) group by attribute, n, res_2, res_3
	), w_g_one_big as (
		select attribute, n, case when n > 2 then power(n * (n-1), 0.5) / (n - 2) * g_one else 0 end as g_one_big from w_sample_mean
			inner join w_g_one using (attribute) group by attribute, n, g_one
	), w_min_ssize as (
		select attribute, n, 25 * power(g_one_big, 2) as min_ssize from w_g_one_big group by attribute, n, g_one_big
	)
	select
		attribute,
		case when (select n from w_sample_mean) <= 2 then -4 else
			case when more_strata then -3 else
				case when (res_2 = 0.0) then -1 else
					(select min_ssize from w_min_ssize)
				end
			end
		end as min_ssize,
		(select n from w_sample_mean) as act_ssize
	from w_res_pow_2, w_check_strata
)
select
	attribute,
	point1p, var1p,
	NULL::double precision as point2p, NULL::double precision as var2p,
	min_ssize, act_ssize,
	s_units
from w_1p_ratio_var
inner join w_units_json using (attribute)
inner join w_minssize using (attribute)
;
        ';

	--RAISE NOTICE '%', _complete_query;
	RETURN QUERY EXECUTE _complete_query;
END;
$BODY$
  LANGUAGE plpgsql STABLE
;

COMMENT ON FUNCTION @extschema@.fn_1p1p_ratio_var(integer, integer) IS 
'For the input arguments corresponding to the ids of single-phase total configurations '
'(one for nominator and the other for denominator) of a ratio estimator, the function '
'calculates the single-phase total, its variance and minimum sample size. Negative mimimum '
'sample size indicates one of the specific situations, see '
'https://gitlab.com/nfiesta/nfiesta_pg/-/wikis/Estimates-Calculation#minimal-sample-size-error-codes.';

-- </function>
