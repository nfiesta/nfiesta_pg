--
-- Copyright 2017, 2021 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- new lookup_table with estimation_period
CREATE TABLE @extschema@.c_estimation_period (
id			serial 		NOT NULL,
estimate_date_begin	date 		NOT NULL,
estimate_date_end	date 		NOT NULL,
label			varchar(200)	NOT NULL,
description		text		NOT NULL,
CONSTRAINT pkey__c_estimation_period PRIMARY KEY (id)
);

ALTER TABLE @extschema@.c_estimation_period ADD CONSTRAINT
chck__c_estimation_period__estimate_date_begin_end CHECK
(estimate_date_begin < estimate_date_end);

ALTER TABLE @extschema@.c_estimation_period ADD CONSTRAINT
ukey__c_estimation_period__estimate_date_begin_end UNIQUE
(estimate_date_begin, estimate_date_end);

ALTER TABLE @extschema@.c_estimation_period ADD CONSTRAINT
ukey__c_estimation_period__label UNIQUE
(label);

GRANT SELECT ON TABLE @extschema@.c_estimation_period TO PUBLIC;
GRANT SELECT ON SEQUENCE @extschema@.c_estimation_period_id_seq TO PUBLIC;

COMMENT ON TABLE @extschema@.c_estimation_period IS 'Lookup table with estimation periods specifically bounded with two dates (begin, end).';
COMMENT ON COLUMN @extschema@.c_estimation_period.id IS 'Identificator of the category.';
COMMENT ON COLUMN @extschema@.c_estimation_period.estimate_date_begin IS 'Begin of the estimation period.';
COMMENT ON COLUMN @extschema@.c_estimation_period.estimate_date_end IS 'End of the estimation period.';
COMMENT ON COLUMN @extschema@.c_estimation_period.label IS 'Label of the category.';
COMMENT ON COLUMN @extschema@.c_estimation_period.description IS 'Detailed description of the category.';

INSERT INTO @extschema@.c_estimation_period (estimate_date_begin, estimate_date_end, label, description)
SELECT DISTINCT estimate_date_begin, estimate_date_end, 
	concat(estimate_date_begin::text, '-', estimate_date_end::text)::text,
	concat(estimate_date_begin::text, '-', estimate_date_end::text)::text
FROM @extschema@.t_total_estimate_conf
ORDER BY estimate_date_begin, estimate_date_end;

-- add reference to this new lookup
ALTER TABLE @extschema@.t_total_estimate_conf ADD COLUMN estimation_period integer;
COMMENT ON COLUMN @extschema@.t_total_estimate_conf.estimation_period IS 'Estimation period, foreign key to lookup table c_estimation_period.';

ALTER TABLE @extschema@.t_total_estimate_conf ADD CONSTRAINT
fkey__t_total_estimate_conf__c_estimation_period FOREIGN KEY (estimation_period)
REFERENCES @extschema@.c_estimation_period;

UPDATE @extschema@.t_total_estimate_conf SET estimation_period = t1.id
FROM @extschema@.c_estimation_period AS t1
WHERE 	t_total_estimate_conf.estimate_date_begin = t1.estimate_date_begin AND
	t_total_estimate_conf.estimate_date_end = t1.estimate_date_end;

DROP VIEW @extschema@.v_conf_overview;

DROP INDEX @extschema@.uidx__t_total_estimate_conf__all_columns;
--CREATE UNIQUE INDEX uidx__t_total_estimate_conf__all_columns ON @extschema@.t_total_estimate_conf
--USING btree (estimation_cell, estimation_period, target_variable, phase_estimate_type, coalesce(force_synthetic,false), coalesce(aux_conf,0));

ALTER TABLE @extschema@.t_total_estimate_conf DROP COLUMN estimate_date_begin;
ALTER TABLE @extschema@.t_total_estimate_conf DROP COLUMN estimate_date_end;

-- lookup for panel and refyearset groups
CREATE TABLE @extschema@.c_panel_refyearset_group (
id		serial		NOT NULL,
label		varchar(200) 	NOT NULL,
description	text		NOT NULL,
panels		integer[],
reference_year_sets integer[],
CONSTRAINT pkey__c_panel_refyearset_group__id PRIMARY KEY (id)
);

GRANT SELECT ON TABLE @extschema@.c_panel_refyearset_group TO PUBLIC;
GRANT SELECT ON SEQUENCE @extschema@.c_panel_refyearset_group_id_seq TO PUBLIC;

COMMENT ON TABLE @extschema@.c_panel_refyearset_group IS 'Lookup table with named aggregated sets of panels and corresponding reference year sets.';
COMMENT ON COLUMN @extschema@.c_panel_refyearset_group.id IS 'Identificator of the category.';
COMMENT ON COLUMN @extschema@.c_panel_refyearset_group.label IS 'Label of the category.';
COMMENT ON COLUMN @extschema@.c_panel_refyearset_group.description IS 'Detailed description of the category.';

-- addition of reference in t_total_estimate_conf
ALTER TABLE @extschema@.t_total_estimate_conf ADD COLUMN panel_refyearset_group integer;

ALTER TABLE @extschema@.t_total_estimate_conf ADD CONSTRAINT
fkey__t_total_estimate_conf__c_panel_refyearset_group FOREIGN KEY (panel_refyearset_group)
REFERENCES @extschema@.c_panel_refyearset_group (id);

COMMENT ON COLUMN @extschema@.t_total_estimate_conf.panel_refyearset_group IS 'Group of panels and appropriate reference year sets used in the estimate of total. This group determines the dataset which enters the calculation. Foreign key to table c_panel_refyearset_group.';

CREATE UNIQUE INDEX uidx__t_total_estimate_conf__all_columns ON @extschema@.t_total_estimate_conf
USING btree (estimation_cell, estimation_period, target_variable, phase_estimate_type, coalesce(force_synthetic,false), coalesce(aux_conf,0), panel_refyearset_group);


-- addition of reference in t_aux_conf
ALTER TABLE @extschema@.t_aux_conf ADD COLUMN panel_refyearset_group integer;

ALTER TABLE @extschema@.t_aux_conf ADD CONSTRAINT
fkey__t_aux_conf__c_panel_refyearset_group FOREIGN KEY (panel_refyearset_group)
REFERENCES @extschema@.c_panel_refyearset_group (id);

COMMENT ON COLUMN @extschema@.t_aux_conf.panel_refyearset_group IS 'Group of panels and appropriate reference year sets used in the estimate of total. This group determines the dataset which enters the calculation. Foreign key to table c_panel_refyearset_group.';

-- t_panel2total_1stph_estimate_conf - unresolved

CREATE TABLE @extschema@.t_panel_refyearset_group (
id			serial		NOT NULL,
panel_refyearset_group	integer		NOT NULL,
panel			integer		NOT NULL,
reference_year_set	integer,
CONSTRAINT pkey__t_panel_refyearset_group PRIMARY KEY (id)
);

ALTER TABLE @extschema@.t_panel_refyearset_group ADD CONSTRAINT
fkey__t_panel_refyearset_group__c_panel_refyearset_group FOREIGN KEY (panel_refyearset_group)
REFERENCES @extschema@.c_panel_refyearset_group (id);

ALTER TABLE @extschema@.t_panel_refyearset_group ADD CONSTRAINT
fkey__t_panel_refyearset_group__t_panel FOREIGN KEY (panel)
REFERENCES sdesign.t_panel (id);

ALTER TABLE @extschema@.t_panel_refyearset_group ADD CONSTRAINT
fkey__t_panel_refyearset_group__t_reference_year_set FOREIGN KEY (reference_year_set)
REFERENCES sdesign.t_reference_year_set (id);

GRANT SELECT ON TABLE @extschema@.t_panel_refyearset_group TO PUBLIC;
GRANT SELECT ON SEQUENCE @extschema@.t_panel_refyearset_group_id_seq TO PUBLIC;

COMMENT ON TABLE @extschema@.t_panel_refyearset_group IS 'Concrete group of panels and corresponding reference year sets under the labeled lookup item.';
COMMENT ON COLUMN @extschema@.t_panel_refyearset_group.panel_refyearset_group IS 'Identificator of the group, foreign key to table c_panel_refyearset_group.';
COMMENT ON COLUMN @extschema@.t_panel_refyearset_group.panel IS 'Identificator of the panel, foreign key to table t_panel.';
COMMENT ON COLUMN @extschema@.t_panel_refyearset_group.reference_year_set IS 'Identificator of the reference year set, foregin key to table t_reference_year_set.';

-- data for 1p
WITH w_array AS (
	SELECT 	total_estimate_conf, 
		array_agg(t1.panel ORDER BY t1.panel, t1.reference_year_set) AS panels, 
		array_agg(t1.reference_year_set ORDER BY t1.panel, t1.reference_year_set) AS refyearsets,
		min(extract('year' FROM reference_date_begin)) AS year_begin,
		max(extract('year' FROM reference_date_end)) AS year_end,
		sum(plot_count) AS plot_sum,
		count(distinct t3.id) AS panel_count
	FROM @extschema@.t_panel2total_2ndph_estimate_conf AS t1
	INNER JOIN sdesign.t_reference_year_set AS t2
	ON t1.reference_year_set = t2.id
	INNER JOIN sdesign.t_panel AS t3
	ON t1.panel = t3.id
	GROUP BY total_estimate_conf
),
w_dist AS (
	SELECT DISTINCT
		panels, refyearsets, year_begin, year_end, plot_sum, panel_count
	FROM w_array
),
w_lookup AS (
	INSERT INTO @extschema@.c_panel_refyearset_group (label, description, panels, reference_year_sets)
	SELECT 	concat('no_of_panels:', panel_count,';',year_begin,'-',year_end,';plot_count:',plot_sum), 
		concat('no_of_panels:', panel_count,';',year_begin,'-',year_end,';plot_count:',plot_sum), 
		panels, refyearsets
	FROM w_dist
	RETURNING id, panels, reference_year_sets
)
INSERT INTO @extschema@.t_panel_refyearset_group(panel_refyearset_group,panel,reference_year_set)
SELECT
	t1.panel_refyearset_group, panel, reference_year_set
FROM
	(SELECT t1.id AS panel_refyearset_group, t2.panel, t2.panel_id 
	FROM 	w_lookup AS t1, 
		unnest(t1.panels) WITH ORDINALITY AS t2(panel, panel_id)
	) AS t1
INNER JOIN
	(SELECT t1.id AS panel_refyearset_group, t2.reference_year_set, t2.ref_id 
	FROM 	w_lookup AS t1, 
		unnest(t1.reference_year_sets) WITH ORDINALITY AS t2(reference_year_set, ref_id)
	) AS t2
ON t1.panel_refyearset_group = t2.panel_refyearset_group AND t1.panel_id = t2.ref_id;
;


UPDATE @extschema@.t_total_estimate_conf SET panel_refyearset_group = t1.panel_refyearset_group
FROM
	(SELECT
		t1.total_estimate_conf,
		t2.id AS panel_refyearset_group

	FROM	(SELECT 
			t1.total_estimate_conf, 
			array_agg(t1.panel ORDER BY t1.panel, t1.reference_year_set) AS panels, 
			array_agg(t1.reference_year_set ORDER BY panel, t1.reference_year_set) AS refyearsets
		FROM @extschema@.t_panel2total_2ndph_estimate_conf AS t1
		GROUP BY t1.total_estimate_conf
		) AS t1
	INNER JOIN
			@extschema@.c_panel_refyearset_group AS t2
	ON t1.panels = t2.panels AND t1.refyearsets = t2.reference_year_sets
	) AS t1
WHERE t_total_estimate_conf.id = t1.total_estimate_conf;

-- data for 2p
WITH w_array AS (
	SELECT 	aux_conf, 
		array_agg(t1.panel ORDER BY t1.panel, t1.reference_year_set) AS panels, 
		sum(plot_count) AS plot_sum,
		count(distinct t3.id) AS panel_count
	FROM @extschema@.t_panel2aux_conf AS t1
	INNER JOIN sdesign.t_panel AS t3
	ON t1.panel = t3.id
	GROUP BY aux_conf
),
w_dist AS (
	SELECT DISTINCT
		panels, plot_sum, panel_count
	FROM w_array
),
w_lookup AS (
	INSERT INTO @extschema@.c_panel_refyearset_group (label, description, panels, reference_year_sets)
	SELECT 	concat('no_of_panels:', panel_count,';plot_count:',plot_sum), 
		concat('no_of_panels:', panel_count,';plot_count:',plot_sum), 
		panels, NULL::int[] AS reference_year_sets
	FROM w_dist
	EXCEPT
	SELECT label, description, panels, reference_year_sets
	FROM @extschema@.c_panel_refyearset_group
	RETURNING id, panels
)
INSERT INTO @extschema@.t_panel_refyearset_group(panel_refyearset_group,panel)
SELECT
	t1.panel_refyearset_group, panel
FROM
	(SELECT t1.id AS panel_refyearset_group, t2.panel, t2.panel_id 
	FROM 	w_lookup AS t1, 
		unnest(t1.panels) WITH ORDINALITY AS t2(panel, panel_id)
	) AS t1
;

UPDATE @extschema@.t_aux_conf SET panel_refyearset_group = t1.panel_refyearset_group
FROM
	(SELECT
		t1.aux_conf,
		t2.id AS panel_refyearset_group

	FROM	(SELECT 
			t1.aux_conf, 
			array_agg(t1.panel ORDER BY t1.panel, t1.reference_year_set) AS panels 
		FROM @extschema@.t_panel2aux_conf AS t1
		GROUP BY t1.aux_conf
		) AS t1
	INNER JOIN
		@extschema@.c_panel_refyearset_group AS t2
	ON t1.panels = t2.panels
	WHERE t2.reference_year_sets IS NULL
	) AS t1
WHERE t_aux_conf.id = t1.aux_conf;

-- set not null
ALTER TABLE @extschema@.t_total_estimate_conf ALTER COLUMN estimation_period SET NOT NULL;
ALTER TABLE @extschema@.t_total_estimate_conf ALTER COLUMN panel_refyearset_group SET NOT NULL;
ALTER TABLE @extschema@.t_aux_conf ALTER COLUMN panel_refyearset_group SET NOT NULL;

-- deletion of redundant columns
ALTER TABLE @extschema@.c_panel_refyearset_group DROP COLUMN panels;
ALTER TABLE @extschema@.c_panel_refyearset_group DROP COLUMN reference_year_sets;

SELECT pg_catalog.pg_extension_config_dump('@extschema@.c_estimation_period','');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.c_panel_refyearset_group','');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_panel_refyearset_group','');

-- drop of current table which holds combination of panels and reference_year_sets
DROP TABLE @extschema@.t_panel2total_2ndph_estimate_conf;
DROP TABLE @extschema@.t_panel2aux_conf;
-- t_panel2total_1stph_estimate_conf


--ADDDING LABEL_EN AND DESCRIPTION_EN TO THE LOOKUP TABLES

ALTER TABLE @extschema@.c_area_domain
ADD COLUMN label_en character varying(250);

COMMENT ON COLUMN @extschema@.c_area_domain.label_en IS 'Label of area domain.';

ALTER TABLE @extschema@.c_area_domain
ADD COLUMN description_en text;

COMMENT ON COLUMN @extschema@.c_area_domain.description_en IS 'Description of area domain.';

ALTER TABLE @extschema@.c_area_domain_category
ADD COLUMN label_en character varying(250);

COMMENT ON COLUMN @extschema@.c_area_domain_category.label_en IS 'Label of area domain.';

ALTER TABLE @extschema@.c_area_domain_category
ADD COLUMN description_en text;

COMMENT ON COLUMN @extschema@.c_area_domain_category.description_en IS 'Description of area domain.';

ALTER TABLE @extschema@.c_auxiliary_variable
ADD COLUMN label_en character varying(120);

COMMENT ON COLUMN @extschema@.c_auxiliary_variable.label_en IS 'Label of auxiliary variable.';

ALTER TABLE @extschema@.c_auxiliary_variable
ADD COLUMN description_en text;

COMMENT ON COLUMN @extschema@.c_auxiliary_variable.description_en IS 'Description of auxiliary variable.';

ALTER TABLE @extschema@.c_auxiliary_variable_category
ADD COLUMN label_en character varying(120);

COMMENT ON COLUMN @extschema@.c_auxiliary_variable_category.label_en IS 'Label of auxiliary variable category.';

ALTER TABLE @extschema@.c_auxiliary_variable_category
ADD COLUMN description_en text;

COMMENT ON COLUMN @extschema@.c_auxiliary_variable_category.description_en IS 'Description of auxiliary variable category.';

ALTER TABLE @extschema@.c_estimation_cell
ADD COLUMN label_en character varying(20);

COMMENT ON COLUMN @extschema@.c_estimation_cell.label_en IS 'Label of estimation cell.';

ALTER TABLE @extschema@.c_estimation_cell
ADD COLUMN description_en text;

COMMENT ON COLUMN @extschema@.c_estimation_cell.description_en IS 'Description of estimation cell.';

ALTER TABLE @extschema@.c_estimation_cell_collection
ADD COLUMN label_en character varying(120);

COMMENT ON COLUMN @extschema@.c_estimation_cell_collection.label_en IS 'Label of estimation cell collection.';

ALTER TABLE @extschema@.c_estimation_cell_collection
ADD COLUMN description_en text;

COMMENT ON COLUMN @extschema@.c_estimation_cell_collection.description_en IS 'Description of estimation cell collection.';

ALTER TABLE @extschema@.c_estimation_period
ADD COLUMN label_en character varying(200);

COMMENT ON COLUMN @extschema@.c_estimation_period.label_en IS 'Label of the category.';

ALTER TABLE @extschema@.c_estimation_period
ADD COLUMN description_en text;

COMMENT ON COLUMN @extschema@.c_estimation_period.description_en IS 'Detailed description of the category.';

ALTER TABLE @extschema@.c_panel_refyearset_group
ADD COLUMN label_en character varying(200);

COMMENT ON COLUMN @extschema@.c_panel_refyearset_group.label_en IS 'Label of the category.';

ALTER TABLE @extschema@.c_panel_refyearset_group
ADD COLUMN description_en text;

COMMENT ON COLUMN @extschema@.c_panel_refyearset_group.description_en IS 'Detailed description of the category.';

ALTER TABLE @extschema@.c_param_area_type
ADD COLUMN label_en character varying(100);

COMMENT ON COLUMN @extschema@.c_param_area_type.label_en IS 'Short description of the type (e.g. Inspire 100x100 with buffer).';

ALTER TABLE @extschema@.c_param_area_type
ADD COLUMN description_en text;

COMMENT ON COLUMN @extschema@.c_param_area_type.description_en IS 'Description of the type.';

ALTER TABLE @extschema@.c_state_or_change
ADD COLUMN label_en character varying(120);

COMMENT ON COLUMN @extschema@.c_state_or_change.label_en IS 'Label of the state or change category.';

ALTER TABLE @extschema@.c_state_or_change
ADD COLUMN description_en text;

COMMENT ON COLUMN @extschema@.c_state_or_change.description_en IS 'Description of the state or change category.';

ALTER TABLE @extschema@.c_sub_population
ADD COLUMN label_en character varying(250);

COMMENT ON COLUMN @extschema@.c_sub_population.label_en IS 'Label of sub-population.';

ALTER TABLE @extschema@.c_sub_population
ADD COLUMN description_en text;

COMMENT ON COLUMN @extschema@.c_sub_population.description_en IS 'Description of sub-population.';

ALTER TABLE @extschema@.c_sub_population_category
ADD COLUMN label_en character varying(250);

COMMENT ON COLUMN @extschema@.c_sub_population_category.label_en IS 'Label of sub-population category.';

ALTER TABLE @extschema@.c_sub_population_category
ADD COLUMN description_en text;

COMMENT ON COLUMN @extschema@.c_sub_population_category.description_en IS 'Description of sub-population category.';

ALTER TABLE @extschema@.c_variable_type
ADD COLUMN label_en character varying(120);

COMMENT ON COLUMN @extschema@.c_variable_type.label_en IS 'Label of variable type.';

ALTER TABLE @extschema@.c_variable_type
ADD COLUMN description_en text;

COMMENT ON COLUMN @extschema@.c_variable_type.description_en IS 'Description of variable type.';


--CHANGES IN THE COLUMNS OF THE C_TARGET_VARIABLE TABLE 

ALTER TABLE @extschema@.c_target_variable
RENAME COLUMN label TO etl_join_id;

ALTER TABLE @extschema@.c_target_variable
DROP COLUMN description;



--INDICES OF FKI__T_RESULT__T_ESTIMATE_CONF AND FKI__T_TOTAL_ESTIMATE_CONF__T_VARIABLE

--Index fki__t_result__t_estimate_conf
DROP INDEX IF EXISTS @extschema@.fki__t_result__t_estimate_conf;
CREATE INDEX fki__t_result__t_estimate_conf
	ON @extschema@.t_result
	USING btree(estimate_conf);

COMMENT ON INDEX @extschema@.fki__t_result__t_estimate_conf
IS 'Index for looking up final results that belong to selected estimate configuration.';

--Index fki__t_total_estimate_conf__t_variable
DROP INDEX IF EXISTS @extschema@.fki__t_total_estimate_conf__t_variable;
CREATE INDEX fki__t_total_estimate_conf__t_variable
	ON @extschema@.t_total_estimate_conf
	USING btree(target_variable);

COMMENT ON INDEX @extschema@.fki__t_total_estimate_conf__t_variable
IS 'Index for looking up total estimate configurations that belong to selected attribute category.';

-- <view name="v_conf_overview" schema="extschema" src="views/extschema/v_conf_overview.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- drop view if exists @extschema@.v_conf_overview;
create or replace view @extschema@.v_conf_overview as (
with w_confs AS MATERIALIZED (
	select
		t_estimate_conf.id as estimate_conf,
		t_total_estimate_conf.id as total_estimate_conf,
		t_total_estimate_conf.panel_refyearset_group,
		t_aux_conf.id as aux_conf,
		t_aux_conf.panel_refyearset_group AS aux_conf_prg,
		t_total_estimate_conf_denom.id as total_estimate_conf__denom,
		t_aux_conf_denom.id as aux_conf__denom,
		case 	when 
				t_total_estimate_conf.id is not null and 
				t_aux_conf.id is null and 
				t_total_estimate_conf_denom.id is null and 
				t_aux_conf_denom.id is null 
			then '1p_total'
			when 
				t_total_estimate_conf.id is not null and 
				t_aux_conf.id is null and 
				t_total_estimate_conf_denom.id is not null and 
				t_aux_conf_denom.id is null 
			then '1p1p_ratio'
			when 
				t_total_estimate_conf.id is not null and 
				t_aux_conf.id is not null and 
				t_total_estimate_conf_denom.id is null and 
				t_aux_conf_denom.id is null 
			then '2p_total'
			when 
				t_total_estimate_conf.id is not null and 
				t_aux_conf.id is not null and 
				t_total_estimate_conf_denom.id is not null and 
				t_aux_conf_denom.id is null 
			then '2p1p_ratio'
			when 
				t_total_estimate_conf.id is not null and 
				t_aux_conf.id is null and 
				t_total_estimate_conf_denom.id is not null and 
				t_aux_conf_denom.id is not null 
			then '1p2p_ratio'
			when 
				t_total_estimate_conf.id is not null and 
				t_aux_conf.id is not null and 
				t_total_estimate_conf_denom.id is not null and 
				t_aux_conf_denom.id is not null 
			then '2p2p_ratio'
			else 'unknown'
		end as estimate_type_str,
		coalesce(t_aux_conf.sigma, t_aux_conf_denom.sigma) as sigma,
		coalesce(t_total_estimate_conf.force_synthetic, t_total_estimate_conf_denom.force_synthetic) as force_synthetic,
------------------------additional info begin-----------------------------------
		coalesce(t_aux_conf.param_area, t_aux_conf_denom.param_area) as param_area, 
		coalesce(f_a_param_area.param_area_code, f_a_param_area_denom.param_area_code) as param_area_code,
		coalesce(t_aux_conf.model, t_aux_conf_denom.model) as model, 
		coalesce(t_model.description, t_model_denom.description) as model_description,
		t_total_estimate_conf.estimation_cell, c_estimation_cell.label as estimation_cell_label, c_estimation_cell.estimation_cell_collection, --f_a_cell.geom,
		t_variable.id as variable, t_variable.target_variable, c_target_variable.etl_join_id as target_variable_label, 
		t_variable.sub_population_category, c_sub_population_category.label as sub_population_category_label,
		t_variable.area_domain_category, c_area_domain_category.label as area_domain_category_label,
		c_estimation_period.estimate_date_begin, c_estimation_period.estimate_date_end
------------------------additional info end-------------------------------------
	from @extschema@.t_estimate_conf
        inner join @extschema@.t_total_estimate_conf ON t_total_estimate_conf.id = t_estimate_conf.total_estimate_conf
	inner join @extschema@.c_estimation_period ON c_estimation_period.id = t_total_estimate_conf.estimation_period
	left join @extschema@.t_aux_conf ON t_aux_conf.id = t_total_estimate_conf.aux_conf
        left join @extschema@.t_total_estimate_conf as t_total_estimate_conf_denom ON t_total_estimate_conf_denom.id = t_estimate_conf.denominator
	left join @extschema@.t_aux_conf as t_aux_conf_denom ON t_aux_conf_denom.id = t_total_estimate_conf_denom.aux_conf
------------------------additional info begin-----------------------------------
	left join @extschema@.f_a_param_area on (t_aux_conf.param_area = f_a_param_area.gid)
	left join @extschema@.t_model on (t_aux_conf.model = t_model.id)
	left join @extschema@.f_a_param_area as f_a_param_area_denom on (t_aux_conf_denom.param_area = f_a_param_area_denom.gid)
	left join @extschema@.t_model as t_model_denom on (t_aux_conf_denom.model = t_model_denom.id)
	inner join @extschema@.c_estimation_cell on (t_total_estimate_conf.estimation_cell = c_estimation_cell.id)
	inner join @extschema@.t_variable on (t_total_estimate_conf.target_variable = t_variable.id)
	inner join @extschema@.c_target_variable on (t_variable.target_variable = c_target_variable.id)
	left join @extschema@.c_sub_population_category on (t_variable.sub_population_category = c_sub_population_category.id)
	left join @extschema@.c_area_domain_category on (t_variable.area_domain_category = c_area_domain_category.id)
------------------------additional info end-------------------------------------
	order by t_estimate_conf.id
)
, w_confs_2nd_panels AS MATERIALIZED (
	select w_confs.*,
		array_agg(t_panel_refyearset_group.panel order by t_panel_refyearset_group.panel) as panels,
		array_agg(t_panel_refyearset_group.reference_year_set order by t_panel_refyearset_group.panel) as ref_year_sets
	from w_confs
	inner join @extschema@.t_panel_refyearset_group on (t_panel_refyearset_group.panel_refyearset_group = w_confs.panel_refyearset_group)
	group by w_confs.estimate_conf, w_confs.total_estimate_conf, w_confs.panel_refyearset_group, 
			w_confs.aux_conf, w_confs.aux_conf_prg, w_confs.total_estimate_conf__denom, w_confs.aux_conf__denom,
			w_confs.estimate_type_str, w_confs.sigma, w_confs.force_synthetic,
			w_confs.param_area, w_confs.param_area_code,
			w_confs.model, w_confs.model_description,
			w_confs.estimation_cell, w_confs.estimation_cell_label, w_confs.estimation_cell_collection,
			w_confs.variable, w_confs.target_variable, w_confs.target_variable_label,
			w_confs.sub_population_category, w_confs.sub_population_category_label, w_confs.area_domain_category, w_confs.area_domain_category_label,
			w_confs.estimate_date_begin, w_confs.estimate_date_end
)
	select w_confs_2nd_panels.*,
		array_agg(t_panel_refyearset_group.panel order by t_panel_refyearset_group.panel) as aux_conf_panels
	from w_confs_2nd_panels
	left join @extschema@.t_panel_refyearset_group on (t_panel_refyearset_group.panel_refyearset_group = w_confs_2nd_panels.aux_conf_prg)
	group by w_confs_2nd_panels.estimate_conf, w_confs_2nd_panels.total_estimate_conf, w_confs_2nd_panels.panel_refyearset_group, 
			w_confs_2nd_panels.aux_conf, w_confs_2nd_panels.aux_conf_prg, w_confs_2nd_panels.total_estimate_conf__denom, w_confs_2nd_panels.aux_conf__denom,
			w_confs_2nd_panels.estimate_type_str, w_confs_2nd_panels.sigma, w_confs_2nd_panels.force_synthetic,
			w_confs_2nd_panels.param_area, w_confs_2nd_panels.param_area_code,
			w_confs_2nd_panels.model, w_confs_2nd_panels.model_description,
			w_confs_2nd_panels.estimation_cell, w_confs_2nd_panels.estimation_cell_label, w_confs_2nd_panels.estimation_cell_collection, w_confs_2nd_panels.variable, w_confs_2nd_panels.target_variable, w_confs_2nd_panels.target_variable_label,
			w_confs_2nd_panels.sub_population_category, w_confs_2nd_panels.sub_population_category_label, w_confs_2nd_panels.area_domain_category, w_confs_2nd_panels.area_domain_category_label,
			w_confs_2nd_panels.estimate_date_begin, w_confs_2nd_panels.estimate_date_end,
			w_confs_2nd_panels.panels, w_confs_2nd_panels.ref_year_sets

);
--select * from @extschema@.v_conf_overview;

GRANT SELECT ON TABLE @extschema@.v_conf_overview TO PUBLIC;

-- </view>

-- <function="fn_1p_data" schema="extschema" src="functions/extschema/fn_1p_data.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_1p_data(integer)

-- DROP FUNCTION @extschema@.fn_1p_data(integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_1p_data(
    IN conf_id integer
)
  RETURNS TABLE(
	gid bigint,
	cluster integer,
	attribute integer,
	stratum integer,
	plots_per_cluster integer,
	plcount bigint,
	cluster_is_in_cell boolean,
	ldsity_d double precision,
	ldsity_d_plus double precision,
	is_aux boolean,
	is_target boolean,
	geom geometry(MultiPoint),
	ldsity_res_d double precision,
	ldsity_res_d_plus double precision,
	pix double precision,
	sweight double precision,
	DELTA_T__G_beta double precision,
	nb_sampling_units integer, 
	sweight_strata_sum double precision, 
	lambda_d_plus double precision
) AS
$BODY$
DECLARE
	_complete_query text;
BEGIN
	--------------------------------QUERY--------------------------------
	_complete_query := '
WITH
---------------------------------------------------------
-- Data block
---------------------------------------------------------
w_ldsity_plot AS MATERIALIZED (
	select
		f_p_plot.gid,
		t_total_estimate_conf.id as conf_id,
		t_panel.stratum,
		t_cluster.id as cluster,
		t_panel_refyearset_group.reference_year_set,
		t_panel_refyearset_group.panel,
		cm_cluster2panel_mapping.sampling_weight,
		t_total_estimate_conf.target_variable as attribute,
		plots_per_cluster,
		true AS plot_is_in_cell,
		coalesce(t_target_data.value, 0) as ldsity,
		f_p_plot.geom
	from @extschema@.t_total_estimate_conf
	inner join @extschema@.c_panel_refyearset_group on t_total_estimate_conf.panel_refyearset_group = c_panel_refyearset_group.id
	inner join @extschema@.t_panel_refyearset_group on t_panel_refyearset_group.panel_refyearset_group = c_panel_refyearset_group.id
	inner join sdesign.t_panel ON t_panel.id = t_panel_refyearset_group.panel
	inner join sdesign.cm_cluster2panel_mapping ON cm_cluster2panel_mapping.panel = t_panel.id
	inner join sdesign.t_cluster ON t_cluster.id = cm_cluster2panel_mapping."cluster"
	inner join sdesign.f_p_plot ON f_p_plot."cluster" = t_cluster.id
	inner join sdesign.cm_plot2cluster_config_mapping on f_p_plot.gid = cm_plot2cluster_config_mapping.plot
	inner join sdesign.t_cluster_configuration ON (t_panel.cluster_configuration = t_cluster_configuration.id
											and cm_plot2cluster_config_mapping.cluster_configuration = t_cluster_configuration.id)
	inner join sdesign.t_stratum ON t_panel.stratum = t_stratum.id
	inner join @extschema@.cm_plot2cell_mapping ON (cm_plot2cell_mapping.estimation_cell = t_total_estimate_conf.estimation_cell 
											and cm_plot2cell_mapping.plot = f_p_plot.gid)
	left join @extschema@.t_target_data on (
		f_p_plot.gid = t_target_data.plot and
		t_panel_refyearset_group.reference_year_set = t_target_data.reference_year_set and
		t_total_estimate_conf.target_variable = t_target_data.variable and
		t_target_data.is_latest)
	where t_total_estimate_conf.id = ' || conf_id || '
)
, w_ldsity_cluster AS MATERIALIZED (
	SELECT
		w_ldsity_plot.conf_id,
		concat(w_ldsity_plot.conf_id, ROW_NUMBER() OVER ())::bigint as gid,
		w_ldsity_plot.cluster,
		w_ldsity_plot.attribute,
		w_ldsity_plot.stratum,
                w_ldsity_plot.panel,
		w_ldsity_plot.plots_per_cluster,
		w_ldsity_plot.sampling_weight,
		count(*) as plcount,
		sum(w_ldsity_plot.plot_is_in_cell::int) > 0 as cluster_is_in_cell,
		sum(w_ldsity_plot.ldsity * plot_is_in_cell::int)/w_ldsity_plot.plots_per_cluster AS ldsity_D,
		sum(w_ldsity_plot.ldsity)/w_ldsity_plot.plots_per_cluster AS ldsity_D_plus,
		st_collect(w_ldsity_plot.geom)::geometry(MultiPoint) as geom
	FROM       w_ldsity_plot
	GROUP BY conf_id, stratum, panel, cluster, sampling_weight, plots_per_cluster, attribute
	ORDER BY stratum, cluster, attribute
)
, w_strata_sum AS MATERIALIZED (
	select
		t.conf_id,
		t_panel.stratum,
		case when t_cluster_configuration.cluster_design then
			t_stratum.buffered_area_m2/10000
		else
			t_stratum.area_m2/10000
		end as lambda_d_plus,
		sum(t_panel.cluster_count) as nb_sampling_units,
		sum(t_panel.sweight_panel_sum) as sweight_strata_sum
	from sdesign.t_stratum
	inner join sdesign.t_panel on (t_panel.stratum = t_stratum.id)
	inner join sdesign.t_cluster_configuration on (t_panel.cluster_configuration = t_cluster_configuration.id)
	inner join (select distinct conf_id, panel from w_ldsity_plot) as t on (t_panel.id = t.panel)
	group by conf_id, t_panel.stratum, lambda_d_plus
)
, w_1p_data AS MATERIALIZED (
	select
		w_ldsity_cluster.gid, w_ldsity_cluster.cluster,
		w_ldsity_cluster.attribute, w_ldsity_cluster.stratum, w_ldsity_cluster.plots_per_cluster,
		w_ldsity_cluster.plcount, w_ldsity_cluster.cluster_is_in_cell, w_ldsity_cluster.ldsity_d,
		w_ldsity_cluster.ldsity_d_plus, false::boolean as is_aux, true::boolean as is_target,
		w_ldsity_cluster.geom,
		NULL::double precision as ldsity_res_D, NULL::double precision as ldsity_res_D_plus,
		NULL::double precision as pix,  w_ldsity_cluster.sampling_weight as sweight, NULL::double precision as DELTA_T__G_beta,
		w_strata_sum.nb_sampling_units::integer, w_strata_sum.sweight_strata_sum, w_strata_sum.lambda_d_plus
	from w_ldsity_cluster
	INNER JOIN w_strata_sum ON w_ldsity_cluster.stratum = w_strata_sum.stratum
)
select * from w_1p_data;
';

	--RAISE NOTICE '%', _complete_query;
	RETURN QUERY EXECUTE _complete_query;
END;
$BODY$
  LANGUAGE plpgsql STABLE
  COST 100
  ROWS 100000;

COMMENT ON FUNCTION @extschema@.fn_1p_data(integer) IS 'Function preparing data for regression estimate.';

-- </function>

DROP FUNCTION @extschema@.fn_1p_est_configuration(integer[], integer[], integer[], date, date, character varying, integer);

-- <function="fn_1p_est_configuration" schema="extschema" src="functions/extschema/configuration/fn_1p_est_configuration.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_1p_est_configuration(integer, integer, integer, varchar, integer)
--DROP FUNCTION @extschema@.fn_1p_est_configuration(integer, integer, integer, character varying, integer);
CREATE OR REPLACE FUNCTION @extschema@.fn_1p_est_configuration(
		_panel_refyearset_group integer, 
		_estimation_cell integer, _estimation_period integer, 
		_note varchar, _variable integer)
RETURNS integer
AS
$function$
DECLARE
_total_estimate_conf		integer;
_stratas_wp			integer[];
_target_label			varchar;
_cell				varchar;
_change_variable		boolean;
BEGIN

	_target_label := (
			SELECT		replace(
						replace(
							concat(coalesce(t2.etl_join_id,'x'), ',', coalesce(t3.label,'x'), ',', coalesce(t4.label,'x'), ',', coalesce(t5.label,'x')),
						'x,',''),
					',x','') AS label
			FROM		@extschema@.t_variable AS t1
			LEFT JOIN 	@extschema@.c_target_variable AS t2 ON t1.target_variable = t2.id
			LEFT JOIN	@extschema@.c_sub_population_category AS t3 ON t1.sub_population_category = t3.id
			LEFT JOIN	@extschema@.c_area_domain_category AS t4 ON t1.area_domain_category = t4.id
			LEFT JOIN	@extschema@.c_auxiliary_variable_category AS t5 ON t1.auxiliary_variable_category = t5.id
			WHERE
				t1.id = _variable
			);


	_change_variable := (
			SELECT		CASE WHEN t3.id = 2 THEN true ELSE false END
			FROM 		@extschema@.t_variable AS t1
			LEFT JOIN 	@extschema@.c_target_variable AS t2 ON t1.target_variable = t2.id
			LEFT JOIN	@extschema@.c_state_or_change AS t3 ON t2.state_or_change = t3.id
			WHERE t1.id = _variable
			);
		

	_cell := (SELECT label FROM @extschema@.c_estimation_cell WHERE id = _estimation_cell);

	-- reset sequence (usually when previous atempt to configure fails, the sequence remains shifted)
	PERFORM setval('@extschema@.t_total_estimate_conf_id_seq', (SELECT coalesce(max(id),0)+1 FROM @extschema@.t_total_estimate_conf), FALSE);

	-- insert into table t_total_estimate_conf
	INSERT INTO @extschema@.t_total_estimate_conf (estimation_cell, estimation_period, total_estimate_conf, 
							target_variable, phase_estimate_type, aux_conf, panel_refyearset_group)
	VALUES
		(_estimation_cell, _estimation_period, concat('1p;T=',_target_label,';Cell=',_cell,_note), _variable, 1, NULL, _panel_refyearset_group)
	RETURNING id
	INTO _total_estimate_conf;

	-- insert into table t_estimate_conf
	INSERT INTO @extschema@.t_estimate_conf (estimate_type, total_estimate_conf, denominator)
	SELECT 1, _total_estimate_conf, NULL;

	RETURN _total_estimate_conf;
END;
$function$
LANGUAGE plpgsql
PARALLEL SAFE;

COMMENT ON FUNCTION @extschema@.fn_1p_est_configuration(integer,integer,integer,varchar,integer) IS 'Function makes the estimate configuration present in the database - provides inserts into the all necessary tables.';

-- </function>

DROP FUNCTION @extschema@.fn_est_configuration(integer,date,date,character varying,integer,integer[]);

-- <function="fn_est_configuration" schema="extschema" src="functions/extschema/configuration/fn_est_configuration.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_est_configuration(integer, regclass)
--DROP FUNCTION @extschema@.fn_est_configuration(integer,integer,character varying,integer,integer[]);
CREATE OR REPLACE FUNCTION @extschema@.fn_est_configuration(_estimation_cell integer, _estimation_period integer, _note varchar, _target_variable integer, _panels integer[] DEFAULT NULL)
RETURNS setof integer
AS
$function$
DECLARE
_total_estimate_conf		integer;
_estimate_date_begin		date;
_estimate_date_end		date;
_stratas			integer[];
_panels_used			integer[];
_refyearsets			integer[];
_estimation_cells		integer[];
BEGIN

_estimation_cells := array[_estimation_cell];
-- panels order
IF _panels IS NOT NULL THEN _panels := (SELECT array_agg(panel ORDER BY panel) FROM unnest(_panels) AS t(panel));
END IF;

SELECT estimate_date_begin, estimate_date_end
FROM @extschema@.c_estimation_period
WHERE id = _estimation_period
INTO _estimate_date_begin, _estimate_date_end;

IF _estimate_date_begin IS NULL OR _estimate_date_end IS NULL
THEN
	RAISE EXCEPTION 'At leats one of the estimate period dates (%, %) is NULL!', _estimate_date_begin, _estimate_date_end;
END IF;

	-- panels with target variable in specified stratas (panels are the ones with the less granularity, hence 1 stratum can have e.g. 4 panels which together results in 1 big panel)
	WITH w_data AS MATERIALIZED (
		SELECT
			stratum, panel, reference_year_set, reference_year_set_fit, total, is_max
		FROM
			@extschema@.fn_get_panels_in_estimation_cells(_estimation_cells, _estimate_date_begin, _estimate_date_end, _target_variable)
		)
	SELECT
		array_agg(DISTINCT stratum ORDER BY stratum) AS stratas,
		array_agg(panel ORDER BY panel) AS panels,
		array_agg(reference_year_set ORDER BY panel) AS refyearsets
	FROM
		w_data
	WHERE
		-- pick up the most dense panel with target variable
		CASE WHEN _panels IS NOT NULL THEN ARRAY[panel] <@ _panels ELSE
		is_max = true
		END
	INTO _stratas, _panels_used, _refyearsets;

	-- simple check if panels found are the same as panels required
	IF _panels IS NOT NULL AND (_panels != _panels_used OR _panels_used IS NULL)
	THEN
		RAISE EXCEPTION 'Required panels does not meet the computation criteria (measured target variable for given estimation period)!
Only these panels from specified array can be used: (%). Or You can try to not specify panels, the function will try to find the maximum of possible panels.', _panels_used;
	END IF;

RETURN QUERY
	SELECT
		@extschema@.fn_1p_est_configuration(panel_refyearset_group, estimation_cell, _estimation_period, _note, _target_variable)
	FROM
		@extschema@.fn_get_panel_refyearset_group4cells(_panels_used, _refyearsets, _estimation_cells);
END;
$function$
LANGUAGE plpgsql
PARALLEL SAFE;

COMMENT ON FUNCTION @extschema@.fn_est_configuration(integer, integer, varchar, integer, integer[]) IS 'Wrapper function. Handles data between fn_get_panels_in_estimation_cells and fn_1p_est_configuration.';

-- </function>

DROP FUNCTION @extschema@.fn_2p_est_configuration(integer,date,date,character varying,integer,integer,boolean);

-- <function="fn_2p_est_configuration" schema="extschema" src="functions/extschema/configuration/fn_2p_est_configuration.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_2p_est_configuration(integer, regclass)
--DROP FUNCTION @extschema@.fn_2p_est_configuration(integer,integer,character varying,integer,integer,boolean);
CREATE OR REPLACE FUNCTION @extschema@.fn_2p_est_configuration(_estimation_cell integer, _estimation_period integer, _note varchar, _target_variable integer, _aux_conf integer, _force_synthetic boolean default False)
RETURNS integer
AS
$function$
DECLARE
_total_estimate_conf		integer;
_estimate_date_begin		date;
_estimate_date_end		date;
_stratas			integer[];
_stratas_wp			integer[];
_panels				integer[];
_refyearsets			integer[];
_panels_aux			integer[];
_param_area			integer;
_param_area_code		varchar;
_target_label			varchar;
_model				integer;
_cell				varchar;
_panel_refyearset_group		integer;
BEGIN

-- test for existing g_betas
-- otherwise the configuration cannot be done (sometimes the g_betas cannot be computed)
-- so this prevents to configure non-computable estimates

IF (SELECT count(*) FROM @extschema@.t_g_beta WHERE aux_conf = $5) = 0
THEN
	RAISE EXCEPTION 'G-betas for required aux_conf (%) are not available (cell=%, period=%). The computation of it was not run or is not able to compute (mostly the problem of matrix inversion).', _aux_conf, _estimation_cell, _estimation_period;
END IF;

SELECT estimate_date_begin, estimate_date_end
FROM @extschema@.c_estimation_period
WHERE id = _estimation_period
INTO _estimate_date_begin, _estimate_date_end;

IF _estimate_date_begin IS NULL OR _estimate_date_end IS NULL
THEN
	RAISE EXCEPTION 'At leats one of the estimate period dates (%, %) is NULL!', _estimate_date_begin, _estimate_date_end;
END IF;


-- create the label of estimate
_target_label := (
		SELECT		replace(
					replace(
						concat(coalesce(t2.etl_join_id,'x'), ',', coalesce(t3.label,'x'), ',', coalesce(t4.label,'x'), ',', coalesce(t5.label,'x')),
					'x,',''),
			       	',x','') AS label
		FROM		@extschema@.t_variable AS t1
		LEFT JOIN 	@extschema@.c_target_variable AS t2 ON t1.target_variable = t2.id
		LEFT JOIN	@extschema@.c_sub_population_category AS t3 ON t1.sub_population_category = t3.id
		LEFT JOIN	@extschema@.c_area_domain_category AS t4 ON t1.area_domain_category = t4.id
		LEFT JOIN	@extschema@.c_auxiliary_variable_category AS t5 ON t1.auxiliary_variable_category = t5.id
		WHERE
			t1.id = _target_variable
		);

_param_area := (SELECT param_area FROM @extschema@.t_aux_conf WHERE id = $5);
_model := (SELECT model FROM @extschema@.t_aux_conf WHERE id = $5);
_param_area_code := (SELECT param_area_code FROM @extschema@.f_a_param_area WHERE gid = _param_area);
_cell := (SELECT label FROM @extschema@.c_estimation_cell WHERE id = $1);

-- reset sequence (usually when previous atempt to configure fails, the sequence remains shifted)
PERFORM setval('@extschema@.t_total_estimate_conf_id_seq', (SELECT coalesce(max(id),0)+1 FROM @extschema@.t_total_estimate_conf), FALSE);

	-- test on param_area_coverage
		SELECT
			array_agg(t1.id ORDER BY t1.id)
		FROM
			sdesign.t_stratum AS t1
		INNER JOIN
			@extschema@.f_a_param_area AS t2
		ON
			-- buffered stratum?
			-- no, if only buffer of the stratum would intersect the cell, 
			-- no consequences on estimate would be introduced, the buffer is in general used only for the edge effect compensation (the compensated area is within non-buffered stratum)
			ST_Intersects(t1.geom, t2.geom) AND NOT ST_Touches(t1.geom, t2.geom)
		WHERE
			t2.gid = _param_area
		INTO _stratas;

		IF _stratas IS NULL
		THEN
			RAISE EXCEPTION 'The specified cell is not intersected by any stratum. Choose another estimation cell.';
		END IF;

	-- existing panels configured in panel2aux_conf
		SELECT
			array_agg(t1.panel ORDER BY t1.panel)
		FROM
			@extschema@.t_panel_refyearset_group AS t1
		INNER JOIN
			@extschema@.t_aux_conf AS t2
		ON	t1.panel_refyearset_group = t2.panel_refyearset_group
		WHERE
			t2.id = _aux_conf
		INTO _panels_aux;

	-- check of panel2total_2ndph
	-- and addition of panels from param_area - is the target variable available not only in cell?

		WITH w_data AS MATERIALIZED (
			SELECT
				t1.id AS stratum, t2.id AS panel, t9.id AS reference_year_set, t2.plot_count AS total
			FROM
				sdesign.t_stratum AS t1
			INNER JOIN
				sdesign.t_panel AS t2
			ON
				t1.id = t2.stratum
			INNER JOIN
				sdesign.cm_refyearset2panel_mapping AS t8
			ON
				t2.id = t8.panel --AND
				--t8.id = t9.reference_year_set
			INNER JOIN
				sdesign.t_reference_year_set AS t9
			ON
				t8.reference_year_set = t9.id
			INNER JOIN
				@extschema@.t_available_datasets AS t6
			ON
				t2.id = t6.panel AND
				t9.id = t6.reference_year_set
			INNER JOIN
				@extschema@.t_variable AS t7
			ON
				t6.variable = t7.id
			WHERE
				array[t1.id] <@ _stratas AND
				t7.id = _target_variable AND 
				(t9.reference_date_begin >= _estimate_date_begin AND
				t9.reference_date_end <= _estimate_date_end)
			GROUP BY
				t1.id, t2.id, t9.id
		)
		SELECT
			array_agg(DISTINCT stratum ORDER BY stratum) AS stratas,
			array_agg(panel ORDER BY panel) AS panels,
			array_agg(reference_year_set ORDER BY panel) AS refyearsets
		FROM
			(SELECT
				stratum, panel, reference_year_set,
				total,
				max(total) OVER(PARTITION BY stratum, panel, reference_year_set) AS max_total
			FROM
				w_data
			) AS t1
		WHERE
			-- pick up the most dense panel with target variable
			total = max_total
		INTO _stratas_wp, _panels, _refyearsets;

		IF _panels != _panels_aux OR _panels IS NULL
		THEN
			RAISE EXCEPTION 'Not all panels coming from g_beta have available target variable! aux_conf: %, panels: %, panels_aux: %',
			_aux_conf, _panels, _panels_aux;
		END IF;

		_panel_refyearset_group := (SELECT @extschema@.fn_get_panel_refyearset_group(_panels, _refyearsets));

		IF _panel_refyearset_group IS NULL
		THEN
			_panel_refyearset_group := (SELECT @extschema@.fn_save_panel_refyearset_group(_panels, _refyearsets));
		END IF;

-- insert into table t_total_estimate_conf
INSERT INTO @extschema@.t_total_estimate_conf (estimation_cell, estimation_period, total_estimate_conf, target_variable, phase_estimate_type, force_synthetic, aux_conf, panel_refyearset_group)
VALUES
	($1, $2, concat('2p;T=',_target_label,';C=',_cell,';PA=',_param_area_code, ';m=',_model,_note), $4, 2, $6, $5, _panel_refyearset_group)
ON CONFLICT (estimation_cell, estimation_period, target_variable, phase_estimate_type, coalesce(force_synthetic,false), coalesce(aux_conf,0), panel_refyearset_group)
DO NOTHING
RETURNING id
INTO _total_estimate_conf;

IF _total_estimate_conf IS NOT NULL
THEN
	-- insert into table t_estimate_conf
	INSERT INTO @extschema@.t_estimate_conf (estimate_type, total_estimate_conf, denominator)
	SELECT 1, _total_estimate_conf, NULL;

	IF	(
			SELECT
				array_agg(t_variable.id ORDER BY t_variable.id)
			FROM @extschema@.t_aux_total
			INNER JOIN @extschema@.t_variable 		ON (t_aux_total.variable = t_variable.id)
			INNER JOIN @extschema@.c_estimation_cell 	ON (t_aux_total.estimation_cell = c_estimation_cell.id)
			INNER JOIN @extschema@.t_model_variables 	ON t_model_variables.variable = t_variable.id
			INNER JOIN @extschema@.t_model 			ON t_model.id = t_model_variables.model
			INNER JOIN @extschema@.t_aux_conf 		ON t_aux_conf.model = t_model_variables.model
			WHERE 	c_estimation_cell.id = $1 AND
				t_aux_conf.id = $5 AND
				t_aux_total.is_latest

		)
		!= (
			SELECT
				array_agg(t_model_variables.variable order by variable)
			FROM @extschema@.t_aux_conf
			INNER JOIN @extschema@.t_model ON t_model.id = t_aux_conf.model
			INNER JOIN @extschema@.t_model_variables ON t_model_variables.model = t_model.id
			WHERE t_aux_conf.id = $5
		)
	THEN
		RAISE EXCEPTION 'fn_2p_est_configuration: t_aux_total not found! (total_estimate_conf: %)', _total_estimate_conf;
	END IF;
ELSE
	RAISE NOTICE 'Required configuration already exists!';
END IF;


RETURN _total_estimate_conf;


END;
$function$
LANGUAGE plpgsql
PARALLEL SAFE;

--COMMENT ON FUNCTION @extschema@.fn_2p_est_configuration() IS '.';

-- </function>

-- <function="fn_get_panels_in_estimation_cells" schema="extschema" src="functions/extschema/configuration/fn_get_panels_in_estimation_cells.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_get_panels_in_estimation_cells(integer, regclass)
--DROP FUNCTION @extschema@.fn_get_panels_in_estimation_cells(integer,date,date,character varying,integer,integer);
CREATE OR REPLACE FUNCTION @extschema@.fn_get_panels_in_estimation_cells(_estimation_cells integer[], _estimate_date_begin date, _estimate_date_end date, _target_variable integer)
RETURNS TABLE (
stratum 			integer,
stratum_label 			varchar(20),
panel 				integer,
panel_label			varchar(20),
panel_subset			integer,
reference_year_set 		integer,
reference_year_set_label 	varchar(20),
reference_date_begin 		date,
reference_date_end 		date,
reference_year_set_fit 		boolean,
portion_of_panel_inside		double precision,
portion_of_period_covered	double precision,
total 				integer,
is_max				boolean
)
AS
$function$
DECLARE
_change_variable 	boolean;
_estimation_cell	integer;
_cell_area_m2		double precision;
_stratas_all		integer[];
_stratas		integer[];
_sum_stratas_m2		double precision;
_result_test		boolean;

BEGIN

_change_variable := (
		SELECT		CASE WHEN t3.id = 2 THEN true ELSE false END
		FROM 		@extschema@.t_variable AS t1
		LEFT JOIN 	@extschema@.c_target_variable AS t2 ON t1.target_variable = t2.id
		LEFT JOIN	@extschema@.c_state_or_change AS t3 ON t2.state_or_change = t3.id
		WHERE t1.id = $5
		);
	
-- panels order
--IF _panels IS NOT NULL THEN _panels := (SELECT array_agg(panel ORDER BY panel) FROM unnest(_panels) AS t(panel));
--END IF;

FOREACH _estimation_cell IN ARRAY _estimation_cells
LOOP

	-- cell data
	WITH w_cell AS (
		-- cell geometry can be divided into smaller blocks, thats why sum() 
		SELECT estimation_cell, sum(st_area(geom)) AS area_m2
		FROM @extschema@.f_a_cell
		WHERE estimation_cell = _estimation_cell
		GROUP BY estimation_cell
	)
	SELECT	area_m2
	FROM w_cell
	INTO _cell_area_m2;

	-- stratas in cell
	WITH w_stratas AS (
		SELECT t1.stratum, t1.area_m2
		FROM @extschema@.t_stratum_in_estimation_cell AS t1
		WHERE estimation_cell = _estimation_cell
	)
	SELECT	array_agg(t1.stratum), sum(t1.area_m2)
	FROM w_stratas AS t1
	INTO _stratas, _sum_stratas_m2;

	IF _sum_stratas_m2 IS NULL
	THEN
		RAISE EXCEPTION 'Estimation cell is not covered by any stratum (estimation_cell = %).', _estimation_cell;
	END IF;

	-- test on cell coverage
	-- two decimal places considered when comparing the final product of division with 1
	_result_test := (SELECT CASE WHEN round(_sum_stratas_m2::numeric/_cell_area_m2::numeric, 2) = 1 THEN true ELSE false END);
	
	IF _result_test = false
	THEN
		RAISE WARNING 'The intersection of stratas and cell (% ha) is less than area of whole estimation cell (% ha).', 
				round(_sum_stratas_m2::numeric/10000.0,1), round(_cell_area_m2::numeric/10000.0,1);
	END IF;

	_stratas_all := _stratas_all || _stratas;

END LOOP;

-- return the list of panels for given target variable
-- if the reference time period is met then it is stated in column reference_year_set_fit
RETURN QUERY
WITH w_data AS (
	SELECT
		t1.id AS stratum,
		t1.stratum AS stratum_label, 
		t2.panel, 
		t2.panel_label,
		t2.panel_subset,
		t2.reference_year_set,
		t2.reference_year_set_label,
		t2.reference_date_begin,
		t2.reference_date_end,
		t2.reference_year_set_fit,
		t2.estimate_range,
		t2.reference_range,
		t2.intersection_range,

		-- find out what portion of panel is inside the estimate range
		CASE WHEN isempty(intersection_range) = false
		THEN
			EXTRACT(DAYS FROM (upper(intersection_range) - lower(intersection_range))) / -- interval of intersection
			EXTRACT(DAYS FROM (upper(reference_range) - lower(reference_range))) -- interval of reference period

		ELSE
			0 -- 0 percent of panel inside the period
		END AS portion_of_panel_inside,

		-- find out what portion of estimation period is covered by panel
		CASE WHEN isempty(intersection_range) = false
		THEN
			EXTRACT(DAYS FROM (upper(intersection_range) - lower(intersection_range))) / -- interval of intersection
			EXTRACT(DAYS FROM (upper(estimate_range) - lower(estimate_range))) -- interval of estimation period
		ELSE 0 -- 0 percent covered by panel
		END AS portion_of_period_covered,
		t2.total
	FROM
		(SELECT t1.id, t1.stratum FROM sdesign.t_stratum AS t1 WHERE array[id] <@ _stratas_all) AS t1
	-- could be inner join, every stratum has to have at least one panel
	-- but in case of not available target variable, stratum would disappear from the list
	LEFT JOIN
		(SELECT
			t2.stratum,
			t2.id AS panel,
			t2.panel AS panel_label,
			t2.plot_count AS total,
			t2.panel_subset,
			t4.id AS reference_year_set,
			t4.reference_year_set AS reference_year_set_label,
			t4.reference_date_begin,
			t4.reference_date_end,
			-- test if reference_year_set fits the parameteres given
			CASE
			WHEN 	t4.reference_date_begin >= $2 AND
				t4.reference_date_end <= $3 
			THEN true
			ELSE false
			END AS reference_year_set_fit,

			-- range of estimation period
			tsrange(_estimate_date_begin,_estimate_date_end) AS estimate_range,
			-- range of reference period
			tsrange(t4.reference_date_begin,t4.reference_date_end) AS reference_range,

			-- intersection of dates
			tsrange(t4.reference_date_begin,t4.reference_date_end) * 
			tsrange(_estimate_date_begin,_estimate_date_end) AS intersection_range

		FROM
			sdesign.t_panel AS t2
		-- inner join, assuming every panel has SOME reference_year_set mapping
		INNER JOIN
			sdesign.cm_refyearset2panel_mapping AS t3
		ON
			t2.id = t3.panel
		-- again inner join, I am curious, what other reference year sets are possible in case the required dates does not fit (but target variable available)
		INNER JOIN
			sdesign.t_reference_year_set AS t4
		ON
			t3.reference_year_set = t4.id
		-- inner join, target variable must fit
		INNER JOIN
			@extschema@.t_available_datasets AS t5
		ON
			t2.id = t5.panel AND
			t4.id = t5.reference_year_set AND
			t5.variable = $4 
		) AS t2
	ON
		t1.id = t2.stratum

), w_max AS (
	SELECT 
		t1.stratum, t1.stratum_label, t1.panel, t1.panel_label, t1.panel_subset,
		t1.reference_year_set, t1.reference_year_set_label, t1.reference_date_begin, t1.reference_date_end, t1.reference_year_set_fit, 
		t1.portion_of_panel_inside, t1.portion_of_period_covered,
		t1.total,
		-- maximum number of plots for a group of panels with the same reference_year_set 
		-- and within belonging to the required reference period
		max(t1.total) OVER(PARTITION BY t1.stratum, t1.reference_year_set, t1.reference_year_set_fit) AS max_total
	FROM w_data AS t1
)
SELECT
	t1.stratum, t1.stratum_label, t1.panel, t1.panel_label, t1.panel_subset,
	t1.reference_year_set, t1.reference_year_set_label, t1.reference_date_begin, t1.reference_date_end, t1.reference_year_set_fit, 
	t1.portion_of_panel_inside, t1.portion_of_period_covered, t1.total,
	CASE WHEN t1.total = t1.max_total THEN true ELSE false END AS is_max
FROM
	w_max AS t1;

END;
$function$
LANGUAGE plpgsql
PARALLEL SAFE;

COMMENT ON FUNCTION @extschema@.fn_get_panels_in_estimation_cells(integer[], date, date, integer) IS 'Function returns list of stratas and panels in the estimation cell. For each panel there is an indicator (reference_year_set_fit) if the panel lies within estimation period and indicator max which tells if the panel is the one with the maximum possible plots.';

-- </function>

-- <function="fn_get_panel_refyearset_group" schema="extschema" src="functions/extschema/configuration/fn_get_panel_refyearset_group.sql">
--
-- Copyright 2017, 2021 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_get_panel_refyearset_group(integer[], integer[])
--DROP FUNCTION @extschema@.fn_get_panel_refyearset_group(integer[], integer[]);
CREATE OR REPLACE FUNCTION @extschema@.fn_get_panel_refyearset_group(
		_panels integer[], _refyearsets integer[]) 
RETURNS integer
AS
$function$
DECLARE
_panel_refyearset_group		integer;
BEGIN

IF _panels IS NULL THEN
	RAISE NOTICE 'Input array of panels is NULL!';
END IF;
-- test on the same length of each array (except estimation_cells)
IF
	array_length(_panels,1) != array_length(_refyearsets,1)
THEN
	RAISE EXCEPTION 'Input arrays of panels (%) and refyearsets (%) are not the same length.', _panels, _refyearsets;
END IF; 

	WITH w_lookup AS (
		SELECT 	t1.panel_refyearset_group,
			array_agg(panel ORDER BY panel, reference_year_set) AS panels, 
			array_agg(reference_year_set ORDER BY panel, reference_year_set) AS refyearsets
		FROM @extschema@.t_panel_refyearset_group AS t1
		GROUP BY t1.panel_refyearset_group
	)
	SELECT panel_refyearset_group
	FROM w_lookup
	WHERE panels = _panels AND refyearsets = _refyearsets
	INTO _panel_refyearset_group;

	RETURN _panel_refyearset_group;
END;
$function$
LANGUAGE plpgsql
PARALLEL SAFE;

COMMENT ON FUNCTION @extschema@.fn_get_panel_refyearset_group(integer[],integer[]) IS 'Returns identificator of panel_refyearset_group.';

-- </function>

-- <function="fn_save_panel_refyearset_group" schema="extschema" src="functions/extschema/configuration/fn_save_panel_refyearset_group.sql">
--
-- Copyright 2017, 2021 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_save_panel_refyearset_group(integer[], integer[], varchar, text)
--DROP FUNCTION @extschema@.fn_save_panel_refyearset_group(integer[], integer[], varchar, text);
CREATE OR REPLACE FUNCTION @extschema@.fn_save_panel_refyearset_group(
		_panels integer[], _refyearsets integer[], 
		_label varchar DEFAULT NULL, _description text DEFAULT NULL)
RETURNS integer
AS
$function$
DECLARE
_panel_refyearset_group		integer;
BEGIN

IF _panels IS NULL THEN
	RAISE NOTICE 'Input array of panels is NULL!';
END IF;
-- test on the same length of each array (except estimation_cells)
IF
	array_length(_panels,1) != array_length(_refyearsets,1)
THEN
	RAISE EXCEPTION 'Input arrays of panels (%) and refyearsets (%) are not the same length.', _panels, _refyearsets;
END IF; 

	SELECT @extschema@.fn_get_panel_refyearset_group (_panels, _refyearsets)
	INTO _panel_refyearset_group;

	IF _panel_refyearset_group IS NULL
	THEN
		IF _label IS NULL THEN
			_label := concat('no_of_panels: ',
					(SELECT count(id)::text FROM sdesign.t_panel
					WHERE array[id] <@ _panels),
					';',
					(SELECT concat(min(reference_date_begin)::text,'-',max(reference_date_end)::text) 
					FROM sdesign.t_reference_year_set
					WHERE array[id] <@ _refyearsets)
					);
		END IF;

		IF _description IS NULL THEN
			_description := _label;
		END IF;

		INSERT INTO @extschema@.c_panel_refyearset_group (label, description)
		SELECT _label, _description
		RETURNING id INTO _panel_refyearset_group;

		INSERT INTO @extschema@.t_panel_refyearset_group (panel_refyearset_group, panel, reference_year_set)
		SELECT
			_panel_refyearset_group, t1.panel, t2.reference_year_set
		FROM
			unnest(_panels) WITH ORDINALITY AS t1(panel,id)
		INNER JOIN
			unnest(_refyearsets) WITH ORDINALITY AS t2(reference_year_set,id)
		ON t1.id = t2.id;
	ELSE
		RAISE EXCEPTION 'Specified combination of panels and reference year sets is already present in the DB! (panel_refyear_set_group = %)', _panel_refyearset_group;
	END IF;

	RETURN _panel_refyearset_group;
END;
$function$
LANGUAGE plpgsql
PARALLEL SAFE;

COMMENT ON FUNCTION @extschema@.fn_save_panel_refyearset_group(integer[],integer[],varchar,text) IS 'Returns panel_refyearset_group identificator for newly inserted group.';

-- </function>

-- <function="fn_get_panel_refyearset_group4cells" schema="extschema" src="functions/extschema/configuration/fn_get_panel_refyearset_group4cells.sql">
--
-- Copyright 2017, 2021 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_get_panel_refyearset_group4cells(integer[], integer[], integer[], varchar, text)
--DROP FUNCTION @extschema@.fn_get_panel_refyearset_group4cells(integer[], integer[], integer[], varchar, text);
CREATE OR REPLACE FUNCTION @extschema@.fn_get_panel_refyearset_group4cells(
		_panels integer[], _refyearsets integer[], 
		_estimation_cells integer[], _label varchar DEFAULT NULL, _description text DEFAULT NULL)
RETURNS TABLE (estimation_cell integer, panel_refyearset_group integer)
AS
$function$
DECLARE
_estimation_cell		integer;
_panel_refyearset_group		integer;
_panels_ec			integer[];
_refyearsets_ec			integer[];
BEGIN

IF _panels IS NULL THEN
	RAISE NOTICE 'Input array of panels is NULL!';
END IF;
-- test on the same length of each array (except estimation_cells)
IF
	array_length(_panels,1) != array_length(_refyearsets,1)
THEN
	RAISE EXCEPTION 'Input arrays of panels (%) and refyearsets (%) are not the same length.', _panels, _refyearsets;
END IF; 

-- loop through an array of estimation cells
FOREACH _estimation_cell IN ARRAY _estimation_cells
LOOP
	-- find out which panels belongs to specified estimation_cell
	SELECT
		array_agg(t3.panel ORDER BY t2.id), array_agg(t4.refyearset ORDER BY t2.id)
	FROM
		sdesign.t_panel AS t1
	INNER JOIN
		@extschema@.t_stratum_in_estimation_cell AS t2
	ON
		t1.stratum = t2.stratum
	INNER JOIN
		unnest(_panels) WITH ORDINALITY AS t3(panel, id)
	ON t1.id = t3.panel
	INNER JOIN
		unnest(_refyearsets) WITH ORDINALITY AS t4(refyearset, id)
	ON t3.id = t4.id
	WHERE t2.estimation_cell = _estimation_cell
	INTO _panels_ec, _refyearsets_ec;

	SELECT @extschema@.fn_get_panel_refyearset_group(_panels_ec, _refyearsets_ec)
	INTO _panel_refyearset_group;
	

	IF _panel_refyearset_group IS NULL
	THEN
		SELECT @extschema@.fn_save_panel_refyearset_group(_panels_ec, _refyearsets_ec)
		INTO _panel_refyearset_group;
	END IF;

	estimation_cell := _estimation_cell;
	panel_refyearset_group := _panel_refyearset_group;

	RETURN NEXT;
END LOOP;
END;
$function$
LANGUAGE plpgsql
PARALLEL SAFE;

COMMENT ON FUNCTION @extschema@.fn_get_panel_refyearset_group4cells(integer[],integer[],integer[],varchar,text) IS 'Returns table with estimation cells and belonging panel_refyearset_group (if necessary, provides insert into lookup).';

-- </function>

-- <function="fn_G_beta" schema="extschema" src="functions/extschema/fn_g_beta.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_G_beta(integer)

-- DROP FUNCTION @extschema@.fn_G_beta(integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_G_beta(
    IN conf_id integer
)
  RETURNS TABLE(
	variable integer,
	cluster integer,
	val double precision
) AS
$BODY$
DECLARE
	_complete_query text;
BEGIN
	--------------------------------QUERY--------------------------------
	_complete_query := '
WITH
w_configuration AS MATERIALIZED (
	select 
		t_aux_conf.id, t_aux_conf.param_area, t_aux_conf.model, t_aux_conf.sigma, t_model.description,
		array_agg(t_model_variables.variable order by t_model_variables.variable) as aux_attributes,
		t_aux_conf.panel_refyearset_group
	from @extschema@.t_aux_conf 
	inner join @extschema@.t_model ON t_model.id = t_aux_conf.model
	inner join @extschema@.t_model_variables ON t_model_variables.model = t_model.id
        where t_aux_conf.id = ' || conf_id || '
	group by t_aux_conf.id, t_aux_conf.param_area, t_aux_conf.model, t_model.description
)
, w_param_area_selection AS MATERIALIZED (
	SELECT
		w_configuration.id as conf_id,
		gid as param_area_gid,
		geom,
		panel_refyearset_group
	FROM @extschema@.f_a_param_area
	INNER JOIN w_configuration ON (w_configuration.param_area = f_a_param_area.gid)
)
, w_plot AS MATERIALIZED (-------------------------LIST OF PLOTS IN PARAMETRIZATION AREA
	select distinct 
		w_param_area_selection.conf_id, f_p_plot.gid, t_cluster.id as cluster, t_panel.stratum,
		t_panel_refyearset_group.panel, 
		f_p_plot.geom
	from w_param_area_selection 
	inner join @extschema@.t_panel_refyearset_group on w_param_area_selection.panel_refyearset_group = t_panel_refyearset_group.panel_refyearset_group
	inner join sdesign.t_panel ON t_panel.id = t_panel_refyearset_group.panel
	inner join sdesign.cm_cluster2panel_mapping ON cm_cluster2panel_mapping.panel = t_panel.id
	inner join sdesign.t_cluster ON t_cluster.id = cm_cluster2panel_mapping."cluster"
	inner join sdesign.f_p_plot ON f_p_plot."cluster" = t_cluster.id
	inner join @extschema@.cm_plot2param_area_mapping ON cm_plot2param_area_mapping.plot = f_p_plot.gid
	inner join @extschema@.f_a_param_area ON (f_a_param_area.gid = cm_plot2param_area_mapping.param_area and w_param_area_selection.param_area_gid = f_a_param_area.gid)
)
, w_ldsity_plot AS MATERIALIZED (
	SELECT
		w_plot.conf_id,
		w_plot.gid,
		w_plot.stratum,
                w_plot.panel,
		w_plot.cluster,
		t_variable.id as attribute,
		t_cluster_configuration.plots_per_cluster,
		coalesce(t_auxiliary_data.value, 0) as ldsity,
		w_plot.geom,
		true as is_aux
	FROM w_plot
	inner join sdesign.cm_plot2cluster_config_mapping on w_plot.gid = cm_plot2cluster_config_mapping.plot
	inner join sdesign.t_cluster_configuration on cm_plot2cluster_config_mapping.cluster_configuration = t_cluster_configuration.id
	inner join w_configuration on w_plot.conf_id = w_configuration.id
	left join @extschema@.t_variable on t_variable.id = ANY (w_configuration.aux_attributes)
	left join @extschema@.t_auxiliary_data on (
		w_plot.gid = t_auxiliary_data.plot and
		t_variable.id = t_auxiliary_data.variable and
		t_auxiliary_data.is_latest)
)
, w_ldsity_cluster AS MATERIALIZED (
 	SELECT
		w_ldsity_plot.conf_id,
		concat(w_ldsity_plot.conf_id, ROW_NUMBER() OVER ())::bigint as gid, 
		w_ldsity_plot.cluster,
		w_ldsity_plot.attribute,
		w_ldsity_plot.stratum,
                w_ldsity_plot.panel,
		w_ldsity_plot.plots_per_cluster,
		count(*) as plcount,
		sum(w_ldsity_plot.ldsity)/w_ldsity_plot.plots_per_cluster AS ldsity_D_plus, -- eq 15,
		w_ldsity_plot.is_aux, --
		st_collect(w_ldsity_plot.geom)::geometry(MultiPoint) as geom
	FROM       w_ldsity_plot
	GROUP BY conf_id, stratum, panel, cluster, plots_per_cluster, attribute, is_aux
	ORDER BY stratum, cluster, attribute
)
, w_X AS MATERIALIZED ( -------------------------AUX LOCAL DENSITY ON TRACT LEVEL
	SELECT
		conf_id,
		attribute AS r,
		cluster AS c,
		ldsity_D_plus as val
	FROM w_ldsity_cluster
	WHERE is_aux
)
, w_clusters AS MATERIALIZED (-------------------------LIST OF TRACTS
	select distinct conf_id, stratum, panel, cluster from w_ldsity_cluster
)
, w_strata_sum AS MATERIALIZED (
	select
		w_configuration.id as conf_id,
                t_stratum.id as f_a_sampling_stratum_gid,
		case when t_cluster_configuration.cluster_design then
			t_stratum.buffered_area_m2/10000
		else
			t_stratum.area_m2/10000
		end as lambda_d_plus,
		plots_per_cluster,
		count(*) as nb_sampling_units, sum (sampling_weight) as sweight_strata_sum
	from w_configuration
	inner join @extschema@.t_aux_conf on t_aux_conf.id = w_configuration.id
	inner join @extschema@.t_panel_refyearset_group ON t_panel_refyearset_group.panel_refyearset_group = t_aux_conf.panel_refyearset_group
	inner join sdesign.t_panel ON t_panel.id = t_panel_refyearset_group.panel
	inner join sdesign.t_cluster_configuration ON t_panel.cluster_configuration = t_cluster_configuration.id
	inner join sdesign.t_stratum ON t_stratum.id = t_panel.stratum
	inner join sdesign.cm_cluster2panel_mapping ON cm_cluster2panel_mapping.panel = t_panel.id
	group by w_configuration.id, f_a_sampling_stratum_gid, lambda_d_plus, plots_per_cluster
)
, w_pix AS MATERIALIZED (-------------------------INCLUSION DENSITY PIX
	SELECT
		w_clusters.conf_id,
		w_clusters.stratum,
		w_clusters.cluster,
		v_strata_sum.sweight_strata_sum / (v_strata_sum.lambda_d_plus * cm_cluster2panel_mapping.sampling_weight) as pix
	FROM w_clusters
        INNER JOIN sdesign.cm_cluster2panel_mapping ON (w_clusters.cluster = cm_cluster2panel_mapping.cluster
								and w_clusters.panel = cm_cluster2panel_mapping.panel)
	INNER JOIN w_strata_sum as v_strata_sum ON w_clusters.stratum = v_strata_sum.f_a_sampling_stratum_gid
)
, w_SIGMA AS MATERIALIZED (
	SELECT
		conf_id,
		1 as r,
		cluster as c,
		case when w_configuration.sigma then (plots_per_cluster^2)::float / plcount::float 
		else 1::float end
		as val
	from (select conf_id, cluster, plots_per_cluster, plcount 
		from w_ldsity_cluster 
		group by conf_id, cluster, plots_per_cluster, plcount) as m
	inner join w_configuration on (m.conf_id = w_configuration.id)
	order by r, c
)
, w_PI AS MATERIALIZED (
	SELECT
		conf_id,
		1 as r,
		cluster as c,
		1.0 / pix as val
	from w_pix
	order by r, c
)
, w_SIGMA_PI AS MATERIALIZED (
	SELECT
		A.conf_id,
		1 as r,
		A.c as c,
		A.val * B.val as val
	from w_SIGMA as A inner join w_PI as B on (A.c = B.c and A.conf_id = B.conf_id) 
	order by r, c
)
, w_XT AS MATERIALIZED (
	SELECT
		conf_id,
		c as r,
		r as c,
		val as val
	from w_X
	order by r, c
)
, w_X_SIGMA_PI AS MATERIALIZED ( -- element-wise multiplication
	select 
		A.conf_id,
		A.r, 
		A.c,
		A.val * B.val as val
	from w_X as A
	inner join w_SIGMA_PI as B on (A.c = B.c and A.conf_id = B.conf_id)
	ORDER BY r, c
)
, w_X_SIGMA_PI_XT AS MATERIALIZED ( -- matrix multiplication
	SELECT 
		A.conf_id,
		ROW_NUMBER() OVER (partition by A.conf_id order by A.r, B.c) AS mid,
		A.r, 
		B.c, 
		sum(A.val * B.val) as val
	FROM w_X_SIGMA_PI as A, w_XT as B
	WHERE A.c = B.r and A.conf_id = B.conf_id
	GROUP BY A.r, B.c, A.conf_id
	ORDER BY r, c
)
, w_agg AS MATERIALIZED (
	select conf_id, r, array_agg(val order by c) as val from w_X_SIGMA_PI_XT group by conf_id, r
)
, w_aggagg AS MATERIALIZED (
	select conf_id, array_agg(val order by r) as val from w_agg group by conf_id
)
, w_inv AS MATERIALIZED (
	select conf_id, @extschema@.fn_inverse(val) AS val from w_aggagg
)
, w_inv_id AS MATERIALIZED (
	select conf_id, mid, invval from w_inv, unnest(w_inv.val) WITH ORDINALITY AS t(invval, mid)
)
, w_X_SIGMA_PI_XT_inv AS MATERIALIZED (
	SELECT 
		conf_id, r, c, invval as val 
	FROM w_X_SIGMA_PI_XT 
	inner join w_inv_id using (conf_id, mid)
	ORDER BY r, c
)
, w_X_SIGMA_PI_XT_inv_X AS MATERIALIZED (
	select
		A.conf_id,
		A.r,
		B.c,
		sum(A.val * B.val) as val
	FROM
		w_X_SIGMA_PI_XT_inv as A, w_X as B
	WHERE A.c = B.r and A.conf_id = B.conf_id
	GROUP BY A.r, B.c, A.conf_id
	ORDER BY r, c
)
, w_G_beta AS MATERIALIZED (
	select
		A.conf_id,
		A.r,
		A.c,
		A.val * B.val as val
	FROM
		w_X_SIGMA_PI_XT_inv_X as A, w_SIGMA as B
	WHERE A.c = B.c and A.conf_id = B.conf_id
	ORDER BY r, c
)
select r as variable, c as cluster, val from w_G_beta;
        ';

	--RAISE NOTICE '%', _complete_query;
	RETURN QUERY EXECUTE _complete_query;
END;
$BODY$
  LANGUAGE plpgsql STABLE
  COST 100
  ROWS 100000;
;

COMMENT ON FUNCTION @extschema@.fn_G_beta(integer) IS 'Function computing matrix G_beta used for regression estimators. G_beta is dependent on model and parametrization domain (not cell). Matrix is represented by variable (row) and cluster (column) indices.';

-- </function>

-- <function="fn_2p_data" schema="extschema" src="functions/extschema/fn_2p_data.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_2p_data(integer)

-- DROP FUNCTION @extschema@.fn_2p_data(integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_2p_data(
    IN conf_id integer
)
  RETURNS TABLE(
	gid bigint,
	cluster integer,
	attribute integer,
	stratum integer,
	plots_per_cluster integer,
	plcount bigint,
	cluster_is_in_cell boolean,
	ldsity_d double precision,
	ldsity_d_plus double precision,
	is_aux boolean,
	is_target boolean,
	geom geometry(MultiPoint),
	ldsity_res_d double precision,
	ldsity_res_d_plus double precision,
	pix double precision,
	sweight double precision,
	DELTA_T__G_beta double precision,
	nb_sampling_units integer,
	sweight_strata_sum double precision,
	lambda_d_plus double precision,
	sigma boolean
) AS
$$
begin
--------------------------------QUERY--------------------------------
---------------------------------------------------------
-- Configuration block
---------------------------------------------------------
execute '
create temporary table w_configuration ON COMMIT DROP AS (
	with w_a AS NOT MATERIALIZED (
		select
			t_total_estimate_conf.estimation_cell, t_aux_conf.model, 
			t_aux_conf.sigma, t_total_estimate_conf.force_synthetic, t_aux_conf.param_area,
			array_agg(t_total_estimate_conf.id order by t_total_estimate_conf.target_variable) as t_total_estimate_conf__id,
			array_agg(t_total_estimate_conf.panel_refyearset_group order by t_total_estimate_conf.target_variable) as t_total_estimate_conf__panel_refyearset_group,
			array_agg(t_aux_conf.id order by t_total_estimate_conf.target_variable) as t_aux_conf__id,
			array_agg(t_total_estimate_conf.target_variable order by t_total_estimate_conf.target_variable) as target_attributes,
			t_total_estimate_conf.aux_conf
		from @extschema@.t_total_estimate_conf
		inner join @extschema@.t_aux_conf ON t_aux_conf.id = t_total_estimate_conf.aux_conf
		where t_total_estimate_conf.id = $1
		group by t_total_estimate_conf.estimation_cell, t_aux_conf.model, t_aux_conf.sigma, t_total_estimate_conf.force_synthetic,
			t_aux_conf.param_area, t_total_estimate_conf.aux_conf
	)
	select
		t_total_estimate_conf__id[1] as id,
		t_total_estimate_conf__panel_refyearset_group[1] AS panel_refyearset_group,
		t_total_estimate_conf__panel_refyearset_group,
		w_a.t_total_estimate_conf__id, w_a.t_aux_conf__id,
		w_a.param_area, w_a.estimation_cell as cell, w_a.model, w_a.sigma, w_a.force_synthetic,
		array_agg(t_model_variables.variable order by t_model_variables.variable) as aux_attributes, w_a.target_attributes
	from w_a
	inner join @extschema@.t_aux_conf ON t_aux_conf.id = w_a.aux_conf
	inner join @extschema@.t_model ON t_model.id = t_aux_conf.model
	inner join @extschema@.t_model_variables ON t_model_variables.model = t_model.id
	group by w_a.estimation_cell, w_a.model, w_a.sigma, w_a.force_synthetic, w_a.param_area, w_a.target_attributes, 
		w_a.t_total_estimate_conf__id, w_a.t_total_estimate_conf__panel_refyearset_group, w_a.t_aux_conf__id
	order by id limit 1
);' using fn_2p_data.conf_id;
analyze w_configuration;

execute '
create temporary table w_cell_selection ON COMMIT DROP AS (
	SELECT
		w_configuration.id as conf_id,
		estimation_cell as estimation_cell,
		geom
	FROM @extschema@.f_a_cell
	INNER JOIN w_configuration ON (w_configuration.cell = f_a_cell.estimation_cell)
);';
analyze w_cell_selection;

execute '
create temporary table w_plot ON COMMIT DROP as (
WITH
w_param_area_selection AS NOT MATERIALIZED (
	SELECT
		w_configuration.id as conf_id,
		gid as param_area_gid,
		geom
	FROM @extschema@.f_a_param_area
	INNER JOIN w_configuration ON (w_configuration.param_area = f_a_param_area.gid)
)
---------------------------------------------------------
-- Data block
---------------------------------------------------------
, w_plot AS NOT MATERIALIZED (
	select distinct
		f_p_plot.gid, w_configuration.id as conf_id, t_cluster.id as cluster, t_panel.stratum,
		case when w_configuration.force_synthetic = True then False else cm_plot2cell_mapping.id IS NOT NULL end AS plot_is_in_cell, 
		-- t_panel2aux_conf.reference_year_set as reference_year_set, t_panel2aux_conf.panel as panel,
		t_panel_refyearset_group.reference_year_set as reference_year_set, t_panel_refyearset_group.panel as panel,
		f_p_plot.geom
	from w_configuration
        inner join @extschema@.t_panel_refyearset_group on w_configuration.panel_refyearset_group = t_panel_refyearset_group.panel_refyearset_group
	--inner join @extschema@.t_panel2aux_conf on w_configuration.t_aux_conf__id[1] = t_panel2aux_conf.aux_conf
	inner join sdesign.t_panel ON (t_panel.id = t_panel_refyearset_group.panel)
	inner join sdesign.cm_cluster2panel_mapping ON cm_cluster2panel_mapping.panel = t_panel.id
	inner join sdesign.t_cluster ON t_cluster.id = cm_cluster2panel_mapping."cluster"
	inner join sdesign.f_p_plot ON f_p_plot."cluster" = t_cluster.id
	left join @extschema@.cm_plot2cell_mapping ON (cm_plot2cell_mapping.estimation_cell = w_configuration.cell and cm_plot2cell_mapping.plot = f_p_plot.gid)
	inner join @extschema@.cm_plot2param_area_mapping ON (cm_plot2param_area_mapping.param_area = w_configuration.param_area and cm_plot2param_area_mapping.plot = f_p_plot.gid)
)
select * from w_plot
);';
analyze w_plot;

execute '
create temporary table w_ldsity_plot ON COMMIT DROP as (
	with w_plot AS NOT MATERIALIZED (select * from w_plot)
		select
			w_plot.gid,
			w_plot.conf_id,
			w_plot.stratum,
			w_plot.panel,
			w_plot.cluster,
			t_variable.id as attribute,
			t_cluster_configuration.plots_per_cluster,
			w_plot.plot_is_in_cell,
			coalesce(t_auxiliary_data.value, 0) as ldsity,
			true as is_aux, false as is_target,
			w_plot.geom
		from w_plot as w_plot
		inner join sdesign.cm_plot2cluster_config_mapping on w_plot.gid = cm_plot2cluster_config_mapping.plot
		inner join sdesign.t_cluster_configuration on cm_plot2cluster_config_mapping.cluster_configuration = t_cluster_configuration.id
		inner join w_configuration on w_plot.conf_id = w_configuration.id
		inner join @extschema@.t_variable on t_variable.id = ANY (w_configuration.aux_attributes)
		left join @extschema@.t_auxiliary_data on (
			w_plot.gid = t_auxiliary_data.plot and
			t_variable.id = t_auxiliary_data.variable and
			t_auxiliary_data.is_latest)
	union all
		select
			w_plot.gid,
			w_plot.conf_id,
			w_plot.stratum,
			w_plot.panel,
			w_plot.cluster,
			t_variable.id as attribute,
			t_cluster_configuration.plots_per_cluster,
			w_plot.plot_is_in_cell,
			coalesce(t_target_data.value, 0) as ldsity,
			false as is_aux, true as is_target,
			w_plot.geom
		from w_plot as w_plot
		inner join sdesign.cm_plot2cluster_config_mapping on w_plot.gid = cm_plot2cluster_config_mapping.plot
		inner join sdesign.t_cluster_configuration on cm_plot2cluster_config_mapping.cluster_configuration = t_cluster_configuration.id
		inner join w_configuration on w_plot.conf_id = w_configuration.id
                inner join @extschema@.t_variable on (t_variable.id = ANY (w_configuration.target_attributes))
		left join @extschema@.t_target_data on (
			w_plot.gid = t_target_data.plot and
			w_plot.reference_year_set = t_target_data.reference_year_set and
			t_variable.id = t_target_data.variable and
			t_target_data.is_latest)
);';
analyze w_ldsity_plot;

execute '
create temporary table w_ldsity_cluster ON COMMIT DROP as (
	SELECT
		w_ldsity_plot.conf_id,
		concat(w_ldsity_plot.conf_id, ROW_NUMBER() OVER ())::bigint as gid,
		w_ldsity_plot.cluster,
		w_ldsity_plot.attribute,
		w_ldsity_plot.stratum,
		w_ldsity_plot.panel,
		w_ldsity_plot.plots_per_cluster,
		count(*) as plcount,
		sum(w_ldsity_plot.plot_is_in_cell::int) > 0 as cluster_is_in_cell,
		sum(w_ldsity_plot.ldsity * plot_is_in_cell::int)/w_ldsity_plot.plots_per_cluster AS ldsity_D,
		sum(w_ldsity_plot.ldsity)/w_ldsity_plot.plots_per_cluster AS ldsity_D_plus,
		w_ldsity_plot.is_aux, w_ldsity_plot.is_target,
		st_collect(w_ldsity_plot.geom)::geometry(MultiPoint) as geom
	FROM       w_ldsity_plot
	GROUP BY conf_id, stratum, panel, cluster, plots_per_cluster, attribute, is_aux, is_target
	ORDER BY stratum, cluster, attribute
);';
analyze w_ldsity_cluster;

execute '
create temporary table w_clusters ON COMMIT DROP as (
	select distinct conf_id, stratum, panel, cluster, cluster_is_in_cell from w_ldsity_cluster
);';
analyze w_clusters;

execute '
create temporary table w_strata_sum ON COMMIT DROP as (
	select
		w_configuration.id as conf_id,
                t_stratum.id as f_a_sampling_stratum_gid,
		case when t_cluster_configuration.cluster_design then
			t_stratum.buffered_area_m2/10000
		else
			t_stratum.area_m2/10000
		end as lambda_d_plus,
		plots_per_cluster,
		count(*) as nb_sampling_units, sum (sampling_weight) as sweight_strata_sum
	from w_configuration
	inner join @extschema@.t_aux_conf on t_aux_conf.id = ANY (w_configuration.t_aux_conf__id)
	inner join @extschema@.t_panel_refyearset_group ON t_panel_refyearset_group.panel_refyearset_group = t_aux_conf.panel_refyearset_group
	inner join sdesign.t_panel ON t_panel.id = t_panel_refyearset_group.panel
	inner join sdesign.t_cluster_configuration ON t_panel.cluster_configuration = t_cluster_configuration.id
	inner join sdesign.t_stratum ON t_stratum.id = t_panel.stratum
	inner join sdesign.cm_cluster2panel_mapping ON cm_cluster2panel_mapping.panel = t_panel.id
	group by w_configuration.id, f_a_sampling_stratum_gid, lambda_d_plus, plots_per_cluster
);';
analyze w_strata_sum;
---------------------------------------------------------
-- Matrix block
---------------------------------------------------------
execute '
create temporary table w_X ON COMMIT DROP AS (
	SELECT
		conf_id,
		attribute AS r,
		cluster AS c,
		ldsity_D as val_D,
		ldsity_D_plus as val
	FROM w_ldsity_cluster
	WHERE is_aux
);';
analyze w_X;

execute '
create temporary table w_Y ON COMMIT DROP AS (
	SELECT
		conf_id,
		attribute AS r,
		cluster AS c,
		ldsity_D as val_D,
		ldsity_D_plus as val
	FROM w_ldsity_cluster
	WHERE is_target
);';
analyze w_Y;

execute '
create temporary table w_pix ON COMMIT DROP as (
with
w_pix AS NOT MATERIALIZED (-------------------------INCLUSION DENSITY PIX
	SELECT
		w_clusters.conf_id,
		w_clusters.stratum,
		w_clusters.cluster,
		w_strata_sum.sweight_strata_sum / (w_strata_sum.lambda_d_plus * cm_cluster2panel_mapping.sampling_weight) as pix,
		cm_cluster2panel_mapping.sampling_weight as sweight
	FROM w_clusters
	INNER JOIN sdesign.cm_cluster2panel_mapping ON (w_clusters.cluster = cm_cluster2panel_mapping.cluster 
								and w_clusters.panel = cm_cluster2panel_mapping.panel)
	INNER JOIN w_strata_sum as w_strata_sum ON w_clusters.stratum = w_strata_sum.f_a_sampling_stratum_gid
)
select * from w_pix
);';
analyze w_pix;

execute '
create temporary table w_t_G_beta ON COMMIT DROP as (
        select
                w_configuration.id as conf_id,
                t_g_beta.variable as r, t_g_beta.cluster as c, t_g_beta.val
        from @extschema@.t_g_beta
        inner join w_configuration on (t_g_beta.aux_conf = w_configuration.t_aux_conf__id[1])
	where t_g_beta.is_latest
);';
analyze w_t_G_beta;

execute '
create temporary table w_PI ON COMMIT DROP as (
	SELECT
		conf_id,
		1 as r,
		cluster as c,
		1.0 / pix as val
	from w_pix
	order by r, c
);';
analyze w_PI;

execute '
create temporary table w_DELTA_G_beta ON COMMIT DROP as (
with w_I AS NOT MATERIALIZED (
	SELECT 
		conf_id,
		1 AS r,
		cluster AS c,
		cluster_is_in_cell::int AS val
	FROM	w_clusters
	order by r, c
)
, w_SIGMA AS NOT MATERIALIZED (
	SELECT
		conf_id,
		1 as r,
		cluster as c,
		case when w_configuration.sigma then (plots_per_cluster^2)::float / plcount::float 
		else 1::float end
		as val
	from (select conf_id, cluster, plots_per_cluster, plcount 
		from w_ldsity_cluster 
		group by conf_id, cluster, plots_per_cluster, plcount) as m
	inner join w_configuration on (m.conf_id = w_configuration.id)
	order by r, c
)
, w_SIGMA_PI AS NOT MATERIALIZED (
	SELECT
		A.conf_id,
		1 as r,
		A.c as c,
		A.val * B.val as val
	from w_SIGMA as A 
	inner join w_PI as B on (A.c = B.c and A.conf_id = B.conf_id) 
	order by r, c
)
, w_t_hat AS NOT MATERIALIZED (  -- eq. 18
	select 
		A.conf_id,
		A.r,
		1 AS c,
		sum(A.val_D * B.val * C.val) as val
	from w_X 		AS A 
	inner join w_PI     	AS B 	on (A.c = B.c and A.conf_id = B.conf_id)
	inner join w_I		AS C 	on (A.c = C.c and A.conf_id = C.conf_id)
	group by A.r, A.conf_id
	order by A.r
)
, w_t AS NOT MATERIALIZED (
	with w_aux_total as (
		select estimation_cell as cell, t_variable.id as attribute, aux_total
		from @extschema@.t_aux_total
		inner join @extschema@.t_variable on (t_aux_total.variable = t_variable.id)
		where t_aux_total.is_latest
	)
	, w_aux_ldsity as (
		select distinct conf_id, r as attribute from w_X
	)
	select
		w_cell_selection.conf_id,
		w_aux_total.attribute as r,
		1 as c,
		coalesce(w_aux_total.aux_total,
			@extschema@.fn_raise_notice(
				format(''fn_2p_data.w_t: t_aux_total not found! (conf_id:%s attribute:%s)'',
					w_cell_selection.conf_id,
					w_aux_ldsity.attribute),
				''exception''
			)::int::float
		) as val
	from
	w_aux_ldsity
	inner join w_cell_selection on (w_aux_ldsity.conf_id = w_cell_selection.conf_id)
	left join w_aux_total on (w_aux_ldsity.attribute = w_aux_total.attribute and w_cell_selection.estimation_cell = w_aux_total.cell)
)
, w_DELTA_T AS NOT MATERIALIZED (
	select
		conf_id,
		c as r, 
		r as c, 
		w_t.val - w_t_hat.val as val 
	from w_t 
	inner join w_t_hat using (r, c, conf_id)
)
, w_DELTA_G_beta AS NOT MATERIALIZED (
	select
		A.conf_id,
		A.r,
		B.c, 
		sum(A.val * B.val) as val
	FROM 	w_DELTA_T as A
	inner join w_t_G_beta as B ON (A.c = B.r and A.conf_id = B.conf_id)
	GROUP BY A.r, B.c, A.conf_id
	order by r, c
)
select * from w_DELTA_G_beta
);';
analyze w_DELTA_G_beta;

execute '
create temporary table w_X_beta ON COMMIT DROP as (
with w_Y_T AS NOT MATERIALIZED (
	SELECT
		conf_id,
		c as r,
		r as c,
		val,
		val_D -- used for 1p estimete
	from w_Y
	order by r, c
)
, w_G_PI AS NOT MATERIALIZED (
	select 
		A.conf_id,
		A.r, 
		A.c AS c,
		A.val * B.val as val
	from w_t_G_beta AS A, w_PI AS B 
	where A.c = B.c and A.conf_id = B.conf_id
	order by r, c
)
, w_beta_hat AS NOT MATERIALIZED (
	select
		A.conf_id,
		A.r,
		B.c, 
		sum(A.val * B.val) as val
	FROM w_G_PI as A, w_Y_T as B
	WHERE A.c = B.r and A.conf_id = B.conf_id
	GROUP BY A.r, B.c, A.conf_id
	order by r, c
)
, w_X_plots_T AS NOT MATERIALIZED (
	SELECT conf_id, gid AS r, cluster, attribute AS c, plots_per_cluster, ldsity AS val 
	FROM w_ldsity_plot WHERE is_aux
)
, w_X_beta AS NOT MATERIALIZED (
	select
		A.conf_id,
		A.r,
		B.c, 
		sum(A.val * B.val) as val
	FROM w_X_plots_T as A, w_beta_hat as B
	WHERE A.c = B.r and A.conf_id = B.conf_id
	GROUP BY A.r, B.c, A.conf_id
	order by r, c 
)
select * from w_X_beta
);';
analyze w_X_beta;

execute '
create temporary table w_ldsity_residuals_cluster ON COMMIT DROP as (
with w_Y_plots_T AS NOT MATERIALIZED (
	SELECT conf_id, gid AS r, cluster, attribute AS c, plots_per_cluster, ldsity AS val 
	FROM w_ldsity_plot WHERE is_target
)
, w_residuals_plot AS NOT MATERIALIZED ( -------------------------RESIDUAL LOCAL DENSITY ON TRACT LEVEL
	select
		w_Y_plots_T.conf_id,
		w_Y_plots_T.r AS plot, 
		w_Y_plots_T.c AS attribute, 
		(w_Y_plots_T.val - w_X_beta.val) AS ldsity 
	from w_Y_plots_T 
	INNER JOIN w_X_beta USING (r, c, conf_id)
)
, w_residuals_cluster AS NOT MATERIALIZED (
	with
	w_residuals_plot AS NOT MATERIALIZED (select * from w_residuals_plot),
	w_ldsity_plot AS NOT MATERIALIZED (select * from w_ldsity_plot)
	SELECT
		w_ldsity_plot.conf_id,
		w_ldsity_plot.cluster,
		w_residuals_plot.attribute,
		w_ldsity_plot.stratum,
		w_ldsity_plot.plots_per_cluster,
		count(*) as plcount,
		sum(plot_is_in_cell::int) > 0 as cluster_is_in_cell,
		sum(w_residuals_plot.ldsity * plot_is_in_cell::int)/w_ldsity_plot.plots_per_cluster AS ldsity_res_D, --eq 38
		sum(w_residuals_plot.ldsity)/w_ldsity_plot.plots_per_cluster AS ldsity_res_D_plus -- eq 39
	FROM w_ldsity_plot AS w_ldsity_plot
	INNER JOIN w_residuals_plot AS w_residuals_plot ON w_ldsity_plot.gid = w_residuals_plot.plot AND w_ldsity_plot.attribute = w_residuals_plot.attribute and w_ldsity_plot.conf_id = w_residuals_plot.conf_id
	GROUP BY stratum, w_ldsity_plot.conf_id, w_ldsity_plot.cluster, w_ldsity_plot.plots_per_cluster, w_residuals_plot.attribute
	ORDER BY stratum, w_ldsity_plot.conf_id, w_ldsity_plot.cluster, w_residuals_plot.attribute
)
, w_ldsity_residuals_cluster AS NOT MATERIALIZED (
	select 
		w_ldsity_cluster.conf_id, w_ldsity_cluster.gid, w_ldsity_cluster.cluster, 
		w_ldsity_cluster.attribute, w_ldsity_cluster.stratum, w_ldsity_cluster.plots_per_cluster, 
		w_ldsity_cluster.plcount, w_ldsity_cluster.cluster_is_in_cell, w_ldsity_cluster.ldsity_d, w_ldsity_cluster.ldsity_d_plus, 
		w_ldsity_cluster.is_aux, w_ldsity_cluster.is_target, w_ldsity_cluster.geom,
		w_residuals_cluster.ldsity_res_D, w_residuals_cluster.ldsity_res_D_plus
	from w_ldsity_cluster
	LEFT JOIN w_residuals_cluster USING (conf_id, cluster, attribute, stratum)
)
select * from w_ldsity_residuals_cluster
);';
analyze w_ldsity_residuals_cluster;

--EXPLAIN (ANALYZE, COSTS, VERBOSE, BUFFERS/*, FORMAT JSON*/)
return query execute '
with w_2p_data AS NOT MATERIALIZED (
	with w_ldsity_residuals_cluster AS NOT MATERIALIZED (select * from w_ldsity_residuals_cluster)
	select 
		w_ldsity_residuals_cluster.conf_id, w_ldsity_residuals_cluster.gid, w_ldsity_residuals_cluster.cluster, 
		w_ldsity_residuals_cluster.attribute, w_ldsity_residuals_cluster.stratum, w_ldsity_residuals_cluster.plots_per_cluster, 
		w_ldsity_residuals_cluster.plcount, w_ldsity_residuals_cluster.cluster_is_in_cell, w_ldsity_residuals_cluster.ldsity_d, 
		w_ldsity_residuals_cluster.ldsity_d_plus, 
		w_ldsity_residuals_cluster.is_aux, w_ldsity_residuals_cluster.is_target, w_ldsity_residuals_cluster.geom,
		w_ldsity_residuals_cluster.ldsity_res_D, w_ldsity_residuals_cluster.ldsity_res_D_plus,
		w_pix.pix, w_pix.sweight, w_DELTA_G_beta.val as DELTA_T__G_beta, 
		w_strata_sum.nb_sampling_units::integer, w_strata_sum.sweight_strata_sum, w_strata_sum.lambda_d_plus
	from w_ldsity_residuals_cluster
	INNER JOIN w_pix USING (conf_id, cluster, stratum)
	INNER JOIN w_DELTA_G_beta ON (w_pix.conf_id = w_DELTA_G_beta.conf_id and cluster = w_DELTA_G_beta.c)
	inner join w_strata_sum on (stratum = f_a_sampling_stratum_gid)
)
, w_2p_data_sigma AS NOT MATERIALIZED (
	select 
		w_2p_data.gid, w_2p_data.cluster, w_2p_data.attribute, w_2p_data.stratum, w_2p_data.plots_per_cluster, w_2p_data.plcount,
		w_2p_data.cluster_is_in_cell as cluster_is_in_cell, 
		w_2p_data.ldsity_d, w_2p_data.ldsity_d_plus, w_2p_data.is_aux, w_2p_data.is_target,
		w_2p_data.geom, w_2p_data.ldsity_res_d, w_2p_data.ldsity_res_d_plus, w_2p_data.pix, w_2p_data.sweight, w_2p_data.DELTA_T__G_beta,
		w_2p_data.nb_sampling_units, w_2p_data.sweight_strata_sum, w_2p_data.lambda_d_plus, w_configuration.sigma
	from w_2p_data 
	inner join w_configuration on (w_2p_data.conf_id = w_configuration.id)
)
select * from w_2p_data_sigma;';

drop table w_configuration;
drop table w_cell_selection;
drop table w_plot;
drop table w_ldsity_plot;
drop table w_ldsity_cluster;
drop table w_clusters;
drop table w_strata_sum;
drop table w_X;
drop table w_Y;
drop table w_pix;
drop table w_t_G_beta;
drop table w_PI;
drop table w_DELTA_G_beta;
drop table w_X_beta;
drop table w_ldsity_residuals_cluster;
end;
$$
  LANGUAGE plpgsql
  COST 100
  ROWS 100000;

COMMENT ON FUNCTION @extschema@.fn_2p_data(integer) IS 'Function preparing data for regression estimate.';

-- </function>

-- <function="fn_delete_aux_conf" schema="extschema" src="functions/extschema/configuration/fn_delete_aux_conf.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_delete_aux_conf(integer)
--DROP FUNCTION @extschema@.fn_delete_aux_conf(integer);
CREATE OR REPLACE FUNCTION @extschema@.fn_delete_aux_conf(_aux_conf integer)
RETURNS void
AS
$function$
DECLARE
BEGIN

-- 2nd order tables
-- t_g_beta
DELETE FROM @extschema@.t_g_beta WHERE aux_conf = $1;
RAISE NOTICE 'Deleting t_g_betas...';
-- t_panel2aux_conf
--DELETE FROM @extschema@.t_panel2aux_conf WHERE aux_conf = $1;
--RAISE NOTICE 'Deleting panel to auxiliary configuration mapping...';
-- t_total_estimate_conf
PERFORM	@extschema@.fn_delete_total_estimate_conf(id)
FROM	@extschema@.t_total_estimate_conf
WHERE	aux_conf = $1;

-- 1st order table
-- t_aux_conf
DELETE FROM @extschema@.t_aux_conf WHERE id = $1;
RAISE NOTICE 'Deleting auxiliary configuration = %.', $1;

RETURN;

END;
$function$
LANGUAGE plpgsql
PARALLEL UNSAFE;

COMMENT ON FUNCTION @extschema@.fn_delete_aux_conf(integer) IS 'Function for auxiliary configuration deletion with all records in referenced tables.';

-- </function>

-- <function="fn_delete_total_estimate_conf" schema="extschema" src="functions/extschema/configuration/fn_delete_total_estimate_conf.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_delete_total_estimate_conf(integer)
--DROP FUNCTION @extschema@.fn_delete_total_estimate_conf(integer);
CREATE OR REPLACE FUNCTION @extschema@.fn_delete_total_estimate_conf(_total_estimate_conf integer)
RETURNS void
AS
$function$
DECLARE
_estimate_conf integer[];
BEGIN

-- 3rd order tables
-- t_result
DELETE FROM @extschema@.t_result WHERE estimate_conf IN (
	SELECT 	id
	FROM 	@extschema@.t_estimate_conf
	WHERE	total_estimate_conf = $1 OR
		denominator = $1)
	;
RAISE NOTICE 'Deleting results.';

-- 2nd order tables
-- t_panel2total_1stph_estimate_conf, 2ndph
--DELETE FROM @extschema@.t_panel2total_2ndph_estimate_conf WHERE total_estimate_conf = $1;
--DELETE FROM @extschema@.t_panel2total_1stph_estimate_conf WHERE total_estimate_conf = $1;
--RAISE NOTICE 'Deleting panel to total_estimate_conf mapping...';
-- t_estimate_conf
WITH w AS MATERIALIZED (DELETE FROM @extschema@.t_estimate_conf WHERE total_estimate_conf = $1 OR denominator = $1 RETURNING id)
SELECT array_agg(id) FROM w INTO _estimate_conf;
RAISE NOTICE 'Deleting estimate configurations (%).', _estimate_conf;

-- 1st order table
-- t_total_estimate_conf
DELETE FROM @extschema@.t_total_estimate_conf WHERE id = $1;
RAISE NOTICE 'Deleting total estimate configuration = %.', $1;

RETURN;
END;
$function$
LANGUAGE plpgsql
PARALLEL UNSAFE;

COMMENT ON FUNCTION @extschema@.fn_delete_total_estimate_conf(integer) IS 'Function for auxiliary configuration deletion with all records in referenced tables.';

-- </function>

-- <function="fn_add_res_total_attr" schema="extschema" src="functions/extschema/additivity/fn_add_res_total_attr.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_add_res_total_attr(integer)

-- DROP FUNCTION @extschema@.fn_add_res_total_attr(integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_add_res_total_attr(
	IN est_confs integer[],
	IN min_diff double precision default 0.0, IN include_null_diff boolean default true
)
  RETURNS TABLE(
	estimation_cell		integer,
	aux_conf		integer,
	force_synthetic		boolean,
	estimate_conf		integer,
	variable		integer,
	point_est		double precision,
	point_est_sum		double precision,
	variables_def		integer[],
	variables_found		integer[],
	estimate_confs_found	integer[],
	diff			double precision
) AS
$BODY$
DECLARE
	_complete_query text;
	_array_text text;
BEGIN
	--------------------------------QUERY--------------------------------
	_array_text := concat(quote_literal(est_confs::text), '::integer[]');
	--raise notice 'est_confs: %',  _array_text;
	_complete_query := '
with w_res_cell_var as not materialized (
	select
		t_result.estimate_conf,
		t_result.point,
		t_total_estimate_conf.estimation_cell,
		t_total_estimate_conf.target_variable as t_variable__id,
		c_estimation_period.estimate_date_begin, c_estimation_period.estimate_date_end, 
		array_agg(t_panel_refyearset_group.panel order by panel) as panels,
		t_total_estimate_conf.aux_conf, force_synthetic
	from @extschema@.t_result
	inner join @extschema@.t_estimate_conf ON t_estimate_conf.id = t_result.estimate_conf
	inner join @extschema@.t_total_estimate_conf ON t_total_estimate_conf.id = t_estimate_conf.total_estimate_conf
	inner join @extschema@.c_estimation_period ON c_estimation_period.id = t_total_estimate_conf.estimation_period
	inner join @extschema@.c_panel_refyearset_group ON c_panel_refyearset_group.id = t_total_estimate_conf.panel_refyearset_group
	inner join @extschema@.t_panel_refyearset_group ON t_panel_refyearset_group.panel_refyearset_group = c_panel_refyearset_group.id
	where (t_estimate_conf.estimate_type = 1)
	and (t_result.is_latest)
	and (t_result.estimate_conf = ANY (' || _array_text || '))
	group by 
		t_result.estimate_conf,
		t_result.point,
		t_total_estimate_conf.estimation_cell,
		t_total_estimate_conf.target_variable,
		c_estimation_period.estimate_date_begin, c_estimation_period.estimate_date_end, 
		t_total_estimate_conf.aux_conf, force_synthetic
)
, w_node_sum as (
	select
		estimation_cell,
		w_res_cell_var.t_variable__id, aux_conf, force_synthetic,
		estimate_date_begin, estimate_date_end, panels,
		estimate_conf as node_estimate_confs,
		sum(point) as node_sum,
		hierarchy.node,
		hierarchy.edges as edges_def
	from w_res_cell_var
	inner join @extschema@.v_variable_hierarchy		as hierarchy on (hierarchy.node = w_res_cell_var.t_variable__id)
	group by estimation_cell, w_res_cell_var.t_variable__id, aux_conf, force_synthetic, estimate_date_begin, estimate_date_end, panels, estimate_conf, node, edges_def
	order by estimation_cell, w_res_cell_var.t_variable__id, aux_conf, force_synthetic, estimate_date_begin, estimate_date_end, panels, estimate_conf, node, edges_def
)
, w_edge_sum as (
	select
		w_node_sum.estimation_cell,
		w_node_sum.aux_conf,
		w_node_sum.force_synthetic,
		w_node_sum.node_estimate_confs,
		w_node_sum.estimate_date_begin, w_node_sum.estimate_date_end, w_node_sum.panels,
		w_node_sum.node,
		w_node_sum.node_sum,
		w_node_sum.edges_def,
		array_agg(w_res_cell_var.t_variable__id order by w_res_cell_var.t_variable__id) as edges_found,
		array_agg(w_res_cell_var.estimate_conf order by w_res_cell_var.estimate_conf) as edges_estimate_confs,
		sum(w_res_cell_var.point) as edges_sum
	from w_node_sum
	left join w_res_cell_var on (
		w_res_cell_var.estimation_cell = w_node_sum.estimation_cell
		and w_res_cell_var.estimate_date_begin = w_node_sum.estimate_date_begin
		and w_res_cell_var.estimate_date_end = w_node_sum.estimate_date_end
		and w_res_cell_var.panels = w_node_sum.panels
		and case when
			w_res_cell_var.aux_conf is null and w_node_sum.aux_conf is null then true
			else w_res_cell_var.aux_conf = w_node_sum.aux_conf end
		and case when
			w_res_cell_var.force_synthetic is null and w_node_sum.force_synthetic is null then true
			else w_res_cell_var.force_synthetic = w_node_sum.force_synthetic end
		and w_res_cell_var.t_variable__id = any(w_node_sum.edges_def))
	group by w_node_sum.estimation_cell, 
		w_node_sum.aux_conf, w_node_sum.force_synthetic, w_node_sum.node_estimate_confs,
		w_node_sum.estimate_date_begin, w_node_sum.estimate_date_end, w_node_sum.panels,
		w_node_sum.node, w_node_sum.node_sum, w_node_sum.edges_def
)
, w_diff as (
	select
		estimation_cell,
		aux_conf,
		force_synthetic,
		node_estimate_confs	as estimate_conf,
		node			as variable,
		node_sum		as point_est,
		edges_sum		as point_est_sum,
		edges_def		as variables_def,
		edges_found		as variables_found,
		edges_estimate_confs	as estimate_confs_found,
		case
			when node_sum != 0.0 and edges_sum = 0.0 then 100.0
			when node_sum = 0.0 and edges_sum = 0.0 then 0
			else abs(1 - (node_sum / edges_sum )) * 100.0
		end as diff
	from w_edge_sum
)
select * from w_diff
where
	(' || include_null_diff || ' and ((diff >= ' || min_diff || ') or (diff is null)))
	or
	(not ' || include_null_diff || ' and (diff >= ' || min_diff || '))
order by diff desc
;
';

	--RAISE NOTICE '%', _complete_query;
	RETURN QUERY EXECUTE _complete_query;
END;
$BODY$
  LANGUAGE plpgsql STABLE
  COST 100
  ROWS 100000;

comment on function @extschema@.fn_add_res_total_attr(integer[], double precision, boolean) is
	'Function showing total estimates attribute additivity (aggregated class estimate should be equal to sum of sub-classes estimates). Hierarchy between aggregated classes and its sub-classes is defined in v_variable_hierarchy.
Function input argument is:
 * Array of estimate configuration ids. FKEY to t_estimate_conf.id. All est. conf. ids to be checked (aggregated class together with sub-classes) need to be passed.
 * Minimal amount of difference [%].
 * Whether include NULL difference -- indicating defined sub-classes missing in data.
Resulting table has following columns:
 * Estimate estimation cell. FKEY to c_estimation_cell.id.
 * Estimate auxiliary configuration. FKEY to t_aux_conf.id.
 * Parameter showing whether estimate is forced to be synthetic.
 * Estimate configuration id. FKEY to t_estimate_conf.id.
 * Estimate attribute -- variable. FKEY to t_variable.id.
 * Aggregated class point estimate.
 * Sum of sub-classes point estimates (belonging to aggregated class).
 * Attributes -- variables defined in hierarchy (v_variable_hierarchy). Array of FKEYs to t_variable.id.
 * Attributes -- variables found in data (t_result). Array of FKEYs to t_variable.id.
 * Estimate configurations found in data (t_result). Array of FKEYs to t_estimate_donf.id.
 * Relative difference between aggregated class estimate and sum of sub-classes estimates.';

GRANT EXECUTE ON FUNCTION @extschema@.fn_add_res_total_attr TO PUBLIC;

-- </function>

-- <function="fn_add_res_total_geo" schema="extschema" src="functions/extschema/additivity/fn_add_res_total_geo.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_add_res_total_geo(integer)

-- DROP FUNCTION @extschema@.fn_add_res_total_geo(integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_add_res_total_geo(
	IN est_confs integer[],
	IN min_diff double precision default 0.0, IN include_null_diff boolean default true
)
  RETURNS TABLE(
	variable		integer,
	aux_conf		integer,
	force_synthetic		boolean,
	estimate_conf		integer,
	estimation_cell		integer,
	point_est		double precision,
	point_est_sum		double precision,
	estimation_cells_def	integer[],
	estimation_cells_found	integer[],
	estimate_confs_found	integer[],
	diff			double precision
) AS
$BODY$
DECLARE
	_complete_query text;
	_array_text text;
BEGIN
	--------------------------------QUERY--------------------------------
	_array_text := concat(quote_literal(est_confs::text), '::integer[]');
	--raise notice 'est_confs: %',  _array_text;
	_complete_query := '
with w_res_cell_var as not materialized (
	select
		t_result.estimate_conf,
		t_result.point,
		t_total_estimate_conf.estimation_cell,
		t_total_estimate_conf.target_variable as t_variable__id,
		c_estimation_period.estimate_date_begin, c_estimation_period.estimate_date_end, 
		json_agg(row_to_json((SELECT d FROM (SELECT est_conf_stratum.stratum, est_conf_stratum.panels) d))
			order by est_conf_stratum.stratum)::jsonb as strata_panels,
		t_total_estimate_conf.aux_conf, force_synthetic
	from @extschema@.t_result
	inner join @extschema@.t_estimate_conf ON t_estimate_conf.id = t_result.estimate_conf
	inner join @extschema@.t_total_estimate_conf ON t_total_estimate_conf.id = t_estimate_conf.total_estimate_conf
	inner join @extschema@.c_estimation_period ON c_estimation_period.id = t_total_estimate_conf.estimation_period
	inner join (
		select
			panel_refyearset_group, stratum, array_agg(t_panel.id order by t_panel.id) as panels
		from @extschema@.t_panel_refyearset_group
		inner join sdesign.t_panel on (t_panel_refyearset_group.panel = t_panel.id)
		group by panel_refyearset_group, stratum
		) as est_conf_stratum
		ON est_conf_stratum.panel_refyearset_group = t_total_estimate_conf.panel_refyearset_group
	where (t_estimate_conf.estimate_type = 1)
	and (t_result.is_latest)
	and (t_result.estimate_conf = ANY (' || _array_text || '))
	group by
		t_result.estimate_conf,
		t_result.point,
		t_total_estimate_conf.estimation_cell,
		t_total_estimate_conf.target_variable,
		c_estimation_period.estimate_date_begin, c_estimation_period.estimate_date_end,
		t_total_estimate_conf.aux_conf, force_synthetic
)
, w_node_sum as (
	select
		estimation_cell,
		w_res_cell_var.t_variable__id, aux_conf, force_synthetic,
		estimate_date_begin, estimate_date_end, strata_panels,
		estimate_conf as node_estimate_confs,
		sum(point) as node_sum,
		hierarchy.node,
		hierarchy.edges as edges_def
	from w_res_cell_var
	inner join @extschema@.v_estimation_cell_hierarchy	as hierarchy on (hierarchy.node = w_res_cell_var.estimation_cell)
	group by estimation_cell, w_res_cell_var.t_variable__id, aux_conf, force_synthetic, estimate_date_begin, estimate_date_end, strata_panels, estimate_conf, node, edges_def
	order by estimation_cell, w_res_cell_var.t_variable__id, aux_conf, force_synthetic, estimate_date_begin, estimate_date_end, strata_panels, estimate_conf, node, edges_def
)
, w_edge_sum as (
	select
		w_node_sum.t_variable__id as variable,
		w_node_sum.aux_conf,
		w_node_sum.force_synthetic,
		w_node_sum.node_estimate_confs,
		w_node_sum.estimate_date_begin, w_node_sum.estimate_date_end,
		w_node_sum.node,
		w_node_sum.node_sum,
		w_node_sum.edges_def,
		array_agg(w_res_cell_var.estimation_cell order by w_res_cell_var.estimation_cell) as edges_found,
		array_agg(w_res_cell_var.estimate_conf order by w_res_cell_var.estimate_conf) as edges_estimate_confs,
		sum(w_res_cell_var.point) as edges_sum
	from w_node_sum
	left join w_res_cell_var on (
		w_res_cell_var.t_variable__id = w_node_sum.t_variable__id
		and w_res_cell_var.estimate_date_begin = w_node_sum.estimate_date_begin
		and w_res_cell_var.estimate_date_end = w_node_sum.estimate_date_end
		and
		(
		SELECT array_to_json(array_agg(elem order by elem))::jsonb
		FROM   jsonb_array_elements(w_node_sum.strata_panels) elem
		WHERE  (elem->>''stratum'')::int in (select jsonb_path_query(w_res_cell_var.strata_panels, ''$.stratum'')::int)
		) = w_res_cell_var.strata_panels
		and case when
			w_res_cell_var.aux_conf is null and w_node_sum.aux_conf is null then true
			else w_res_cell_var.aux_conf = w_node_sum.aux_conf end
		and case when
			w_res_cell_var.force_synthetic is null and w_node_sum.force_synthetic is null then true
			else w_res_cell_var.force_synthetic = w_node_sum.force_synthetic end
		and w_res_cell_var.estimation_cell = any(w_node_sum.edges_def))
	group by w_node_sum.t_variable__id,
		w_node_sum.aux_conf, w_node_sum.force_synthetic, w_node_sum.node_estimate_confs,
		w_node_sum.estimate_date_begin, w_node_sum.estimate_date_end,
		w_node_sum.node, w_node_sum.node_sum, w_node_sum.edges_def
)
, w_diff as (
	select
		variable,
		aux_conf,
		force_synthetic,
		node_estimate_confs	as estimate_conf,
		node			as estimation_cell,
		node_sum		as point_est,
		edges_sum		as point_est_sum,
		edges_def		as estimation_cells_def,
		edges_found		as estimation_cells_found,
		edges_estimate_confs	as estimate_confs_found,
		case
			when node_sum != 0.0 and edges_sum = 0.0 then 100.0
			when node_sum = 0.0 and edges_sum = 0.0 then 0
			else abs(1 - (node_sum / edges_sum )) * 100.0
		end as diff
	from w_edge_sum
)
select * from w_diff
where
	(' || include_null_diff || ' and ((diff >= ' || min_diff || ') or (diff is null)))
	or
	(not ' || include_null_diff || ' and (diff >= ' || min_diff || '))
order by diff desc
;
';

	--RAISE NOTICE '%', _complete_query;
	RETURN QUERY EXECUTE _complete_query;
END;
$BODY$
  LANGUAGE plpgsql STABLE
  COST 100
  ROWS 100000;

comment on function @extschema@.fn_add_res_total_geo(integer[], double precision, boolean) is
	'Fnction showing total estimates geographic additivity (aggregated class estimate should be equal to sum of sub-classes estimates). Hierarchy between aggregated classes and its sub-classes is defined in v_estimation_cell_hierarchy.
Function input argument is:
 * Array of estimate configuration ids. FKEY to t_estimate_conf.id. All est. conf. ids to be checked (aggregated class together with sub-classes) need to be passed.
 * Minimal amount of difference [%].
 * Whether include NULL difference -- indicating defined sub-classes missing in data.
Resulting table has following columns:
 * Estimate attribute -- variable. FKEY to t_variable.id.
 * Estimate auxiliary configuration. FKEY to t_aux_conf.id.
 * Parameter showing whether estimate is forced to be synthetic.
 * Estimate configuration id. FKEY to t_estimate_conf.id.
 * Estimate estimation cell. FKEY to c_estimation_cell.id.
 * Aggregated class point estimate.
 * Sum of sub-classes point estimates (belonging to aggregated class).
 * Estimation cells defined in hierarchy (v_estimation_cell_hierarchy). Array of FKEYs to c_estimation_cell.id.
 * Estimation cells found in data (t_result). Array of FKEYs to c_estimation_cell.id.
 * Estimate configurations found in data (t_result). Array of FKEYs to t_estimate_donf.id.
 * Relative difference between aggregated class estimate and sum of sub-classes estimates.';

GRANT EXECUTE ON FUNCTION @extschema@.fn_add_res_total_geo TO PUBLIC;

-- </function>

-- <function="fn_add_res_ratio_attr" schema="extschema" src="functions/extschema/additivity/fn_add_res_ratio_attr.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_add_res_ratio_attr(integer)

-- DROP FUNCTION @extschema@.fn_add_res_ratio_attr(integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_add_res_ratio_attr(
	IN est_confs integer[],
	IN min_diff double precision default 0.0, IN include_null_diff boolean default true
)
  RETURNS TABLE(
	estimation_cell		integer,
	aux_conf		integer,
	force_synthetic		boolean,
	estimate_conf		integer,
	denominator		integer,
	variable		integer,
	point_est		double precision,
	point_est_sum		double precision,
	variables_def		integer[],
	variables_found		integer[],
	estimate_confs_found	integer[],
	diff			double precision
) AS
$BODY$
DECLARE
	_complete_query text;
	_array_text text;
BEGIN
	--------------------------------QUERY--------------------------------
	_array_text := concat(quote_literal(est_confs::text), '::integer[]');
	--raise notice 'est_confs: %',  _array_text;
	_complete_query := '
with w_res_cell_var as not materialized (
	select
		t_result.estimate_conf,
		t_result.point,
		t_total_estimate_conf.estimation_cell,
		t_total_estimate_conf.target_variable as t_variable__id,
		c_estimation_period.estimate_date_begin, c_estimation_period.estimate_date_end, 
		array_agg(t_panel_refyearset_group.panel order by panel) as panels,
		t_total_estimate_conf.aux_conf, force_synthetic, t_estimate_conf.denominator
	from @extschema@.t_result
	inner join @extschema@.t_estimate_conf ON t_estimate_conf.id = t_result.estimate_conf
	inner join @extschema@.t_total_estimate_conf ON t_total_estimate_conf.id = t_estimate_conf.total_estimate_conf
	inner join @extschema@.c_estimation_period ON c_estimation_period.id = t_total_estimate_conf.estimation_period
	inner join @extschema@.c_panel_refyearset_group ON c_panel_refyearset_group.id = t_total_estimate_conf.panel_refyearset_group
	inner join @extschema@.t_panel_refyearset_group ON t_panel_refyearset_group.panel_refyearset_group = c_panel_refyearset_group.id
	where (t_estimate_conf.estimate_type = 2)
	and (t_result.is_latest)
	and (t_result.estimate_conf = ANY (' || _array_text || '))
	group by 
		t_result.estimate_conf,
		t_result.point,
		t_total_estimate_conf.estimation_cell,
		t_total_estimate_conf.target_variable,
		c_estimation_period.estimate_date_begin, c_estimation_period.estimate_date_end,
		t_total_estimate_conf.aux_conf, force_synthetic, t_estimate_conf.denominator
)
, w_node_sum as (
	select
		estimation_cell,
		w_res_cell_var.t_variable__id, aux_conf, force_synthetic, denominator,
		estimate_date_begin, estimate_date_end, panels,
		estimate_conf as node_estimate_confs,
		sum(point) as node_sum,
		hierarchy.node,
		hierarchy.edges as edges_def
	from w_res_cell_var
	inner join @extschema@.v_variable_hierarchy 	as hierarchy on (hierarchy.node = w_res_cell_var.t_variable__id)
	group by estimation_cell, w_res_cell_var.t_variable__id, aux_conf, force_synthetic, estimate_date_begin, estimate_date_end, panels, estimate_conf, node, edges_def, denominator
	order by estimation_cell, w_res_cell_var.t_variable__id, aux_conf, force_synthetic, estimate_date_begin, estimate_date_end, panels, estimate_conf, node, edges_def, denominator
)
, w_edge_sum as (
	select
		w_node_sum.estimation_cell,
		w_node_sum.aux_conf,
		w_node_sum.force_synthetic,
		w_node_sum.node_estimate_confs, w_node_sum.denominator,
		w_node_sum.estimate_date_begin, w_node_sum.estimate_date_end, w_node_sum.panels,
		w_node_sum.node,
		w_node_sum.node_sum,
		w_node_sum.edges_def,
		array_agg(w_res_cell_var.t_variable__id order by w_res_cell_var.t_variable__id) as edges_found,
		array_agg(w_res_cell_var.estimate_conf order by w_res_cell_var.estimate_conf) as edges_estimate_confs,
		sum(w_res_cell_var.point) as edges_sum
	from w_node_sum
	left join w_res_cell_var on (
		w_res_cell_var.estimation_cell = w_node_sum.estimation_cell
		and w_res_cell_var.estimate_date_begin = w_node_sum.estimate_date_begin
		and w_res_cell_var.estimate_date_end = w_node_sum.estimate_date_end
		and w_res_cell_var.panels = w_node_sum.panels
		and case when
			w_res_cell_var.aux_conf is null and w_node_sum.aux_conf is null then true
			else w_res_cell_var.aux_conf = w_node_sum.aux_conf end
		and case when
			w_res_cell_var.force_synthetic is null and w_node_sum.force_synthetic is null then true
			else w_res_cell_var.force_synthetic = w_node_sum.force_synthetic end
		and w_res_cell_var.t_variable__id = any(w_node_sum.edges_def)) and w_res_cell_var.denominator = w_node_sum.denominator
	group by w_node_sum.estimation_cell, 
		w_node_sum.aux_conf, w_node_sum.force_synthetic, w_node_sum.node_estimate_confs, w_node_sum.denominator,
		w_node_sum.estimate_date_begin, w_node_sum.estimate_date_end, w_node_sum.panels,
		w_node_sum.node, w_node_sum.node_sum, w_node_sum.edges_def
)
, w_diff as (
	select
		estimation_cell,
		aux_conf,
		force_synthetic,
		node_estimate_confs	as estimate_conf, denominator,
		node			as variable,
		node_sum		as point_est,
		edges_sum		as point_est_sum,
		edges_def		as variables_def,
		edges_found		as variables_found,
		edges_estimate_confs	as estimate_confs_found,
		case
			when node_sum != 0.0 and edges_sum = 0.0 then 100.0
			when node_sum = 0.0 and edges_sum = 0.0 then 0
			else abs(1 - (node_sum / edges_sum )) * 100.0
		end as diff
	from w_edge_sum
)
select * from w_diff
where
	(' || include_null_diff || ' and ((diff >= ' || min_diff || ') or (diff is null)))
	or
	(not ' || include_null_diff || ' and (diff >= ' || min_diff || '))
order by diff desc
;
';

	--RAISE NOTICE '%', _complete_query;
	RETURN QUERY EXECUTE _complete_query;
END;
$BODY$
  LANGUAGE plpgsql STABLE
  COST 100
  ROWS 100000;

comment on function @extschema@.fn_add_res_ratio_attr(integer[], double precision, boolean) is
'Function showing ratio estimates attribute additivity (aggregated class estimate should be equal to sum of sub-classes estimates). Hierarchy between aggregated classes and its sub-classes is defined in v_variable_hierarchy.
Function input argument is:
 * Array of attributes -- variables (FKEY to t_variable.id). All variables to be checked (aggregated class together with sub-classes) need to be passed.
 * Minimal amount of difference [%].
 * Whether include NULL difference -- indicating defined sub-classes missing in data.
Resulting table has following columns:
 * Estimate estimation cell. FKEY to c_estimation_cell.id.
 * Estimate auxiliary configuration. FKEY to t_aux_conf.id.
 * Parameter showing whether estimate is forced to be synthetic.
 * Estimate configuration id. FKEY to t_estimate_conf.id.
 * Estimate configuration id of denominator. FKEY to t_estimate_conf.id.
 * Estimate attribute -- variable. FKEY to t_variable.id.
 * Aggregated class point estimate.
 * Sum of sub-classes point estimates (belonging to aggregated class).
 * Attributes -- variables defined in hierarchy (v_variable_hierarchy). Array of FKEYs to t_variable.id.
 * Attributes -- variables found in data (t_result). Array of FKEYs to t_variable.id.
 * Estimate configurations found in data (t_result). Array of FKEYs to t_estimate_donf.id.
 * Relative difference between aggregated class estimate and sum of sub-classes estimates.';

GRANT EXECUTE ON FUNCTION @extschema@.fn_add_res_ratio_attr TO PUBLIC;

-- </function>

