--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

CREATE OR REPLACE VIEW @extschema@.v_estimation_cell_hierarchy AS
SELECT cell_superior as node, array_agg(cell order by cell) AS edges
FROM @extschema@.t_estimation_cell_hierarchy
GROUP BY cell_superior
ORDER BY cell_superior;

GRANT SELECT ON TABLE @extschema@.v_estimation_cell_hierarchy TO PUBLIC;
