--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

CREATE OR REPLACE VIEW @extschema@.v_variable_hierarchy_aux_total AS
SELECT
	t_variable_hierarchy.variable_superior as node,
	array_agg(t_variable_hierarchy.variable order by t_variable_hierarchy.variable) AS edges
FROM @extschema@.t_variable_hierarchy
INNER JOIN @extschema@.t_variable 			AS edge_var ON t_variable_hierarchy.variable = edge_var.id
LEFT JOIN @extschema@.c_sub_population_category 	AS edge_spc ON edge_var.sub_population_category = edge_spc.id
LEFT JOIN @extschema@.c_area_domain_category 		AS edge_adc ON edge_var.area_domain_category = edge_adc.id
LEFT JOIN @extschema@.c_auxiliary_variable_category 	AS edge_avc ON edge_var.auxiliary_variable_category = edge_avc.id
GROUP BY
	t_variable_hierarchy.variable_superior,
	edge_spc.sub_population, edge_adc.area_domain, edge_avc.auxiliary_variable
ORDER BY
	t_variable_hierarchy.variable_superior,
	edge_spc.sub_population, edge_adc.area_domain, edge_avc.auxiliary_variable
;

GRANT SELECT ON TABLE @extschema@.v_variable_hierarchy_aux_total TO PUBLIC;

----------------------------------------------------------------------------------------------------------

CREATE OR REPLACE VIEW @extschema@.v_variable_hierarchy_plot AS
SELECT
	node_ads.panel, node_ads.reference_year_set,
	t_variable_hierarchy.variable_superior as node,
	array_agg(t_variable_hierarchy.variable order by t_variable_hierarchy.variable) AS edges
FROM @extschema@.t_variable_hierarchy
INNER JOIN @extschema@.t_variable 			AS node_var	ON (t_variable_hierarchy.variable_superior	=	node_var.id)
INNER JOIN @extschema@.t_available_datasets		AS node_ads	ON (node_var.id 				=	node_ads.variable)
INNER JOIN @extschema@.t_variable 			AS edge_var	ON (t_variable_hierarchy.variable		=	edge_var.id)
INNER JOIN @extschema@.t_available_datasets		AS edge_ads	ON (edge_var.id 				=	edge_ads.variable)
LEFT JOIN @extschema@.c_sub_population_category 	AS edge_spc 	ON edge_var.sub_population_category 		= 	edge_spc.id
LEFT JOIN @extschema@.c_area_domain_category 		AS edge_adc 	ON edge_var.area_domain_category 		= 	edge_adc.id
LEFT JOIN @extschema@.c_auxiliary_variable_category 	AS edge_avc 	ON edge_var.auxiliary_variable_category 	= 	edge_avc.id
WHERE  	(node_var.auxiliary_variable_category is null and edge_var.auxiliary_variable_category is null)
	and ((node_ads.panel = edge_ads.panel) AND (node_ads.reference_year_set = edge_ads.reference_year_set))
GROUP BY
	node_ads.panel, node_ads.reference_year_set,
	t_variable_hierarchy.variable_superior,
	edge_spc.sub_population, edge_adc.area_domain, edge_avc.auxiliary_variable
ORDER BY
	node_ads.panel, node_ads.reference_year_set,
	t_variable_hierarchy.variable_superior,
	edge_spc.sub_population, edge_adc.area_domain, edge_avc.auxiliary_variable
;

GRANT SELECT ON TABLE @extschema@.v_variable_hierarchy_plot TO PUBLIC;

----------------------------------------------------------------------------------------------------------

CREATE OR REPLACE VIEW @extschema@.v_variable_hierarchy_plot_aux AS
SELECT
	node_ads.panel,
	t_variable_hierarchy.variable_superior as node,
	array_agg(t_variable_hierarchy.variable order by t_variable_hierarchy.variable) AS edges
FROM @extschema@.t_variable_hierarchy
INNER JOIN @extschema@.t_variable 			AS node_var	ON (t_variable_hierarchy.variable_superior	=	node_var.id)
INNER JOIN @extschema@.t_available_datasets		AS node_ads	ON (node_var.id 				=	node_ads.variable)
INNER JOIN @extschema@.t_variable 			AS edge_var	ON (t_variable_hierarchy.variable		=	edge_var.id)
INNER JOIN @extschema@.t_available_datasets		AS edge_ads	ON (edge_var.id 				=	edge_ads.variable)
LEFT JOIN @extschema@.c_sub_population_category 	AS edge_spc 	ON edge_var.sub_population_category 		= 	edge_spc.id
LEFT JOIN @extschema@.c_area_domain_category 		AS edge_adc 	ON edge_var.area_domain_category 		= 	edge_adc.id
LEFT JOIN @extschema@.c_auxiliary_variable_category 	AS edge_avc 	ON edge_var.auxiliary_variable_category 	= 	edge_avc.id
WHERE  	(node_var.auxiliary_variable_category is not null and edge_var.auxiliary_variable_category is not null)
	and ((node_ads.panel = edge_ads.panel))
GROUP BY
	node_ads.panel,
	t_variable_hierarchy.variable_superior,
	edge_spc.sub_population, edge_adc.area_domain, edge_avc.auxiliary_variable
ORDER BY
	node_ads.panel,
	t_variable_hierarchy.variable_superior,
	edge_spc.sub_population, edge_adc.area_domain, edge_avc.auxiliary_variable
;

GRANT SELECT ON TABLE @extschema@.v_variable_hierarchy_plot_aux TO PUBLIC;

----------------------------------------------------------------------------------------------------------

CREATE OR REPLACE VIEW @extschema@.v_variable_hierarchy_res AS
SELECT
	node_tec.panel_refyearset_group,
	t_variable_hierarchy.variable_superior as node,
	array_agg(distinct t_variable_hierarchy.variable order by t_variable_hierarchy.variable) AS edges
FROM @extschema@.t_variable_hierarchy
INNER JOIN @extschema@.t_variable 			AS node_var	ON (t_variable_hierarchy.variable_superior	=	node_var.id)
INNER JOIN (select distinct variable, panel_refyearset_group from @extschema@.t_total_estimate_conf)            
							AS node_tec     ON (node_var.id                                 =       node_tec.variable)
INNER JOIN @extschema@.t_variable 			AS edge_var	ON (t_variable_hierarchy.variable		=	edge_var.id)
INNER JOIN (select distinct variable, panel_refyearset_group from @extschema@.t_total_estimate_conf)           
							AS edge_tec     ON (edge_var.id                                 =       edge_tec.variable)
LEFT JOIN @extschema@.c_sub_population_category 	AS edge_spc 	ON edge_var.sub_population_category 		= 	edge_spc.id
LEFT JOIN @extschema@.c_area_domain_category 		AS edge_adc 	ON edge_var.area_domain_category 		= 	edge_adc.id
LEFT JOIN @extschema@.c_auxiliary_variable_category 	AS edge_avc 	ON edge_var.auxiliary_variable_category 	= 	edge_avc.id
WHERE (node_tec.panel_refyearset_group = edge_tec.panel_refyearset_group)
GROUP BY
	node_tec.panel_refyearset_group,
	t_variable_hierarchy.variable_superior,
	edge_spc.sub_population, edge_adc.area_domain, edge_avc.auxiliary_variable
ORDER BY
	node_tec.panel_refyearset_group,
	t_variable_hierarchy.variable_superior,
	edge_spc.sub_population, edge_adc.area_domain, edge_avc.auxiliary_variable
;

GRANT SELECT ON TABLE @extschema@.v_variable_hierarchy_res TO PUBLIC;

