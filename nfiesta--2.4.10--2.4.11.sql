--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

alter table @extschema@.t_auxiliary_data	add column value_inserted 	timestamp with time zone default now();
alter table @extschema@.t_target_data		add column value_inserted 	timestamp with time zone default now();
alter table @extschema@.t_aux_total		add column aux_total_inserted 	timestamp with time zone default now();

-- Following table was not used since t_total_estimate_data was removed. Is replaced by c_phase_estimate_type:
drop sequence @extschema@.c_aux_phase_type_id_seq;
drop table @extschema@.c_aux_phase_type;
