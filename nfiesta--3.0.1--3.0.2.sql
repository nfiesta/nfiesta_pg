--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

alter table @extschema@.c_area_domain_category drop constraint ukey__c_area_domain_category__label;
alter table @extschema@.c_area_domain_category add constraint ukey__c_area_domain_category__label_area_domain UNIQUE (label, area_domain);

alter table @extschema@.c_sub_population_category drop constraint ukey__c_sub_population_category__label;
alter table @extschema@.c_sub_population_category add constraint ukey__c_sub_population_category__label_sub_population UNIQUE (label, sub_population);
