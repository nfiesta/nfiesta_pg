--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- <function name="fn_get_area_sub_population_categories" schema="extschema" src="functions/extschema/configuration/fn_get_area_sub_population_categories.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_get_area_sub_population_categories.sql(integer, integer, integer)
--DROP FUNCTION @extschema@.fn_get_area_sub_population_categories.sql(integer, integer, integer);
CREATE OR REPLACE FUNCTION @extschema@.fn_get_area_sub_population_categories(
		_target_variable integer, _area_or_sub_pop integer, _id integer)
RETURNS TABLE (
variable 	integer,
attype 		integer,
category 	integer
)
AS
$function$
DECLARE
BEGIN

IF _target_variable IS NULL THEN
	RAISE EXCEPTION 'Target variable is NULL!';
END IF;

IF _area_or_sub_pop IS NULL
THEN
	RAISE EXCEPTION 'Parameter area domain or sub_population is not choosen!';
END IF; 

IF _area_or_sub_pop NOT IN (100,200)
THEN
	RAISE EXCEPTION 'Parameter area domain or sub_population has to be either 100 (area_domain) or 200 (sub_population)!';
END IF; 

IF NOT EXISTS (SELECT id FROM @extschema@.c_target_variable WHERE id = _target_variable)
THEN
	RAISE EXCEPTION 'Specified target_variable (%) does not exist in table c_target_variable!', _target_variable;
END IF;

IF _id IS NULL
THEN
	IF NOT EXISTS (
		SELECT t1.id
		FROM @extschema@.t_variable AS t1
		WHERE t1.target_variable = _target_variable)
	THEN 
		RAISE EXCEPTION 'Specified target_variable(%) does not exist in the data (t_variable)!', _target_variable;
	END IF;

	RETURN QUERY EXECUTE
	'SELECT id, NULL::integer, NULL::integer
	FROM @extschema@.t_variable AS t1
	WHERE t1.target_variable = $1 AND t1.area_domain_category IS NULL AND t1.sub_population_category IS NULL'
	USING _target_variable;
ELSE

	CASE WHEN _area_or_sub_pop = 100
	THEN
		IF _area_or_sub_pop = 100 AND NOT EXISTS (SELECT id FROM @extschema@.c_area_domain WHERE id = _id)
		THEN
			RAISE EXCEPTION 'Specified area_domain (%) does not exist in table c_area_domain!', _id;
		END IF;
		IF _area_or_sub_pop = 100 AND NOT EXISTS (
			SELECT t1.id
			FROM @extschema@.t_variable AS t1
			INNER JOIN @extschema@.c_area_domain_category AS t2
			ON t1.area_domain_category = t2.id
			WHERE t1.target_variable = _target_variable AND
				t2.area_domain = _id)
		THEN 
			RAISE EXCEPTION 'Specified combination of target_variable(%) and area_domain (%) does not exist in the data (t_variable)!', _target_variable, _id;
		END IF;

		RETURN QUERY EXECUTE
		'WITH RECURSIVE w_variables (target_variable, variable_superior, variable) AS (
			SELECT
				t2.target_variable,
				variable_superior,
				variable
			FROM @extschema@.t_variable_hierarchy AS t1
			LEFT JOIN @extschema@.t_variable AS t2
			ON t1.variable = t2.id
			LEFT JOIN @extschema@.c_area_domain_category AS t3
			ON t2.area_domain_category = t3.id
			WHERE 	t2.target_variable = $1 AND
				t3.area_domain = $2
			UNION ALL
			SELECT
				t1.target_variable,
				t2.variable_superior,
				t2.variable
			FROM w_variables AS t1
			LEFT JOIN @extschema@.t_variable_hierarchy AS t2
			ON t1.variable_superior = t2.variable
			WHERE t2.variable IS NOT NULL
		)
		, w_attr_types AS (
			SELECT
				t1.target_variable,
				t1.variable_superior, t1.variable, 
				t3.area_domain AS area_domain_sup, t5.area_domain, 
				t3.id AS id_cat_sup, t3.label AS category_sup,
				t5.id AS id_cat, t5.label AS category
			FROM w_variables AS t1
			INNER JOIN
				@extschema@.t_variable AS t2
			ON 
				t1.variable_superior = t2.id
			LEFT JOIN
				@extschema@.c_area_domain_category AS t3
			ON t2.area_domain_category = t3.id
			INNER JOIN
				@extschema@.t_variable AS t4
			ON 
				t1.variable = t4.id
			LEFT JOIN
				@extschema@.c_area_domain_category AS t5
			ON t4.area_domain_category = t5.id
		),
		w_union AS (
			SELECT	target_variable, variable_superior AS variable,  area_domain_sup AS area_domain
			FROM	w_attr_types
			UNION ALL
			SELECT	target_variable, variable,  area_domain
			FROM	w_attr_types
		)
		,w_distinct AS (
			SELECT DISTINCT target_variable, area_domain
			FROM w_union
		)
		SELECT
			t3.id AS variable, t2.area_domain AS type, t2.id AS category
		FROM
			w_distinct AS t1
		LEFT JOIN
			@extschema@.c_area_domain_category AS t2
		ON 	t1.area_domain = t2.area_domain
		INNER JOIN
			@extschema@.t_variable AS t3
		ON t1.target_variable = t3.target_variable AND
		CASE WHEN t2.id IS NOT NULL THEN t3.area_domain_category = t2.id
		ELSE t3.area_domain_category IS NULL END
		ORDER BY t3.id
		'
		USING _target_variable, _id;

	WHEN _area_or_sub_pop = 200
	THEN
		IF _area_or_sub_pop = 200 AND NOT EXISTS (SELECT id FROM @extschema@.c_sub_population WHERE id = _id)
		THEN
			RAISE EXCEPTION 'Specified sub_population (%) does not exist in table c_sub_population!', _id;
		END IF;

		IF _area_or_sub_pop = 200 AND NOT EXISTS (
			SELECT t1.id
			FROM @extschema@.t_variable AS t1
			INNER JOIN @extschema@.c_sub_population_category AS t2
			ON t1.sub_population_category = t2.id
			WHERE t1.target_variable = _target_variable AND
				t2.sub_population = _id)
		THEN 
			RAISE EXCEPTION 'Specified combination of target_variable(%) and sub_population (%) does not exist in the data (t_variable)!', _target_variable, _id;
		END IF;

		RETURN QUERY EXECUTE
		'WITH RECURSIVE w_variables (target_variable, variable_superior, variable) AS (
			SELECT
				t2.target_variable,
				variable_superior,
				variable
			FROM @extschema@.t_variable_hierarchy AS t1
			LEFT JOIN @extschema@.t_variable AS t2
			ON t1.variable = t2.id
			LEFT JOIN @extschema@.c_sub_population_category AS t3
			ON t2.sub_population_category = t3.id
			WHERE 	t2.target_variable = $1 AND
				t3.sub_population = $2
			UNION ALL
			SELECT
				t1.target_variable,
				t2.variable_superior,
				t2.variable
			FROM w_variables AS t1
			LEFT JOIN @extschema@.t_variable_hierarchy AS t2
			ON t1.variable_superior = t2.variable
			WHERE t2.variable IS NOT NULL
		)
		, w_attr_types AS (
			SELECT
				t1.target_variable,
				t1.variable_superior, t1.variable, 
				t3.sub_population AS sub_population_sup, t5.sub_population, 
				t3.id AS id_cat_sup, t3.label AS category_sup,
				t5.id AS id_cat, t5.label AS category
			FROM w_variables AS t1
			INNER JOIN
				@extschema@.t_variable AS t2
			ON 
				t1.variable_superior = t2.id
			LEFT JOIN
				@extschema@.c_sub_population_category AS t3
			ON t2.sub_population_category = t3.id
			INNER JOIN
				@extschema@.t_variable AS t4
			ON 
				t1.variable = t4.id
			LEFT JOIN
				@extschema@.c_sub_population_category AS t5
			ON t4.sub_population_category = t5.id
		),
		w_union AS (
			SELECT	target_variable, variable_superior AS variable,  sub_population_sup AS sub_population
			FROM	w_attr_types
			UNION ALL
			SELECT	target_variable, variable,  sub_population
			FROM	w_attr_types
		)
		,w_distinct AS (
			SELECT DISTINCT target_variable, sub_population
			FROM w_union
		)
		SELECT
			t3.id AS variable, t2.sub_population AS type, t2.id AS category
		FROM
			w_distinct AS t1
		LEFT JOIN
			@extschema@.c_sub_population_category AS t2
		ON 	t1.sub_population = t2.sub_population
		INNER JOIN
			@extschema@.t_variable AS t3
		ON t1.target_variable = t3.target_variable AND
		CASE WHEN t2.id IS NOT NULL THEN t3.sub_population_category = t2.id
		ELSE t3.sub_population_category IS NULL END
		ORDER BY t3.id'
		USING _target_variable, _id;
	ELSE
		RAISE EXCEPTION 'Uknown attribute type!';
	END CASE;
END IF;
END;
$function$
LANGUAGE plpgsql
PARALLEL SAFE;

COMMENT ON FUNCTION @extschema@.fn_get_area_sub_population_categories(integer,integer,integer) IS 'Function returns table with all hierarchically superior variables and its complementary categories within area domain or sub_population. Input parameters are target_variable (id from table c_target_variable), area_or_sub_pop (100 for area_domain, 200 for sub_population) and id (id from table c_area_domain or id from table c_sub_population)';

-- </function>
