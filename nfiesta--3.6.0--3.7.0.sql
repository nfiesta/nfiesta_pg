-- Copyright 2017, 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.

-- drop function that that is redesigned and replaced by the belo fn_api_get_1pgroups4regtotal(INT, INT[], INT[])
DROP FUNCTION @extschema@.fn_api_find_1pgroups4regtotal(INT, INT[], INT[]);

-- <function name="fn_api_get_1pgroups4regtotal" schema="extschema" src="functions/extschema/configuration/fn_api_get_1pgroups4regtotal.sql">
-- Copyright 2017, 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_api_get_1pgroups4regtotal(INT, ARRAY[INT], ARRAY[INT])
-- DROP FUNCTION @extschema@.fn_api_get_1pgroups4regtotal(INT, INT[], INT[]);

CREATE OR REPLACE FUNCTION @extschema@.fn_api_get_1pgroups4regtotal(_estimation_period INT, _estimation_cells INT[], _variables INT[])
RETURNS TABLE(
	estimation_period_id INT, 
	estimation_period_label VARCHAR(200), 
	estimation_period_label_en VARCHAR(200), 
	estimation_period_description TEXT, 
	estimation_period_description_en TEXT,
	country_id INT,
	country_label VARCHAR(200),
	country_label_en VARCHAR(200),
	country_description TEXT,
	country_description_en TEXT,
	strata_set_id INT,
	strata_set_label VARCHAR(200),
	strata_set_label_en VARCHAR(200),
	strata_set_description TEXT,
	strata_set_description_en TEXT,
	stratum_id INT,
	stratum_label VARCHAR(200),
	stratum_label_en VARCHAR(200),
	stratum_description TEXT,
	stratum_description_en TEXT,
	panel_refyearset_group_id INT, 
	panel_refyearset_group_label VARCHAR(200),
	panel_refyearset_group_label_en VARCHAR(200),
	panel_refyearset_group_description TEXT,
	panel_refyearset_group_description_en TEXT,
	complete_group BOOLEAN)
AS
$function$
BEGIN

-- testing input arguments if not NULL
IF _estimation_period IS NULL THEN
	RAISE EXCEPTION 'fn_api_get_1pgroups4regtotal: Function argument _estimation_period INT must not be NULL!';
END IF;

IF _estimation_cells IS NULL THEN
	RAISE EXCEPTION 'fn_api_get_1pgroups4regtotal: Function argument _estimation_cells INT[] must not be NULL!';
END IF;

IF _variables IS NULL THEN
	RAISE EXCEPTION 'fn_api_get_1pgroups4regtotal: Function argument _variables INT[] must not be NULL!';
END IF;

-- checking input arrays if they do not contain NULL
IF (SELECT array_position(_estimation_cells, NULL) IS NOT NULL) THEN
	RAISE EXCEPTION 'fn_api_get_1pgroups4regtotal: Function argument _estimation_cells INT[] must not be an array containing NULL!';
END IF;

IF (SELECT array_position(_variables, NULL) IS NOT NULL) THEN
	RAISE EXCEPTION 'fn_api_get_1pgroups4regtotal: Function argument _variables INT[] must not be an array containing NULL!';
END IF;

-- checking one cell collection within input argument _estimation_cells 
IF (SELECT count(DISTINCT estimation_cell_collection) FROM @extschema@.c_estimation_cell WHERE ARRAY[id] <@ _estimation_cells) IS DISTINCT FROM 1 THEN 
	RAISE EXCEPTION 'fn_api_get_1pgroups4regtotal: Function argument _estimation_cells INT[] must contain cells corresponding to one and the only estimation cell collection!';
END IF;

RETURN QUERY EXECUTE '
WITH w_reg2conf AS MATERIALIZED (
	SELECT 
		$1 AS estimation_period_reg,
		t3.stratum AS stratum_reg,
		t1.estimation_cell AS estimation_cell_reg,
		t2.variable AS variable_reg
	FROM
	  (SELECT unnest($2) AS estimation_cell) AS t1 
	 CROSS JOIN
	  (SELECT unnest($3) AS variable) AS t2
	 INNER JOIN
	  @extschema@.t_stratum_in_estimation_cell AS t3
	 ON t1.estimation_cell = t3.estimation_cell
), w_conf1p AS MATERIALIZED (
SELECT 
	t1.*,
	t2.*,
	count(*) OVER (PARTITION BY t1.estimation_period_reg, t1.stratum_reg) AS no_of_desired_reg_estimates,
	count(*) FILTER (WHERE t2.panel_refyearset_group IS NOT NULL) OVER (PARTITION BY t1.estimation_period_reg, t1.stratum_reg, t2.panel_refyearset_group) AS no_configured_1pestimates_per_group
FROM 
	w_reg2conf AS t1
LEFT JOIN @extschema@.t_total_estimate_conf  AS t2
	ON 
	t1.estimation_period_reg = t2.estimation_period AND
	t1.estimation_cell_reg = t2.estimation_cell AND
	t1.variable_reg = t2.variable AND
	t2.phase_estimate_type = 1

), w_conf1p_agg AS MATERIALIZED ( 
SELECT DISTINCT
	estimation_period_reg,
	stratum_reg,
	panel_refyearset_group,
	no_of_desired_reg_estimates = no_configured_1pestimates_per_group AS complete_group
FROM 
	w_conf1p
WHERE panel_refyearset_group IS NOT NULL
-- the above could be the final query, but we need to add records for strata with NULL in panel_refyearset_group in case there are no 1p configiration 
-- for any cell and for any varibale either so the WHERE condition filter such records out
-- at the same time the WHERE is needed in order to avoid duplicate records (one with NULL group, the other NOT NULL but still incomplete) for groups with
-- partial configuration for some cells or for some variables only
)
SELECT 
-- here by left JOIN we get records for all strata at any circumstances, 
--potentially with NULL groups (no 1p configs at all for any cell and any desired variable within the given stratum)
	t3.id AS estimation_period_id,
	t3.label::VARCHAR(200) AS estimation_period_label,
	t3.label_en::VARCHAR(200) AS estimation_period_label_en,
    t3.description AS estimation_period_description,
	t3.description_en AS estimation_period_description_en,
	t6.id AS country_id,
	t6.label::VARCHAR(200) AS country_label,
	t6.label_en::VARCHAR(200) AS country_label_en,
	t6.description AS country_description,
	t6.description_en AS country_description_en,
	t5.id AS strata_set_id,
	t5.strata_set::VARCHAR(200) AS strata_set_label,
	t5.strata_set::VARCHAR(200) AS strata_set_label_en,
	t5.label::TEXT AS strata_set_description,
	t5.label::TEXT AS strata_set_description_en,
	t4.id AS stratum_id,
	t4.stratum::VARCHAR(200) AS stratum_label,
	t4.stratum::VARCHAR(200) AS stratum_label_en,
	t4.label::TEXT AS stratum_description,
	t4.label::TEXT AS stratum_description_en,
	t7.id AS panel_refyearset_group_id,
	t7.label AS panel_refyearset_group_label,
	t7.label_en AS panel_refyearset_group_label_en,
	t7.description AS panel_refyearset_group_description,
	t7.description_en AS panel_refyearset_group_description_en,
	t2.complete_group
FROM 
	(SELECT DISTINCT estimation_period_reg, stratum_reg FROM w_reg2conf) AS t1
LEFT JOIN
	w_conf1p_agg AS t2
	ON t1.estimation_period_reg = t2.estimation_period_reg AND t1.stratum_reg = t2.stratum_reg
INNER JOIN 
	@extschema@.c_estimation_period AS t3
	ON t1.estimation_period_reg = t3.id
INNER JOIN 
	sdesign.t_stratum AS t4
	ON t1.stratum_reg = t4.id
INNER JOIN 
	sdesign.t_strata_set AS t5
	ON t4.strata_set = t5.id
INNER JOIN 
	sdesign.c_country AS t6
	ON t5.country = t6.id
LEFT JOIN 
	@extschema@.c_panel_refyearset_group AS t7
	ON t2.panel_refyearset_group = t7.id;' USING _estimation_period, _estimation_cells, _variables;
END;
$function$
LANGUAGE plpgsql
PARALLEL SAFE;

COMMENT ON FUNCTION  @extschema@.fn_api_get_1pgroups4regtotal(INT, INT[], INT[]) IS 
'Function returns list of strata and panel refyearset groups with a Bollean '
'indicator saying whether 1p configurations using the group correspond to all '
'desired regression estimates, i.e. considering a subset of cells for estimation ' 
'intersecting (not just touching) the stratum and considering the desired list of '
'variables.';

/* tests
-- test NULL for _estimation_period argument
SELECT * FROM @extschema@.fn_api_get_1pgroups4regtotal(NULL, ARRAY[44,45,46,47,48,49,50,51,52,53,56,57,58,62], ARRAY[1,2,3]);

-- test NULL for _estimation_cell argument
SELECT * FROM @extschema@.fn_api_get_1pgroups4regtotal(1, NULL, ARRAY[1,2,3]);

-- test NULL for _variables argument
SELECT * FROM @extschema@.fn_api_get_1pgroups4regtotal(1, ARRAY[44,45,46,47,48,49,50,51,52,53,56,57,58,62], NULL);

-- test for NULL as an element of _estimation_cell argument
SELECT * FROM @extschema@.fn_api_get_1pgroups4regtotal(1, ARRAY[44,45,46,47,NULL,49,50,51,52,53,56,57,58,62], ARRAY[1,2,3]);
SELECT * FROM @extschema@.fn_api_get_1pgroups4regtotal(1, ARRAY[NULL], ARRAY[1,2,3]);
SELECT * FROM @extschema@.fn_api_get_1pgroups4regtotal(1, ARRAY[NULL]::int[], ARRAY[1,2,3]);

-- test for NULL as an element of _variables argument
SELECT * FROM @extschema@.fn_api_get_1pgroups4regtotal(1, ARRAY[44,45,46,47,48,49,50,51,52,53,56,57,58,62], ARRAY[NULL,2,NULL]);
SELECT * FROM @extschema@.fn_api_get_1pgroups4regtotal(1, ARRAY[44,45,46,47,48,49,50,51,52,53,56,57,58,62], ARRAY[NULL]);
SELECT * FROM @extschema@.fn_api_get_1pgroups4regtotal(1, ARRAY[44,45,46,47,48,49,50,51,52,53,56,57,58,62], ARRAY[NULL]::int[]);

-- test if estimation cells belong to one estimation cell collection (adding 1 to the valid array _estimation_cells, 1 
-- belongs to another collection but the same stratum
SELECT * FROM @extschema@.fn_api_get_1pgroups4regtotal(1, ARRAY[1,44,45,46,47,48,49,50,51,52,53,56,57,58,62], ARRAY[1,2,3]);

-- tests on valid input
-- cells of one stratum three variables, complete group
SELECT * FROM @extschema@.fn_api_get_1pgroups4regtotal(1, ARRAY[44,45,46,47,48,49,50,51,52,53,56,57,58,62], ARRAY[1,2,3]);

-- cells of one stratum, one variable without configuration added to three with configuration making the group incomplete
SELECT * FROM @extschema@.fn_api_get_1pgroups4regtotal(1, ARRAY[44,45,46,47,48,49,50,51,52,53,56,57,58,62], ARRAY[1,2,3,-1]);

-- cells of one stratum, one variable without configuration, no group given (NULL)
SELECT * FROM @extschema@.fn_api_get_1pgroups4regtotal(1, ARRAY[44,45,46,47,48,49,50,51,52,53,56,57,58,62], ARRAY[-1]);
*/
-- </function>

-- <function name="fn_api_get_group4panel_refyearset_combinations" schema="extschema" src="functions/extschema/configuration/fn_api_get_group4panel_refyearset_combinations.sql">
-- Copyright 2017, 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_api_get_group4panel_refyearset_combinations(integer[], integer[])
--DROP FUNCTION @extschema@.fn_api_get_group4panel_refyearset_combinations(integer[], integer[]);
CREATE OR REPLACE FUNCTION @extschema@.fn_api_get_group4panel_refyearset_combinations(_panels integer[], _refyearsets integer[]) 
RETURNS TABLE (
	id INT, 
	label VARCHAR(200),
	label_en VARCHAR(200),
	description TEXT,
	description_en TEXT)
AS
$function$
BEGIN

IF _panels IS NULL THEN
	RAISE EXCEPTION 'fn_api_get_group4panel_refyearset_combinations: Function argument _panels INT[] must not be NULL!';
END IF;

IF _refyearsets IS NULL THEN
	RAISE EXCEPTION 'fn_api_get_group4panel_refyearset_combinations: Function argument _refyearsets INT[] must not be NULL!';
END IF;

-- test on the same length of each array (except estimation_cells)
IF
	array_length(_panels,1) != array_length(_refyearsets,1)
THEN
	RAISE EXCEPTION 'fn_api_get_group4panel_refyearset_combinations: Lengths of arrays of function arguments _panels INT[] and _refyearsets INT[] do not match!';
END IF; 

-- checking input arrays if they do not contain NULL
IF (SELECT array_position(_panels, NULL) IS NOT NULL) THEN
	RAISE EXCEPTION 'fn_api_get_group4panel_refyearset_combinations:: Function argument _panels INT[] must not be an array containing NULL!';
END IF;

IF (SELECT array_position(_refyearsets, NULL) IS NOT NULL) AND (SELECT array_length(array_positions(_refyearsets, NULL),1) != array_length(_refyearsets,1)) THEN
	RAISE EXCEPTION 'fn_api_get_1pgroups4regtotal: Function argument _reyearsets INT[] must contain either NULLs only or NOT NULLs only!';
END IF;

RETURN QUERY EXECUTE ' 
WITH w_groups_agg AS MATERIALIZED (
	SELECT 	
		t1.panel_refyearset_group,
		array_agg(panel ORDER BY panel, reference_year_set) AS panels, 
		array_agg(reference_year_set ORDER BY panel, reference_year_set) AS refyearsets
	FROM 
		@extschema@.t_panel_refyearset_group AS t1
	GROUP BY t1.panel_refyearset_group
)
SELECT 
	t2.id,
	t2.label,
	t2.label_en,
	t2.description,
	t2.description_en
FROM 
	w_groups_agg AS t1
INNER JOIN
	@extschema@.c_panel_refyearset_group AS t2
	ON t1.panel_refyearset_group = t2.id
WHERE 
	t1.panels = $1 AND t1.refyearsets = $2; ' USING _panels, _refyearsets;
END;
$function$
LANGUAGE plpgsql
PARALLEL SAFE;

COMMENT ON FUNCTION @extschema@.fn_api_get_group4panel_refyearset_combinations(integer[],integer[]) IS 
'The function returns the group of panel and reference year sets corresponding  '
'to the input arrays. If there is no such group, the function returns no records. '
'It also finds groups with no reference-year sets attached (NULLs in the '
'function argument _refyearsets) that are used for auxiliary configurations '
'within t_aux_conf. If the input argument _refyearsets contains only NULLs, it '
'must be explicitly cast to ::int[] within the function call.';

/*
-- testing false inputs

-- passing NULL for _panels
SELECT * FROM @extschema@.fn_api_get_group4panel_refyearset_combinations(NULL, ARRAY[2,3,3]);

-- passing NULL for _refyearsets
SELECT * FROM @extschema@.fn_api_get_group4panel_refyearset_combinations(ARRAY[1,2,5], NULL);

-- passing _panels and _refyearsets of unequal length
SELECT * FROM @extschema@.fn_api_get_group4panel_refyearset_combinations(ARRAY[1,2,5], ARRAY[2]);

-- pasisng NULL within _panels
SELECT * FROM @extschema@.fn_api_get_group4panel_refyearset_combinations(ARRAY[1,NULL,5], ARRAY[2,3,3]);

-- pasisng NULL within _refyearsets
SELECT * FROM @extschema@.fn_api_get_group4panel_refyearset_combinations(ARRAY[1,2,5], ARRAY[2,NULL,3]);

-- valid inputs

-- existing group No 13
SELECT * FROM @extschema@.fn_api_get_group4panel_refyearset_combinations(ARRAY[1,5,7], ARRAY[2,3,3]);

-- existing group No 10
SELECT * FROM @extschema@.fn_api_get_group4panel_refyearset_combinations(ARRAY[1,2,5], ARRAY[NULL,NULL,NULL]::int[]);

-- non existing group, no records retrieved
SELECT * FROM @extschema@.fn_api_get_group4panel_refyearset_combinations(ARRAY[1,2,5], ARRAY[2,3,3]);

*/
-- </function>

-- <function name="fn_api_get_panel_refyearset_combinations4groups" schema="extschema" src="functions/extschema/configuration/fn_api_get_panel_refyearset_combinations4groups.sql">
-- Copyright 2017, 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_api_get_panel_refyearset_combinations4groups(integer[])
--DROP FUNCTION @extschema@.fn_api_get_panel_refyearset_combinations4groups(integer[]);

CREATE OR REPLACE FUNCTION @extschema@.fn_api_get_panel_refyearset_combinations4groups(_panel_refyearset_groups INT[])
RETURNS TABLE (panel INT, reference_year_set INT)
AS
$function$
BEGIN

IF _panel_refyearset_groups IS NULL THEN
	RAISE EXCEPTION 'fn_api_get_panel_refyearset_combinations4groups: Function argument __panel_refyearset_groups INT[] must not be NULL!';
END IF;

-- checking _panel_refyearset_groups array if does not contain NULL
IF (SELECT array_position(_panel_refyearset_groups, NULL) IS NOT NULL) THEN
	RAISE EXCEPTION 'fn_api_get_panel_refyearset_combinations4groups:: Function argument _panel_refyearset_groups INT[] must not be an array containing NULL!';
END IF;

RETURN QUERY EXECUTE '
SELECT DISTINCT
	panel,
	reference_year_set
FROM 
	@extschema@.t_panel_refyearset_group
WHERE ARRAY[panel_refyearset_group] <@ $1;' USING _panel_refyearset_groups;
END;
$function$
LANGUAGE plpgsql
PARALLEL SAFE;

COMMENT ON FUNCTION @extschema@.fn_api_get_panel_refyearset_combinations4groups(integer[]) IS 
'The function returns equaly ordered arrays of panels and reference-year sets '
'belonging to any group of panels and reference year combinations passed ' 
'to the function as an argument. Only distinct pairs of panel and reference year '
'sets are returned.';

/*
-- testing false inputs

-- passing NULL for _panel_refyearset_groups
SELECT * FROM @extschema@.fn_api_get_panel_refyearset_combinations4groups(NULL);

-- passing an _panel_refyearset_groups containing NULL
SELECT * FROM @extschema@.fn_api_get_panel_refyearset_combinations4groups(ARRAY[23, 10, NULL]);

-- testing valid inputs

-- group 23
SELECT * FROM @extschema@.fn_api_get_panel_refyearset_combinations4groups(ARRAY[23]);

-- group 10 (no reference year sets)
SELECT * FROM @extschema@.fn_api_get_panel_refyearset_combinations4groups(ARRAY[10]);

-- group 20
SELECT * FROM @extschema@.fn_api_get_panel_refyearset_combinations4groups(ARRAY[20]);

-- groups 20 and 23, note combination 1 (panel) and 2 (refyearset) included only once in the function output
SELECT * FROM @extschema@.fn_api_get_panel_refyearset_combinations4groups(ARRAY[20,23]);

-- non-existing group, no records
SELECT * FROM @extschema@.fn_api_get_panel_refyearset_combinations4groups(ARRAY[-20]);

 -- combination of non-existing and existing group, records of the existing returned
SELECT * FROM @extschema@.fn_api_get_panel_refyearset_combinations4groups(ARRAY[-20, 23]);
*/
-- </function>