--
-- Copyright 2017, 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.

-- <function name="fn_api_find_1pgroups4regtotal" schema="extschema" src="functions/extschema/configuration/fn_api_find_1pgroups4regtotal.sql">
--
-- Copyright 2017, 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_api_find_1pgroups4regtotal(INT, ARRAY[INT], ARRAY[INT])
--DROP FUNCTION @extschema@.fn_api_find_1pgroups4regtotal(INT, INT[], INT[]);

CREATE OR REPLACE FUNCTION @extschema@.fn_api_find_1pgroups4regtotal(_estimation_period INT, _estimation_cells INT[], _variables INT[])
RETURNS TABLE(panel_refyearset_group INT, number_of_1p_estimates BIGINT, all_estimates_included BOOLEAN)
AS
$function$
DECLARE
_number_of_strata_combinations_within_estimation_cells INT;

BEGIN

-- testing input parameters if not NULL
IF _estimation_period IS NULL THEN
	RAISE EXCEPTION 'fn_api_find_1pgroups4regtotal: parameter _estimation_period INT must not be null!';
END IF;

IF _estimation_cells IS NULL THEN
	RAISE EXCEPTION 'fn_api_find_1pgroups4regtotal: parameter _estimation_cells INT[] must not be null!';
END IF;

IF _variables IS NULL THEN
	RAISE EXCEPTION 'fn_api_find_1pgroups4regtotal: parameter _variables INT[] must not be null!';
END IF;

-- checking input arrays if they do not contain NULL
IF (SELECT array_position(_estimation_cells, NULL) IS NOT NULL) THEN
	RAISE EXCEPTION 'fn_api_find_1pgroups4regtotal: parameter _estimation_cells INT[] must not be an array containing NULL!';
END IF;

IF (SELECT array_position(_variables, NULL) IS NOT NULL) THEN
	RAISE EXCEPTION 'fn_api_find_1pgroups4regtotal: parameter _variables INT[] must not be an array containing NULL!';
END IF;

-- checking one strata combination within input parameter _estimation_cells
WITH w_strata_combinations AS MATERIALIZED (
SELECT 
	array_agg(stratum ORDER BY stratum) AS strata_combination 
FROM 
	@extschema@.t_stratum_in_estimation_cell 
WHERE 
	ARRAY[estimation_cell] <@   _estimation_cells  -- ARRAY[44,45,46,47,48,49,50,51,52,53,56,57,58,59,62]  -- ARRAY[44,45,46,47,48,49,50,51,52,53,56,57,58,62] 
GROUP BY estimation_cell
)
SELECT 
	count(DISTINCT strata_combination) 
FROM
	w_strata_combinations
INTO _number_of_strata_combinations_within_estimation_cells;

IF _number_of_strata_combinations_within_estimation_cells IS DISTINCT FROM 1 THEN
	RAISE EXCEPTION 'fn_api_find_1pgroups4regtotal: parameter _estimation_cells INT[] must contain cells corresponding to one and the only sampling strata combination!';
END IF;

-- checking one cell collection within input parameter _estimation_cells 
IF (SELECT count(DISTINCT estimation_cell_collection) FROM @extschema@.c_estimation_cell WHERE ARRAY[id] <@ _estimation_cells) IS DISTINCT FROM 1 THEN 
	RAISE EXCEPTION 'fn_api_find_1pgroups4regtotal: parameter _estimation_cells INT[] must contain cells corresponding to one and the only estimation cell collection!';
END IF;


RETURN QUERY EXECUTE '
WITH w_reg2conf AS (
	SELECT 
		$1 AS estimation_period,
		t1.estimation_cell,
		t2.variable
	FROM
	  (SELECT unnest($2) AS estimation_cell) AS t1
	 CROSS JOIN 
	  (SELECT unnest($3) AS variable) AS t2
)
SELECT 
	t1.panel_refyearset_group,
	count(*) AS number_of_1p_estimates,
	count(*) = (SELECT count(*) FROM w_reg2conf) AS all_estimates_included
FROM 
	@extschema@.t_total_estimate_conf AS t1
INNER JOIN 
	w_reg2conf AS t2
ON 
	t1.estimation_period = t2.estimation_period AND
	t1.estimation_cell = t2.estimation_cell AND
	t1.variable = t2.variable AND
	t1.phase_estimate_type = 1
GROUP BY t1.panel_refyearset_group;' USING _estimation_period, _estimation_cells, _variables;
END;
$function$
LANGUAGE plpgsql
PARALLEL SAFE;

COMMENT ON FUNCTION  @extschema@.fn_api_find_1pgroups4regtotal(INT, INT[], INT[]) IS 
'Function retrieves ids of groups of panel and regerence-year set combinations '
'with number of 1p estimates and a Bollean value indicating if all regression '
'estimates to be configured have a mathing single-phase estimate configured with '
'this group. ' ;

/* tests
-- test NULL for _estimation_period argument
SELECT * FROM @extschema@.fn_api_find_1pgroups4regtotal(NULL, ARRAY[44,45,46,47,48,49,50,51,52,53,56,57,58,62], ARRAY[1,2,3])

-- test NULL for _estimation_cell argument
SELECT * FROM @extschema@.fn_api_find_1pgroups4regtotal(1, NULL, ARRAY[1,2,3])

-- test NULL for _estimation_cell argument
SELECT * FROM @extschema@.fn_api_find_1pgroups4regtotal(1, ARRAY[44,45,46,47,48,49,50,51,52,53,56,57,58,62], NULL)

-- test for NULL as an element of _estimation_cell argument
SELECT * FROM @extschema@.fn_api_find_1pgroups4regtotal(1, ARRAY[44,45,46,47,NULL,49,50,51,52,53,56,57,58,62], ARRAY[1,2,3])
SELECT * FROM @extschema@.fn_api_find_1pgroups4regtotal(1, ARRAY[NULL], ARRAY[1,2,3])
SELECT * FROM @extschema@.fn_api_find_1pgroups4regtotal(1, ARRAY[NULL]::int[], ARRAY[1,2,3])

-- test for NULL as an element of _variable argument
SELECT * FROM @extschema@.fn_api_find_1pgroups4regtotal(1, ARRAY[44,45,46,47,48,49,50,51,52,53,56,57,58,62], ARRAY[NULL,2,NULL])
SELECT * FROM @extschema@.fn_api_find_1pgroups4regtotal(1, ARRAY[44,45,46,47,48,49,50,51,52,53,56,57,58,62], ARRAY[NULL])
SELECT * FROM @extschema@.fn_api_find_1pgroups4regtotal(1, ARRAY[44,45,46,47,48,49,50,51,52,53,56,57,58,62], ARRAY[NULL]::int[])

-- test if estimation cells belong to one estimation cell collection (adding 1 to the valid array _estimation_cells, 1 
-- belongs to another collection but the same stratum
SELECT * FROM @extschema@.fn_api_find_1pgroups4regtotal(1, ARRAY[1,44,45,46,47,48,49,50,51,52,53,56,57,58,62], ARRAY[1,2,3]);

-- test if estimation cells belong to one and the only combination of sampling strata (typically to one sampling strata only)
-- the cells of valid input belong to one stratum only, but here we add cell No 59 which intersects one more stratum
SELECT * FROM @extschema@.fn_api_find_1pgroups4regtotal(1, ARRAY[44,45,46,47,48,49,50,51,52,53,56,57,58,59,62], ARRAY[1,2,3]);

-- test on valid input
SELECT * FROM @extschema@.fn_api_find_1pgroups4regtotal(1, ARRAY[44,45,46,47,48,49,50,51,52,53,56,57,58,62], ARRAY[1,2,3])
*/
-- </function>