-- Copyright 2017, 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.

-- <function name="fn_api_save_single_phase_total_config" schema="extschema" src="functions/extschema/configuration/fn_api_save_single_phase_total_config.sql">
--
-- Copyright 2017, 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: nfiesta.fn_api_save_single_phase_total_config(INT, INT, INT, INT, INT[])

-- DROP FUNCTION nfiesta.fn_api_save_single_phase_total_config(INT, INT, INT, INT, INT[]);

CREATE OR REPLACE FUNCTION nfiesta.fn_api_save_single_phase_total_config(_estimation_period INT, _estimation_cell INT, _variable INT,  
	_aggregate_panel_refyearset_group INT, _panel_refyearset_groups_by_strata INT[])
RETURNS INT
AS
$function$
DECLARE 
_total_estimate_conf INT;

BEGIN
-- testing input parameters if not NULL
IF _estimation_period IS NULL THEN
	RAISE EXCEPTION 'fn_api_save_single_phase_total_config: Function argument _estimation_period INT must not be NULL!';
END IF;	
	
IF _estimation_cell IS NULL THEN
	RAISE EXCEPTION 'fn_api_save_single_phase_total_config: Function argument _estimation_cell INT must not be NULL!';
END IF;

IF _variable IS NULL THEN
	RAISE EXCEPTION 'fn_api_save_single_phase_total_config: Function argument _variable INT must not be NULL!';
END IF;

IF _aggregate_panel_refyearset_group IS NULL THEN
	RAISE EXCEPTION 'fn_api_save_single_phase_total_config: Function argument _aggregate_panel_refyearset_group INT must not be NULL!';
END IF;

IF _panel_refyearset_groups_by_strata IS NULL THEN
	RAISE EXCEPTION 'fn_api_save_single_phase_total_config: Function argument _panel_refyearset_groups_by_strata INT must not be NULL!';
END IF;

-- checking if _panel_refyearset_groups_by_strata does not contain NULL
IF (SELECT array_position(_panel_refyearset_groups_by_strata, NULL) IS NOT NULL) THEN
	RAISE EXCEPTION 'fn_api_save_single_phase_total_config: Function argument _panel_refyearset_groups_by_strata INT[] must not be an array containing NULL!';
END IF;

SELECT id FROM nfiesta.t_total_estimate_conf WHERE 
	estimation_cell = _estimation_cell AND 
	estimation_period = _estimation_period AND 
	variable = _variable AND 
	phase_estimate_type = 1 AND
	panel_refyearset_group = _aggregate_panel_refyearset_group
INTO _total_estimate_conf; 
	
IF _total_estimate_conf IS NULL THEN 
	-- insert single phase total configurations into t_total_estimate_conf and t_estimate_conf by calling an internal non-api function, the function returns id of 
	-- of the nwe record in nfiesta.t_total_estimate_conf
	SELECT nfiesta.fn_1p_est_configuration(_aggregate_panel_refyearset_group, _estimation_cell, _estimation_period, 'API-GUI configuration', _variable) INTO _total_estimate_conf;
ELSE 
	RAISE WARNING 'fn_api_save_single_phase_total_config: An identical configuration of single-phase estimate exists for the estimation cell (id = %) and variable (id = %)', _estimation_cell, _variable;
END IF;

UPDATE nfiesta.t_total_estimate_conf 
SET panel_refyearset_groups_by_strata = _panel_refyearset_groups_by_strata
WHERE id = _total_estimate_conf;

RETURN _total_estimate_conf;

END;
$function$
LANGUAGE plpgsql
PARALLEL SAFE;

COMMENT ON FUNCTION nfiesta.fn_api_save_single_phase_total_config(INT, INT, INT, INT, INT[]) IS 
'The function saves configuration of a single phase total estimator into tables nfiesta.t_total_estimate_conf and nfiesta.t_estimate_conf.'
'It is a wrapper function that internally callls nfiesta.fn_1p_est_configuration function.';

/* tests
-------------------------------------------------------------------------------------------------------------------------------------------------------
-- tests for function nfiesta.fn_api_save_single_phase_total_config
-------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------- testing invalid inputs ------------------------------------------------------------------------------
-- test NULL for _estimation_period argument
SELECT * FROM nfiesta.fn_api_save_single_phase_total_config(NULL,1,3,14, ARRAY[14]);

-- test NULL for _estimation_cell argument
SELECT * FROM nfiesta.fn_api_save_single_phase_total_config(1,NULL,3,14, ARRAY[14]);

-- test NULL for _variable argument
SELECT * FROM nfiesta.fn_api_save_single_phase_total_config(1,1,NULL,14, ARRAY[14]);

-- test NULL for _panel_refyearset_group argument
SELECT * FROM nfiesta.fn_api_save_single_phase_total_config(1,1,3,NULL, ARRAY[14]);

-- test NULL for _panel_refyearset_groups_by_strata argument
SELECT * FROM nfiesta.fn_api_save_single_phase_total_config(1, 1, 3, 14, NULL);
SELECT * FROM nfiesta.fn_api_save_single_phase_total_config(1, 1, 3, 14, ARRAY[NULL::int]);

------------------------------------------------ testing valid inputs ---------------------------------------------------------------------------------
-- first delete the existing configurations and results
DELETE FROM nfiesta.t_result WHERE id IN (3, 511);
DELETE FROM nfiesta.t_estimate_conf WHERE id IN (3, 511);
DELETE FROM nfiesta.t_total_estimate_conf WHERE id IN (3, 728);

-- save a configuration of single phase total
SELECT * FROM nfiesta.fn_api_save_single_phase_total_config(1,1,3,14, ARRAY[14]);

-- trying to save the same single-phase config the second time suceeds but raises a warning (the insert to t_total_estimate_conf is actually skipped)
SELECT * FROM nfiesta.fn_api_save_single_phase_total_config(1,1,3,14, ARRAY[14]);

------------------------------------------------ helping code ---------------------------------------------------------------------------------
-- helping code
SELECT * FROM nfiesta.t_total_estimate_conf WHERE estimation_cell = 1 AND variable = 3; -- estimation period 1, panel refyearset group 14

SELECT * FROM nfiesta.t_estimate_conf WHERE total_estimate_conf = 728 -- 1654 -- AND denominator IS NULL;

SELECT * FROM nfiesta.t_result WHERE estimate_conf = 1654 -- 3;
SELECT * FROM nfiesta.t_result WHERE estimate_conf = 1654;

DELETE FROM nfiesta.t_result WHERE id IN (3, 511);
DELETE FROM nfiesta.t_estimate_conf WHERE id IN (1655) -- (3, 511);
DELETE FROM nfiesta.t_total_estimate_conf WHERE id = 728;
*/
-- </function>


