--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-------------------------------------------------COPY DYNAMIC DATA--------------------------------
insert into sdesign.c_country				select * from @extschema@.c_country order by id;
insert into sdesign.t_strata_set			select * from @extschema@.t_strata_set order by id;
insert into sdesign.t_stratum				select * from @extschema@.t_stratum order by id;
insert into sdesign.t_cluster_configuration		select * from @extschema@.t_cluster_configuration order by id;
insert into sdesign.t_panel				select * from @extschema@.t_panel order by id;

insert into sdesign.t_cluster				select * from @extschema@.t_cluster order by id;
insert into sdesign.f_p_plot				select * from @extschema@.f_p_plot order by gid;
insert into sdesign.cm_cluster2panel_mapping		select * from @extschema@.cm_cluster2panel_mapping order by id;
insert into sdesign.cm_plot2cluster_config_mapping	select * from @extschema@.cm_plot2cluster_config_mapping order by id;

insert into sdesign.t_inventory_campaign		select * from @extschema@.t_inventory_campaign order by id;
insert into sdesign.t_reference_year_set		select * from @extschema@.t_reference_year_set order by id;
insert into sdesign.t_plot_measurement_dates		select * from @extschema@.t_plot_measurement_dates order by id;
insert into sdesign.cm_refyearset2panel_mapping		select * from @extschema@.cm_refyearset2panel_mapping order by id;

-------------------------------------------------ADJUST FOREIGN KEYS--------------------------------
-------------------------------------------------f_p_plot
--fkey__cm_plot2cell_mapping__f_p_plot
alter table @extschema@.cm_plot2cell_mapping drop constraint fkey__cm_plot2cell_mapping__f_p_plot;
alter table @extschema@.cm_plot2cell_mapping add constraint fkey__cm_plot2cell_mapping__f_p_plot
	FOREIGN KEY (plot) REFERENCES sdesign.f_p_plot (gid);
--fkey__cm_plot2param_area_mapping__f_p_plot
alter table @extschema@.cm_plot2param_area_mapping drop constraint fkey__cm_plot2param_area_mapping__f_p_plot;
alter table @extschema@.cm_plot2param_area_mapping add constraint fkey__cm_plot2param_area_mapping__f_p_plot
	FOREIGN KEY (plot) REFERENCES sdesign.f_p_plot (gid);
--fkey__t_auxiliary_data__f_p_plot
alter table @extschema@.t_auxiliary_data drop constraint fkey__t_auxiliary_data__f_p_plot;
alter table @extschema@.t_auxiliary_data add constraint fkey__t_auxiliary_data__f_p_plot
	FOREIGN KEY (plot) REFERENCES sdesign.f_p_plot (gid);
--fkey__t_target_data__f_p_plot
alter table @extschema@.t_target_data drop constraint fkey__t_target_data__f_p_plot;
alter table @extschema@.t_target_data add constraint fkey__t_target_data__f_p_plot
	FOREIGN KEY (plot) REFERENCES sdesign.f_p_plot (gid);
-------------------------------------------------t_cluster
--fkey__t_g_beta__t_cluster
alter table @extschema@.t_g_beta drop constraint fkey__t_g_beta__t_cluster;
alter table @extschema@.t_g_beta add constraint fkey__t_g_beta__t_cluster
	FOREIGN KEY (cluster) REFERENCES sdesign.t_cluster (id);
-------------------------------------------------t_panel
--fkey__t_panel2aux_conf__t_panel
alter table @extschema@.t_panel2aux_conf drop constraint fkey__t_panel2aux_conf__t_panel;
alter table @extschema@.t_panel2aux_conf add constraint fkey__t_panel2aux_conf__t_panel
	FOREIGN KEY (panel) REFERENCES sdesign.t_panel (id);
--fkey__t_panel2total_1stph_est_data__t_panel
alter table @extschema@.t_panel2total_1stph_estimate_conf drop constraint fkey__t_panel2total_1stph_est_data__t_panel;
alter table @extschema@.t_panel2total_1stph_estimate_conf add constraint fkey__t_panel2total_1stph_est_data__t_panel
	FOREIGN KEY (panel) REFERENCES sdesign.t_panel (id);
--fkey__t_panel2total_2ndph_est_data__t_panel
alter table @extschema@.t_panel2total_2ndph_estimate_conf drop constraint fkey__t_panel2total_2ndph_est_data__t_panel;
alter table @extschema@.t_panel2total_2ndph_estimate_conf add constraint fkey__t_panel2total_2ndph_est_data__t_panel
	FOREIGN KEY (panel) REFERENCES sdesign.t_panel (id);
--t_available_datasets_panel_fkey
alter table @extschema@.t_available_datasets drop constraint t_available_datasets_panel_fkey;
alter table @extschema@.t_available_datasets add constraint fkey__t_available_datasets__t_panel
	FOREIGN KEY (panel) REFERENCES sdesign.t_panel (id);
-------------------------------------------------t_stratum
--fkey__t_stratum_in_estimation_cell__t_stratum
alter table @extschema@.t_stratum_in_estimation_cell drop constraint fkey__t_stratum_in_estimation_cell__t_stratum;
alter table @extschema@.t_stratum_in_estimation_cell add constraint fkey__t_stratum_in_estimation_cell__t_stratum
	FOREIGN KEY (stratum) REFERENCES sdesign.t_stratum (id);
-------------------------------------------------t_reference_year_set
--fkey__t_panel2aux_conf__t_reference_year_set
alter table @extschema@.t_panel2aux_conf drop constraint fkey__t_panel2aux_conf__t_reference_year_set;
alter table @extschema@.t_panel2aux_conf add constraint fkey__t_panel2aux_conf__t_reference_year_set
	FOREIGN KEY (reference_year_set) REFERENCES sdesign.t_reference_year_set (id);
--fkey__t_panel2total_1stph_est_data__t_reference_year_set
alter table @extschema@.t_panel2total_1stph_estimate_conf drop constraint fkey__t_panel2total_1stph_est_data__t_reference_year_set;
alter table @extschema@.t_panel2total_1stph_estimate_conf add constraint fkey__t_panel2total_1stph_est_data__t_reference_year_set
	FOREIGN KEY (reference_year_set) REFERENCES sdesign.t_reference_year_set (id);
--fkey__t_panel2total_2ndph_est_data__t_reference_year_set
alter table @extschema@.t_panel2total_2ndph_estimate_conf drop constraint fkey__t_panel2total_2ndph_est_data__t_reference_year_set;
alter table @extschema@.t_panel2total_2ndph_estimate_conf add constraint fkey__t_panel2total_2ndph_est_data__t_reference_year_set
	FOREIGN KEY (reference_year_set) REFERENCES sdesign.t_reference_year_set (id);
--fkey__t_target_data__t_reference_year_set
alter table @extschema@.t_target_data drop constraint fkey__t_target_data__t_reference_year_set;
alter table @extschema@.t_target_data add constraint fkey__t_target_data__t_reference_year_set
	FOREIGN KEY (reference_year_set) REFERENCES sdesign.t_reference_year_set (id);
--t_available_datasets_reference_year_set_fkey
alter table @extschema@.t_available_datasets drop constraint t_available_datasets_reference_year_set_fkey;
alter table @extschema@.t_available_datasets add constraint t_available_datasets_reference_year_set_fkey
	FOREIGN KEY (reference_year_set) REFERENCES sdesign.t_reference_year_set (id);

-------------------------------------------------ADJUST FUNCTIONS AND VIEWS------------------------

-- <function_name="" function_schema="extschema" src="functions/extschema/additivity/fn_add_plot_aux_attr.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_add_plot_aux_attr(integer)

-- DROP FUNCTION @extschema@.fn_add_plot_aux_attr(integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_add_plot_aux_attr(
	IN variables integer[],
	IN min_diff double precision default 0.0, IN include_null_diff boolean default true
)
  RETURNS TABLE(
	plot			integer,
	variable		integer,
	ldsity			double precision,
	ldsity_sum		double precision,
	variables_def		integer[],
	variables_found		integer[],
	diff			double precision
) AS
$BODY$
DECLARE
	_complete_query text;
	_array_text text;
BEGIN
	--------------------------------QUERY--------------------------------
	_array_text := concat(quote_literal(variables::text), '::integer[]');
	--raise notice 'variables: %',  _array_text;
	_complete_query := '
with w_plot_var as not materialized (
	select
		f_p_plot.gid as plot, t_available_datasets.variable, t_available_datasets.reference_year_set,
		coalesce(t_auxiliary_data.value, 0) as value
	from sdesign.f_p_plot
	inner join sdesign.t_cluster on (f_p_plot.cluster = t_cluster.id)
	inner join sdesign.cm_cluster2panel_mapping on (t_cluster.id = cm_cluster2panel_mapping.cluster)
	inner join sdesign.t_panel on (cm_cluster2panel_mapping.panel = t_panel.id)
	inner join @extschema@.t_available_datasets on (t_panel.id = t_available_datasets.panel)
	inner join @extschema@.t_variable on (t_available_datasets.variable = t_variable.id and t_variable.auxiliary_variable_category is not null)
	left join @extschema@.t_auxiliary_data on (
		f_p_plot.gid = t_auxiliary_data.plot
		and t_available_datasets.variable = t_auxiliary_data.variable
		and t_auxiliary_data.is_latest)
	WHERE t_available_datasets.variable = ANY (' || _array_text || ')

)
, w_node_sum as (
	select
		plot,

		w_plot_var.variable,
		value as node_sum,
		v_variable_hierarchy.node,
		v_variable_hierarchy.edges as edges_def
	from w_plot_var
	inner join @extschema@.v_variable_hierarchy on (v_variable_hierarchy.node = w_plot_var.variable)
)
, w_edge_sum as (
	select
		w_node_sum.plot,

		w_node_sum.node,
		w_node_sum.node_sum,
		w_node_sum.edges_def,
		array_agg(w_plot_var.variable order by w_plot_var.variable) as edges_found,
		sum(w_plot_var.value) as edges_sum
	from w_node_sum
	left join w_plot_var on (
		w_plot_var.plot = w_node_sum.plot

		and w_plot_var.variable = any(w_node_sum.edges_def))
	group by w_node_sum.plot,
		w_node_sum.node, w_node_sum.node_sum, w_node_sum.edges_def
)
, w_diff as (
	select
		plot,

		node		as variable,
		node_sum	as ldsity,
		edges_sum	as ldsity_sum,
		edges_def	as variables_def,
		edges_found	as variables_found,
		case
			when node_sum != 0.0 and edges_sum = 0.0 then 100.0
			when node_sum = 0.0 and edges_sum = 0.0 then 0
			else abs(1 - (node_sum / edges_sum )) * 100.0
		end as diff
	from w_edge_sum
)
select * from w_diff
where
	(' || include_null_diff || ' and ((diff >= ' || min_diff || ') or (diff is null)))
	or
	(not ' || include_null_diff || ' and (diff >= ' || min_diff || '))
order by diff desc
;
';

	--RAISE NOTICE '%', _complete_query;
	RETURN QUERY EXECUTE _complete_query;
END;
$BODY$
  LANGUAGE plpgsql STABLE
  COST 100
  ROWS 100000;

comment on function @extschema@.fn_add_plot_aux_attr(integer[], double precision, boolean) is
'Function showing plot level auxiliary local density attribute additivity (aggregated class estimate should be equal to sum of sub-classes estimates) of auxiliary local densities. Hierarchy between aggregated classes and its sub-classes is defined in v_variable_hierarchy.
Function input argument is:
 * Array of attributes -- variables (FKEY to t_variable.id). All variables to be checked (aggregated class together with sub-classes) need to be passed.
 * Minimal amount of difference [%].
 * Whether include NULL difference -- indicating defined sub-classes missing in data.
Resulting table has following columns:
 * Plot. FKEY to f_p_plot.gid.
 * Auxiliary total attribute -- variable. FKEY to t_variable.id.
 * Aggregated class local density.
 * Sum of sub-classes local densities (belonging to aggregated class).
 * Attributes -- variables defined in hierarchy (v_variable_hierarchy). Array of FKEYs to t_variable.id.
 * Attributes -- variables found in data (t_auxiliary_data). Array of FKEYs to t_variable.id.
 * Relative difference between aggregated class local densities and sum of sub-classes local densities.';

GRANT EXECUTE ON FUNCTION @extschema@.fn_add_plot_aux_attr TO PUBLIC;

-- </function>
-- <function_name="" function_schema="extschema" src="functions/extschema/additivity/fn_add_plot_target_attr.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_add_plot_target_attr(integer)

-- DROP FUNCTION @extschema@.fn_add_plot_target_attr(integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_add_plot_target_attr(
	IN variables integer[],
	IN min_diff double precision default 0.0, IN include_null_diff boolean default true
)
  RETURNS TABLE(
	plot			integer,
	reference_year_set	integer,
	variable		integer,
	ldsity			double precision,
	ldsity_sum		double precision,
	variables_def		integer[],
	variables_found		integer[],
	diff			double precision
) AS
$BODY$
DECLARE
	_complete_query text;
	_array_text text;
BEGIN
	--------------------------------QUERY--------------------------------
	_array_text := concat(quote_literal(variables::text), '::integer[]');
	--raise notice 'variables: %',  _array_text;
	_complete_query := '
with w_plot_var as not materialized (
	select
		f_p_plot.gid as plot, t_available_datasets.variable, t_available_datasets.reference_year_set,
		coalesce(t_target_data.value, 0) as value
	from sdesign.f_p_plot
	inner join sdesign.t_cluster on (f_p_plot.cluster = t_cluster.id)
	inner join sdesign.cm_cluster2panel_mapping on (t_cluster.id = cm_cluster2panel_mapping.cluster)
	inner join sdesign.t_panel on (cm_cluster2panel_mapping.panel = t_panel.id)
	inner join @extschema@.t_available_datasets on (t_panel.id = t_available_datasets.panel)
	inner join @extschema@.t_variable on (t_available_datasets.variable = t_variable.id and t_variable.auxiliary_variable_category is null)
	left join @extschema@.t_target_data on (
		f_p_plot.gid = t_target_data.plot
		and t_available_datasets.variable = t_target_data.variable
		and t_available_datasets.reference_year_set = t_target_data.reference_year_set
		and t_target_data.is_latest)
	WHERE t_available_datasets.variable = ANY (' || _array_text || ')
)
, w_node_sum as (
	select
		plot,
		w_plot_var.reference_year_set,
		w_plot_var.variable,
		value as node_sum,
		v_variable_hierarchy.node,
		v_variable_hierarchy.edges as edges_def
	from w_plot_var
	inner join @extschema@.v_variable_hierarchy on (v_variable_hierarchy.node = w_plot_var.variable)
)
, w_edge_sum as (
	select
		w_node_sum.plot,
		w_node_sum.reference_year_set,
		w_node_sum.node,
		w_node_sum.node_sum,
		w_node_sum.edges_def,
		array_agg(w_plot_var.variable order by w_plot_var.variable) as edges_found,
		sum(w_plot_var.value) as edges_sum
	from w_node_sum
	left join w_plot_var on (
		w_plot_var.plot = w_node_sum.plot
		and w_plot_var.reference_year_set = w_node_sum.reference_year_set
		and w_plot_var.variable = any(w_node_sum.edges_def))
	group by w_node_sum.plot, w_node_sum.reference_year_set,
		w_node_sum.node, w_node_sum.node_sum, w_node_sum.edges_def
)
, w_diff as (
	select
		plot,
		reference_year_set,
		node		as variable,
		node_sum	as ldsity,
		edges_sum	as ldsity_sum,
		edges_def	as variables_def,
		edges_found	as variables_found,
		case
			when node_sum != 0.0 and edges_sum = 0.0 then 100.0
			when node_sum = 0.0 and edges_sum = 0.0 then 0
			else abs(1 - (node_sum / edges_sum )) * 100.0
		end as diff
	from w_edge_sum
)
select * from w_diff
where
	(' || include_null_diff || ' and ((diff >= ' || min_diff || ') or (diff is null)))
	or
	(not ' || include_null_diff || ' and (diff >= ' || min_diff || '))
order by diff desc
;
';

	--RAISE NOTICE '%', _complete_query;
	RETURN QUERY EXECUTE _complete_query;
END;
$BODY$
  LANGUAGE plpgsql STABLE
  COST 100
  ROWS 100000;

comment on function @extschema@.fn_add_plot_target_attr(integer[], double precision, boolean) is
'Function showing plot level target local density attribute additivity (aggregated class estimate should be equal to sum of sub-classes estimates) of target local densities. Hierarchy between aggregated classes and its sub-classes is defined in v_variable_hierarchy.
Function input argument is:
 * Array of attributes -- variables (FKEY to t_variable.id). All variables to be checked (aggregated class together with sub-classes) need to be passed.
 * Minimal amount of difference [%].
 * Whether include NULL difference -- indicating defined sub-classes missing in data.
Resulting table has following columns:
 * Plot. FKEY to f_p_plot.gid.
 * Reference year set. FKEY to t_reference_year_set.id.
 * Target total attribute -- variable. FKEY to t_variable.id.
 * Aggregated class local density.
 * Sum of sub-classes local densities (belonging to aggregated class).
 * Attributes -- variables defined in hierarchy (v_variable_hierarchy). Array of FKEYs to t_variable.id.
 * Attributes -- variables found in data (t_auxiliary_data). Array of FKEYs to t_variable.id.
 * Relative difference between aggregated class local densities and sum of sub-classes local densities.';

GRANT EXECUTE ON FUNCTION @extschema@.fn_add_plot_target_attr TO PUBLIC;

-- </function>
-- <function_name="" function_schema="extschema" src="functions/extschema/additivity/fn_add_res_ratio_attr.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_add_res_ratio_attr(integer)

-- DROP FUNCTION @extschema@.fn_add_res_ratio_attr(integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_add_res_ratio_attr(
	IN est_confs integer[],
	IN min_diff double precision default 0.0, IN include_null_diff boolean default true
)
  RETURNS TABLE(
	estimation_cell		integer,
	aux_conf		integer,
	force_synthetic		boolean,
	estimate_conf		integer,
	denominator		integer,
	variable		integer,
	point_est		double precision,
	point_est_sum		double precision,
	variables_def		integer[],
	variables_found		integer[],
	estimate_confs_found	integer[],
	diff			double precision
) AS
$BODY$
DECLARE
	_complete_query text;
	_array_text text;
BEGIN
	--------------------------------QUERY--------------------------------
	_array_text := concat(quote_literal(est_confs::text), '::integer[]');
	--raise notice 'est_confs: %',  _array_text;
	_complete_query := '
with w_res_cell_var as not materialized (
	select
		t_result.estimate_conf,
		t_result.point,
		t_total_estimate_conf.estimation_cell,
		t_total_estimate_conf.target_variable as t_variable__id,
		t_total_estimate_conf.estimate_date_begin, t_total_estimate_conf.estimate_date_end, 
		array_agg(t_panel2total_2ndph_estimate_conf.panel order by panel) as t_panel2total_2ndph_estimate_conf_panels,
		t_total_estimate_conf.aux_conf, force_synthetic, t_estimate_conf.denominator
	from @extschema@.t_result
	inner join @extschema@.t_estimate_conf ON t_estimate_conf.id = t_result.estimate_conf
	inner join @extschema@.t_total_estimate_conf ON t_total_estimate_conf.id = t_estimate_conf.total_estimate_conf
	inner join @extschema@.t_panel2total_2ndph_estimate_conf ON t_panel2total_2ndph_estimate_conf.total_estimate_conf = t_total_estimate_conf.id
	where (t_estimate_conf.estimate_type = 2)
	and (t_result.is_latest)
	and (t_result.estimate_conf = ANY (' || _array_text || '))
	group by 
		t_result.estimate_conf,
		t_result.point,
		t_total_estimate_conf.estimation_cell,
		t_total_estimate_conf.target_variable,
		t_total_estimate_conf.estimate_date_begin, t_total_estimate_conf.estimate_date_end,
		t_total_estimate_conf.aux_conf, force_synthetic, t_estimate_conf.denominator
)
, w_node_sum as (
	select
		estimation_cell,
		w_res_cell_var.t_variable__id, aux_conf, force_synthetic, denominator,
		estimate_date_begin, estimate_date_end, t_panel2total_2ndph_estimate_conf_panels,
		estimate_conf as node_estimate_confs,
		sum(point) as node_sum,
		hierarchy.node,
		hierarchy.edges as edges_def
	from w_res_cell_var
	inner join @extschema@.v_variable_hierarchy 	as hierarchy on (hierarchy.node = w_res_cell_var.t_variable__id)
	group by estimation_cell, w_res_cell_var.t_variable__id, aux_conf, force_synthetic, estimate_date_begin, estimate_date_end, t_panel2total_2ndph_estimate_conf_panels, estimate_conf, node, edges_def, denominator
	order by estimation_cell, w_res_cell_var.t_variable__id, aux_conf, force_synthetic, estimate_date_begin, estimate_date_end, t_panel2total_2ndph_estimate_conf_panels, estimate_conf, node, edges_def, denominator
)
, w_edge_sum as (
	select
		w_node_sum.estimation_cell,
		w_node_sum.aux_conf,
		w_node_sum.force_synthetic,
		w_node_sum.node_estimate_confs, w_node_sum.denominator,
		w_node_sum.estimate_date_begin, w_node_sum.estimate_date_end, w_node_sum.t_panel2total_2ndph_estimate_conf_panels,
		w_node_sum.node,
		w_node_sum.node_sum,
		w_node_sum.edges_def,
		array_agg(w_res_cell_var.t_variable__id order by w_res_cell_var.t_variable__id) as edges_found,
		array_agg(w_res_cell_var.estimate_conf order by w_res_cell_var.estimate_conf) as edges_estimate_confs,
		sum(w_res_cell_var.point) as edges_sum
	from w_node_sum
	left join w_res_cell_var on (
		w_res_cell_var.estimation_cell = w_node_sum.estimation_cell
		and w_res_cell_var.estimate_date_begin = w_node_sum.estimate_date_begin
		and w_res_cell_var.estimate_date_end = w_node_sum.estimate_date_end
		and w_res_cell_var.t_panel2total_2ndph_estimate_conf_panels = w_node_sum.t_panel2total_2ndph_estimate_conf_panels
		and case when
			w_res_cell_var.aux_conf is null and w_node_sum.aux_conf is null then true
			else w_res_cell_var.aux_conf = w_node_sum.aux_conf end
		and case when
			w_res_cell_var.force_synthetic is null and w_node_sum.force_synthetic is null then true
			else w_res_cell_var.force_synthetic = w_node_sum.force_synthetic end
		and w_res_cell_var.t_variable__id = any(w_node_sum.edges_def)) and w_res_cell_var.denominator = w_node_sum.denominator
	group by w_node_sum.estimation_cell, 
		w_node_sum.aux_conf, w_node_sum.force_synthetic, w_node_sum.node_estimate_confs, w_node_sum.denominator,
		w_node_sum.estimate_date_begin, w_node_sum.estimate_date_end, w_node_sum.t_panel2total_2ndph_estimate_conf_panels,
		w_node_sum.node, w_node_sum.node_sum, w_node_sum.edges_def
)
, w_diff as (
	select
		estimation_cell,
		aux_conf,
		force_synthetic,
		node_estimate_confs	as estimate_conf, denominator,
		node			as variable,
		node_sum		as point_est,
		edges_sum		as point_est_sum,
		edges_def		as variables_def,
		edges_found		as variables_found,
		edges_estimate_confs	as estimate_confs_found,
		case
			when node_sum != 0.0 and edges_sum = 0.0 then 100.0
			when node_sum = 0.0 and edges_sum = 0.0 then 0
			else abs(1 - (node_sum / edges_sum )) * 100.0
		end as diff
	from w_edge_sum
)
select * from w_diff
where
	(' || include_null_diff || ' and ((diff >= ' || min_diff || ') or (diff is null)))
	or
	(not ' || include_null_diff || ' and (diff >= ' || min_diff || '))
order by diff desc
;
';

	--RAISE NOTICE '%', _complete_query;
	RETURN QUERY EXECUTE _complete_query;
END;
$BODY$
  LANGUAGE plpgsql STABLE
  COST 100
  ROWS 100000;

comment on function @extschema@.fn_add_res_ratio_attr(integer[], double precision, boolean) is
'Function showing ratio estimates attribute additivity (aggregated class estimate should be equal to sum of sub-classes estimates). Hierarchy between aggregated classes and its sub-classes is defined in v_variable_hierarchy.
Function input argument is:
 * Array of attributes -- variables (FKEY to t_variable.id). All variables to be checked (aggregated class together with sub-classes) need to be passed.
 * Minimal amount of difference [%].
 * Whether include NULL difference -- indicating defined sub-classes missing in data.
Resulting table has following columns:
 * Estimate estimation cell. FKEY to c_estimation_cell.id.
 * Estimate auxiliary configuration. FKEY to t_aux_conf.id.
 * Parameter showing whether estimate is forced to be synthetic.
 * Estimate configuration id. FKEY to t_estimate_conf.id.
 * Estimate configuration id of denominator. FKEY to t_estimate_conf.id.
 * Estimate attribute -- variable. FKEY to t_variable.id.
 * Aggregated class point estimate.
 * Sum of sub-classes point estimates (belonging to aggregated class).
 * Attributes -- variables defined in hierarchy (v_variable_hierarchy). Array of FKEYs to t_variable.id.
 * Attributes -- variables found in data (t_result). Array of FKEYs to t_variable.id.
 * Estimate configurations found in data (t_result). Array of FKEYs to t_estimate_donf.id.
 * Relative difference between aggregated class estimate and sum of sub-classes estimates.';

GRANT EXECUTE ON FUNCTION @extschema@.fn_add_res_ratio_attr TO PUBLIC;

-- </function>
-- <function_name="" function_schema="extschema" src="functions/extschema/additivity/fn_add_res_total_attr.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_add_res_total_attr(integer)

-- DROP FUNCTION @extschema@.fn_add_res_total_attr(integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_add_res_total_attr(
	IN est_confs integer[],
	IN min_diff double precision default 0.0, IN include_null_diff boolean default true
)
  RETURNS TABLE(
	estimation_cell		integer,
	aux_conf		integer,
	force_synthetic		boolean,
	estimate_conf		integer,
	variable		integer,
	point_est		double precision,
	point_est_sum		double precision,
	variables_def		integer[],
	variables_found		integer[],
	estimate_confs_found	integer[],
	diff			double precision
) AS
$BODY$
DECLARE
	_complete_query text;
	_array_text text;
BEGIN
	--------------------------------QUERY--------------------------------
	_array_text := concat(quote_literal(est_confs::text), '::integer[]');
	--raise notice 'est_confs: %',  _array_text;
	_complete_query := '
with w_res_cell_var as not materialized (
	select
		t_result.estimate_conf,
		t_result.point,
		t_total_estimate_conf.estimation_cell,
		t_total_estimate_conf.target_variable as t_variable__id,
		t_total_estimate_conf.estimate_date_begin, t_total_estimate_conf.estimate_date_end, 
		array_agg(t_panel2total_2ndph_estimate_conf.panel order by panel) as t_panel2total_2ndph_estimate_conf_panels,
		t_total_estimate_conf.aux_conf, force_synthetic
	from @extschema@.t_result
	inner join @extschema@.t_estimate_conf ON t_estimate_conf.id = t_result.estimate_conf
	inner join @extschema@.t_total_estimate_conf ON t_total_estimate_conf.id = t_estimate_conf.total_estimate_conf
	inner join @extschema@.t_panel2total_2ndph_estimate_conf ON t_panel2total_2ndph_estimate_conf.total_estimate_conf = t_total_estimate_conf.id
	where (t_estimate_conf.estimate_type = 1)
	and (t_result.is_latest)
	and (t_result.estimate_conf = ANY (' || _array_text || '))
	group by 
		t_result.estimate_conf,
		t_result.point,
		t_total_estimate_conf.estimation_cell,
		t_total_estimate_conf.target_variable,
		t_total_estimate_conf.estimate_date_begin, t_total_estimate_conf.estimate_date_end, 
		t_total_estimate_conf.aux_conf, force_synthetic
)
, w_node_sum as (
	select
		estimation_cell,
		w_res_cell_var.t_variable__id, aux_conf, force_synthetic,
		estimate_date_begin, estimate_date_end, t_panel2total_2ndph_estimate_conf_panels,
		estimate_conf as node_estimate_confs,
		sum(point) as node_sum,
		hierarchy.node,
		hierarchy.edges as edges_def
	from w_res_cell_var
	inner join @extschema@.v_variable_hierarchy		as hierarchy on (hierarchy.node = w_res_cell_var.t_variable__id)
	group by estimation_cell, w_res_cell_var.t_variable__id, aux_conf, force_synthetic, estimate_date_begin, estimate_date_end, t_panel2total_2ndph_estimate_conf_panels, estimate_conf, node, edges_def
	order by estimation_cell, w_res_cell_var.t_variable__id, aux_conf, force_synthetic, estimate_date_begin, estimate_date_end, t_panel2total_2ndph_estimate_conf_panels, estimate_conf, node, edges_def
)
, w_edge_sum as (
	select
		w_node_sum.estimation_cell,
		w_node_sum.aux_conf,
		w_node_sum.force_synthetic,
		w_node_sum.node_estimate_confs,
		w_node_sum.estimate_date_begin, w_node_sum.estimate_date_end, w_node_sum.t_panel2total_2ndph_estimate_conf_panels,
		w_node_sum.node,
		w_node_sum.node_sum,
		w_node_sum.edges_def,
		array_agg(w_res_cell_var.t_variable__id order by w_res_cell_var.t_variable__id) as edges_found,
		array_agg(w_res_cell_var.estimate_conf order by w_res_cell_var.estimate_conf) as edges_estimate_confs,
		sum(w_res_cell_var.point) as edges_sum
	from w_node_sum
	left join w_res_cell_var on (
		w_res_cell_var.estimation_cell = w_node_sum.estimation_cell
		and w_res_cell_var.estimate_date_begin = w_node_sum.estimate_date_begin
		and w_res_cell_var.estimate_date_end = w_node_sum.estimate_date_end
		and w_res_cell_var.t_panel2total_2ndph_estimate_conf_panels = w_node_sum.t_panel2total_2ndph_estimate_conf_panels
		and case when
			w_res_cell_var.aux_conf is null and w_node_sum.aux_conf is null then true
			else w_res_cell_var.aux_conf = w_node_sum.aux_conf end
		and case when
			w_res_cell_var.force_synthetic is null and w_node_sum.force_synthetic is null then true
			else w_res_cell_var.force_synthetic = w_node_sum.force_synthetic end
		and w_res_cell_var.t_variable__id = any(w_node_sum.edges_def))
	group by w_node_sum.estimation_cell, 
		w_node_sum.aux_conf, w_node_sum.force_synthetic, w_node_sum.node_estimate_confs,
		w_node_sum.estimate_date_begin, w_node_sum.estimate_date_end, w_node_sum.t_panel2total_2ndph_estimate_conf_panels,
		w_node_sum.node, w_node_sum.node_sum, w_node_sum.edges_def
)
, w_diff as (
	select
		estimation_cell,
		aux_conf,
		force_synthetic,
		node_estimate_confs	as estimate_conf,
		node			as variable,
		node_sum		as point_est,
		edges_sum		as point_est_sum,
		edges_def		as variables_def,
		edges_found		as variables_found,
		edges_estimate_confs	as estimate_confs_found,
		case
			when node_sum != 0.0 and edges_sum = 0.0 then 100.0
			when node_sum = 0.0 and edges_sum = 0.0 then 0
			else abs(1 - (node_sum / edges_sum )) * 100.0
		end as diff
	from w_edge_sum
)
select * from w_diff
where
	(' || include_null_diff || ' and ((diff >= ' || min_diff || ') or (diff is null)))
	or
	(not ' || include_null_diff || ' and (diff >= ' || min_diff || '))
order by diff desc
;
';

	--RAISE NOTICE '%', _complete_query;
	RETURN QUERY EXECUTE _complete_query;
END;
$BODY$
  LANGUAGE plpgsql STABLE
  COST 100
  ROWS 100000;

comment on function @extschema@.fn_add_res_total_attr(integer[], double precision, boolean) is
	'Function showing total estimates attribute additivity (aggregated class estimate should be equal to sum of sub-classes estimates). Hierarchy between aggregated classes and its sub-classes is defined in v_variable_hierarchy.
Function input argument is:
 * Array of estimate configuration ids. FKEY to t_estimate_conf.id. All est. conf. ids to be checked (aggregated class together with sub-classes) need to be passed.
 * Minimal amount of difference [%].
 * Whether include NULL difference -- indicating defined sub-classes missing in data.
Resulting table has following columns:
 * Estimate estimation cell. FKEY to c_estimation_cell.id.
 * Estimate auxiliary configuration. FKEY to t_aux_conf.id.
 * Parameter showing whether estimate is forced to be synthetic.
 * Estimate configuration id. FKEY to t_estimate_conf.id.
 * Estimate attribute -- variable. FKEY to t_variable.id.
 * Aggregated class point estimate.
 * Sum of sub-classes point estimates (belonging to aggregated class).
 * Attributes -- variables defined in hierarchy (v_variable_hierarchy). Array of FKEYs to t_variable.id.
 * Attributes -- variables found in data (t_result). Array of FKEYs to t_variable.id.
 * Estimate configurations found in data (t_result). Array of FKEYs to t_estimate_donf.id.
 * Relative difference between aggregated class estimate and sum of sub-classes estimates.';

GRANT EXECUTE ON FUNCTION @extschema@.fn_add_res_total_attr TO PUBLIC;

-- </function>
-- <function_name="" function_schema="extschema" src="functions/extschema/additivity/fn_add_res_total_geo.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_add_res_total_geo(integer)

-- DROP FUNCTION @extschema@.fn_add_res_total_geo(integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_add_res_total_geo(
	IN est_confs integer[],
	IN min_diff double precision default 0.0, IN include_null_diff boolean default true
)
  RETURNS TABLE(
	variable		integer,
	aux_conf		integer,
	force_synthetic		boolean,
	estimate_conf		integer,
	estimation_cell		integer,
	point_est		double precision,
	point_est_sum		double precision,
	estimation_cells_def	integer[],
	estimation_cells_found	integer[],
	estimate_confs_found	integer[],
	diff			double precision
) AS
$BODY$
DECLARE
	_complete_query text;
	_array_text text;
BEGIN
	--------------------------------QUERY--------------------------------
	_array_text := concat(quote_literal(est_confs::text), '::integer[]');
	--raise notice 'est_confs: %',  _array_text;
	_complete_query := '
with w_res_cell_var as not materialized (
	select
		t_result.estimate_conf,
		t_result.point,
		t_total_estimate_conf.estimation_cell,
		t_total_estimate_conf.target_variable as t_variable__id,
		t_total_estimate_conf.estimate_date_begin, t_total_estimate_conf.estimate_date_end,
		json_agg(row_to_json((SELECT d FROM (SELECT est_conf_stratum.stratum, est_conf_stratum.panels) d))
			order by est_conf_stratum.stratum)::jsonb as strata_panels,
		t_total_estimate_conf.aux_conf, force_synthetic
	from @extschema@.t_result
	inner join @extschema@.t_estimate_conf ON t_estimate_conf.id = t_result.estimate_conf
	inner join @extschema@.t_total_estimate_conf ON t_total_estimate_conf.id = t_estimate_conf.total_estimate_conf
	inner join (
		select
			total_estimate_conf, stratum, array_agg(t_panel.id order by t_panel.id) as panels
		from @extschema@.t_panel2total_2ndph_estimate_conf
		inner join sdesign.t_panel on (t_panel2total_2ndph_estimate_conf.panel = t_panel.id)
		group by total_estimate_conf, stratum
		) as est_conf_stratum
		ON est_conf_stratum.total_estimate_conf = t_total_estimate_conf.id
	where (t_estimate_conf.estimate_type = 1)
	and (t_result.is_latest)
	and (t_result.estimate_conf = ANY (' || _array_text || '))
	group by
		t_result.estimate_conf,
		t_result.point,
		t_total_estimate_conf.estimation_cell,
		t_total_estimate_conf.target_variable,
		t_total_estimate_conf.estimate_date_begin, t_total_estimate_conf.estimate_date_end,
		t_total_estimate_conf.aux_conf, force_synthetic
)
, w_node_sum as (
	select
		estimation_cell,
		w_res_cell_var.t_variable__id, aux_conf, force_synthetic,
		estimate_date_begin, estimate_date_end, strata_panels,
		estimate_conf as node_estimate_confs,
		sum(point) as node_sum,
		hierarchy.node,
		hierarchy.edges as edges_def
	from w_res_cell_var
	inner join @extschema@.v_estimation_cell_hierarchy	as hierarchy on (hierarchy.node = w_res_cell_var.estimation_cell)
	group by estimation_cell, w_res_cell_var.t_variable__id, aux_conf, force_synthetic, estimate_date_begin, estimate_date_end, strata_panels, estimate_conf, node, edges_def
	order by estimation_cell, w_res_cell_var.t_variable__id, aux_conf, force_synthetic, estimate_date_begin, estimate_date_end, strata_panels, estimate_conf, node, edges_def
)
, w_edge_sum as (
	select
		w_node_sum.t_variable__id as variable,
		w_node_sum.aux_conf,
		w_node_sum.force_synthetic,
		w_node_sum.node_estimate_confs,
		w_node_sum.estimate_date_begin, w_node_sum.estimate_date_end,
		w_node_sum.node,
		w_node_sum.node_sum,
		w_node_sum.edges_def,
		array_agg(w_res_cell_var.estimation_cell order by w_res_cell_var.estimation_cell) as edges_found,
		array_agg(w_res_cell_var.estimate_conf order by w_res_cell_var.estimate_conf) as edges_estimate_confs,
		sum(w_res_cell_var.point) as edges_sum
	from w_node_sum
	left join w_res_cell_var on (
		w_res_cell_var.t_variable__id = w_node_sum.t_variable__id
		and w_res_cell_var.estimate_date_begin = w_node_sum.estimate_date_begin
		and w_res_cell_var.estimate_date_end = w_node_sum.estimate_date_end
		and
		(
		SELECT array_to_json(array_agg(elem order by elem))::jsonb
		FROM   jsonb_array_elements(w_node_sum.strata_panels) elem
		WHERE  (elem->>''stratum'')::int in (select jsonb_path_query(w_res_cell_var.strata_panels, ''$.stratum'')::int)
		) = w_res_cell_var.strata_panels
		and case when
			w_res_cell_var.aux_conf is null and w_node_sum.aux_conf is null then true
			else w_res_cell_var.aux_conf = w_node_sum.aux_conf end
		and case when
			w_res_cell_var.force_synthetic is null and w_node_sum.force_synthetic is null then true
			else w_res_cell_var.force_synthetic = w_node_sum.force_synthetic end
		and w_res_cell_var.estimation_cell = any(w_node_sum.edges_def))
	group by w_node_sum.t_variable__id,
		w_node_sum.aux_conf, w_node_sum.force_synthetic, w_node_sum.node_estimate_confs,
		w_node_sum.estimate_date_begin, w_node_sum.estimate_date_end,
		w_node_sum.node, w_node_sum.node_sum, w_node_sum.edges_def
)
, w_diff as (
	select
		variable,
		aux_conf,
		force_synthetic,
		node_estimate_confs	as estimate_conf,
		node			as estimation_cell,
		node_sum		as point_est,
		edges_sum		as point_est_sum,
		edges_def		as estimation_cells_def,
		edges_found		as estimation_cells_found,
		edges_estimate_confs	as estimate_confs_found,
		case
			when node_sum != 0.0 and edges_sum = 0.0 then 100.0
			when node_sum = 0.0 and edges_sum = 0.0 then 0
			else abs(1 - (node_sum / edges_sum )) * 100.0
		end as diff
	from w_edge_sum
)
select * from w_diff
where
	(' || include_null_diff || ' and ((diff >= ' || min_diff || ') or (diff is null)))
	or
	(not ' || include_null_diff || ' and (diff >= ' || min_diff || '))
order by diff desc
;
';

	--RAISE NOTICE '%', _complete_query;
	RETURN QUERY EXECUTE _complete_query;
END;
$BODY$
  LANGUAGE plpgsql STABLE
  COST 100
  ROWS 100000;

comment on function @extschema@.fn_add_res_total_geo(integer[], double precision, boolean) is
	'Fnction showing total estimates geographic additivity (aggregated class estimate should be equal to sum of sub-classes estimates). Hierarchy between aggregated classes and its sub-classes is defined in v_estimation_cell_hierarchy.
Function input argument is:
 * Array of estimate configuration ids. FKEY to t_estimate_conf.id. All est. conf. ids to be checked (aggregated class together with sub-classes) need to be passed.
 * Minimal amount of difference [%].
 * Whether include NULL difference -- indicating defined sub-classes missing in data.
Resulting table has following columns:
 * Estimate attribute -- variable. FKEY to t_variable.id.
 * Estimate auxiliary configuration. FKEY to t_aux_conf.id.
 * Parameter showing whether estimate is forced to be synthetic.
 * Estimate configuration id. FKEY to t_estimate_conf.id.
 * Estimate estimation cell. FKEY to c_estimation_cell.id.
 * Aggregated class point estimate.
 * Sum of sub-classes point estimates (belonging to aggregated class).
 * Estimation cells defined in hierarchy (v_estimation_cell_hierarchy). Array of FKEYs to c_estimation_cell.id.
 * Estimation cells found in data (t_result). Array of FKEYs to c_estimation_cell.id.
 * Estimate configurations found in data (t_result). Array of FKEYs to t_estimate_donf.id.
 * Relative difference between aggregated class estimate and sum of sub-classes estimates.';

GRANT EXECUTE ON FUNCTION @extschema@.fn_add_res_total_geo TO PUBLIC;

-- </function>
-- <function_name="" function_schema="extschema" src="functions/extschema/configuration/fn_1p_est_configuration.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_1p_est_configuration(integer[], integer[], integer[], integer[], date, date, varchar, integer)
--DROP FUNCTION @extschema@.fn_1p_est_configuration(integer[], integer[], integer[], integer[], date, date, character varying, integer);
CREATE OR REPLACE FUNCTION @extschema@.fn_1p_est_configuration(
		_panels integer[], _refyearsets integer[], 
		_estimation_cells integer[], _estimate_date_begin date, _estimate_date_end date, 
		_note varchar, _variable integer)
RETURNS integer
AS
$function$
DECLARE
_estimation_cell		integer;
_total_estimate_conf		integer;
_stratas_wp			integer[];
_target_label			varchar;
_cell				varchar;
_change_variable		boolean;
_panels_ec			integer[];
_refyearsets_ec			integer[];
BEGIN

IF _panels IS NULL THEN
	RAISE NOTICE 'Input array of panels is NULL!';
END IF;
-- test on the same length of each array (except estimation_cells)
IF
	array_length(_panels,1) != array_length(_refyearsets,1)
THEN
	RAISE EXCEPTION 'Input arrays of panels (%) and refyearsets (%) are not the same length.', _panels, _refyearsets;
END IF; 

IF _estimate_date_begin >= _estimate_date_end
THEN
	RAISE EXCEPTION 'Estimate_date_begin (%) is bigger then estimate_date_end (%) or they are equal!', _estimate_date_begin, _estimate_date_end;
END IF;

-- loop through an array of estimation cells
FOREACH _estimation_cell IN ARRAY _estimation_cells
LOOP
	_target_label := (
			SELECT		replace(
						replace(
							concat(coalesce(t2.label,'x'), ',', coalesce(t3.label,'x'), ',', coalesce(t4.label,'x'), ',', coalesce(t5.label,'x')),
						'x,',''),
					',x','') AS label
			FROM		@extschema@.t_variable AS t1
			LEFT JOIN 	@extschema@.c_target_variable AS t2 ON t1.target_variable = t2.id
			LEFT JOIN	@extschema@.c_sub_population_category AS t3 ON t1.sub_population_category = t3.id
			LEFT JOIN	@extschema@.c_area_domain_category AS t4 ON t1.area_domain_category = t4.id
			LEFT JOIN	@extschema@.c_auxiliary_variable_category AS t5 ON t1.auxiliary_variable_category = t5.id
			WHERE
				t1.id = _variable
			);


	_change_variable := (
			SELECT		CASE WHEN t3.id = 2 THEN true ELSE false END
			FROM 		@extschema@.t_variable AS t1
			LEFT JOIN 	@extschema@.c_target_variable AS t2 ON t1.target_variable = t2.id
			LEFT JOIN	@extschema@.c_state_or_change AS t3 ON t2.state_or_change = t3.id
			WHERE t1.id = _variable
			);
		

	_cell := (SELECT label FROM @extschema@.c_estimation_cell WHERE id = _estimation_cell);



	-- find out which panels belongs to specified estimation_cell
	SELECT
		array_agg(t3.panel ORDER BY t2.id), array_agg(t4.refyearset ORDER BY t2.id)
	FROM
		sdesign.t_panel AS t1
	INNER JOIN
		@extschema@.t_stratum_in_estimation_cell AS t2
	ON
		t1.stratum = t2.stratum
	INNER JOIN
		unnest(_panels) WITH ORDINALITY AS t3(panel, id)
	ON t1.id = t3.panel
	INNER JOIN
		unnest(_refyearsets) WITH ORDINALITY AS t4(refyearset, id)
	ON t3.id = t4.id
	WHERE t2.estimation_cell = _estimation_cell
	INTO _panels_ec, _refyearsets_ec;
	
	-- reset sequence (usually when previous atempt to configure fails, the sequence remains shifted)
	PERFORM setval('@extschema@.t_total_estimate_conf_id_seq', (SELECT coalesce(max(id),0)+1 FROM @extschema@.t_total_estimate_conf), FALSE);

	-- insert into table t_total_estimate_conf
	INSERT INTO @extschema@.t_total_estimate_conf (estimation_cell, estimate_date_begin, estimate_date_end, total_estimate_conf, target_variable, phase_estimate_type, aux_conf)
	VALUES
		(_estimation_cell, _estimate_date_begin, _estimate_date_end, concat('1p;T=',_target_label,';Cell=',_cell,_note), _variable, 1, NULL)
	RETURNING id
	INTO _total_estimate_conf;


		-- panels with target variable in specified stratas (panels are the ones with the less granularity, hence 1 stratum can have e.g. 4 panels which together results in 1 big panel)

		-- if it is change variable, the panels resulted in previous query are those who have the target variable available,
		-- that, they have been measured at least twice, otherwise the change target variable would not be available
		-- but there must be also reference for the beginning of the reference period of sample panel
		-- in other words, the sample panel must be measured twice WITHIN the given period
		-- the given period must be specified according to estimation of the change e.g.
		
		-- reference year sets for panel is lets say 2011 and 2016
		-- for the estimation of state the given period would be only 1.1.2016-31.12.2016
		-- byt for the estimation of the change it must be 1.1.2011-31.12.2016 to take this panel accounted
/*
		IF _change_variable = true
		THEN
			SELECT
				array_agg(DISTINCT stratum ORDER BY stratum) AS stratas,
				array_agg(t1.panel ORDER BY t1.panel) AS panels
			FROM
				sdesign.t_panel AS t1
			INNER JOIN
				sdesign.cm_refyearset2panel_mapping AS t2
			ON	t1.id = t2.panel
			INNER JOIN
				sdesign.t_reference_year_set AS t3
			ON	t2.reference_year_set = t3.id
			WHERE
				t2.panel = ANY(_panels_ec) AND		-- panels measured twice
				NOT t3.id = ANY(_refyearsets_ec) AND	-- give away reference year sets already accounted
				t3.reference_date_begin >= $5 AND	-- condition for the given period
				t3.reference_date_end <= $6
			INTO _stratas_wp, _panels_wp;			-- resulted list of panels used for calculation
		END IF;
*/

	-- insert into table t_panel2total_2ndph_estimate_conf
	INSERT INTO @extschema@.t_panel2total_2ndph_estimate_conf (total_estimate_conf, panel, reference_year_set)
	SELECT
		_total_estimate_conf, panel, reference_year_set
	FROM
		unnest(_panels_ec) WITH ORDINALITY AS t1(panel, id)
	INNER JOIN
		unnest(_refyearsets_ec) WITH ORDINALITY AS t2(reference_year_set,id)
	ON
		t1.id = t2.id;

	-- insert into table t_estimate_conf
	INSERT INTO @extschema@.t_estimate_conf (estimate_type, total_estimate_conf, denominator)
	SELECT 1, _total_estimate_conf, NULL;

	RETURN _total_estimate_conf;
END LOOP;
END;
$function$
LANGUAGE plpgsql
PARALLEL SAFE;

COMMENT ON FUNCTION @extschema@.fn_1p_est_configuration(integer[],integer[],integer[],date,date,varchar,integer) IS 'Function makes the estimate configuration present in the database - provides inserts into the all necessary tables.';

-- </function>
-- <function_name="" function_schema="extschema" src="functions/extschema/configuration/fn_2p_est_configuration.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_2p_est_configuration(integer, regclass)
--DROP FUNCTION @extschema@.fn_2p_est_configuration(integer,date,date,character varying,integer,integer);
CREATE OR REPLACE FUNCTION @extschema@.fn_2p_est_configuration(_estimation_cell integer, _estimate_date_begin date, _estimate_date_end date, _note varchar, _target_variable integer, _aux_conf integer, _force_synthetic boolean default False)
RETURNS integer
AS
$function$
DECLARE
_total_estimate_conf		integer;
_stratas			integer[];
_stratas_wp			integer[];
_panels				integer[];
_refyearsets			integer[];
_panels_aux			integer[];
_param_area			integer;
_param_area_code		varchar;
_target_label			varchar;
_model				integer;
_cell				varchar;
BEGIN

-- test for existing g_betas
-- otherwise the configuration cannot be done (sometimes the g_betas cannot be computed)
-- so this prevents to configure non-computable estimates

IF (SELECT count(*) FROM @extschema@.t_g_beta WHERE aux_conf = $6) = 0
THEN
	RAISE EXCEPTION 'G-betas for required aux_conf are not available. The computation of it was not run or is not able to compute (mostly the problem of matrix inversion).';
END IF;

-- create the label of estimate
_target_label := (
		SELECT		replace(
					replace(
						concat(coalesce(t2.label,'x'), ',', coalesce(t3.label,'x'), ',', coalesce(t4.label,'x'), ',', coalesce(t5.label,'x')),
					'x,',''),
			       	',x','') AS label
		FROM		@extschema@.t_variable AS t1
		LEFT JOIN 	@extschema@.c_target_variable AS t2 ON t1.target_variable = t2.id
		LEFT JOIN	@extschema@.c_sub_population_category AS t3 ON t1.sub_population_category = t3.id
		LEFT JOIN	@extschema@.c_area_domain_category AS t4 ON t1.area_domain_category = t4.id
		LEFT JOIN	@extschema@.c_auxiliary_variable_category AS t5 ON t1.auxiliary_variable_category = t5.id
		WHERE
			t1.id = $5
		);

_param_area := (SELECT param_area FROM @extschema@.t_aux_conf WHERE id = $6);
_model := (SELECT model FROM @extschema@.t_aux_conf WHERE id = $6);
_param_area_code := (SELECT param_area_code FROM @extschema@.f_a_param_area WHERE gid = _param_area);
_cell := (SELECT label FROM @extschema@.c_estimation_cell WHERE id = $1);

-- reset sequence (usually when previous atempt to configure fails, the sequence remains shifted)
PERFORM setval('@extschema@.t_total_estimate_conf_id_seq', (SELECT coalesce(max(id),0)+1 FROM @extschema@.t_total_estimate_conf), FALSE);

-- insert into table t_total_estimate_conf
INSERT INTO @extschema@.t_total_estimate_conf (estimation_cell, estimate_date_begin, estimate_date_end, total_estimate_conf, target_variable, phase_estimate_type, force_synthetic, aux_conf)
VALUES
	($1, $2, $3, concat('2p;T=',_target_label,';C=',_cell,';PA=',_param_area_code, ';m=',_model,_note), $5, 2, $7, $6)
ON CONFLICT (estimation_cell, estimate_date_begin, estimate_date_end, target_variable, phase_estimate_type, coalesce(force_synthetic,false), coalesce(aux_conf,0))
DO NOTHING
RETURNING id
INTO _total_estimate_conf;

IF _total_estimate_conf IS NOT NULL THEN

	-- test on param_area_coverage
		SELECT
			array_agg(t1.id ORDER BY t1.id)
		FROM
			sdesign.t_stratum AS t1
		INNER JOIN
			@extschema@.f_a_param_area AS t2
		ON
			-- buffered stratum?
			-- no, if only buffer of the stratum would intersect the cell, 
			-- no consequences on estimate would be introduced, the buffer is in general used only for the edge effect compensation (the compensated area is within non-buffered stratum)
			ST_Intersects(t1.geom, t2.geom) AND NOT ST_Touches(t1.geom, t2.geom)
		WHERE
			t2.gid = _param_area
		INTO _stratas;

		IF _stratas IS NULL
		THEN
			RAISE EXCEPTION 'The specified cell is not intersected by any stratum. Choose another estimation cell.';
		END IF;

	-- existing panels configured in panel2aux_conf
		SELECT
			array_agg(panel ORDER BY panel)
		FROM
			@extschema@.t_panel2aux_conf AS t1
		WHERE
			t1.aux_conf = $6
		INTO _panels_aux;


	-- check of panel2total_2ndph
	-- and addition of panels from param_area - is the target variable available not only in cell?

		WITH w_data AS MATERIALIZED (
			SELECT
				t1.id AS stratum, t2.id AS panel, t9.id AS reference_year_set, t2.plot_count AS total
			FROM
				sdesign.t_stratum AS t1
			INNER JOIN
				sdesign.t_panel AS t2
			ON
				t1.id = t2.stratum
			INNER JOIN
				sdesign.cm_refyearset2panel_mapping AS t8
			ON
				t2.id = t8.panel --AND
				--t8.id = t9.reference_year_set
			INNER JOIN
				sdesign.t_reference_year_set AS t9
			ON
				t8.reference_year_set = t9.id
			INNER JOIN
				@extschema@.t_available_datasets AS t6
			ON
				t2.id = t6.panel AND
				t9.id = t6.reference_year_set
			INNER JOIN
				@extschema@.t_variable AS t7
			ON
				t6.variable = t7.id
			WHERE
				array[t1.id] <@ _stratas AND
				t7.id = $5 AND 
				(t9.reference_date_begin >= $2 AND
				t9.reference_date_end <= $3)
			GROUP BY
				t1.id, t2.id, t9.id
		)
		SELECT
			array_agg(DISTINCT stratum ORDER BY stratum) AS stratas,
			array_agg(panel ORDER BY panel) AS panels,
			array_agg(reference_year_set ORDER BY panel) AS refyearsets
		FROM
			(SELECT
				stratum, panel, reference_year_set,
				total,
				max(total) OVER(PARTITION BY stratum, panel, reference_year_set) AS max_total
			FROM
				w_data
			) AS t1
		WHERE
			-- pick up the most dense panel with target variable
			total = max_total
		INTO _stratas_wp, _panels, _refyearsets;

		IF _panels != _panels_aux OR _panels IS NULL
		THEN
			RAISE EXCEPTION 'Not all panels coming from g_beta have available target variable! total_estimate_conf: %, panels: %, panels_aux: %', _total_estimate_conf, _panels, _panels_aux;
		END IF;

	-- insert into table t_panel2total_2ndph_estimate_conf
	INSERT INTO @extschema@.t_panel2total_2ndph_estimate_conf (total_estimate_conf, panel, reference_year_set)
	SELECT
		_total_estimate_conf, panel, reference_year_set
	FROM
		unnest(_panels) WITH ORDINALITY AS t1(panel, id)
	INNER JOIN
		unnest(_refyearsets) WITH ORDINALITY AS t2(reference_year_set,id)
	ON
		t1.id = t2.id;

	-- insert into table t_estimate_conf
	INSERT INTO @extschema@.t_estimate_conf (estimate_type, total_estimate_conf, denominator)
	SELECT 1, _total_estimate_conf, NULL;

	IF	(
			SELECT
				array_agg(t_variable.id ORDER BY t_variable.id)
			FROM @extschema@.t_aux_total
			INNER JOIN @extschema@.t_variable 		ON (t_aux_total.variable = t_variable.id)
			INNER JOIN @extschema@.c_estimation_cell 	ON (t_aux_total.estimation_cell = c_estimation_cell.id)
			INNER JOIN @extschema@.t_model_variables 	ON t_model_variables.variable = t_variable.id
			INNER JOIN @extschema@.t_model 			ON t_model.id = t_model_variables.model
			INNER JOIN @extschema@.t_aux_conf 		ON t_aux_conf.model = t_model_variables.model
			WHERE 	c_estimation_cell.id = $1 AND
				t_aux_conf.id = $6 AND
				t_aux_total.is_latest

		)
		!= (
			SELECT
				array_agg(t_model_variables.variable order by variable)
			FROM @extschema@.t_aux_conf
			INNER JOIN @extschema@.t_model ON t_model.id = t_aux_conf.model
			INNER JOIN @extschema@.t_model_variables ON t_model_variables.model = t_model.id
			WHERE t_aux_conf.id = $6
		)
	THEN
		RAISE EXCEPTION 'fn_2p_est_configuration: t_aux_total not found! (total_estimate_conf: %)', _total_estimate_conf;
	END IF;

ELSE
	RAISE NOTICE 'Required configuration already exists!';
END IF;

RETURN _total_estimate_conf;


END;
$function$
LANGUAGE plpgsql
PARALLEL SAFE;

--COMMENT ON FUNCTION @extschema@.fn_2p_est_configuration() IS '.';

-- </function>
-- <function_name="" function_schema="extschema" src="functions/extschema/configuration/fn_delete_aux_conf.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_delete_aux_conf(integer)
--DROP FUNCTION @extschema@.fn_delete_aux_conf(integer);
CREATE OR REPLACE FUNCTION @extschema@.fn_delete_aux_conf(_aux_conf integer)
RETURNS void
AS
$function$
DECLARE
BEGIN

-- 2nd order tables
-- t_g_beta
DELETE FROM @extschema@.t_g_beta WHERE aux_conf = $1;
RAISE NOTICE 'Deleting t_g_betas...';
-- t_panel2aux_conf
DELETE FROM @extschema@.t_panel2aux_conf WHERE aux_conf = $1;
RAISE NOTICE 'Deleting panel to auxiliary configuration mapping...';
-- t_total_estimate_conf
PERFORM	@extschema@.fn_delete_total_estimate_conf(id)
FROM	@extschema@.t_total_estimate_conf
WHERE	aux_conf = $1;

-- 1st order table
-- t_aux_conf
DELETE FROM @extschema@.t_aux_conf WHERE id = $1;
RAISE NOTICE 'Deleting auxiliary configuration = %.', $1;

RETURN;

END;
$function$
LANGUAGE plpgsql
PARALLEL UNSAFE;

COMMENT ON FUNCTION @extschema@.fn_delete_aux_conf(integer) IS 'Function for auxiliary configuration deletion with all records in referenced tables.';

-- </function>
-- <function_name="" function_schema="extschema" src="functions/extschema/configuration/fn_delete_total_estimate_conf.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_delete_total_estimate_conf(integer)
--DROP FUNCTION @extschema@.fn_delete_total_estimate_conf(integer);
CREATE OR REPLACE FUNCTION @extschema@.fn_delete_total_estimate_conf(_total_estimate_conf integer)
RETURNS void
AS
$function$
DECLARE
_estimate_conf integer[];
BEGIN

-- 3rd order tables
-- t_result
DELETE FROM @extschema@.t_result WHERE estimate_conf IN (
	SELECT 	id
	FROM 	@extschema@.t_estimate_conf
	WHERE	total_estimate_conf = $1 OR
		denominator = $1)
	;
RAISE NOTICE 'Deleting results.';

-- 2nd order tables
-- t_panel2total_1stph_estimate_conf, 2ndph
DELETE FROM @extschema@.t_panel2total_2ndph_estimate_conf WHERE total_estimate_conf = $1;
DELETE FROM @extschema@.t_panel2total_1stph_estimate_conf WHERE total_estimate_conf = $1;
RAISE NOTICE 'Deleting panel to total_estimate_conf mapping...';
-- t_estimate_conf
WITH w AS MATERIALIZED (DELETE FROM @extschema@.t_estimate_conf WHERE total_estimate_conf = $1 OR denominator = $1 RETURNING id)
SELECT array_agg(id) FROM w INTO _estimate_conf;
RAISE NOTICE 'Deleting estimate configurations (%).', _estimate_conf;

-- 1st order table
-- t_total_estimate_conf
DELETE FROM @extschema@.t_total_estimate_conf WHERE id = $1;
RAISE NOTICE 'Deleting total estimate configuration = %.', $1;

RETURN;
END;
$function$
LANGUAGE plpgsql
PARALLEL UNSAFE;

COMMENT ON FUNCTION @extschema@.fn_delete_total_estimate_conf(integer) IS 'Function for auxiliary configuration deletion with all records in referenced tables.';

-- </function>
-- <function_name="" function_schema="extschema" src="functions/extschema/configuration/fn_get_panels_in_estimation_cells.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_get_panels_in_estimation_cells(integer, regclass)
--DROP FUNCTION @extschema@.fn_get_panels_in_estimation_cells(integer,date,date,character varying,integer,integer);
CREATE OR REPLACE FUNCTION @extschema@.fn_get_panels_in_estimation_cells(_estimation_cells integer[], _estimate_date_begin date, _estimate_date_end date, _target_variable integer)
RETURNS TABLE (
stratum 			integer,
stratum_label 			varchar(20),
panel 				integer,
panel_label			varchar(20),
panel_subset			integer,
reference_year_set 		integer,
reference_year_set_label 	varchar(20),
reference_date_begin 		date,
reference_date_end 		date,
reference_year_set_fit 		boolean,
portion_of_panel_inside		double precision,
portion_of_period_covered	double precision,
total 				integer,
is_max				boolean
)
AS
$function$
DECLARE
_change_variable 	boolean;
_estimation_cell	integer;
_cell_area_m2		double precision;
_stratas_all		integer[];
_stratas		integer[];
_sum_stratas_m2		double precision;
_result_test		boolean;

BEGIN

_change_variable := (
		SELECT		CASE WHEN t3.id = 2 THEN true ELSE false END
		FROM 		@extschema@.t_variable AS t1
		LEFT JOIN 	@extschema@.c_target_variable AS t2 ON t1.target_variable = t2.id
		LEFT JOIN	@extschema@.c_state_or_change AS t3 ON t2.state_or_change = t3.id
		WHERE t1.id = $5
		);
	
-- panels order
--IF _panels IS NOT NULL THEN _panels := (SELECT array_agg(panel ORDER BY panel) FROM unnest(_panels) AS t(panel));
--END IF;

FOREACH _estimation_cell IN ARRAY _estimation_cells
LOOP

	-- cell data
	WITH w_cell AS (
		-- cell geometry can be divided into smaller blocks, thats why sum() 
		SELECT estimation_cell, sum(st_area(geom)) AS area_m2
		FROM @extschema@.f_a_cell
		WHERE estimation_cell = _estimation_cell
		GROUP BY estimation_cell
	)
	SELECT	area_m2
	FROM w_cell
	INTO _cell_area_m2;

	-- stratas in cell
	WITH w_stratas AS (
		SELECT t1.stratum, t1.area_m2
		FROM @extschema@.t_stratum_in_estimation_cell AS t1
		WHERE estimation_cell = _estimation_cell
	)
	SELECT	array_agg(t1.stratum), sum(t1.area_m2)
	FROM w_stratas AS t1
	INTO _stratas, _sum_stratas_m2;

	IF _sum_stratas_m2 IS NULL
	THEN
		RAISE EXCEPTION 'Estimation cell is not covered by any stratum (estimation_cell = %).', _estimation_cell;
	END IF;

	-- test on cell coverage
	-- two decimal places considered when comparing the final product of division with 1
	_result_test := (SELECT CASE WHEN round(_sum_stratas_m2::numeric/_cell_area_m2::numeric, 2) = 1 THEN true ELSE false END);
	
	IF _result_test = false
	THEN
		RAISE WARNING 'The intersection of stratas and cell (% ha) is less than area of whole estimation cell (% ha).', 
				round(_sum_stratas_m2::numeric/10000.0,1), round(_cell_area_m2::numeric/10000.0,1);
	END IF;

	_stratas_all := _stratas_all || _stratas;

END LOOP;

-- return the list of panels for given target variable
-- if the reference time period is met then it is stated in column reference_year_set_fit
RETURN QUERY
WITH w_data AS (
	SELECT
		t1.id AS stratum,
		t1.stratum AS stratum_label, 
		t2.panel, 
		t2.panel_label,
		t2.panel_subset,
		t2.reference_year_set,
		t2.reference_year_set_label,
		t2.reference_date_begin,
		t2.reference_date_end,
		t2.reference_year_set_fit,
		t2.estimate_range,
		t2.reference_range,
		t2.intersection_range,

		-- find out what portion of panel is inside the estimate range
		CASE WHEN isempty(intersection_range) = false
		THEN
			EXTRACT(DAYS FROM (upper(intersection_range) - lower(intersection_range))) / -- interval of intersection
			EXTRACT(DAYS FROM (upper(reference_range) - lower(reference_range))) -- interval of reference period

		ELSE
			0 -- 0 percent of panel inside the period
		END AS portion_of_panel_inside,

		-- find out what portion of estimation period is covered by panel
		CASE WHEN isempty(intersection_range) = false
		THEN
			EXTRACT(DAYS FROM (upper(intersection_range) - lower(intersection_range))) / -- interval of intersection
			EXTRACT(DAYS FROM (upper(estimate_range) - lower(estimate_range))) -- interval of estimation period
		ELSE 0 -- 0 percent covered by panel
		END AS portion_of_period_covered,
		t2.total
	FROM
		(SELECT t1.id, t1.stratum FROM sdesign.t_stratum AS t1 WHERE array[id] <@ _stratas_all) AS t1
	-- could be inner join, every stratum has to have at least one panel
	-- but in case of not available target variable, stratum would disappear from the list
	LEFT JOIN
		(SELECT
			t2.stratum,
			t2.id AS panel,
			t2.panel AS panel_label,
			t2.plot_count AS total,
			t2.panel_subset,
			t4.id AS reference_year_set,
			t4.reference_year_set AS reference_year_set_label,
			t4.reference_date_begin,
			t4.reference_date_end,
			-- test if reference_year_set fits the parameteres given
			CASE
			WHEN 	t4.reference_date_begin >= $2 AND
				t4.reference_date_end <= $3 
			THEN true
			ELSE false
			END AS reference_year_set_fit,

			-- range of estimation period
			tsrange(_estimate_date_begin,_estimate_date_end) AS estimate_range,
			-- range of reference period
			tsrange(t4.reference_date_begin,t4.reference_date_end) AS reference_range,

			-- intersection of dates
			tsrange(t4.reference_date_begin,t4.reference_date_end) * 
			tsrange(_estimate_date_begin,_estimate_date_end) AS intersection_range

		FROM
			sdesign.t_panel AS t2
		-- inner join, assuming every panel has SOME reference_year_set mapping
		INNER JOIN
			sdesign.cm_refyearset2panel_mapping AS t3
		ON
			t2.id = t3.panel
		-- again inner join, I am curious, what other reference year sets are possible in case the required dates does not fit (but target variable available)
		INNER JOIN
			sdesign.t_reference_year_set AS t4
		ON
			t3.reference_year_set = t4.id
		-- inner join, target variable must fit
		INNER JOIN
			@extschema@.t_available_datasets AS t5
		ON
			t2.id = t5.panel AND
			t4.id = t5.reference_year_set AND
			t5.variable = $4 
		) AS t2
	ON
		t1.id = t2.stratum

), w_max AS (
	SELECT 
		t1.stratum, t1.stratum_label, t1.panel, t1.panel_label, t1.panel_subset,
		t1.reference_year_set, t1.reference_year_set_label, t1.reference_date_begin, t1.reference_date_end, t1.reference_year_set_fit, 
		t1.portion_of_panel_inside, t1.portion_of_period_covered,
		t1.total,
		-- maximum number of plots for a group of panels with the same reference_year_set 
		-- and within belonging to the required reference period
		max(t1.total) OVER(PARTITION BY t1.stratum, t1.reference_year_set, t1.reference_year_set_fit) AS max_total
	FROM w_data AS t1
)
SELECT
	t1.stratum, t1.stratum_label, t1.panel, t1.panel_label, t1.panel_subset,
	t1.reference_year_set, t1.reference_year_set_label, t1.reference_date_begin, t1.reference_date_end, t1.reference_year_set_fit, 
	t1.portion_of_panel_inside, t1.portion_of_period_covered, t1.total,
	CASE WHEN t1.total = t1.max_total THEN true ELSE false END AS is_max
FROM
	w_max AS t1;

END;
$function$
LANGUAGE plpgsql
PARALLEL SAFE;

COMMENT ON FUNCTION @extschema@.fn_get_panels_in_estimation_cells(integer[], date, date, integer) IS 'Function returns list of stratas and panels in the estimation cell. For each panel there is an indicator (reference_year_set_fit) if the panel lies within estimation period and indicator max which tells if the panel is the one with the maximum possible plots.';

-- </function>
-- <function_name="" function_schema="extschema" src="functions/extschema/fn_1p_data.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_1p_data(integer)

-- DROP FUNCTION @extschema@.fn_1p_data(integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_1p_data(
    IN conf_id integer
)
  RETURNS TABLE(
	gid bigint,
	cluster integer,
	attribute integer,
	stratum integer,
	plots_per_cluster integer,
	plcount bigint,
	cluster_is_in_cell boolean,
	ldsity_d double precision,
	ldsity_d_plus double precision,
	is_aux boolean,
	is_target boolean,
	geom geometry(MultiPoint),
	ldsity_res_d double precision,
	ldsity_res_d_plus double precision,
	pix double precision,
	sweight double precision,
	DELTA_T__G_beta double precision,
	nb_sampling_units integer, 
	sweight_strata_sum double precision, 
	lambda_d_plus double precision
) AS
$BODY$
DECLARE
	_complete_query text;
BEGIN
	--------------------------------QUERY--------------------------------
	_complete_query := '
WITH
---------------------------------------------------------
-- Data block
---------------------------------------------------------
w_ldsity_plot AS MATERIALIZED (
	select
		f_p_plot.gid,
		t_total_estimate_conf.id as conf_id,
		t_panel.stratum,
		t_panel2total_2ndph_estimate_conf.panel,
		t_cluster.id as cluster,
		cm_cluster2panel_mapping.sampling_weight,
		t_total_estimate_conf.target_variable as attribute,
		plots_per_cluster,
		true AS plot_is_in_cell,
		coalesce(t_target_data.value, 0) as ldsity,
		f_p_plot.geom
	from @extschema@.t_total_estimate_conf
	inner join @extschema@.t_panel2total_2ndph_estimate_conf on t_total_estimate_conf.id = t_panel2total_2ndph_estimate_conf.total_estimate_conf
	inner join sdesign.t_panel ON t_panel.id = t_panel2total_2ndph_estimate_conf.panel
	inner join sdesign.cm_cluster2panel_mapping ON cm_cluster2panel_mapping.panel = t_panel.id
	inner join sdesign.t_cluster ON t_cluster.id = cm_cluster2panel_mapping."cluster"
	inner join sdesign.f_p_plot ON f_p_plot."cluster" = t_cluster.id
	inner join sdesign.cm_plot2cluster_config_mapping on f_p_plot.gid = cm_plot2cluster_config_mapping.plot
	inner join sdesign.t_cluster_configuration ON (t_panel.cluster_configuration = t_cluster_configuration.id
											and cm_plot2cluster_config_mapping.cluster_configuration = t_cluster_configuration.id)
	inner join sdesign.t_stratum ON t_panel.stratum = t_stratum.id
	inner join @extschema@.cm_plot2cell_mapping ON (cm_plot2cell_mapping.estimation_cell = t_total_estimate_conf.estimation_cell 
											and cm_plot2cell_mapping.plot = f_p_plot.gid)
	left join @extschema@.t_target_data on (
		f_p_plot.gid = t_target_data.plot and
		t_panel2total_2ndph_estimate_conf.reference_year_set = t_target_data.reference_year_set and
		t_total_estimate_conf.target_variable = t_target_data.variable and
		t_target_data.is_latest)
	where t_total_estimate_conf.id = ' || conf_id || '
)
, w_ldsity_cluster AS MATERIALIZED (
	SELECT
		w_ldsity_plot.conf_id,
		concat(w_ldsity_plot.conf_id, ROW_NUMBER() OVER ())::bigint as gid,
		w_ldsity_plot.cluster,
		w_ldsity_plot.attribute,
		w_ldsity_plot.stratum,
                w_ldsity_plot.panel,
		w_ldsity_plot.plots_per_cluster,
		w_ldsity_plot.sampling_weight,
		count(*) as plcount,
		sum(w_ldsity_plot.plot_is_in_cell::int) > 0 as cluster_is_in_cell,
		sum(w_ldsity_plot.ldsity * plot_is_in_cell::int)/w_ldsity_plot.plots_per_cluster AS ldsity_D,
		sum(w_ldsity_plot.ldsity)/w_ldsity_plot.plots_per_cluster AS ldsity_D_plus,
		st_collect(w_ldsity_plot.geom)::geometry(MultiPoint) as geom
	FROM       w_ldsity_plot
	GROUP BY conf_id, stratum, panel, cluster, sampling_weight, plots_per_cluster, attribute
	ORDER BY stratum, cluster, attribute
)
, w_strata_sum AS MATERIALIZED (
	select
		t.conf_id,
		t_panel.stratum,
		case when t_cluster_configuration.cluster_design then
			t_stratum.buffered_area_m2/10000
		else
			t_stratum.area_m2/10000
		end as lambda_d_plus,
		sum(t_panel.cluster_count) as nb_sampling_units,
		sum(t_panel.sweight_panel_sum) as sweight_strata_sum
	from sdesign.t_stratum
	inner join sdesign.t_panel on (t_panel.stratum = t_stratum.id)
	inner join sdesign.t_cluster_configuration on (t_panel.cluster_configuration = t_cluster_configuration.id)
	inner join (select distinct conf_id, panel from w_ldsity_plot) as t on (t_panel.id = t.panel)
	group by conf_id, t_panel.stratum, lambda_d_plus
)
, w_1p_data AS MATERIALIZED (
	select
		w_ldsity_cluster.gid, w_ldsity_cluster.cluster,
		w_ldsity_cluster.attribute, w_ldsity_cluster.stratum, w_ldsity_cluster.plots_per_cluster,
		w_ldsity_cluster.plcount, w_ldsity_cluster.cluster_is_in_cell, w_ldsity_cluster.ldsity_d,
		w_ldsity_cluster.ldsity_d_plus, false::boolean as is_aux, true::boolean as is_target,
		w_ldsity_cluster.geom,
		NULL::double precision as ldsity_res_D, NULL::double precision as ldsity_res_D_plus,
		NULL::double precision as pix,  w_ldsity_cluster.sampling_weight as sweight, NULL::double precision as DELTA_T__G_beta,
		w_strata_sum.nb_sampling_units::integer, w_strata_sum.sweight_strata_sum, w_strata_sum.lambda_d_plus
	from w_ldsity_cluster
	INNER JOIN w_strata_sum ON w_ldsity_cluster.stratum = w_strata_sum.stratum
)
select * from w_1p_data;
';

	--RAISE NOTICE '%', _complete_query;
	RETURN QUERY EXECUTE _complete_query;
END;
$BODY$
  LANGUAGE plpgsql STABLE
  COST 100
  ROWS 100000;

COMMENT ON FUNCTION @extschema@.fn_1p_data(integer) IS 'Function preparing data for regression estimate.';

-- </function>
-- <function_name="" function_schema="extschema" src="functions/extschema/fn_2p_data.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_2p_data(integer)

-- DROP FUNCTION @extschema@.fn_2p_data(integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_2p_data(
    IN conf_id integer
)
  RETURNS TABLE(
	gid bigint,
	cluster integer,
	attribute integer,
	stratum integer,
	plots_per_cluster integer,
	plcount bigint,
	cluster_is_in_cell boolean,
	ldsity_d double precision,
	ldsity_d_plus double precision,
	is_aux boolean,
	is_target boolean,
	geom geometry(MultiPoint),
	ldsity_res_d double precision,
	ldsity_res_d_plus double precision,
	pix double precision,
	sweight double precision,
	DELTA_T__G_beta double precision,
	nb_sampling_units integer,
	sweight_strata_sum double precision,
	lambda_d_plus double precision,
	sigma boolean
) AS
$$
begin
--------------------------------QUERY--------------------------------
---------------------------------------------------------
-- Configuration block
---------------------------------------------------------
execute '
create temporary table w_configuration ON COMMIT DROP AS (
	with w_a AS NOT MATERIALIZED (
		select
			t_total_estimate_conf.estimation_cell, t_aux_conf.model, 
			t_aux_conf.sigma, t_total_estimate_conf.force_synthetic, t_aux_conf.param_area,
			array_agg(t_total_estimate_conf.id order by t_total_estimate_conf.target_variable) as t_total_estimate_conf__id,
			array_agg(t_aux_conf.id order by t_total_estimate_conf.target_variable) as t_aux_conf__id,
			array_agg(t_total_estimate_conf.target_variable order by t_total_estimate_conf.target_variable) as target_attributes,
			t_total_estimate_conf.aux_conf
		from @extschema@.t_total_estimate_conf
		inner join @extschema@.t_aux_conf ON t_aux_conf.id = t_total_estimate_conf.aux_conf
		where t_total_estimate_conf.id = $1
		group by t_total_estimate_conf.estimation_cell, t_aux_conf.model, t_aux_conf.sigma, t_total_estimate_conf.force_synthetic,
			t_aux_conf.param_area, t_total_estimate_conf.aux_conf
	)
	select
		t_total_estimate_conf__id[1] as id,
		w_a.t_total_estimate_conf__id, w_a.t_aux_conf__id,
		w_a.param_area, w_a.estimation_cell as cell, w_a.model, w_a.sigma, w_a.force_synthetic,
		array_agg(t_model_variables.variable order by t_model_variables.variable) as aux_attributes, w_a.target_attributes
	from w_a
	inner join @extschema@.t_aux_conf ON t_aux_conf.id = w_a.aux_conf
	inner join @extschema@.t_model ON t_model.id = t_aux_conf.model
	inner join @extschema@.t_model_variables ON t_model_variables.model = t_model.id
	group by w_a.estimation_cell, w_a.model, w_a.sigma, w_a.force_synthetic, w_a.param_area, w_a.target_attributes, w_a.t_total_estimate_conf__id, w_a.t_aux_conf__id
	order by id limit 1
);' using fn_2p_data.conf_id;
analyze w_configuration;

execute '
create temporary table w_cell_selection ON COMMIT DROP AS (
	SELECT
		w_configuration.id as conf_id,
		estimation_cell as estimation_cell,
		geom
	FROM @extschema@.f_a_cell
	INNER JOIN w_configuration ON (w_configuration.cell = f_a_cell.estimation_cell)
);';
analyze w_cell_selection;

execute '
create temporary table w_plot ON COMMIT DROP as (
WITH
w_param_area_selection AS NOT MATERIALIZED (
	SELECT
		w_configuration.id as conf_id,
		gid as param_area_gid,
		geom
	FROM @extschema@.f_a_param_area
	INNER JOIN w_configuration ON (w_configuration.param_area = f_a_param_area.gid)
)
---------------------------------------------------------
-- Data block
---------------------------------------------------------
, w_plot AS NOT MATERIALIZED (
	select distinct
		f_p_plot.gid, w_configuration.id as conf_id, t_cluster.id as cluster, t_panel.stratum,
		case when w_configuration.force_synthetic = True then False else cm_plot2cell_mapping.id IS NOT NULL end AS plot_is_in_cell, 
		-- t_panel2aux_conf.reference_year_set as reference_year_set, t_panel2aux_conf.panel as panel,
		t_panel2total_2ndph_estimate_conf.reference_year_set as reference_year_set, t_panel2total_2ndph_estimate_conf.panel as panel,
		f_p_plot.geom
	from w_configuration
        inner join @extschema@.t_panel2total_2ndph_estimate_conf on w_configuration.t_total_estimate_conf__id[1] = t_panel2total_2ndph_estimate_conf.total_estimate_conf
	--inner join @extschema@.t_panel2aux_conf on w_configuration.t_aux_conf__id[1] = t_panel2aux_conf.aux_conf
	inner join sdesign.t_panel ON (t_panel.id = t_panel2total_2ndph_estimate_conf.panel)
	inner join sdesign.cm_cluster2panel_mapping ON cm_cluster2panel_mapping.panel = t_panel.id
	inner join sdesign.t_cluster ON t_cluster.id = cm_cluster2panel_mapping."cluster"
	inner join sdesign.f_p_plot ON f_p_plot."cluster" = t_cluster.id
	left join @extschema@.cm_plot2cell_mapping ON (cm_plot2cell_mapping.estimation_cell = w_configuration.cell and cm_plot2cell_mapping.plot = f_p_plot.gid)
	inner join @extschema@.cm_plot2param_area_mapping ON (cm_plot2param_area_mapping.param_area = w_configuration.param_area and cm_plot2param_area_mapping.plot = f_p_plot.gid)
)
select * from w_plot
);';
analyze w_plot;

execute '
create temporary table w_ldsity_plot ON COMMIT DROP as (
	with w_plot AS NOT MATERIALIZED (select * from w_plot)
		select
			w_plot.gid,
			w_plot.conf_id,
			w_plot.stratum,
			w_plot.panel,
			w_plot.cluster,
			t_variable.id as attribute,
			t_cluster_configuration.plots_per_cluster,
			w_plot.plot_is_in_cell,
			coalesce(t_auxiliary_data.value, 0) as ldsity,
			true as is_aux, false as is_target,
			w_plot.geom
		from w_plot as w_plot
		inner join sdesign.cm_plot2cluster_config_mapping on w_plot.gid = cm_plot2cluster_config_mapping.plot
		inner join sdesign.t_cluster_configuration on cm_plot2cluster_config_mapping.cluster_configuration = t_cluster_configuration.id
		inner join w_configuration on w_plot.conf_id = w_configuration.id
		inner join @extschema@.t_variable on t_variable.id = ANY (w_configuration.aux_attributes)
		left join @extschema@.t_auxiliary_data on (
			w_plot.gid = t_auxiliary_data.plot and
			t_variable.id = t_auxiliary_data.variable and
			t_auxiliary_data.is_latest)
	union all
		select
			w_plot.gid,
			w_plot.conf_id,
			w_plot.stratum,
			w_plot.panel,
			w_plot.cluster,
			t_variable.id as attribute,
			t_cluster_configuration.plots_per_cluster,
			w_plot.plot_is_in_cell,
			coalesce(t_target_data.value, 0) as ldsity,
			false as is_aux, true as is_target,
			w_plot.geom
		from w_plot as w_plot
		inner join sdesign.cm_plot2cluster_config_mapping on w_plot.gid = cm_plot2cluster_config_mapping.plot
		inner join sdesign.t_cluster_configuration on cm_plot2cluster_config_mapping.cluster_configuration = t_cluster_configuration.id
		inner join w_configuration on w_plot.conf_id = w_configuration.id
                inner join @extschema@.t_variable on (t_variable.id = ANY (w_configuration.target_attributes))
		left join @extschema@.t_target_data on (
			w_plot.gid = t_target_data.plot and
			w_plot.reference_year_set = t_target_data.reference_year_set and
			t_variable.id = t_target_data.variable and
			t_target_data.is_latest)
);';
analyze w_ldsity_plot;

execute '
create temporary table w_ldsity_cluster ON COMMIT DROP as (
	SELECT
		w_ldsity_plot.conf_id,
		concat(w_ldsity_plot.conf_id, ROW_NUMBER() OVER ())::bigint as gid,
		w_ldsity_plot.cluster,
		w_ldsity_plot.attribute,
		w_ldsity_plot.stratum,
		w_ldsity_plot.panel,
		w_ldsity_plot.plots_per_cluster,
		count(*) as plcount,
		sum(w_ldsity_plot.plot_is_in_cell::int) > 0 as cluster_is_in_cell,
		sum(w_ldsity_plot.ldsity * plot_is_in_cell::int)/w_ldsity_plot.plots_per_cluster AS ldsity_D,
		sum(w_ldsity_plot.ldsity)/w_ldsity_plot.plots_per_cluster AS ldsity_D_plus,
		w_ldsity_plot.is_aux, w_ldsity_plot.is_target,
		st_collect(w_ldsity_plot.geom)::geometry(MultiPoint) as geom
	FROM       w_ldsity_plot
	GROUP BY conf_id, stratum, panel, cluster, plots_per_cluster, attribute, is_aux, is_target
	ORDER BY stratum, cluster, attribute
);';
analyze w_ldsity_cluster;

execute '
create temporary table w_clusters ON COMMIT DROP as (
	select distinct conf_id, stratum, panel, cluster, cluster_is_in_cell from w_ldsity_cluster
);';
analyze w_clusters;

execute '
create temporary table w_strata_sum ON COMMIT DROP as (
	select
		w_configuration.id as conf_id,
                t_stratum.id as f_a_sampling_stratum_gid,
		case when t_cluster_configuration.cluster_design then
			t_stratum.buffered_area_m2/10000
		else
			t_stratum.area_m2/10000
		end as lambda_d_plus,
		plots_per_cluster,
		count(*) as nb_sampling_units, sum (sampling_weight) as sweight_strata_sum
	from w_configuration
	inner join @extschema@.t_aux_conf on t_aux_conf.id = ANY (w_configuration.t_aux_conf__id)
	inner join @extschema@.t_panel2aux_conf ON t_panel2aux_conf.aux_conf = t_aux_conf.id
	inner join sdesign.t_panel ON t_panel.id = t_panel2aux_conf.panel
	inner join sdesign.t_cluster_configuration ON t_panel.cluster_configuration = t_cluster_configuration.id
	inner join sdesign.t_stratum ON t_stratum.id = t_panel.stratum
	inner join sdesign.cm_cluster2panel_mapping ON cm_cluster2panel_mapping.panel = t_panel.id
	group by w_configuration.id, f_a_sampling_stratum_gid, lambda_d_plus, plots_per_cluster
);';
analyze w_strata_sum;
---------------------------------------------------------
-- Matrix block
---------------------------------------------------------
execute '
create temporary table w_X ON COMMIT DROP AS (
	SELECT
		conf_id,
		attribute AS r,
		cluster AS c,
		ldsity_D as val_D,
		ldsity_D_plus as val
	FROM w_ldsity_cluster
	WHERE is_aux
);';
analyze w_X;

execute '
create temporary table w_Y ON COMMIT DROP AS (
	SELECT
		conf_id,
		attribute AS r,
		cluster AS c,
		ldsity_D as val_D,
		ldsity_D_plus as val
	FROM w_ldsity_cluster
	WHERE is_target
);';
analyze w_Y;

execute '
create temporary table w_pix ON COMMIT DROP as (
with
w_pix AS NOT MATERIALIZED (-------------------------INCLUSION DENSITY PIX
	SELECT
		w_clusters.conf_id,
		w_clusters.stratum,
		w_clusters.cluster,
		w_strata_sum.sweight_strata_sum / (w_strata_sum.lambda_d_plus * cm_cluster2panel_mapping.sampling_weight) as pix,
		cm_cluster2panel_mapping.sampling_weight as sweight
	FROM w_clusters
	INNER JOIN sdesign.cm_cluster2panel_mapping ON (w_clusters.cluster = cm_cluster2panel_mapping.cluster 
								and w_clusters.panel = cm_cluster2panel_mapping.panel)
	INNER JOIN w_strata_sum as w_strata_sum ON w_clusters.stratum = w_strata_sum.f_a_sampling_stratum_gid
)
select * from w_pix
);';
analyze w_pix;

execute '
create temporary table w_t_G_beta ON COMMIT DROP as (
        select
                w_configuration.id as conf_id,
                t_g_beta.variable as r, t_g_beta.cluster as c, t_g_beta.val
        from @extschema@.t_g_beta
        inner join w_configuration on (t_g_beta.aux_conf = w_configuration.t_aux_conf__id[1])
	where t_g_beta.is_latest
);';
analyze w_t_G_beta;

execute '
create temporary table w_PI ON COMMIT DROP as (
	SELECT
		conf_id,
		1 as r,
		cluster as c,
		1.0 / pix as val
	from w_pix
	order by r, c
);';
analyze w_PI;

execute '
create temporary table w_DELTA_G_beta ON COMMIT DROP as (
with w_I AS NOT MATERIALIZED (
	SELECT 
		conf_id,
		1 AS r,
		cluster AS c,
		cluster_is_in_cell::int AS val
	FROM	w_clusters
	order by r, c
)
, w_SIGMA AS NOT MATERIALIZED (
	SELECT
		conf_id,
		1 as r,
		cluster as c,
		case when w_configuration.sigma then (plots_per_cluster^2)::float / plcount::float 
		else 1::float end
		as val
	from (select conf_id, cluster, plots_per_cluster, plcount 
		from w_ldsity_cluster 
		group by conf_id, cluster, plots_per_cluster, plcount) as m
	inner join w_configuration on (m.conf_id = w_configuration.id)
	order by r, c
)
, w_SIGMA_PI AS NOT MATERIALIZED (
	SELECT
		A.conf_id,
		1 as r,
		A.c as c,
		A.val * B.val as val
	from w_SIGMA as A 
	inner join w_PI as B on (A.c = B.c and A.conf_id = B.conf_id) 
	order by r, c
)
, w_t_hat AS NOT MATERIALIZED (  -- eq. 18
	select 
		A.conf_id,
		A.r,
		1 AS c,
		sum(A.val_D * B.val * C.val) as val
	from w_X 		AS A 
	inner join w_PI     	AS B 	on (A.c = B.c and A.conf_id = B.conf_id)
	inner join w_I		AS C 	on (A.c = C.c and A.conf_id = C.conf_id)
	group by A.r, A.conf_id
	order by A.r
)
, w_t AS NOT MATERIALIZED (
	with w_aux_total as (
		select estimation_cell as cell, t_variable.id as attribute, aux_total
		from @extschema@.t_aux_total
		inner join @extschema@.t_variable on (t_aux_total.variable = t_variable.id)
		where t_aux_total.is_latest
	)
	, w_aux_ldsity as (
		select distinct conf_id, r as attribute from w_X
	)
	select
		w_cell_selection.conf_id,
		w_aux_total.attribute as r,
		1 as c,
		coalesce(w_aux_total.aux_total,
			@extschema@.fn_raise_notice(
				format(''fn_2p_data.w_t: t_aux_total not found! (conf_id:%s attribute:%s)'',
					w_cell_selection.conf_id,
					w_aux_ldsity.attribute),
				''exception''
			)::int::float
		) as val
	from
	w_aux_ldsity
	inner join w_cell_selection on (w_aux_ldsity.conf_id = w_cell_selection.conf_id)
	left join w_aux_total on (w_aux_ldsity.attribute = w_aux_total.attribute and w_cell_selection.estimation_cell = w_aux_total.cell)
)
, w_DELTA_T AS NOT MATERIALIZED (
	select
		conf_id,
		c as r, 
		r as c, 
		w_t.val - w_t_hat.val as val 
	from w_t 
	inner join w_t_hat using (r, c, conf_id)
)
, w_DELTA_G_beta AS NOT MATERIALIZED (
	select
		A.conf_id,
		A.r,
		B.c, 
		sum(A.val * B.val) as val
	FROM 	w_DELTA_T as A
	inner join w_t_G_beta as B ON (A.c = B.r and A.conf_id = B.conf_id)
	GROUP BY A.r, B.c, A.conf_id
	order by r, c
)
select * from w_DELTA_G_beta
);';
analyze w_DELTA_G_beta;

execute '
create temporary table w_X_beta ON COMMIT DROP as (
with w_Y_T AS NOT MATERIALIZED (
	SELECT
		conf_id,
		c as r,
		r as c,
		val,
		val_D -- used for 1p estimete
	from w_Y
	order by r, c
)
, w_G_PI AS NOT MATERIALIZED (
	select 
		A.conf_id,
		A.r, 
		A.c AS c,
		A.val * B.val as val
	from w_t_G_beta AS A, w_PI AS B 
	where A.c = B.c and A.conf_id = B.conf_id
	order by r, c
)
, w_beta_hat AS NOT MATERIALIZED (
	select
		A.conf_id,
		A.r,
		B.c, 
		sum(A.val * B.val) as val
	FROM w_G_PI as A, w_Y_T as B
	WHERE A.c = B.r and A.conf_id = B.conf_id
	GROUP BY A.r, B.c, A.conf_id
	order by r, c
)
, w_X_plots_T AS NOT MATERIALIZED (
	SELECT conf_id, gid AS r, cluster, attribute AS c, plots_per_cluster, ldsity AS val 
	FROM w_ldsity_plot WHERE is_aux
)
, w_X_beta AS NOT MATERIALIZED (
	select
		A.conf_id,
		A.r,
		B.c, 
		sum(A.val * B.val) as val
	FROM w_X_plots_T as A, w_beta_hat as B
	WHERE A.c = B.r and A.conf_id = B.conf_id
	GROUP BY A.r, B.c, A.conf_id
	order by r, c 
)
select * from w_X_beta
);';
analyze w_X_beta;

execute '
create temporary table w_ldsity_residuals_cluster ON COMMIT DROP as (
with w_Y_plots_T AS NOT MATERIALIZED (
	SELECT conf_id, gid AS r, cluster, attribute AS c, plots_per_cluster, ldsity AS val 
	FROM w_ldsity_plot WHERE is_target
)
, w_residuals_plot AS NOT MATERIALIZED ( -------------------------RESIDUAL LOCAL DENSITY ON TRACT LEVEL
	select
		w_Y_plots_T.conf_id,
		w_Y_plots_T.r AS plot, 
		w_Y_plots_T.c AS attribute, 
		(w_Y_plots_T.val - w_X_beta.val) AS ldsity 
	from w_Y_plots_T 
	INNER JOIN w_X_beta USING (r, c, conf_id)
)
, w_residuals_cluster AS NOT MATERIALIZED (
	with
	w_residuals_plot AS NOT MATERIALIZED (select * from w_residuals_plot),
	w_ldsity_plot AS NOT MATERIALIZED (select * from w_ldsity_plot)
	SELECT
		w_ldsity_plot.conf_id,
		w_ldsity_plot.cluster,
		w_residuals_plot.attribute,
		w_ldsity_plot.stratum,
		w_ldsity_plot.plots_per_cluster,
		count(*) as plcount,
		sum(plot_is_in_cell::int) > 0 as cluster_is_in_cell,
		sum(w_residuals_plot.ldsity * plot_is_in_cell::int)/w_ldsity_plot.plots_per_cluster AS ldsity_res_D, --eq 38
		sum(w_residuals_plot.ldsity)/w_ldsity_plot.plots_per_cluster AS ldsity_res_D_plus -- eq 39
	FROM w_ldsity_plot AS w_ldsity_plot
	INNER JOIN w_residuals_plot AS w_residuals_plot ON w_ldsity_plot.gid = w_residuals_plot.plot AND w_ldsity_plot.attribute = w_residuals_plot.attribute and w_ldsity_plot.conf_id = w_residuals_plot.conf_id
	GROUP BY stratum, w_ldsity_plot.conf_id, w_ldsity_plot.cluster, w_ldsity_plot.plots_per_cluster, w_residuals_plot.attribute
	ORDER BY stratum, w_ldsity_plot.conf_id, w_ldsity_plot.cluster, w_residuals_plot.attribute
)
, w_ldsity_residuals_cluster AS NOT MATERIALIZED (
	select 
		w_ldsity_cluster.conf_id, w_ldsity_cluster.gid, w_ldsity_cluster.cluster, 
		w_ldsity_cluster.attribute, w_ldsity_cluster.stratum, w_ldsity_cluster.plots_per_cluster, 
		w_ldsity_cluster.plcount, w_ldsity_cluster.cluster_is_in_cell, w_ldsity_cluster.ldsity_d, w_ldsity_cluster.ldsity_d_plus, 
		w_ldsity_cluster.is_aux, w_ldsity_cluster.is_target, w_ldsity_cluster.geom,
		w_residuals_cluster.ldsity_res_D, w_residuals_cluster.ldsity_res_D_plus
	from w_ldsity_cluster
	LEFT JOIN w_residuals_cluster USING (conf_id, cluster, attribute, stratum)
)
select * from w_ldsity_residuals_cluster
);';
analyze w_ldsity_residuals_cluster;

--EXPLAIN (ANALYZE, COSTS, VERBOSE, BUFFERS/*, FORMAT JSON*/)
return query execute '
with w_2p_data AS NOT MATERIALIZED (
	with w_ldsity_residuals_cluster AS NOT MATERIALIZED (select * from w_ldsity_residuals_cluster)
	select 
		w_ldsity_residuals_cluster.conf_id, w_ldsity_residuals_cluster.gid, w_ldsity_residuals_cluster.cluster, 
		w_ldsity_residuals_cluster.attribute, w_ldsity_residuals_cluster.stratum, w_ldsity_residuals_cluster.plots_per_cluster, 
		w_ldsity_residuals_cluster.plcount, w_ldsity_residuals_cluster.cluster_is_in_cell, w_ldsity_residuals_cluster.ldsity_d, 
		w_ldsity_residuals_cluster.ldsity_d_plus, 
		w_ldsity_residuals_cluster.is_aux, w_ldsity_residuals_cluster.is_target, w_ldsity_residuals_cluster.geom,
		w_ldsity_residuals_cluster.ldsity_res_D, w_ldsity_residuals_cluster.ldsity_res_D_plus,
		w_pix.pix, w_pix.sweight, w_DELTA_G_beta.val as DELTA_T__G_beta, 
		w_strata_sum.nb_sampling_units::integer, w_strata_sum.sweight_strata_sum, w_strata_sum.lambda_d_plus
	from w_ldsity_residuals_cluster
	INNER JOIN w_pix USING (conf_id, cluster, stratum)
	INNER JOIN w_DELTA_G_beta ON (w_pix.conf_id = w_DELTA_G_beta.conf_id and cluster = w_DELTA_G_beta.c)
	inner join w_strata_sum on (stratum = f_a_sampling_stratum_gid)
)
, w_2p_data_sigma AS NOT MATERIALIZED (
	select 
		w_2p_data.gid, w_2p_data.cluster, w_2p_data.attribute, w_2p_data.stratum, w_2p_data.plots_per_cluster, w_2p_data.plcount,
		w_2p_data.cluster_is_in_cell as cluster_is_in_cell, 
		w_2p_data.ldsity_d, w_2p_data.ldsity_d_plus, w_2p_data.is_aux, w_2p_data.is_target,
		w_2p_data.geom, w_2p_data.ldsity_res_d, w_2p_data.ldsity_res_d_plus, w_2p_data.pix, w_2p_data.sweight, w_2p_data.DELTA_T__G_beta,
		w_2p_data.nb_sampling_units, w_2p_data.sweight_strata_sum, w_2p_data.lambda_d_plus, w_configuration.sigma
	from w_2p_data 
	inner join w_configuration on (w_2p_data.conf_id = w_configuration.id)
)
select * from w_2p_data_sigma;';

drop table w_configuration;
drop table w_cell_selection;
drop table w_plot;
drop table w_ldsity_plot;
drop table w_ldsity_cluster;
drop table w_clusters;
drop table w_strata_sum;
drop table w_X;
drop table w_Y;
drop table w_pix;
drop table w_t_G_beta;
drop table w_PI;
drop table w_DELTA_G_beta;
drop table w_X_beta;
drop table w_ldsity_residuals_cluster;
end;
$$
  LANGUAGE plpgsql
  COST 100
  ROWS 100000;

COMMENT ON FUNCTION @extschema@.fn_2p_data(integer) IS 'Function preparing data for regression estimate.';

-- </function>
-- <function_name="" function_schema="extschema" src="functions/extschema/fn_g_beta.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_G_beta(integer)

-- DROP FUNCTION @extschema@.fn_G_beta(integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_G_beta(
    IN conf_id integer
)
  RETURNS TABLE(
	variable integer,
	cluster integer,
	val double precision
) AS
$BODY$
DECLARE
	_complete_query text;
BEGIN
	--------------------------------QUERY--------------------------------
	_complete_query := '
WITH
w_configuration AS MATERIALIZED (
	select 
		t_aux_conf.id, t_aux_conf.param_area, t_aux_conf.model, t_aux_conf.sigma, t_model.description,
		array_agg(t_model_variables.variable order by t_model_variables.variable) as aux_attributes
	from @extschema@.t_aux_conf 
	inner join @extschema@.t_model ON t_model.id = t_aux_conf.model
	inner join @extschema@.t_model_variables ON t_model_variables.model = t_model.id
        where t_aux_conf.id = ' || conf_id || '
	group by t_aux_conf.id, t_aux_conf.param_area, t_aux_conf.model, t_model.description
)
, w_param_area_selection AS MATERIALIZED (
	SELECT
		w_configuration.id as conf_id,
		gid as param_area_gid,
		geom
	FROM @extschema@.f_a_param_area
	INNER JOIN w_configuration ON (w_configuration.param_area = f_a_param_area.gid)
)
, w_plot AS MATERIALIZED (-------------------------LIST OF PLOTS IN PARAMETRIZATION AREA
	select distinct 
		w_param_area_selection.conf_id, f_p_plot.gid, t_cluster.id as cluster, t_panel.stratum,
		t_panel2aux_conf.panel, 
		f_p_plot.geom
	from w_param_area_selection 
	inner join @extschema@.t_panel2aux_conf on w_param_area_selection.conf_id = t_panel2aux_conf.aux_conf
	inner join sdesign.t_panel ON t_panel.id = t_panel2aux_conf.panel
	inner join sdesign.cm_cluster2panel_mapping ON cm_cluster2panel_mapping.panel = t_panel.id
	inner join sdesign.t_cluster ON t_cluster.id = cm_cluster2panel_mapping."cluster"
	inner join sdesign.f_p_plot ON f_p_plot."cluster" = t_cluster.id
	inner join @extschema@.cm_plot2param_area_mapping ON cm_plot2param_area_mapping.plot = f_p_plot.gid
	inner join @extschema@.f_a_param_area ON (f_a_param_area.gid = cm_plot2param_area_mapping.param_area and w_param_area_selection.param_area_gid = f_a_param_area.gid)
)
, w_ldsity_plot AS MATERIALIZED (
	SELECT
		w_plot.conf_id,
		w_plot.gid,
		w_plot.stratum,
                w_plot.panel,
		w_plot.cluster,
		t_variable.id as attribute,
		t_cluster_configuration.plots_per_cluster,
		coalesce(t_auxiliary_data.value, 0) as ldsity,
		w_plot.geom,
		true as is_aux
	FROM w_plot
	inner join sdesign.cm_plot2cluster_config_mapping on w_plot.gid = cm_plot2cluster_config_mapping.plot
	inner join sdesign.t_cluster_configuration on cm_plot2cluster_config_mapping.cluster_configuration = t_cluster_configuration.id
	inner join w_configuration on w_plot.conf_id = w_configuration.id
	left join @extschema@.t_variable on t_variable.id = ANY (w_configuration.aux_attributes)
	left join @extschema@.t_auxiliary_data on (
		w_plot.gid = t_auxiliary_data.plot and
		t_variable.id = t_auxiliary_data.variable and
		t_auxiliary_data.is_latest)
)
, w_ldsity_cluster AS MATERIALIZED (
 	SELECT
		w_ldsity_plot.conf_id,
		concat(w_ldsity_plot.conf_id, ROW_NUMBER() OVER ())::bigint as gid, 
		w_ldsity_plot.cluster,
		w_ldsity_plot.attribute,
		w_ldsity_plot.stratum,
                w_ldsity_plot.panel,
		w_ldsity_plot.plots_per_cluster,
		count(*) as plcount,
		sum(w_ldsity_plot.ldsity)/w_ldsity_plot.plots_per_cluster AS ldsity_D_plus, -- eq 15,
		w_ldsity_plot.is_aux, --
		st_collect(w_ldsity_plot.geom)::geometry(MultiPoint) as geom
	FROM       w_ldsity_plot
	GROUP BY conf_id, stratum, panel, cluster, plots_per_cluster, attribute, is_aux
	ORDER BY stratum, cluster, attribute
)
, w_X AS MATERIALIZED ( -------------------------AUX LOCAL DENSITY ON TRACT LEVEL
	SELECT
		conf_id,
		attribute AS r,
		cluster AS c,
		ldsity_D_plus as val
	FROM w_ldsity_cluster
	WHERE is_aux
)
, w_clusters AS MATERIALIZED (-------------------------LIST OF TRACTS
	select distinct conf_id, stratum, panel, cluster from w_ldsity_cluster
)
, w_strata_sum AS MATERIALIZED (
	select
		w_configuration.id as conf_id,
                t_stratum.id as f_a_sampling_stratum_gid,
		case when t_cluster_configuration.cluster_design then
			t_stratum.buffered_area_m2/10000
		else
			t_stratum.area_m2/10000
		end as lambda_d_plus,
		plots_per_cluster,
		count(*) as nb_sampling_units, sum (sampling_weight) as sweight_strata_sum
	from w_configuration
	inner join @extschema@.t_aux_conf on t_aux_conf.id = w_configuration.id
	inner join @extschema@.t_panel2aux_conf ON t_panel2aux_conf.aux_conf = t_aux_conf.id
	inner join sdesign.t_panel ON t_panel.id = t_panel2aux_conf.panel
	inner join sdesign.t_cluster_configuration ON t_panel.cluster_configuration = t_cluster_configuration.id
	inner join sdesign.t_stratum ON t_stratum.id = t_panel.stratum
	inner join sdesign.cm_cluster2panel_mapping ON cm_cluster2panel_mapping.panel = t_panel.id
	group by w_configuration.id, f_a_sampling_stratum_gid, lambda_d_plus, plots_per_cluster
)
, w_pix AS MATERIALIZED (-------------------------INCLUSION DENSITY PIX
	SELECT
		w_clusters.conf_id,
		w_clusters.stratum,
		w_clusters.cluster,
		v_strata_sum.sweight_strata_sum / (v_strata_sum.lambda_d_plus * cm_cluster2panel_mapping.sampling_weight) as pix
	FROM w_clusters
        INNER JOIN sdesign.cm_cluster2panel_mapping ON (w_clusters.cluster = cm_cluster2panel_mapping.cluster
								and w_clusters.panel = cm_cluster2panel_mapping.panel)
	INNER JOIN w_strata_sum as v_strata_sum ON w_clusters.stratum = v_strata_sum.f_a_sampling_stratum_gid
)
, w_SIGMA AS MATERIALIZED (
	SELECT
		conf_id,
		1 as r,
		cluster as c,
		case when w_configuration.sigma then (plots_per_cluster^2)::float / plcount::float 
		else 1::float end
		as val
	from (select conf_id, cluster, plots_per_cluster, plcount 
		from w_ldsity_cluster 
		group by conf_id, cluster, plots_per_cluster, plcount) as m
	inner join w_configuration on (m.conf_id = w_configuration.id)
	order by r, c
)
, w_PI AS MATERIALIZED (
	SELECT
		conf_id,
		1 as r,
		cluster as c,
		1.0 / pix as val
	from w_pix
	order by r, c
)
, w_SIGMA_PI AS MATERIALIZED (
	SELECT
		A.conf_id,
		1 as r,
		A.c as c,
		A.val * B.val as val
	from w_SIGMA as A inner join w_PI as B on (A.c = B.c and A.conf_id = B.conf_id) 
	order by r, c
)
, w_XT AS MATERIALIZED (
	SELECT
		conf_id,
		c as r,
		r as c,
		val as val
	from w_X
	order by r, c
)
, w_X_SIGMA_PI AS MATERIALIZED ( -- element-wise multiplication
	select 
		A.conf_id,
		A.r, 
		A.c,
		A.val * B.val as val
	from w_X as A
	inner join w_SIGMA_PI as B on (A.c = B.c and A.conf_id = B.conf_id)
	ORDER BY r, c
)
, w_X_SIGMA_PI_XT AS MATERIALIZED ( -- matrix multiplication
	SELECT 
		A.conf_id,
		ROW_NUMBER() OVER (partition by A.conf_id order by A.r, B.c) AS mid,
		A.r, 
		B.c, 
		sum(A.val * B.val) as val
	FROM w_X_SIGMA_PI as A, w_XT as B
	WHERE A.c = B.r and A.conf_id = B.conf_id
	GROUP BY A.r, B.c, A.conf_id
	ORDER BY r, c
)
, w_agg AS MATERIALIZED (
	select conf_id, r, array_agg(val order by c) as val from w_X_SIGMA_PI_XT group by conf_id, r
)
, w_aggagg AS MATERIALIZED (
	select conf_id, array_agg(val order by r) as val from w_agg group by conf_id
)
, w_inv AS MATERIALIZED (
	select conf_id, @extschema@.fn_inverse(val) AS val from w_aggagg
)
, w_inv_id AS MATERIALIZED (
	select conf_id, mid, invval from w_inv, unnest(w_inv.val) WITH ORDINALITY AS t(invval, mid)
)
, w_X_SIGMA_PI_XT_inv AS MATERIALIZED (
	SELECT 
		conf_id, r, c, invval as val 
	FROM w_X_SIGMA_PI_XT 
	inner join w_inv_id using (conf_id, mid)
	ORDER BY r, c
)
, w_X_SIGMA_PI_XT_inv_X AS MATERIALIZED (
	select
		A.conf_id,
		A.r,
		B.c,
		sum(A.val * B.val) as val
	FROM
		w_X_SIGMA_PI_XT_inv as A, w_X as B
	WHERE A.c = B.r and A.conf_id = B.conf_id
	GROUP BY A.r, B.c, A.conf_id
	ORDER BY r, c
)
, w_G_beta AS MATERIALIZED (
	select
		A.conf_id,
		A.r,
		A.c,
		A.val * B.val as val
	FROM
		w_X_SIGMA_PI_XT_inv_X as A, w_SIGMA as B
	WHERE A.c = B.c and A.conf_id = B.conf_id
	ORDER BY r, c
)
select r as variable, c as cluster, val from w_G_beta;
        ';

	--RAISE NOTICE '%', _complete_query;
	RETURN QUERY EXECUTE _complete_query;
END;
$BODY$
  LANGUAGE plpgsql STABLE
  COST 100
  ROWS 100000;
;

COMMENT ON FUNCTION @extschema@.fn_G_beta(integer) IS 'Function computing matrix G_beta used for regression estimators. G_beta is dependent on model and parametrization domain (not cell). Matrix is represented by variable (row) and cluster (column) indices.';

-- </function>
-- <view_name="v_conf_overview" view_schema="extschema" src="views/extschema/v_conf_overview.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- drop view if exists @extschema@.v_conf_overview;
create or replace view @extschema@.v_conf_overview as (
with w_confs AS MATERIALIZED (
	select
		t_estimate_conf.id as estimate_conf,
		t_total_estimate_conf.id as total_estimate_conf,
		t_aux_conf.id as aux_conf,
		t_total_estimate_conf_denom.id as total_estimate_conf__denom,
		t_aux_conf_denom.id as aux_conf__denom,
		case 	when 
				t_total_estimate_conf.id is not null and 
				t_aux_conf.id is null and 
				t_total_estimate_conf_denom.id is null and 
				t_aux_conf_denom.id is null 
			then '1p_total'
			when 
				t_total_estimate_conf.id is not null and 
				t_aux_conf.id is null and 
				t_total_estimate_conf_denom.id is not null and 
				t_aux_conf_denom.id is null 
			then '1p1p_ratio'
			when 
				t_total_estimate_conf.id is not null and 
				t_aux_conf.id is not null and 
				t_total_estimate_conf_denom.id is null and 
				t_aux_conf_denom.id is null 
			then '2p_total'
			when 
				t_total_estimate_conf.id is not null and 
				t_aux_conf.id is not null and 
				t_total_estimate_conf_denom.id is not null and 
				t_aux_conf_denom.id is null 
			then '2p1p_ratio'
			when 
				t_total_estimate_conf.id is not null and 
				t_aux_conf.id is null and 
				t_total_estimate_conf_denom.id is not null and 
				t_aux_conf_denom.id is not null 
			then '1p2p_ratio'
			when 
				t_total_estimate_conf.id is not null and 
				t_aux_conf.id is not null and 
				t_total_estimate_conf_denom.id is not null and 
				t_aux_conf_denom.id is not null 
			then '2p2p_ratio'
			else 'unknown'
		end as estimate_type_str,
		coalesce(t_aux_conf.sigma, t_aux_conf_denom.sigma) as sigma,
		coalesce(t_total_estimate_conf.force_synthetic, t_total_estimate_conf_denom.force_synthetic) as force_synthetic,
------------------------additional info begin-----------------------------------
		coalesce(t_aux_conf.param_area, t_aux_conf_denom.param_area) as param_area, 
		coalesce(f_a_param_area.param_area_code, f_a_param_area_denom.param_area_code) as param_area_code,
		coalesce(t_aux_conf.model, t_aux_conf_denom.model) as model, 
		coalesce(t_model.description, t_model_denom.description) as model_description,
		t_total_estimate_conf.estimation_cell, c_estimation_cell.label as estimation_cell_label, c_estimation_cell.estimation_cell_collection, --f_a_cell.geom,
		t_variable.id as variable, t_variable.target_variable, c_target_variable.label as target_variable_label, 
		t_variable.sub_population_category, c_sub_population_category.label as sub_population_category_label,
		t_variable.area_domain_category, c_area_domain_category.label as area_domain_category_label,
		t_total_estimate_conf.estimate_date_begin, t_total_estimate_conf.estimate_date_end
------------------------additional info end-------------------------------------
	from @extschema@.t_estimate_conf
        inner join @extschema@.t_total_estimate_conf ON t_total_estimate_conf.id = t_estimate_conf.total_estimate_conf
	left join @extschema@.t_aux_conf ON t_aux_conf.id = t_total_estimate_conf.aux_conf
        left join @extschema@.t_total_estimate_conf as t_total_estimate_conf_denom ON t_total_estimate_conf_denom.id = t_estimate_conf.denominator
	left join @extschema@.t_aux_conf as t_aux_conf_denom ON t_aux_conf_denom.id = t_total_estimate_conf_denom.aux_conf
------------------------additional info begin-----------------------------------
	left join @extschema@.f_a_param_area on (t_aux_conf.param_area = f_a_param_area.gid)
	left join @extschema@.t_model on (t_aux_conf.model = t_model.id)
	left join @extschema@.f_a_param_area as f_a_param_area_denom on (t_aux_conf_denom.param_area = f_a_param_area_denom.gid)
	left join @extschema@.t_model as t_model_denom on (t_aux_conf_denom.model = t_model_denom.id)
	inner join @extschema@.c_estimation_cell on (t_total_estimate_conf.estimation_cell = c_estimation_cell.id)
	inner join @extschema@.t_variable on (t_total_estimate_conf.target_variable = t_variable.id)
	inner join @extschema@.c_target_variable on (t_variable.target_variable = c_target_variable.id)
	left join @extschema@.c_sub_population_category on (t_variable.sub_population_category = c_sub_population_category.id)
	left join @extschema@.c_area_domain_category on (t_variable.area_domain_category = c_area_domain_category.id)
------------------------additional info end-------------------------------------
	order by t_estimate_conf.id
)
, w_confs_2nd_panels AS MATERIALIZED (
	select w_confs.*,
		array_agg(t_panel2total_2ndph_estimate_conf.panel order by t_panel2total_2ndph_estimate_conf.panel) as t_panel2total_2ndph_estimate_conf_panels,
		array_agg(t_panel2total_2ndph_estimate_conf.reference_year_set order by t_panel2total_2ndph_estimate_conf.panel) as est_data_2ndph_ref_year_sets
	from w_confs
	inner join @extschema@.t_panel2total_2ndph_estimate_conf on (t_panel2total_2ndph_estimate_conf.total_estimate_conf = w_confs.total_estimate_conf)
	group by w_confs.estimate_conf, w_confs.total_estimate_conf, w_confs.aux_conf, w_confs.total_estimate_conf__denom, w_confs.aux_conf__denom,
			w_confs.estimate_type_str, w_confs.sigma, w_confs.force_synthetic,
			w_confs.param_area, w_confs.param_area_code,
			w_confs.model, w_confs.model_description,
			w_confs.estimation_cell, w_confs.estimation_cell_label, w_confs.estimation_cell_collection,
			w_confs.variable, w_confs.target_variable, w_confs.target_variable_label,
			w_confs.sub_population_category, w_confs.sub_population_category_label, w_confs.area_domain_category, w_confs.area_domain_category_label,
			w_confs.estimate_date_begin, w_confs.estimate_date_end
)
	select w_confs_2nd_panels.*,
		array_agg(t_panel2aux_conf.panel order by t_panel2aux_conf.panel) as t_panel2aux_conf_panels
	from w_confs_2nd_panels
	left join @extschema@.t_panel2aux_conf on (t_panel2aux_conf.aux_conf = w_confs_2nd_panels.aux_conf)
	group by w_confs_2nd_panels.estimate_conf, w_confs_2nd_panels.total_estimate_conf, w_confs_2nd_panels.aux_conf, w_confs_2nd_panels.total_estimate_conf__denom, w_confs_2nd_panels.aux_conf__denom,
			w_confs_2nd_panels.estimate_type_str, w_confs_2nd_panels.sigma, w_confs_2nd_panels.force_synthetic,
			w_confs_2nd_panels.param_area, w_confs_2nd_panels.param_area_code,
			w_confs_2nd_panels.model, w_confs_2nd_panels.model_description,
			w_confs_2nd_panels.estimation_cell, w_confs_2nd_panels.estimation_cell_label, w_confs_2nd_panels.estimation_cell_collection, w_confs_2nd_panels.variable, w_confs_2nd_panels.target_variable, w_confs_2nd_panels.target_variable_label,
			w_confs_2nd_panels.sub_population_category, w_confs_2nd_panels.sub_population_category_label, w_confs_2nd_panels.area_domain_category, w_confs_2nd_panels.area_domain_category_label,
			w_confs_2nd_panels.estimate_date_begin, w_confs_2nd_panels.estimate_date_end,
			w_confs_2nd_panels.t_panel2total_2ndph_estimate_conf_panels, w_confs_2nd_panels.est_data_2ndph_ref_year_sets

);
--select * from @extschema@.v_conf_overview;

GRANT SELECT ON TABLE @extschema@.v_conf_overview TO PUBLIC;

-- </view>

-------------------------------------------------DELETE OBSOLETE TABLES------------------------

drop table @extschema@.cm_refyearset2panel_mapping;
drop table @extschema@.t_plot_measurement_dates;
drop table @extschema@.t_reference_year_set;
drop table @extschema@.t_inventory_campaign;
drop table @extschema@.cm_plot2cluster_config_mapping;
drop table @extschema@.cm_cluster2panel_mapping;
drop table @extschema@.f_p_plot;
drop table @extschema@.t_cluster;
drop table @extschema@.t_panel;
drop table @extschema@.t_cluster_configuration;
drop table @extschema@.t_stratum;
drop table @extschema@.t_strata_set;
drop table @extschema@.c_country;
