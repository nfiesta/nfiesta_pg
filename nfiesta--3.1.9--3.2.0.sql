--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- t_available_datasets => add column ldsity_threshold and UNIQUE KEY
---------------------------------------------------------------------------------------------------
alter table @extschema@.t_available_datasets add COLUMN ldsity_threshold double precision;
comment on column @extschema@.t_available_datasets.ldsity_threshold IS 'Saved threshold for given combination panel, reference_year_set and variable.';

alter table @extschema@.t_additivity_set_plot rename column add_check_treshold TO add_check_threshold;

-- <function name="fn_add_triggers" schema="extschema" src="functions/extschema/additivity/fn_add_triggers.sql">
--
-- Copyright 2021, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

CREATE OR REPLACE FUNCTION @extschema@.fn_prevent_update()
  RETURNS trigger AS
$BODY$
    BEGIN
        RAISE EXCEPTION 'fn_prevent_update -- UPDATE -- only update of is_latests is possible';
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

drop trigger if exists trg__target_data__pr_upd ON @extschema@.t_target_data;
CREATE TRIGGER trg__target_data__pr_upd
BEFORE UPDATE OF id, plot, value, value_inserted, available_datasets -- only update of is_latests is possible
ON @extschema@.t_target_data
FOR EACH ROW
EXECUTE PROCEDURE @extschema@.fn_prevent_update();

--------------------------------------------------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION @extschema@.fn_check_update()
  RETURNS trigger AS
$BODY$
    BEGIN
	IF (old.is_latest = false AND new.is_latest = true) THEN
	        RAISE EXCEPTION 'fn_check_update -- UPDATE -- only set is_latest to false is possible. use INSERT for new valid values';
	END IF;
	RETURN NEW;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

drop trigger if exists trg__target_data__ch_upd ON @extschema@.t_target_data;
CREATE TRIGGER trg__target_data__ch_upd
AFTER UPDATE OF is_latest
ON @extschema@.t_target_data
FOR EACH ROW
EXECUTE PROCEDURE @extschema@.fn_check_update();

--------------------------------------------------------------------------------------------------------------------------------

--drop function @extschema@.fn_update_last_change() cascade;
CREATE OR REPLACE FUNCTION @extschema@.fn_update_last_change() RETURNS TRIGGER AS $src$
    DECLARE
    BEGIN
	IF (TG_TABLE_NAME = 't_target_data' or TG_TABLE_NAME = 't_auxiliary_data') THEN
        	IF (TG_OP = 'DELETE') THEN
			RAISE EXCEPTION 'fn_update_last_change -- DELETE not permited (UPDATE to is_latest = false instead)';
		ELSIF (TG_OP = 'UPDATE') THEN
			with w_data as (
				select 
					available_datasets, now() as max_val_upd
				from trans_table
				group by available_datasets
			)
			update @extschema@.t_available_datasets 
			set last_change = w_data.max_val_upd
			from w_data
			where t_available_datasets.id = w_data.available_datasets
			;
			/*raise notice '%		fn_update_last_change -- % -- %: () %', 
				clock_timestamp()::timestamp with time zone at time zone 'Europe/Prague', 
				TG_TABLE_NAME, TG_OP, 
				(select json_agg(r) 
					from (
						select
							json_build_object(
								'available_datasets', available_datasets, 
								'max_val_upd', now()
							) as r
						from trans_table
						group by available_datasets
					) as foo
				);*/
		ELSIF (TG_OP = 'INSERT') THEN
			with w_data as (
				select 
					available_datasets, max(value_inserted) as max_val_ins
				from trans_table
				group by available_datasets
			)
			update @extschema@.t_available_datasets 
			set last_change = w_data.max_val_ins
			from w_data
			where t_available_datasets.id = w_data.available_datasets
			;
			/*raise notice '%		fn_update_last_change -- % -- %: () %', 
				clock_timestamp()::timestamp with time zone at time zone 'Europe/Prague', 
				TG_TABLE_NAME, TG_OP, 
				(select json_agg(r)
					from (
						select 
							json_build_object(
								'available_datasets', available_datasets, 
								'max_val_ins', max(value_inserted)
						) as r
						from trans_table
						group by available_datasets
					) as foo
				);*/
		ELSE
			RAISE EXCEPTION 'fn_update_last_change -- trigger operation not known: %', TG_OP;
		END IF;
	ELSE
		RAISE EXCEPTION 'fn_update_last_change -- % -- table name not known: %', TG_OP, TG_TABLE_NAME;
	END IF;
        RETURN NULL; -- result is ignored since this is an AFTER trigger
    END;
$src$ LANGUAGE plpgsql;

drop trigger if exists trg__target_data__ins ON @extschema@.t_target_data;
CREATE TRIGGER trg__target_data__ins
    AFTER INSERT ON @extschema@.t_target_data
    REFERENCING NEW TABLE AS trans_table
    FOR EACH STATEMENT EXECUTE FUNCTION @extschema@.fn_update_last_change();

drop trigger if exists trg__target_data__upd ON @extschema@.t_target_data;
CREATE TRIGGER trg__target_data__upd
    AFTER UPDATE ON @extschema@.t_target_data
    REFERENCING NEW TABLE AS trans_table
    FOR EACH STATEMENT EXECUTE FUNCTION @extschema@.fn_update_last_change();

drop trigger if exists trg__target_data__del ON @extschema@.t_target_data;
CREATE TRIGGER trg__target_data__del
    AFTER DELETE ON @extschema@.t_target_data
    REFERENCING OLD TABLE AS trans_table
    FOR EACH STATEMENT EXECUTE FUNCTION @extschema@.fn_update_last_change();

---------------------------------------------
drop trigger if exists trg__auxiliary_data__ins ON @extschema@.t_auxiliary_data;
CREATE TRIGGER trg__auxiliary_data__ins
    AFTER INSERT ON @extschema@.t_auxiliary_data
    REFERENCING NEW TABLE AS trans_table
    FOR EACH STATEMENT EXECUTE FUNCTION @extschema@.fn_update_last_change();

drop trigger if exists trg__auxiliary_data__upd ON @extschema@.t_auxiliary_data;
CREATE TRIGGER trg__auxiliary_data__upd
    AFTER UPDATE ON @extschema@.t_auxiliary_data
    REFERENCING NEW TABLE AS trans_table
    FOR EACH STATEMENT EXECUTE FUNCTION @extschema@.fn_update_last_change();

drop trigger if exists trg__auxiliary_data__del ON @extschema@.t_auxiliary_data;
CREATE TRIGGER trg__auxiliary_data__del
    AFTER DELETE ON @extschema@.t_auxiliary_data
    REFERENCING OLD TABLE AS trans_table
    FOR EACH STATEMENT EXECUTE FUNCTION @extschema@.fn_update_last_change();


---------------------------------------------

CREATE OR REPLACE FUNCTION @extschema@.fn_refresh_t_additivity_set_plot()
RETURNS trigger LANGUAGE plpgsql AS $$
BEGIN
	--RAISE NOTICE 'fn_refresh_t_additivity_set_plot -- % -- %', TG_OP, TG_TABLE_NAME;
	IF (TG_TABLE_NAME = 't_variable_hierarchy') THEN
    	IF (TG_OP = 'DELETE') THEN
        	RAISE EXCEPTION 'fn_refresh_t_additivity_set_plot -- DELETE on table t_variable_hierarchy not permited';
        ELSIF (TG_OP = 'INSERT' OR TG_OP = 'UPDATE') THEN
			with w_data as (
				SELECT 	node_ads.id as node_ads_id,
					t_variable_hierarchy.variable_superior AS node,
					array_agg(edge_ads.id) as edge_ads_id,
					array_agg(t_variable_hierarchy.variable ORDER BY t_variable_hierarchy.variable) AS edges,
					node_ads.ldsity_threshold * 100 as add_check_threshold
				FROM @extschema@.t_variable_hierarchy
				JOIN @extschema@.t_variable 				AS node_var ON t_variable_hierarchy.variable_superior = node_var.id
				JOIN @extschema@.t_available_datasets 			AS node_ads ON node_var.id = node_ads.variable
				JOIN @extschema@.t_variable 				AS edge_var ON t_variable_hierarchy.variable = edge_var.id
				JOIN @extschema@.t_available_datasets 			AS edge_ads ON edge_var.id = edge_ads.variable
				LEFT JOIN @extschema@.c_sub_population_category		ON edge_var.sub_population_category = c_sub_population_category.id
				LEFT JOIN @extschema@.c_area_domain_category 		ON edge_var.area_domain_category = c_area_domain_category.id
				LEFT JOIN @extschema@.c_auxiliary_variable_category	ON edge_var.auxiliary_variable_category = c_auxiliary_variable_category.id
				WHERE
					node_ads.panel = edge_ads.panel
					AND
					(
							(node_ads.reference_year_set = edge_ads.reference_year_set
							AND node_var.auxiliary_variable_category IS NULL
							AND edge_var.auxiliary_variable_category IS NULL)
						OR
							(node_ads.reference_year_set IS NULL and edge_ads.reference_year_set IS NULL
							AND node_var.auxiliary_variable_category IS NOT NULL
							AND edge_var.auxiliary_variable_category IS NOT NULL)
					)
				GROUP BY 	node_ads.id, node_ads.panel, node_ads.reference_year_set, t_variable_hierarchy.variable_superior,
						c_sub_population_category.sub_population, c_area_domain_category.area_domain, c_auxiliary_variable_category.auxiliary_variable
				ORDER BY 	node_ads.id, node_ads.panel, node_ads.reference_year_set, t_variable_hierarchy.variable_superior,
						c_sub_population_category.sub_population, c_area_domain_category.area_domain, c_auxiliary_variable_category.auxiliary_variable
			),
			w_distinct_selection as (
				select distinct w_data.node_ads_id, w_data.edges, w_data.add_check_threshold
				from w_data join trans_table on
				(
					trans_table.variable_superior = w_data.node
					OR
					trans_table.variable = any (w_data.edges)
				)
			)
			INSERT INTO @extschema@.t_additivity_set_plot(available_datasets, edges, add_check_threshold, dbg_data)
			select
				node_ads_id, edges, add_check_threshold,
				json_build_object(
					'fn_refresh_t_additivity_set_plot', json_build_object(
						'TG_TABLE_NAME', TG_TABLE_NAME, 
						'TG_OP', TG_OP, 
						'trigerred_time', (clock_timestamp()::timestamp with time zone)::text
						)
					) as dbg_data
			from w_distinct_selection
			;
		ELSE
				RAISE EXCEPTION 'fn_refresh_t_additivity_set_plot -- trigger operation not known: %', TG_OP;
		END IF;
	ELSIF (TG_TABLE_NAME = 't_available_datasets') THEN
	IF (TG_OP = 'DELETE') THEN
		RAISE EXCEPTION 'fn_refresh_t_additivity_set_plot -- DELETE on table t_available_datasets not permited';
	ELSIF (TG_OP = 'INSERT' OR TG_OP = 'UPDATE') THEN
			with w_data as (
				SELECT 	node_ads.id as node_ads_id,
					array_agg(edge_ads.id) as edge_ads_id,
					array_agg(t_variable_hierarchy.variable ORDER BY t_variable_hierarchy.variable) AS edges,
					node_ads.ldsity_threshold * 100 as add_check_threshold
				FROM @extschema@.t_variable_hierarchy
				JOIN @extschema@.t_variable 				AS node_var ON t_variable_hierarchy.variable_superior = node_var.id
				JOIN @extschema@.t_available_datasets 			AS node_ads ON node_var.id = node_ads.variable
				JOIN @extschema@.t_variable 				AS edge_var ON t_variable_hierarchy.variable = edge_var.id
				JOIN @extschema@.t_available_datasets 			AS edge_ads ON edge_var.id = edge_ads.variable
				LEFT JOIN @extschema@.c_sub_population_category		ON edge_var.sub_population_category = c_sub_population_category.id
				LEFT JOIN @extschema@.c_area_domain_category 		ON edge_var.area_domain_category = c_area_domain_category.id
				LEFT JOIN @extschema@.c_auxiliary_variable_category	ON edge_var.auxiliary_variable_category = c_auxiliary_variable_category.id
				WHERE
					node_ads.panel = edge_ads.panel
					AND
					(
							(node_ads.reference_year_set = edge_ads.reference_year_set
							AND node_var.auxiliary_variable_category IS NULL
							AND edge_var.auxiliary_variable_category IS NULL)
						OR
							(node_ads.reference_year_set IS NULL and edge_ads.reference_year_set IS NULL
							AND node_var.auxiliary_variable_category IS NOT NULL
							AND edge_var.auxiliary_variable_category IS NOT NULL)
					)
				GROUP BY 	node_ads.id, node_ads.panel, node_ads.reference_year_set, t_variable_hierarchy.variable_superior,
						c_sub_population_category.sub_population, c_area_domain_category.area_domain, c_auxiliary_variable_category.auxiliary_variable
				ORDER BY 	node_ads.id, node_ads.panel, node_ads.reference_year_set, t_variable_hierarchy.variable_superior,
						c_sub_population_category.sub_population, c_area_domain_category.area_domain, c_auxiliary_variable_category.auxiliary_variable
			),
			w_distinct_selection as (
				select distinct w_data.node_ads_id, w_data.edges, w_data.add_check_threshold
				from w_data join trans_table on
				(
					trans_table.id = w_data.node_ads_id
					OR
					trans_table.id = any (w_data.edge_ads_id)
				)
			)
			INSERT INTO @extschema@.t_additivity_set_plot(available_datasets, edges, add_check_threshold, dbg_data)
			select
				node_ads_id, edges, add_check_threshold,
				json_build_object(
					'fn_refresh_t_additivity_set_plot', json_build_object(
						'TG_TABLE_NAME', TG_TABLE_NAME, 
						'TG_OP', TG_OP, 
						'trigerred_time', (clock_timestamp()::timestamp with time zone)::text
						)
					) as dbg_data
			from w_distinct_selection
			;
		ELSE
				RAISE EXCEPTION 'fn_refresh_t_additivity_set_plot -- trigger operation not known: %', TG_OP;
		END IF;
	ELSE
		RAISE EXCEPTION 'fn_refresh_t_additivity_set_plot -- % -- table name not known: %', TG_OP, TG_TABLE_NAME;
	END IF;
	RETURN NULL;
END;
$$;

--target_data.t_variable_hierarchy
drop trigger if exists trg__t_variable_hierarchy__refresh_t_additivity_set_plot__upd ON @extschema@.t_variable_hierarchy;
CREATE TRIGGER trg__t_variable_hierarchy__refresh_t_additivity_set_plot__upd
AFTER UPDATE ON @extschema@.t_variable_hierarchy
REFERENCING NEW TABLE AS trans_table
FOR EACH STATEMENT EXECUTE PROCEDURE @extschema@.fn_refresh_t_additivity_set_plot();

drop trigger if exists trg__t_variable_hierarchy__refresh_t_additivity_set_plot__ins ON @extschema@.t_variable_hierarchy;
CREATE TRIGGER trg__t_variable_hierarchy__refresh_t_additivity_set_plot__ins
AFTER INSERT ON @extschema@.t_variable_hierarchy
REFERENCING NEW TABLE AS trans_table
FOR EACH STATEMENT EXECUTE PROCEDURE @extschema@.fn_refresh_t_additivity_set_plot();

drop trigger if exists trg__t_variable_hierarchy__refresh_t_additivity_set_plot__del ON @extschema@.t_variable_hierarchy;
CREATE TRIGGER trg__t_variable_hierarchy__refresh_t_additivity_set_plot__del
AFTER DELETE ON @extschema@.t_variable_hierarchy
REFERENCING OLD TABLE AS trans_table
FOR EACH STATEMENT EXECUTE PROCEDURE @extschema@.fn_refresh_t_additivity_set_plot();

/*
delete from @extschema@.t_additivity_set_plot;
select * from @extschema@.t_additivity_set_plot;
select * from @extschema@.t_variable_hierarchy;
update @extschema@.t_variable_hierarchy set variable = 16 where id = 14;
insert into @extschema@.t_variable_hierarchy (id, variable, variable_superior) values (15, 1, 14);
delete from @extschema@.t_variable_hierarchy where id = 15;
*/

--target_data.t_available_datasets
drop trigger if exists trg__t_available_datasets__refresh_t_additivity_set_plot__upd ON @extschema@.t_available_datasets;
CREATE TRIGGER trg__t_available_datasets__refresh_t_additivity_set_plot__upd
AFTER UPDATE ON @extschema@.t_available_datasets
REFERENCING NEW TABLE AS trans_table
FOR EACH STATEMENT EXECUTE PROCEDURE @extschema@.fn_refresh_t_additivity_set_plot();

drop trigger if exists trg__t_available_datasets__refresh_t_additivity_set_plot__ins ON @extschema@.t_available_datasets;
CREATE TRIGGER trg__t_available_datasets__refresh_t_additivity_set_plot__ins
AFTER INSERT ON @extschema@.t_available_datasets
REFERENCING NEW TABLE AS trans_table
FOR EACH STATEMENT EXECUTE PROCEDURE @extschema@.fn_refresh_t_additivity_set_plot();

drop trigger if exists trg__t_available_datasets__refresh_t_additivity_set_plot__del ON @extschema@.t_available_datasets;
CREATE TRIGGER trg__t_available_datasets__refresh_t_additivity_set_plot__del
AFTER DELETE ON @extschema@.t_available_datasets
REFERENCING OLD TABLE AS trans_table
FOR EACH STATEMENT EXECUTE PROCEDURE @extschema@.fn_refresh_t_additivity_set_plot();

/*
delete from @extschema@.t_additivity_set_plot;
select * from @extschema@.t_additivity_set_plot;
select * from @extschema@.t_available_datasets;
update @extschema@.t_available_datasets set last_change = now() where id = 1;
insert into @extschema@.t_available_datasets (id, panel, reference_year_set, variable, last_change) values (50, 1, 2, 8, now());
delete from @extschema@.t_available_datasets where id = 50;
*/

--------------------------------------------------------------------------------------------------------------------------------

--drop function @extschema@.fn_launch_add_check() cascade;
CREATE OR REPLACE FUNCTION @extschema@.fn_launch_add_check() RETURNS TRIGGER AS $src$
DECLARE
		_var_sup int;
		_var_inf int[];
		_panel int;
		_refyearsets int;
		_errp json;
		_dbg_data jsonb;
BEGIN
	IF (TG_TABLE_NAME = 't_additivity_set_plot') THEN
		IF (TG_OP = 'DELETE') THEN
			RAISE EXCEPTION 'fn_launch_add_check -- DELETE on table t_additivity_set_plot not permited';
		ELSIF (TG_OP = 'UPDATE') THEN
			RETURN NULL;
		ELSIF (TG_OP = 'INSERT') THEN
			------------------ _vars_add_set
			select
				t_available_datasets.variable 	into _var_sup
			from @extschema@.t_available_datasets
			where t_available_datasets.id = new.available_datasets;
			select
				new.edges			into _var_inf
			;
			------------------ _plots
			select
				t_available_datasets.panel	into _panel
			from @extschema@.t_available_datasets
			where t_available_datasets.id = new.available_datasets;
			------------------ _refyearsets
			select
				t_available_datasets.reference_year_set into _refyearsets
			from @extschema@.t_available_datasets
			where t_available_datasets.id = new.available_datasets;
			----------------------------------------------------------------------------------------------
			if array_prepend(_var_sup, _var_inf) is not null then
				_dbg_data = jsonb_build_object(
							'add_check_start', (clock_timestamp()::timestamp with time zone)::text
						);
				if (	select 
						(target_variable is not null) and (auxiliary_variable_category is null) as is_target
					from @extschema@.t_variable 
					where id = _var_sup) 
				then 
					with w_err as (
						select @extschema@.fn_add_plot_target_attr(_var_sup, _var_inf, _panel, _refyearsets, 1e-6, true) as fn_err
					)
					, w_err_t as (
						select (fn_err).* from w_err
					)
					, w_err_json as (
						select 
							json_build_object(
								'plot'				, plot,
								'reference_year_set'		, reference_year_set,
								'variable'			, variable,
								'ldsity'			, ldsity,
								'ldsity_sum'			, ldsity_sum,
								'variables_def'			, variables_def,
								'variables_found'		, variables_found,
								'diff'				, diff
							) as errj
						from w_err_t
					)
					select json_agg(errj) into _errp from w_err_json;
				else
					with w_err as (
						select @extschema@.fn_add_plot_aux_attr(_var_sup, _var_inf, _panel, 1e-6, true) as fn_err
					)
					, w_err_t as (
						select (fn_err).* from w_err
					)
					, w_err_json as (
						select 
							json_build_object(
								'plot'				, plot,
								'variable'			, variable,
								'ldsity'			, ldsity,
								'ldsity_sum'			, ldsity_sum,
								'variables_def'			, variables_def,
								'variables_found'		, variables_found,
								'diff'				, diff
							) as errj
						from w_err_t
					)
					select json_agg(errj) into _errp from w_err_json;
				end if;

				_dbg_data = jsonb_set(_dbg_data,
						'{add_check_stop}', to_jsonb((clock_timestamp()::timestamp with time zone)::text)
						);

				update @extschema@.t_additivity_set_plot 
				set dbg_data = jsonb_set(dbg_data, '{fn_launch_additivity_check}', _dbg_data) 
				where id = new.id;
				
				IF
					(json_array_length(_errp) > 0)
					THEN RAISE EXCEPTION 'fn_launch_add_check -- % -- % -- plot level local densities are not additive:
						variables in additivity set: sup: %, inf: % , checked panels: %, refyearsets: %,
						found err plots: 
						%', TG_TABLE_NAME, TG_OP, _var_sup, _var_inf, _panel, _refyearsets, 
						jsonb_pretty(_errp::jsonb);
				END IF;
			else
				RAISE WARNING 'fn_launch_add_check -- % -- % -- additivity check was skiped (no variables found in hierarchy):
						variables in additivity set: %, checked panels: %, refyearsets: %'
						, TG_TABLE_NAME, TG_OP, 
						_vars_add_set, _panel, _refyearsets;
			end if;
		ELSE
			RAISE EXCEPTION 'fn_launch_add_check -- trigger operation not known: %', TG_OP;
		END IF;
	ELSE
		RAISE EXCEPTION 'fn_launch_add_check -- % -- table name not known: %', TG_OP, TG_TABLE_NAME;
	END IF;
RETURN NULL; -- result is ignored since this is an AFTER trigger
END;
$src$ LANGUAGE plpgsql;

drop trigger if exists trg__additivity_set_plot__ins on @extschema@.t_additivity_set_plot;
CREATE CONSTRAINT TRIGGER trg__additivity_set_plot__ins
    AFTER INSERT ON @extschema@.t_additivity_set_plot
    DEFERRABLE INITIALLY DEFERRED
    FOR EACH ROW EXECUTE FUNCTION @extschema@.fn_launch_add_check();

drop trigger if exists trg__additivity_set_plot__upd on @extschema@.t_additivity_set_plot;
CREATE CONSTRAINT TRIGGER trg__additivity_set_plot__upd
    AFTER UPDATE ON @extschema@.t_additivity_set_plot
    DEFERRABLE INITIALLY DEFERRED
    FOR EACH ROW EXECUTE FUNCTION @extschema@.fn_launch_add_check();

drop trigger if exists trg__additivity_set_plot__del on @extschema@.t_additivity_set_plot;
CREATE CONSTRAINT TRIGGER trg__additivity_set_plot__del
    AFTER DELETE ON @extschema@.t_additivity_set_plot
    DEFERRABLE INITIALLY DEFERRED
    FOR EACH ROW EXECUTE FUNCTION @extschema@.fn_launch_add_check();

--------------------------------------------------------------------------------------------------------------------------------

--drop function @extschema@.fn_check_threshold() cascade;
CREATE OR REPLACE FUNCTION @extschema@.fn_check_threshold() RETURNS TRIGGER AS $src$
DECLARE
		_node_threshold int;
		_edges_threshold int;
BEGIN
	IF (TG_TABLE_NAME = 't_additivity_set_plot') THEN
		IF (TG_OP = 'DELETE') THEN
			RAISE EXCEPTION 'fn_check_threshold -- DELETE on table t_additivity_set_plot not permited';
		ELSIF (TG_OP = 'UPDATE') THEN
			RETURN NULL;
		ELSIF (TG_OP = 'INSERT') THEN
			select ldsity_threshold into _node_threshold
			from @extschema@.t_available_datasets
			where id = new.available_datasets;

			select distinct ldsity_threshold into _edges_threshold
			from @extschema@.t_available_datasets 
			where id in (select unnest(new.edges));

			IF (_node_threshold != _edges_threshold)
					THEN RAISE EXCEPTION 'fn_check_threshold -- % -- % -- threshold for aditivity set are not the same: %, %', TG_TABLE_NAME, TG_OP, _node_threshold, edges_threshold;
			END IF;
		ELSE
			RAISE EXCEPTION 'fn_check_threshold -- trigger operation not known: %', TG_OP;
		END IF;
	ELSE
		RAISE EXCEPTION 'fn_check_threshold -- % -- table name not known: %', TG_OP, TG_TABLE_NAME;
	END IF;
RETURN NULL; -- result is ignored since this is an AFTER trigger
END;
$src$ LANGUAGE plpgsql;

drop trigger if exists trg__additivity_set_plot__check_threshold__ins on @extschema@.t_additivity_set_plot;
CREATE CONSTRAINT TRIGGER trg__additivity_set_plot__check_threshold__ins
    AFTER INSERT ON @extschema@.t_additivity_set_plot
    DEFERRABLE INITIALLY DEFERRED
    FOR EACH ROW EXECUTE FUNCTION @extschema@.fn_check_threshold();

drop trigger if exists trg__additivity_set_plot__check_threshold__upd on @extschema@.t_additivity_set_plot;
CREATE CONSTRAINT TRIGGER trg__additivity_set_plot__check_threshold__upd
    AFTER UPDATE ON @extschema@.t_additivity_set_plot
    DEFERRABLE INITIALLY DEFERRED
    FOR EACH ROW EXECUTE FUNCTION @extschema@.fn_check_threshold();

drop trigger if exists trg__additivity_set_plot__check_threshold__del on @extschema@.t_additivity_set_plot;
CREATE CONSTRAINT TRIGGER trg__additivity_set_plot__check_threshold__del
    AFTER DELETE ON @extschema@.t_additivity_set_plot
    DEFERRABLE INITIALLY DEFERRED
    FOR EACH ROW EXECUTE FUNCTION @extschema@.fn_check_threshold();

-- </function>

alter table @extschema@.t_available_datasets
add constraint ukey__t_available_datasets unique(panel, reference_year_set, variable, ldsity_threshold);
comment on constraint ukey__t_available_datasets on @extschema@.t_available_datasets is
'The identifier of record for colums panel, reference_year_set, variable, ldsity_threshold must be unique.';
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
-- t_available_datasets => update column ldsity_threshold
---------------------------------------------------------------------------------------------------
update @extschema@.t_available_datasets
set
	ldsity_threshold = 0.0000000001
where
	ldsity_threshold is null;


alter table @extschema@.t_available_datasets alter column ldsity_threshold set not null;
---------------------------------------------------------------------------------------------------

DROP FUNCTION IF EXISTS @extschema@.fn_etl_import_ldsity_values(json, double precision) CASCADE;

-- <function name="fn_etl_import_ldsity_values" schema="extschema" src="functions/extschema/etl/fn_etl_import_ldsity_values.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_import_ldsity_values(json)

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_import_ldsity_values(json) CASCADE;

create or replace function @extschema@.fn_etl_import_ldsity_values
(
	_available_datasets_and_ldsity_values json
)
returns text
as
$$
declare
		_max_id_ttd								integer;
		_max_id_tad								integer;
		_target_variable						integer;
		_target_variable_ad						integer;
		_country								varchar;
		_country_ad								varchar;
		_strata_sets							varchar[];
		_strata_sets_ad							varchar[];
		_stratums								varchar[];
		_stratums_ad							varchar[];
		_panels									varchar[];
		_panels_ad								varchar[];
		_reference_year_sets					varchar[];
		_reference_year_sets_ad					varchar[];
		_inventory_campaigns					varchar[];
		_inventory_campaigns_ad					varchar[];
		_clusters								varchar[];
		_cluster_configurations					varchar[];
		_cluster_configurations_ad				varchar[];
		_array_target_variable					integer[];
		_array_target_variable_ad				integer[];
		_array_sub_population_category			integer[];
		_array_sub_population_category_ad		integer[];
		_array_area_domain_category				integer[];
		_array_area_domain_category_ad			integer[];
		_array_panel							varchar[];
		_array_panel_ad							varchar[];
		_array_reference_year_set				varchar[];
		_array_reference_year_set_ad			varchar[];
		_check_cmrys2pm							integer;
		_check_cmrys2pm_ad						integer;
		_array_cmrys2pm							integer[];
		_array_cmrys2pm_ad						integer[];
		_check_count_records_input_json			integer;
		_check_count_records_after_inner_join	integer;
		_res_ttd								text;
		_res_tad								text;
		_res									text;
		_check_ldsity_threshold					integer;
		_check_ldsity_threshold_unique			integer;
begin
		if _available_datasets_and_ldsity_values is null
		then
			raise exception 'Error 01: fn_etl_import_ldsity_values: Input argument _available_datasets_and_ldsity_values must not by NULL!';
		end if;
	
		_max_id_ttd := (select coalesce(max(id),0) from @extschema@.t_target_data);
		_max_id_tad := (select coalesce(max(id),0) from @extschema@.t_available_datasets);

		---------------------------------------------------------------------------------
		-- check target variable [element "available_datasets"] --
		with
		w1 as	(select json_array_elements((_available_datasets_and_ldsity_values->>'available_datasets')::json) as s)
		,w2 as	(select	(s->>'target_variable')::integer as target_variable	from w1)
		select distinct w2.target_variable from w2
		into _target_variable_ad;

		if (select count(ctv.*) is distinct from 1 from @extschema@.c_target_variable as ctv where ctv.id = _target_variable_ad)
		then
			raise exception 'Error 02a: fn_etl_import_ldsity_values: Input JSON element "target_variable = %" in element "available_datasets" is not present in table c_target_variable!',_target_variable_ad;
		end if;

		-- check target variable [element "ldsity_values"] --
		with
		w1 as	(select json_array_elements((_available_datasets_and_ldsity_values->>'ldsity_values')::json) as s)
		,w2 as	(select	(s->>'target_variable')::integer as target_variable	from w1)
		select distinct w2.target_variable from w2
		into _target_variable;

		if (select count(ctv.*) is distinct from 1 from @extschema@.c_target_variable as ctv where ctv.id = _target_variable)
		then
			raise exception 'Error 02b: fn_etl_import_ldsity_values: Input JSON element "target_variable = %" in element "ldsity_values" is not present in table c_target_variable!',_target_variable;
		end if;
		---------------------------------------------------------------------------------
		-- check country [element "available_datasets"] --
		with
		w1 as	(select json_array_elements((_available_datasets_and_ldsity_values->>'available_datasets')::json) as s)
		,w2 as	(select	(s->>'country')::varchar as country	from w1)
		select distinct w2.country from w2
		into _country_ad;

		if (select count(cc.*) is distinct from 1 from sdesign.c_country as cc where cc.label = _country_ad)
		then
			raise exception 'Error 03a: fn_etl_import_ldsity_values: Input JSON element "country = %" in element "available_datasets" is not present in table sdesign.c_country!',_country_ad;
		end if;

		-- check country [element "ldsity_values"] --
		with
		w1 as	(select json_array_elements((_available_datasets_and_ldsity_values->>'ldsity_values')::json) as s)
		,w2 as	(select	(s->>'country')::varchar as country	from w1)
		select distinct w2.country from w2
		into _country;

		if (select count(cc.*) is distinct from 1 from sdesign.c_country as cc where cc.label = _country)
		then
			raise exception 'Error 03b: fn_etl_import_ldsity_values: Input JSON element "country = %" in element "ldsity_values" is not present in table sdesign.c_country!',_country;
		end if;
		---------------------------------------------------------------------------------
		-- check strata sets [element "available_datasets"] --
		with
		w1 as	(select json_array_elements((_available_datasets_and_ldsity_values->>'available_datasets')::json) as s)
		,w2 as	(select	(s->>'strata_set')::varchar as strata_set from w1)
		,w3 as	(select distinct w2.strata_set from w2)
		select array_agg(w3.strata_set order by w3.strata_set) from w3
		into _strata_sets_ad;

		for i in 1..array_length(_strata_sets_ad,1)
		loop
			if	(
				select count(tss.*) is distinct from 1 from sdesign.t_strata_set as tss where tss.country =
				(select cc.id from sdesign.c_country as cc where cc.label = _country_ad)
				and tss.strata_set = _strata_sets_ad[i]
				)
			then
				raise exception 'Error 04a: fn_etl_import_ldsity_values: Some of input JSON element "strata_set = %" in element "available_datasets" is not present in table sdesign.t_strata for input JSON element "country = %"!',_strata_sets_ad[i], _country_ad;
			end if;		
		end loop;

		-- check strata sets [element "ldsity_values"] --
		with
		w1 as	(select json_array_elements((_available_datasets_and_ldsity_values->>'ldsity_values')::json) as s)
		,w2 as	(select	(s->>'strata_set')::varchar as strata_set from w1)
		,w3 as	(select distinct w2.strata_set from w2)
		select array_agg(w3.strata_set order by w3.strata_set) from w3
		into _strata_sets;

		for i in 1..array_length(_strata_sets,1)
		loop
			if	(
				select count(tss.*) is distinct from 1 from sdesign.t_strata_set as tss where tss.country =
				(select cc.id from sdesign.c_country as cc where cc.label = _country)
				and tss.strata_set = _strata_sets[i]
				)
			then
				raise exception 'Error 04b: fn_etl_import_ldsity_values: Some of input JSON element "strata_set = %" in element "ldsity_values" is not present in table sdesign.t_strata for input JSON element "country = %"!',_strata_sets[i], _country;
			end if;		
		end loop;
		---------------------------------------------------------------------------------
		-- check stratums [element "available_datasets"] --
		with
		w1 as	(select json_array_elements((_available_datasets_and_ldsity_values->>'available_datasets')::json) as s)
		,w2 as	(select	(s->>'stratum')::varchar as stratum from w1)
		,w3 as	(select distinct w2.stratum from w2)
		select array_agg(w3.stratum order by w3.stratum) from w3
		into _stratums_ad;

		for i in 1..array_length(_stratums_ad,1)
		loop
			if	(
				select count(ts.*) is distinct from 1 from sdesign.t_stratum as ts where ts.strata_set in
				(select tss.id from sdesign.t_strata_set as tss where tss.country =
				(select cc.id from sdesign.c_country as cc where cc.label = _country_ad))
				and ts.stratum = _stratums_ad[i]
				)
			then
				raise exception 'Error 05a: fn_etl_import_ldsity_values: Some of input JSON element "stratum = %" in element "available_datasets" is not present in table sdesign.t_stratum for input JSON element "country = %"!',_stratums_ad[i], _country_ad;
			end if;	
		end loop;

		-- check stratums [element "ldsity_values"] --
		with
		w1 as	(select json_array_elements((_available_datasets_and_ldsity_values->>'ldsity_values')::json) as s)
		,w2 as	(select	(s->>'stratum')::varchar as stratum from w1)
		,w3 as	(select distinct w2.stratum from w2)
		select array_agg(w3.stratum order by w3.stratum) from w3
		into _stratums;

		for i in 1..array_length(_stratums,1)
		loop
			if	(
				select count(ts.*) is distinct from 1 from sdesign.t_stratum as ts where ts.strata_set in
				(select tss.id from sdesign.t_strata_set as tss where tss.country =
				(select cc.id from sdesign.c_country as cc where cc.label = _country))
				and ts.stratum = _stratums[i]
				)
			then
				raise exception 'Error 05b: fn_etl_import_ldsity_values: Some of input JSON element "stratum = %" in element "ldsity_values" is not present in table sdesign.t_stratum for input JSON element "country = %"!',_stratums[i], _country;
			end if;	
		end loop;
		---------------------------------------------------------------------------------
		-- check panels [element "available_datasets"] --
		with
		w1 as	(select json_array_elements((_available_datasets_and_ldsity_values->>'available_datasets')::json) as s)
		,w2 as	(select	(s->>'panel')::varchar as panel	from w1)
		,w3 as	(select distinct w2.panel from w2)
		select array_agg(w3.panel order by w3.panel) from w3
		into _panels_ad;

		for i in 1..array_length(_panels_ad,1)
		loop
			if	(
				select count(tp.*) is distinct from 1 from sdesign.t_panel as tp where tp.stratum in
				(select ts.id from sdesign.t_stratum as ts where ts.strata_set in
				(select tss.id from sdesign.t_strata_set as tss where tss.country = 
				(select cc.id from sdesign.c_country as cc where cc.label = _country_ad)))
				and tp.panel = _panels_ad[i]
				)
			then
				raise exception 'Error 06a: fn_etl_import_ldsity_values: Some of input JSON element "panel = %" in element "available_datasets" is not present in table sdesign.t_panel for input JSON element "country = %"!',_panels_ad[i], _country_ad;
			end if;		
		end loop;

		-- check panels [element "ldsity_values"] --
		with
		w1 as	(select json_array_elements((_available_datasets_and_ldsity_values->>'ldsity_values')::json) as s)
		,w2 as	(select	(s->>'panel')::varchar as panel	from w1)
		,w3 as	(select distinct w2.panel from w2)
		select array_agg(w3.panel order by w3.panel) from w3
		into _panels;

		for i in 1..array_length(_panels,1)
		loop
			if	(
				select count(tp.*) is distinct from 1 from sdesign.t_panel as tp where tp.stratum in
				(select ts.id from sdesign.t_stratum as ts where ts.strata_set in
				(select tss.id from sdesign.t_strata_set as tss where tss.country = 
				(select cc.id from sdesign.c_country as cc where cc.label = _country)))
				and tp.panel = _panels[i]
				)
			then
				raise exception 'Error 06b: fn_etl_import_ldsity_values: Some of input JSON element "panel = %" in element "ldsity_values" is not present in table sdesign.t_panel for input JSON element "country = %"!',_panels[i], _country;
			end if;		
		end loop;
		---------------------------------------------------------------------------------
		-- check reference year sets [element "available_datasets"] --
		with
		w1 as	(select json_array_elements((_available_datasets_and_ldsity_values->>'available_datasets')::json) as s)
		,w2 as	(select	(s->>'reference_year_set')::varchar as reference_year_set from w1)
		,w3 as	(select distinct w2.reference_year_set from w2)
		select array_agg(w3.reference_year_set order by w3.reference_year_set) from w3
		into _reference_year_sets_ad;

		for i in 1..array_length(_reference_year_sets_ad,1)
		loop
			if	(
				select count(trys.*) is distinct from 1 from sdesign.t_reference_year_set as trys where trys.id in
				(select cmr.reference_year_set from sdesign.cm_refyearset2panel_mapping as cmr where cmr.panel in
				(select tp.id from sdesign.t_panel as tp where tp.stratum in
				(select ts.id from sdesign.t_stratum as ts where ts.strata_set in
				(select tss.id from sdesign.t_strata_set as tss where tss.country = 
				(select cc.id from sdesign.c_country as cc where cc.label = _country_ad)))))
				and trys.reference_year_set = _reference_year_sets_ad[i]
				)
			then
				raise exception 'Error 07a: fn_etl_import_ldsity_values: Some of input JSON element "reference_year_set = %" in element "available_datasets" is not present in table sdesign.t_reference_year_set for input JSON element "country = %"!',_reference_year_sets_ad[i], _country_ad;
			end if;		
		end loop;

		-- check reference year sets [element "ldsity_values"] --
		with
		w1 as	(select json_array_elements((_available_datasets_and_ldsity_values->>'ldsity_values')::json) as s)
		,w2 as	(select	(s->>'reference_year_set')::varchar as reference_year_set from w1)
		,w3 as	(select distinct w2.reference_year_set from w2)
		select array_agg(w3.reference_year_set order by w3.reference_year_set) from w3
		into _reference_year_sets;

		for i in 1..array_length(_reference_year_sets,1)
		loop
			if	(
				select count(trys.*) is distinct from 1 from sdesign.t_reference_year_set as trys where trys.id in
				(select cmr.reference_year_set from sdesign.cm_refyearset2panel_mapping as cmr where cmr.panel in
				(select tp.id from sdesign.t_panel as tp where tp.stratum in
				(select ts.id from sdesign.t_stratum as ts where ts.strata_set in
				(select tss.id from sdesign.t_strata_set as tss where tss.country = 
				(select cc.id from sdesign.c_country as cc where cc.label = _country)))))
				and trys.reference_year_set = _reference_year_sets[i]
				)
			then
				raise exception 'Error 07b: fn_etl_import_ldsity_values: Some of input JSON element "reference_year_set = %" in element "ldsity_values" is not present in table sdesign.t_reference_year_set for input JSON element "country = %"!',_reference_year_sets[i], _country;
			end if;		
		end loop;
		---------------------------------------------------------------------------------
		-- check inventory campaigns [element "available_datasets"] --
		with
		w1 as	(select json_array_elements((_available_datasets_and_ldsity_values->>'available_datasets')::json) as s)
		,w2 as	(select	(s->>'inventory_campaign')::varchar as inventory_campaign from w1)
		,w3 as	(select distinct w2.inventory_campaign from w2)
		select array_agg(w3.inventory_campaign order by w3.inventory_campaign) from w3
		into _inventory_campaigns_ad;

		for i in 1..array_length(_inventory_campaigns_ad,1)
		loop
			if	(
				select count(tic.*) is distinct from 1 from sdesign.t_inventory_campaign as tic where tic.id in
				(select trys.inventory_campaign from sdesign.t_reference_year_set as trys where trys.id in
				(select cmr.reference_year_set from sdesign.cm_refyearset2panel_mapping as cmr where cmr.panel in
				(select tp.id from sdesign.t_panel as tp where tp.stratum in
				(select ts.id from sdesign.t_stratum as ts where ts.strata_set in
				(select tss.id from sdesign.t_strata_set as tss where tss.country = 
				(select cc.id from sdesign.c_country as cc where cc.label = _country_ad))))))
				and tic.inventory = _inventory_campaigns_ad[i]
				)
			then
				raise exception 'Error 08a: fn_etl_import_ldsity_values: Some of input JSON element "inventory_campaign = %" in element "available_datasets" is not present in table sdesign.t_inventory_campaign for input JSON element "country = %"!',_inventory_campaigns_ad[i], _country_ad;
			end if;		
		end loop;

		-- check inventory campaigns [element "ldsity_values"] --
		with
		w1 as	(select json_array_elements((_available_datasets_and_ldsity_values->>'ldsity_values')::json) as s)
		,w2 as	(select	(s->>'inventory_campaign')::varchar as inventory_campaign from w1)
		,w3 as	(select distinct w2.inventory_campaign from w2)
		select array_agg(w3.inventory_campaign order by w3.inventory_campaign) from w3
		into _inventory_campaigns;

		for i in 1..array_length(_inventory_campaigns,1)
		loop
			if	(
				select count(tic.*) is distinct from 1 from sdesign.t_inventory_campaign as tic where tic.id in
				(select trys.inventory_campaign from sdesign.t_reference_year_set as trys where trys.id in
				(select cmr.reference_year_set from sdesign.cm_refyearset2panel_mapping as cmr where cmr.panel in
				(select tp.id from sdesign.t_panel as tp where tp.stratum in
				(select ts.id from sdesign.t_stratum as ts where ts.strata_set in
				(select tss.id from sdesign.t_strata_set as tss where tss.country = 
				(select cc.id from sdesign.c_country as cc where cc.label = _country))))))
				and tic.inventory = _inventory_campaigns[i]
				)
			then
				raise exception 'Error 08b: fn_etl_import_ldsity_values: Some of input JSON element "inventory_campaign = %" in element "ldsity_values" is not present in table sdesign.t_inventory_campaign for input JSON element "country = %"!',_inventory_campaigns[i], _country;
			end if;		
		end loop;
		---------------------------------------------------------------------------------
		-- check clusters [element "ldsity_values"] --
		with
		w1 as	(select json_array_elements((_available_datasets_and_ldsity_values->>'ldsity_values')::json) as s)
		,w2 as	(select	(s->>'cluster')::varchar as cluster from w1)
		,w3 as	(select distinct w2.cluster from w2)
		select array_agg(w3.cluster order by w3.cluster) from w3
		into _clusters;

		for i in 1..array_length(_clusters,1)
		loop
			if	(
				select count(tc.*) is distinct from 1 from sdesign.t_cluster as tc where tc.id in
				(select ccpm.cluster from sdesign.cm_cluster2panel_mapping as ccpm where ccpm.panel in
				(select tp.id from sdesign.t_panel as tp where tp.stratum in
				(select ts.id from sdesign.t_stratum as ts where ts.strata_set in
				(select tss.id from sdesign.t_strata_set as tss where tss.country = 
				(select cc.id from sdesign.c_country as cc where cc.label = _country)))))
				and tc.cluster = _clusters[i]
				)
			then
				raise exception 'Error 09: fn_etl_import_ldsity_values: Some of input JSON element "cluster = %" is not present in table sdesign.t_cluster for input JSON element "country = %"!',_clusters[i], _country;
			end if;		
		end loop;
		---------------------------------------------------------------------------------
		-- check cluster configurations [element "available_datasets"] --
		with
		w1 as	(select json_array_elements((_available_datasets_and_ldsity_values->>'available_datasets')::json) as s)
		,w2 as	(select	(s->>'cluster_configuration')::varchar as cluster_configuration from w1)
		,w3 as	(select distinct w2.cluster_configuration from w2)
		select array_agg(w3.cluster_configuration order by w3.cluster_configuration) from w3
		into _cluster_configurations_ad;

		for i in 1..array_length(_cluster_configurations_ad,1)
		loop
			if	(
				select count(tcc.*) is distinct from 1 from sdesign.t_cluster_configuration as tcc where tcc.id in
				(select tp.cluster_configuration from sdesign.t_panel as tp where tp.stratum in
				(select ts.id from sdesign.t_stratum as ts where ts.strata_set in
				(select tss.id from sdesign.t_strata_set as tss where tss.country = 
				(select cc.id from sdesign.c_country as cc where cc.label = _country_ad))))
				and tcc.cluster_configuration = _cluster_configurations_ad[i]
				)
			then
				raise exception 'Error 10b: fn_etl_import_ldsity_values: Some of input JSON element "cluster_configuration = %" in element "available_datasets" is not present in table sdesign.t_cluster_configuration for input JSON element "country = %"!',_cluster_configurations_ad[i], _country_ad;
			end if;		
		end loop;

		-- check cluster configurations [element "ldsity_values"] --
		with
		w1 as	(select json_array_elements((_available_datasets_and_ldsity_values->>'ldsity_values')::json) as s)
		,w2 as	(select	(s->>'cluster_configuration')::varchar as cluster_configuration from w1)
		,w3 as	(select distinct w2.cluster_configuration from w2)
		select array_agg(w3.cluster_configuration order by w3.cluster_configuration) from w3
		into _cluster_configurations;

		for i in 1..array_length(_cluster_configurations,1)
		loop
			if	(
				select count(tcc.*) is distinct from 1 from sdesign.t_cluster_configuration as tcc where tcc.id in
				(select tp.cluster_configuration from sdesign.t_panel as tp where tp.stratum in
				(select ts.id from sdesign.t_stratum as ts where ts.strata_set in
				(select tss.id from sdesign.t_strata_set as tss where tss.country = 
				(select cc.id from sdesign.c_country as cc where cc.label = _country))))
				and tcc.cluster_configuration = _cluster_configurations[i]
				)
			then
				raise exception 'Error 10b: fn_etl_import_ldsity_values: Some of input JSON element "cluster_configuration = %" in element "ldsity_values" is not present in table sdesign.t_cluster_configuration for input JSON element "country = %"!',_cluster_configurations[i], _country;
			end if;		
		end loop;
		---------------------------------------------------------------------------------
		-- check combinations of target_variable, sub_population_category and area_domain_category [element "available_datasets"] --
		with
		w1 as	(
				select json_array_elements((_available_datasets_and_ldsity_values->>'available_datasets')::json) as s
				)
		,w2 as	(
				select
						(s->>'target_variable')::integer			as target_variable,
						(s->>'sub_population_category')::integer	as sub_population_category,
						(s->>'area_domain_category')::integer		as area_domain_category	
				from w1
				)
		,w3 as	(select distinct target_variable, sub_population_category, area_domain_category from w2)
		select
				array_agg(w3.target_variable order by w3.sub_population_category, w3.area_domain_category) as target_variable,
				array_agg(w3.sub_population_category order by w3.sub_population_category, w3.area_domain_category) as sub_population_category,
				array_agg(w3.area_domain_category order by w3.sub_population_category, w3.area_domain_category) as area_domain_category
		from
				w3
		into
				_array_target_variable_ad,
				_array_sub_population_category_ad,
				_array_area_domain_category_ad;

		for i in 1..array_length(_array_sub_population_category_ad,1)
		loop
			if	(
				with
				w1 as	(
						select
								id,
								target_variable,
								case when sub_population_category is null then 0 else sub_population_category end as sub_population_category,
								case when area_domain_category is null then 0 else area_domain_category end as area_domain_category
						from
								@extschema@.t_variable where target_variable = _array_target_variable_ad[i]
						)
				select count(w1.*) is distinct from 1 from w1
				where w1.sub_population_category = _array_sub_population_category_ad[i]
				and w1.area_domain_category = _array_area_domain_category_ad[i]
				)
			then
				raise exception 'Error 11a: fn_etl_import_ldsity_values: Some of input combination JSON elements "target_variable = %", "sub_population_category = %" and "area_domain_category = %" in element "available_datasets" is not present in table t_variable!',_array_target_variable_ad[i], _array_sub_population_category_ad[i], _array_area_domain_category_ad[i];
			end if;
		end loop;

		-- check combinations of target_variable, sub_population_category and area_domain_category [element "ldsity_values"] --
		with
		w1 as	(
				select json_array_elements((_available_datasets_and_ldsity_values->>'ldsity_values')::json) as s
				)
		,w2 as	(
				select
						(s->>'target_variable')::integer			as target_variable,
						(s->>'sub_population_category')::integer	as sub_population_category,
						(s->>'area_domain_category')::integer		as area_domain_category	
				from w1
				)
		,w3 as	(select distinct target_variable, sub_population_category, area_domain_category from w2)
		select
				array_agg(w3.target_variable order by w3.sub_population_category, w3.area_domain_category) as target_variable,
				array_agg(w3.sub_population_category order by w3.sub_population_category, w3.area_domain_category) as sub_population_category,
				array_agg(w3.area_domain_category order by w3.sub_population_category, w3.area_domain_category) as area_domain_category
		from
				w3
		into
				_array_target_variable,
				_array_sub_population_category,
				_array_area_domain_category;

		for i in 1..array_length(_array_sub_population_category,1)
		loop
			if	(
				with
				w1 as	(
						select
								id,
								target_variable,
								case when sub_population_category is null then 0 else sub_population_category end as sub_population_category,
								case when area_domain_category is null then 0 else area_domain_category end as area_domain_category
						from
								@extschema@.t_variable where target_variable = _array_target_variable[i]
						)
				select count(w1.*) is distinct from 1 from w1
				where w1.sub_population_category = _array_sub_population_category[i]
				and w1.area_domain_category = _array_area_domain_category[i]
				)
			then
				raise exception 'Error 11b: fn_etl_import_ldsity_values: Some of input combination JSON elements "target_variable = %", "sub_population_category = %" and "area_domain_category = %" in element "ldsity_values" is not present in table t_variable!',_array_target_variable[i], _array_sub_population_category[i], _array_area_domain_category[i];
			end if;
		end loop;
		---------------------------------------------------------------------------------
		-- check combinations of panel and reference year set [element "available_datasets"] --
		with
		w1 as	(
				select json_array_elements((_available_datasets_and_ldsity_values->>'available_datasets')::json) as s
				)
		,w2 as	(
				select
						(s->>'panel')::varchar				as panel,
						(s->>'reference_year_set')::varchar	as reference_year_set
				from w1
				)
		,w3 as	(select distinct panel, reference_year_set from w2)
		select
				array_agg(w3.panel order by w3.panel, w3.reference_year_set) as panel,
				array_agg(w3.reference_year_set order by w3.panel, w3.reference_year_set) as reference_year_set
		from
				w3
		into
				_array_panel_ad,
				_array_reference_year_set_ad;

		for i in 1..array_length(_array_panel_ad,1)
		loop
			select cmrys.id from sdesign.cm_refyearset2panel_mapping as cmrys
			where cmrys.panel =	(
							select tp.id from sdesign.t_panel as tp where tp.stratum in
							(select ts.id from sdesign.t_stratum as ts where ts.strata_set in
							(select tss.id from sdesign.t_strata_set as tss where tss.country = 
							(select cc.id from sdesign.c_country as cc where cc.label = _country_ad)))
							and tp.panel = _array_panel_ad[i]
							)
			and cmrys.reference_year_set =	(
											select trys.id from sdesign.t_reference_year_set as trys where trys.id in
											(select cmr.reference_year_set from sdesign.cm_refyearset2panel_mapping as cmr where cmr.panel in
											(select tp.id from sdesign.t_panel as tp where tp.stratum in
											(select ts.id from sdesign.t_stratum as ts where ts.strata_set in
											(select tss.id from sdesign.t_strata_set as tss where tss.country = 
											(select cc.id from sdesign.c_country as cc where cc.label = _country_ad)))))
											and trys.reference_year_set = _array_reference_year_set_ad[i]
											)
			into _check_cmrys2pm_ad;

			if _check_cmrys2pm_ad is null
			then
				raise exception 'Error 12a: fn_etl_import_ldsity_values: Some of input combination JSON elements "panel = %" and "reference_year_set = %" in element "available_datasets" is not present in table sdesign.cm_refyearset2panel_mapping for input JSON element "country = %"!',_array_panel_ad[i], _array_reference_year_set_ad[i], _country_ad;
			end if;

			if i = 1
			then
				_array_cmrys2pm_ad := array[_check_cmrys2pm_ad];
			else
				_array_cmrys2pm_ad := _array_cmrys2pm_ad || array[_check_cmrys2pm_ad];
			end if;
		end loop;

		-- check combinations of panel and reference year set [element "ldsity_values"] --
		with
		w1 as	(
				select json_array_elements((_available_datasets_and_ldsity_values->>'ldsity_values')::json) as s
				)
		,w2 as	(
				select
						(s->>'panel')::varchar				as panel,
						(s->>'reference_year_set')::varchar	as reference_year_set
				from w1
				)
		,w3 as	(select distinct panel, reference_year_set from w2)
		select
				array_agg(w3.panel order by w3.panel, w3.reference_year_set) as panel,
				array_agg(w3.reference_year_set order by w3.panel, w3.reference_year_set) as reference_year_set
		from
				w3
		into
				_array_panel,
				_array_reference_year_set;

		for i in 1..array_length(_array_panel,1)
		loop
			select cmrys.id from sdesign.cm_refyearset2panel_mapping as cmrys
			where cmrys.panel =	(
							select tp.id from sdesign.t_panel as tp where tp.stratum in
							(select ts.id from sdesign.t_stratum as ts where ts.strata_set in
							(select tss.id from sdesign.t_strata_set as tss where tss.country = 
							(select cc.id from sdesign.c_country as cc where cc.label = _country)))
							and tp.panel = _array_panel[i]
							)
			and cmrys.reference_year_set =	(
											select trys.id from sdesign.t_reference_year_set as trys where trys.id in
											(select cmr.reference_year_set from sdesign.cm_refyearset2panel_mapping as cmr where cmr.panel in
											(select tp.id from sdesign.t_panel as tp where tp.stratum in
											(select ts.id from sdesign.t_stratum as ts where ts.strata_set in
											(select tss.id from sdesign.t_strata_set as tss where tss.country = 
											(select cc.id from sdesign.c_country as cc where cc.label = _country)))))
											and trys.reference_year_set = _array_reference_year_set[i]
											)
			into _check_cmrys2pm;

			if _check_cmrys2pm is null
			then
				raise exception 'Error 12b: fn_etl_import_ldsity_values: Some of input combination JSON elements "panel = %" and "reference_year_set = %" in element "ldsity_values" is not present in table sdesign.cm_refyearset2panel_mapping for input JSON element "country = %"!',_array_panel[i], _array_reference_year_set[i], _country;
			end if;

			if i = 1
			then
				_array_cmrys2pm := array[_check_cmrys2pm];
			else
				_array_cmrys2pm := _array_cmrys2pm || array[_check_cmrys2pm];
			end if;
		end loop;
		---------------------------------------------------------------------------------
		-- check NOT NULL of ldsity_threshold [element "available_datasets"] --
		with
		w1 as	(
				select json_array_elements((_available_datasets_and_ldsity_values->>'available_datasets')::json) as s
				)
		,w2 as	(
				select
						(s->>'ldsity_threshold')::double precision as ldsity_threshold
				from w1
				)
		select count(w2.*) from w2 where w2.ldsity_threshold is null
		into _check_ldsity_threshold;

		if _check_ldsity_threshold > 0
		then
			raise exception 'Error 13: fn_etl_import_ldsity_values: In element "available_datasets->>ldsity_threshold" is NULL"!';
		end if;
		---------------------------------------------------------------------------------
		-- check if inserting value ldsity_threshold is unique for combination panel,
		-- reference_year_set and variable [element "available_datasets"] --
		with
		w1 as	(
				select json_array_elements((_available_datasets_and_ldsity_values->>'available_datasets')::json) as s
				)
		,w2 as	(
				select
						(s->>'target_variable')::integer			as target_variable,
						(s->>'country')::varchar					as country,
						(s->>'strata_set')::varchar					as strata_set,
						(s->>'stratum')::varchar					as stratum,
						(s->>'reference_year_set')::varchar			as reference_year_set,
						(s->>'panel')::varchar						as panel,
						(s->>'cluster_configuration')::varchar		as cluster_configuration,
						(s->>'inventory_campaign')::varchar			as inventory_campaign,
						(s->>'sub_population_category')::integer	as sub_population_category,
						(s->>'area_domain_category')::integer		as area_domain_category,
						(s->>'ldsity_threshold')::double precision	as ldsity_threshold
				from w1
				)
		,w3 as	(
				select distinct t.t_panel__id, t.t_panel__panel, t.t_cluster_configuration__id, t.t_cluster_configuration__cluster_configuration,
				t.t_stratum__id, t.t_stratum__stratum, t.t_strata_set__id, t.t_strata_set__strata_set, t.c_country__id, t.c_country__label,
				t.t_reference_year_set__id, t.t_reference_year_set__reference_year_set, t.t_inventory_campaign__id, t.t_inventory_campaign__inventory
				from
						(
						select		
								t_panel.id as t_panel__id,
								t_panel.panel as t_panel__panel,
								t_cluster.id as t_cluster__id,
								t_cluster.cluster as t_cluster__cluster,
								f_p_plot.gid as f_p_plot__gid,
								f_p_plot.plot as f_p_plot__plot,
								t_cluster_configuration.id as t_cluster_configuration__id,
								t_cluster_configuration.cluster_configuration as t_cluster_configuration__cluster_configuration,
								t_stratum.id as t_stratum__id,
								t_stratum.stratum as t_stratum__stratum,
								t_strata_set.id as t_strata_set__id,
								t_strata_set.strata_set as t_strata_set__strata_set,
								c_country.id as c_country__id,
								c_country.label as c_country__label,
								t_reference_year_set.id as t_reference_year_set__id,
								t_reference_year_set.reference_year_set as t_reference_year_set__reference_year_set,
								t_inventory_campaign.id as t_inventory_campaign__id,
								t_inventory_campaign.inventory as t_inventory_campaign__inventory
						from
									sdesign.cm_refyearset2panel_mapping
						inner join	sdesign.t_panel on t_panel.id = cm_refyearset2panel_mapping.panel
						inner join	sdesign.cm_cluster2panel_mapping on cm_cluster2panel_mapping.panel = t_panel.id
						inner join	sdesign.t_cluster on t_cluster.id = cm_cluster2panel_mapping."cluster"
						inner join	sdesign.f_p_plot on f_p_plot."cluster" = t_cluster.id
						inner join	sdesign.cm_plot2cluster_config_mapping on f_p_plot.gid = cm_plot2cluster_config_mapping.plot
						inner join	sdesign.t_cluster_configuration on (t_panel.cluster_configuration = t_cluster_configuration.id and cm_plot2cluster_config_mapping.cluster_configuration = t_cluster_configuration.id)
						inner join	sdesign.t_stratum on t_panel.stratum = t_stratum.id
						inner join	sdesign.t_strata_set on t_stratum.strata_set = t_strata_set.id
						inner join	sdesign.c_country on t_strata_set.country = c_country.id
						inner join	sdesign.t_reference_year_set on cm_refyearset2panel_mapping.reference_year_set = t_reference_year_set.id
						inner join	sdesign.t_inventory_campaign on t_reference_year_set.inventory_campaign = t_inventory_campaign.id
						where
								cm_refyearset2panel_mapping.id in (select unnest(_array_cmrys2pm_ad))
						) as t
				)
		,w4 as	(
				select
						w2.*,
						w3.t_panel__id,
						w3.t_reference_year_set__id
				from
						w2 inner join w3
						on w2.panel = w3.t_panel__panel
						and w2.stratum = w3.t_stratum__stratum
						and w2.reference_year_set = w3.t_reference_year_set__reference_year_set
						and w2.inventory_campaign = w3.t_inventory_campaign__inventory
						and w2.cluster_configuration = w3.t_cluster_configuration__cluster_configuration
						and w2.strata_set = w3.t_strata_set__strata_set
						and w2.country = w3.c_country__label
				)
		,w5 as	(
				select
						id,
						target_variable,
						case when sub_population_category is null then 0 else sub_population_category end as sub_population_category,
						case when area_domain_category is null then 0 else area_domain_category end as area_domain_category
				from
						@extschema@.t_variable 
				where target_variable = (select distinct w2.target_variable from w2)
				)
		,w6 as	(
				select
						w4.target_variable,
						w4.t_panel__id as panel,
						w4.t_reference_year_set__id as reference_year_set,
						w5.id as variable,
						w4.ldsity_threshold
				from
						w4 inner join w5 on (w4.sub_population_category = w5.sub_population_category and w4.area_domain_category = w5.area_domain_category)
				)
		,w7 as	(
				select
						w6.target_variable,
						w6.panel,
						w6.reference_year_set,
						w6.variable,
						count(w6.ldsity_threshold) as count_ldsity_threshold
				from
						w6
				group by
						w6.target_variable,
						w6.panel,
						w6.reference_year_set,
						w6.variable
				)
		select count(w7.*) from w7 where w7.count_ldsity_threshold > 1
		into _check_ldsity_threshold_unique;

		if _check_ldsity_threshold_unique > 0
		then
			raise exception 'Error 14: fn_etl_import_ldsity_values: For insert vaules in element "available_datasets" not exists unique values for combination panel, reference_year_set, variable and ldsity_threshold"!';
		end if;
		---------------------------------------------------------------------------------
		---------------------------------------------------------------------------------
		-- get count of records in input JSON in part ldsity_values
		with
		w1 as	(
				select json_array_elements((_available_datasets_and_ldsity_values->>'ldsity_values')::json) as s
				)
		,w2 as	(
				select
						(s->>'target_variable')::integer			as target_variable,
						(s->>'country')::varchar					as country,
						(s->>'strata_set')::varchar					as strata_set,
						(s->>'stratum')::varchar					as stratum,
						(s->>'reference_year_set')::varchar			as reference_year_set,
						(s->>'panel')::varchar						as panel,
						(s->>'cluster')::varchar					as cluster,
						(s->>'cluster_configuration')::varchar		as cluster_configuration,
						(s->>'inventory_campaign')::varchar			as inventory_campaign,
						(s->>'plot')::varchar						as plot
				from w1
				)
		select count(w2.*) from w2
		into _check_count_records_input_json;
		---------------------------------------------------------------------------------
		-- get count of records after inner join with sdesign for given combinations of panels and reference year sets
		with
		w1 as	(
				select json_array_elements((_available_datasets_and_ldsity_values->>'ldsity_values')::json) as s
				)
		,w2 as	(
				select
						(s->>'target_variable')::integer			as target_variable,
						(s->>'country')::varchar					as country,
						(s->>'strata_set')::varchar					as strata_set,
						(s->>'stratum')::varchar					as stratum,
						(s->>'reference_year_set')::varchar			as reference_year_set,
						(s->>'panel')::varchar						as panel,
						(s->>'cluster')::varchar					as cluster,
						(s->>'cluster_configuration')::varchar		as cluster_configuration,
						(s->>'inventory_campaign')::varchar			as inventory_campaign,
						(s->>'plot')::varchar						as plot
				from w1
				)
		,w3 as	(
				select		
						t_panel.id as t_panel__id,
						t_panel.panel as t_panel__panel,
						t_cluster.id as t_cluster__id,
						t_cluster.cluster as t_cluster__cluster,
						f_p_plot.gid as f_p_plot__gid,
						f_p_plot.plot as f_p_plot__plot,
						t_cluster_configuration.id as t_cluster_configuration__id,
						t_cluster_configuration.cluster_configuration as t_cluster_configuration__cluster_configuration,
						t_stratum.id as t_stratum__id,
						t_stratum.stratum as t_stratum__stratum,
						t_strata_set.id as t_strata_set__id,
						t_strata_set.strata_set as t_strata_set__strata_set,
						c_country.id as c_country__id,
						c_country.label as c_country__label,
						t_reference_year_set.id as t_reference_year_set__id,
						t_reference_year_set.reference_year_set as t_reference_year_set__reference_year_set,
						t_inventory_campaign.id as t_inventory_campaign__id,
						t_inventory_campaign.inventory as t_inventory_campaign__inventory
				from
							sdesign.cm_refyearset2panel_mapping
				inner join	sdesign.t_panel on t_panel.id = cm_refyearset2panel_mapping.panel
				inner join	sdesign.cm_cluster2panel_mapping on cm_cluster2panel_mapping.panel = t_panel.id
				inner join	sdesign.t_cluster on t_cluster.id = cm_cluster2panel_mapping."cluster"
				inner join	sdesign.f_p_plot on f_p_plot."cluster" = t_cluster.id
				inner join	sdesign.cm_plot2cluster_config_mapping on f_p_plot.gid = cm_plot2cluster_config_mapping.plot
				inner join	sdesign.t_cluster_configuration on (t_panel.cluster_configuration = t_cluster_configuration.id and cm_plot2cluster_config_mapping.cluster_configuration = t_cluster_configuration.id)
				inner join	sdesign.t_stratum on t_panel.stratum = t_stratum.id
				inner join	sdesign.t_strata_set on t_stratum.strata_set = t_strata_set.id
				inner join	sdesign.c_country on t_strata_set.country = c_country.id
				inner join	sdesign.t_reference_year_set on cm_refyearset2panel_mapping.reference_year_set = t_reference_year_set.id
				inner join	sdesign.t_inventory_campaign on t_reference_year_set.inventory_campaign = t_inventory_campaign.id
				where
						cm_refyearset2panel_mapping.id in (select unnest(_array_cmrys2pm))
				)
		,w4 as	(
				select
						w2.*, w3.f_p_plot__gid
				from
						w2 inner join w3
						on w2.plot = w3.f_p_plot__plot
						and w2.cluster = w3.t_cluster__cluster
						and w2.panel = w3.t_panel__panel
						and w2.stratum = w3.t_stratum__stratum
						and w2.reference_year_set = w3.t_reference_year_set__reference_year_set
						and w2.inventory_campaign = w3.t_inventory_campaign__inventory
						and w2.cluster_configuration = w3.t_cluster_configuration__cluster_configuration
						and w2.strata_set = w3.t_strata_set__strata_set
						and w2.country = w3.c_country__label
				)
		select count(w4.*) from w4
		into _check_count_records_after_inner_join;
		---------------------------------------------------------------------------------
		-- check records
		if _check_count_records_input_json is distinct from _check_count_records_after_inner_join
		then
				raise exception 'Error 15: fn_etl_import_ldsity_values: Composite key in input JSON elements is not compatible with SDESIGN on target DB!';
		end if;
		---------------------------------------------------------------------------------
		---------------------------------------------------------------------------------
		-- INSERT available datasets INTO t_available_datasets table
		with
		w1 as	(
				select json_array_elements((_available_datasets_and_ldsity_values->>'available_datasets')::json) as s
				)
		,w2 as	(
				select
						(s->>'target_variable')::integer			as target_variable,
						(s->>'country')::varchar					as country,
						(s->>'strata_set')::varchar					as strata_set,
						(s->>'stratum')::varchar					as stratum,
						(s->>'reference_year_set')::varchar			as reference_year_set,
						(s->>'panel')::varchar						as panel,
						(s->>'cluster_configuration')::varchar		as cluster_configuration,
						(s->>'inventory_campaign')::varchar			as inventory_campaign,
						(s->>'sub_population_category')::integer	as sub_population_category,
						(s->>'area_domain_category')::integer		as area_domain_category,
						(s->>'ldsity_threshold')::double precision	as ldsity_threshold
				from w1
				)
		,w3 as	(
				select distinct t.t_panel__id, t.t_panel__panel, t.t_cluster_configuration__id, t.t_cluster_configuration__cluster_configuration,
				t.t_stratum__id, t.t_stratum__stratum, t.t_strata_set__id, t.t_strata_set__strata_set, t.c_country__id, t.c_country__label,
				t.t_reference_year_set__id, t.t_reference_year_set__reference_year_set, t.t_inventory_campaign__id, t.t_inventory_campaign__inventory
				from
						(
						select		
								t_panel.id as t_panel__id,
								t_panel.panel as t_panel__panel,
								t_cluster.id as t_cluster__id,
								t_cluster.cluster as t_cluster__cluster,
								f_p_plot.gid as f_p_plot__gid,
								f_p_plot.plot as f_p_plot__plot,
								t_cluster_configuration.id as t_cluster_configuration__id,
								t_cluster_configuration.cluster_configuration as t_cluster_configuration__cluster_configuration,
								t_stratum.id as t_stratum__id,
								t_stratum.stratum as t_stratum__stratum,
								t_strata_set.id as t_strata_set__id,
								t_strata_set.strata_set as t_strata_set__strata_set,
								c_country.id as c_country__id,
								c_country.label as c_country__label,
								t_reference_year_set.id as t_reference_year_set__id,
								t_reference_year_set.reference_year_set as t_reference_year_set__reference_year_set,
								t_inventory_campaign.id as t_inventory_campaign__id,
								t_inventory_campaign.inventory as t_inventory_campaign__inventory
						from
									sdesign.cm_refyearset2panel_mapping
						inner join	sdesign.t_panel on t_panel.id = cm_refyearset2panel_mapping.panel
						inner join	sdesign.cm_cluster2panel_mapping on cm_cluster2panel_mapping.panel = t_panel.id
						inner join	sdesign.t_cluster on t_cluster.id = cm_cluster2panel_mapping."cluster"
						inner join	sdesign.f_p_plot on f_p_plot."cluster" = t_cluster.id
						inner join	sdesign.cm_plot2cluster_config_mapping on f_p_plot.gid = cm_plot2cluster_config_mapping.plot
						inner join	sdesign.t_cluster_configuration on (t_panel.cluster_configuration = t_cluster_configuration.id and cm_plot2cluster_config_mapping.cluster_configuration = t_cluster_configuration.id)
						inner join	sdesign.t_stratum on t_panel.stratum = t_stratum.id
						inner join	sdesign.t_strata_set on t_stratum.strata_set = t_strata_set.id
						inner join	sdesign.c_country on t_strata_set.country = c_country.id
						inner join	sdesign.t_reference_year_set on cm_refyearset2panel_mapping.reference_year_set = t_reference_year_set.id
						inner join	sdesign.t_inventory_campaign on t_reference_year_set.inventory_campaign = t_inventory_campaign.id
						where
								cm_refyearset2panel_mapping.id in (select unnest(_array_cmrys2pm_ad))
						) as t
				)
		,w4 as	(
				select
						w2.*,
						w3.t_panel__id,
						w3.t_reference_year_set__id
				from
						w2 inner join w3
						on w2.panel = w3.t_panel__panel
						and w2.stratum = w3.t_stratum__stratum
						and w2.reference_year_set = w3.t_reference_year_set__reference_year_set
						and w2.inventory_campaign = w3.t_inventory_campaign__inventory
						and w2.cluster_configuration = w3.t_cluster_configuration__cluster_configuration
						and w2.strata_set = w3.t_strata_set__strata_set
						and w2.country = w3.c_country__label
				)
		,w5 as	(
				select
						id,
						target_variable,
						case when sub_population_category is null then 0 else sub_population_category end as sub_population_category,
						case when area_domain_category is null then 0 else area_domain_category end as area_domain_category
				from
						@extschema@.t_variable 
				where target_variable = (select distinct w2.target_variable from w2)
				)
		,w6 as	(
				select
						w4.target_variable,
						w4.t_panel__id as panel,
						w4.t_reference_year_set__id as reference_year_set,
						w5.id as variable,
						w4.ldsity_threshold
				from
						w4 inner join w5 on (w4.sub_population_category = w5.sub_population_category and w4.area_domain_category = w5.area_domain_category)
				)
		,w7 as	(-- new records for insert without ldsity_threshold
				select w6.panel, w6.reference_year_set, w6.variable from w6
				except
				select panel, reference_year_set, variable from @extschema@.t_available_datasets
				--where variable in (select w6.variable from w6))
				)
		,w8 as	(-- recods for update without ldsity_threshold
				select w6.panel, w6.reference_year_set, w6.variable from w6
				except
				select w7.panel, w7.reference_year_set, w7.variable from w7
				)
		,w9 as	(
				-- new records for insert with ldsity_threshold
				select w6.* from w6 inner join w7
				on w6.panel = w7.panel
				and w6.reference_year_set = w7.reference_year_set
				and w6.variable = w7.variable
				)
		,w10 as	(
				-- records for update with ldsity_threshold
				select w6.* from w6 inner join w8
				on w6.panel = w8.panel
				and w6.reference_year_set = w8.reference_year_set
				and w6.variable = w8.variable
				)
		,w11 as	(
				update @extschema@.t_available_datasets
				set
					ldsity_threshold = w10.ldsity_threshold
				from
					w10
				where
					w10.panel = t_available_datasets.panel
				and
					w10.reference_year_set = t_available_datasets.reference_year_set
				and
					w10.variable = t_available_datasets.variable
				)
		insert into @extschema@.t_available_datasets(panel,reference_year_set,variable,ldsity_threshold,last_change)
		select w9.panel, w9.reference_year_set, w9.variable, w9.ldsity_threshold, now() from w9
		order by w9.panel, w9.reference_year_set, w9.variable;
		---------------------------------------------------------------------------------
		---------------------------------------------------------------------------------
		-- INSERT ldsity values INTO t_target_data table
		with
		w1 as	(
				select json_array_elements((_available_datasets_and_ldsity_values->>'ldsity_values')::json) as s
				)
		,w2 as	(
				select
						(s->>'target_variable')::integer			as target_variable,
						(s->>'country')::varchar					as country,
						(s->>'strata_set')::varchar					as strata_set,
						(s->>'stratum')::varchar					as stratum,
						(s->>'reference_year_set')::varchar			as reference_year_set,
						(s->>'panel')::varchar						as panel,
						(s->>'cluster')::varchar					as cluster,
						(s->>'cluster_configuration')::varchar		as cluster_configuration,
						(s->>'inventory_campaign')::varchar			as inventory_campaign,
						(s->>'plot')::varchar						as plot,
						(s->>'sub_population_category')::integer	as sub_population_category,
						(s->>'area_domain_category')::integer		as area_domain_category,
						(s->>'value')::float						as value
				from w1
				)
		,w3 as	(
				select		
						t_panel.id as t_panel__id,
						t_panel.panel as t_panel__panel,
						t_cluster.id as t_cluster__id,
						t_cluster.cluster as t_cluster__cluster,
						f_p_plot.gid as f_p_plot__gid,
						f_p_plot.plot as f_p_plot__plot,
						t_cluster_configuration.id as t_cluster_configuration__id,
						t_cluster_configuration.cluster_configuration as t_cluster_configuration__cluster_configuration,
						t_stratum.id as t_stratum__id,
						t_stratum.stratum as t_stratum__stratum,
						t_strata_set.id as t_strata_set__id,
						t_strata_set.strata_set as t_strata_set__strata_set,
						c_country.id as c_country__id,
						c_country.label as c_country__label,
						t_reference_year_set.id as t_reference_year_set__id,
						t_reference_year_set.reference_year_set as t_reference_year_set__reference_year_set,
						t_inventory_campaign.id as t_inventory_campaign__id,
						t_inventory_campaign.inventory as t_inventory_campaign__inventory
				from
							sdesign.cm_refyearset2panel_mapping
				inner join	sdesign.t_panel on t_panel.id = cm_refyearset2panel_mapping.panel
				inner join	sdesign.cm_cluster2panel_mapping on cm_cluster2panel_mapping.panel = t_panel.id
				inner join	sdesign.t_cluster on t_cluster.id = cm_cluster2panel_mapping."cluster"
				inner join	sdesign.f_p_plot on f_p_plot."cluster" = t_cluster.id
				inner join	sdesign.cm_plot2cluster_config_mapping on f_p_plot.gid = cm_plot2cluster_config_mapping.plot
				inner join	sdesign.t_cluster_configuration on (t_panel.cluster_configuration = t_cluster_configuration.id and cm_plot2cluster_config_mapping.cluster_configuration = t_cluster_configuration.id)
				inner join	sdesign.t_stratum on t_panel.stratum = t_stratum.id
				inner join	sdesign.t_strata_set on t_stratum.strata_set = t_strata_set.id
				inner join	sdesign.c_country on t_strata_set.country = c_country.id
				inner join	sdesign.t_reference_year_set on cm_refyearset2panel_mapping.reference_year_set = t_reference_year_set.id
				inner join	sdesign.t_inventory_campaign on t_reference_year_set.inventory_campaign = t_inventory_campaign.id
				where
						cm_refyearset2panel_mapping.id in (select unnest(_array_cmrys2pm))
				)		
		,w4 as	(
				select
						w2.*,
						w3.f_p_plot__gid,
						w3.t_panel__id,
						w3.t_reference_year_set__id
				from
						w2 inner join w3
						on w2.plot = w3.f_p_plot__plot
						and w2.cluster = w3.t_cluster__cluster
						and w2.panel = w3.t_panel__panel
						and w2.stratum = w3.t_stratum__stratum
						and w2.reference_year_set = w3.t_reference_year_set__reference_year_set
						and w2.inventory_campaign = w3.t_inventory_campaign__inventory
						and w2.cluster_configuration = w3.t_cluster_configuration__cluster_configuration
						and w2.strata_set = w3.t_strata_set__strata_set
						and w2.country = w3.c_country__label
				)
		,w5 as	(
				select
						id,
						target_variable,
						case when sub_population_category is null then 0 else sub_population_category end as sub_population_category,
						case when area_domain_category is null then 0 else area_domain_category end as area_domain_category
				from
						@extschema@.t_variable 
				where target_variable = (select distinct w2.target_variable from w2)
				)
		,w6 as	(
				select
						w4.target_variable,
						w4.f_p_plot__gid as plot,
						w4.value,
						w4.t_panel__id as panel,
						w4.t_reference_year_set__id as reference_year_set,
						w5.id as variable
				from
						w4 inner join w5 on (w4.sub_population_category = w5.sub_population_category and w4.area_domain_category = w5.area_domain_category)
				)
		,w7 as	(
				select distinct w6.target_variable, w6.plot, w6.panel, w6.value, w6.reference_year_set, w6.variable from w6
				)
		,w10 as	(
				select
						w7.*,
						tad.id as available_datasets,
						tad.ldsity_threshold
				from
						w7
						inner join @extschema@.t_available_datasets as tad
				on	(
					w7.panel = tad.panel and
					w7.reference_year_set = tad.reference_year_set and
					w7.variable = tad.variable
					)
				)
		,w_ttd as	(
					select
							ttd.id,
							ttd.plot,
							(	select t_panel.id as panel_upr
								from 		sdesign.f_p_plot
								inner join 	sdesign.t_cluster on (f_p_plot.cluster = t_cluster.id)
								inner join	sdesign.cm_cluster2panel_mapping on (t_cluster.id = cm_cluster2panel_mapping.cluster)
								inner join	sdesign.t_panel on (cm_cluster2panel_mapping.panel = t_panel.id)
								inner join	sdesign.t_cluster_configuration on (t_panel.cluster_configuration = t_cluster_configuration.id)
								inner join	sdesign.cm_plot2cluster_config_mapping on (t_cluster_configuration.id = cm_plot2cluster_config_mapping.cluster_configuration and
																f_p_plot.gid = cm_plot2cluster_config_mapping.plot)
								where f_p_plot.gid = coalesce(w10.plot,ttd.plot)
							),
							ttd.value,
							coalesce(w10.plot,ttd.plot) as plot_upr,
							coalesce(w10.available_datasets, ttd.available_datasets) as available_datasets_upr,
							case
								when ttd.value is     null and w10.value is not null then w10.value
								when ttd.value is not null and ttd.value is distinct from 0.0 and w10.value is null then 0.0
								when ttd.value is not null and ttd.value = 0.0 and w10.value is null then null::double precision
								else w10.value
							end
								as value_upr,
							case
								when ttd.value is     null and w10.value is not null then false
								when ttd.value is not null and ttd.value is distinct from 0.0 and w10.value is null then false
								when ttd.value is not null and ttd.value = 0.0 and w10.value is null then null::boolean									
								else
									case
										when ttd.value = 0.0 and w10.value = 0.0 then true
										when ttd.value = 0.0 and w10.value is distinct from 0.0
												then
													case
														when (abs(1 - (ttd.value / w10.value)) * 100.0) <= w10.ldsity_threshold then true
														else false
													end
										when ttd.value is distinct from 0.0 and w10.value = 0.0
												then
													case
														when (abs(1 - (w10.value / ttd.value)) * 100.0) <= w10.ldsity_threshold then true
														else false
													end
										else
											case
												when (abs(1 - (ttd.value / w10.value)) * 100.0) <= w10.ldsity_threshold then true
												else false
											end
									end									
							end
								as value_identic				
					from
						(
						select * 
						from @extschema@.t_target_data
						where available_datasets in (
							select id 
							from @extschema@.t_available_datasets 
							where panel in (select distinct panel from w10)
							and variable in (select id from @extschema@.t_variable where target_variable = (select distinct w2.target_variable from w2))
							and reference_year_set in (select distinct w4.t_reference_year_set__id from w4)
						)
						and plot in (select distinct w10.plot from w10)
						and is_latest = true
						) as ttd
					full outer join w10
							on (ttd.plot = w10.plot and ttd.available_datasets = w10.available_datasets)
					)
		,w_update as	(
						update @extschema@.t_target_data set is_latest = false where id in
						(select id from w_ttd where value_identic = false and id is not null)
						returning t_target_data.plot, t_target_data.available_datasets
						)
		insert into @extschema@.t_target_data(plot,value,value_inserted,is_latest,available_datasets)
		select
				w_ttd.plot_upr as plot,
				w_ttd.value_upr as value,
				now() as value_inserted,
				true as is_latest,
				available_datasets_upr
		from
				w_ttd
				left join w_update on w_ttd.plot_upr = w_update.plot and w_ttd.available_datasets_upr = w_update.available_datasets
		where
				w_ttd.value_identic = false
		order
				by w_ttd.plot_upr, w_ttd.available_datasets_upr;

	_res_ttd := concat('The ',(select count(*) from @extschema@.t_target_data where id > _max_id_ttd),' new local densities were inserted for ',(select count(t.plot) from (select distinct plot from @extschema@.t_target_data where id > _max_id_ttd) as t),' plots.');
	_res_tad := concat('The ',(select count(*) from @extschema@.t_available_datasets where id > _max_id_tad),' new rows were inserted into t_available_datasets.');
			
	_res := _res_ttd || ' ' || _res_tad;

	return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_import_ldsity_values(json) IS
'The function for the specified list of input arguments inserts data into the t_available_datasets table and inserts data into the t_target_data table (aggregated local density at the plot level).';

grant execute on function @extschema@.fn_etl_import_ldsity_values(json) to public;

-- </function>


