--
-- Copyright 2017, 2023 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--


---------------------------------------------------------------------------------------------------
-- c_area_domain and c_sub_population => add column atomic boolean
---------------------------------------------------------------------------------------------------
ALTER TABLE @extschema@.c_area_domain ADD COLUMN atomic boolean;
COMMENT ON COLUMN @extschema@.c_area_domain.atomic IS 'Indenticator if given area domain is (true) or not (false) atomic category.';

ALTER TABLE @extschema@.c_sub_population ADD COLUMN atomic boolean;
COMMENT ON COLUMN @extschema@.c_sub_population.atomic IS 'Indenticator if given area domain is (true) or not (false) atomic category.';
---------------------------------------------------------------------------------------------------



DROP FUNCTION IF EXISTS @extschema@.fn_etl_import_area_domain(integer, varchar, text, varchar, text) CASCADE;
DROP FUNCTION IF EXISTS @extschema@.fn_etl_import_sub_population(integer, varchar, text, varchar, text) CASCADE;
DROP FUNCTION IF EXISTS @extschema@.fn_etl_get_area_domains(integer, integer[]) CASCADE;
DROP FUNCTION IF EXISTS @extschema@.fn_etl_get_sub_populations(integer, integer[]) CASCADE;
DROP FUNCTION IF EXISTS @extschema@.fn_etl_check_area_domain(integer, varchar) CASCADE;
DROP FUNCTION IF EXISTS @extschema@.fn_etl_check_sub_population(integer, varchar) CASCADE;


-- <function name="fn_etl_import_area_domain" schema="extschema" src="functions/extschema/etl/fn_etl_import_area_domain.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_import_area_domain(integer, varchar, text, varchar, text, boolean)

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_import_area_domain(integer, varchar, text, varchar, text, boolean) CASCADE;

create or replace function @extschema@.fn_etl_import_area_domain
(
	_id					integer,
	_label				varchar,
	_description		text,
	_label_en			varchar,
	_description_en		text,
	_atomic				boolean
)
returns table
(
	id		integer,
	etl_id	integer
)
as
$$
declare
		_etl_id		integer;
begin
	if _id is null
	then
		raise exception 'Error 01: fn_etl_import_area_domain: Input argument _id must not be NULL!';
	end if;

	if _label is null
	then
		raise exception 'Error 02: fn_etl_import_area_domain: Input argument _label must not be NULL!';
	end if;

	if _description is null
	then
		raise exception 'Error 03: fn_etl_import_area_domain: Input argument _description must not be NULL!';
	end if; 

	if _label_en is null
	then
		raise exception 'Error 04: fn_etl_import_area_domain: Input argument _label_en must not be NULL!';
	end if;

	if _description_en is null
	then
		raise exception 'Error 05: fn_etl_import_area_domain: Input argument _description must not be NULL!';
	end if; 	

	if _atomic is null
	then
		raise exception 'Error 06: fn_etl_import_area_domain: Input argument _atomic must not be NULL!';
	end if; 		

	insert into @extschema@.c_area_domain(label, description, label_en, description_en, atomic)
	select _label, _description, _label_en, _description_en, _atomic
	returning c_area_domain.id
	into _etl_id;

	return query select _id, _etl_id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_import_area_domain(integer, varchar, text, varchar, text, boolean) is
'Function inserts a record into table c_area_domain based on given parameters.';

grant execute on function @extschema@.fn_etl_import_area_domain(integer, varchar, text, varchar, text, boolean) to public;
-- </function>



-- <function name="fn_etl_import_sub_population" schema="extschema" src="functions/extschema/etl/fn_etl_import_sub_population.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_import_sub_population(integer, varchar, text, varchar, text, boolean)

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_import_sub_population(integer, varchar, text, varchar, text, boolean) CASCADE;

create or replace function @extschema@.fn_etl_import_sub_population
(
	_id					integer,
	_label				varchar,
	_description		text,
	_label_en			varchar,
	_description_en		text,
	_atomic				boolean
)
returns table
(
	id		integer,
	etl_id	integer
)
as
$$
declare
		_etl_id		integer;
begin
	if _id is null
	then
		raise exception 'Error 01: fn_etl_import_sub_population: Input argument _id must not be NULL!';
	end if;

	if _label is null
	then
		raise exception 'Error 02: fn_etl_import_sub_population: Input argument _label must not be NULL!';
	end if;

	if _description is null
	then
		raise exception 'Error 03: fn_etl_import_sub_population: Input argument _description must not be NULL!';
	end if;

	if _label_en is null
	then
		raise exception 'Error 04: fn_etl_import_sub_population: Input argument _label_en must not be NULL!';
	end if;

	if _description_en is null
	then
		raise exception 'Error 05: fn_etl_import_sub_population: Input argument _description_en must not be NULL!';
	end if;

	if _atomic is null
	then
		raise exception 'Error 06: fn_etl_import_sub_population: Input argument _atomic must not be NULL!';
	end if;  	  

	insert into @extschema@.c_sub_population(label, description, label_en, description_en, atomic)
	select _label, _description, _label_en, _description_en, _atomic
	returning c_sub_population.id
	into _etl_id;

	return query select _id, _etl_id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_import_sub_population(integer, varchar, text, varchar, text, boolean) is
'Function inserts a record into table c_sub_population based on given parameters.';

grant execute on function @extschema@.fn_etl_import_sub_population(integer, varchar, text, varchar, text, boolean) to public;
-- </function>



-- <function name="fn_etl_get_area_domains" schema="extschema" src="functions/extschema/etl/fn_etl_get_area_domains.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_get_area_domains(integer, integer[])

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_get_area_domains(integer, integer[]) CASCADE;

create or replace function @extschema@.fn_etl_get_area_domains
(
	_id			integer,
	_etl_id		integer[] default null::integer[]
)
returns table
(
	id				integer,
	etl_id			integer,
	label			varchar,
	description		text,
	label_en		varchar,
	description_en	text,
	atomic			boolean
)
as
$$
declare
begin
		if _id is null
		then
			raise exception 'Error 01: fn_etl_get_area_domains: Input argument _id must not be null!';
		end if;

		if _etl_id is null
		then
			return query
			select
					_id as id,
					ad.id as etl_id,
					ad.label,
					ad.description,
					ad.label_en,
					ad.description_en,
					ad.atomic
			from
					@extschema@.c_area_domain as ad
			order
					by ad.id;
		else
			return query
			select
					_id as id,
					ad.id as etl_id,
					ad.label,
					ad.description,
					ad.label_en,
					ad.description_en,
					ad.atomic
			from
					@extschema@.c_area_domain as ad
			where
					ad.id not in (select unnest(_etl_id))
			order
					by ad.id;
		end if;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_get_area_domains(integer, integer[]) is
'Function returns all record from table c_area_domain.';

grant execute on function @extschema@.fn_etl_get_area_domains(integer, integer[]) to public;
-- </function>



-- <function name="fn_etl_get_sub_populations" schema="extschema" src="functions/extschema/etl/fn_etl_get_sub_populations.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_get_sub_populations(integer, integer[])

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_get_sub_populations(integer, integer[]) CASCADE;

create or replace function @extschema@.fn_etl_get_sub_populations
(
	_id			integer,
	_etl_id		integer[] default null::integer[]
)
returns table
(
	id				integer,
	etl_id			integer,
	label			varchar,
	description		text,
	label_en		varchar,
	description_en	text,
	atomic			boolean
)
as
$$
declare
begin
		if _id is null
		then
			raise exception 'Error 01: fn_etl_get_sub_populations: Input argument _id must not be null!';
		end if;

		if _etl_id is null
		then
			return query
			select
					_id as id,
					sp.id as etl_id,
					sp.label,
					sp.description,
					sp.label_en,
					sp.description_en,
					sp.atomic
			from
					@extschema@.c_sub_population as sp
			order
					by sp.id;
		else
			return query
			select
					_id as id,
					sp.id as etl_id,
					sp.label,
					sp.description,
					sp.label_en,
					sp.description_en,
					sp.atomic
			from
					@extschema@.c_sub_population as sp
			where
					sp.id not in (select unnest(_etl_id))					
			order
					by sp.id;		
		end if;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_get_sub_populations(integer, integer[]) is
'Function returns all record from table c_sup_population.';

grant execute on function @extschema@.fn_etl_get_sub_populations(integer, integer[]) to public;
-- </function>



-- <function name="fn_etl_check_area_domain" schema="extschema" src="functions/extschema/etl/fn_etl_check_area_domain.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_check_area_domain(integer, varchar)

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_check_area_domain(integer, varchar) CASCADE;

create or replace function @extschema@.fn_etl_check_area_domain
(
	_id			integer,
	_label_en	varchar
)
returns table
(
	id				integer,
	etl_id			integer,
	label			character varying,
	description		text,
	label_en		character varying,
	description_en	text,
	atomic			boolean
)
as
$$
declare
		_res_etl_id				integer;
		_res_label				character varying;
		_res_description		text;
		_res_label_en			character varying;
		_res_description_en		text;
		_res_atomic				boolean;
begin
		if _id is null
		then
			raise exception 'Error 01: fn_etl_check_area_domain: Input argument _id must not be NULL!';
		end if; 
	
		if _label_en is null
		then
			raise exception 'Error 02: fn_etl_check_area_domain: Input argument _label_en must not be NULL!';
		end if;
	
		select
				cad.id,
				cad.label,
				cad.description,
				cad.label_en,
				cad.description_en,
				cad.atomic
		from
				@extschema@.c_area_domain as cad
		where
				@extschema@.fn_etl_array_compare
					(
					string_to_array(replace(lower(cad.label_en),' ',''),';'),
					string_to_array(replace(lower(_label_en),' ',''),';')					
					)
		into
				_res_etl_id,
				_res_label,
				_res_description,
				_res_label_en,
				_res_description_en,
				_res_atomic;

		return query select _id, _res_etl_id, _res_label, _res_description, _res_label_en, _res_description_en, _res_atomic;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_check_area_domain(integer, varchar) is
'Function returns record ID from table c_area_domain based on given parameters.';

grant execute on function @extschema@.fn_etl_check_area_domain(integer, varchar) to public;
-- </function>



-- <function name="fn_etl_check_sub_population" schema="extschema" src="functions/extschema/etl/fn_etl_check_sub_population.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_check_sub_population(integer, varchar)

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_check_sub_population(integer, varchar) CASCADE;

create or replace function @extschema@.fn_etl_check_sub_population
(
	_id			integer,
	_label_en	varchar
)
returns table
(
	id				integer,
	etl_id			integer,
	label			character varying,
	description		text,
	label_en		character varying,
	description_en	text,
	atomic			boolean
)
as
$$
declare
		_res_etl_id				integer;
		_res_label				character varying;
		_res_description		text;
		_res_label_en			character varying;
		_res_description_en		text;
		_res_atomic				boolean;
begin
		if _id is null
		then
			raise exception 'Error 01: fn_etl_check_sub_population: Input argument _id must not by NULL!';
		end if; 
	
		if _label_en is null
		then
			raise exception 'Error 02: fn_etl_check_sub_population: Input argument _label_en must not by NULL!';
		end if;
	
		select
				csp.id,
				csp.label,
				csp.description,
				csp.label_en,
				csp.description_en,
				csp.atomic
		from
				@extschema@.c_sub_population as csp
		where
				@extschema@.fn_etl_array_compare
					(
					string_to_array(replace(lower(csp.label_en),' ',''),';'),
					string_to_array(replace(lower(_label_en),' ',''),';')
					)
		into
				_res_etl_id,
				_res_label,
				_res_description,
				_res_label_en,
				_res_description_en,
				_res_atomic;

		return query select _id, _res_etl_id, _res_label, _res_description, _res_label_en, _res_description_en, _res_atomic;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_check_sub_population(integer, varchar) is
'Function returns record ID from table c_sup_population based on given parameters.';

grant execute on function @extschema@.fn_etl_check_sub_population(integer, varchar) to public;
-- </function>



---------------------------------------------------------------------------------------------------
-- cm_area_domain_category
---------------------------------------------------------------------------------------------------
--
-- Name: cm_area_domain_category; Type: TABLE; Schema: @extschema@. Owner: -
--

CREATE TABLE @extschema@.cm_area_domain_category
(
    id integer NOT NULL,
    area_domain_category integer NOT NULL,
    atomic_category integer NOT NULL
);


--
-- Name: TABLE cm_area_domain_category; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON TABLE @extschema@.cm_area_domain_category IS 'Mapping table of area domain categories.';


--
-- Name: COLUMN cm_area_domain_category.id; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.cm_area_domain_category.id IS 'Identifier of mapping, primary key.';


--
-- Name: COLUMN cm_area_domain_category.area_domain_category; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.cm_area_domain_category.area_domain_category IS 'Identifier of area domain category, foreign key to table c_area_domain_category.';


--
-- Name: COLUMN cm_area_domain_category.atomic_category; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.cm_area_domain_category.atomic_category IS 'Identifier of atomic area domain category, foreign key to table c_area_domain_category.';


--
-- Name: cm_area_domain_category_id_seq; Type: SEQUENCE; Schema: @extschema@. Owner: -
--

CREATE SEQUENCE @extschema@.cm_area_domain_category_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: cm_area_domain_category_id_seq; Type: SEQUENCE OWNED BY; Schema: @extschema@. Owner: -
--

ALTER SEQUENCE @extschema@.cm_area_domain_category_id_seq OWNED BY @extschema@.cm_area_domain_category.id;


--
-- Name: cm_area_domain_category id; Type: DEFAULT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.cm_area_domain_category ALTER COLUMN id SET DEFAULT nextval('@extschema@.cm_area_domain_category_id_seq'::regclass);


--
-- Name: cm_area_domain_category pkey__cm_area_domain_category; Type: CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.cm_area_domain_category
    ADD CONSTRAINT pkey__cm_area_domain_category PRIMARY KEY (id);


--
-- Name: CONSTRAINT pkey__cm_area_domain_category ON cm_area_domain_category; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON CONSTRAINT pkey__cm_area_domain_category ON @extschema@.cm_area_domain_category IS 'Primary key.';


--
-- Name: cm_area_domain_category fkey__cm_area_domain_category__c_area_domain_category_1; Type: FK CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.cm_area_domain_category
    ADD CONSTRAINT fkey__cm_area_domain_category__c_area_domain_category_1 FOREIGN KEY (area_domain_category) REFERENCES @extschema@.c_area_domain_category(id);


--
-- Name: CONSTRAINT fkey__cm_area_domain_category__c_area_domain_category_1 ON cm_area_domain_category; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON CONSTRAINT fkey__cm_area_domain_category__c_area_domain_category_1 ON @extschema@.cm_area_domain_category IS 'Foreign key to table @extschema@.c_area_domain_category.';


--
-- Name: cm_area_domain_category fkey__cm_area_domain_category__c_area_domain_category_2; Type: FK CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.cm_area_domain_category
    ADD CONSTRAINT fkey__cm_area_domain_category__c_area_domain_category_2 FOREIGN KEY (atomic_category) REFERENCES @extschema@.c_area_domain_category(id);


--
-- Name: CONSTRAINT fkey__cm_area_domain_category__c_area_domain_category_2 ON cm_area_domain_category; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON CONSTRAINT fkey__cm_area_domain_category__c_area_domain_category_2 ON @extschema@.cm_area_domain_category IS 'Foreign key to table @extschema@.c_area_domain_category.';


--
-- Name: cm_area_domain_category ukey__cm_area_domain_category; Type: UK CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.cm_area_domain_category
    ADD CONSTRAINT ukey__cm_area_domain_category UNIQUE(area_domain_category,atomic_category);


--
-- Name: CONSTRAINT ukey__cm_area_domain_category ON cm_area_domain_category; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON CONSTRAINT ukey__cm_area_domain_category ON @extschema@.cm_area_domain_category IS 'Unique key on columns area_domain_category and atomic_category.';


--
-- Name: cm_area_domain_category fki__cm_area_domain_category__area_domain_category; Type: FKI; Schema: @extschema@. Owner: -
--

CREATE INDEX fki__cm_area_domain_category__area_domain_category
  ON @extschema@.cm_area_domain_category USING btree (area_domain_category);


--
-- Name: cm_area_domain_category fki__cm_area_domain_category__area_domain_category; Type: COMMENT; Schema: @extschema@. Owner: -
--  

COMMENT ON INDEX @extschema@.fki__cm_area_domain_category__area_domain_category IS 'B-tree index on column area_domain_category.';


--
-- Name: cm_area_domain_category fki__cm_area_domain_category__atomic_category; Type: FKI; Schema: @extschema@. Owner: -
--

CREATE INDEX fki__cm_area_domain_category__atomic_category
  ON @extschema@.cm_area_domain_category USING btree (atomic_category);


--
-- Name: cm_area_domain_category fki__cm_area_domain_category__atomic_category; Type: COMMENT; Schema: @extschema@. Owner: -
--  

COMMENT ON INDEX @extschema@.fki__cm_area_domain_category__atomic_category IS 'B-tree index on column atomic_category.';
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------



---------------------------------------------------------------------------------------------------
-- cm_sub_population_category
---------------------------------------------------------------------------------------------------
--
-- Name: cm_sub_population_category; Type: TABLE; Schema: @extschema@. Owner: -
--

CREATE TABLE @extschema@.cm_sub_population_category
(
    id integer NOT NULL,
    sub_population_category integer NOT NULL,
    atomic_category integer NOT NULL
);


--
-- Name: TABLE cm_sub_population_category; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON TABLE @extschema@.cm_sub_population_category IS 'Mapping table of sub population categories.';


--
-- Name: COLUMN cm_sub_population_category.id; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.cm_sub_population_category.id IS 'Identifier of mapping, primary key.';


--
-- Name: COLUMN cm_sub_population_category.sub_population_category; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.cm_sub_population_category.sub_population_category IS 'Identifier of sub population category, foreign key to table c_sub_population_category.';


--
-- Name: COLUMN cm_sub_population_category.atomic_category; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.cm_sub_population_category.atomic_category IS 'Identifier of atomic sub population category, foreign key to table c_sub_population_category.';


--
-- Name: cm_sub_population_category_id_seq; Type: SEQUENCE; Schema: @extschema@. Owner: -
--

CREATE SEQUENCE @extschema@.cm_sub_population_category_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: cm_sub_population_category_id_seq; Type: SEQUENCE OWNED BY; Schema: @extschema@. Owner: -
--

ALTER SEQUENCE @extschema@.cm_sub_population_category_id_seq OWNED BY @extschema@.cm_sub_population_category.id;


--
-- Name: cm_sub_population_category id; Type: DEFAULT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.cm_sub_population_category ALTER COLUMN id SET DEFAULT nextval('@extschema@.cm_sub_population_category_id_seq'::regclass);


--
-- Name: cm_sub_population_category pkey__cm_sub_population_category; Type: CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.cm_sub_population_category
    ADD CONSTRAINT pkey__cm_sub_population_category PRIMARY KEY (id);


--
-- Name: CONSTRAINT pkey__cm_sub_population_category ON cm_sub_population_category; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON CONSTRAINT pkey__cm_sub_population_category ON @extschema@.cm_sub_population_category IS 'Primary key.';


--
-- Name: cm_sub_population_category fkey__cm_sub_population_category__c_sub_population_category_1; Type: FK CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.cm_sub_population_category
    ADD CONSTRAINT fkey__cm_sub_population_category__c_sub_population_category_1 FOREIGN KEY (sub_population_category) REFERENCES @extschema@.c_sub_population_category(id);


--
-- Name: CONSTRAINT fkey__cm_sub_population_category__c_sub_population_category_1 ON cm_sub_population_category; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON CONSTRAINT fkey__cm_sub_population_category__c_sub_population_category_1 ON @extschema@.cm_sub_population_category IS 'Foreign key to table @extschema@.c_sub_population_category.';


--
-- Name: cm_sub_population_category fkey__cm_sub_population_category__c_sub_population_category_2; Type: FK CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.cm_sub_population_category
    ADD CONSTRAINT fkey__cm_sub_population_category__c_sub_population_category_2 FOREIGN KEY (atomic_category) REFERENCES @extschema@.c_sub_population_category(id);


--
-- Name: CONSTRAINT fkey__cm_sub_population_category__c_sub_population_category_2 ON cm_sub_population_category; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON CONSTRAINT fkey__cm_sub_population_category__c_sub_population_category_2 ON @extschema@.cm_sub_population_category IS 'Foreign key to table @extschema@.c_sub_population_category.';


--
-- Name: cm_sub_population_category ukey__cm_sub_population_category; Type: UK CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.cm_sub_population_category
    ADD CONSTRAINT ukey__cm_sub_population_category UNIQUE(sub_population_category,atomic_category);


--
-- Name: CONSTRAINT ukey__cm_sub_population_category ON cm_sub_population_category; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON CONSTRAINT ukey__cm_sub_population_category ON @extschema@.cm_sub_population_category IS 'Unique key on columns sub_population_category and atomic_category.';


--
-- Name: cm_sub_population_category fki__cm_sub_population_category__sub_population_category; Type: FKI; Schema: @extschema@. Owner: -
--

CREATE INDEX fki__cm_sub_population_category__sub_population_category
  ON @extschema@.cm_sub_population_category USING btree (sub_population_category);


--
-- Name: cm_sub_population_category fki__cm_sub_population_category__sub_population_category; Type: COMMENT; Schema: @extschema@. Owner: -
--  

COMMENT ON INDEX @extschema@.fki__cm_sub_population_category__sub_population_category IS 'B-tree index on column sub_population_category.';


--
-- Name: cm_sub_population_category fki__cm_sub_population_category__atomic_category; Type: FKI; Schema: @extschema@. Owner: -
--

CREATE INDEX fki__cm_sub_population_category__atomic_category
  ON @extschema@.cm_sub_population_category USING btree (atomic_category);


--
-- Name: cm_sub_population_category fki__cm_sub_population_category__atomic_category; Type: COMMENT; Schema: @extschema@. Owner: -
--  

COMMENT ON INDEX @extschema@.fki__cm_sub_population_category__atomic_category IS 'B-tree index on column atomic_category.';
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------



-- <function name="fn_etl_import_area_domain_category_mapping" schema="extschema" src="functions/extschema/etl/fn_etl_import_area_domain_category_mapping.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_import_area_domain_category_mapping(json)

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_import_area_domain_category_mapping(json) CASCADE;

create or replace function @extschema@.fn_etl_import_area_domain_category_mapping
(
	_adc_mapping	json
)
returns text
as
$$
declare
		_max_id_cm_area_domain_category		integer;
		_check_atomic_id					integer;
		_res								text;
begin
		-------------------------------------------------------------
		_max_id_cm_area_domain_category := (select coalesce(max(id),0) from @extschema@.cm_area_domain_category);
		-------------------------------------------------------------
		with
		w1 as	(
				select json_array_elements(_adc_mapping) as s
				)
		,w2 as	(
				select
						(s->>'area_domain_category_non_atomic')::integer	as area_domain_category_non_atomic,
						(s->>'area_domain_category_atomic')::integer		as area_domain_category_atomic
				from w1
				)
		,w3 as	(-- NON ATOMIC TYPE for inserting non atomic category
				select distinct cadc1.area_domain from @extschema@.c_area_domain_category as cadc1
				where cadc1.id in (select w2.area_domain_category_non_atomic from w2)
				)
		,w4 as	(-- ATOMIC TYPE for inserting atomic category
				select distinct cadc2.area_domain from @extschema@.c_area_domain_category as cadc2
				where cadc2.id in (select w2.area_domain_category_atomic from w2)
				)
		,w5 as	(
				select
						cad1.id,
						cad1.label_en,
						string_to_array(cad1.label_en,';') as label_en_array
				from
						@extschema@.c_area_domain as cad1
				where
						cad1.id in (select w3.area_domain from w3)
				)		
		,w6 as	(
				select w5.id, w5.label_en, unnest(w5.label_en_array) as label_en_i from w5
				)
		,w7 as	(
				select w6.*, cad3.id as atomic_id from w6
				left join (select cad2.* from @extschema@.c_area_domain as cad2 where cad2.id in (select w4.area_domain from w4)) as cad3
				on w6.label_en_i = cad3.label_en
				)
		select count(w7.*) from w7 where w7.atomic_id is null
		into _check_atomic_id;
		-------------------------------------------------------------
		if _check_atomic_id > 0
		then
			raise exception 'Error 01: fn_etl_import_area_domain_category_mapping: For some of inserting non atomic category into cm_area_domain_category table not exists their atomic type in c_area_domain table!';
		end if;
		-------------------------------------------------------------
		-- adding data into cm_area_domain_category
		with
		w1 as	(
				select json_array_elements(_adc_mapping) as s
				)
		,w2 as	(
				select
						(s->>'area_domain_category_non_atomic')::integer	as area_domain_category,
						(s->>'area_domain_category_atomic')::integer		as atomic_category
				from w1
				)
		,w3 as (
				select w2.area_domain_category, w2.atomic_category from w2 except
				select t1.area_domain_category, t1.atomic_category from @extschema@.cm_area_domain_category as t1
				)
		insert into @extschema@.cm_area_domain_category(area_domain_category,atomic_category)
		select w3.area_domain_category, w3.atomic_category from w3
		order by w3.area_domain_category, w3.atomic_category;
		-------------------------------------------------------------
		-------------------------------------------------------------
		_res := concat('The ',(select count(*) from @extschema@.cm_area_domain_category where id > _max_id_cm_area_domain_category),' new records were inserted into cm_area_domain_category table.');
		-------------------------------------------------------------
		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_import_area_domain_category_mapping(json) is
'Function inserts a new records into table cm_area_domain_category.';

grant execute on function @extschema@.fn_etl_import_area_domain_category_mapping(json) to public;
-- </function>



-- <function name="fn_etl_import_sub_population_category_mapping" schema="extschema" src="functions/extschema/etl/fn_etl_import_sub_population_category_mapping.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_import_sub_population_category_mapping(json)

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_import_sub_population_category_mapping(json) CASCADE;

create or replace function @extschema@.fn_etl_import_sub_population_category_mapping
(
	_spc_mapping	json
)
returns text
as
$$
declare
		_max_id_cm_sub_population_category	integer;
		_check_atomic_id					integer;
		_res								text;
begin
		-------------------------------------------------------------
		_max_id_cm_sub_population_category := (select coalesce(max(id),0) from @extschema@.cm_sub_population_category);
		-------------------------------------------------------------
		with
		w1 as	(
				select json_array_elements(_spc_mapping) as s
				)
		,w2 as	(
				select
						(s->>'sub_population_category_non_atomic')::integer	as sub_population_category_non_atomic,
						(s->>'sub_population_category_atomic')::integer		as sub_population_category_atomic
				from w1
				)
		,w3 as	(-- NON ATOMIC TYPE for inserting non atomic category
				select distinct cspc1.sub_population from @extschema@.c_sub_population_category as cspc1
				where cspc1.id in (select w2.sub_population_category_non_atomic from w2)
				)
		,w4 as	(-- ATOMIC TYPE for inserting atomic category
				select distinct cspc2.sub_population from @extschema@.c_sub_population_category as cspc2
				where cspc2.id in (select w2.sub_population_category_atomic from w2)
				)
		,w5 as	(
				select
						csp1.id,
						csp1.label_en,
						string_to_array(csp1.label_en,';') as label_en_array
				from
						@extschema@.c_sub_population as csp1
				where
						csp1.id in (select w3.sub_population from w3)
				)		
		,w6 as	(
				select w5.id, w5.label_en, unnest(w5.label_en_array) as label_en_i from w5
				)
		,w7 as	(
				select w6.*, csp3.id as atomic_id from w6
				left join (select csp2.* from @extschema@.c_sub_population as csp2 where csp2.id in (select w4.sub_population from w4)) as csp3
				on w6.label_en_i = csp3.label_en
				)
		select count(w7.*) from w7 where w7.atomic_id is null
		into _check_atomic_id;
		-------------------------------------------------------------
		if _check_atomic_id > 0
		then
			raise exception 'Error 01: fn_etl_import_sub_population_category_mapping: For some of inserting non atomic category into cm_sub_population_category table not exists their atomic type in c_sub_population table!';
		end if;
		-------------------------------------------------------------
		-- adding data into cm_sub_population_category
		with
		w1 as	(
				select json_array_elements(_spc_mapping) as s
				)
		,w2 as	(
				select
						(s->>'sub_population_category_non_atomic')::integer	as sub_population_category,
						(s->>'sub_population_category_atomic')::integer		as atomic_category
				from w1
				)
		,w3 as (
				select w2.sub_population_category, w2.atomic_category from w2 except
				select t1.sub_population_category, t1.atomic_category from @extschema@.cm_sub_population_category as t1
				)
		insert into @extschema@.cm_sub_population_category(sub_population_category,atomic_category)
		select w3.sub_population_category, w3.atomic_category from w3
		order by w3.sub_population_category, w3.atomic_category;
		-------------------------------------------------------------
		-------------------------------------------------------------
		_res := concat('The ',(select count(*) from @extschema@.cm_sub_population_category where id > _max_id_cm_sub_population_category),' new records were inserted into cm_sub_population_category table.');
		-------------------------------------------------------------
		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_import_sub_population_category_mapping(json) is
'Function inserts a new records into table cm_sub_population_category.';

grant execute on function @extschema@.fn_etl_import_sub_population_category_mapping(json) to public;
-- </function>



---------------------------------------------------------------------------------------------------
-- c_area_domain and c_sub_population => update column atomic and set not null
---------------------------------------------------------------------------------------------------
update @extschema@.c_area_domain set atomic = true where array_length(string_to_array(label_en,';'),1) = 1;
update @extschema@.c_area_domain set atomic = false where atomic is null;

update @extschema@.c_sub_population set atomic = true where array_length(string_to_array(label_en,';'),1) = 1;
update @extschema@.c_sub_population set atomic = false where atomic is null;

alter table @extschema@.c_area_domain alter column atomic set not null;
alter table @extschema@.c_sub_population alter column atomic set not null;
---------------------------------------------------------------------------------------------------