--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

ALTER TABLE @extschema@.t_total_estimate_conf ALTER COLUMN total_estimate_conf TYPE varchar(250);

ALTER TABLE @extschema@.t_panel ADD COLUMN panel_subset integer;
COMMENT ON COLUMN @extschema@.t_panel.panel_subset IS 'A foreign key which points to the panel from which is the current panel subseted. It is mainly designed to solve problems like "hierarchical" arrangement of panels, where one panel contains a subset of plots from another panel.';

ALTER TABLE @extschema@.t_panel ADD CONSTRAINT
fkey__t_panel__t_panel FOREIGN KEY (panel_subset)
REFERENCES @extschema@.t_panel(id);

DROP FUNCTION @extschema@.fn_get_panels_in_estimation_cell(integer, date, date, integer);

-- <function name="fn_get_panels_in_estimation_cells" schema="extschema" src="functions/extschema/configuration/fn_get_panels_in_estimation_cells.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_get_panels_in_estimation_cells(integer, regclass)
--DROP FUNCTION @extschema@.fn_get_panels_in_estimation_cells(integer,date,date,character varying,integer,integer);
CREATE OR REPLACE FUNCTION @extschema@.fn_get_panels_in_estimation_cells(_estimation_cells integer[], _estimate_date_begin date, _estimate_date_end date, _target_variable integer)
RETURNS TABLE (
stratum 			integer,
stratum_label 			varchar(20),
panel 				integer,
panel_label			varchar(20),
panel_subset			integer,
reference_year_set 		integer,
reference_year_set_label 	varchar(20),
reference_date_begin 		date,
reference_date_end 		date,
reference_year_set_fit 		boolean,
portion_of_panel_inside		double precision,
portion_of_period_covered	double precision,
total 				integer,
is_max				boolean
)
AS
$function$
DECLARE
_change_variable 	boolean;
_estimation_cell	integer;
_cell_area_m2		double precision;
_stratas_all		integer[];
_stratas		integer[];
_sum_stratas_m2		double precision;
_result_test		boolean;

BEGIN

_change_variable := (
		SELECT		CASE WHEN t3.id = 2 THEN true ELSE false END
		FROM 		@extschema@.t_variable AS t1
		LEFT JOIN 	@extschema@.c_target_variable AS t2 ON t1.target_variable = t2.id
		LEFT JOIN	@extschema@.c_state_or_change AS t3 ON t2.state_or_change = t3.id
		WHERE t1.id = $5
		);
	
-- panels order
--IF _panels IS NOT NULL THEN _panels := (SELECT array_agg(panel ORDER BY panel) FROM unnest(_panels) AS t(panel));
--END IF;

FOREACH _estimation_cell IN ARRAY _estimation_cells
LOOP

	-- cell data
	WITH w_cell AS (
		-- cell geometry can be divided into smaller blocks, thats why sum() 
		SELECT estimation_cell, sum(st_area(geom)) AS area_m2
		FROM @extschema@.f_a_cell
		WHERE estimation_cell = _estimation_cell
		GROUP BY estimation_cell
	)
	SELECT	area_m2
	FROM w_cell
	INTO _cell_area_m2;

	-- stratas in cell
	WITH w_stratas AS (
		SELECT t1.stratum, t1.area_m2
		FROM @extschema@.t_stratum_in_estimation_cell AS t1
		WHERE estimation_cell = _estimation_cell
	)
	SELECT	array_agg(t1.stratum), sum(t1.area_m2)
	FROM w_stratas AS t1
	INTO _stratas, _sum_stratas_m2;

	IF _sum_stratas_m2 IS NULL
	THEN
		RAISE EXCEPTION 'Estimation cell is not covered by any stratum (estimation_cell = %).', _estimation_cell;
	END IF;

	-- test on cell coverage
	-- two decimal places considered when comparing the final product of division with 1
	_result_test := (SELECT CASE WHEN round(_sum_stratas_m2::numeric/_cell_area_m2::numeric, 2) = 1 THEN true ELSE false END);
	
	IF _result_test = false
	THEN
		RAISE WARNING 'The intersection of stratas and cell (% ha) is less than area of whole estimation cell (% ha).', 
				round(_sum_stratas_m2::numeric/10000.0,1), round(_cell_area_m2::numeric/10000.0,1);
	END IF;

	_stratas_all := _stratas_all || _stratas;

END LOOP;

-- return the list of panels for given target variable
-- if the reference time period is met then it is stated in column reference_year_set_fit
RETURN QUERY
WITH w_data AS (
	SELECT
		t1.id AS stratum,
		t1.stratum AS stratum_label, 
		t2.panel, 
		t2.panel_label,
		t2.panel_subset,
		t2.reference_year_set,
		t2.reference_year_set_label,
		t2.reference_date_begin,
		t2.reference_date_end,
		t2.reference_year_set_fit,
		t2.estimate_range,
		t2.reference_range,
		t2.intersection_range,

		-- find out what portion of panel is inside the estimate range
		CASE WHEN isempty(intersection_range) = false
		THEN
			EXTRACT(DAYS FROM (upper(intersection_range) - lower(intersection_range))) / -- interval of intersection
			EXTRACT(DAYS FROM (upper(reference_range) - lower(reference_range))) -- interval of reference period

		ELSE
			0 -- 0 percent of panel inside the period
		END AS portion_of_panel_inside,

		-- find out what portion of estimation period is covered by panel
		CASE WHEN isempty(intersection_range) = false
		THEN
			EXTRACT(DAYS FROM (upper(intersection_range) - lower(intersection_range))) / -- interval of intersection
			EXTRACT(DAYS FROM (upper(estimate_range) - lower(estimate_range))) -- interval of estimation period
		ELSE 0 -- 0 percent covered by panel
		END AS portion_of_period_covered,
		t2.total
	FROM
		(SELECT t1.id, t1.stratum FROM @extschema@.t_stratum AS t1 WHERE array[id] <@ _stratas_all) AS t1
	-- could be inner join, every stratum has to have at least one panel
	-- but in case of not available target variable, stratum would disappear from the list
	LEFT JOIN
		(SELECT
			t2.stratum,
			t2.id AS panel,
			t2.panel AS panel_label,
			t2.plot_count AS total,
			t2.panel_subset,
			t4.id AS reference_year_set,
			t4.reference_year_set AS reference_year_set_label,
			t4.reference_date_begin,
			t4.reference_date_end,
			-- test if reference_year_set fits the parameteres given
			CASE
			WHEN 	t4.reference_date_begin >= $2 AND
				t4.reference_date_end <= $3 
			THEN true
			ELSE false
			END AS reference_year_set_fit,

			-- range of estimation period
			tsrange(_estimate_date_begin,_estimate_date_end) AS estimate_range,
			-- range of reference period
			tsrange(t4.reference_date_begin,t4.reference_date_end) AS reference_range,

			-- intersection of dates
			tsrange(t4.reference_date_begin,t4.reference_date_end) * 
			tsrange(_estimate_date_begin,_estimate_date_end) AS intersection_range

		FROM
			@extschema@.t_panel AS t2
		-- inner join, assuming every panel has SOME reference_year_set mapping
		INNER JOIN
			@extschema@.cm_refyearset2panel_mapping AS t3
		ON
			t2.id = t3.panel
		-- again inner join, I am curious, what other reference year sets are possible in case the required dates does not fit (but target variable available)
		INNER JOIN
			@extschema@.t_reference_year_set AS t4
		ON
			t3.reference_year_set = t4.id
		-- inner join, target variable must fit
		INNER JOIN
			@extschema@.t_available_datasets AS t5
		ON
			t2.id = t5.panel AND
			t4.id = t5.reference_year_set AND
			t5.variable = $4 
		) AS t2
	ON
		t1.id = t2.stratum

), w_max AS (
	SELECT 
		t1.stratum, t1.stratum_label, t1.panel, t1.panel_label, t1.panel_subset,
		t1.reference_year_set, t1.reference_year_set_label, t1.reference_date_begin, t1.reference_date_end, t1.reference_year_set_fit, 
		t1.portion_of_panel_inside, t1.portion_of_period_covered,
		t1.total,
		-- maximum number of plots for a group of panels with the same reference_year_set 
		-- and within belonging to the required reference period
		max(t1.total) OVER(PARTITION BY t1.stratum, t1.reference_year_set, t1.reference_year_set_fit) AS max_total
	FROM w_data AS t1
)
SELECT
	t1.stratum, t1.stratum_label, t1.panel, t1.panel_label, t1.panel_subset,
	t1.reference_year_set, t1.reference_year_set_label, t1.reference_date_begin, t1.reference_date_end, t1.reference_year_set_fit, 
	t1.portion_of_panel_inside, t1.portion_of_period_covered, t1.total,
	CASE WHEN t1.total = t1.max_total THEN true ELSE false END AS is_max
FROM
	w_max AS t1;

END;
$function$
LANGUAGE plpgsql
PARALLEL SAFE;

COMMENT ON FUNCTION @extschema@.fn_get_panels_in_estimation_cells(integer[], date, date, integer) IS 'Function returns list of stratas and panels in the estimation cell. For each panel there is an indicator (reference_year_set_fit) if the panel lies within estimation period and indicator max which tells if the panel is the one with the maximum possible plots.';

-- </function>

DROP FUNCTION @extschema@.fn_est_configuration(integer,date,date,character varying,integer,integer[]);
-- <function name="fn_est_configuration" schema="extschema" src="functions/extschema/configuration/fn_est_configuration.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_est_configuration(integer, regclass)
--DROP FUNCTION @extschema@.fn_est_configuration(integer,date,date,character varying,integer,integer[]);
CREATE OR REPLACE FUNCTION @extschema@.fn_est_configuration(_estimation_cell integer, _estimate_date_begin date, _estimate_date_end date, _note varchar, _target_variable integer, _panels integer[] DEFAULT NULL)
RETURNS setof integer
AS
$function$
DECLARE
_total_estimate_conf		integer;
_stratas			integer[];
_panels_used			integer[];
_refyearsets			integer[];
_estimation_cells		integer[];
BEGIN

_estimation_cells := array[_estimation_cell];
-- panels order
IF _panels IS NOT NULL THEN _panels := (SELECT array_agg(panel ORDER BY panel) FROM unnest(_panels) AS t(panel));
END IF;

	-- panels with target variable in specified stratas (panels are the ones with the less granularity, hence 1 stratum can have e.g. 4 panels which together results in 1 big panel)
	WITH w_data AS MATERIALIZED (
		SELECT
			stratum, panel, reference_year_set, reference_year_set_fit, total, is_max
		FROM
			@extschema@.fn_get_panels_in_estimation_cells(_estimation_cells, _estimate_date_begin, _estimate_date_end, _target_variable)
		)
	SELECT
		array_agg(DISTINCT stratum ORDER BY stratum) AS stratas,
		array_agg(panel ORDER BY panel) AS panels,
		array_agg(reference_year_set ORDER BY panel) AS refyearsets
	FROM
		w_data
	WHERE
		-- pick up the most dense panel with target variable
		CASE WHEN _panels IS NOT NULL THEN ARRAY[panel] <@ _panels ELSE
		is_max = true
		END
	INTO _stratas, _panels_used, _refyearsets;

	-- simple check if panels found are the same as panels required
	IF _panels IS NOT NULL AND (_panels != _panels_used OR _panels_used IS NULL)
	THEN
		RAISE EXCEPTION 'Required panels does not meet the computation criteria (measured target variable for given estimation period)!
Only these panels from specified array can be used: (%). Or You can try to not specify panels, the function will try to find the maximum of possible panels.', _panels_used;
	END IF;

RETURN QUERY
	SELECT @extschema@.fn_1p_est_configuration(_panels_used, _refyearsets, _estimation_cells, _estimate_date_begin, _estimate_date_end, _note, _target_variable);
END;
$function$
LANGUAGE plpgsql
PARALLEL SAFE;

COMMENT ON FUNCTION @extschema@.fn_est_configuration(integer, date, date, varchar, integer, integer[]) IS 'Wrapper function. Handles data between fn_get_panels_in_estimation_cells and fn_1p_est_configuration.';

-- </function>

DROP FUNCTION @extschema@.fn_1p_est_configuration(integer[], integer[], integer[], integer, date, date, character varying, integer);
-- <function name="fn_1p_est_configuration" schema="extschema" src="functions/extschema/configuration/fn_1p_est_configuration.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_1p_est_configuration(integer[], integer[], integer[], integer[], date, date, varchar, integer)
--DROP FUNCTION @extschema@.fn_1p_est_configuration(integer[], integer[], integer[], integer[], date, date, character varying, integer);
CREATE OR REPLACE FUNCTION @extschema@.fn_1p_est_configuration(
		_panels integer[], _refyearsets integer[], 
		_estimation_cells integer[], _estimate_date_begin date, _estimate_date_end date, 
		_note varchar, _variable integer)
RETURNS integer
AS
$function$
DECLARE
_estimation_cell		integer;
_total_estimate_conf		integer;
_stratas_wp			integer[];
_target_label			varchar;
_cell				varchar;
_change_variable		boolean;
_panels_ec			integer[];
_refyearsets_ec			integer[];
BEGIN

IF _panels IS NULL THEN
	RAISE NOTICE 'Input array of panels is NULL!';
END IF;
-- test on the same length of each array (except estimation_cells)
IF
	array_length(_panels,1) != array_length(_refyearsets,1)
THEN
	RAISE EXCEPTION 'Input arrays of panels (%) and refyearsets (%) are not the same length.', _panels, _refyearsets;
END IF; 

IF _estimate_date_begin >= _estimate_date_end
THEN
	RAISE EXCEPTION 'Estimate_date_begin (%) is bigger then estimate_date_end (%) or they are equal!', _estimate_date_begin, _estimate_date_end;
END IF;

-- loop through an array of estimation cells
FOREACH _estimation_cell IN ARRAY _estimation_cells
LOOP
	_target_label := (
			SELECT		replace(
						replace(
							concat(coalesce(t2.label,'x'), ',', coalesce(t3.label,'x'), ',', coalesce(t4.label,'x'), ',', coalesce(t5.label,'x')),
						'x,',''),
					',x','') AS label
			FROM		@extschema@.t_variable AS t1
			LEFT JOIN 	@extschema@.c_target_variable AS t2 ON t1.target_variable = t2.id
			LEFT JOIN	@extschema@.c_sub_population_category AS t3 ON t1.sub_population_category = t3.id
			LEFT JOIN	@extschema@.c_area_domain_category AS t4 ON t1.area_domain_category = t4.id
			LEFT JOIN	@extschema@.c_auxiliary_variable_category AS t5 ON t1.auxiliary_variable_category = t5.id
			WHERE
				t1.id = _variable
			);


	_change_variable := (
			SELECT		CASE WHEN t3.id = 2 THEN true ELSE false END
			FROM 		@extschema@.t_variable AS t1
			LEFT JOIN 	@extschema@.c_target_variable AS t2 ON t1.target_variable = t2.id
			LEFT JOIN	@extschema@.c_state_or_change AS t3 ON t2.state_or_change = t3.id
			WHERE t1.id = _variable
			);
		

	_cell := (SELECT label FROM @extschema@.c_estimation_cell WHERE id = _estimation_cell);



	-- find out which panels belongs to specified estimation_cell
	SELECT
		array_agg(t3.panel ORDER BY t2.id), array_agg(t4.refyearset ORDER BY t2.id)
	FROM
		@extschema@.t_panel AS t1
	INNER JOIN
		@extschema@.t_stratum_in_estimation_cell AS t2
	ON
		t1.stratum = t2.stratum
	INNER JOIN
		unnest(_panels) WITH ORDINALITY AS t3(panel, id)
	ON t1.id = t3.panel
	INNER JOIN
		unnest(_refyearsets) WITH ORDINALITY AS t4(refyearset, id)
	ON t3.id = t4.id
	WHERE t2.estimation_cell = _estimation_cell
	INTO _panels_ec, _refyearsets_ec;
	
	-- reset sequence (usually when previous atempt to configure fails, the sequence remains shifted)
	PERFORM setval('@extschema@.t_total_estimate_conf_id_seq', (SELECT coalesce(max(id),0)+1 FROM @extschema@.t_total_estimate_conf), FALSE);

	-- insert into table t_total_estimate_conf
	INSERT INTO @extschema@.t_total_estimate_conf (estimation_cell, estimate_date_begin, estimate_date_end, total_estimate_conf, target_variable, phase_estimate_type, aux_conf)
	VALUES
		(_estimation_cell, _estimate_date_begin, _estimate_date_end, concat('1p;T=',_target_label,';Cell=',_cell,_note), _variable, 1, NULL)
	RETURNING id
	INTO _total_estimate_conf;


		-- panels with target variable in specified stratas (panels are the ones with the less granularity, hence 1 stratum can have e.g. 4 panels which together results in 1 big panel)

		-- if it is change variable, the panels resulted in previous query are those who have the target variable available,
		-- that, they have been measured at least twice, otherwise the change target variable would not be available
		-- but there must be also reference for the beginning of the reference period of sample panel
		-- in other words, the sample panel must be measured twice WITHIN the given period
		-- the given period must be specified according to estimation of the change e.g.
		
		-- reference year sets for panel is lets say 2011 and 2016
		-- for the estimation of state the given period would be only 1.1.2016-31.12.2016
		-- byt for the estimation of the change it must be 1.1.2011-31.12.2016 to take this panel accounted
/*
		IF _change_variable = true
		THEN
			SELECT
				array_agg(DISTINCT stratum ORDER BY stratum) AS stratas,
				array_agg(t1.panel ORDER BY t1.panel) AS panels
			FROM
				@extschema@.t_panel AS t1
			INNER JOIN
				@extschema@.cm_refyearset2panel_mapping AS t2
			ON	t1.id = t2.panel
			INNER JOIN
				@extschema@.t_reference_year_set AS t3
			ON	t2.reference_year_set = t3.id
			WHERE
				t2.panel = ANY(_panels_ec) AND		-- panels measured twice
				NOT t3.id = ANY(_refyearsets_ec) AND	-- give away reference year sets already accounted
				t3.reference_date_begin >= $5 AND	-- condition for the given period
				t3.reference_date_end <= $6
			INTO _stratas_wp, _panels_wp;			-- resulted list of panels used for calculation
		END IF;
*/

	-- insert into table t_panel2total_2ndph_estimate_conf
	INSERT INTO @extschema@.t_panel2total_2ndph_estimate_conf (total_estimate_conf, panel, reference_year_set)
	SELECT
		_total_estimate_conf, panel, reference_year_set
	FROM
		unnest(_panels_ec) WITH ORDINALITY AS t1(panel, id)
	INNER JOIN
		unnest(_refyearsets_ec) WITH ORDINALITY AS t2(reference_year_set,id)
	ON
		t1.id = t2.id;

	-- insert into table t_estimate_conf
	INSERT INTO @extschema@.t_estimate_conf (estimate_type, total_estimate_conf, denominator)
	SELECT 1, _total_estimate_conf, NULL;

	RETURN _total_estimate_conf;
END LOOP;
END;
$function$
LANGUAGE plpgsql
PARALLEL SAFE;

COMMENT ON FUNCTION @extschema@.fn_1p_est_configuration(integer[],integer[],integer[],date,date,varchar,integer) IS 'Function makes the estimate configuration present in the database - provides inserts into the all necessary tables.';

-- </function>


