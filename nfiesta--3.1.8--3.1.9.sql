--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- <function name="fn_add_triggers" schema="extschema" src="functions/extschema/additivity/fn_add_triggers.sql">
--
-- Copyright 2021, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

CREATE OR REPLACE FUNCTION @extschema@.fn_prevent_update()
  RETURNS trigger AS
$BODY$
    BEGIN
        RAISE EXCEPTION 'fn_prevent_update -- UPDATE -- only update of is_latests is possible';
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

drop trigger if exists trg__target_data__pr_upd ON @extschema@.t_target_data;
CREATE TRIGGER trg__target_data__pr_upd
BEFORE UPDATE OF id, plot, value, value_inserted, available_datasets -- only update of is_latests is possible
ON @extschema@.t_target_data
FOR EACH ROW
EXECUTE PROCEDURE @extschema@.fn_prevent_update();

--------------------------------------------------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION @extschema@.fn_check_update()
  RETURNS trigger AS
$BODY$
    BEGIN
	IF (old.is_latest = false AND new.is_latest = true) THEN
	        RAISE EXCEPTION 'fn_check_update -- UPDATE -- only set is_latest to false is possible. use INSERT for new valid values';
	END IF;
	RETURN NEW;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

drop trigger if exists trg__target_data__ch_upd ON @extschema@.t_target_data;
CREATE TRIGGER trg__target_data__ch_upd
AFTER UPDATE OF is_latest
ON @extschema@.t_target_data
FOR EACH ROW
EXECUTE PROCEDURE @extschema@.fn_check_update();

--------------------------------------------------------------------------------------------------------------------------------

--drop function @extschema@.fn_update_last_change() cascade;
CREATE OR REPLACE FUNCTION @extschema@.fn_update_last_change() RETURNS TRIGGER AS $src$
    DECLARE
    BEGIN
	IF (TG_TABLE_NAME = 't_target_data' or TG_TABLE_NAME = 't_auxiliary_data') THEN
        	IF (TG_OP = 'DELETE') THEN
			RAISE EXCEPTION 'fn_update_last_change -- DELETE not permited (UPDATE to is_latest = false instead)';
		ELSIF (TG_OP = 'UPDATE') THEN
			with w_data as (
				select 
					available_datasets, now() as max_val_upd
				from trans_table
				group by available_datasets
			)
			update @extschema@.t_available_datasets 
			set last_change = w_data.max_val_upd
			from w_data
			where t_available_datasets.id = w_data.available_datasets
			;
			/*raise notice '%		fn_update_last_change -- % -- %: () %', 
				clock_timestamp()::timestamp with time zone at time zone 'Europe/Prague', 
				TG_TABLE_NAME, TG_OP, 
				(select json_agg(r) 
					from (
						select
							json_build_object(
								'available_datasets', available_datasets, 
								'max_val_upd', now()
							) as r
						from trans_table
						group by available_datasets
					) as foo
				);*/
		ELSIF (TG_OP = 'INSERT') THEN
			with w_data as (
				select 
					available_datasets, max(value_inserted) as max_val_ins
				from trans_table
				group by available_datasets
			)
			update @extschema@.t_available_datasets 
			set last_change = w_data.max_val_ins
			from w_data
			where t_available_datasets.id = w_data.available_datasets
			;
			/*raise notice '%		fn_update_last_change -- % -- %: () %', 
				clock_timestamp()::timestamp with time zone at time zone 'Europe/Prague', 
				TG_TABLE_NAME, TG_OP, 
				(select json_agg(r)
					from (
						select 
							json_build_object(
								'available_datasets', available_datasets, 
								'max_val_ins', max(value_inserted)
						) as r
						from trans_table
						group by available_datasets
					) as foo
				);*/
		ELSE
			RAISE EXCEPTION 'fn_update_last_change -- trigger operation not known: %', TG_OP;
		END IF;
	ELSE
		RAISE EXCEPTION 'fn_update_last_change -- % -- table name not known: %', TG_OP, TG_TABLE_NAME;
	END IF;
        RETURN NULL; -- result is ignored since this is an AFTER trigger
    END;
$src$ LANGUAGE plpgsql;

drop trigger if exists trg__target_data__ins ON @extschema@.t_target_data;
CREATE TRIGGER trg__target_data__ins
    AFTER INSERT ON @extschema@.t_target_data
    REFERENCING NEW TABLE AS trans_table
    FOR EACH STATEMENT EXECUTE FUNCTION @extschema@.fn_update_last_change();

drop trigger if exists trg__target_data__upd ON @extschema@.t_target_data;
CREATE TRIGGER trg__target_data__upd
    AFTER UPDATE ON @extschema@.t_target_data
    REFERENCING NEW TABLE AS trans_table
    FOR EACH STATEMENT EXECUTE FUNCTION @extschema@.fn_update_last_change();

drop trigger if exists trg__target_data__del ON @extschema@.t_target_data;
CREATE TRIGGER trg__target_data__del
    AFTER DELETE ON @extschema@.t_target_data
    REFERENCING OLD TABLE AS trans_table
    FOR EACH STATEMENT EXECUTE FUNCTION @extschema@.fn_update_last_change();

---------------------------------------------
drop trigger if exists trg__auxiliary_data__ins ON @extschema@.t_auxiliary_data;
CREATE TRIGGER trg__auxiliary_data__ins
    AFTER INSERT ON @extschema@.t_auxiliary_data
    REFERENCING NEW TABLE AS trans_table
    FOR EACH STATEMENT EXECUTE FUNCTION @extschema@.fn_update_last_change();

drop trigger if exists trg__auxiliary_data__upd ON @extschema@.t_auxiliary_data;
CREATE TRIGGER trg__auxiliary_data__upd
    AFTER UPDATE ON @extschema@.t_auxiliary_data
    REFERENCING NEW TABLE AS trans_table
    FOR EACH STATEMENT EXECUTE FUNCTION @extschema@.fn_update_last_change();

drop trigger if exists trg__auxiliary_data__del ON @extschema@.t_auxiliary_data;
CREATE TRIGGER trg__auxiliary_data__del
    AFTER DELETE ON @extschema@.t_auxiliary_data
    REFERENCING OLD TABLE AS trans_table
    FOR EACH STATEMENT EXECUTE FUNCTION @extschema@.fn_update_last_change();


---------------------------------------------

CREATE OR REPLACE FUNCTION @extschema@.fn_refresh_t_additivity_set_plot()
RETURNS trigger LANGUAGE plpgsql AS $$
BEGIN
	--RAISE NOTICE 'fn_refresh_t_additivity_set_plot -- % -- %', TG_OP, TG_TABLE_NAME;
	IF (TG_TABLE_NAME = 't_variable_hierarchy') THEN
    	IF (TG_OP = 'DELETE') THEN
        	RAISE EXCEPTION 'fn_refresh_t_additivity_set_plot -- DELETE on table t_variable_hierarchy not permited';
        ELSIF (TG_OP = 'INSERT' OR TG_OP = 'UPDATE') THEN
			with w_data as (
				SELECT 	node_ads.id as node_ads_id,
					t_variable_hierarchy.variable_superior AS node,
					array_agg(edge_ads.id) as edge_ads_id,
					array_agg(t_variable_hierarchy.variable ORDER BY t_variable_hierarchy.variable) AS edges,
					10e-6 as add_check_treshold
				FROM @extschema@.t_variable_hierarchy
				JOIN @extschema@.t_variable 				AS node_var ON t_variable_hierarchy.variable_superior = node_var.id
				JOIN @extschema@.t_available_datasets 			AS node_ads ON node_var.id = node_ads.variable
				JOIN @extschema@.t_variable 				AS edge_var ON t_variable_hierarchy.variable = edge_var.id
				JOIN @extschema@.t_available_datasets 			AS edge_ads ON edge_var.id = edge_ads.variable
				LEFT JOIN @extschema@.c_sub_population_category		ON edge_var.sub_population_category = c_sub_population_category.id
				LEFT JOIN @extschema@.c_area_domain_category 		ON edge_var.area_domain_category = c_area_domain_category.id
				LEFT JOIN @extschema@.c_auxiliary_variable_category	ON edge_var.auxiliary_variable_category = c_auxiliary_variable_category.id
				WHERE
					node_ads.panel = edge_ads.panel
					AND
					(
							(node_ads.reference_year_set = edge_ads.reference_year_set
							AND node_var.auxiliary_variable_category IS NULL
							AND edge_var.auxiliary_variable_category IS NULL)
						OR
							(node_ads.reference_year_set IS NULL and edge_ads.reference_year_set IS NULL
							AND node_var.auxiliary_variable_category IS NOT NULL
							AND edge_var.auxiliary_variable_category IS NOT NULL)
					)
				GROUP BY 	node_ads.id, node_ads.panel, node_ads.reference_year_set, t_variable_hierarchy.variable_superior,
						c_sub_population_category.sub_population, c_area_domain_category.area_domain, c_auxiliary_variable_category.auxiliary_variable
				ORDER BY 	node_ads.id, node_ads.panel, node_ads.reference_year_set, t_variable_hierarchy.variable_superior,
						c_sub_population_category.sub_population, c_area_domain_category.area_domain, c_auxiliary_variable_category.auxiliary_variable
			),
			w_distinct_selection as (
				select distinct w_data.node_ads_id, w_data.edges, w_data.add_check_treshold
				from w_data join trans_table on
				(
					trans_table.variable_superior = w_data.node
					OR
					trans_table.variable = any (w_data.edges)
				)
			)
			INSERT INTO @extschema@.t_additivity_set_plot(available_datasets, edges, add_check_treshold, dbg_data)
			select
				node_ads_id, edges, add_check_treshold,
				json_build_object(
					'fn_refresh_t_additivity_set_plot', json_build_object(
						'TG_TABLE_NAME', TG_TABLE_NAME, 
						'TG_OP', TG_OP, 
						'trigerred_time', (clock_timestamp()::timestamp with time zone)::text
						)
					) as dbg_data
			from w_distinct_selection
			;
		ELSE
				RAISE EXCEPTION 'fn_refresh_t_additivity_set_plot -- trigger operation not known: %', TG_OP;
		END IF;
	ELSIF (TG_TABLE_NAME = 't_available_datasets') THEN
	IF (TG_OP = 'DELETE') THEN
		RAISE EXCEPTION 'fn_refresh_t_additivity_set_plot -- DELETE on table t_available_datasets not permited';
	ELSIF (TG_OP = 'INSERT' OR TG_OP = 'UPDATE') THEN
			with w_data as (
				SELECT 	node_ads.id as node_ads_id,
					array_agg(edge_ads.id) as edge_ads_id,
					array_agg(t_variable_hierarchy.variable ORDER BY t_variable_hierarchy.variable) AS edges,
					10e-6 as add_check_treshold
				FROM @extschema@.t_variable_hierarchy
				JOIN @extschema@.t_variable 				AS node_var ON t_variable_hierarchy.variable_superior = node_var.id
				JOIN @extschema@.t_available_datasets 			AS node_ads ON node_var.id = node_ads.variable
				JOIN @extschema@.t_variable 				AS edge_var ON t_variable_hierarchy.variable = edge_var.id
				JOIN @extschema@.t_available_datasets 			AS edge_ads ON edge_var.id = edge_ads.variable
				LEFT JOIN @extschema@.c_sub_population_category		ON edge_var.sub_population_category = c_sub_population_category.id
				LEFT JOIN @extschema@.c_area_domain_category 		ON edge_var.area_domain_category = c_area_domain_category.id
				LEFT JOIN @extschema@.c_auxiliary_variable_category	ON edge_var.auxiliary_variable_category = c_auxiliary_variable_category.id
				WHERE
					node_ads.panel = edge_ads.panel
					AND
					(
							(node_ads.reference_year_set = edge_ads.reference_year_set
							AND node_var.auxiliary_variable_category IS NULL
							AND edge_var.auxiliary_variable_category IS NULL)
						OR
							(node_ads.reference_year_set IS NULL and edge_ads.reference_year_set IS NULL
							AND node_var.auxiliary_variable_category IS NOT NULL
							AND edge_var.auxiliary_variable_category IS NOT NULL)
					)
				GROUP BY 	node_ads.id, node_ads.panel, node_ads.reference_year_set, t_variable_hierarchy.variable_superior,
						c_sub_population_category.sub_population, c_area_domain_category.area_domain, c_auxiliary_variable_category.auxiliary_variable
				ORDER BY 	node_ads.id, node_ads.panel, node_ads.reference_year_set, t_variable_hierarchy.variable_superior,
						c_sub_population_category.sub_population, c_area_domain_category.area_domain, c_auxiliary_variable_category.auxiliary_variable
			),
			w_distinct_selection as (
				select distinct w_data.node_ads_id, w_data.edges, w_data.add_check_treshold
				from w_data join trans_table on
				(
					trans_table.id = w_data.node_ads_id
					OR
					trans_table.id = any (w_data.edge_ads_id)
				)
			)
			INSERT INTO @extschema@.t_additivity_set_plot(available_datasets, edges, add_check_treshold, dbg_data)
			select
				node_ads_id, edges, add_check_treshold,
				json_build_object(
					'fn_refresh_t_additivity_set_plot', json_build_object(
						'TG_TABLE_NAME', TG_TABLE_NAME, 
						'TG_OP', TG_OP, 
						'trigerred_time', (clock_timestamp()::timestamp with time zone)::text
						)
					) as dbg_data
			from w_distinct_selection
			;
		ELSE
				RAISE EXCEPTION 'fn_refresh_t_additivity_set_plot -- trigger operation not known: %', TG_OP;
		END IF;
	ELSE
		RAISE EXCEPTION 'fn_refresh_t_additivity_set_plot -- % -- table name not known: %', TG_OP, TG_TABLE_NAME;
	END IF;
	RETURN NULL;
END;
$$;

--target_data.t_variable_hierarchy
drop trigger if exists trg__t_variable_hierarchy__refresh_t_additivity_set_plot__upd ON @extschema@.t_variable_hierarchy;
CREATE TRIGGER trg__t_variable_hierarchy__refresh_t_additivity_set_plot__upd
AFTER UPDATE ON @extschema@.t_variable_hierarchy
REFERENCING NEW TABLE AS trans_table
FOR EACH STATEMENT EXECUTE PROCEDURE @extschema@.fn_refresh_t_additivity_set_plot();

drop trigger if exists trg__t_variable_hierarchy__refresh_t_additivity_set_plot__ins ON @extschema@.t_variable_hierarchy;
CREATE TRIGGER trg__t_variable_hierarchy__refresh_t_additivity_set_plot__ins
AFTER INSERT ON @extschema@.t_variable_hierarchy
REFERENCING NEW TABLE AS trans_table
FOR EACH STATEMENT EXECUTE PROCEDURE @extschema@.fn_refresh_t_additivity_set_plot();

drop trigger if exists trg__t_variable_hierarchy__refresh_t_additivity_set_plot__del ON @extschema@.t_variable_hierarchy;
CREATE TRIGGER trg__t_variable_hierarchy__refresh_t_additivity_set_plot__del
AFTER DELETE ON @extschema@.t_variable_hierarchy
REFERENCING OLD TABLE AS trans_table
FOR EACH STATEMENT EXECUTE PROCEDURE @extschema@.fn_refresh_t_additivity_set_plot();

/*
delete from @extschema@.t_additivity_set_plot;
select * from @extschema@.t_additivity_set_plot;
select * from @extschema@.t_variable_hierarchy;
update @extschema@.t_variable_hierarchy set variable = 16 where id = 14;
insert into @extschema@.t_variable_hierarchy (id, variable, variable_superior) values (15, 1, 14);
delete from @extschema@.t_variable_hierarchy where id = 15;
*/

--target_data.t_available_datasets
drop trigger if exists trg__t_available_datasets__refresh_t_additivity_set_plot__upd ON @extschema@.t_available_datasets;
CREATE TRIGGER trg__t_available_datasets__refresh_t_additivity_set_plot__upd
AFTER UPDATE ON @extschema@.t_available_datasets
REFERENCING NEW TABLE AS trans_table
FOR EACH STATEMENT EXECUTE PROCEDURE @extschema@.fn_refresh_t_additivity_set_plot();

drop trigger if exists trg__t_available_datasets__refresh_t_additivity_set_plot__ins ON @extschema@.t_available_datasets;
CREATE TRIGGER trg__t_available_datasets__refresh_t_additivity_set_plot__ins
AFTER INSERT ON @extschema@.t_available_datasets
REFERENCING NEW TABLE AS trans_table
FOR EACH STATEMENT EXECUTE PROCEDURE @extschema@.fn_refresh_t_additivity_set_plot();

drop trigger if exists trg__t_available_datasets__refresh_t_additivity_set_plot__del ON @extschema@.t_available_datasets;
CREATE TRIGGER trg__t_available_datasets__refresh_t_additivity_set_plot__del
AFTER DELETE ON @extschema@.t_available_datasets
REFERENCING OLD TABLE AS trans_table
FOR EACH STATEMENT EXECUTE PROCEDURE @extschema@.fn_refresh_t_additivity_set_plot();

/*
delete from @extschema@.t_additivity_set_plot;
select * from @extschema@.t_additivity_set_plot;
select * from @extschema@.t_available_datasets;
update @extschema@.t_available_datasets set last_change = now() where id = 1;
insert into @extschema@.t_available_datasets (id, panel, reference_year_set, variable, last_change) values (50, 1, 2, 8, now());
delete from @extschema@.t_available_datasets where id = 50;
*/

--------------------------------------------------------------------------------------------------------------------------------

--drop function @extschema@.fn_launch_add_check() cascade;
CREATE OR REPLACE FUNCTION @extschema@.fn_launch_add_check() RETURNS TRIGGER AS $src$
DECLARE
		_var_sup int;
		_var_inf int[];
		_panel int;
		_refyearsets int;
		_treshold double precision;
		_errp json;
		_dbg_data jsonb;
BEGIN
	IF (TG_TABLE_NAME = 't_additivity_set_plot') THEN
		IF (TG_OP = 'DELETE') THEN
			RAISE EXCEPTION 'fn_launch_add_check -- DELETE on table fn_launch_add_check not permited';
		ELSIF (TG_OP = 'UPDATE') THEN
			RETURN NULL; -- result is ignored since this is an AFTER trigger
		ELSIF (TG_OP = 'INSERT') THEN
			------------------ _vars_add_set
			select
				t_available_datasets.variable 	into _var_sup
			from @extschema@.t_available_datasets
			where t_available_datasets.id = new.available_datasets;
			select
				new.edges			into _var_inf
			;
			------------------ _plots
			select
				t_available_datasets.panel	into _panel
			from @extschema@.t_available_datasets
			where t_available_datasets.id = new.available_datasets;
			------------------ _refyearsets
			select
				t_available_datasets.reference_year_set into _refyearsets
			from @extschema@.t_available_datasets
			where t_available_datasets.id = new.available_datasets;
			----------------------------------------------------------------------------------------------
			if array_prepend(_var_sup, _var_inf) is not null then
				_dbg_data = jsonb_build_object(
							'add_check_start', (clock_timestamp()::timestamp with time zone)::text
						);
				if (	select 
						(target_variable is not null) and (auxiliary_variable_category is null) as is_target
					from @extschema@.t_variable 
					where id = _var_sup) 
				then 
					with w_err as (
						select @extschema@.fn_add_plot_target_attr(_var_sup, _var_inf, _panel, _refyearsets, 1e-6, true) as fn_err
					)
					, w_err_t as (
						select (fn_err).* from w_err
					)
					, w_err_json as (
						select 
							json_build_object(
								'plot'				, plot,
								'reference_year_set'		, reference_year_set,
								'variable'			, variable,
								'ldsity'			, ldsity,
								'ldsity_sum'			, ldsity_sum,
								'variables_def'			, variables_def,
								'variables_found'		, variables_found,
								'diff'				, diff
							) as errj
						from w_err_t
					)
					select json_agg(errj) into _errp from w_err_json;
				else
					with w_err as (
						select @extschema@.fn_add_plot_aux_attr(_var_sup, _var_inf, _panel, 1e-6, true) as fn_err
					)
					, w_err_t as (
						select (fn_err).* from w_err
					)
					, w_err_json as (
						select 
							json_build_object(
								'plot'				, plot,
								'variable'			, variable,
								'ldsity'			, ldsity,
								'ldsity_sum'			, ldsity_sum,
								'variables_def'			, variables_def,
								'variables_found'		, variables_found,
								'diff'				, diff
							) as errj
						from w_err_t
					)
					select json_agg(errj) into _errp from w_err_json;
				end if;

				_dbg_data = jsonb_set(_dbg_data,
						'{add_check_stop}', to_jsonb((clock_timestamp()::timestamp with time zone)::text)
						);

				update @extschema@.t_additivity_set_plot 
				set dbg_data = jsonb_set(dbg_data, '{fn_launch_additivity_check}', _dbg_data) 
				where id = new.id;
				
				IF
					(json_array_length(_errp) > 0)
					THEN RAISE EXCEPTION 'fn_launch_add_check -- % -- % -- plot level local densities are not additive:
						variables in additivity set: sup: %, inf: % , checked panels: %, refyearsets: %,
						found err plots: 
						%', TG_TABLE_NAME, TG_OP, _var_sup, _var_inf, _panel, _refyearsets, 
						jsonb_pretty(_errp::jsonb);
				END IF;
			else
				RAISE WARNING 'fn_launch_add_check -- % -- % -- additivity check was skiped (no variables found in hierarchy):
						variables in additivity set: %, checked panels: %, refyearsets: %'
						, TG_TABLE_NAME, TG_OP, 
						_vars_add_set, _panel, _refyearsets;
			end if;
		ELSE
			RAISE EXCEPTION 'fn_launch_add_check -- trigger operation not known: %', TG_OP;
		END IF;
	ELSE
		RAISE EXCEPTION 'fn_launch_add_check -- % -- table name not known: %', TG_OP, TG_TABLE_NAME;
	END IF;
RETURN NULL; -- result is ignored since this is an AFTER trigger
END;
$src$ LANGUAGE plpgsql;

drop trigger if exists trg__additivity_set_plot__ins on @extschema@.t_additivity_set_plot;
CREATE CONSTRAINT TRIGGER trg__additivity_set_plot__ins
    AFTER INSERT ON @extschema@.t_additivity_set_plot
    DEFERRABLE INITIALLY DEFERRED
    FOR EACH ROW EXECUTE FUNCTION @extschema@.fn_launch_add_check();

drop trigger if exists trg__additivity_set_plot__upd on @extschema@.t_additivity_set_plot;
CREATE CONSTRAINT TRIGGER trg__additivity_set_plot__upd
    AFTER UPDATE ON @extschema@.t_additivity_set_plot
    DEFERRABLE INITIALLY DEFERRED
    FOR EACH ROW EXECUTE FUNCTION @extschema@.fn_launch_add_check();

drop trigger if exists trg__additivity_set_plot__del on @extschema@.t_additivity_set_plot;
CREATE CONSTRAINT TRIGGER trg__additivity_set_plot__del
    AFTER DELETE ON @extschema@.t_additivity_set_plot
    DEFERRABLE INITIALLY DEFERRED
    FOR EACH ROW EXECUTE FUNCTION @extschema@.fn_launch_add_check();

-- </function>
