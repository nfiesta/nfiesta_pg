--
-- Copyright 2017, 2023 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

DROP FUNCTION @extschema@.fn_get_area_sub_population_categories(integer, integer, integer);
-- <function name="fn_get_area_sub_population_categories" schema="extschema" src="functions/extschema/configuration/fn_get_area_sub_population_categories.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_get_area_sub_population_categories(integer, integer, integer)
--DROP FUNCTION @extschema@.fn_get_area_sub_population_categories(integer, integer, integer);
CREATE OR REPLACE FUNCTION @extschema@.fn_get_area_sub_population_categories(
		_target_variable integer, _area_or_sub_pop integer, _id integer)
RETURNS TABLE (
variable 	integer,
attype 		integer,
category 	integer,
label		varchar,
description	text,
label_en	varchar,
description_en	text
)
AS
$function$
DECLARE
BEGIN

IF _target_variable IS NULL THEN
	RAISE EXCEPTION 'Target variable is NULL!';
END IF;

IF _area_or_sub_pop IS NULL
THEN
	RAISE EXCEPTION 'Parameter area domain or sub_population is not choosen!';
END IF; 

IF _area_or_sub_pop NOT IN (100,200)
THEN
	RAISE EXCEPTION 'Parameter area domain or sub_population has to be either 100 (area_domain) or 200 (sub_population)!';
END IF; 

IF NOT EXISTS (SELECT id FROM @extschema@.c_target_variable WHERE id = _target_variable)
THEN
	RAISE EXCEPTION 'Specified target_variable (%) does not exist in table c_target_variable!', _target_variable;
END IF;

IF NOT EXISTS (
	SELECT t1.id
	FROM @extschema@.t_variable AS t1
	WHERE t1.target_variable = _target_variable)
THEN 
	RAISE EXCEPTION 'Specified target_variable(%) does not exist in the data (t_variable)!', _target_variable;
END IF;

CASE WHEN _area_or_sub_pop = 100
THEN
	IF _id IS NOT NULL AND NOT EXISTS (SELECT id FROM @extschema@.c_area_domain WHERE id = _id)
	THEN
		RAISE EXCEPTION 'Specified area_domain (%) does not exist in table c_area_domain!', _id;
	END IF;

	IF _id IS NOT NULL AND NOT EXISTS (
		SELECT t1.id
		FROM @extschema@.t_variable AS t1
		INNER JOIN @extschema@.c_area_domain_category AS t2
		ON t1.area_domain_category = t2.id
		WHERE t1.target_variable = _target_variable AND
			t2.area_domain = _id)
	THEN 
		RAISE EXCEPTION 'Specified combination of target_variable(%) and area_domain (%) does not exist in the data (t_variable)!', _target_variable, _id;
	END IF;

	RETURN QUERY EXECUTE
	'WITH RECURSIVE w_variables (target_variable, variable_superior, variable) AS (
		SELECT
			t1.target_variable,
			coalesce(t2.variable_superior,t1.id) AS variable_superior,
			coalesce(t2.variable, t1.id) AS variable
		FROM @extschema@.t_variable AS t1
		LEFT JOIN @extschema@.t_variable_hierarchy AS t2
		ON t2.variable = t1.id
		LEFT JOIN @extschema@.c_area_domain_category AS t3
		ON t1.area_domain_category = t3.id
		WHERE 	t1.target_variable = $1 AND
			CASE WHEN $2 IS NOT NULL THEN t3.area_domain = $2 ELSE t1.area_domain_category IS NULL END
		UNION ALL
		SELECT
			t1.target_variable,
			t2.variable_superior,
			t2.variable
		FROM w_variables AS t1
		LEFT JOIN @extschema@.t_variable_hierarchy AS t2
		ON t1.variable_superior = t2.variable
		WHERE t2.variable IS NOT NULL
	)
	, w_attr_types AS (
		SELECT
			t1.target_variable,
			t1.variable_superior, t1.variable, 
			t3.area_domain AS area_domain_sup, t5.area_domain, 
			t3.id AS id_cat_sup, t3.label AS category_sup,
			t5.id AS id_cat, t5.label AS category
		FROM w_variables AS t1
		INNER JOIN
			@extschema@.t_variable AS t2
		ON 
			t1.variable_superior = t2.id
		LEFT JOIN
			@extschema@.c_area_domain_category AS t3
		ON t2.area_domain_category = t3.id
		INNER JOIN
			@extschema@.t_variable AS t4
		ON 
			t1.variable = t4.id
		LEFT JOIN
			@extschema@.c_area_domain_category AS t5
		ON t4.area_domain_category = t5.id
	),
	w_union AS (
		SELECT	target_variable, variable_superior AS variable,  area_domain_sup AS area_domain
		FROM	w_attr_types
		UNION ALL
		SELECT	target_variable, variable,  area_domain
		FROM	w_attr_types
	)
	,w_distinct AS (
		SELECT DISTINCT target_variable, area_domain
		FROM w_union
	)
	SELECT
		t3.id AS variable, t2.area_domain AS type, t2.id AS category,
		t2.label, t2.description, t2.label_en, t2.description_en
	FROM
		w_distinct AS t1
	LEFT JOIN
		@extschema@.c_area_domain_category AS t2
	ON 	t1.area_domain = t2.area_domain
	INNER JOIN
		@extschema@.t_variable AS t3
	ON t1.target_variable = t3.target_variable AND
	CASE WHEN t2.id IS NOT NULL THEN t3.area_domain_category = t2.id
	ELSE t3.area_domain_category IS NULL END
	ORDER BY t3.id
	'
	USING _target_variable, _id;

WHEN _area_or_sub_pop = 200
THEN
	IF _id IS NOT NULL AND NOT EXISTS (SELECT id FROM @extschema@.c_sub_population WHERE id = _id)
	THEN
		RAISE EXCEPTION 'Specified sub_population (%) does not exist in table c_sub_population!', _id;
	END IF;

	IF _id IS NOT NULL AND NOT EXISTS (
		SELECT t1.id
		FROM @extschema@.t_variable AS t1
		INNER JOIN @extschema@.c_sub_population_category AS t2
		ON t1.sub_population_category = t2.id
		WHERE t1.target_variable = _target_variable AND
			t2.sub_population = _id)
	THEN 
		RAISE EXCEPTION 'Specified combination of target_variable(%) and sub_population (%) does not exist in the data (t_variable)!', _target_variable, _id;
	END IF;

	RETURN QUERY EXECUTE
	'WITH RECURSIVE w_variables (target_variable, variable_superior, variable) AS (
		SELECT
			t1.target_variable,
			coalesce(t2.variable_superior,t1.id) AS variable_superior,
			coalesce(t2.variable, t1.id) AS variable
		FROM @extschema@.t_variable AS t1
		LEFT JOIN @extschema@.t_variable_hierarchy AS t2
		ON t2.variable = t1.id
		LEFT JOIN @extschema@.c_sub_population_category AS t3
		ON t1.sub_population_category = t3.id
		WHERE 	t1.target_variable = $1 AND
			CASE WHEN $2 IS NOT NULL THEN t3.sub_population = $2 ELSE t1.sub_population_category IS NULL END
		UNION ALL
		SELECT
			t1.target_variable,
			t2.variable_superior,
			t2.variable
		FROM w_variables AS t1
		LEFT JOIN @extschema@.t_variable_hierarchy AS t2
		ON t1.variable_superior = t2.variable
		WHERE t2.variable IS NOT NULL
	)
	, w_attr_types AS (
		SELECT
			t1.target_variable,
			t1.variable_superior, t1.variable, 
			t3.sub_population AS sub_population_sup, t5.sub_population, 
			t3.id AS id_cat_sup, t3.label AS category_sup,
			t5.id AS id_cat, t5.label AS category
		FROM w_variables AS t1
		INNER JOIN
			@extschema@.t_variable AS t2
		ON 
			t1.variable_superior = t2.id
		LEFT JOIN
			@extschema@.c_sub_population_category AS t3
		ON t2.sub_population_category = t3.id
		INNER JOIN
			@extschema@.t_variable AS t4
		ON 
			t1.variable = t4.id
		LEFT JOIN
			@extschema@.c_sub_population_category AS t5
		ON t4.sub_population_category = t5.id
	),
	w_union AS (
		SELECT	target_variable, variable_superior AS variable,  sub_population_sup AS sub_population
		FROM	w_attr_types
		UNION ALL
		SELECT	target_variable, variable,  sub_population
		FROM	w_attr_types
	)
	,w_distinct AS (
		SELECT DISTINCT target_variable, sub_population
		FROM w_union
	)
	SELECT
		t3.id AS variable, t2.sub_population AS type, t2.id AS category,
		t2.label, t2.description, t2.label_en, t2.description_en
	FROM
		w_distinct AS t1
	LEFT JOIN
		@extschema@.c_sub_population_category AS t2
	ON 	t1.sub_population = t2.sub_population
	INNER JOIN
		@extschema@.t_variable AS t3
	ON t1.target_variable = t3.target_variable AND
	CASE WHEN t2.id IS NOT NULL THEN t3.sub_population_category = t2.id
	ELSE t3.sub_population_category IS NULL END
	ORDER BY t3.id'
	USING _target_variable, _id;
ELSE
	RAISE EXCEPTION 'Uknown attribute type!';
END CASE;
END;
$function$
LANGUAGE plpgsql
PARALLEL SAFE;

COMMENT ON FUNCTION @extschema@.fn_get_area_sub_population_categories(integer,integer,integer) IS 'Function returns table with all hierarchically superior variables and its complementary categories within area domain or sub_population. Input parameters are target_variable (id from table c_target_variable), area_or_sub_pop (100 for area_domain, 200 for sub_population) and id (id from table c_area_domain or id from table c_sub_population)';

-- </function>

-- <function name="fn_get_num_denom_variables" schema="extschema" src="functions/extschema/configuration/fn_get_num_denom_variables.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_get_num_denom_variables.sql(integer, integer)
--DROP FUNCTION @extschema@.fn_get_num_denom_variables.sql(integer, integer);
CREATE OR REPLACE FUNCTION @extschema@.fn_get_num_denom_variables(
		_num_var integer[], _denom_var integer[] DEFAULT NULL::int[])
RETURNS TABLE (
variable_numerator 	integer,
variable_denominator 	integer
)
AS
$function$
DECLARE
_denom_var4query	integer[];
BEGIN
	IF _num_var IS NULL
	THEN
		RAISE EXCEPTION 'Given array of numerator variables is NULL!';
	END IF;

	IF (SELECT count(*) FROM @extschema@.t_variable WHERE array[id] <@ _num_var) != array_length(_num_var,1)
	THEN
		RAISE EXCEPTION 'Not all of numerator variables are present in table t_variable.';
	END IF;

	IF (SELECT count(*) FROM @extschema@.t_variable WHERE array[id] <@ _denom_var) != array_length(_denom_var,1)
	THEN
		RAISE EXCEPTION 'Not all of denominator variables are present in table t_variable.';
	END IF;

	IF array_length(_num_var,1) < array_length(_denom_var,1)
	THEN
		RAISE EXCEPTION 'Given array of denominator variables must be shorter (or equal) then given array of numerator variables.';
	END IF;

	IF _denom_var IS NULL
	THEN _denom_var4query := _num_var;
	ELSE _denom_var4query := _denom_var;
	END IF;

	RETURN QUERY EXECUTE
	'
	WITH RECURSIVE w_var AS (
		SELECT 1 AS iter, variable_superior AS variable_start, variable_superior, variable
		FROM @extschema@.t_variable_hierarchy
		WHERE variable_superior = ANY($1)
		UNION ALL
		SELECT iter + 1 AS iter, t1.variable_start, t2.variable_superior, t2.variable
		FROM w_var AS t1
		INNER JOIN @extschema@.t_variable_hierarchy AS t2
		ON t1.variable = t2.variable_superior
		WHERE NOT ARRAY[t1.variable] <@ $1
	), w_all AS (
		SELECT distinct iter, t1.variable_start, t1.variable_superior, t1.variable, t3.label AS lab_start, t7.label AS lab_sup, t5.label AS lab_var
		FROM w_var AS t1
		INNER JOIN @extschema@.t_variable AS t2
		ON t1.variable_start = t2.id
		LEFT JOIN @extschema@.c_sub_population_category AS t3
		ON t2.sub_population_category = t3.id
		INNER JOIN @extschema@.t_variable AS t4
		ON t1.variable = t4.id
		LEFT JOIN @extschema@.c_sub_population_category AS t5
		ON t4.sub_population_category = t5.id
		INNER JOIN @extschema@.t_variable AS t6
		ON t1.variable_superior = t6.id
		LEFT JOIN @extschema@.c_sub_population_category AS t7
		ON t6.sub_population_category = t7.id
		WHERE variable = ANY($2)
		ORDER BY variable_start, variable
	), w_iter AS(
		SELECT 	variable_start, variable_superior, variable, lab_start, --lab_sup, 
			lab_var,
			iter, min(iter) OVER (partition by variable) AS min_iter
		FROM w_all
	), w_final AS (
		SELECT variable_start AS denom, variable AS num
		FROM w_iter WHERE iter = min_iter AND
			NOT ARRAY[variable] <@ $1
		UNION ALL
		SELECT t1.var AS denom, t1.var AS num
		FROM unnest($1) AS t1(var)
	)
	SELECT DISTINCT t1.num, CASE WHEN $3 IS NOT NULL THEN t1.denom ELSE NULL::int END AS denom --, t3.label AS lab_denom, t5.label AS lab_num
	FROM w_final AS t1
	INNER JOIN @extschema@.t_variable AS t2
	ON t1.denom = t2.id
	LEFT JOIN @extschema@.c_sub_population_category AS t3
	ON t2.sub_population_category = t3.id
	INNER JOIN @extschema@.t_variable AS t4
	ON t1.num = t4.id
	LEFT JOIN @extschema@.c_sub_population_category AS t5
	ON t4.sub_population_category = t5.id
	ORDER BY denom, t1.num;
	'
	USING _denom_var4query, _num_var, _denom_var;

END;
$function$
LANGUAGE plpgsql
PARALLEL SAFE;

COMMENT ON FUNCTION @extschema@.fn_get_num_denom_variables(integer[],integer[]) IS 'Function returns table with assigned denominator variables to numerator variables.';

-- </function>

-- <function name="fn_get_area_domains4target_variable" schema="extschema" src="functions/extschema/configuration/fn_get_area_domains4target_variable.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_get_area_domains4target_variable(integer, integer)
--DROP FUNCTION @extschema@.fn_get_area_domains4target_variable(integer, integer);
CREATE OR REPLACE FUNCTION @extschema@.fn_get_area_domains4target_variable(
		_target_variable integer, 
		_numerator_area_domain integer DEFAULT NULL::int)
RETURNS TABLE (
	id integer,
	label varchar,
	description text, 
	label_en varchar,
	description_en text
)
AS
$function$
BEGIN
IF _target_variable IS NULL THEN
	RAISE EXCEPTION 'Target variable is NULL!';
END IF;

IF NOT EXISTS (SELECT * FROM @extschema@.c_target_variable AS t1 WHERE t1.id = _target_variable)
THEN
	RAISE EXCEPTION 'Specified target_variable (%) does not exist in table c_target_variable!', _target_variable;
END IF;

IF _numerator_area_domain IS NOT NULL
THEN
	IF NOT EXISTS (SELECT * FROM @extschema@.c_area_domain AS t1 WHERE t1.id = _numerator_area_domain)
	THEN RAISE EXCEPTION 'Given area domain (%) does not exist in table c_area_domain!', _numerator_area_domain;
	END IF;
END IF;

RETURN QUERY EXECUTE
	'WITH RECURSIVE w_variables (target_variable, variable_superior, variable) AS (
			SELECT
				t2.target_variable,
				variable_superior,
				variable
			FROM @extschema@.t_variable_hierarchy AS t1
			LEFT JOIN @extschema@.t_variable AS t2
			ON t1.variable = t2.id
			LEFT JOIN @extschema@.c_area_domain_category AS t3
			ON t2.area_domain_category = t3.id
			WHERE 	t2.target_variable = $1 AND 		-- first used as a nominator and second as denominator
				-- this condition is applied when the target variable is selected a denominator
				-- and the user defined an area_domain for nominator
				-- the principle is to find a common sorting for both variables (nom and denom)
				CASE WHEN $2 IS NOT NULL THEN t3.area_domain = $2 ELSE true END
			UNION ALL
			SELECT
				t1.target_variable,
				t2.variable_superior,
				t2.variable
			FROM w_variables AS t1
			LEFT JOIN @extschema@.t_variable_hierarchy AS t2
			ON t1.variable_superior = t2.variable
			WHERE t2.variable IS NOT NULL
		)
		, w_attr_types AS (
			SELECT
				t1.target_variable,
				t1.variable_superior, t1.variable, 
				t3.area_domain AS area_domain_sup, t5.area_domain, 
				t3.id AS id_cat_sup, t3.label AS category_sup,
				t5.id AS id_cat, t5.label AS category
			FROM w_variables AS t1
			INNER JOIN
				@extschema@.t_variable AS t2
			ON 
				t1.variable_superior = t2.id
			LEFT JOIN
				@extschema@.c_area_domain_category AS t3
			ON t2.area_domain_category = t3.id
			INNER JOIN
				@extschema@.t_variable AS t4
			ON 
				t1.variable = t4.id
			LEFT JOIN
				@extschema@.c_area_domain_category AS t5
			ON t4.area_domain_category = t5.id
		),
		w_union AS (
			SELECT	target_variable, variable_superior AS variable,  area_domain_sup AS area_domain
			FROM	w_attr_types
			UNION ALL
			SELECT	target_variable, variable,  area_domain
			FROM	w_attr_types
		)
		,w_distinct AS (
			SELECT DISTINCT target_variable, area_domain
			FROM w_union
		)
		,w_final AS (
			SELECT 
				coalesce(t2.id, t3.id) AS id,
				coalesce(t2.label, t3.label) AS label,
				coalesce(t2.description, t3.description) AS description,
				coalesce(t2.label_en, t3.label_en) AS label_en,
				coalesce(t2.description_en, t3.description_en) AS description_en
			FROM w_distinct AS t1
			LEFT JOIN @extschema@.c_area_domain AS t2
			ON t1.area_domain = t2.id
			LEFT JOIN
				(SELECT
					0 AS id,
					''bez rozlišení'' AS label, 
					''Bez rozlišení.'' AS description,
					''altogether'' AS label_en,
					''Altogether.'' AS description_en
				) AS t3
			ON coalesce(t1.area_domain,0) = t3.id
		)
		SELECT id, label, description, label_en, description_en 
		FROM w_final
		ORDER BY id
		'
		USING _target_variable, _numerator_area_domain;
END;
$function$
LANGUAGE plpgsql
PARALLEL SAFE;

COMMENT ON FUNCTION @extschema@.fn_get_area_domains4target_variable(integer,integer) IS 'Function returns table with area domains constrained by the availability for a given target variable and optionally by a given area domain of numerator if the target variable comes in the meaning of the denominator.';

-- </function>

-- <function name="fn_get_sub_populations4target_variable" schema="extschema" src="functions/extschema/configuration/fn_get_sub_populations4target_variable.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_get_sub_populations4target_variable(integer, integer)
--DROP FUNCTION @extschema@.fn_get_sub_populations4target_variable(integer, integer);
CREATE OR REPLACE FUNCTION @extschema@.fn_get_sub_populations4target_variable(
		_target_variable integer, 
		_numerator_sub_population integer DEFAULT NULL::int)
RETURNS TABLE (
	id integer,
	label varchar,
	description text, 
	label_en varchar,
	description_en text
)
AS
$function$
BEGIN
IF _target_variable IS NULL THEN
	RAISE EXCEPTION 'Target variable is NULL!';
END IF;

IF NOT EXISTS (SELECT * FROM @extschema@.c_target_variable AS t1 WHERE t1.id = _target_variable)
THEN
	RAISE EXCEPTION 'Specified target_variable (%) does not exist in table c_target_variable!', _target_variable;
END IF;

IF _numerator_sub_population IS NOT NULL
THEN
	IF NOT EXISTS (SELECT * FROM @extschema@.c_sub_population AS t1 WHERE t1.id = _numerator_sub_population)
	THEN RAISE EXCEPTION 'Given area domain (%) does not exist in table c_sub_population!', _numerator_sub_population;
	END IF;
END IF;

RETURN QUERY EXECUTE
	'WITH RECURSIVE w_variables (target_variable, variable_superior, variable) AS (
			SELECT
				t2.target_variable,
				variable_superior,
				variable
			FROM @extschema@.t_variable_hierarchy AS t1
			LEFT JOIN @extschema@.t_variable AS t2
			ON t1.variable = t2.id
			LEFT JOIN @extschema@.c_sub_population_category AS t3
			ON t2.sub_population_category = t3.id
			WHERE 	t2.target_variable = $1 AND 		-- first used as a nominator and second as denominator
				-- this condition is applied when the target variable is selected a denominator
				-- and the user defined an sub_population for nominator
				-- the principle is to find a common sorting for both variables (nom and denom)
				CASE WHEN $2 IS NOT NULL THEN t3.sub_population = $2 ELSE true END
			UNION ALL
			SELECT
				t1.target_variable,
				t2.variable_superior,
				t2.variable
			FROM w_variables AS t1
			LEFT JOIN @extschema@.t_variable_hierarchy AS t2
			ON t1.variable_superior = t2.variable
			WHERE t2.variable IS NOT NULL
		)
		, w_attr_types AS (
			SELECT
				t1.target_variable,
				t1.variable_superior, t1.variable, 
				t3.sub_population AS sub_population_sup, t5.sub_population, 
				t3.id AS id_cat_sup, t3.label AS category_sup,
				t5.id AS id_cat, t5.label AS category
			FROM w_variables AS t1
			INNER JOIN
				@extschema@.t_variable AS t2
			ON 
				t1.variable_superior = t2.id
			LEFT JOIN
				@extschema@.c_sub_population_category AS t3
			ON t2.sub_population_category = t3.id
			INNER JOIN
				@extschema@.t_variable AS t4
			ON 
				t1.variable = t4.id
			LEFT JOIN
				@extschema@.c_sub_population_category AS t5
			ON t4.sub_population_category = t5.id
		),
		w_union AS (
			SELECT	target_variable, variable_superior AS variable,  sub_population_sup AS sub_population
			FROM	w_attr_types
			UNION ALL
			SELECT	target_variable, variable,  sub_population
			FROM	w_attr_types
		)
		,w_distinct AS (
			SELECT DISTINCT target_variable, sub_population
			FROM w_union
		)
		,w_final AS (
			SELECT 
				coalesce(t2.id, t3.id) AS id,
				coalesce(t2.label, t3.label) AS label,
				coalesce(t2.description, t3.description) AS description,
				coalesce(t2.label_en, t3.label_en) AS label_en,
				coalesce(t2.description_en, t3.description_en) AS description_en
			FROM w_distinct AS t1
			LEFT JOIN @extschema@.c_sub_population AS t2
			ON t1.sub_population = t2.id
			LEFT JOIN
				(SELECT
					0 AS id,
					''bez rozlišení'' AS label, 
					''Bez rozlišení.'' AS description,
					''altogether'' AS label_en,
					''Altogether.'' AS description_en
				) AS t3
			ON coalesce(t1.sub_population,0) = t3.id
		)
		SELECT id, label, description, label_en, description_en 
		FROM w_final
		'
		USING _target_variable, _numerator_sub_population;
END;
$function$
LANGUAGE plpgsql
PARALLEL SAFE;

COMMENT ON FUNCTION @extschema@.fn_get_sub_populations4target_variable(integer,integer) IS 'Function returns table with sub populations constrained by the availability for a given target variable and optionally by a given sub population of numerator if the target variable comes in the meaning of the denominator.';

-- </function>

-- <function name="fn_get_attribute_categories4target_variable" schema="extschema" src="functions/extschema/configuration/fn_get_attribute_categories4target_variable.sql">
--
-- Copyright 2017, 2023 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_get_attribute_categories4target_variable(integer, integer, integer, integer, integer, integer)
--DROP FUNCTION @extschema@.fn_get_attribute_categories4target_variable(integer, integer, integer, integer, integer, integer);
CREATE OR REPLACE FUNCTION @extschema@.fn_get_attribute_categories4target_variable(
	_numerator_target_variable integer,
	_numerator_area_domain integer DEFAULT NULL::int,
	_numerator_sub_population integer DEFAULT NULL::int,
	_denominator_target_variable integer DEFAULT NULL::int,
	_denominator_area_domain integer DEFAULT NULL::int,
	_denominator_sub_population integer DEFAULT NULL::int)
RETURNS TABLE (
	nominator_variable integer,
	denominator_variable integer, 
	label varchar,
	description text,
	label_en varchar, 
	description_en text
)
AS
$function$
BEGIN
IF _numerator_target_variable IS NULL THEN
	RAISE EXCEPTION 'Target variable for numerator is NULL!';
END IF;

IF NOT EXISTS (SELECT id FROM @extschema@.c_target_variable WHERE id = _numerator_target_variable)
THEN
	RAISE EXCEPTION 'Specified numerator target variable (%) does not exist in table c_target_variable!', _numerator_target_variable;
END IF;

IF _numerator_area_domain IS NOT NULL
THEN
	IF NOT EXISTS (SELECT * FROM @extschema@.c_area_domain WHERE id = _numerator_area_domain)
	THEN RAISE EXCEPTION 'Given numerator area domain (%) does not exist in table c_area_domain!', _numerator_area_domain;
	END IF;
END IF;

IF _numerator_sub_population IS NOT NULL
THEN
	IF NOT EXISTS (SELECT * FROM @extschema@.c_sub_population WHERE id = _numerator_sub_population)
	THEN RAISE EXCEPTION 'Given numerator sub population (%) does not exist in table c_sub_population!', _numerator_sub_population;
	END IF;
END IF;

IF _denominator_target_variable IS NOT NULL
THEN
	IF NOT EXISTS (SELECT * FROM @extschema@.c_target_variable WHERE id = _denominator_target_variable)
	THEN RAISE EXCEPTION 'Given denominator target variable (%) does not exist in table c_target_variable!', _denominator_targer_variable;
	END IF;
END IF;

IF _denominator_area_domain IS NOT NULL
THEN
	IF NOT EXISTS (SELECT * FROM @extschema@.c_area_domain WHERE id = _denominator_area_domain)
	THEN RAISE EXCEPTION 'Given denominator area domain (%) does not exist in table c_area_domain!', _denominator_area_domain;
	END IF;
END IF;

IF _denominator_sub_population IS NOT NULL
THEN
	IF NOT EXISTS (SELECT * FROM @extschema@.c_sub_population WHERE id = _denominator_sub_population)
	THEN RAISE EXCEPTION 'Given denominator sub population (%) does not exist in table c_sub_population!', _denominator_sub_population;
	END IF;
END IF;

RETURN QUERY
	WITH w_ad_num AS(
		SELECT t1.variable, t1.category AS adc, t1.label AS adc_label, t1.description AS adc_description, t1.label_en AS adc_label_en, t1.description_en AS adc_description_en
		FROM @extschema@.fn_get_area_sub_population_categories(_numerator_target_variable,100,_numerator_area_domain) AS t1
	), w_sp_num AS (
		SELECT t1.variable, t1.category AS spc, t1.label AS spc_label, t1.description AS spc_description, t1.label_en AS spc_label_en, t1.description_en AS spc_description_en
		FROM @extschema@.fn_get_area_sub_population_categories(_numerator_target_variable,200,_numerator_sub_population) AS t1
	), w_numerator_variables AS (
		SELECT 	t1.variable AS id,
			t1.adc, adc_label, adc_description, adc_label_en, adc_description_en, 
			t2.spc, spc_label, spc_description, spc_label_en, spc_description_en
		FROM w_ad_num AS t1
		INNER JOIN w_sp_num AS t2
		ON t1.variable = t2.variable
	), w_numerator_variables_agg AS (
		SELECT array_agg(id ORDER BY id) AS num_variables
		FROM w_numerator_variables
	), w_ad_denom AS(
		SELECT t1.variable, t1.category AS adc, t1.label AS adc_label, t1.description AS adc_description, t1.label_en AS adc_label_en, t1.description_en AS adc_description_en
		FROM @extschema@.fn_get_area_sub_population_categories(_denominator_target_variable,100,_denominator_area_domain) AS t1
	), w_sp_denom AS (
		SELECT t1.variable, t1.category AS spc, t1.label AS spc_label, t1.description AS spc_description, t1.label_en AS spc_label_en, t1.description_en AS spc_description_en
		FROM @extschema@.fn_get_area_sub_population_categories(_denominator_target_variable,200,_denominator_sub_population) AS t1
	), w_denominator_variables AS (
		SELECT	t1.variable AS id, 
			t1.adc, adc_label, adc_description, adc_label_en, adc_description_en, 
			t2.spc, spc_label, spc_description, spc_label_en, spc_description_en
		FROM w_ad_denom AS t1
		INNER JOIN w_sp_denom AS t2
		ON t1.variable = t2.variable
	), w_denominator_variables_agg AS (
		SELECT array_agg(id ORDER BY id) AS denom_variables
		FROM w_denominator_variables
	),
	w_num_denom AS (
		SELECT t3.variable_numerator, t3.variable_denominator
		FROM 	w_numerator_variables_agg AS t1,
			w_denominator_variables_agg AS t2,
			@extschema@.fn_get_num_denom_variables(t1.num_variables, t2.denom_variables) AS t3
	), w_final AS (
		SELECT 
			t1.variable_numerator,
			t1.variable_denominator,
			coalesce(nullif(concat_ws(' - ', t2.adc_label, t2.spc_label),''),'bez rozlišení')::varchar AS label_num, 
			coalesce(nullif(concat_ws(' - ', t3.adc_label, t3.spc_label),''),'bez rozlišení')::varchar AS label_denom,
			coalesce(nullif(concat_ws(' - ', t2.adc_description, t2.spc_description),''),'Bez rozlišení.')::text AS description_num, 
			coalesce(nullif(concat_ws(' - ', t3.adc_description, t3.spc_description),''),'Bez rozlišení.')::text AS description_denom,
			coalesce(nullif(concat_ws(' - ', t2.adc_label_en, t2.spc_label_en),''),'altogether')::varchar AS label_en_num, 
			coalesce(nullif(concat_ws(' - ', t3.adc_label_en, t3.spc_label_en),''),'altogether')::varchar AS label_en_denom,
			coalesce(nullif(concat_ws(' - ', t2.adc_description_en, t2.spc_description_en),''),'Altogether.')::text AS description_en_num, 
			coalesce(nullif(concat_ws(' - ', t3.adc_description_en, t3.spc_description_en),''),'Altogether.')::text AS description_en_denom
		FROM
			w_num_denom AS t1
		INNER JOIN 
			w_numerator_variables AS t2
		ON t1.variable_numerator = t2.id
		LEFT JOIN
			w_denominator_variables AS t3
		ON t1.variable_denominator = t3.id
	) 
	SELECT
		variable_numerator,
		variable_denominator,
		concat_ws(' / ', label_num, label_denom)::varchar AS label, 
		concat_ws(' / ', description_num, description_denom)::text AS description, 
		concat_ws(' / ', label_en_num, label_en_denom)::varchar AS label_en, 
		concat_ws(' / ', description_en_num, description_en_denom)::text AS description_en 
	FROM w_final
	ORDER BY variable_numerator, variable_denominator;

/*		@extschema@.t_variable AS t2
	ON t1.variable_numerator = t2.id
	LEFT JOIN @extschema@.c_area_domain_category AS t3
	ON t2.area_domain_category = t3.id
	LEFT JOIN @extschema@.c_sub_population_category AS t4
	ON t2.sub_population_category = t4.id
	INNER JOIN @extschema@.t_variable AS t5
	ON t1.variable_denominator = t5.id;	
	LEFT JOIN @extschema@.c_area_domain_category AS t6
	ON t5.area_domain_category = t6.id
	LEFT JOIN @extschema@.c_sub_population_category AS t7
	ON t5.sub_population_category = t7.id;
*/

END;
$function$
LANGUAGE plpgsql
PARALLEL SAFE;

COMMENT ON FUNCTION @extschema@.fn_get_attribute_categories4target_variable(integer,integer,integer,integer,integer,integer) IS 'Function returns table with attribute categories (area domain and sub population categories) for given target variable and area domain/sub population. It also solves the right combination of numerator and denominator categories.';

-- </function>

