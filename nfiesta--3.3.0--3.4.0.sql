--
-- Copyright 2017, 2023 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--


-- <function name="fn_etl_check_area_domains4etl" schema="extschema" src="functions/extschema/etl/fn_etl_check_area_domains4etl.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_check_area_domains4etl(integer, integer[], varchar, character varying, integer[])

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_check_area_domains4etl(integer, integer[], varchar, character varying, integer[]) CASCADE;

create or replace function @extschema@.fn_etl_check_area_domains4etl
(
	_id					integer,													-- ordering ID from source DB
	_area_domain		integer[],													-- array of area domains for set count of elements
	_label_en			varchar,													-- label of area domains for check
	_national_language	character varying(2) default 'en'::character varying(2),	-- for result ordering column	
	_etl_id				integer[] default null::integer[]							-- etl ids of area domains that had been etl-ed yet
)
returns table
(
	id				integer,
	etl_id			integer,
	label			character varying,
	description		text,
	label_en		character varying,
	description_en	text,
	atomic			boolean,
	check_label_en	boolean
)
as
$$
declare
		_check_count			integer;
		_set_ordering_column	text;
begin
		if _id is null
		then
			raise exception 'Error 01: fn_etl_check_area_domains4etl: Input argument _id must not be NULL!';
		end if; 
	
		if _area_domain is null
		then
			raise exception 'Error 02: fn_etl_check_area_domains4etl: Input argument _area_domain must not be NULL!';
		end if;
	
		if _label_en is null
		then
			raise exception 'Error 03: fn_etl_check_area_domains4etl: Input argument _label_en must not be NULL!';
		end if;

		if _national_language is null
		then
			raise exception 'Error 04: fn_etl_check_area_domains4etl: Input argument _national_language must not be NULL!';
		end if;
		---------------------------------------------------
		with
		w1 as	(
				select cad.* from @extschema@.c_area_domain as cad
				where cad.id not in (select unnest(_etl_id))
				and array_length(string_to_array(cad.label_en,';'),1) = array_length(_area_domain,1)
				)
		,w2 as	(
				select
						w1.*,
						@extschema@.fn_etl_array_compare
							(
							string_to_array(replace(lower(w1.label_en),' ',''),';'),
							string_to_array(replace(lower(_label_en),' ',''),';')					
							) as check
				from w1
				)
		select count(w2.*) from w2 where w2.check = true
		into _check_count;
		---------------------------------------------------
		if _check_count > 1
		then
			raise exception 'Error 05: fn_etl_check_area_domains4etl: For input argument _label_en exist more than one record in c_area_domain table!';
		end if;
		---------------------------------------------------
		if _national_language = 'en'
		then
			_set_ordering_column := 'w2.label_en';		
		else
			_set_ordering_column := 'w2.label';
		end if;
		---------------------------------------------------
		if _check_count = 1
		then
			return query execute
			'
			with
			w1 as	(
					select cad.* from @extschema@.c_area_domain as cad
					where cad.id not in (select unnest($4))
					and array_length(string_to_array(cad.label_en,'';''),1) = array_length($2,1)
					)
			,w2 as	(
					select
							w1.*,
							@extschema@.fn_etl_array_compare
								(
								string_to_array(replace(lower(w1.label_en),'' '',''''),'';''),
								string_to_array(replace(lower($3),'' '',''''),'';'')					
								) as check
					from w1
					)
			select
					$1 as id,
					w2.id as etl_id,
					w2.label,
					w2.description,
					w2.label_en,
					w2.description_en,
					w2.atomic,
					w2.check
			from
					w2 where w2.check = true
			'
			using _id, _area_domain, _label_en, _etl_id;		
		else
			return query execute
			'
			with
			w1 as	(
					select cad.* from @extschema@.c_area_domain as cad
					where cad.id not in (select unnest($4))
					and array_length(string_to_array(cad.label_en,'';''),1) = array_length($2,1)
					)
			,w2 as	(
					select
							w1.*,
							@extschema@.fn_etl_array_compare
								(
								string_to_array(replace(lower(w1.label_en),'' '',''''),'';''),
								string_to_array(replace(lower($3),'' '',''''),'';'')					
								) as check
					from w1
					)
			select
					$1 as id,
					w2.id as etl_id,
					w2.label,
					w2.description,
					w2.label_en,
					w2.description_en,
					w2.atomic,
					w2.check
			from
					w2 order by '|| _set_ordering_column ||'
			'
			using _id, _area_domain, _label_en, _etl_id;
		end if;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_check_area_domains4etl(integer, integer[], varchar, character varying, integer[]) is
'The function returns one record of area domain if input label of area domain was paired in target DB or the function returns record(s) of area domains that are possible to pair in target DB.';

grant execute on function @extschema@.fn_etl_check_area_domains4etl(integer, integer[], varchar, character varying, integer[]) to public;
-- </function>



-- <function name="fn_etl_check_sub_populations4etl" schema="extschema" src="functions/extschema/etl/fn_etl_check_sub_populations4etl.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_check_sub_populations4etl(integer, integer[], varchar, character varying, integer[])

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_check_sub_populations4etl(integer, integer[], varchar, character varying, integer[]) CASCADE;

create or replace function @extschema@.fn_etl_check_sub_populations4etl
(
	_id					integer,													-- ordering ID from source DB
	_sub_population		integer[],													-- array of area domains for set count of elements
	_label_en			varchar,													-- label of area domains for check
	_national_language	character varying(2) default 'en'::character varying(2),	-- for result ordering column	
	_etl_id				integer[] default null::integer[]							-- etl ids of area domains that had been etl-ed yet
)
returns table
(
	id				integer,
	etl_id			integer,
	label			character varying,
	description		text,
	label_en		character varying,
	description_en	text,
	atomic			boolean,
	check_label_en	boolean
)
as
$$
declare
		_check_count			integer;
		_set_ordering_column	text;
begin
		if _id is null
		then
			raise exception 'Error 01: fn_etl_check_sub_populations4etl: Input argument _id must not be NULL!';
		end if; 
	
		if _sub_population is null
		then
			raise exception 'Error 02: fn_etl_check_sub_populations4etl: Input argument _sub_population must not be NULL!';
		end if;
	
		if _label_en is null
		then
			raise exception 'Error 03: fn_etl_check_sub_populations4etl: Input argument _label_en must not be NULL!';
		end if;

		if _national_language is null
		then
			raise exception 'Error 04: fn_etl_check_sub_populations4etl: Input argument _national_language must not be NULL!';
		end if;
		---------------------------------------------------
		with
		w1 as	(
				select csp.* from @extschema@.c_sub_population as csp
				where csp.id not in (select unnest(_etl_id))
				and array_length(string_to_array(csp.label_en,';'),1) = array_length(_sub_population,1)
				)
		,w2 as	(
				select
						w1.*,
						@extschema@.fn_etl_array_compare
							(
							string_to_array(replace(lower(w1.label_en),' ',''),';'),
							string_to_array(replace(lower(_label_en),' ',''),';')					
							) as check
				from w1
				)
		select count(w2.*) from w2 where w2.check = true
		into _check_count;
		---------------------------------------------------
		if _check_count > 1
		then
			raise exception 'Error 05: fn_etl_check_sub_populations4etl: For input argument _label_en exist more than one record in c_sub_population table!';
		end if;
		---------------------------------------------------
		if _national_language = 'en'
		then
			_set_ordering_column := 'w2.label_en';		
		else
			_set_ordering_column := 'w2.label';
		end if;
		---------------------------------------------------
		if _check_count = 1
		then
			return query execute
			'
			with
			w1 as	(
					select csp.* from @extschema@.c_sub_population as csp
					where csp.id not in (select unnest($4))
					and array_length(string_to_array(csp.label_en,'';''),1) = array_length($2,1)
					)
			,w2 as	(
					select
							w1.*,
							@extschema@.fn_etl_array_compare
								(
								string_to_array(replace(lower(w1.label_en),'' '',''''),'';''),
								string_to_array(replace(lower($3),'' '',''''),'';'')					
								) as check
					from w1
					)
			select
					$1 as id,
					w2.id as etl_id,
					w2.label,
					w2.description,
					w2.label_en,
					w2.description_en,
					w2.atomic,
					w2.check
			from
					w2 where w2.check = true
			'
			using _id, _sub_population, _label_en, _etl_id;		
		else
			return query execute
			'
			with
			w1 as	(
					select csp.* from @extschema@.c_sub_population as csp
					where csp.id not in (select unnest($4))
					and array_length(string_to_array(csp.label_en,'';''),1) = array_length($2,1)
					)
			,w2 as	(
					select
							w1.*,
							@extschema@.fn_etl_array_compare
								(
								string_to_array(replace(lower(w1.label_en),'' '',''''),'';''),
								string_to_array(replace(lower($3),'' '',''''),'';'')					
								) as check
					from w1
					)
			select
					$1 as id,
					w2.id as etl_id,
					w2.label,
					w2.description,
					w2.label_en,
					w2.description_en,
					w2.atomic,
					w2.check
			from
					w2 order by '|| _set_ordering_column ||'
			'
			using _id, _sub_population, _label_en, _etl_id;
		end if;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_check_sub_populations4etl(integer, integer[], varchar, character varying, integer[]) is
'The function returns one record of sub population if input label of sub population was paired in target DB or the function returns record(s) of sub populations that are possible to pair in target DB.';

grant execute on function @extschema@.fn_etl_check_sub_populations4etl(integer, integer[], varchar, character varying, integer[]) to public;
-- </function>



-- <function name="fn_etl_check_area_domains4etl_json" schema="extschema" src="functions/extschema/etl/fn_etl_check_area_domains4etl_json.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_check_area_domains4etl_json(json, integer[])

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_check_area_domains4etl_json(json, integer[]) CASCADE;

create or replace function @extschema@.fn_etl_check_area_domains4etl_json
(
	_area_domains		json,
	_etl_id				integer[] default null::integer[]
)
returns json
as
$$
declare
		_array_id						integer[];
		_check_etl_id					integer;
		_res_id_transfer				integer[];
		_res_id_manually				integer[];
		_res_id_pair					integer[];
		_area_domain_i					integer[];
		_label_en_type_i				varchar;
		_area_domain_etl_id				integer[];
		_area_domain_check				boolean[];
		_array_category_label_en_source	text[];
		_count_categories_target		integer;
		_do_action_in_loop				integer;
		_id_comulation					integer[];
		_etl_id_i_target_array			integer[];
		_res_loop_iii					integer;
		_check_manually_pair			integer;
		_check_manually_transfer		integer;
		_check_pair_transfer			integer;
		_res							json;

		_area_domain_ii					integer[];
		_label_en_type_ii				varchar;
		_area_domain_etl_id_ii			integer[];
		_area_domain_check_ii			boolean[];
		_res_id_manually_final			integer[];
begin
		if _area_domains is null
		then
			raise exception 'Error 01: fn_etl_check_area_domains4etl_json: Input argument _area_domains must not be NULL!';
		end if;

		with
		w1 as	(
				select _area_domains as res
				)
		,w2 as	(
				select json_array_elements(w1.res) as res from w1
				)
		,w3 as	(
				select (res->>'id')::integer as id from	w2
				)
		select array_agg(w3.id order by w3.id) from	w3
		into _array_id;
	
		select count(t.*) from
		(
		select cad.id from @extschema@.c_area_domain as cad except -- EXCEPT input ETL ids with ids in c_area_domain_table
		select unnest(_etl_id)
		) as t
		into _check_etl_id;
	
		if _check_etl_id = 0 -- if IDs in input argument _etl_id removed all IDs from c_area_domain table => then auto transfer
		then
			with
			w1 as	(select unnest(_array_id) as id, true as auto_transfer, false as auto_pair)
			select json_agg(json_build_object('id',w1.id,'auto_transfer',w1.auto_transfer,'auto_pair',w1.auto_pair)) from w1
			into _res;
		else
			_res_id_transfer := array[0];
			_res_id_manually := array[0];
			_res_id_pair := array[0];

			for i in 1..array_length(_array_id,1) -- The first cycle over TYPEs
			loop
				-----------------------------------------------------
				with
				w1 as	(
						select _area_domains as res
						)
				,w2 as	(
						select json_array_elements(w1.res) as res from w1
						)
				,w3 as	(
						select
								(res->>'id')::integer as id,
								replace(replace((res->>'area_domain')::varchar,'[',''),']','') as area_domain,
								(res->>'label_en_type')::varchar as label_en_type
						from
								w2
						)
				,w4 as	(
						select
								w3.id,
								(string_to_array(w3.area_domain,','))::integer[] as area_domain,
								w3.label_en_type
						from
								w3
						)
				select
						w4.area_domain,
						w4.label_en_type
				from
						w4 where w4.id = _array_id[i]
				into
						_area_domain_i,
						_label_en_type_i;
				-----------------------------------------------------
				with
				w1 as	(
						select * from @extschema@.fn_etl_check_area_domains4etl
							(
							_array_id[i],
							_area_domain_i,
							_label_en_type_i,
							'en'::varchar,
							_etl_id
							)
						)
				select
						array_agg(w1.etl_id order by w1.etl_id) as ad_etl_id, -- ID from c_area_domain table
						array_agg(w1.check_label_en order by w1.etl_id) as ad_check
				from
						w1
				into
						_area_domain_etl_id,
						_area_domain_check;
				-----------------------------------------------------					
				if _area_domain_etl_id is null  -- => auto transfer
				then
					_res_id_transfer := _res_id_transfer || _array_id[i];
				else
					if array_length(_area_domain_etl_id,1) = 1 and _area_domain_check[1] = true -- maybe to pair if all categories will be paired
					then
						-- area domain source and area domain target labels are equal
						-- => check categories
						with
						w1 as	(
								select _area_domains as res
								)
						,w2 as	(
								select json_array_elements(w1.res) as res from w1
								)
						,w3 as	(
								select
										(res->>'id')::integer as id,
										replace(replace((res->>'area_domain')::varchar,'[',''),']','') as area_domain,
										(res->>'label_en_type')::varchar as label_en_type,
										res->'category' as category_json
								from
										w2
								)
						,w4 as	(
								select
										w3.id,
										(string_to_array(w3.area_domain,','))::integer[] as area_domain,
										w3.label_en_type,
										w3.category_json
								from
										w3 where w3.id = _array_id[i]
								)
						,w5 as	(
								select
										w4.id,
										w4.area_domain,
										w4.label_en_type,
										json_array_elements(w4.category_json) as category_json
								from
										w4
								)
						,w6 as	(
								select category_json->>'label_en_category' as category_label_en from w5
								)
						select array_agg(w6.category_label_en) from w6
						into _array_category_label_en_source;
					
						select count(cadc.*) from @extschema@.c_area_domain_category as cadc
						where cadc.area_domain = _area_domain_etl_id[1]
						into _count_categories_target;
					
						if array_length(_array_category_label_en_source,1) is distinct from _count_categories_target
						then
							-- info: Source and target labels of area domain type are equal, but count of their categories are not equal!
							-- => manually pair
							_res_loop_iii := 0; -- <= loop over categories is not needed !!!
						else
							_do_action_in_loop := 1;
							_id_comulation := array[0];

							for iii in 1..array_length(_array_category_label_en_source,1)
							loop						
								if _do_action_in_loop = 1
								then
									select array_agg(cadc.id) from @extschema@.c_area_domain_category as cadc
									where cadc.area_domain = _area_domain_etl_id[1]
									and cadc.id not in (select unnest(_id_comulation))
									and @extschema@.fn_etl_array_compare
										(
										string_to_array(replace(lower(cadc.label_en),' ',''),';'),
										string_to_array(replace(lower(_array_category_label_en_source[iii]),' ',''),';')					
										) = true
									into _etl_id_i_target_array;
									-- founded 1 element in array => ok => auto pair
									-- founded 2 or more elements in array => error
									-- founded 0 => null => ko => manualy pair
								
									if _etl_id_i_target_array is null
									then
										_do_action_in_loop := 0; -- stop action in loop
										_res_loop_iii := 0;
									else
										if array_length(_etl_id_i_target_array,1) is distinct from 1
										then
											raise exception 'Error 02: fn_etl_check_area_domains4etl_json: For area domain type exists two or more equal categories!';
										else
											_id_comulation := _id_comulation || _etl_id_i_target_array[1];
											_res_loop_iii := 1;
										end if;
									end if;
								end if;						
							end loop;
						end if;							

						if _res_loop_iii = 0
						then
							_res_id_manually := _res_id_manually || _array_id[i];
						else
							_res_id_pair := _res_id_pair || _array_id[i];
							_etl_id := _etl_id || _area_domain_etl_id[1];		
						end if;	
					else
						-- => manualy pair
						_res_id_manually := _res_id_manually || _array_id[i];
					end if;
				end if;
				-----------------------------------------------------
			end loop;

			if	_res_id_pair is distinct from array[0] -- exists one or more types for auto pairing
				and
				_res_id_manually is distinct from array[0] -- exists one or more types for manualy pairing that can move to auto transfer
			then
				--_res_id_pair := array_remove(_res_id_pair,0);
				--_res_id_manually := array_remove(_res_id_manually,0);

				_res_id_manually_final := _res_id_manually;

				for ii in 1..array_length(_res_id_manually,1) -- the second cycle over types for manualy pairing
				loop
					if _res_id_manually[ii] is distinct from 0
					then
						-----------------------------------------------------
						with
						w1 as	(
								select _area_domains as res
								)
						,w2 as	(
								select json_array_elements(w1.res) as res from w1
								)
						,w3 as	(
								select
										(res->>'id')::integer as id,
										replace(replace((res->>'area_domain')::varchar,'[',''),']','') as area_domain,
										(res->>'label_en_type')::varchar as label_en_type
								from
										w2
								)
						,w4 as	(
								select
										w3.id,
										(string_to_array(w3.area_domain,','))::integer[] as area_domain,
										w3.label_en_type
								from
										w3
								)
						select
								w4.area_domain,
								w4.label_en_type
						from
								w4 where w4.id = _res_id_manually[ii] --_array_id[i]
						into
								_area_domain_ii,
								_label_en_type_ii;
						-----------------------------------------------------
						with
						w1 as	(
								select * from @extschema@.fn_etl_check_area_domains4etl
									(
									_res_id_manually[ii], --_array_id[ii],
									_area_domain_ii,
									_label_en_type_ii,
									'en'::varchar,
									_etl_id -- this array is supplemented by auto pairing IDs
									)
								)
						select
								array_agg(w1.etl_id order by w1.etl_id) as ad_etl_id, -- ID from c_area_domain table
								array_agg(w1.check_label_en order by w1.etl_id) as ad_check
						from
								w1
						into
								_area_domain_etl_id_ii,
								_area_domain_check_ii;
						-----------------------------------------------------
						if _area_domain_etl_id_ii is null  -- => auto transfer
						then
							_res_id_transfer := _res_id_transfer || _res_id_manually[ii]; -- manually is moved to auto transfer
							_res_id_manually_final := array_remove(_res_id_manually_final,_res_id_manually[ii]); -- id of area domain type is removed from manually pairing
						else
							-- stay in manualy pairing
							-- exist different types with equal count of categories
						end if;																
					end if;
				end loop;
			end if;

			
			-- check MANUALLY and PAIR
			with
			w1 as	(select unnest(_res_id_manually_final) as id_manually)
			,w2 as	(select unnest(_res_id_pair) as id_pair)	
			,w3 as	(select w1.* from w1 where w1.id_manually is distinct from 0)
			,w4 as	(select w2.* from w2 where w2.id_pair is distinct from 0)
			,w5 as	(select count(w3.*) as res from w3 where w3.id_manually in (select w4.id_pair from w4))
			,w6 as	(select count(w4.*) as res from w4 where w4.id_pair in (select w3.id_manually from w3))
			select sum(t.res) from (select w5.res from w5 union all	select w6.res from w6) as t
			into _check_manually_pair;

			if _check_manually_pair is distinct from 0
			then
				raise exception 'Error 03: fn_etl_check_area_domains4etl_json: There must not exist situation auto pair and manually pair for the area domain type at the same time!';
			end if;

			-- check MANUALLY and TRANFER
			with
			w1 as	(select unnest(_res_id_manually_final) as id_manually)
			,w2 as	(select unnest(_res_id_transfer) as id_transfer)	
			,w3 as	(select w1.* from w1 where w1.id_manually is distinct from 0)
			,w4 as	(select w2.* from w2 where w2.id_transfer is distinct from 0)
			,w5 as	(select count(w3.*) as res from w3 where w3.id_manually in (select w4.id_transfer from w4))
			,w6 as	(select count(w4.*) as res from w4 where w4.id_transfer in (select w3.id_manually from w3))
			select sum(t.res) from (select w5.res from w5 union all	select w6.res from w6) as t
			into _check_manually_transfer;

			if _check_manually_transfer is distinct from 0
			then
				raise exception 'Error 04: fn_etl_check_area_domains4etl_json: There must not exist situation auto transfer and manually pair for the area domain type at the same time!';
			end if;	

			-- check PAIR and TRANFER
			with
			w1 as	(select unnest(_res_id_pair) as id_pair)
			,w2 as	(select unnest(_res_id_transfer) as id_transfer)	
			,w3 as	(select w1.* from w1 where w1.id_pair is distinct from 0)
			,w4 as	(select w2.* from w2 where w2.id_transfer is distinct from 0)
			,w5 as	(select count(w3.*) as res from w3 where w3.id_pair in (select w4.id_transfer from w4))
			,w6 as	(select count(w4.*) as res from w4 where w4.id_transfer in (select w3.id_pair from w3))
			select sum(t.res) from (select w5.res from w5 union all	select w6.res from w6) as t
			into _check_pair_transfer;

			if _check_pair_transfer is distinct from 0
			then
				raise exception 'Error 05: fn_etl_check_area_domains4etl_json: There must not exist situation auto transfer and auto pair for the area domain type at the same time!';
			end if;					

			with
			w1 as	(select unnest(_array_id) as id)
			,w2 as	(select unnest(_res_id_pair) as id_pair)
			,w3 as	(select unnest(_res_id_transfer) as id_transfer)
			,w4 as	(select w2.* from w2 where w2.id_pair is distinct from 0)
			,w5 as	(select w3.* from w3 where w3.id_transfer is distinct from 0)
			,w6 as	(
					select
							w1.id,
							case when w5.id_transfer is null then false else true end as auto_transfer,
							case when w4.id_pair is null then false else true end as auto_pair
					from
							w1
							left join w5 on w1.id = w5.id_transfer
							left join w4 on w1.id = w4.id_pair
					order
							by w1.id
					)
			select json_agg(json_build_object('id',w6.id,'auto_transfer',w6.auto_transfer,'auto_pair',w6.auto_pair)) from w6
			into _res;

		end if;
	
		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_check_area_domains4etl_json(json, integer[]) is
'The function returns one record of area domain if input label of area domain was paired in target DB or the function returns record(s) of area domains that are possible to pair in target DB. Output is in JSON format.';

grant execute on function @extschema@.fn_etl_check_area_domains4etl_json(json, integer[]) to public;
-- </function>



-- <function name="fn_etl_check_sub_populations4etl_json" schema="extschema" src="functions/extschema/etl/fn_etl_check_sub_populations4etl_json.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_check_sub_populations4etl_json(json, integer[])

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_check_sub_populations4etl_json(json, integer[]) CASCADE;

create or replace function @extschema@.fn_etl_check_sub_populations4etl_json
(
	_sub_populations	json,
	_etl_id				integer[] default null::integer[]
)
returns json
as
$$
declare
		_array_id						integer[];
		_check_etl_id					integer;
		_res_id_transfer				integer[];
		_res_id_manually				integer[];
		_res_id_pair					integer[];
		_sub_population_i				integer[];
		_label_en_type_i				varchar;
		_sub_population_etl_id			integer[];
		_sub_population_check			boolean[];
		_array_category_label_en_source	text[];
		_count_categories_target		integer;
		_do_action_in_loop				integer;
		_id_comulation					integer[];
		_etl_id_i_target_array			integer[];
		_res_loop_iii					integer;
		_check_manually_pair			integer;
		_check_manually_transfer		integer;
		_check_pair_transfer			integer;
		_res							json;

		_sub_population_ii				integer[];
		_label_en_type_ii				varchar;
		_sub_population_etl_id_ii		integer[];
		_sub_population_check_ii		boolean[];
		_res_id_manually_final			integer[];
begin
		if _sub_populations is null
		then
			raise exception 'Error 01: fn_etl_check_sub_populations4etl_json: Input argument _sub_populations must not be NULL!';
		end if;

		with
		w1 as	(
				select _sub_populations as res
				)
		,w2 as	(
				select json_array_elements(w1.res) as res from w1
				)
		,w3 as	(
				select (res->>'id')::integer as id from	w2
				)
		select array_agg(w3.id order by w3.id) from	w3
		into _array_id;
	
		select count(t.*) from
		(
		select csp.id from @extschema@.c_sub_population as csp except -- EXCEPT input ETL ids with ids in c_sub_population_table
		select unnest(_etl_id)
		) as t
		into _check_etl_id;
	
		if _check_etl_id = 0 -- if IDs in input argument _etl_id removed all IDs from c_sub_population table => then auto transfer
		then
			with
			w1 as	(select unnest(_array_id) as id, true as auto_transfer, false as auto_pair)
			select json_agg(json_build_object('id',w1.id,'auto_transfer',w1.auto_transfer,'auto_pair',w1.auto_pair)) from w1
			into _res;
		else
			_res_id_transfer := array[0];
			_res_id_manually := array[0];
			_res_id_pair := array[0];

			for i in 1..array_length(_array_id,1) -- The first cycle over TYPEs
			loop
				-----------------------------------------------------
				with
				w1 as	(
						select _sub_populations as res
						)
				,w2 as	(
						select json_array_elements(w1.res) as res from w1
						)
				,w3 as	(
						select
								(res->>'id')::integer as id,
								replace(replace((res->>'sub_population')::varchar,'[',''),']','') as sub_population,
								(res->>'label_en_type')::varchar as label_en_type
						from
								w2
						)
				,w4 as	(
						select
								w3.id,
								(string_to_array(w3.sub_population,','))::integer[] as sub_population,
								w3.label_en_type
						from
								w3
						)
				select
						w4.sub_population,
						w4.label_en_type
				from
						w4 where w4.id = _array_id[i]
				into
						_sub_population_i,
						_label_en_type_i;
				-----------------------------------------------------
				with
				w1 as	(
						select * from @extschema@.fn_etl_check_sub_populations4etl
							(
							_array_id[i],
							_sub_population_i,
							_label_en_type_i,
							'en'::varchar,
							_etl_id
							)
						)
				select
						array_agg(w1.etl_id order by w1.etl_id) as sp_etl_id, -- ID from c_sub_population table
						array_agg(w1.check_label_en order by w1.etl_id) as sp_check
				from
						w1
				into
						_sub_population_etl_id,
						_sub_population_check;
				-----------------------------------------------------					
				if _sub_population_etl_id is null  -- => auto transfer
				then
					_res_id_transfer := _res_id_transfer || _array_id[i];
				else
					if array_length(_sub_population_etl_id,1) = 1 and _sub_population_check[1] = true -- maybe to pair if all categories will be paired
					then
						-- sub population domain source and sub population target labels are equal
						-- => check categories
						with
						w1 as	(
								select _sub_populations as res
								)
						,w2 as	(
								select json_array_elements(w1.res) as res from w1
								)
						,w3 as	(
								select
										(res->>'id')::integer as id,
										replace(replace((res->>'sub_population')::varchar,'[',''),']','') as sub_population,
										(res->>'label_en_type')::varchar as label_en_type,
										res->'category' as category_json
								from
										w2
								)
						,w4 as	(
								select
										w3.id,
										(string_to_array(w3.sub_population,','))::integer[] as sub_population,
										w3.label_en_type,
										w3.category_json
								from
										w3 where w3.id = _array_id[i]
								)
						,w5 as	(
								select
										w4.id,
										w4.sub_population,
										w4.label_en_type,
										json_array_elements(w4.category_json) as category_json
								from
										w4
								)
						,w6 as	(
								select category_json->>'label_en_category' as category_label_en from w5
								)
						select array_agg(w6.category_label_en) from w6
						into _array_category_label_en_source;
					
						select count(cspc.*) from @extschema@.c_sub_population_category as cspc
						where cspc.sub_population = _sub_population_etl_id[1]
						into _count_categories_target;
					
						if array_length(_array_category_label_en_source,1) is distinct from _count_categories_target
						then
							-- info: Source and target labels of sub population type are equal, but count of their categories are not equal!
							-- => manually pair
							_res_loop_iii := 0; -- <= loop over categories is not needed !!!
						else
							_do_action_in_loop := 1;
							_id_comulation := array[0];

							for iii in 1..array_length(_array_category_label_en_source,1)
							loop						
								if _do_action_in_loop = 1
								then
									select array_agg(cspc.id) from @extschema@.c_sub_population_category as cspc
									where cspc.sub_population = _sub_population_etl_id[1]
									and cspc.id not in (select unnest(_id_comulation))
									and @extschema@.fn_etl_array_compare
										(
										string_to_array(replace(lower(cspc.label_en),' ',''),';'),
										string_to_array(replace(lower(_array_category_label_en_source[iii]),' ',''),';')					
										) = true
									into _etl_id_i_target_array;
									-- founded 1 element in array => ok => auto pair
									-- founded 2 or more elements in array => error
									-- founded 0 => null => ko => manualy pair
								
									if _etl_id_i_target_array is null
									then
										_do_action_in_loop := 0; -- stop action in loop
										_res_loop_iii := 0;
									else
										if array_length(_etl_id_i_target_array,1) is distinct from 1
										then
											raise exception 'Error 02: fn_etl_check_sub_populations4etl_json: For sub population type exists two or more equal categories!';
										else
											_id_comulation := _id_comulation || _etl_id_i_target_array[1];
											_res_loop_iii := 1;
										end if;
									end if;
								end if;						
							end loop;
						end if;							

						if _res_loop_iii = 0
						then
							_res_id_manually := _res_id_manually || _array_id[i];
						else
							_res_id_pair := _res_id_pair || _array_id[i];
							_etl_id := _etl_id || _sub_population_etl_id[1];		
						end if;	
					else
						-- => manualy pair
						_res_id_manually := _res_id_manually || _array_id[i];
					end if;
				end if;
				-----------------------------------------------------
			end loop;

			if	_res_id_pair is distinct from array[0] -- exists one or more types for auto pairing
				and
				_res_id_manually is distinct from array[0] -- exists one or more types for manualy pairing that can move to auto transfer
			then
				--_res_id_pair := array_remove(_res_id_pair,0);
				--_res_id_manually := array_remove(_res_id_manually,0);

				_res_id_manually_final := _res_id_manually;

				for ii in 1..array_length(_res_id_manually,1) -- the second cycle over types for manualy pairing
				loop
					if _res_id_manually[ii] is distinct from 0
					then
						-----------------------------------------------------
						with
						w1 as	(
								select _sub_populations as res
								)
						,w2 as	(
								select json_array_elements(w1.res) as res from w1
								)
						,w3 as	(
								select
										(res->>'id')::integer as id,
										replace(replace((res->>'sub_population')::varchar,'[',''),']','') as sub_population,
										(res->>'label_en_type')::varchar as label_en_type
								from
										w2
								)
						,w4 as	(
								select
										w3.id,
										(string_to_array(w3.sub_population,','))::integer[] as sub_population,
										w3.label_en_type
								from
										w3
								)
						select
								w4.sub_population,
								w4.label_en_type
						from
								w4 where w4.id = _res_id_manually[ii] --_array_id[i]
						into
								_sub_population_ii,
								_label_en_type_ii;
						-----------------------------------------------------
						with
						w1 as	(
								select * from @extschema@.fn_etl_check_sub_populations4etl
									(
									_res_id_manually[ii], --_array_id[ii],
									_sub_population_ii,
									_label_en_type_ii,
									'en'::varchar,
									_etl_id -- this array is supplemented by auto pairing IDs
									)
								)
						select
								array_agg(w1.etl_id order by w1.etl_id) as sp_etl_id, -- ID from c_sub_population table
								array_agg(w1.check_label_en order by w1.etl_id) as sp_check
						from
								w1
						into
								_sub_population_etl_id_ii,
								_sub_population_check_ii;
						-----------------------------------------------------
						if _sub_population_etl_id_ii is null  -- => auto transfer
						then
							_res_id_transfer := _res_id_transfer || _res_id_manually[ii]; -- manually is moved to auto transfer
							_res_id_manually_final := array_remove(_res_id_manually_final,_res_id_manually[ii]); -- id of sub population type is removed from manually pairing
						else
							-- stay in manualy pairing
							-- exist different types with equal count of categories
						end if;																
					end if;
				end loop;
			end if;

			
			-- check MANUALLY and PAIR
			with
			w1 as	(select unnest(_res_id_manually_final) as id_manually)
			,w2 as	(select unnest(_res_id_pair) as id_pair)	
			,w3 as	(select w1.* from w1 where w1.id_manually is distinct from 0)
			,w4 as	(select w2.* from w2 where w2.id_pair is distinct from 0)
			,w5 as	(select count(w3.*) as res from w3 where w3.id_manually in (select w4.id_pair from w4))
			,w6 as	(select count(w4.*) as res from w4 where w4.id_pair in (select w3.id_manually from w3))
			select sum(t.res) from (select w5.res from w5 union all	select w6.res from w6) as t
			into _check_manually_pair;

			if _check_manually_pair is distinct from 0
			then
				raise exception 'Error 03: fn_etl_check_sub_populations4etl_json: There must not exist situation auto pair and manually pair for the sub population type at the same time!';
			end if;

			-- check MANUALLY and TRANFER
			with
			w1 as	(select unnest(_res_id_manually_final) as id_manually)
			,w2 as	(select unnest(_res_id_transfer) as id_transfer)	
			,w3 as	(select w1.* from w1 where w1.id_manually is distinct from 0)
			,w4 as	(select w2.* from w2 where w2.id_transfer is distinct from 0)
			,w5 as	(select count(w3.*) as res from w3 where w3.id_manually in (select w4.id_transfer from w4))
			,w6 as	(select count(w4.*) as res from w4 where w4.id_transfer in (select w3.id_manually from w3))
			select sum(t.res) from (select w5.res from w5 union all	select w6.res from w6) as t
			into _check_manually_transfer;

			if _check_manually_transfer is distinct from 0
			then
				raise exception 'Error 04: fn_etl_check_sub_populations4etl_json: There must not exist situation auto transfer and manually pair for the sub population type at the same time!';
			end if;	

			-- check PAIR and TRANFER
			with
			w1 as	(select unnest(_res_id_pair) as id_pair)
			,w2 as	(select unnest(_res_id_transfer) as id_transfer)	
			,w3 as	(select w1.* from w1 where w1.id_pair is distinct from 0)
			,w4 as	(select w2.* from w2 where w2.id_transfer is distinct from 0)
			,w5 as	(select count(w3.*) as res from w3 where w3.id_pair in (select w4.id_transfer from w4))
			,w6 as	(select count(w4.*) as res from w4 where w4.id_transfer in (select w3.id_pair from w3))
			select sum(t.res) from (select w5.res from w5 union all	select w6.res from w6) as t
			into _check_pair_transfer;

			if _check_pair_transfer is distinct from 0
			then
				raise exception 'Error 05: fn_etl_check_sub_populations4etl_json: There must not exist situation auto transfer and auto pair for the sub population type at the same time!';
			end if;					

			with
			w1 as	(select unnest(_array_id) as id)
			,w2 as	(select unnest(_res_id_pair) as id_pair)
			,w3 as	(select unnest(_res_id_transfer) as id_transfer)
			,w4 as	(select w2.* from w2 where w2.id_pair is distinct from 0)
			,w5 as	(select w3.* from w3 where w3.id_transfer is distinct from 0)
			,w6 as	(
					select
							w1.id,
							case when w5.id_transfer is null then false else true end as auto_transfer,
							case when w4.id_pair is null then false else true end as auto_pair
					from
							w1
							left join w5 on w1.id = w5.id_transfer
							left join w4 on w1.id = w4.id_pair
					order
							by w1.id
					)
			select json_agg(json_build_object('id',w6.id,'auto_transfer',w6.auto_transfer,'auto_pair',w6.auto_pair)) from w6
			into _res;

		end if;
	
		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_check_sub_populations4etl_json(json, integer[]) is
'The function returns one record of sub population if input label of sub population was paired in target DB or the function returns record(s) of sub populations that are possible to pair in target DB. Output is in JSON format.';

grant execute on function @extschema@.fn_etl_check_sub_populations4etl_json(json, integer[]) to public;
-- </function>



-- <function name="fn_etl_check_area_domain_categories4etl" schema="extschema" src="functions/extschema/etl/fn_etl_check_area_domain_categories4etl.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_check_area_domain_categories4etl(integer, json)

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_check_area_domain_categories4etl(integer, json) CASCADE;

create or replace function @extschema@.fn_etl_check_area_domain_categories4etl
(
	_area_domain			integer, -- id from target c_area_domain
	_area_domain_category	json
)
returns table
(
	id						integer,
	area_domain_category	integer[],
	etl_id					integer,
	label					character varying,
	description				text,
	label_en				character varying,
	description_en			text,
	check_label_en			boolean
)
as
$$
declare
		_label_en_type_source				varchar[];
		_check_label_en_type				integer;
		_check_count_categories				integer;
		_check_count_elements				integer;
		_check_count_categories_distinct	integer;
		_check_count_label_en_distinct		integer;
		_raise_notice_text					text;
		_check_info							integer;
begin	
		if _area_domain is null
		then
			raise exception 'Error 01: fn_etl_check_area_domain_categories4etl: Input argument _area_domain must not be NULL!';
		end if;
	
		if _area_domain_category is null
		then
			raise exception 'Error 02: fn_etl_check_area_domain_categories4etl: Input argument _area_domain_category must not be NULL!';
		end if;
		---------------------------------------------------
		with
		w1 as	(
				select _area_domain_category as res
				)
		,w2 as	(
				select json_array_elements(w1.res) as res from w1
				)
		,w3 as	(
				select (res->>'label_en_type')::varchar as label_en_type_source	from w2
				)
		,w4 as	(
				select replace(lower(w3.label_en_type_source),' ','') as label_en_type_source from w3
				)
		select array_agg(t.label_en_type_source) from (select distinct w4.label_en_type_source from w4) as t
		into _label_en_type_source;

		if array_length(_label_en_type_source,1) is distinct from 1
		then
			raise exception 'Error 03: fn_etl_check_area_domain_categories4etl: English labels of area domain in input argument _area_domain_category are not equal!';
		end if;
		--------------------------------------------------
		-- check if input area domain is equal with target
		with
		w1 as	(
				select
						cad.id,
						string_to_array(replace(lower(cad.label_en),' ',''),';') as label_en_type_target4compare
				from
						@extschema@.c_area_domain as cad
				where
						cad.id = _area_domain
				)
		select count(w1.*) from w1 where @extschema@.fn_etl_array_compare(string_to_array(lower(_label_en_type_source[1]),';'),w1.label_en_type_target4compare) = true
		into _check_label_en_type;
		-- there will be value 0 or 1
		-- 0 => source area domain label_en is different from target area domain label_en
		-----------------------------------------------------------------------
		-----------------------------------------------------------------------
		-- source area domain label_en is equal with target area domain label_en => next step are checks of categories
		---------------------------------------------------
		-- count of categories for target area domain
		select count(cadc.*) from @extschema@.c_area_domain_category as cadc
		where cadc.area_domain = _area_domain
		into _check_count_categories;
		---------------------------------------------------
		-- count of categories for source area domain
		with
		w1 as	(
				select _area_domain_category as res
				)
		,w2 as	(
				select json_array_elements(w1.res) from w1
				)
		select count(w2.*) from w2
		into _check_count_elements;
		---------------------------------------------------
		if _check_count_categories is distinct from _check_count_elements
		then
			--raise exception 'Error 03: fn_etl_check_area_domain_categories4etl: Count elements in input argument _area_domain_category is not equal count of categories of input area domain [ID = %] in target c_area_domain_category table!',_area_domain;
			if _check_label_en_type = 1
			then
				_raise_notice_text := 'Selected area domain to pair is equal with source but count of categories is not equal!';
			else
				_raise_notice_text := 'Selected area domain to pair is not equal with source and count of categories is not equal!';
			end if;

			_check_info := 1;
		else
			_check_info := 0;
		end if;
		---------------------------------------------------
		select count(t.*) from
		(select distinct cadc.label_en from @extschema@.c_area_domain_category as cadc
		where cadc.area_domain = _area_domain) as t
		into _check_count_categories_distinct;
		---------------------------------------------------
		if _check_count_categories is distinct from _check_count_categories_distinct
		then
			--raise exception 'Error 04: fn_etl_check_area_domain_categories4etl: English label of categories of input area domain [ID = %] in target c_area_domain_category table is not unique!',_area_domain;
			if _check_label_en_type = 1
			then			
				_raise_notice_text := 'Selected area domain to pair is equal with source but english lables of target are not unique!';
			else
				_raise_notice_text := 'Selected area domain to pair is not equal with source and english lables of target are not unique!';
			end if;

			_check_info := _check_info + 1;
		else
			_check_info := _check_info + 0;
		end if;
		---------------------------------------------------
		with
		w1 as	(
				select _area_domain_category as res
				)
		,w2 as	(
				select json_array_elements(w1.res) as res from w1
				)
		,w3 as	(
				select (res->>'label_en')::varchar as label_en from	w2
				)
		select count(t.*) from
		(select distinct w3.label_en from w3) as t
		into _check_count_label_en_distinct;
		---------------------------------------------------
		if _check_count_elements is distinct from _check_count_label_en_distinct
		then
			--raise exception 'Error 05: fn_etl_check_area_domain_categories4etl: English label of categories of input argument _area_domain_category = % is not unique!',_area_domain_category;
			if _check_label_en_type = 1
			then
				_raise_notice_text := 'Selected area domain to pair is equal with source but english lables of source are not unique!';
			else
				_raise_notice_text := 'Selected area domain to pair is not equal with source and english lables of source are not unique!';
			end if;

			_check_info := _check_info + 1;
		else
			_check_info := _check_info + 0;
		end if;	
		--------------------------------------------------
		-----------------------------------------------------------------------
		-----------------------------------------------------------------------
		if _check_info = 0
		then
			-- OK	
			return query
			with
			w1a as	(
					select _area_domain_category as res
					)
			,w2a as	(
					select json_array_elements(w1a.res) as res from w1a
					)
			,w3a as	(
					select
							(res->>'id')::integer as id_ordering_source,
							replace(replace((res->>'area_domain_category')::varchar,'[',''),']','') as area_domain_category_source,
							(res->>'label_en')::varchar as label_en_source
					from
							w2a
					)
			,w4a as	(
					select
							w3a.id_ordering_source,
							(string_to_array(w3a.area_domain_category_source,','))::integer[] as area_domain_category_source,
							w3a.label_en_source,
							string_to_array(replace(lower(w3a.label_en_source),' ',''),';') as label_en_source4compare
					from
							w3a
					)
			---------------------
			,w1b as	(
					select
							cadc.id as etl_id,
							cadc.area_domain,
							cadc.label as label_target,
							cadc.description as description_target,
							cadc.label_en as label_en_target,
							cadc.description_en as description_en_target,
							string_to_array(replace(lower(cadc.label_en),' ',''),';') as label_en_target4compare
					from
							@extschema@.c_area_domain_category as cadc
					where
							cadc.area_domain = _area_domain
					)
			---------------------
			,w1 as 	(
					select w1b.*, w4a.* from w1b cross join w4a
					)
			,w2 as	(
					select w1.*, true as check_label_en from w1
					where @extschema@.fn_etl_array_compare(w1.label_en_source4compare,w1.label_en_target4compare)
					)
			,w3 as	(
					select
							w2.id_ordering_source,
							w2.area_domain_category_source,
							w2.etl_id,
							w2.label_target as label,
							w2.description_target as description,
							w2.label_en_target as label_en,
							w2.description_en_target as description_en,
							w2.check_label_en
					from
							w2
					)
			,w4 as	(
					select
							w4a.id_ordering_source as id,
							w4a.area_domain_category_source as area_domain_category,
							w3.etl_id,
							w3.label,
							w3.description,
							w3.label_en,
							w3.description_en,
							w3.check_label_en
					from
							w4a left join w3
					on
							w4a.id_ordering_source = w3.id_ordering_source
					and		w4a.area_domain_category_source = w3.area_domain_category_source
					)
			select w4.* from w4 order by w4.id;
		else
			raise notice '%',_raise_notice_text;

			return query
			select
					null::integer as id,
					null::integer[] as area_domain_category,
					null::integer as etl_id,
					cadc.label,
					cadc.description,
					cadc.label_en,
					cadc.description_en,
					null::boolean as check_label_en
			from
					@extschema@.c_area_domain_category as cadc
			where
					cadc.area_domain = _area_domain
			order
					by cadc.id;			
		end if;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_check_area_domain_categories4etl(integer, json) is
'The function returns records of area domain categories for given input area domain with inforamtion if input label of area domain category was paired in target DB or not.';

grant execute on function @extschema@.fn_etl_check_area_domain_categories4etl(integer, json) to public;
-- </function>



-- <function name="fn_etl_check_sub_population_categories4etl" schema="extschema" src="functions/extschema/etl/fn_etl_check_sub_population_categories4etl.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_check_sub_population_categories4etl(integer, json)

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_check_sub_population_categories4etl(integer, json) CASCADE;

create or replace function @extschema@.fn_etl_check_sub_population_categories4etl
(
	_sub_population			integer, -- id from target c_sub_population
	_sub_population_category	json
)
returns table
(
	id						integer,
	sub_population_category	integer[],
	etl_id					integer,
	label					character varying,
	description				text,
	label_en				character varying,
	description_en			text,
	check_label_en			boolean
)
as
$$
declare
		_label_en_type_source				varchar[];
		_check_label_en_type				integer;
		_check_count_categories				integer;
		_check_count_elements				integer;
		_check_count_categories_distinct	integer;
		_check_count_label_en_distinct		integer;
		_raise_notice_text					text;
		_check_info							integer;
begin	
		if _sub_population is null
		then
			raise exception 'Error 01: fn_etl_check_sub_population_categories4etl: Input argument _sub_population must not be NULL!';
		end if;
	
		if _sub_population_category is null
		then
			raise exception 'Error 02: fn_etl_check_sub_population_categories4etl: Input argument _sub_population_category must not be NULL!';
		end if;
		---------------------------------------------------
		with
		w1 as	(
				select _sub_population_category as res
				)
		,w2 as	(
				select json_array_elements(w1.res) as res from w1
				)
		,w3 as	(
				select (res->>'label_en_type')::varchar as label_en_type_source	from w2
				)
		,w4 as	(
				select replace(lower(w3.label_en_type_source),' ','') as label_en_type_source from w3
				)
		select array_agg(t.label_en_type_source) from (select distinct w4.label_en_type_source from w4) as t
		into _label_en_type_source;

		if array_length(_label_en_type_source,1) is distinct from 1
		then
			raise exception 'Error 03: fn_etl_check_sub_population_categories4etl: English labels of sub population in input argument _sub_population_category are not equal!';
		end if;
		--------------------------------------------------
		-- check if input sub population is equal with target
		with
		w1 as	(
				select
						csp.id,
						string_to_array(replace(lower(csp.label_en),' ',''),';') as label_en_type_target4compare
				from
						@extschema@.c_sub_population as csp
				where
						csp.id = _sub_population
				)
		select count(w1.*) from w1 where @extschema@.fn_etl_array_compare(string_to_array(lower(_label_en_type_source[1]),';'),w1.label_en_type_target4compare) = true
		into _check_label_en_type;
		-- there will be value 0 or 1
		-- 0 => source sub population label_en is different from target sub population label_en
		-----------------------------------------------------------------------
		-----------------------------------------------------------------------
		-- source sub population label_en is equal with target sub population label_en => next step are checks of categories
		---------------------------------------------------
		-- count of categories for target sub population
		select count(cspc.*) from @extschema@.c_sub_population_category as cspc
		where cspc.sub_population = _sub_population
		into _check_count_categories;
		---------------------------------------------------
		-- count of categories for source sub population
		with
		w1 as	(
				select _sub_population_category as res
				)
		,w2 as	(
				select json_array_elements(w1.res) from w1
				)
		select count(w2.*) from w2
		into _check_count_elements;
		---------------------------------------------------
		if _check_count_categories is distinct from _check_count_elements
		then
			--raise exception 'Error 03: fn_etl_check_sub_population_categories4etl: Count elements in input argument _sub_population_category is not equal count of categories of input sub population [ID = %] in target c_sub_population_category table!',_sub_population;
			if _check_label_en_type = 1
			then
				_raise_notice_text := 'Selected sub population to pair is equal with source but count of categories is not equal!';
			else
				_raise_notice_text := 'Selected sub population to pair is not equal with source and count of categories is not equal!';
			end if;

			_check_info := 1;
		else
			_check_info := 0;
		end if;
		---------------------------------------------------
		select count(t.*) from
		(select distinct cspc.label_en from @extschema@.c_sub_population_category as cspc
		where cspc.sub_population = _sub_population) as t
		into _check_count_categories_distinct;
		---------------------------------------------------
		if _check_count_categories is distinct from _check_count_categories_distinct
		then
			--raise exception 'Error 04: fn_etl_check_sub_population_categories4etl: English label of categories of input sub population [ID = %] in target c_sub_population_category table is not unique!',_sub_population;
			if _check_label_en_type = 1
			then			
				_raise_notice_text := 'Selected sub population to pair is equal with source but english lables of target are not unique!';
			else
				_raise_notice_text := 'Selected sub population to pair is not equal with source and english lables of target are not unique!';
			end if;

			_check_info := _check_info + 1;
		else
			_check_info := _check_info + 0;
		end if;
		---------------------------------------------------
		with
		w1 as	(
				select _sub_population_category as res
				)
		,w2 as	(
				select json_array_elements(w1.res) as res from w1
				)
		,w3 as	(
				select (res->>'label_en')::varchar as label_en from	w2
				)
		select count(t.*) from
		(select distinct w3.label_en from w3) as t
		into _check_count_label_en_distinct;
		---------------------------------------------------
		if _check_count_elements is distinct from _check_count_label_en_distinct
		then
			--raise exception 'Error 05: fn_etl_check_sub_population_categories4etl: English label of categories of input argument _sub_population_category = % is not unique!',_sub_population_category;
			if _check_label_en_type = 1
			then
				_raise_notice_text := 'Selected sub population to pair is equal with source but english lables of source are not unique!';
			else
				_raise_notice_text := 'Selected sub population to pair is not equal with source and english lables of source are not unique!';
			end if;

			_check_info := _check_info + 1;
		else
			_check_info := _check_info + 0;
		end if;	
		--------------------------------------------------
		-----------------------------------------------------------------------
		-----------------------------------------------------------------------
		if _check_info = 0
		then
			-- OK	
			return query
			with
			w1a as	(
					select _sub_population_category as res
					)
			,w2a as	(
					select json_array_elements(w1a.res) as res from w1a
					)
			,w3a as	(
					select
							(res->>'id')::integer as id_ordering_source,
							replace(replace((res->>'sub_population_category')::varchar,'[',''),']','') as sub_population_category_source,
							(res->>'label_en')::varchar as label_en_source
					from
							w2a
					)
			,w4a as	(
					select
							w3a.id_ordering_source,
							(string_to_array(w3a.sub_population_category_source,','))::integer[] as sub_population_category_source,
							w3a.label_en_source,
							string_to_array(replace(lower(w3a.label_en_source),' ',''),';') as label_en_source4compare
					from
							w3a
					)
			---------------------
			,w1b as	(
					select
							cspc.id as etl_id,
							cspc.sub_population,
							cspc.label as label_target,
							cspc.description as description_target,
							cspc.label_en as label_en_target,
							cspc.description_en as description_en_target,
							string_to_array(replace(lower(cspc.label_en),' ',''),';') as label_en_target4compare
					from
							@extschema@.c_sub_population_category as cspc
					where
							cspc.sub_population = _sub_population
					)
			---------------------
			,w1 as 	(
					select w1b.*, w4a.* from w1b cross join w4a
					)
			,w2 as	(
					select w1.*, true as check_label_en from w1
					where @extschema@.fn_etl_array_compare(w1.label_en_source4compare,w1.label_en_target4compare)
					)
			,w3 as	(
					select
							w2.id_ordering_source,
							w2.sub_population_category_source,
							w2.etl_id,
							w2.label_target as label,
							w2.description_target as description,
							w2.label_en_target as label_en,
							w2.description_en_target as description_en,
							w2.check_label_en
					from
							w2
					)
			,w4 as	(
					select
							w4a.id_ordering_source as id,
							w4a.sub_population_category_source as sub_population_category,
							w3.etl_id,
							w3.label,
							w3.description,
							w3.label_en,
							w3.description_en,
							w3.check_label_en
					from
							w4a left join w3
					on
							w4a.id_ordering_source = w3.id_ordering_source
					and		w4a.sub_population_category_source = w3.sub_population_category_source
					)
			select w4.* from w4 order by w4.id;
		else
			raise notice '%',_raise_notice_text;

			return query
			select
					null::integer as id,
					null::integer[] as sub_population_category,
					null::integer as etl_id,
					cspc.label,
					cspc.description,
					cspc.label_en,
					cspc.description_en,
					null::boolean as check_label_en
			from
					@extschema@.c_sub_population_category as cspc
			where
					cspc.sub_population = _sub_population
			order
					by cspc.id;			
		end if;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_check_sub_population_categories4etl(integer, json) is
'The function returns records of sub population categories for given input sub population with inforamtion if input label of sub population category was paired in target DB or not.';

grant execute on function @extschema@.fn_etl_check_sub_population_categories4etl(integer, json) to public;
-- </function>



-- <function name="fn_etl_get_area_domain_categories4roller" schema="extschema" src="functions/extschema/etl/fn_etl_get_area_domain_categories4roller.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_get_area_domain_categories4roller(integer, integer, character varying, integer[])

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_get_area_domain_categories4roller(integer, integer, character varying, integer[]) CASCADE;

create or replace function @extschema@.fn_etl_get_area_domain_categories4roller
(
	_id_ordering			integer,
	_area_domain			integer,
	_national_language		character varying(2) default 'en'::character varying(2),
	_etl_id					integer[] default null::integer[]
)
returns table
(
	id						integer,
	etl_id					integer,
	label					character varying,
	description				text,
	label_en				character varying,
	description_en			text
)
as
$$
declare
		_set_ordering_column				text;
begin	
		if _id_ordering is null
		then
			raise exception 'Error 01: fn_etl_get_area_domain_categories4roller: Input argument _id_ordering must not be NULL!';
		end if;
	
		if _area_domain is null
		then
			raise exception 'Error 02: fn_etl_get_area_domain_categories4roller: Input argument _area_domain must not be NULL!';
		end if;
	
		if _national_language is null
		then
			raise exception 'Error 03: fn_etl_get_area_domain_categories4roller: Input argument _national_language must not be NULL!';
		end if;
		---------------------------------------------------
		if _national_language = 'en'
		then
			_set_ordering_column := 'w1.label_en';
		else
			_set_ordering_column := 'w1.label';
		end if;
		---------------------------------------------------
		return query execute
		'
		with
		w1 as	(
				select cadc.* from @extschema@.c_area_domain_category cadc
				where cadc.area_domain = $1
				and cadc.id not in (select unnest($2))
				)
		select
				$3 as id,
				w1.id as etl_id,
				w1.label,
				w1.description,
				w1.label_en,
				w1.description_en
		from
				w1 order by '||_set_ordering_column ||'
		'
		using _area_domain, _etl_id, _id_ordering;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_get_area_domain_categories4roller(integer, integer, character varying, integer[]) is
'The function returns records of area domain categories for given area domain into roller.';

grant execute on function @extschema@.fn_etl_get_area_domain_categories4roller(integer, integer, character varying, integer[]) to public;
-- </function>



-- <function name="fn_etl_get_sub_population_categories4roller" schema="extschema" src="functions/extschema/etl/fn_etl_get_sub_population_categories4roller.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_get_sub_population_categories4roller(integer, integer, character varying, integer[])

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_get_sub_population_categories4roller(integer, integer, character varying, integer[]) CASCADE;

create or replace function @extschema@.fn_etl_get_sub_population_categories4roller
(
	_id_ordering			integer,
	_sub_population			integer,
	_national_language		character varying(2) default 'en'::character varying(2),
	_etl_id					integer[] default null::integer[]
)
returns table
(
	id						integer,
	etl_id					integer,
	label					character varying,
	description				text,
	label_en				character varying,
	description_en			text
)
as
$$
declare
		_set_ordering_column				text;
begin	
		if _id_ordering is null
		then
			raise exception 'Error 01: fn_etl_get_sub_population_categories4roller: Input argument _id_ordering must not be NULL!';
		end if;
	
		if _sub_population is null
		then
			raise exception 'Error 02: fn_etl_get_sub_population_categories4roller: Input argument _sub_population must not be NULL!';
		end if;
	
		if _national_language is null
		then
			raise exception 'Error 03: fn_etl_get_sub_population_categories4roller: Input argument _national_language must not be NULL!';
		end if;
		---------------------------------------------------
		if _national_language = 'en'
		then
			_set_ordering_column := 'w1.label_en';
		else
			_set_ordering_column := 'w1.label';
		end if;
		---------------------------------------------------
		return query execute
		'
		with
		w1 as	(
				select cspc.* from @extschema@.c_sub_population_category as cspc
				where cspc.sub_population = $1
				and cspc.id not in (select unnest($2))
				)
		select
				$3 as id,
				w1.id as etl_id,
				w1.label,
				w1.description,
				w1.label_en,
				w1.description_en
		from
				w1 order by '||_set_ordering_column ||'
		'
		using _sub_population, _etl_id, _id_ordering;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_get_sub_population_categories4roller(integer, integer, character varying, integer[]) is
'The function returns records of sub population categories for given sub population into roller.';

grant execute on function @extschema@.fn_etl_get_sub_population_categories4roller(integer, integer, character varying, integer[]) to public;
-- </function>



-- <function name="fn_etl_get_topics4target_variable" schema="extschema" src="functions/extschema/etl/fn_etl_get_topics4target_variable.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_get_topics4target_variable(integer, character varying, boolean)

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_get_topics4target_variable(integer, character varying, boolean) CASCADE;

create or replace function @extschema@.fn_etl_get_topics4target_variable
(
	_target_variable	integer,
	_national_language	character varying(2) default 'en'::character varying,
	_linked				boolean default null::boolean
)
returns table
(
	id				integer,
	label			varchar,
	description		text,
	label_en		varchar,
	description_en	text,
	linked			boolean
)
as
$$
declare
		_set_ordering_column	text;
		_cond					text;
begin
		if _target_variable is null
		then
			raise exception 'Error 01: fn_etl_get_topics4target_variable: Input argument _target_variable must not be null!';
		end if;
	
		if _national_language is null
		then
			raise exception 'Error 01: fn_etl_get_topics4target_variable: Input argument _national_language must not be null!';
		end if;	
		---------------------------------------------------
		if _national_language = 'en'
		then
			_set_ordering_column := 'w3.label_en';
		else
			_set_ordering_column := 'w3.label';
		end if;
		---------------------------------------------------
		if _linked is null
		then
			_cond := 'TRUE';
		else
			if _linked = true
			then
				_cond := 'w3.linked = true';
			else
				_cond := 'w3.linked = false';
			end if;
		end if;
		---------------------------------------------------	
		return query execute
		'
		with
		w1 as	(
				select ct.id, ct.label, ct.description, ct.label_en, ct.description_en
				from @extschema@.c_topic as ct
				)
		,w2 as	(
				select distinct topic from @extschema@.cm_tvariable2topic
				where target_variable = $1
				)
		,w3 as	(
				select
						w1.id,
						w1.label,
						w1.description,
						w1.label_en,
						w1.description_en,
						case when w2.topic is null then false else true end as linked
				from
						w1 left join w2 on w1.id = w2.topic
				)
		select
				w3.* from w3
		where
				'|| _cond ||'
		order
				by '|| _set_ordering_column ||'
		'
		using _target_variable;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_get_topics4target_variable(integer, character varying, boolean) is
'The function returns all topics and information whitch topic is the given target variable linked.';

grant execute on function @extschema@.fn_etl_get_topics4target_variable(integer, character varying, boolean) to public;
-- </function>

