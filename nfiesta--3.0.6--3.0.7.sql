--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

drop view @extschema@.v_variable_hierarchy;

-- <view name="v_variable_hierarchy" schema="extschema" src="views/extschema/v_variable_hierarchy.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

CREATE OR REPLACE VIEW @extschema@.v_variable_hierarchy_aux_total AS
SELECT
	t_variable_hierarchy.variable_superior as node,
	array_agg(t_variable_hierarchy.variable order by t_variable_hierarchy.variable) AS edges
FROM @extschema@.t_variable_hierarchy
INNER JOIN @extschema@.t_variable 			AS edge_var ON t_variable_hierarchy.variable = edge_var.id
LEFT JOIN @extschema@.c_sub_population_category 	AS edge_spc ON edge_var.sub_population_category = edge_spc.id
LEFT JOIN @extschema@.c_area_domain_category 		AS edge_adc ON edge_var.area_domain_category = edge_adc.id
LEFT JOIN @extschema@.c_auxiliary_variable_category 	AS edge_avc ON edge_var.auxiliary_variable_category = edge_avc.id
GROUP BY
	t_variable_hierarchy.variable_superior,
	edge_spc.sub_population, edge_adc.area_domain, edge_avc.auxiliary_variable
ORDER BY
	t_variable_hierarchy.variable_superior,
	edge_spc.sub_population, edge_adc.area_domain, edge_avc.auxiliary_variable
;

GRANT SELECT ON TABLE @extschema@.v_variable_hierarchy_aux_total TO PUBLIC;

----------------------------------------------------------------------------------------------------------

CREATE OR REPLACE VIEW @extschema@.v_variable_hierarchy_plot AS
SELECT
	node_ads.panel, node_ads.reference_year_set,
	t_variable_hierarchy.variable_superior as node,
	array_agg(t_variable_hierarchy.variable order by t_variable_hierarchy.variable) AS edges
FROM @extschema@.t_variable_hierarchy
INNER JOIN @extschema@.t_variable 			AS node_var	ON (t_variable_hierarchy.variable_superior	=	node_var.id)
INNER JOIN @extschema@.t_available_datasets		AS node_ads	ON (node_var.id 				=	node_ads.variable)
INNER JOIN @extschema@.t_variable 			AS edge_var	ON (t_variable_hierarchy.variable		=	edge_var.id)
INNER JOIN @extschema@.t_available_datasets		AS edge_ads	ON (edge_var.id 				=	edge_ads.variable)
LEFT JOIN @extschema@.c_sub_population_category 	AS edge_spc 	ON edge_var.sub_population_category 		= 	edge_spc.id
LEFT JOIN @extschema@.c_area_domain_category 		AS edge_adc 	ON edge_var.area_domain_category 		= 	edge_adc.id
LEFT JOIN @extschema@.c_auxiliary_variable_category 	AS edge_avc 	ON edge_var.auxiliary_variable_category 	= 	edge_avc.id
WHERE  	(node_var.auxiliary_variable_category is null and edge_var.auxiliary_variable_category is null)
	and ((node_ads.panel = edge_ads.panel) AND (node_ads.reference_year_set = edge_ads.reference_year_set))
GROUP BY
	node_ads.panel, node_ads.reference_year_set,
	t_variable_hierarchy.variable_superior,
	edge_spc.sub_population, edge_adc.area_domain, edge_avc.auxiliary_variable
ORDER BY
	node_ads.panel, node_ads.reference_year_set,
	t_variable_hierarchy.variable_superior,
	edge_spc.sub_population, edge_adc.area_domain, edge_avc.auxiliary_variable
;

GRANT SELECT ON TABLE @extschema@.v_variable_hierarchy_plot TO PUBLIC;

----------------------------------------------------------------------------------------------------------

CREATE OR REPLACE VIEW @extschema@.v_variable_hierarchy_plot_aux AS
SELECT
	node_ads.panel, node_ads.reference_year_set,
	t_variable_hierarchy.variable_superior as node,
	array_agg(t_variable_hierarchy.variable order by t_variable_hierarchy.variable) AS edges
FROM @extschema@.t_variable_hierarchy
INNER JOIN @extschema@.t_variable 			AS node_var	ON (t_variable_hierarchy.variable_superior	=	node_var.id)
INNER JOIN @extschema@.t_available_datasets		AS node_ads	ON (node_var.id 				=	node_ads.variable)
INNER JOIN @extschema@.t_variable 			AS edge_var	ON (t_variable_hierarchy.variable		=	edge_var.id)
INNER JOIN @extschema@.t_available_datasets		AS edge_ads	ON (edge_var.id 				=	edge_ads.variable)
LEFT JOIN @extschema@.c_sub_population_category 	AS edge_spc 	ON edge_var.sub_population_category 		= 	edge_spc.id
LEFT JOIN @extschema@.c_area_domain_category 		AS edge_adc 	ON edge_var.area_domain_category 		= 	edge_adc.id
LEFT JOIN @extschema@.c_auxiliary_variable_category 	AS edge_avc 	ON edge_var.auxiliary_variable_category 	= 	edge_avc.id
WHERE  	(node_var.auxiliary_variable_category is not null and edge_var.auxiliary_variable_category is not null)
	and ((node_ads.panel = edge_ads.panel))
GROUP BY
	node_ads.panel, node_ads.reference_year_set,
	t_variable_hierarchy.variable_superior,
	edge_spc.sub_population, edge_adc.area_domain, edge_avc.auxiliary_variable
ORDER BY
	node_ads.panel, node_ads.reference_year_set,
	t_variable_hierarchy.variable_superior,
	edge_spc.sub_population, edge_adc.area_domain, edge_avc.auxiliary_variable
;

GRANT SELECT ON TABLE @extschema@.v_variable_hierarchy_plot_aux TO PUBLIC;

----------------------------------------------------------------------------------------------------------

CREATE OR REPLACE VIEW @extschema@.v_variable_hierarchy_res AS
SELECT
	node_tec.panel_refyearset_group,
	t_variable_hierarchy.variable_superior as node,
	array_agg(distinct t_variable_hierarchy.variable order by t_variable_hierarchy.variable) AS edges
FROM @extschema@.t_variable_hierarchy
INNER JOIN @extschema@.t_variable 			AS node_var	ON (t_variable_hierarchy.variable_superior	=	node_var.id)
INNER JOIN (select distinct variable, panel_refyearset_group from @extschema@.t_total_estimate_conf)            
							AS node_tec     ON (node_var.id                                 =       node_tec.variable)
INNER JOIN @extschema@.t_variable 			AS edge_var	ON (t_variable_hierarchy.variable		=	edge_var.id)
INNER JOIN (select distinct variable, panel_refyearset_group from @extschema@.t_total_estimate_conf)           
							AS edge_tec     ON (edge_var.id                                 =       edge_tec.variable)
LEFT JOIN @extschema@.c_sub_population_category 	AS edge_spc 	ON edge_var.sub_population_category 		= 	edge_spc.id
LEFT JOIN @extschema@.c_area_domain_category 		AS edge_adc 	ON edge_var.area_domain_category 		= 	edge_adc.id
LEFT JOIN @extschema@.c_auxiliary_variable_category 	AS edge_avc 	ON edge_var.auxiliary_variable_category 	= 	edge_avc.id
WHERE (node_tec.panel_refyearset_group = edge_tec.panel_refyearset_group)
GROUP BY
	node_tec.panel_refyearset_group,
	t_variable_hierarchy.variable_superior,
	edge_spc.sub_population, edge_adc.area_domain, edge_avc.auxiliary_variable
ORDER BY
	node_tec.panel_refyearset_group,
	t_variable_hierarchy.variable_superior,
	edge_spc.sub_population, edge_adc.area_domain, edge_avc.auxiliary_variable
;

GRANT SELECT ON TABLE @extschema@.v_variable_hierarchy_res TO PUBLIC;


-- </view>

-- <function name="fn_check_t_target_data" schema="extschema" src="functions/extschema/fn_check_t_target_data.sql">
--
-- Copyright 2021, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

CREATE OR REPLACE FUNCTION @extschema@.fn_check_t_target_data() RETURNS TRIGGER AS $src$
    DECLARE
		_vars int[];
		_vars_add_set int[];
		_plots int[];
		_refyearsets int[];
		_errp json;
    BEGIN
        IF (TG_OP = 'DELETE' or TG_OP = 'UPDATE' or TG_OP = 'INSERT') THEN
			IF (TG_TABLE_NAME = 't_target_data') THEN
				with w_vars_add_set as (
						select array_prepend(node, edges) as vs
					from trans_table
					inner join sdesign.f_p_plot on (trans_table.plot = f_p_plot.gid)
					inner join sdesign.t_cluster on (t_cluster.id = f_p_plot.cluster)
					inner join sdesign.cm_cluster2panel_mapping  on (cm_cluster2panel_mapping.cluster = t_cluster.id)
					inner join sdesign.t_panel on (t_panel.id = cm_cluster2panel_mapping.panel)
					inner join sdesign.cm_plot2cluster_config_mapping on (f_p_plot.gid = cm_plot2cluster_config_mapping.plot)
					inner join sdesign.t_cluster_configuration on (t_cluster_configuration.id = cm_plot2cluster_config_mapping.cluster_configuration)
					inner join @extschema@.v_variable_hierarchy_plot
						on ((trans_table.variable = v_variable_hierarchy_plot.node
							or trans_table.variable = any (v_variable_hierarchy_plot.edges))
						and t_panel.id = v_variable_hierarchy_plot.panel
						and trans_table.reference_year_set = v_variable_hierarchy_plot.reference_year_set
						)
					where t_panel.cluster_configuration = t_cluster_configuration.id
				)
				, w_vars_add_set_un as (
						select unnest(vs) as v from w_vars_add_set
				)
				select array_agg(distinct v)			into _vars_add_set		from w_vars_add_set_un;
				select array_agg(distinct variable)		into _vars			from trans_table;
				select array_agg(distinct plot) 		into _plots 			from trans_table;
				select array_agg(distinct reference_year_set) 	into _refyearsets 		from trans_table;
			ELSIF (TG_TABLE_NAME = 't_available_datasets') THEN
				with w_vars_add_set as (
						select array_prepend(node, edges) as vs
					from trans_table
					inner join @extschema@.v_variable_hierarchy_plot
						on ((trans_table.variable = v_variable_hierarchy_plot.node
							or trans_table.variable = any (v_variable_hierarchy_plot.edges))
						and trans_table.panel = v_variable_hierarchy_plot.panel
						and trans_table.reference_year_set = v_variable_hierarchy_plot.reference_year_set
						)
				)
				, w_vars_add_set_un as (
						select unnest(vs) as v from w_vars_add_set
				)
				select array_agg(distinct v)			into _vars_add_set		from w_vars_add_set_un;
				select array_agg(distinct variable)		into _vars			from trans_table;
				select array_agg(distinct f_p_plot.gid)		into _plots			from trans_table
					inner join sdesign.t_panel on (trans_table.panel = t_panel.id)
					inner join sdesign.cm_cluster2panel_mapping on (t_panel.id = cm_cluster2panel_mapping.panel)
					inner join sdesign.t_cluster on (cm_cluster2panel_mapping.cluster = t_cluster.id)
					inner join sdesign.f_p_plot on (t_cluster.id = f_p_plot.cluster)
					inner join sdesign.cm_plot2cluster_config_mapping on (f_p_plot.gid = cm_plot2cluster_config_mapping.plot)
					inner join sdesign.t_cluster_configuration on (t_cluster_configuration.id = cm_plot2cluster_config_mapping.cluster_configuration)
					where t_panel.cluster_configuration = t_cluster_configuration.id
					;
				select array_agg(distinct reference_year_set) 	into _refyearsets 		from trans_table;
			ELSE
				RAISE EXCEPTION 'fn_check_t_target_data -- % -- table name not known: %', TG_OP, TG_TABLE_NAME;
			END IF;


			if _vars_add_set is not null then
				--raise notice '%		fn_check_t_target_data -- % -- % -- CHECK START -- select target_data.fn_add_plot_target_attr(%, %, %, %, %);', clock_timestamp()::timestamp with time zone at time zone 'Europe/Prague', TG_TABLE_NAME, TG_OP, _vars_add_set, _plots, _refyearsets, 1e-6, true;
				
				with w_err as (
					select @extschema@.fn_add_plot_target_attr(_vars_add_set, _plots, _refyearsets, 1e-6, true) as fn_err
				)
				, w_err_t as (
					select (fn_err).* from w_err
				)
				, w_err_json as (
					select 
						json_build_object(
							'plot'				, plot,
							'reference_year_set'		, reference_year_set,
							'variable'			, variable,
							'ldsity'			, ldsity,
							'ldsity_sum'			, ldsity_sum,
							'variables_def'			, variables_def,
							'variables_found'		, variables_found,
							'diff'				, diff
						) as errj
					from w_err_t
				)
				select json_agg(errj) into _errp from w_err_json;
				--raise notice '%		fn_check_t_target_data -- % -- % -- CHECK STOP', clock_timestamp()::timestamp with time zone at time zone 'Europe/Prague', TG_TABLE_NAME, TG_OP;

				IF
					(json_array_length(_errp) > 0)
					THEN RAISE EXCEPTION 'fn_check_t_target_data -- % -- % -- plot level local densities are not additive:
						checked variables: %, variables in additivity set: %, checked plots: %,
						found err plots: 
						%', TG_TABLE_NAME, TG_OP, _vars, _vars_add_set, _plots, jsonb_pretty(_errp::jsonb);
				END IF;
			else
				RAISE WARNING 'fn_check_t_target_data -- % -- % -- additivity check was skiped:
						* 0 rows edited (last line displays no plots)
						* corresponding variable hierarchy not found (last line displays no variables in additivity set)
						checked variables: %, variables in additivity set: %, checked plots: %,'
						, TG_TABLE_NAME, TG_OP, _vars, _vars_add_set, _plots;
			end if;
	ELSE
		RAISE EXCEPTION 'fn_check_t_target_data -- trigger operation not known: %', TG_OP;
	END IF;
        RETURN NULL; -- result is ignored since this is an AFTER trigger
    END;
$src$ LANGUAGE plpgsql;

DROP TRIGGER trg__target_data__ins ON @extschema@.t_target_data;
CREATE TRIGGER trg__target_data__ins
    AFTER INSERT ON @extschema@.t_target_data
    REFERENCING NEW TABLE AS trans_table
    FOR EACH STATEMENT EXECUTE FUNCTION @extschema@.fn_check_t_target_data();

DROP TRIGGER trg__target_data__upd ON @extschema@.t_target_data;
CREATE TRIGGER trg__target_data__upd
    AFTER UPDATE ON @extschema@.t_target_data
    REFERENCING NEW TABLE AS trans_table
    FOR EACH STATEMENT EXECUTE FUNCTION @extschema@.fn_check_t_target_data();

DROP TRIGGER trg__target_data__del ON @extschema@.t_target_data;
CREATE TRIGGER trg__target_data__del
    AFTER DELETE ON @extschema@.t_target_data
    REFERENCING OLD TABLE AS trans_table
    FOR EACH STATEMENT EXECUTE FUNCTION @extschema@.fn_check_t_target_data();

DROP TRIGGER trg__available_datasets__ins ON @extschema@.t_available_datasets;
CREATE TRIGGER trg__available_datasets__ins
    AFTER INSERT ON @extschema@.t_available_datasets
    REFERENCING NEW TABLE AS trans_table
    FOR EACH STATEMENT EXECUTE FUNCTION @extschema@.fn_check_t_target_data();

DROP TRIGGER trg__available_datasets__upd ON @extschema@.t_available_datasets;
CREATE TRIGGER trg__available_datasets__upd
    AFTER UPDATE ON @extschema@.t_available_datasets
    REFERENCING NEW TABLE AS trans_table
    FOR EACH STATEMENT EXECUTE FUNCTION @extschema@.fn_check_t_target_data();

DROP TRIGGER trg__available_datasets__del ON @extschema@.t_available_datasets;
CREATE TRIGGER trg__available_datasets__del
    AFTER DELETE ON @extschema@.t_available_datasets
    REFERENCING OLD TABLE AS trans_table
    FOR EACH STATEMENT EXECUTE FUNCTION @extschema@.fn_check_t_target_data();

-- </function>

-- <function name="fn_add_plot_aux_attr" schema="extschema" src="functions/extschema/additivity/fn_add_plot_aux_attr.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_add_plot_aux_attr(integer)

-- DROP FUNCTION @extschema@.fn_add_plot_aux_attr(integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_add_plot_aux_attr(
	IN variables integer[],
	IN min_diff double precision default 0.0, IN include_null_diff boolean default true
)
  RETURNS TABLE(
	plot			integer,
	variable		integer,
	ldsity			double precision,
	ldsity_sum		double precision,
	variables_def		integer[],
	variables_found		integer[],
	diff			double precision
) AS
$BODY$
DECLARE
	_complete_query text;
	_array_text text;
BEGIN
	--------------------------------QUERY--------------------------------
	_array_text := concat(quote_literal(variables::text), '::integer[]');
	--raise notice 'variables: %',  _array_text;
	_complete_query := '
with w_plot_var as not materialized (
	select
		f_p_plot.gid as plot, t_available_datasets.variable, t_available_datasets.panel,
		coalesce(t_auxiliary_data.value, 0) as value
	from sdesign.f_p_plot
	inner join sdesign.t_cluster on (f_p_plot.cluster = t_cluster.id)
	inner join sdesign.cm_cluster2panel_mapping on (t_cluster.id = cm_cluster2panel_mapping.cluster)
	inner join sdesign.t_panel on (cm_cluster2panel_mapping.panel = t_panel.id)
	inner join sdesign.t_cluster_configuration on (t_panel.cluster_configuration = t_cluster_configuration.id)
	inner join sdesign.cm_plot2cluster_config_mapping on (t_cluster_configuration.id = cm_plot2cluster_config_mapping.cluster_configuration and cm_plot2cluster_config_mapping.plot = f_p_plot.gid)
	inner join @extschema@.t_available_datasets on (t_panel.id = t_available_datasets.panel)
	inner join @extschema@.t_variable on (t_available_datasets.variable = t_variable.id and t_variable.auxiliary_variable_category is not null)
	left join @extschema@.t_auxiliary_data on (
		f_p_plot.gid = t_auxiliary_data.plot
		and t_available_datasets.variable = t_auxiliary_data.variable
		and t_auxiliary_data.is_latest)
	WHERE t_available_datasets.variable = ANY (' || _array_text || ')

)
, w_node_sum as (
	select
		plot,
		w_plot_var.panel,

		w_plot_var.variable,
		value as node_sum,
		v_variable_hierarchy_plot_aux.node,
		v_variable_hierarchy_plot_aux.edges as edges_def
	from w_plot_var
	inner join @extschema@.v_variable_hierarchy_plot_aux on (v_variable_hierarchy_plot_aux.node = w_plot_var.variable and
							v_variable_hierarchy_plot_aux.panel = w_plot_var.panel)
)
, w_edge_sum as (
	select
		w_node_sum.plot,

		w_node_sum.node,
		w_node_sum.node_sum,
		w_node_sum.edges_def,
		array_agg(w_plot_var.variable order by w_plot_var.variable) as edges_found,
		sum(w_plot_var.value) as edges_sum
	from w_node_sum
	left join w_plot_var on (
		w_plot_var.plot = w_node_sum.plot
		and w_plot_var.panel = w_node_sum.panel
		
		and w_plot_var.variable = any(w_node_sum.edges_def))
	group by w_node_sum.plot,
		w_node_sum.node, w_node_sum.node_sum, w_node_sum.edges_def
)
, w_diff as (
	select
		plot,

		node		as variable,
		node_sum	as ldsity,
		edges_sum	as ldsity_sum,
		edges_def	as variables_def,
		edges_found	as variables_found,
		case
			when edges_sum != 0.0 and node_sum = 0.0 then 1.0
			when node_sum = 0.0 and edges_sum = 0.0 then 0
			else abs( (node_sum - edges_sum) / node_sum )
		end as diff
	from w_edge_sum
)
select * from w_diff
where
	(' || include_null_diff || ' and ((diff >= ' || min_diff || ') or (diff is null)))
	or
	(not ' || include_null_diff || ' and (diff >= ' || min_diff || '))
order by diff desc
;
';

	--RAISE NOTICE '%', _complete_query;
	RETURN QUERY EXECUTE _complete_query;
END;
$BODY$
  LANGUAGE plpgsql STABLE
  COST 100
  ROWS 100000;

comment on function @extschema@.fn_add_plot_aux_attr(integer[], double precision, boolean) is
'Function showing plot level auxiliary local density attribute additivity (aggregated class estimate should be equal to sum of sub-classes estimates) of auxiliary local densities. Hierarchy between aggregated classes and its sub-classes is defined in v_variable_hierarchy_plot.
Function input argument is:
 * Array of attributes -- variables (FKEY to t_variable.id). All variables to be checked (aggregated class together with sub-classes) need to be passed.
 * Minimal amount of difference [%].
 * Whether include NULL difference -- indicating defined sub-classes missing in data.
Resulting table has following columns:
 * Plot. FKEY to f_p_plot.gid.
 * Auxiliary total attribute -- variable. FKEY to t_variable.id.
 * Aggregated class local density.
 * Sum of sub-classes local densities (belonging to aggregated class).
 * Attributes -- variables defined in hierarchy (v_variable_hierarchy_plot). Array of FKEYs to t_variable.id.
 * Attributes -- variables found in data (t_auxiliary_data). Array of FKEYs to t_variable.id.
 * Relative difference between aggregated class local densities and sum of sub-classes local densities.';

GRANT EXECUTE ON FUNCTION @extschema@.fn_add_plot_aux_attr TO PUBLIC;

-- </function>

-- <function name="fn_add_plot_target_attr" schema="extschema" src="functions/extschema/additivity/fn_add_plot_target_attr.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_add_plot_target_attr(integer)

-- DROP FUNCTION @extschema@.fn_add_plot_target_attr(integer[], integer[], double precision, boolean);
CREATE OR REPLACE FUNCTION @extschema@.fn_add_plot_target_attr(
	IN variables integer[],
	IN plots integer[] default NULL,
	IN refyearsets integer[] default NULL,
	IN min_diff double precision default 0.0, IN include_null_diff boolean default true
)
  RETURNS TABLE(
	plot			integer,
	reference_year_set	integer,
	variable		integer,
	ldsity			double precision,
	ldsity_sum		double precision,
	variables_def		integer[],
	variables_found		integer[],
	diff			double precision
) AS
$BODY$
DECLARE
	_complete_query text;
	_array_text_v text;
	_array_text_p text;
	_array_text_r text;

BEGIN
	--------------------------------QUERY--------------------------------
	_array_text_v := concat(quote_literal(variables::text), '::integer[]');
	if plots is not null then
		_array_text_p := concat(' AND f_p_plot.gid = ANY (', quote_literal(plots::text), '::integer[])');
	else
		_array_text_p := '';
	end if;

	if refyearsets is not null then
		_array_text_r := concat(' AND t_available_datasets.reference_year_set = ANY (', quote_literal(refyearsets::text), '::integer[])');
	else
		_array_text_r := '';
	end if;

	--raise notice 'variables: %',  _array_text_v;
	--raise notice 'plots: %',  _array_text_p;
	_complete_query := '
with w_plot_var as not materialized (
	select
		f_p_plot.gid as plot, t_available_datasets.variable, t_available_datasets.panel, t_available_datasets.reference_year_set,
		coalesce(t_target_data.value, 0) as value
	from sdesign.f_p_plot
	inner join sdesign.t_cluster on (f_p_plot.cluster = t_cluster.id)
	inner join sdesign.cm_cluster2panel_mapping on (t_cluster.id = cm_cluster2panel_mapping.cluster)
	inner join sdesign.t_panel on (cm_cluster2panel_mapping.panel = t_panel.id)
	inner join sdesign.t_cluster_configuration on (t_panel.cluster_configuration = t_cluster_configuration.id)
	inner join sdesign.cm_plot2cluster_config_mapping on (t_cluster_configuration.id = cm_plot2cluster_config_mapping.cluster_configuration and cm_plot2cluster_config_mapping.plot = f_p_plot.gid)
	inner join @extschema@.t_available_datasets on (t_panel.id = t_available_datasets.panel)
	inner join @extschema@.t_variable on (t_available_datasets.variable = t_variable.id and t_variable.auxiliary_variable_category is null)
	left join @extschema@.t_target_data on (
		f_p_plot.gid = t_target_data.plot
		and t_available_datasets.variable = t_target_data.variable
		and t_available_datasets.reference_year_set = t_target_data.reference_year_set
		and t_target_data.is_latest)
	WHERE 	t_available_datasets.variable = ANY (' || _array_text_v || ') 
			' || _array_text_p || '
			' || _array_text_r || '
)
, w_node_sum as (
	select
		plot,
		w_plot_var.panel,
		w_plot_var.reference_year_set,
		w_plot_var.variable,
		value as node_sum,
		v_variable_hierarchy_plot.node,
		v_variable_hierarchy_plot.edges as edges_def
	from w_plot_var
	inner join @extschema@.v_variable_hierarchy_plot on (v_variable_hierarchy_plot.node = w_plot_var.variable and
							v_variable_hierarchy_plot.panel = w_plot_var.panel and
							v_variable_hierarchy_plot.reference_year_set = w_plot_var.reference_year_set
	)
	where (v_variable_hierarchy_plot.edges <@ ' || _array_text_v || ')
)
, w_edge_sum as (
	select
		w_node_sum.plot,
		w_node_sum.reference_year_set,
		w_node_sum.node,
		w_node_sum.node_sum,
		w_node_sum.edges_def,
		array_agg(w_plot_var.variable order by w_plot_var.variable) as edges_found,
		sum(w_plot_var.value) as edges_sum
	from w_node_sum
	left join w_plot_var on (
		w_plot_var.plot = w_node_sum.plot
		and w_plot_var.panel = w_node_sum.panel
		and w_plot_var.reference_year_set = w_node_sum.reference_year_set
		and w_plot_var.variable = any(w_node_sum.edges_def))
	group by w_node_sum.plot, w_node_sum.reference_year_set,
		w_node_sum.node, w_node_sum.node_sum, w_node_sum.edges_def
)
, w_diff as (
	select
		plot,
		reference_year_set,
		node		as variable,
		node_sum	as ldsity,
		edges_sum	as ldsity_sum,
		edges_def	as variables_def,
		edges_found	as variables_found,
		case
			when edges_sum != 0.0 and node_sum = 0.0 then 1.0
			when node_sum = 0.0 and edges_sum = 0.0 then 0
			else abs( (node_sum - edges_sum) / node_sum )
		end as diff
	from w_edge_sum
)
select * from w_diff
where
	(' || include_null_diff || ' and ((diff >= ' || min_diff || ') or (diff is null)))
	or
	(not ' || include_null_diff || ' and (diff >= ' || min_diff || '))
order by diff desc
;
';

	--RAISE NOTICE '%', _complete_query;
	RETURN QUERY EXECUTE _complete_query;
END;
$BODY$
  LANGUAGE plpgsql STABLE
  COST 100
  ROWS 100000;

comment on function @extschema@.fn_add_plot_target_attr(integer[], integer[], integer[], double precision, boolean) is
'Function showing plot level target local density attribute additivity (aggregated class estimate should be equal to sum of sub-classes estimates) of target local densities. Hierarchy between aggregated classes and its sub-classes is defined in v_variable_hierarchy_plot.
Function input argument is:
 * Array of attributes -- variables (FKEY to t_variable.id). All variables to be checked (aggregated class together with sub-classes) need to be passed.
 * Array of plots -- plots (FKEY to f_p_plot.gid). If NULL all available plots are checked.
 * Array of reference year sets -- reference year sets (FKEY to t_reference_year_set.id). If NULL all available reference year sets are checked.
 * Minimal amount of difference [%].
 * Whether include NULL difference -- indicating defined sub-classes missing in data.
Resulting table has following columns:
 * Plot. FKEY to f_p_plot.gid.
 * Reference year set. FKEY to t_reference_year_set.id.
 * Target total attribute -- variable. FKEY to t_variable.id.
 * Aggregated class local density.
 * Sum of sub-classes local densities (belonging to aggregated class).
 * Attributes -- variables defined in hierarchy (v_variable_hierarchy_plot). Array of FKEYs to t_variable.id.
 * Attributes -- variables found in data (t_auxiliary_data). Array of FKEYs to t_variable.id.
 * Relative difference between aggregated class local densities and sum of sub-classes local densities.';

GRANT EXECUTE ON FUNCTION @extschema@.fn_add_plot_target_attr TO PUBLIC;

-- </function>

-- <function name="fn_add_aux_total_attr" schema="extschema" src="functions/extschema/additivity/fn_add_aux_total_attr.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_add_aux_total_attr(integer)

-- DROP FUNCTION @extschema@.fn_add_aux_total_attr(integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_add_aux_total_attr(
	IN variables integer[],
	IN min_diff double precision default 0.0, IN include_null_diff boolean default true
)
  RETURNS TABLE(
	estimation_cell		integer,
	variable		integer,
	aux_total		double precision,
	aux_total_sum		double precision,
	variables_def		integer[],
	variables_found		integer[],
	diff			double precision
) AS
$BODY$
DECLARE
	_complete_query text;
	_array_text text;
BEGIN
	--------------------------------QUERY--------------------------------
	_array_text := concat(quote_literal(variables::text), '::integer[]');
	--raise notice 'variables: %',  _array_text;
	_complete_query := '
with w_auxtotal_cell_var as not materialized (
	select
		t_aux_total.estimation_cell,
		t_aux_total.aux_total,
		t_aux_total.variable
	from @extschema@.t_aux_total
	where t_aux_total.is_latest
	and t_aux_total.variable = ANY (' || _array_text || ')
)
, w_node_sum as (
	select
		estimation_cell,
		variable,
		sum(aux_total) as node_sum,
		hierarchy.node,
		hierarchy.edges as edges_def
	from w_auxtotal_cell_var
	inner join @extschema@.v_variable_hierarchy_aux_total		as hierarchy on (hierarchy.node = w_auxtotal_cell_var.variable)
	group by estimation_cell, variable, node, edges
	order by estimation_cell, variable, node, edges
)
, w_edge_sum as (
	select
		w_node_sum.estimation_cell,
		w_node_sum.node,
		w_node_sum.node_sum,
		w_node_sum.edges_def,
		array_agg(w_auxtotal_cell_var.variable order by w_auxtotal_cell_var.variable) as edges_found,
		sum(w_auxtotal_cell_var.aux_total) as edges_sum
	from w_node_sum
	left join w_auxtotal_cell_var on (
		w_auxtotal_cell_var.estimation_cell = w_node_sum.estimation_cell
		and w_auxtotal_cell_var.variable = any(w_node_sum.edges_def))
	group by w_node_sum.estimation_cell, w_node_sum.node, w_node_sum.node_sum, w_node_sum.edges_def
)
, w_diff as (
	select
		estimation_cell,
		node			as variable,
		node_sum		as aux_total,
		edges_sum		as aux_total_sum,
		edges_def		as variables_def,
		edges_found		as variables_found,
		case
			when edges_sum != 0.0 and node_sum = 0.0 then 1.0
			when node_sum = 0.0 and edges_sum = 0.0 then 0
			else abs( (node_sum - edges_sum) / node_sum )
		end as diff
	from w_edge_sum
)
select * from w_diff
where
	(' || include_null_diff || ' and ((diff >= ' || min_diff || ') or (diff is null)))
	or
	(not ' || include_null_diff || ' and (diff >= ' || min_diff || '))
order by diff desc
;
';

	--RAISE NOTICE '%', _complete_query;
	RETURN QUERY EXECUTE _complete_query;
END;
$BODY$
  LANGUAGE plpgsql STABLE
  COST 100
  ROWS 100000;

comment on function @extschema@.fn_add_aux_total_attr(integer[], double precision, boolean) is
'Function showing auxiliary total attribute additivity (aggregated class estimate should be equal to sum of sub-classes estimates). Hierarchy between aggregated classes and its sub-classes is defined in v_variable_hierarchy_aux_total.
Function input argument is:
 * Array of attributes -- variables (FKEY to t_variable.id). All variables to be checked (aggregated class together with sub-classes) need to be passed.
 * Minimal amount of difference [%].
 * Whether include NULL difference -- indicating defined sub-classes missing in data.
Resulting table has following columns:
 * Auxiliary total estimation cell. FKEY to c_estimation_cell.id.
 * Auxiliary total attribute -- variable. FKEY to t_variable.id.
 * Aggregated class auxiliary total.
 * Sum of sub-classes auxiliary totals (belonging to aggregated class).
 * Attributes -- variables defined in hierarchy (v_variable_hierarchy_aux_total). Array of FKEYs to t_variable.id.
 * Attributes -- variables found in data (t_result). Array of FKEYs to t_variable.id.
 * Relative difference between aggregated class auxiliary total and sum of sub-classes auxiliary totals.';

GRANT EXECUTE ON FUNCTION @extschema@.fn_add_aux_total_attr TO PUBLIC;

-- </function>

-- <function name="fn_add_res_ratio_attr" schema="extschema" src="functions/extschema/additivity/fn_add_res_ratio_attr.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_add_res_ratio_attr(integer)

-- DROP FUNCTION @extschema@.fn_add_res_ratio_attr(integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_add_res_ratio_attr(
	IN est_confs integer[],
	IN min_diff double precision default 0.0, IN include_null_diff boolean default true
)
  RETURNS TABLE(
	estimation_cell		integer,
	aux_conf		integer,
	force_synthetic		boolean,
	estimate_conf		integer,
	denominator		integer,
	variable		integer,
	point_est		double precision,
	point_est_sum		double precision,
	variables_def		integer[],
	variables_found		integer[],
	estimate_confs_found	integer[],
	diff			double precision
) AS
$BODY$
DECLARE
	_complete_query text;
	_array_text text;
BEGIN
	--------------------------------QUERY--------------------------------
	_array_text := concat(quote_literal(est_confs::text), '::integer[]');
	--raise notice 'est_confs: %',  _array_text;
	_complete_query := '
with w_res_cell_var as not materialized (
	select
		t_result.estimate_conf,
		t_result.point,
		t_total_estimate_conf.estimation_cell,
		t_total_estimate_conf.variable as t_variable__id,
		t_total_estimate_conf.panel_refyearset_group,
		c_estimation_period.estimate_date_begin, c_estimation_period.estimate_date_end, 
		array_agg(t_panel_refyearset_group.panel order by panel) as panels,
		t_total_estimate_conf.aux_conf, force_synthetic, t_estimate_conf.denominator
	from @extschema@.t_result
	inner join @extschema@.t_estimate_conf ON t_estimate_conf.id = t_result.estimate_conf
	inner join @extschema@.t_total_estimate_conf ON t_total_estimate_conf.id = t_estimate_conf.total_estimate_conf
	inner join @extschema@.c_estimation_period ON c_estimation_period.id = t_total_estimate_conf.estimation_period
	inner join @extschema@.c_panel_refyearset_group ON c_panel_refyearset_group.id = t_total_estimate_conf.panel_refyearset_group
	inner join @extschema@.t_panel_refyearset_group ON t_panel_refyearset_group.panel_refyearset_group = c_panel_refyearset_group.id
	where (t_estimate_conf.estimate_type = 2)
	and (t_result.is_latest)
	and (t_result.estimate_conf = ANY (' || _array_text || '))
	group by 
		t_result.estimate_conf,
		t_result.point,
		t_total_estimate_conf.estimation_cell,
		t_total_estimate_conf.variable,
		t_total_estimate_conf.panel_refyearset_group,
		c_estimation_period.estimate_date_begin, c_estimation_period.estimate_date_end,
		t_total_estimate_conf.aux_conf, force_synthetic, t_estimate_conf.denominator
)
, w_node_sum as (
	select
		estimation_cell,
		w_res_cell_var.t_variable__id, aux_conf, force_synthetic, denominator,
		estimate_date_begin, estimate_date_end, panels,
		estimate_conf as node_estimate_confs,
		sum(point) as node_sum,
		hierarchy.node,
		hierarchy.edges as edges_def
	from w_res_cell_var
	inner join @extschema@.v_variable_hierarchy_res		as hierarchy on (hierarchy.node = w_res_cell_var.t_variable__id and hierarchy.panel_refyearset_group = w_res_cell_var.panel_refyearset_group)
	group by estimation_cell, w_res_cell_var.t_variable__id, aux_conf, force_synthetic, estimate_date_begin, estimate_date_end, panels, estimate_conf, node, edges_def, denominator
	order by estimation_cell, w_res_cell_var.t_variable__id, aux_conf, force_synthetic, estimate_date_begin, estimate_date_end, panels, estimate_conf, node, edges_def, denominator
)
, w_edge_sum as (
	select
		w_node_sum.estimation_cell,
		w_node_sum.aux_conf,
		w_node_sum.force_synthetic,
		w_node_sum.node_estimate_confs, w_node_sum.denominator,
		w_node_sum.estimate_date_begin, w_node_sum.estimate_date_end, w_node_sum.panels,
		w_node_sum.node,
		w_node_sum.node_sum,
		w_node_sum.edges_def,
		array_agg(w_res_cell_var.t_variable__id order by w_res_cell_var.t_variable__id) as edges_found,
		array_agg(w_res_cell_var.estimate_conf order by w_res_cell_var.estimate_conf) as edges_estimate_confs,
		sum(w_res_cell_var.point) as edges_sum
	from w_node_sum
	left join w_res_cell_var on (
		w_res_cell_var.estimation_cell = w_node_sum.estimation_cell
		and w_res_cell_var.estimate_date_begin = w_node_sum.estimate_date_begin
		and w_res_cell_var.estimate_date_end = w_node_sum.estimate_date_end
		and w_res_cell_var.panels = w_node_sum.panels
		and case when
			w_res_cell_var.aux_conf is null and w_node_sum.aux_conf is null then true
			else w_res_cell_var.aux_conf = w_node_sum.aux_conf end
		and case when
			w_res_cell_var.force_synthetic is null and w_node_sum.force_synthetic is null then true
			else w_res_cell_var.force_synthetic = w_node_sum.force_synthetic end
		and w_res_cell_var.t_variable__id = any(w_node_sum.edges_def)) and w_res_cell_var.denominator = w_node_sum.denominator
	group by w_node_sum.estimation_cell, 
		w_node_sum.aux_conf, w_node_sum.force_synthetic, w_node_sum.node_estimate_confs, w_node_sum.denominator,
		w_node_sum.estimate_date_begin, w_node_sum.estimate_date_end, w_node_sum.panels,
		w_node_sum.node, w_node_sum.node_sum, w_node_sum.edges_def
)
, w_diff as (
	select
		estimation_cell,
		aux_conf,
		force_synthetic,
		node_estimate_confs	as estimate_conf, denominator,
		node			as variable,
		node_sum		as point_est,
		edges_sum		as point_est_sum,
		edges_def		as variables_def,
		edges_found		as variables_found,
		edges_estimate_confs	as estimate_confs_found,
		case
			when edges_sum != 0.0 and node_sum = 0.0 then 1.0
			when node_sum = 0.0 and edges_sum = 0.0 then 0
			else abs( (node_sum - edges_sum) / node_sum )
		end as diff
	from w_edge_sum
)
select * from w_diff
where
	(' || include_null_diff || ' and ((diff >= ' || min_diff || ') or (diff is null)))
	or
	(not ' || include_null_diff || ' and (diff >= ' || min_diff || '))
order by diff desc
;
';

	--RAISE NOTICE '%', _complete_query;
	RETURN QUERY EXECUTE _complete_query;
END;
$BODY$
  LANGUAGE plpgsql STABLE
  COST 100
  ROWS 100000;

comment on function @extschema@.fn_add_res_ratio_attr(integer[], double precision, boolean) is
'Function showing ratio estimates attribute additivity (aggregated class estimate should be equal to sum of sub-classes estimates). Hierarchy between aggregated classes and its sub-classes is defined in v_variable_hierarchy_res.
Function input argument is:
 * Array of attributes -- variables (FKEY to t_variable.id). All variables to be checked (aggregated class together with sub-classes) need to be passed.
 * Minimal amount of difference [%].
 * Whether include NULL difference -- indicating defined sub-classes missing in data.
Resulting table has following columns:
 * Estimate estimation cell. FKEY to c_estimation_cell.id.
 * Estimate auxiliary configuration. FKEY to t_aux_conf.id.
 * Parameter showing whether estimate is forced to be synthetic.
 * Estimate configuration id. FKEY to t_estimate_conf.id.
 * Estimate configuration id of denominator. FKEY to t_estimate_conf.id.
 * Estimate attribute -- variable. FKEY to t_variable.id.
 * Aggregated class point estimate.
 * Sum of sub-classes point estimates (belonging to aggregated class).
 * Attributes -- variables defined in hierarchy (v_variable_hierarchy_res). Array of FKEYs to t_variable.id.
 * Attributes -- variables found in data (t_result). Array of FKEYs to t_variable.id.
 * Estimate configurations found in data (t_result). Array of FKEYs to t_estimate_donf.id.
 * Relative difference between aggregated class estimate and sum of sub-classes estimates.';

GRANT EXECUTE ON FUNCTION @extschema@.fn_add_res_ratio_attr TO PUBLIC;

-- </function>

-- <function name="fn_add_res_total_attr" schema="extschema" src="functions/extschema/additivity/fn_add_res_total_attr.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_add_res_total_attr(integer)

-- DROP FUNCTION @extschema@.fn_add_res_total_attr(integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_add_res_total_attr(
	IN est_confs integer[],
	IN min_diff double precision default 0.0, IN include_null_diff boolean default true
)
  RETURNS TABLE(
	estimation_cell		integer,
	aux_conf		integer,
	force_synthetic		boolean,
	estimate_conf		integer,
	variable		integer,
	point_est		double precision,
	point_est_sum		double precision,
	variables_def		integer[],
	variables_found		integer[],
	estimate_confs_found	integer[],
	diff			double precision
) AS
$BODY$
DECLARE
	_complete_query text;
	_array_text text;
BEGIN
	--------------------------------QUERY--------------------------------
	_array_text := concat(quote_literal(est_confs::text), '::integer[]');
	--raise notice 'est_confs: %',  _array_text;
	_complete_query := '
with w_res_cell_var as not materialized (
	select
		t_result.estimate_conf,
		t_result.point,
		t_total_estimate_conf.estimation_cell,
		t_total_estimate_conf.variable as t_variable__id,
		t_total_estimate_conf.panel_refyearset_group,
		c_estimation_period.estimate_date_begin, c_estimation_period.estimate_date_end, 
		array_agg(t_panel_refyearset_group.panel order by panel) as panels,
		t_total_estimate_conf.aux_conf, force_synthetic
	from @extschema@.t_result
	inner join @extschema@.t_estimate_conf ON t_estimate_conf.id = t_result.estimate_conf
	inner join @extschema@.t_total_estimate_conf ON t_total_estimate_conf.id = t_estimate_conf.total_estimate_conf
	inner join @extschema@.c_estimation_period ON c_estimation_period.id = t_total_estimate_conf.estimation_period
	inner join @extschema@.c_panel_refyearset_group ON c_panel_refyearset_group.id = t_total_estimate_conf.panel_refyearset_group
	inner join @extschema@.t_panel_refyearset_group ON t_panel_refyearset_group.panel_refyearset_group = c_panel_refyearset_group.id
	where (t_estimate_conf.estimate_type = 1)
	and (t_result.is_latest)
	and (t_result.estimate_conf = ANY (' || _array_text || '))
	group by 
		t_result.estimate_conf,
		t_result.point,
		t_total_estimate_conf.estimation_cell,
		t_total_estimate_conf.variable,
		t_total_estimate_conf.panel_refyearset_group,
		c_estimation_period.estimate_date_begin, c_estimation_period.estimate_date_end, 
		t_total_estimate_conf.aux_conf, force_synthetic
)
, w_node_sum as (
	select
		estimation_cell,
		w_res_cell_var.t_variable__id, aux_conf, force_synthetic,
		estimate_date_begin, estimate_date_end, panels,
		estimate_conf as node_estimate_confs,
		sum(point) as node_sum,
		hierarchy.node,
		hierarchy.edges as edges_def
	from w_res_cell_var
	inner join @extschema@.v_variable_hierarchy_res		as hierarchy on (hierarchy.node = w_res_cell_var.t_variable__id and hierarchy.panel_refyearset_group = w_res_cell_var.panel_refyearset_group)
	group by estimation_cell, w_res_cell_var.t_variable__id, aux_conf, force_synthetic, estimate_date_begin, estimate_date_end, panels, estimate_conf, node, edges_def
	order by estimation_cell, w_res_cell_var.t_variable__id, aux_conf, force_synthetic, estimate_date_begin, estimate_date_end, panels, estimate_conf, node, edges_def
)
, w_edge_sum as (
	select
		w_node_sum.estimation_cell,
		w_node_sum.aux_conf,
		w_node_sum.force_synthetic,
		w_node_sum.node_estimate_confs,
		w_node_sum.estimate_date_begin, w_node_sum.estimate_date_end, w_node_sum.panels,
		w_node_sum.node,
		w_node_sum.node_sum,
		w_node_sum.edges_def,
		array_agg(w_res_cell_var.t_variable__id order by w_res_cell_var.t_variable__id) as edges_found,
		array_agg(w_res_cell_var.estimate_conf order by w_res_cell_var.estimate_conf) as edges_estimate_confs,
		sum(w_res_cell_var.point) as edges_sum
	from w_node_sum
	left join w_res_cell_var on (
		w_res_cell_var.estimation_cell = w_node_sum.estimation_cell
		and w_res_cell_var.estimate_date_begin = w_node_sum.estimate_date_begin
		and w_res_cell_var.estimate_date_end = w_node_sum.estimate_date_end
		and w_res_cell_var.panels = w_node_sum.panels
		and case when
			w_res_cell_var.aux_conf is null and w_node_sum.aux_conf is null then true
			else w_res_cell_var.aux_conf = w_node_sum.aux_conf end
		and case when
			w_res_cell_var.force_synthetic is null and w_node_sum.force_synthetic is null then true
			else w_res_cell_var.force_synthetic = w_node_sum.force_synthetic end
		and w_res_cell_var.t_variable__id = any(w_node_sum.edges_def))
	group by w_node_sum.estimation_cell, 
		w_node_sum.aux_conf, w_node_sum.force_synthetic, w_node_sum.node_estimate_confs,
		w_node_sum.estimate_date_begin, w_node_sum.estimate_date_end, w_node_sum.panels,
		w_node_sum.node, w_node_sum.node_sum, w_node_sum.edges_def
)
, w_diff as (
	select
		estimation_cell,
		aux_conf,
		force_synthetic,
		node_estimate_confs	as estimate_conf,
		node			as variable,
		node_sum		as point_est,
		edges_sum		as point_est_sum,
		edges_def		as variables_def,
		edges_found		as variables_found,
		edges_estimate_confs	as estimate_confs_found,
		case
			when edges_sum != 0.0 and node_sum = 0.0 then 1.0
			when node_sum = 0.0 and edges_sum = 0.0 then 0
			else abs( (node_sum - edges_sum) / node_sum )
		end as diff
	from w_edge_sum
)
select * from w_diff
where
	(' || include_null_diff || ' and ((diff >= ' || min_diff || ') or (diff is null)))
	or
	(not ' || include_null_diff || ' and (diff >= ' || min_diff || '))
order by diff desc
;
';

	--RAISE NOTICE '%', _complete_query;
	RETURN QUERY EXECUTE _complete_query;
END;
$BODY$
  LANGUAGE plpgsql STABLE
  COST 100
  ROWS 100000;

comment on function @extschema@.fn_add_res_total_attr(integer[], double precision, boolean) is
	'Function showing total estimates attribute additivity (aggregated class estimate should be equal to sum of sub-classes estimates). Hierarchy between aggregated classes and its sub-classes is defined in v_variable_hierarchy_res.
Function input argument is:
 * Array of estimate configuration ids. FKEY to t_estimate_conf.id. All est. conf. ids to be checked (aggregated class together with sub-classes) need to be passed.
 * Minimal amount of difference [%].
 * Whether include NULL difference -- indicating defined sub-classes missing in data.
Resulting table has following columns:
 * Estimate estimation cell. FKEY to c_estimation_cell.id.
 * Estimate auxiliary configuration. FKEY to t_aux_conf.id.
 * Parameter showing whether estimate is forced to be synthetic.
 * Estimate configuration id. FKEY to t_estimate_conf.id.
 * Estimate attribute -- variable. FKEY to t_variable.id.
 * Aggregated class point estimate.
 * Sum of sub-classes point estimates (belonging to aggregated class).
 * Attributes -- variables defined in hierarchy (v_variable_hierarchy_res). Array of FKEYs to t_variable.id.
 * Attributes -- variables found in data (t_result). Array of FKEYs to t_variable.id.
 * Estimate configurations found in data (t_result). Array of FKEYs to t_estimate_donf.id.
 * Relative difference between aggregated class estimate and sum of sub-classes estimates.';

GRANT EXECUTE ON FUNCTION @extschema@.fn_add_res_total_attr TO PUBLIC;

-- </function>


-- <function name="fn_etl_import_variable_hierarchy" schema="extschema" src="functions/extschema/etl/fn_etl_import_variable_hierarchy.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_import_variable_hierarchy(json)

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_import_variable_hierarchy(json) CASCADE;

create or replace function @extschema@.fn_etl_import_variable_hierarchy
(
	_variables	json
)
returns text
as
$$
declare
		_max_id_tvh		integer;
		_res			text;
begin
		if _variables is null
		then
			raise exception 'Error 01: fn_etl_import_variable_hierarchy: Input argument _variables must not by NULL!';
		end if;
	
		_max_id_tvh := (select coalesce(max(id),0) from @extschema@.t_variable_hierarchy);
	
		with
		w1 as	(
				select json_array_elements(_variables) as s
			)
		,w2a as	(
				select
						(s->>'target_variable')::integer					as target_variable,
						(s->>'sub_population_category')::integer		as sub_population_category,
						(s->>'area_domain_category')::integer			as area_domain_category,
						(s->>'sub_population_category_superior')::integer	as sub_population_category_superior,
						(s->>'area_domain_category_superior')::integer		as area_domain_category_superior
				from w1
			)
		,w2 as	(
			-- ignore panel and reference_year_set information, because it will be delivered through t_available_datasets later in ETL process
			select * from w2a
			group by target_variable, sub_population_category, area_domain_category, sub_population_category_superior, area_domain_category_superior
			order by target_variable, sub_population_category, area_domain_category, sub_population_category_superior, area_domain_category_superior
			)
		,w3 as	(
				select
						t_variable.id,
						t_variable.target_variable,
						case when t_variable.sub_population_category is null then 0 else t_variable.sub_population_category end as sub_population_category,
						case when t_variable.area_domain_category is null then 0 else t_variable.area_domain_category end as area_domain_category
				from
						@extschema@.t_variable
				where
						t_variable.target_variable = (select distinct w2.target_variable from w2)
			)
		,w4 as	(
				select
						w2.*,
						w3.id as variable,
						w3_sup.id as variable_superior
				from
						w2
				inner join w3		on (
					(w2.sub_population_category = w3.sub_population_category) and
					(w2.area_domain_category = w3.area_domain_category)
				)
				inner join w3 as w3_sup	on (
					(w2.sub_population_category_superior = w3_sup.sub_population_category) and
					(w2.area_domain_category_superior = w3_sup.area_domain_category))
			)
		,w5 as	(
				select w4.variable, w4.variable_superior from w4 except
				select tvh.variable, tvh.variable_superior from @extschema@.t_variable_hierarchy as tvh
			)
		insert into @extschema@.t_variable_hierarchy(variable,variable_superior)
		select variable, variable_superior from w5 order by variable, variable_superior;
				
		_res := concat('The ',(select count(*) from @extschema@.t_variable_hierarchy where id > _max_id_tvh),' new rows were inserted into t_variable_hierarchy.');
	
		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_import_variable_hierarchy(json) IS
'The function solves ETL proces for t_variable_hierarchy table.';

grant execute on function @extschema@.fn_etl_import_variable_hierarchy(json) to public;

-- </function>
