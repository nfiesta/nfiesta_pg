--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

alter table @extschema@.t_total_estimate_conf rename column target_variable to variable;

drop view if exists @extschema@.v_conf_overview;
-- <view_name="v_conf_overview" function_schema="extschema" src="views/extschema/v_conf_overview.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- drop view if exists @extschema@.v_conf_overview;
create or replace view @extschema@.v_conf_overview as (
with w_confs AS MATERIALIZED (
	select
		t_estimate_conf.id as estimate_conf,
		t_total_estimate_conf.id as total_estimate_conf,
		t_total_estimate_conf.panel_refyearset_group,
		t_aux_conf.id as aux_conf,
		t_aux_conf.panel_refyearset_group AS aux_conf_prg,
		t_total_estimate_conf_denom.id as total_estimate_conf__denom,
		t_aux_conf_denom.id as aux_conf__denom,
		case 	when 
				t_total_estimate_conf.id is not null and 
				t_aux_conf.id is null and 
				t_total_estimate_conf_denom.id is null and 
				t_aux_conf_denom.id is null 
			then '1p_total'
			when 
				t_total_estimate_conf.id is not null and 
				t_aux_conf.id is null and 
				t_total_estimate_conf_denom.id is not null and 
				t_aux_conf_denom.id is null 
			then '1p1p_ratio'
			when 
				t_total_estimate_conf.id is not null and 
				t_aux_conf.id is not null and 
				t_total_estimate_conf_denom.id is null and 
				t_aux_conf_denom.id is null 
			then '2p_total'
			when 
				t_total_estimate_conf.id is not null and 
				t_aux_conf.id is not null and 
				t_total_estimate_conf_denom.id is not null and 
				t_aux_conf_denom.id is null 
			then '2p1p_ratio'
			when 
				t_total_estimate_conf.id is not null and 
				t_aux_conf.id is null and 
				t_total_estimate_conf_denom.id is not null and 
				t_aux_conf_denom.id is not null 
			then '1p2p_ratio'
			when 
				t_total_estimate_conf.id is not null and 
				t_aux_conf.id is not null and 
				t_total_estimate_conf_denom.id is not null and 
				t_aux_conf_denom.id is not null 
			then '2p2p_ratio'
			else 'unknown'
		end as estimate_type_str,
		coalesce(t_aux_conf.sigma, t_aux_conf_denom.sigma) as sigma,
		coalesce(t_total_estimate_conf.force_synthetic, t_total_estimate_conf_denom.force_synthetic) as force_synthetic,
------------------------additional info begin-----------------------------------
		coalesce(t_aux_conf.param_area, t_aux_conf_denom.param_area) as param_area, 
		coalesce(f_a_param_area.param_area_code, f_a_param_area_denom.param_area_code) as param_area_code,
		coalesce(t_aux_conf.model, t_aux_conf_denom.model) as model, 
		coalesce(t_model.description, t_model_denom.description) as model_description,
		t_total_estimate_conf.estimation_cell, c_estimation_cell.label as estimation_cell_label, c_estimation_cell.estimation_cell_collection, --f_a_cell.geom,
		t_variable.id as variable, t_variable.target_variable, c_target_variable.etl_join_id as target_variable_label, 
		t_variable.sub_population_category, c_sub_population_category.label as sub_population_category_label,
		t_variable.area_domain_category, c_area_domain_category.label as area_domain_category_label,
		c_estimation_period.estimate_date_begin, c_estimation_period.estimate_date_end
------------------------additional info end-------------------------------------
	from @extschema@.t_estimate_conf
        inner join @extschema@.t_total_estimate_conf ON t_total_estimate_conf.id = t_estimate_conf.total_estimate_conf
	inner join @extschema@.c_estimation_period ON c_estimation_period.id = t_total_estimate_conf.estimation_period
	left join @extschema@.t_aux_conf ON t_aux_conf.id = t_total_estimate_conf.aux_conf
        left join @extschema@.t_total_estimate_conf as t_total_estimate_conf_denom ON t_total_estimate_conf_denom.id = t_estimate_conf.denominator
	left join @extschema@.t_aux_conf as t_aux_conf_denom ON t_aux_conf_denom.id = t_total_estimate_conf_denom.aux_conf
------------------------additional info begin-----------------------------------
	left join @extschema@.f_a_param_area on (t_aux_conf.param_area = f_a_param_area.gid)
	left join @extschema@.t_model on (t_aux_conf.model = t_model.id)
	left join @extschema@.f_a_param_area as f_a_param_area_denom on (t_aux_conf_denom.param_area = f_a_param_area_denom.gid)
	left join @extschema@.t_model as t_model_denom on (t_aux_conf_denom.model = t_model_denom.id)
	inner join @extschema@.c_estimation_cell on (t_total_estimate_conf.estimation_cell = c_estimation_cell.id)
	inner join @extschema@.t_variable on (t_total_estimate_conf.variable = t_variable.id)
	inner join @extschema@.c_target_variable on (t_variable.target_variable = c_target_variable.id)
	left join @extschema@.c_sub_population_category on (t_variable.sub_population_category = c_sub_population_category.id)
	left join @extschema@.c_area_domain_category on (t_variable.area_domain_category = c_area_domain_category.id)
------------------------additional info end-------------------------------------
	order by t_estimate_conf.id
)
, w_confs_2nd_panels AS MATERIALIZED (
	select w_confs.*,
		array_agg(t_panel_refyearset_group.panel order by t_panel_refyearset_group.panel) as panels,
		array_agg(t_panel_refyearset_group.reference_year_set order by t_panel_refyearset_group.panel) as ref_year_sets
	from w_confs
	inner join @extschema@.t_panel_refyearset_group on (t_panel_refyearset_group.panel_refyearset_group = w_confs.panel_refyearset_group)
	group by w_confs.estimate_conf, w_confs.total_estimate_conf, w_confs.panel_refyearset_group, 
			w_confs.aux_conf, w_confs.aux_conf_prg, w_confs.total_estimate_conf__denom, w_confs.aux_conf__denom,
			w_confs.estimate_type_str, w_confs.sigma, w_confs.force_synthetic,
			w_confs.param_area, w_confs.param_area_code,
			w_confs.model, w_confs.model_description,
			w_confs.estimation_cell, w_confs.estimation_cell_label, w_confs.estimation_cell_collection,
			w_confs.variable, w_confs.target_variable, w_confs.target_variable_label,
			w_confs.sub_population_category, w_confs.sub_population_category_label, w_confs.area_domain_category, w_confs.area_domain_category_label,
			w_confs.estimate_date_begin, w_confs.estimate_date_end
)
	select w_confs_2nd_panels.*,
		array_agg(t_panel_refyearset_group.panel order by t_panel_refyearset_group.panel) as aux_conf_panels
	from w_confs_2nd_panels
	left join @extschema@.t_panel_refyearset_group on (t_panel_refyearset_group.panel_refyearset_group = w_confs_2nd_panels.aux_conf_prg)
	group by w_confs_2nd_panels.estimate_conf, w_confs_2nd_panels.total_estimate_conf, w_confs_2nd_panels.panel_refyearset_group, 
			w_confs_2nd_panels.aux_conf, w_confs_2nd_panels.aux_conf_prg, w_confs_2nd_panels.total_estimate_conf__denom, w_confs_2nd_panels.aux_conf__denom,
			w_confs_2nd_panels.estimate_type_str, w_confs_2nd_panels.sigma, w_confs_2nd_panels.force_synthetic,
			w_confs_2nd_panels.param_area, w_confs_2nd_panels.param_area_code,
			w_confs_2nd_panels.model, w_confs_2nd_panels.model_description,
			w_confs_2nd_panels.estimation_cell, w_confs_2nd_panels.estimation_cell_label, w_confs_2nd_panels.estimation_cell_collection, w_confs_2nd_panels.variable, w_confs_2nd_panels.target_variable, w_confs_2nd_panels.target_variable_label,
			w_confs_2nd_panels.sub_population_category, w_confs_2nd_panels.sub_population_category_label, w_confs_2nd_panels.area_domain_category, w_confs_2nd_panels.area_domain_category_label,
			w_confs_2nd_panels.estimate_date_begin, w_confs_2nd_panels.estimate_date_end,
			w_confs_2nd_panels.panels, w_confs_2nd_panels.ref_year_sets

);
--select * from @extschema@.v_conf_overview;

GRANT SELECT ON TABLE @extschema@.v_conf_overview TO PUBLIC;

-- </view>

-- <function_name="fn_add_res_ratio_attr" function_schema="extschema" src="functions/extschema/additivity/fn_add_res_ratio_attr.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_add_res_ratio_attr(integer)

-- DROP FUNCTION @extschema@.fn_add_res_ratio_attr(integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_add_res_ratio_attr(
	IN est_confs integer[],
	IN min_diff double precision default 0.0, IN include_null_diff boolean default true
)
  RETURNS TABLE(
	estimation_cell		integer,
	aux_conf		integer,
	force_synthetic		boolean,
	estimate_conf		integer,
	denominator		integer,
	variable		integer,
	point_est		double precision,
	point_est_sum		double precision,
	variables_def		integer[],
	variables_found		integer[],
	estimate_confs_found	integer[],
	diff			double precision
) AS
$BODY$
DECLARE
	_complete_query text;
	_array_text text;
BEGIN
	--------------------------------QUERY--------------------------------
	_array_text := concat(quote_literal(est_confs::text), '::integer[]');
	--raise notice 'est_confs: %',  _array_text;
	_complete_query := '
with w_res_cell_var as not materialized (
	select
		t_result.estimate_conf,
		t_result.point,
		t_total_estimate_conf.estimation_cell,
		t_total_estimate_conf.variable as t_variable__id,
		c_estimation_period.estimate_date_begin, c_estimation_period.estimate_date_end, 
		array_agg(t_panel_refyearset_group.panel order by panel) as panels,
		t_total_estimate_conf.aux_conf, force_synthetic, t_estimate_conf.denominator
	from @extschema@.t_result
	inner join @extschema@.t_estimate_conf ON t_estimate_conf.id = t_result.estimate_conf
	inner join @extschema@.t_total_estimate_conf ON t_total_estimate_conf.id = t_estimate_conf.total_estimate_conf
	inner join @extschema@.c_estimation_period ON c_estimation_period.id = t_total_estimate_conf.estimation_period
	inner join @extschema@.c_panel_refyearset_group ON c_panel_refyearset_group.id = t_total_estimate_conf.panel_refyearset_group
	inner join @extschema@.t_panel_refyearset_group ON t_panel_refyearset_group.panel_refyearset_group = c_panel_refyearset_group.id
	where (t_estimate_conf.estimate_type = 2)
	and (t_result.is_latest)
	and (t_result.estimate_conf = ANY (' || _array_text || '))
	group by 
		t_result.estimate_conf,
		t_result.point,
		t_total_estimate_conf.estimation_cell,
		t_total_estimate_conf.variable,
		c_estimation_period.estimate_date_begin, c_estimation_period.estimate_date_end,
		t_total_estimate_conf.aux_conf, force_synthetic, t_estimate_conf.denominator
)
, w_node_sum as (
	select
		estimation_cell,
		w_res_cell_var.t_variable__id, aux_conf, force_synthetic, denominator,
		estimate_date_begin, estimate_date_end, panels,
		estimate_conf as node_estimate_confs,
		sum(point) as node_sum,
		hierarchy.node,
		hierarchy.edges as edges_def
	from w_res_cell_var
	inner join @extschema@.v_variable_hierarchy 	as hierarchy on (hierarchy.node = w_res_cell_var.t_variable__id)
	group by estimation_cell, w_res_cell_var.t_variable__id, aux_conf, force_synthetic, estimate_date_begin, estimate_date_end, panels, estimate_conf, node, edges_def, denominator
	order by estimation_cell, w_res_cell_var.t_variable__id, aux_conf, force_synthetic, estimate_date_begin, estimate_date_end, panels, estimate_conf, node, edges_def, denominator
)
, w_edge_sum as (
	select
		w_node_sum.estimation_cell,
		w_node_sum.aux_conf,
		w_node_sum.force_synthetic,
		w_node_sum.node_estimate_confs, w_node_sum.denominator,
		w_node_sum.estimate_date_begin, w_node_sum.estimate_date_end, w_node_sum.panels,
		w_node_sum.node,
		w_node_sum.node_sum,
		w_node_sum.edges_def,
		array_agg(w_res_cell_var.t_variable__id order by w_res_cell_var.t_variable__id) as edges_found,
		array_agg(w_res_cell_var.estimate_conf order by w_res_cell_var.estimate_conf) as edges_estimate_confs,
		sum(w_res_cell_var.point) as edges_sum
	from w_node_sum
	left join w_res_cell_var on (
		w_res_cell_var.estimation_cell = w_node_sum.estimation_cell
		and w_res_cell_var.estimate_date_begin = w_node_sum.estimate_date_begin
		and w_res_cell_var.estimate_date_end = w_node_sum.estimate_date_end
		and w_res_cell_var.panels = w_node_sum.panels
		and case when
			w_res_cell_var.aux_conf is null and w_node_sum.aux_conf is null then true
			else w_res_cell_var.aux_conf = w_node_sum.aux_conf end
		and case when
			w_res_cell_var.force_synthetic is null and w_node_sum.force_synthetic is null then true
			else w_res_cell_var.force_synthetic = w_node_sum.force_synthetic end
		and w_res_cell_var.t_variable__id = any(w_node_sum.edges_def)) and w_res_cell_var.denominator = w_node_sum.denominator
	group by w_node_sum.estimation_cell, 
		w_node_sum.aux_conf, w_node_sum.force_synthetic, w_node_sum.node_estimate_confs, w_node_sum.denominator,
		w_node_sum.estimate_date_begin, w_node_sum.estimate_date_end, w_node_sum.panels,
		w_node_sum.node, w_node_sum.node_sum, w_node_sum.edges_def
)
, w_diff as (
	select
		estimation_cell,
		aux_conf,
		force_synthetic,
		node_estimate_confs	as estimate_conf, denominator,
		node			as variable,
		node_sum		as point_est,
		edges_sum		as point_est_sum,
		edges_def		as variables_def,
		edges_found		as variables_found,
		edges_estimate_confs	as estimate_confs_found,
		case
			when edges_sum != 0.0 and node_sum = 0.0 then 1.0
			when node_sum = 0.0 and edges_sum = 0.0 then 0
			else abs( (node_sum - edges_sum) / node_sum )
		end as diff
	from w_edge_sum
)
select * from w_diff
where
	(' || include_null_diff || ' and ((diff >= ' || min_diff || ') or (diff is null)))
	or
	(not ' || include_null_diff || ' and (diff >= ' || min_diff || '))
order by diff desc
;
';

	--RAISE NOTICE '%', _complete_query;
	RETURN QUERY EXECUTE _complete_query;
END;
$BODY$
  LANGUAGE plpgsql STABLE
  COST 100
  ROWS 100000;

comment on function @extschema@.fn_add_res_ratio_attr(integer[], double precision, boolean) is
'Function showing ratio estimates attribute additivity (aggregated class estimate should be equal to sum of sub-classes estimates). Hierarchy between aggregated classes and its sub-classes is defined in v_variable_hierarchy.
Function input argument is:
 * Array of attributes -- variables (FKEY to t_variable.id). All variables to be checked (aggregated class together with sub-classes) need to be passed.
 * Minimal amount of difference [%].
 * Whether include NULL difference -- indicating defined sub-classes missing in data.
Resulting table has following columns:
 * Estimate estimation cell. FKEY to c_estimation_cell.id.
 * Estimate auxiliary configuration. FKEY to t_aux_conf.id.
 * Parameter showing whether estimate is forced to be synthetic.
 * Estimate configuration id. FKEY to t_estimate_conf.id.
 * Estimate configuration id of denominator. FKEY to t_estimate_conf.id.
 * Estimate attribute -- variable. FKEY to t_variable.id.
 * Aggregated class point estimate.
 * Sum of sub-classes point estimates (belonging to aggregated class).
 * Attributes -- variables defined in hierarchy (v_variable_hierarchy). Array of FKEYs to t_variable.id.
 * Attributes -- variables found in data (t_result). Array of FKEYs to t_variable.id.
 * Estimate configurations found in data (t_result). Array of FKEYs to t_estimate_donf.id.
 * Relative difference between aggregated class estimate and sum of sub-classes estimates.';

GRANT EXECUTE ON FUNCTION @extschema@.fn_add_res_ratio_attr TO PUBLIC;

-- </function>

-- <function_name="fn_add_res_total_attr" function_schema="extschema" src="functions/extschema/additivity/fn_add_res_total_attr.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_add_res_total_attr(integer)

-- DROP FUNCTION @extschema@.fn_add_res_total_attr(integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_add_res_total_attr(
	IN est_confs integer[],
	IN min_diff double precision default 0.0, IN include_null_diff boolean default true
)
  RETURNS TABLE(
	estimation_cell		integer,
	aux_conf		integer,
	force_synthetic		boolean,
	estimate_conf		integer,
	variable		integer,
	point_est		double precision,
	point_est_sum		double precision,
	variables_def		integer[],
	variables_found		integer[],
	estimate_confs_found	integer[],
	diff			double precision
) AS
$BODY$
DECLARE
	_complete_query text;
	_array_text text;
BEGIN
	--------------------------------QUERY--------------------------------
	_array_text := concat(quote_literal(est_confs::text), '::integer[]');
	--raise notice 'est_confs: %',  _array_text;
	_complete_query := '
with w_res_cell_var as not materialized (
	select
		t_result.estimate_conf,
		t_result.point,
		t_total_estimate_conf.estimation_cell,
		t_total_estimate_conf.variable as t_variable__id,
		c_estimation_period.estimate_date_begin, c_estimation_period.estimate_date_end, 
		array_agg(t_panel_refyearset_group.panel order by panel) as panels,
		t_total_estimate_conf.aux_conf, force_synthetic
	from @extschema@.t_result
	inner join @extschema@.t_estimate_conf ON t_estimate_conf.id = t_result.estimate_conf
	inner join @extschema@.t_total_estimate_conf ON t_total_estimate_conf.id = t_estimate_conf.total_estimate_conf
	inner join @extschema@.c_estimation_period ON c_estimation_period.id = t_total_estimate_conf.estimation_period
	inner join @extschema@.c_panel_refyearset_group ON c_panel_refyearset_group.id = t_total_estimate_conf.panel_refyearset_group
	inner join @extschema@.t_panel_refyearset_group ON t_panel_refyearset_group.panel_refyearset_group = c_panel_refyearset_group.id
	where (t_estimate_conf.estimate_type = 1)
	and (t_result.is_latest)
	and (t_result.estimate_conf = ANY (' || _array_text || '))
	group by 
		t_result.estimate_conf,
		t_result.point,
		t_total_estimate_conf.estimation_cell,
		t_total_estimate_conf.variable,
		c_estimation_period.estimate_date_begin, c_estimation_period.estimate_date_end, 
		t_total_estimate_conf.aux_conf, force_synthetic
)
, w_node_sum as (
	select
		estimation_cell,
		w_res_cell_var.t_variable__id, aux_conf, force_synthetic,
		estimate_date_begin, estimate_date_end, panels,
		estimate_conf as node_estimate_confs,
		sum(point) as node_sum,
		hierarchy.node,
		hierarchy.edges as edges_def
	from w_res_cell_var
	inner join @extschema@.v_variable_hierarchy		as hierarchy on (hierarchy.node = w_res_cell_var.t_variable__id)
	group by estimation_cell, w_res_cell_var.t_variable__id, aux_conf, force_synthetic, estimate_date_begin, estimate_date_end, panels, estimate_conf, node, edges_def
	order by estimation_cell, w_res_cell_var.t_variable__id, aux_conf, force_synthetic, estimate_date_begin, estimate_date_end, panels, estimate_conf, node, edges_def
)
, w_edge_sum as (
	select
		w_node_sum.estimation_cell,
		w_node_sum.aux_conf,
		w_node_sum.force_synthetic,
		w_node_sum.node_estimate_confs,
		w_node_sum.estimate_date_begin, w_node_sum.estimate_date_end, w_node_sum.panels,
		w_node_sum.node,
		w_node_sum.node_sum,
		w_node_sum.edges_def,
		array_agg(w_res_cell_var.t_variable__id order by w_res_cell_var.t_variable__id) as edges_found,
		array_agg(w_res_cell_var.estimate_conf order by w_res_cell_var.estimate_conf) as edges_estimate_confs,
		sum(w_res_cell_var.point) as edges_sum
	from w_node_sum
	left join w_res_cell_var on (
		w_res_cell_var.estimation_cell = w_node_sum.estimation_cell
		and w_res_cell_var.estimate_date_begin = w_node_sum.estimate_date_begin
		and w_res_cell_var.estimate_date_end = w_node_sum.estimate_date_end
		and w_res_cell_var.panels = w_node_sum.panels
		and case when
			w_res_cell_var.aux_conf is null and w_node_sum.aux_conf is null then true
			else w_res_cell_var.aux_conf = w_node_sum.aux_conf end
		and case when
			w_res_cell_var.force_synthetic is null and w_node_sum.force_synthetic is null then true
			else w_res_cell_var.force_synthetic = w_node_sum.force_synthetic end
		and w_res_cell_var.t_variable__id = any(w_node_sum.edges_def))
	group by w_node_sum.estimation_cell, 
		w_node_sum.aux_conf, w_node_sum.force_synthetic, w_node_sum.node_estimate_confs,
		w_node_sum.estimate_date_begin, w_node_sum.estimate_date_end, w_node_sum.panels,
		w_node_sum.node, w_node_sum.node_sum, w_node_sum.edges_def
)
, w_diff as (
	select
		estimation_cell,
		aux_conf,
		force_synthetic,
		node_estimate_confs	as estimate_conf,
		node			as variable,
		node_sum		as point_est,
		edges_sum		as point_est_sum,
		edges_def		as variables_def,
		edges_found		as variables_found,
		edges_estimate_confs	as estimate_confs_found,
		case
			when edges_sum != 0.0 and node_sum = 0.0 then 1.0
			when node_sum = 0.0 and edges_sum = 0.0 then 0
			else abs( (node_sum - edges_sum) / node_sum )
		end as diff
	from w_edge_sum
)
select * from w_diff
where
	(' || include_null_diff || ' and ((diff >= ' || min_diff || ') or (diff is null)))
	or
	(not ' || include_null_diff || ' and (diff >= ' || min_diff || '))
order by diff desc
;
';

	--RAISE NOTICE '%', _complete_query;
	RETURN QUERY EXECUTE _complete_query;
END;
$BODY$
  LANGUAGE plpgsql STABLE
  COST 100
  ROWS 100000;

comment on function @extschema@.fn_add_res_total_attr(integer[], double precision, boolean) is
	'Function showing total estimates attribute additivity (aggregated class estimate should be equal to sum of sub-classes estimates). Hierarchy between aggregated classes and its sub-classes is defined in v_variable_hierarchy.
Function input argument is:
 * Array of estimate configuration ids. FKEY to t_estimate_conf.id. All est. conf. ids to be checked (aggregated class together with sub-classes) need to be passed.
 * Minimal amount of difference [%].
 * Whether include NULL difference -- indicating defined sub-classes missing in data.
Resulting table has following columns:
 * Estimate estimation cell. FKEY to c_estimation_cell.id.
 * Estimate auxiliary configuration. FKEY to t_aux_conf.id.
 * Parameter showing whether estimate is forced to be synthetic.
 * Estimate configuration id. FKEY to t_estimate_conf.id.
 * Estimate attribute -- variable. FKEY to t_variable.id.
 * Aggregated class point estimate.
 * Sum of sub-classes point estimates (belonging to aggregated class).
 * Attributes -- variables defined in hierarchy (v_variable_hierarchy). Array of FKEYs to t_variable.id.
 * Attributes -- variables found in data (t_result). Array of FKEYs to t_variable.id.
 * Estimate configurations found in data (t_result). Array of FKEYs to t_estimate_donf.id.
 * Relative difference between aggregated class estimate and sum of sub-classes estimates.';

GRANT EXECUTE ON FUNCTION @extschema@.fn_add_res_total_attr TO PUBLIC;

-- </function>

-- <function_name="fn_add_res_total_geo" function_schema="extschema" src="functions/extschema/additivity/fn_add_res_total_geo.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_add_res_total_geo(integer)

-- DROP FUNCTION @extschema@.fn_add_res_total_geo(integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_add_res_total_geo(
	IN est_confs integer[],
	IN min_diff double precision default 0.0, IN include_null_diff boolean default true
)
  RETURNS TABLE(
	variable		integer,
	aux_conf		integer,
	force_synthetic		boolean,
	estimate_conf		integer,
	estimation_cell		integer,
	point_est		double precision,
	point_est_sum		double precision,
	estimation_cells_def	integer[],
	estimation_cells_found	integer[],
	estimate_confs_found	integer[],
	diff			double precision
) AS
$BODY$
DECLARE
	_complete_query text;
	_array_text text;
BEGIN
	--------------------------------QUERY--------------------------------
	_array_text := concat(quote_literal(est_confs::text), '::integer[]');
	--raise notice 'est_confs: %',  _array_text;
	_complete_query := '
with w_res_cell_var as not materialized (
	select
		t_result.estimate_conf,
		t_result.point,
		t_total_estimate_conf.estimation_cell,
		t_total_estimate_conf.variable as t_variable__id,
		c_estimation_period.estimate_date_begin, c_estimation_period.estimate_date_end, 
		json_agg(row_to_json((SELECT d FROM (SELECT est_conf_stratum.stratum, est_conf_stratum.panels) d))
			order by est_conf_stratum.stratum)::jsonb as strata_panels,
		t_total_estimate_conf.aux_conf, force_synthetic
	from @extschema@.t_result
	inner join @extschema@.t_estimate_conf ON t_estimate_conf.id = t_result.estimate_conf
	inner join @extschema@.t_total_estimate_conf ON t_total_estimate_conf.id = t_estimate_conf.total_estimate_conf
	inner join @extschema@.c_estimation_period ON c_estimation_period.id = t_total_estimate_conf.estimation_period
	inner join (
		select
			panel_refyearset_group, stratum, array_agg(t_panel.id order by t_panel.id) as panels
		from @extschema@.t_panel_refyearset_group
		inner join sdesign.t_panel on (t_panel_refyearset_group.panel = t_panel.id)
		group by panel_refyearset_group, stratum
		) as est_conf_stratum
		ON est_conf_stratum.panel_refyearset_group = t_total_estimate_conf.panel_refyearset_group
	where (t_estimate_conf.estimate_type = 1)
	and (t_result.is_latest)
	and (t_result.estimate_conf = ANY (' || _array_text || '))
	group by
		t_result.estimate_conf,
		t_result.point,
		t_total_estimate_conf.estimation_cell,
		t_total_estimate_conf.variable,
		c_estimation_period.estimate_date_begin, c_estimation_period.estimate_date_end,
		t_total_estimate_conf.aux_conf, force_synthetic
)
, w_node_sum as (
	select
		estimation_cell,
		w_res_cell_var.t_variable__id, aux_conf, force_synthetic,
		estimate_date_begin, estimate_date_end, strata_panels,
		estimate_conf as node_estimate_confs,
		sum(point) as node_sum,
		hierarchy.node,
		hierarchy.edges as edges_def
	from w_res_cell_var
	inner join @extschema@.v_estimation_cell_hierarchy	as hierarchy on (hierarchy.node = w_res_cell_var.estimation_cell)
	group by estimation_cell, w_res_cell_var.t_variable__id, aux_conf, force_synthetic, estimate_date_begin, estimate_date_end, strata_panels, estimate_conf, node, edges_def
	order by estimation_cell, w_res_cell_var.t_variable__id, aux_conf, force_synthetic, estimate_date_begin, estimate_date_end, strata_panels, estimate_conf, node, edges_def
)
, w_edge_sum as (
	select
		w_node_sum.t_variable__id as variable,
		w_node_sum.aux_conf,
		w_node_sum.force_synthetic,
		w_node_sum.node_estimate_confs,
		w_node_sum.estimate_date_begin, w_node_sum.estimate_date_end,
		w_node_sum.node,
		w_node_sum.node_sum,
		w_node_sum.edges_def,
		array_agg(w_res_cell_var.estimation_cell order by w_res_cell_var.estimation_cell) as edges_found,
		array_agg(w_res_cell_var.estimate_conf order by w_res_cell_var.estimate_conf) as edges_estimate_confs,
		sum(w_res_cell_var.point) as edges_sum
	from w_node_sum
	left join w_res_cell_var on (
		w_res_cell_var.t_variable__id = w_node_sum.t_variable__id
		and w_res_cell_var.estimate_date_begin = w_node_sum.estimate_date_begin
		and w_res_cell_var.estimate_date_end = w_node_sum.estimate_date_end
		and
		(
		SELECT array_to_json(array_agg(elem order by elem))::jsonb
		FROM   jsonb_array_elements(w_node_sum.strata_panels) elem
		WHERE  (elem->>''stratum'')::int in (select jsonb_path_query(w_res_cell_var.strata_panels, ''$.stratum'')::int)
		) = w_res_cell_var.strata_panels
		and case when
			w_res_cell_var.aux_conf is null and w_node_sum.aux_conf is null then true
			else w_res_cell_var.aux_conf = w_node_sum.aux_conf end
		and case when
			w_res_cell_var.force_synthetic is null and w_node_sum.force_synthetic is null then true
			else w_res_cell_var.force_synthetic = w_node_sum.force_synthetic end
		and w_res_cell_var.estimation_cell = any(w_node_sum.edges_def))
	group by w_node_sum.t_variable__id,
		w_node_sum.aux_conf, w_node_sum.force_synthetic, w_node_sum.node_estimate_confs,
		w_node_sum.estimate_date_begin, w_node_sum.estimate_date_end,
		w_node_sum.node, w_node_sum.node_sum, w_node_sum.edges_def
)
, w_diff as (
	select
		variable,
		aux_conf,
		force_synthetic,
		node_estimate_confs	as estimate_conf,
		node			as estimation_cell,
		node_sum		as point_est,
		edges_sum		as point_est_sum,
		edges_def		as estimation_cells_def,
		edges_found		as estimation_cells_found,
		edges_estimate_confs	as estimate_confs_found,
		case
			when edges_sum != 0.0 and node_sum = 0.0 then 1.0
			when node_sum = 0.0 and edges_sum = 0.0 then 0
			else abs( (node_sum - edges_sum) / node_sum )
		end as diff
	from w_edge_sum
)
select * from w_diff
where
	(' || include_null_diff || ' and ((diff >= ' || min_diff || ') or (diff is null)))
	or
	(not ' || include_null_diff || ' and (diff >= ' || min_diff || '))
order by diff desc
;
';

	--RAISE NOTICE '%', _complete_query;
	RETURN QUERY EXECUTE _complete_query;
END;
$BODY$
  LANGUAGE plpgsql STABLE
  COST 100
  ROWS 100000;

comment on function @extschema@.fn_add_res_total_geo(integer[], double precision, boolean) is
	'Fnction showing total estimates geographic additivity (aggregated class estimate should be equal to sum of sub-classes estimates). Hierarchy between aggregated classes and its sub-classes is defined in v_estimation_cell_hierarchy.
Function input argument is:
 * Array of estimate configuration ids. FKEY to t_estimate_conf.id. All est. conf. ids to be checked (aggregated class together with sub-classes) need to be passed.
 * Minimal amount of difference [%].
 * Whether include NULL difference -- indicating defined sub-classes missing in data.
Resulting table has following columns:
 * Estimate attribute -- variable. FKEY to t_variable.id.
 * Estimate auxiliary configuration. FKEY to t_aux_conf.id.
 * Parameter showing whether estimate is forced to be synthetic.
 * Estimate configuration id. FKEY to t_estimate_conf.id.
 * Estimate estimation cell. FKEY to c_estimation_cell.id.
 * Aggregated class point estimate.
 * Sum of sub-classes point estimates (belonging to aggregated class).
 * Estimation cells defined in hierarchy (v_estimation_cell_hierarchy). Array of FKEYs to c_estimation_cell.id.
 * Estimation cells found in data (t_result). Array of FKEYs to c_estimation_cell.id.
 * Estimate configurations found in data (t_result). Array of FKEYs to t_estimate_donf.id.
 * Relative difference between aggregated class estimate and sum of sub-classes estimates.';

GRANT EXECUTE ON FUNCTION @extschema@.fn_add_res_total_geo TO PUBLIC;

-- </function>

-- <function_name="fn_1p_est_configuration" function_schema="extschema" src="functions/extschema/configuration/fn_1p_est_configuration.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_1p_est_configuration(integer, integer, integer, varchar, integer)
--DROP FUNCTION @extschema@.fn_1p_est_configuration(integer, integer, integer, character varying, integer);
CREATE OR REPLACE FUNCTION @extschema@.fn_1p_est_configuration(
		_panel_refyearset_group integer, 
		_estimation_cell integer, _estimation_period integer, 
		_note varchar, _variable integer)
RETURNS integer
AS
$function$
DECLARE
_total_estimate_conf		integer;
_stratas_wp			integer[];
_target_label			varchar;
_cell				varchar;
_change_variable		boolean;
BEGIN

	_target_label := (
			SELECT		replace(
						replace(
							concat(coalesce(t2.etl_join_id,'x'), ',', coalesce(t3.label,'x'), ',', coalesce(t4.label,'x'), ',', coalesce(t5.label,'x')),
						'x,',''),
					',x','') AS label
			FROM		@extschema@.t_variable AS t1
			LEFT JOIN 	@extschema@.c_target_variable AS t2 ON t1.target_variable = t2.id
			LEFT JOIN	@extschema@.c_sub_population_category AS t3 ON t1.sub_population_category = t3.id
			LEFT JOIN	@extschema@.c_area_domain_category AS t4 ON t1.area_domain_category = t4.id
			LEFT JOIN	@extschema@.c_auxiliary_variable_category AS t5 ON t1.auxiliary_variable_category = t5.id
			WHERE
				t1.id = _variable
			);


	_change_variable := (
			SELECT		CASE WHEN t3.id = 2 THEN true ELSE false END
			FROM 		@extschema@.t_variable AS t1
			LEFT JOIN 	@extschema@.c_target_variable AS t2 ON t1.target_variable = t2.id
			LEFT JOIN	@extschema@.c_state_or_change AS t3 ON t2.state_or_change = t3.id
			WHERE t1.id = _variable
			);
		

	_cell := (SELECT label FROM @extschema@.c_estimation_cell WHERE id = _estimation_cell);

	-- reset sequence (usually when previous atempt to configure fails, the sequence remains shifted)
	PERFORM setval('@extschema@.t_total_estimate_conf_id_seq', (SELECT coalesce(max(id),0)+1 FROM @extschema@.t_total_estimate_conf), FALSE);

	-- insert into table t_total_estimate_conf
	INSERT INTO @extschema@.t_total_estimate_conf (estimation_cell, estimation_period, total_estimate_conf, 
							variable, phase_estimate_type, aux_conf, panel_refyearset_group)
	VALUES
		(_estimation_cell, _estimation_period, concat('1p;T=',_target_label,';Cell=',_cell,_note), _variable, 1, NULL, _panel_refyearset_group)
	RETURNING id
	INTO _total_estimate_conf;

	-- insert into table t_estimate_conf
	INSERT INTO @extschema@.t_estimate_conf (estimate_type, total_estimate_conf, denominator)
	SELECT 1, _total_estimate_conf, NULL;

	RETURN _total_estimate_conf;
END;
$function$
LANGUAGE plpgsql
PARALLEL SAFE;

COMMENT ON FUNCTION @extschema@.fn_1p_est_configuration(integer,integer,integer,varchar,integer) IS 'Function makes the estimate configuration present in the database - provides inserts into the all necessary tables.';

-- </function>

-- <function_name="fn_2p_est_configuration" function_schema="extschema" src="functions/extschema/configuration/fn_2p_est_configuration.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_2p_est_configuration(integer, regclass)
--DROP FUNCTION @extschema@.fn_2p_est_configuration(integer,integer,character varying,integer,integer,boolean);
CREATE OR REPLACE FUNCTION @extschema@.fn_2p_est_configuration(_estimation_cell integer, _estimation_period integer, _note varchar, _target_variable integer, _aux_conf integer, _force_synthetic boolean default False)
RETURNS integer
AS
$function$
DECLARE
_total_estimate_conf		integer;
_estimate_date_begin		date;
_estimate_date_end		date;
_stratas			integer[];
_stratas_wp			integer[];
_panels				integer[];
_refyearsets			integer[];
_panels_aux			integer[];
_param_area			integer;
_param_area_code		varchar;
_target_label			varchar;
_model				integer;
_cell				varchar;
_panel_refyearset_group		integer;
BEGIN

-- test for existing g_betas
-- otherwise the configuration cannot be done (sometimes the g_betas cannot be computed)
-- so this prevents to configure non-computable estimates

IF (SELECT count(*) FROM @extschema@.t_g_beta WHERE aux_conf = $5) = 0
THEN
	RAISE EXCEPTION 'G-betas for required aux_conf (%) are not available (cell=%, period=%). The computation of it was not run or is not able to compute (mostly the problem of matrix inversion).', _aux_conf, _estimation_cell, _estimation_period;
END IF;

SELECT estimate_date_begin, estimate_date_end
FROM @extschema@.c_estimation_period
WHERE id = _estimation_period
INTO _estimate_date_begin, _estimate_date_end;

IF _estimate_date_begin IS NULL OR _estimate_date_end IS NULL
THEN
	RAISE EXCEPTION 'At leats one of the estimate period dates (%, %) is NULL!', _estimate_date_begin, _estimate_date_end;
END IF;


-- create the label of estimate
_target_label := (
		SELECT		replace(
					replace(
						concat(coalesce(t2.etl_join_id,'x'), ',', coalesce(t3.label,'x'), ',', coalesce(t4.label,'x'), ',', coalesce(t5.label,'x')),
					'x,',''),
			       	',x','') AS label
		FROM		@extschema@.t_variable AS t1
		LEFT JOIN 	@extschema@.c_target_variable AS t2 ON t1.target_variable = t2.id
		LEFT JOIN	@extschema@.c_sub_population_category AS t3 ON t1.sub_population_category = t3.id
		LEFT JOIN	@extschema@.c_area_domain_category AS t4 ON t1.area_domain_category = t4.id
		LEFT JOIN	@extschema@.c_auxiliary_variable_category AS t5 ON t1.auxiliary_variable_category = t5.id
		WHERE
			t1.id = _target_variable
		);

_param_area := (SELECT param_area FROM @extschema@.t_aux_conf WHERE id = $5);
_model := (SELECT model FROM @extschema@.t_aux_conf WHERE id = $5);
_param_area_code := (SELECT param_area_code FROM @extschema@.f_a_param_area WHERE gid = _param_area);
_cell := (SELECT label FROM @extschema@.c_estimation_cell WHERE id = $1);

-- reset sequence (usually when previous atempt to configure fails, the sequence remains shifted)
PERFORM setval('@extschema@.t_total_estimate_conf_id_seq', (SELECT coalesce(max(id),0)+1 FROM @extschema@.t_total_estimate_conf), FALSE);

	-- test on param_area_coverage
		SELECT
			array_agg(t1.id ORDER BY t1.id)
		FROM
			sdesign.t_stratum AS t1
		INNER JOIN
			@extschema@.f_a_param_area AS t2
		ON
			-- buffered stratum?
			-- no, if only buffer of the stratum would intersect the cell, 
			-- no consequences on estimate would be introduced, the buffer is in general used only for the edge effect compensation (the compensated area is within non-buffered stratum)
			ST_Intersects(t1.geom, t2.geom) AND NOT ST_Touches(t1.geom, t2.geom)
		WHERE
			t2.gid = _param_area
		INTO _stratas;

		IF _stratas IS NULL
		THEN
			RAISE EXCEPTION 'The specified cell is not intersected by any stratum. Choose another estimation cell.';
		END IF;

	-- existing panels configured in panel2aux_conf
		SELECT
			array_agg(t1.panel ORDER BY t1.panel)
		FROM
			@extschema@.t_panel_refyearset_group AS t1
		INNER JOIN
			@extschema@.t_aux_conf AS t2
		ON	t1.panel_refyearset_group = t2.panel_refyearset_group
		WHERE
			t2.id = _aux_conf
		INTO _panels_aux;

	-- check of panel2total_2ndph
	-- and addition of panels from param_area - is the target variable available not only in cell?

		WITH w_data AS MATERIALIZED (
			SELECT
				t1.id AS stratum, t2.id AS panel, t9.id AS reference_year_set, t2.plot_count AS total
			FROM
				sdesign.t_stratum AS t1
			INNER JOIN
				sdesign.t_panel AS t2
			ON
				t1.id = t2.stratum
			INNER JOIN
				sdesign.cm_refyearset2panel_mapping AS t8
			ON
				t2.id = t8.panel --AND
				--t8.id = t9.reference_year_set
			INNER JOIN
				sdesign.t_reference_year_set AS t9
			ON
				t8.reference_year_set = t9.id
			INNER JOIN
				@extschema@.t_available_datasets AS t6
			ON
				t2.id = t6.panel AND
				t9.id = t6.reference_year_set
			INNER JOIN
				@extschema@.t_variable AS t7
			ON
				t6.variable = t7.id
			WHERE
				array[t1.id] <@ _stratas AND
				t7.id = _target_variable AND 
				(t9.reference_date_begin >= _estimate_date_begin AND
				t9.reference_date_end <= _estimate_date_end)
			GROUP BY
				t1.id, t2.id, t9.id
		)
		SELECT
			array_agg(DISTINCT stratum ORDER BY stratum) AS stratas,
			array_agg(panel ORDER BY panel) AS panels,
			array_agg(reference_year_set ORDER BY panel) AS refyearsets
		FROM
			(SELECT
				stratum, panel, reference_year_set,
				total,
				max(total) OVER(PARTITION BY stratum, panel, reference_year_set) AS max_total
			FROM
				w_data
			) AS t1
		WHERE
			-- pick up the most dense panel with target variable
			total = max_total
		INTO _stratas_wp, _panels, _refyearsets;

		IF _panels != _panels_aux OR _panels IS NULL
		THEN
			RAISE EXCEPTION 'Not all panels coming from g_beta have available target variable! aux_conf: %, panels: %, panels_aux: %',
			_aux_conf, _panels, _panels_aux;
		END IF;

		_panel_refyearset_group := (SELECT @extschema@.fn_get_panel_refyearset_group(_panels, _refyearsets));

		IF _panel_refyearset_group IS NULL
		THEN
			_panel_refyearset_group := (SELECT @extschema@.fn_save_panel_refyearset_group(_panels, _refyearsets));
		END IF;

-- insert into table t_total_estimate_conf
INSERT INTO @extschema@.t_total_estimate_conf (estimation_cell, estimation_period, total_estimate_conf, variable, phase_estimate_type, force_synthetic, aux_conf, panel_refyearset_group)
VALUES
	($1, $2, concat('2p;T=',_target_label,';C=',_cell,';PA=',_param_area_code, ';m=',_model,_note), $4, 2, $6, $5, _panel_refyearset_group)
ON CONFLICT (estimation_cell, estimation_period, variable, phase_estimate_type, coalesce(force_synthetic,false), coalesce(aux_conf,0), panel_refyearset_group)
DO NOTHING
RETURNING id
INTO _total_estimate_conf;

IF _total_estimate_conf IS NOT NULL
THEN
	-- insert into table t_estimate_conf
	INSERT INTO @extschema@.t_estimate_conf (estimate_type, total_estimate_conf, denominator)
	SELECT 1, _total_estimate_conf, NULL;

	IF	(
			SELECT
				array_agg(t_variable.id ORDER BY t_variable.id)
			FROM @extschema@.t_aux_total
			INNER JOIN @extschema@.t_variable 		ON (t_aux_total.variable = t_variable.id)
			INNER JOIN @extschema@.c_estimation_cell 	ON (t_aux_total.estimation_cell = c_estimation_cell.id)
			INNER JOIN @extschema@.t_model_variables 	ON t_model_variables.variable = t_variable.id
			INNER JOIN @extschema@.t_model 			ON t_model.id = t_model_variables.model
			INNER JOIN @extschema@.t_aux_conf 		ON t_aux_conf.model = t_model_variables.model
			WHERE 	c_estimation_cell.id = $1 AND
				t_aux_conf.id = $5 AND
				t_aux_total.is_latest

		)
		!= (
			SELECT
				array_agg(t_model_variables.variable order by variable)
			FROM @extschema@.t_aux_conf
			INNER JOIN @extschema@.t_model ON t_model.id = t_aux_conf.model
			INNER JOIN @extschema@.t_model_variables ON t_model_variables.model = t_model.id
			WHERE t_aux_conf.id = $5
		)
	THEN
		RAISE EXCEPTION 'fn_2p_est_configuration: t_aux_total not found! (total_estimate_conf: %)', _total_estimate_conf;
	END IF;
ELSE
	RAISE NOTICE 'Required configuration already exists!';
END IF;


RETURN _total_estimate_conf;


END;
$function$
LANGUAGE plpgsql
PARALLEL SAFE;

--COMMENT ON FUNCTION @extschema@.fn_2p_est_configuration() IS '.';

-- </function>

-- <function_name="fn_1p_data" function_schema="extschema" src="functions/extschema/fn_1p_data.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_1p_data(integer)

-- DROP FUNCTION @extschema@.fn_1p_data(integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_1p_data(
    IN conf_id integer
)
  RETURNS TABLE(
	gid bigint,
	cluster integer,
	attribute integer,
	stratum integer,
	plots_per_cluster integer,
	plcount bigint,
	cluster_is_in_cell boolean,
	ldsity_d double precision,
	ldsity_d_plus double precision,
	is_aux boolean,
	is_target boolean,
	geom geometry(MultiPoint),
	ldsity_res_d double precision,
	ldsity_res_d_plus double precision,
	pix double precision,
	sweight double precision,
	DELTA_T__G_beta double precision,
	nb_sampling_units integer, 
	sweight_strata_sum double precision, 
	lambda_d_plus double precision
) AS
$BODY$
DECLARE
	_complete_query text;
BEGIN
	--------------------------------QUERY--------------------------------
	_complete_query := '
WITH
---------------------------------------------------------
-- Data block
---------------------------------------------------------
w_ldsity_plot AS MATERIALIZED (
	select
		f_p_plot.gid,
		t_total_estimate_conf.id as conf_id,
		t_panel.stratum,
		t_cluster.id as cluster,
		t_panel_refyearset_group.reference_year_set,
		t_panel_refyearset_group.panel,
		cm_cluster2panel_mapping.sampling_weight,
		t_total_estimate_conf.variable as attribute,
		plots_per_cluster,
		true AS plot_is_in_cell,
		coalesce(t_target_data.value, 0) as ldsity,
		f_p_plot.geom
	from @extschema@.t_total_estimate_conf
	inner join @extschema@.c_panel_refyearset_group on t_total_estimate_conf.panel_refyearset_group = c_panel_refyearset_group.id
	inner join @extschema@.t_panel_refyearset_group on t_panel_refyearset_group.panel_refyearset_group = c_panel_refyearset_group.id
	inner join sdesign.t_panel ON t_panel.id = t_panel_refyearset_group.panel
	inner join sdesign.cm_cluster2panel_mapping ON cm_cluster2panel_mapping.panel = t_panel.id
	inner join sdesign.t_cluster ON t_cluster.id = cm_cluster2panel_mapping."cluster"
	inner join sdesign.f_p_plot ON f_p_plot."cluster" = t_cluster.id
	inner join sdesign.cm_plot2cluster_config_mapping on f_p_plot.gid = cm_plot2cluster_config_mapping.plot
	inner join sdesign.t_cluster_configuration ON (t_panel.cluster_configuration = t_cluster_configuration.id
											and cm_plot2cluster_config_mapping.cluster_configuration = t_cluster_configuration.id)
	inner join sdesign.t_stratum ON t_panel.stratum = t_stratum.id
	inner join @extschema@.cm_plot2cell_mapping ON (cm_plot2cell_mapping.estimation_cell = t_total_estimate_conf.estimation_cell 
											and cm_plot2cell_mapping.plot = f_p_plot.gid)
	left join @extschema@.t_target_data on (
		f_p_plot.gid = t_target_data.plot and
		t_panel_refyearset_group.reference_year_set = t_target_data.reference_year_set and
		t_total_estimate_conf.variable = t_target_data.variable and
		t_target_data.is_latest)
	where t_total_estimate_conf.id = ' || conf_id || '
)
, w_ldsity_cluster AS MATERIALIZED (
	SELECT
		w_ldsity_plot.conf_id,
		concat(w_ldsity_plot.conf_id, ROW_NUMBER() OVER ())::bigint as gid,
		w_ldsity_plot.cluster,
		w_ldsity_plot.attribute,
		w_ldsity_plot.stratum,
                w_ldsity_plot.panel,
		w_ldsity_plot.plots_per_cluster,
		w_ldsity_plot.sampling_weight,
		count(*) as plcount,
		sum(w_ldsity_plot.plot_is_in_cell::int) > 0 as cluster_is_in_cell,
		sum(w_ldsity_plot.ldsity * plot_is_in_cell::int)/w_ldsity_plot.plots_per_cluster AS ldsity_D,
		sum(w_ldsity_plot.ldsity)/w_ldsity_plot.plots_per_cluster AS ldsity_D_plus,
		st_collect(w_ldsity_plot.geom)::geometry(MultiPoint) as geom
	FROM       w_ldsity_plot
	GROUP BY conf_id, stratum, panel, cluster, sampling_weight, plots_per_cluster, attribute
	ORDER BY stratum, cluster, attribute
)
, w_strata_sum AS MATERIALIZED (
	select
		t.conf_id,
		t_panel.stratum,
		case when t_cluster_configuration.cluster_design then
			t_stratum.buffered_area_m2/10000
		else
			t_stratum.area_m2/10000
		end as lambda_d_plus,
		sum(t_panel.cluster_count) as nb_sampling_units,
		sum(t_panel.sweight_panel_sum) as sweight_strata_sum
	from sdesign.t_stratum
	inner join sdesign.t_panel on (t_panel.stratum = t_stratum.id)
	inner join sdesign.t_cluster_configuration on (t_panel.cluster_configuration = t_cluster_configuration.id)
	inner join (select distinct conf_id, panel from w_ldsity_plot) as t on (t_panel.id = t.panel)
	group by conf_id, t_panel.stratum, lambda_d_plus
)
, w_1p_data AS MATERIALIZED (
	select
		w_ldsity_cluster.gid, w_ldsity_cluster.cluster,
		w_ldsity_cluster.attribute, w_ldsity_cluster.stratum, w_ldsity_cluster.plots_per_cluster,
		w_ldsity_cluster.plcount, w_ldsity_cluster.cluster_is_in_cell, w_ldsity_cluster.ldsity_d,
		w_ldsity_cluster.ldsity_d_plus, false::boolean as is_aux, true::boolean as is_target,
		w_ldsity_cluster.geom,
		NULL::double precision as ldsity_res_D, NULL::double precision as ldsity_res_D_plus,
		NULL::double precision as pix,  w_ldsity_cluster.sampling_weight as sweight, NULL::double precision as DELTA_T__G_beta,
		w_strata_sum.nb_sampling_units::integer, w_strata_sum.sweight_strata_sum, w_strata_sum.lambda_d_plus
	from w_ldsity_cluster
	INNER JOIN w_strata_sum ON w_ldsity_cluster.stratum = w_strata_sum.stratum
)
select * from w_1p_data;
';

	--RAISE NOTICE '%', _complete_query;
	RETURN QUERY EXECUTE _complete_query;
END;
$BODY$
  LANGUAGE plpgsql STABLE
  COST 100
  ROWS 100000;

COMMENT ON FUNCTION @extschema@.fn_1p_data(integer) IS 'Function preparing data for regression estimate.';

-- </function>

-- <function_name="fn_2p_data" function_schema="extschema" src="functions/extschema/fn_2p_data.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_2p_data(integer)

-- DROP FUNCTION @extschema@.fn_2p_data(integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_2p_data(
    IN conf_id integer
)
  RETURNS TABLE(
	gid bigint,
	cluster integer,
	attribute integer,
	stratum integer,
	plots_per_cluster integer,
	plcount bigint,
	cluster_is_in_cell boolean,
	ldsity_d double precision,
	ldsity_d_plus double precision,
	is_aux boolean,
	is_target boolean,
	geom geometry(MultiPoint),
	ldsity_res_d double precision,
	ldsity_res_d_plus double precision,
	pix double precision,
	sweight double precision,
	DELTA_T__G_beta double precision,
	nb_sampling_units integer,
	sweight_strata_sum double precision,
	lambda_d_plus double precision,
	sigma boolean
) AS
$$
begin
--------------------------------QUERY--------------------------------
---------------------------------------------------------
-- Configuration block
---------------------------------------------------------
execute '
create temporary table w_configuration ON COMMIT DROP AS (
	with w_a AS NOT MATERIALIZED (
		select
			t_total_estimate_conf.estimation_cell, t_aux_conf.model, 
			t_aux_conf.sigma, t_total_estimate_conf.force_synthetic, t_aux_conf.param_area,
			array_agg(t_total_estimate_conf.id order by t_total_estimate_conf.variable) as t_total_estimate_conf__id,
			array_agg(t_total_estimate_conf.panel_refyearset_group order by t_total_estimate_conf.variable) as t_total_estimate_conf__panel_refyearset_group,
			array_agg(t_aux_conf.id order by t_total_estimate_conf.variable) as t_aux_conf__id,
			array_agg(t_total_estimate_conf.variable order by t_total_estimate_conf.variable) as target_attributes,
			t_total_estimate_conf.aux_conf
		from @extschema@.t_total_estimate_conf
		inner join @extschema@.t_aux_conf ON t_aux_conf.id = t_total_estimate_conf.aux_conf
		where t_total_estimate_conf.id = $1
		group by t_total_estimate_conf.estimation_cell, t_aux_conf.model, t_aux_conf.sigma, t_total_estimate_conf.force_synthetic,
			t_aux_conf.param_area, t_total_estimate_conf.aux_conf
	)
	select
		t_total_estimate_conf__id[1] as id,
		t_total_estimate_conf__panel_refyearset_group[1] AS panel_refyearset_group,
		t_total_estimate_conf__panel_refyearset_group,
		w_a.t_total_estimate_conf__id, w_a.t_aux_conf__id,
		w_a.param_area, w_a.estimation_cell as cell, w_a.model, w_a.sigma, w_a.force_synthetic,
		array_agg(t_model_variables.variable order by t_model_variables.variable) as aux_attributes, w_a.target_attributes
	from w_a
	inner join @extschema@.t_aux_conf ON t_aux_conf.id = w_a.aux_conf
	inner join @extschema@.t_model ON t_model.id = t_aux_conf.model
	inner join @extschema@.t_model_variables ON t_model_variables.model = t_model.id
	group by w_a.estimation_cell, w_a.model, w_a.sigma, w_a.force_synthetic, w_a.param_area, w_a.target_attributes, 
		w_a.t_total_estimate_conf__id, w_a.t_total_estimate_conf__panel_refyearset_group, w_a.t_aux_conf__id
	order by id limit 1
);' using fn_2p_data.conf_id;
analyze w_configuration;

execute '
create temporary table w_cell_selection ON COMMIT DROP AS (
	SELECT
		w_configuration.id as conf_id,
		estimation_cell as estimation_cell,
		geom
	FROM @extschema@.f_a_cell
	INNER JOIN w_configuration ON (w_configuration.cell = f_a_cell.estimation_cell)
);';
analyze w_cell_selection;

execute '
create temporary table w_plot ON COMMIT DROP as (
WITH
w_param_area_selection AS NOT MATERIALIZED (
	SELECT
		w_configuration.id as conf_id,
		gid as param_area_gid,
		geom
	FROM @extschema@.f_a_param_area
	INNER JOIN w_configuration ON (w_configuration.param_area = f_a_param_area.gid)
)
---------------------------------------------------------
-- Data block
---------------------------------------------------------
, w_plot AS NOT MATERIALIZED (
	select distinct
		f_p_plot.gid, w_configuration.id as conf_id, t_cluster.id as cluster, t_panel.stratum,
		case when w_configuration.force_synthetic = True then False else cm_plot2cell_mapping.id IS NOT NULL end AS plot_is_in_cell, 
		-- t_panel2aux_conf.reference_year_set as reference_year_set, t_panel2aux_conf.panel as panel,
		t_panel_refyearset_group.reference_year_set as reference_year_set, t_panel_refyearset_group.panel as panel,
		f_p_plot.geom
	from w_configuration
        inner join @extschema@.t_panel_refyearset_group on w_configuration.panel_refyearset_group = t_panel_refyearset_group.panel_refyearset_group
	--inner join @extschema@.t_panel2aux_conf on w_configuration.t_aux_conf__id[1] = t_panel2aux_conf.aux_conf
	inner join sdesign.t_panel ON (t_panel.id = t_panel_refyearset_group.panel)
	inner join sdesign.cm_cluster2panel_mapping ON cm_cluster2panel_mapping.panel = t_panel.id
	inner join sdesign.t_cluster ON t_cluster.id = cm_cluster2panel_mapping."cluster"
	inner join sdesign.f_p_plot ON f_p_plot."cluster" = t_cluster.id
	left join @extschema@.cm_plot2cell_mapping ON (cm_plot2cell_mapping.estimation_cell = w_configuration.cell and cm_plot2cell_mapping.plot = f_p_plot.gid)
	inner join @extschema@.cm_plot2param_area_mapping ON (cm_plot2param_area_mapping.param_area = w_configuration.param_area and cm_plot2param_area_mapping.plot = f_p_plot.gid)
)
select * from w_plot
);';
analyze w_plot;

execute '
create temporary table w_ldsity_plot ON COMMIT DROP as (
	with w_plot AS NOT MATERIALIZED (select * from w_plot)
		select
			w_plot.gid,
			w_plot.conf_id,
			w_plot.stratum,
			w_plot.panel,
			w_plot.cluster,
			t_variable.id as attribute,
			t_cluster_configuration.plots_per_cluster,
			w_plot.plot_is_in_cell,
			coalesce(t_auxiliary_data.value, 0) as ldsity,
			true as is_aux, false as is_target,
			w_plot.geom
		from w_plot as w_plot
		inner join sdesign.cm_plot2cluster_config_mapping on w_plot.gid = cm_plot2cluster_config_mapping.plot
		inner join sdesign.t_cluster_configuration on cm_plot2cluster_config_mapping.cluster_configuration = t_cluster_configuration.id
		inner join w_configuration on w_plot.conf_id = w_configuration.id
		inner join @extschema@.t_variable on t_variable.id = ANY (w_configuration.aux_attributes)
		left join @extschema@.t_auxiliary_data on (
			w_plot.gid = t_auxiliary_data.plot and
			t_variable.id = t_auxiliary_data.variable and
			t_auxiliary_data.is_latest)
	union all
		select
			w_plot.gid,
			w_plot.conf_id,
			w_plot.stratum,
			w_plot.panel,
			w_plot.cluster,
			t_variable.id as attribute,
			t_cluster_configuration.plots_per_cluster,
			w_plot.plot_is_in_cell,
			coalesce(t_target_data.value, 0) as ldsity,
			false as is_aux, true as is_target,
			w_plot.geom
		from w_plot as w_plot
		inner join sdesign.cm_plot2cluster_config_mapping on w_plot.gid = cm_plot2cluster_config_mapping.plot
		inner join sdesign.t_cluster_configuration on cm_plot2cluster_config_mapping.cluster_configuration = t_cluster_configuration.id
		inner join w_configuration on w_plot.conf_id = w_configuration.id
                inner join @extschema@.t_variable on (t_variable.id = ANY (w_configuration.target_attributes))
		left join @extschema@.t_target_data on (
			w_plot.gid = t_target_data.plot and
			w_plot.reference_year_set = t_target_data.reference_year_set and
			t_variable.id = t_target_data.variable and
			t_target_data.is_latest)
);';
analyze w_ldsity_plot;

execute '
create temporary table w_ldsity_cluster ON COMMIT DROP as (
	SELECT
		w_ldsity_plot.conf_id,
		concat(w_ldsity_plot.conf_id, ROW_NUMBER() OVER ())::bigint as gid,
		w_ldsity_plot.cluster,
		w_ldsity_plot.attribute,
		w_ldsity_plot.stratum,
		w_ldsity_plot.panel,
		w_ldsity_plot.plots_per_cluster,
		count(*) as plcount,
		sum(w_ldsity_plot.plot_is_in_cell::int) > 0 as cluster_is_in_cell,
		sum(w_ldsity_plot.ldsity * plot_is_in_cell::int)/w_ldsity_plot.plots_per_cluster AS ldsity_D,
		sum(w_ldsity_plot.ldsity)/w_ldsity_plot.plots_per_cluster AS ldsity_D_plus,
		w_ldsity_plot.is_aux, w_ldsity_plot.is_target,
		st_collect(w_ldsity_plot.geom)::geometry(MultiPoint) as geom
	FROM       w_ldsity_plot
	GROUP BY conf_id, stratum, panel, cluster, plots_per_cluster, attribute, is_aux, is_target
	ORDER BY stratum, cluster, attribute
);';
analyze w_ldsity_cluster;

execute '
create temporary table w_clusters ON COMMIT DROP as (
	select distinct conf_id, stratum, panel, cluster, cluster_is_in_cell from w_ldsity_cluster
);';
analyze w_clusters;

execute '
create temporary table w_strata_sum ON COMMIT DROP as (
	select
		w_configuration.id as conf_id,
                t_stratum.id as f_a_sampling_stratum_gid,
		case when t_cluster_configuration.cluster_design then
			t_stratum.buffered_area_m2/10000
		else
			t_stratum.area_m2/10000
		end as lambda_d_plus,
		plots_per_cluster,
		count(*) as nb_sampling_units, sum (sampling_weight) as sweight_strata_sum
	from w_configuration
	inner join @extschema@.t_aux_conf on t_aux_conf.id = ANY (w_configuration.t_aux_conf__id)
	inner join @extschema@.t_panel_refyearset_group ON t_panel_refyearset_group.panel_refyearset_group = t_aux_conf.panel_refyearset_group
	inner join sdesign.t_panel ON t_panel.id = t_panel_refyearset_group.panel
	inner join sdesign.t_cluster_configuration ON t_panel.cluster_configuration = t_cluster_configuration.id
	inner join sdesign.t_stratum ON t_stratum.id = t_panel.stratum
	inner join sdesign.cm_cluster2panel_mapping ON cm_cluster2panel_mapping.panel = t_panel.id
	group by w_configuration.id, f_a_sampling_stratum_gid, lambda_d_plus, plots_per_cluster
);';
analyze w_strata_sum;
---------------------------------------------------------
-- Matrix block
---------------------------------------------------------
execute '
create temporary table w_X ON COMMIT DROP AS (
	SELECT
		conf_id,
		attribute AS r,
		cluster AS c,
		ldsity_D as val_D,
		ldsity_D_plus as val
	FROM w_ldsity_cluster
	WHERE is_aux
);';
analyze w_X;

execute '
create temporary table w_Y ON COMMIT DROP AS (
	SELECT
		conf_id,
		attribute AS r,
		cluster AS c,
		ldsity_D as val_D,
		ldsity_D_plus as val
	FROM w_ldsity_cluster
	WHERE is_target
);';
analyze w_Y;

execute '
create temporary table w_pix ON COMMIT DROP as (
with
w_pix AS NOT MATERIALIZED (-------------------------INCLUSION DENSITY PIX
	SELECT
		w_clusters.conf_id,
		w_clusters.stratum,
		w_clusters.cluster,
		w_strata_sum.sweight_strata_sum / (w_strata_sum.lambda_d_plus * cm_cluster2panel_mapping.sampling_weight) as pix,
		cm_cluster2panel_mapping.sampling_weight as sweight
	FROM w_clusters
	INNER JOIN sdesign.cm_cluster2panel_mapping ON (w_clusters.cluster = cm_cluster2panel_mapping.cluster 
								and w_clusters.panel = cm_cluster2panel_mapping.panel)
	INNER JOIN w_strata_sum as w_strata_sum ON w_clusters.stratum = w_strata_sum.f_a_sampling_stratum_gid
)
select * from w_pix
);';
analyze w_pix;

execute '
create temporary table w_t_G_beta ON COMMIT DROP as (
        select
                w_configuration.id as conf_id,
                t_g_beta.variable as r, t_g_beta.cluster as c, t_g_beta.val
        from @extschema@.t_g_beta
        inner join w_configuration on (t_g_beta.aux_conf = w_configuration.t_aux_conf__id[1])
	where t_g_beta.is_latest
);';
analyze w_t_G_beta;

execute '
create temporary table w_PI ON COMMIT DROP as (
	SELECT
		conf_id,
		1 as r,
		cluster as c,
		1.0 / pix as val
	from w_pix
	order by r, c
);';
analyze w_PI;

execute '
create temporary table w_DELTA_G_beta ON COMMIT DROP as (
with w_I AS NOT MATERIALIZED (
	SELECT 
		conf_id,
		1 AS r,
		cluster AS c,
		cluster_is_in_cell::int AS val
	FROM	w_clusters
	order by r, c
)
, w_SIGMA AS NOT MATERIALIZED (
	SELECT
		conf_id,
		1 as r,
		cluster as c,
		case when w_configuration.sigma then (plots_per_cluster^2)::float / plcount::float 
		else 1::float end
		as val
	from (select conf_id, cluster, plots_per_cluster, plcount 
		from w_ldsity_cluster 
		group by conf_id, cluster, plots_per_cluster, plcount) as m
	inner join w_configuration on (m.conf_id = w_configuration.id)
	order by r, c
)
, w_SIGMA_PI AS NOT MATERIALIZED (
	SELECT
		A.conf_id,
		1 as r,
		A.c as c,
		A.val * B.val as val
	from w_SIGMA as A 
	inner join w_PI as B on (A.c = B.c and A.conf_id = B.conf_id) 
	order by r, c
)
, w_t_hat AS NOT MATERIALIZED (  -- eq. 18
	select 
		A.conf_id,
		A.r,
		1 AS c,
		sum(A.val_D * B.val * C.val) as val
	from w_X 		AS A 
	inner join w_PI     	AS B 	on (A.c = B.c and A.conf_id = B.conf_id)
	inner join w_I		AS C 	on (A.c = C.c and A.conf_id = C.conf_id)
	group by A.r, A.conf_id
	order by A.r
)
, w_t AS NOT MATERIALIZED (
	with w_aux_total as (
		select estimation_cell as cell, t_variable.id as attribute, aux_total
		from @extschema@.t_aux_total
		inner join @extschema@.t_variable on (t_aux_total.variable = t_variable.id)
		where t_aux_total.is_latest
	)
	, w_aux_ldsity as (
		select distinct conf_id, r as attribute from w_X
	)
	select
		w_cell_selection.conf_id,
		w_aux_total.attribute as r,
		1 as c,
		coalesce(w_aux_total.aux_total,
			@extschema@.fn_raise_notice(
				format(''fn_2p_data.w_t: t_aux_total not found! (conf_id:%s attribute:%s)'',
					w_cell_selection.conf_id,
					w_aux_ldsity.attribute),
				''exception''
			)::int::float
		) as val
	from
	w_aux_ldsity
	inner join w_cell_selection on (w_aux_ldsity.conf_id = w_cell_selection.conf_id)
	left join w_aux_total on (w_aux_ldsity.attribute = w_aux_total.attribute and w_cell_selection.estimation_cell = w_aux_total.cell)
)
, w_DELTA_T AS NOT MATERIALIZED (
	select
		conf_id,
		c as r, 
		r as c, 
		w_t.val - w_t_hat.val as val 
	from w_t 
	inner join w_t_hat using (r, c, conf_id)
)
, w_DELTA_G_beta AS NOT MATERIALIZED (
	select
		A.conf_id,
		A.r,
		B.c, 
		sum(A.val * B.val) as val
	FROM 	w_DELTA_T as A
	inner join w_t_G_beta as B ON (A.c = B.r and A.conf_id = B.conf_id)
	GROUP BY A.r, B.c, A.conf_id
	order by r, c
)
select * from w_DELTA_G_beta
);';
analyze w_DELTA_G_beta;

execute '
create temporary table w_X_beta ON COMMIT DROP as (
with w_Y_T AS NOT MATERIALIZED (
	SELECT
		conf_id,
		c as r,
		r as c,
		val,
		val_D -- used for 1p estimete
	from w_Y
	order by r, c
)
, w_G_PI AS NOT MATERIALIZED (
	select 
		A.conf_id,
		A.r, 
		A.c AS c,
		A.val * B.val as val
	from w_t_G_beta AS A, w_PI AS B 
	where A.c = B.c and A.conf_id = B.conf_id
	order by r, c
)
, w_beta_hat AS NOT MATERIALIZED (
	select
		A.conf_id,
		A.r,
		B.c, 
		sum(A.val * B.val) as val
	FROM w_G_PI as A, w_Y_T as B
	WHERE A.c = B.r and A.conf_id = B.conf_id
	GROUP BY A.r, B.c, A.conf_id
	order by r, c
)
, w_X_plots_T AS NOT MATERIALIZED (
	SELECT conf_id, gid AS r, cluster, attribute AS c, plots_per_cluster, ldsity AS val 
	FROM w_ldsity_plot WHERE is_aux
)
, w_X_beta AS NOT MATERIALIZED (
	select
		A.conf_id,
		A.r,
		B.c, 
		sum(A.val * B.val) as val
	FROM w_X_plots_T as A, w_beta_hat as B
	WHERE A.c = B.r and A.conf_id = B.conf_id
	GROUP BY A.r, B.c, A.conf_id
	order by r, c 
)
select * from w_X_beta
);';
analyze w_X_beta;

execute '
create temporary table w_ldsity_residuals_cluster ON COMMIT DROP as (
with w_Y_plots_T AS NOT MATERIALIZED (
	SELECT conf_id, gid AS r, cluster, attribute AS c, plots_per_cluster, ldsity AS val 
	FROM w_ldsity_plot WHERE is_target
)
, w_residuals_plot AS NOT MATERIALIZED ( -------------------------RESIDUAL LOCAL DENSITY ON TRACT LEVEL
	select
		w_Y_plots_T.conf_id,
		w_Y_plots_T.r AS plot, 
		w_Y_plots_T.c AS attribute, 
		(w_Y_plots_T.val - w_X_beta.val) AS ldsity 
	from w_Y_plots_T 
	INNER JOIN w_X_beta USING (r, c, conf_id)
)
, w_residuals_cluster AS NOT MATERIALIZED (
	with
	w_residuals_plot AS NOT MATERIALIZED (select * from w_residuals_plot),
	w_ldsity_plot AS NOT MATERIALIZED (select * from w_ldsity_plot)
	SELECT
		w_ldsity_plot.conf_id,
		w_ldsity_plot.cluster,
		w_residuals_plot.attribute,
		w_ldsity_plot.stratum,
		w_ldsity_plot.plots_per_cluster,
		count(*) as plcount,
		sum(plot_is_in_cell::int) > 0 as cluster_is_in_cell,
		sum(w_residuals_plot.ldsity * plot_is_in_cell::int)/w_ldsity_plot.plots_per_cluster AS ldsity_res_D, --eq 38
		sum(w_residuals_plot.ldsity)/w_ldsity_plot.plots_per_cluster AS ldsity_res_D_plus -- eq 39
	FROM w_ldsity_plot AS w_ldsity_plot
	INNER JOIN w_residuals_plot AS w_residuals_plot ON w_ldsity_plot.gid = w_residuals_plot.plot AND w_ldsity_plot.attribute = w_residuals_plot.attribute and w_ldsity_plot.conf_id = w_residuals_plot.conf_id
	GROUP BY stratum, w_ldsity_plot.conf_id, w_ldsity_plot.cluster, w_ldsity_plot.plots_per_cluster, w_residuals_plot.attribute
	ORDER BY stratum, w_ldsity_plot.conf_id, w_ldsity_plot.cluster, w_residuals_plot.attribute
)
, w_ldsity_residuals_cluster AS NOT MATERIALIZED (
	select 
		w_ldsity_cluster.conf_id, w_ldsity_cluster.gid, w_ldsity_cluster.cluster, 
		w_ldsity_cluster.attribute, w_ldsity_cluster.stratum, w_ldsity_cluster.plots_per_cluster, 
		w_ldsity_cluster.plcount, w_ldsity_cluster.cluster_is_in_cell, w_ldsity_cluster.ldsity_d, w_ldsity_cluster.ldsity_d_plus, 
		w_ldsity_cluster.is_aux, w_ldsity_cluster.is_target, w_ldsity_cluster.geom,
		w_residuals_cluster.ldsity_res_D, w_residuals_cluster.ldsity_res_D_plus
	from w_ldsity_cluster
	LEFT JOIN w_residuals_cluster USING (conf_id, cluster, attribute, stratum)
)
select * from w_ldsity_residuals_cluster
);';
analyze w_ldsity_residuals_cluster;

--EXPLAIN (ANALYZE, COSTS, VERBOSE, BUFFERS/*, FORMAT JSON*/)
return query execute '
with w_2p_data AS NOT MATERIALIZED (
	with w_ldsity_residuals_cluster AS NOT MATERIALIZED (select * from w_ldsity_residuals_cluster)
	select 
		w_ldsity_residuals_cluster.conf_id, w_ldsity_residuals_cluster.gid, w_ldsity_residuals_cluster.cluster, 
		w_ldsity_residuals_cluster.attribute, w_ldsity_residuals_cluster.stratum, w_ldsity_residuals_cluster.plots_per_cluster, 
		w_ldsity_residuals_cluster.plcount, w_ldsity_residuals_cluster.cluster_is_in_cell, w_ldsity_residuals_cluster.ldsity_d, 
		w_ldsity_residuals_cluster.ldsity_d_plus, 
		w_ldsity_residuals_cluster.is_aux, w_ldsity_residuals_cluster.is_target, w_ldsity_residuals_cluster.geom,
		w_ldsity_residuals_cluster.ldsity_res_D, w_ldsity_residuals_cluster.ldsity_res_D_plus,
		w_pix.pix, w_pix.sweight, w_DELTA_G_beta.val as DELTA_T__G_beta, 
		w_strata_sum.nb_sampling_units::integer, w_strata_sum.sweight_strata_sum, w_strata_sum.lambda_d_plus
	from w_ldsity_residuals_cluster
	INNER JOIN w_pix USING (conf_id, cluster, stratum)
	INNER JOIN w_DELTA_G_beta ON (w_pix.conf_id = w_DELTA_G_beta.conf_id and cluster = w_DELTA_G_beta.c)
	inner join w_strata_sum on (stratum = f_a_sampling_stratum_gid)
)
, w_2p_data_sigma AS NOT MATERIALIZED (
	select 
		w_2p_data.gid, w_2p_data.cluster, w_2p_data.attribute, w_2p_data.stratum, w_2p_data.plots_per_cluster, w_2p_data.plcount,
		w_2p_data.cluster_is_in_cell as cluster_is_in_cell, 
		w_2p_data.ldsity_d, w_2p_data.ldsity_d_plus, w_2p_data.is_aux, w_2p_data.is_target,
		w_2p_data.geom, w_2p_data.ldsity_res_d, w_2p_data.ldsity_res_d_plus, w_2p_data.pix, w_2p_data.sweight, w_2p_data.DELTA_T__G_beta,
		w_2p_data.nb_sampling_units, w_2p_data.sweight_strata_sum, w_2p_data.lambda_d_plus, w_configuration.sigma
	from w_2p_data 
	inner join w_configuration on (w_2p_data.conf_id = w_configuration.id)
)
select * from w_2p_data_sigma;';

drop table w_configuration;
drop table w_cell_selection;
drop table w_plot;
drop table w_ldsity_plot;
drop table w_ldsity_cluster;
drop table w_clusters;
drop table w_strata_sum;
drop table w_X;
drop table w_Y;
drop table w_pix;
drop table w_t_G_beta;
drop table w_PI;
drop table w_DELTA_G_beta;
drop table w_X_beta;
drop table w_ldsity_residuals_cluster;
end;
$$
  LANGUAGE plpgsql
  COST 100
  ROWS 100000;

COMMENT ON FUNCTION @extschema@.fn_2p_data(integer) IS 'Function preparing data for regression estimate.';

-- </function>
