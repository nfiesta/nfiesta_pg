--
-- Copyright 2021, 2025 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--


-- <function name="fn_api_before_delete_panel_refyearset_group" schema="extschema" src="functions/extschema/configuration/fn_api_before_delete_panel_refyearset_group.sql">
--
-- Copyright 2021, 2025 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

DROP FUNCTION IF EXISTS nfiesta.fn_launch_add_manual(_t_additivity_set_plot__id integer);

CREATE OR REPLACE FUNCTION nfiesta.fn_launch_add_manual(_t_additivity_set_plot__id integer)
RETURNS json
AS
$src$
DECLARE
    _var_sup int;
    _var_inf int[];
    _panel int; _panel_label text;
    _refyearsets int; _refyearsets_label text;
    _errp json;
    _dbg_data jsonb;
    _add_check_threshold float;
BEGIN
    ------------------ _vars_add_set
    select
        t_available_datasets.variable 	into _var_sup
    from nfiesta.t_available_datasets
    join nfiesta.t_additivity_set_plot on (t_additivity_set_plot.available_datasets = t_available_datasets.id)
    where t_additivity_set_plot.id = _t_additivity_set_plot__id;

    select
        edges			into _var_inf
    from nfiesta.t_additivity_set_plot where id = _t_additivity_set_plot__id
    ;
    
        create temporary table w_var_list as
            select
                t_variable.id,
                json_build_object(
                    'target_variable',          c_target_variable.metadata->'en'->>'pf_label',
                    'sub_population',           coalesce(c_sub_population.label, 'altogether'),
                    'sub_population_category',  coalesce(c_sub_population_category.label, 'altogether'),
                    'area_domain',              coalesce(c_area_domain.label, 'altogether'),
                    'area_domain_category',     coalesce(c_area_domain_category.label, 'altogether')
                ) as variable
            from nfiesta.t_variable
            join nfiesta.c_target_variable on t_variable.target_variable = c_target_variable.id
            left join nfiesta.c_sub_population_category on t_variable.sub_population_category = c_sub_population_category.id 
            left join nfiesta.c_sub_population on c_sub_population_category.sub_population = c_sub_population.id 
            left join nfiesta.c_area_domain_category on t_variable.area_domain_category = c_area_domain_category.id 
            left join nfiesta.c_area_domain on c_area_domain_category.area_domain = c_area_domain.id 
            where t_variable.id = any (array_prepend(_var_sup, _var_inf));
------------------ _plots
    select
        t_available_datasets.panel	into _panel
    from nfiesta.t_available_datasets
    where t_available_datasets.id = (select available_datasets from nfiesta.t_additivity_set_plot where id = _t_additivity_set_plot__id);
        
        select panel into _panel_label from sdesign.t_panel where id = _panel;
    
        ------------------ _refyearsets
    select
        t_available_datasets.reference_year_set into _refyearsets
    from nfiesta.t_available_datasets
    where t_available_datasets.id = (select available_datasets from nfiesta.t_additivity_set_plot where id = _t_additivity_set_plot__id);
    
        select reference_year_set into _refyearsets_label from sdesign.t_reference_year_set where id = _refyearsets;

    select add_check_threshold into _add_check_threshold from nfiesta.t_additivity_set_plot where id = _t_additivity_set_plot__id;
    ----------------------------------------------------------------------------------------------
    if array_prepend(_var_sup, _var_inf) is not null then
        _dbg_data = jsonb_build_object(
                    'add_check_start', (clock_timestamp()::timestamp with time zone)::text
                );
        if (	select 
                (target_variable is not null) and (auxiliary_variable_category is null) as is_target
            from nfiesta.t_variable 
            where id = _var_sup) 
        then 
            with w_err as (
                select nfiesta.fn_add_plot_target_attr(_var_sup, _var_inf, _panel, _refyearsets, _add_check_threshold, true) as fn_err
            )
            , w_err_t as (
                select (fn_err).*, (fn_err).variables_def = (fn_err).variables_found as def_eq_found from w_err
            )
            , w_err_json as (
                select 
                    json_build_object(
                        'plot'				, (select plot from sdesign.f_p_plot where gid = w_err_t.plot),
                        'aggregated ldsity'		, ldsity,
                        'sum of ldsity'			, ldsity_sum,
                        'variables not as defined'	, vf.variables_found_json,
                        'diff'				, diff
                    ) as errj
                from w_err_t
                                join w_var_list on w_var_list.id = w_err_t.variable
                                , lateral   (   select array_agg(w_var_list.variable) as variables_found_json 
                                                from (select unnest(variables_found) as var) as vfu 
                                                join w_var_list on w_var_list.id = vfu.var
                                                where not def_eq_found
                                            ) as vf
            )
            select json_agg(errj) into _errp from w_err_json;
        else
            with w_err as (
                select nfiesta.fn_add_plot_aux_attr(_var_sup, _var_inf, _panel, 1e-6, true) as fn_err
            )
            , w_err_t as (
                select (fn_err).* from w_err
            )
            , w_err_json as (
                select 
                    json_build_object(
                        'plot'				, plot,
                        'variable'			, variable,
                        'ldsity'			, ldsity,
                        'ldsity_sum'			, ldsity_sum,
                        'variables_def'			, variables_def,
                        'variables_found'		, variables_found,
                        'diff'				, diff
                    ) as errj
                from w_err_t
            )
            select json_agg(errj) into _errp from w_err_json;
        end if;

        _dbg_data = jsonb_set(_dbg_data,
                '{add_check_stop}', to_jsonb((clock_timestamp()::timestamp with time zone)::text)
                );
        
        IF
            (json_array_length(_errp) > 0)
            THEN RAISE WARNING 'fn_launch_add_check -- % -- % -- plot level local densities are not additive:
                variables in additivity set: sup: %, inf: % , checked panels: %, refyearsets: %,
                found err plots: returned', 'manual additivity check', 'manual additivity check', _var_sup, _var_inf, _panel_label, _refyearsets_label
                ;
                        return format('{"additivity_status":"%s", "panel":"%s", "refyearset":"%s", "aggregated variable":%s, "variables":%s, "err":%s}', 
                            'failed', 
                            _panel_label, 
                            _refyearsets_label, 
                (select variable from w_var_list where id = _var_sup),
                            (select json_agg(w_var_list.variable) as variables_def_json from w_var_list where id = ANY (_var_inf)),
                            _errp
                            )::json;
        else
            update nfiesta.t_additivity_set_plot 
            set dbg_data = jsonb_set(dbg_data, '{fn_launch_additivity_check}', _dbg_data) 
            where id = _t_additivity_set_plot__id;
                        return format('{"additivity_status":"%s", "panel":"%s", "refyearset":"%s", "aggregated variable":%s, "variables":%s}', 
                            'OK', 
                            _panel_label, 
                            _refyearsets_label,
                (select variable from w_var_list where id = _var_sup),
                            (select json_agg(w_var_list.variable) as variables_def_json from w_var_list where id = ANY (_var_inf))
                            )::json;
        END IF;
    else
        RAISE WARNING 'fn_launch_add_check -- % -- % -- additivity check was skiped (no variables found in hierarchy):
                variables in additivity set: %, checked panels: %, refyearsets: %'
                , 'manual additivity check', 'manual additivity check', 
                _vars_add_set, _panel_label, _refyearsets_label;
                return format('{"additivity_status":"%s", "panel":"%s", "refyearset":"%s", "aggregated variable":%s, "variables":%s}', 
                    'skipped', 
                    _panel_label, 
                    _refyearsets_label,
            (select variable from w_var_list where id = _var_sup),
                    (select json_agg(w_var_list.variable) as variables_def_json from w_var_list where id = ANY (_var_inf))
                    )::json;
    end if;
END$src$
LANGUAGE plpgsql
VOLATILE
COST 100000
SECURITY INVOKER;

COMMENT ON FUNCTION nfiesta.fn_launch_add_manual(integer) IS
'Function performing additivity check. Manual triggering.'; 
-- </function>