-- Copyright 2017, 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.

DROP FUNCTION IF EXISTS nfiesta.fn_api_before_delete_panel_refyearset_group(integer);

---------------------------------------------------------------------------------------------------
-- c_estimation_period => add column atomic boolean
---------------------------------------------------------------------------------------------------
ALTER TABLE nfiesta.c_estimation_period ADD COLUMN default_in_olap BOOLEAN NOT NULL DEFAULT FALSE;
COMMENT ON COLUMN nfiesta.c_estimation_period.default_in_olap IS 'The TRUE value indicates, that the nFIESTA-GUI ESTIMATES module can reduce the number of records in the OLAP cube listing the potential/existing estimates (to make the app work faster).';
---------------------------------------------------------------------------------------------------

-- <function name="fn_api_before_delete_panel_refyearset_group" schema="extschema" src="functions/extschema/configuration/fn_api_before_delete_panel_refyearset_group.sql">
-- Copyright 2017, 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: nfiesta.fn_api_before_delete_panel_refyearset_group(integer)
--DROP FUNCTION nfiesta.fn_api_before_delete_panel_refyearset_group(integer);

CREATE OR REPLACE FUNCTION nfiesta.fn_api_before_delete_panel_refyearset_group (
	_id INT, 
	out _t_aux_conf_exi boolean, out _t_total_estimate_conf_exi boolean
)
 LANGUAGE plpgsql
AS $function$
BEGIN

IF _id IS NULL THEN
	RAISE EXCEPTION 'Error 01: fn_api_before_delete_panel_refyearset_group: Function argument _id INT must not be NULL!';
END IF;

IF NOT EXISTS (SELECT * FROM nfiesta.c_panel_refyearset_group WHERE id = _id) THEN
	RAISE EXCEPTION 'Error 02: fn_api_before_delete_panel_refyearset_group: Panel with reference year set group id = % does not exist in table nfiesta.c_panel_refyearset_group and cannot be deleted.', $1;
END IF;

SELECT EXISTS (SELECT * FROM nfiesta.t_aux_conf WHERE panel_refyearset_group = _id) INTO _t_aux_conf_exi;
SELECT EXISTS (SELECT * FROM nfiesta.t_total_estimate_conf WHERE panel_refyearset_group = _id) INTO _t_total_estimate_conf_exi;

END;
$function$
;

COMMENT ON FUNCTION nfiesta.fn_api_before_delete_panel_refyearset_group(integer) IS 
'The function controls if there is a row in tables nfiesta.t_aux_conf and nfiesta.t_total_estimate_conf, '
'where panel_refyearset_group = _id passed as an argument. If at least one result column is TRUE, '
'the row with id = _id in table nfiesta.c_panel_refyearset_group cannot be deleted.';

/*
-- testing false inputs

-- passing NULL for _id
SELECT * FROM nfiesta.fn_api_before_delete_panel_refyearset_group(NULL);

-- non-existing group, no records
SELECT * FROM nfiesta.fn_api_before_delete_panel_refyearset_group(-20);

-- testing valid inputs
-- panel group 1
SELECT * FROM nfiesta.fn_api_before_delete_panel_refyearset_group(1);

-- panel group 26
SELECT * FROM nfiesta.fn_api_before_delete_panel_refyearset_group(26);
*/
-- </function>

-- <function name="fn_api_get_list_of_estimation_periods" schema="extschema" src="functions/extschema/configuration/fn_api_get_list_of_estimation_periods.sql">
-- Copyright 2017, 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: nfiesta.fn_api_get_list_of_estimation_periods()
--DROP FUNCTION nfiesta.fn_api_get_list_of_estimation_periods();

CREATE OR REPLACE FUNCTION nfiesta.fn_api_get_list_of_estimation_periods()
 RETURNS TABLE(id INT, estimate_date_begin DATE, estimate_date_end DATE, label VARCHAR(200), description TEXT, 
 label_en VARCHAR(200), description_en TEXT, default_in_olap BOOLEAN)
 LANGUAGE plpgsql
AS $function$
BEGIN

RETURN QUERY EXECUTE ' 
SELECT 
	id,
	estimate_date_begin,
	estimate_date_end,
	label,
	description,
	label_en,
	description_en,
    default_in_olap
FROM nfiesta.c_estimation_period
ORDER BY label, label_en;';
END;
$function$
;

COMMENT ON FUNCTION nfiesta.fn_api_get_list_of_estimation_periods() IS 
'The function returns id, estimate_date_begin, estimate_date_end, label, description, label_en, description_en, default_in_olap '
'from nfiesta.c_estimation_period of all estimation periods.';

/*
-- testing
SELECT * FROM nfiesta.fn_api_get_list_of_estimation_periods();
*/
-- </function>

-- <function name="fn_api_before_delete_estimation_period" schema="extschema" src="functions/extschema/configuration/fn_api_before_delete_estimation_period.sql">
-- Copyright 2017, 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: nfiesta.fn_api_before_delete_estimation_period(integer)
--DROP FUNCTION nfiesta.fn_api_before_delete_estimation_period(integer);

CREATE OR REPLACE FUNCTION nfiesta.fn_api_before_delete_estimation_period (
	_id INT, 
	out _t_total_estimate_conf_exi boolean
)
 LANGUAGE plpgsql
AS $function$
BEGIN

IF _id IS NULL THEN
	RAISE EXCEPTION 'Error 01: fn_api_before_delete_estimation_period: Function argument _id INT must not be NULL!';
END IF;

IF NOT EXISTS (SELECT * FROM nfiesta.c_estimation_period WHERE id = _id) THEN
	RAISE EXCEPTION 'Error 02: fn_api_before_delete_estimation_period: Estimation period id = % does not exist in table nfiesta.c_estimation_period and cannot be deleted.', $1;
END IF;

SELECT EXISTS (SELECT * FROM nfiesta.t_total_estimate_conf WHERE estimation_period = _id) INTO _t_total_estimate_conf_exi;

END;
$function$
;

COMMENT ON FUNCTION nfiesta.fn_api_before_delete_estimation_period(integer) IS 
'The function controls if there is a row in table nfiesta.t_total_estimate_conf, '
'where estimation_period = _id passed as an argument. If the function returns TRUE, '
'the row with id = _id in table nfiesta.c_estimation_period cannot be deleted.';

/*
-- testing false inputs

-- passing NULL for _id
SELECT * FROM nfiesta.fn_api_before_delete_estimation_period(NULL);

-- non-existing group, no records
SELECT * FROM nfiesta.fn_api_before_delete_estimation_period(-20);

-- testing valid inputs

-- estimation period 1
SELECT * FROM nfiesta.fn_api_before_delete_estimation_period(1);
*/
-- </function>

-- <function name="fn_api_delete_estimation_period" schema="extschema" src="functions/extschema/configuration/fn_api_delete_estimation_period.sql">
-- Copyright 2017, 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: nfiesta.fn_api_delete_estimation_period(integer)
--DROP FUNCTION nfiesta.fn_api_delete_estimation_period(integer);

CREATE OR REPLACE FUNCTION nfiesta.fn_api_delete_estimation_period (_id INT)
 RETURNS VOID
 LANGUAGE plpgsql
AS $function$
BEGIN

IF _id IS NULL THEN
	RAISE EXCEPTION 'Error 01: fn_api_delete_estimation_period: Function argument _id INT must not be NULL!';
END IF;

IF NOT EXISTS (SELECT * FROM nfiesta.c_estimation_period WHERE id = _id) THEN
	RAISE EXCEPTION 'Error 02: fn_api_delete_estimation_period: Estimation period with id = % does not exist in table nfiesta.c_estimation_period and cannot be deleted.', $1;
END IF;

IF EXISTS (SELECT * FROM nfiesta.t_total_estimate_conf WHERE estimation_period = _id) THEN
	RAISE EXCEPTION 'Error 03: fn_api_delete_estimation_period: Period with reference year set group id = % is referenced from table nfiesta.t_total_estimate_conf and cannot be deleted.', $1;
END IF;

DELETE FROM nfiesta.c_estimation_period WHERE id = _id;
RAISE NOTICE 'Deleting row from nfiesta.c_estimation_period with id = %.', $1;

END;
$function$
;

COMMENT ON FUNCTION nfiesta.fn_api_delete_estimation_period(integer) IS 
'The function deletes row from lookup table nfiesta.c_estimation_period, where id = _id passed as an argument.';

/*
-- testing false inputs

-- passing NULL for _id
SELECT * FROM nfiesta.fn_api_delete_estimation_period(NULL);

-- non-existing group, no records
SELECT * FROM nfiesta.fn_api_delete_estimation_period(-20);

-- testing valid inputs
SELECT * FROM nfiesta.fn_api_delete_estimation_period(1);
*/
-- </function>

-- <function name="fn_api_update_estimation_period" schema="extschema" src="functions/extschema/configuration/fn_api_update_estimation_period.sql">
-- Copyright 2017, 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: nfiesta.fn_api_update_estimation_period(INT, VARCHAR(200), TEXT, VARCHAR(200), TEXT, BOOLEAN)
--DROP FUNCTION nfiesta.fn_api_update_estimation_period(INT, VARCHAR(200), TEXT, VARCHAR(200), TEXT, BOOLEAN);

CREATE OR REPLACE FUNCTION nfiesta.fn_api_update_estimation_period (_id INT, _label VARCHAR(200), _description TEXT, 
_label_en VARCHAR(200), _description_en TEXT, _default_in_olap BOOLEAN)
 RETURNS VOID
 LANGUAGE plpgsql
AS $function$

BEGIN

IF _id IS NULL THEN
	RAISE EXCEPTION 'Error 01: fn_api_update_estimation_period: Function argument _id INT must not be NULL!';
END IF;

IF _label IS NULL THEN
	RAISE EXCEPTION 'Error 02: fn_api_update_estimation_period: Function argument _label VARCHAR(200) must not be NULL!';
END IF;

IF _description IS NULL THEN
	RAISE EXCEPTION 'Error 03: fn_api_update_estimation_period: Function argument _description TEXT must not be NULL!';
END IF;

IF _label_en IS NULL THEN
	RAISE EXCEPTION 'Error 04: fn_api_update_estimation_period: Function argument _label_en VARCHAR(200) must not be NULL!';
END IF;

IF _description_en IS NULL THEN
	RAISE EXCEPTION 'Error 05: fn_api_update_estimation_period: Function argument _description_en TEXT must not be NULL!';
END IF;

IF NOT EXISTS (SELECT * FROM nfiesta.c_estimation_period WHERE id = _id) THEN
	RAISE EXCEPTION 'Error 06: fn_api_update_estimation_period: Estimation period with id = % does not exist in table nfiesta.c_estimation_period and cannot be updated.', $1;
END IF;

IF _id <> (SELECT id FROM nfiesta.c_estimation_period WHERE label = _label) THEN
	RAISE EXCEPTION 'Error 07: fn_api_update_estimation_period: Function argument _label VARCHAR(200) already exists in table nfiesta.c_estimation_period.';
END IF;

IF _id <> (SELECT id FROM nfiesta.c_estimation_period WHERE description = _description) THEN
	RAISE EXCEPTION 'Error 08: fn_api_update_estimation_period: Function argument _description TEXT already exists in table nfiesta.c_estimation_period.';
END IF;

IF _id <> (SELECT id FROM nfiesta.c_estimation_period WHERE label_en = _label_en) THEN
	RAISE EXCEPTION 'Error 09: fn_api_update_estimation_period: Function argument _label_en VARCHAR(200) already exists in table nfiesta.c_estimation_period.';
END IF;

IF _id <> (SELECT id FROM nfiesta.c_estimation_period WHERE description_en = _description_en) THEN
	RAISE EXCEPTION 'Error 10: fn_api_update_estimation_period: Function argument _description_en TEXT already exists in table nfiesta.c_estimation_period.';
END IF;

UPDATE nfiesta.c_estimation_period 
SET 
	label = _label,
	description = _description,
	label_en = _label_en,
	description_en = _description_en,
    default_in_olap = _default_in_olap
WHERE id = _id;
RAISE NOTICE 'Updating row in nfiesta.c_estimation_period with id = %.', _id;

END;
$function$
;

COMMENT ON FUNCTION nfiesta.fn_api_update_estimation_period(INT, VARCHAR(200), TEXT, VARCHAR(200), TEXT, BOOLEAN) IS 
'The function updates label, description, label_en, description_en, default_in_olap (passed as arguments 2-6) in lookup '
'table nfiesta.c_estimation_period, where id = _id passed as an argument 1.';

/*
-- testing false inputs

-- passing NULL for arguments
SELECT * FROM nfiesta.fn_api_update_estimation_period(NULL,NULL,NULL,NULL,NULL,NULL);
SELECT * FROM nfiesta.fn_api_update_estimation_period(10,NULL,NULL,NULL,NULL,NULL);

-- non-existing group, no records
SELECT * FROM nfiesta.fn_api_update_estimation_period(-20,'new_label','new_description','new_label_en','new_description_en',false);

-- testing valid inputs

-- existing _label and _description of the same id as input _id
SELECT * FROM nfiesta.fn_api_update_estimation_period(1,'NFI2','NFI2','new_label_en','new_description_en',true);

-- group 1
SELECT * FROM nfiesta.fn_api_update_estimation_period(1,'new_label_1','new_description_1','new_label_en_1','new_description_en_1',true);
*/
-- </function>

-- <function name="fn_api_do_labels_and_descriptions_of_estimation_period_exist" schema="extschema" src="functions/extschema/configuration/fn_api_do_labels_and_descriptions_of_estimation_period_exist.sql">
-- Copyright 2017, 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: nfiesta.fn_api_do_labels_and_descriptions_of_estimation_period_exist(VARCHAR(200), VARCHAR(200), TEXT, TEXT)
-- DROP FUNCTION nfiesta.fn_api_do_labels_and_descriptions_of_estimation_period_exist(VARCHAR(200), VARCHAR(200), TEXT, TEXT);

CREATE OR REPLACE FUNCTION nfiesta.fn_api_do_labels_and_descriptions_of_estimation_period_exist(
	IN _label VARCHAR(200), IN _label_en VARCHAR(200), IN _description TEXT, IN _description_en TEXT,
	OUT _label_exists BOOLEAN, OUT _label_en_exists BOOLEAN, OUT _description_exists BOOLEAN, OUT _description_en_exists BOOLEAN)
AS
$function$
BEGIN

IF _label IS NULL THEN
	RAISE EXCEPTION 'Error 01: fn_api_do_labels_and_descriptions_of_estimation_period_exist: Function argument _label VARCHAR(200) must not be NULL!';
END IF;

IF _label_en IS NULL THEN
	RAISE EXCEPTION 'Error 02: fn_api_do_labels_and_descriptions_of_estimation_period_exist: Function argument _label_en VARCHAR(200) must not be NULL!';
END IF;

IF _description IS NULL THEN
	RAISE EXCEPTION 'Error 03: fn_api_do_labels_and_descriptions_of_estimation_period_exist: Function argument _description TEXT must not be NULL!';
END IF;

IF _description_en IS NULL THEN
	RAISE EXCEPTION 'Error 04: fn_api_do_labels_and_descriptions_of_estimation_period_exist: Function argument _description_en TEXT must not be NULL!';
END IF;

SELECT EXISTS(SELECT id FROM nfiesta.c_estimation_period WHERE label = _label) INTO _label_exists;
SELECT EXISTS(SELECT id FROM nfiesta.c_estimation_period WHERE label_en = _label_en) INTO _label_en_exists;
SELECT EXISTS(SELECT id FROM nfiesta.c_estimation_period WHERE description = _description) INTO _description_exists;
SELECT EXISTS(SELECT id FROM nfiesta.c_estimation_period WHERE description_en = _description_en) INTO _description_en_exists;

END;
$function$
LANGUAGE plpgsql
PARALLEL SAFE;

COMMENT ON FUNCTION nfiesta.fn_api_do_labels_and_descriptions_of_estimation_period_exist(VARCHAR(200), VARCHAR(200), TEXT, TEXT) IS 
'For each of the function arguments (label, label_en, description, description_en) the function checks '
'whether a record with identical value of the argument exists in the codelist of estimation periods. '
'The function returns four Booleans. The TRUE value indicates that a period with the same value '
'of the respective argument was found, otherwise FALSE is returned.';

/*
-- testing false inputs

-- passing NULL for _label
SELECT * FROM nfiesta.fn_api_do_labels_and_descriptions_of_estimation_period_exist(
	NULL, 'new_label_en_1', 'new_description_1', 'new_description_en_1');

-- passing NULL for _label_en
SELECT * FROM nfiesta.fn_api_do_labels_and_descriptions_of_estimation_period_exist(
	'new_label_1', NULL, 'new_description_1', 'new_description_en_1');

-- passing NULL for _description
SELECT * FROM nfiesta.fn_api_do_labels_and_descriptions_of_estimation_period_exist(
	'new_label_1', 'new_label_en_1', NULL, 'new_description_en_1');

-- passing NULL for _description
SELECT * FROM nfiesta.fn_api_do_labels_and_descriptions_of_estimation_period_exist(
	'new_label_1', 'new_label_en_1', 'new_description_1', NULL);

-- testing valid inputs

-- all found, all TRUE
SELECT * FROM nfiesta.fn_api_do_labels_and_descriptions_of_estimation_period_exist(
	'new_label_1', 'new_label_en_1', 'new_description_1', 'new_description_en_1');
	
-- first found (TRUE), the rest FALSE
SELECT * FROM nfiesta.fn_api_do_labels_and_descriptions_of_estimation_period_exist(
	'new_label_1', 'no_label_en', 'no_description', 'no_description_en');
	
*/
-- </function>

-- <function name="fn_api_save_estimation_period" schema="extschema" src="functions/extschema/configuration/fn_api_save_estimation_period.sql">
-- Copyright 2017, 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.

-- Function: nfiesta.fn_api_save_estimation_period(DATE, DATE, VARCHAR(200), TEXT, VARCHAR(200), TEXT, BOOLEAN)
-- DROP FUNCTION nfiesta.fn_api_save_estimation_period(DATE, DATE, VARCHAR(200), TEXT, VARCHAR(200), TEXT, BOOLEAN);

CREATE OR REPLACE FUNCTION nfiesta.fn_api_save_estimation_period(_estimate_date_begin DATE, _estimate_date_end DATE, _label VARCHAR(200), _description TEXT, 
	_label_en VARCHAR(200), _description_en TEXT, _default_in_olap BOOLEAN)
RETURNS INT
AS
$function$
DECLARE
_estimation_period		integer;
BEGIN

-- testing input parameters if not NULL
IF _estimate_date_begin IS NULL THEN
	RAISE EXCEPTION 'Error 01: fn_api_save_estimation_period: Function argument _estimate_date_begin DATE must not be NULL!';
END IF;

IF _estimate_date_end IS NULL THEN
	RAISE EXCEPTION 'Error 02: fn_api_save_estimation_period: Function argument _estimate_date_end DATE must not be NULL!';
END IF;

IF _label IS NULL THEN
	RAISE EXCEPTION 'Error 03: fn_api_save_estimation_period: Function argument _label VARCHAR(200) must not be NULL!';
END IF;

IF _description IS NULL THEN
	RAISE EXCEPTION 'Error 04: fn_api_save_estimation_period: Function argument _description TEXT must not be NULL!';
END IF;


IF _label_en IS NULL THEN
	RAISE EXCEPTION 'Error 05: fn_api_save_estimation_period: Function argument _label_en VARCHAR(200) must not be NULL!';
END IF;

IF _description_en IS NULL THEN
	RAISE EXCEPTION 'Error 06: fn_api_save_estimation_period: Function argument _description_en TEXT must not be NULL!';
END IF;

INSERT INTO nfiesta.c_estimation_period (estimate_date_begin, estimate_date_end, label, description, label_en, description_en, default_in_olap)
SELECT _estimate_date_begin, _estimate_date_end, _label, _description, _label_en, _description_en, _default_in_olap
RETURNING id INTO _estimation_period;

RETURN _estimation_period;

END;
$function$
LANGUAGE plpgsql
PARALLEL SAFE;

COMMENT ON FUNCTION  nfiesta.fn_api_save_estimation_period(DATE, DATE, VARCHAR(200), TEXT, VARCHAR(200), TEXT, BOOLEAN) IS 
'The function insert row into lookup table nfiesta.c_estimation_period. The columns estimate_date_begin, '
'estimate_date_end, label, description, label_en, description_en, default_in_olap are passed as input arguments '
'of the function. The function returns a unique identifier of the newly inserted estimation period.';

/* tests
-------------------------------------------------------------------------------------------------------------------------------------------------------
-- tests for function nfiesta.fn_api_save_estimation_period(_estimate_date_begin DATE, _estimate_date_end DATE, _label VARCHAR(200), _description TEXT, 
--	_label_en VARCHAR(200), _description_en TEXT, default_in_olap BOOLEAN
-------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------- testing invalid inputs ------------------------------------------------------------------------------
-- test NULL for _estimate_date_begin argument
SELECT * FROM nfiesta.fn_api_save_estimation_period(NULL, '2015-12-31', 'nejaky label', 'nejaky description', 'some label en', 'some description_en', NULL);

-- test NULL for _estimate_date_end argument
SELECT * FROM nfiesta.fn_api_save_estimation_period('2011-01-01', NULL, 'nejaky label', 'nejaky description', 'some label en', 'some description_en', NULL);

-- passing NULL for _label
SELECT * FROM nfiesta.fn_api_save_estimation_period('2011-01-01', '2015-12-31', NULL, 'nejaky description', 'some label en', 'some description_en', NULL);

-- passing NULL for _description
SELECT * FROM nfiesta.fn_api_save_estimation_period('2011-01-01', '2015-12-31', 'nejaky label', NULL, 'some label en', 'some description_en', NULL);

-- passing NULL for _label_en
SELECT * FROM nfiesta.fn_api_save_estimation_period('2011-01-01', '2015-12-31', 'nejaky label', 'nejaky description', NULL, 'some description_en', NULL);

-- passing NULL for _description_en
SELECT * FROM nfiesta.fn_api_save_estimation_period('2011-01-01', '2015-12-31', 'nejaky label', 'nejaky description', 'some label en', NULL, NULL);

-- test if saving a estimation period with begin date and end date already used 
SELECT * FROM nfiesta.fn_api_save_estimation_period('2011-01-01', '2015-12-31', 'nejaky label', 'nejaky description', 'some label en', 'some description_en', false);

------------------------------------------------ testing valid inputs ---------------------------------------------------------------------------------
SELECT * FROM nfiesta.fn_api_save_estimation_period('2016-01-01', '2020-12-31', 'NIL3', '3. cyklus NIL', 'NFI3', '3rd cycle of czech NFI', true);
*/
-- </function>