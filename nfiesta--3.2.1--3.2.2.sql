--
-- Copyright 2017, 2023 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- <function name="fn_get_num_denom_variables" schema="extschema" src="functions/extschema/configuration/fn_get_num_denom_variables.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_get_num_denom_variables.sql(integer, integer)
--DROP FUNCTION @extschema@.fn_get_num_denom_variables.sql(integer, integer);
CREATE OR REPLACE FUNCTION @extschema@.fn_get_num_denom_variables(
		_num_var integer[], _denom_var integer[] DEFAULT NULL::int[])
RETURNS TABLE (
variable_numerator 	integer,
variable_denominator 	integer
)
AS
$function$
BEGIN
	IF _num_var IS NULL
	THEN
		RAISE EXCEPTION 'Given array of numerator variables is NULL!';
	END IF;

	IF (SELECT count(*) FROM @extschema@.t_variable WHERE array[id] <@ _num_var) != array_length(_num_var,1)
	THEN
		RAISE EXCEPTION 'Not all of numerator variables are present in table t_variable.';
	END IF;

	IF (SELECT count(*) FROM @extschema@.t_variable WHERE array[id] <@ _denom_var) != array_length(_denom_var,1)
	THEN
		RAISE EXCEPTION 'Not all of denominator variables are present in table t_variable.';
	END IF;

	IF array_length(_num_var,1) < array_length(_denom_var,1)
	THEN
		RAISE EXCEPTION 'Given array of denominator variables must be shorter (or equal) then given array of numerator variables.';
	END IF;

	IF _denom_var IS NULL
	THEN
		RETURN QUERY EXECUTE
		'SELECT t1.variable_num, NULL::integer AS variable_denom
		FROM unnest($1) AS t1(variable_num)'
		USING _num_var;
	ELSE
		RETURN QUERY EXECUTE
		'
		WITH w_numerator AS (
			SELECT 
				t1.id AS variable_num, 
				--t3.id AS adc, 
				--t3.label AS adc_label, 
				array_agg(DISTINCT coalesce(t5.atomic_category,t3.id) 
					ORDER BY coalesce(t5.atomic_category,t3.id)) FILTER
					(WHERE coalesce(t5.atomic_category,t3.id) IS NOT NULL) AS area_domain_category, 
				array_agg(DISTINCT coalesce(t9.atomic_category,t7.id) 
					ORDER BY coalesce(t9.atomic_category, t7.id)) FILTER
					(WHERE coalesce(t9.atomic_category,t7.id) IS NOT NULL) AS sub_population_category
			FROM	@extschema@.t_variable AS t1
			INNER JOIN
				unnest($1) AS t2(var_num)
			ON t1.id = t2.var_num
			LEFT JOIN @extschema@.c_area_domain_category AS t3
			ON t1.area_domain_category = t3.id
			LEFT JOIN @extschema@.c_area_domain AS t4
			ON t3.area_domain = t4.id
			LEFT JOIN @extschema@.cm_area_domain_category AS t5
			ON t3.id = t5.area_domain_category
			LEFT JOIN @extschema@.c_area_domain_category AS t6
			ON t5.atomic_category = t6.id
			LEFT JOIN @extschema@.c_sub_population_category AS t7
			ON t1.sub_population_category = t7.id
			LEFT JOIN @extschema@.c_sub_population AS t8
			ON t7.sub_population = t8.id
			LEFT JOIN @extschema@.cm_sub_population_category AS t9
			ON t7.id = t9.sub_population_category
			LEFT JOIN @extschema@.c_sub_population_category AS t10
			ON t9.atomic_category = t10.id
			GROUP BY t1.id
		)
		--denominator
		, w_denominator AS (
			SELECT 
				t1.id AS variable_denom, 
				array_agg(DISTINCT coalesce(t5.atomic_category,t3.id) 
					ORDER BY coalesce(t5.atomic_category,t3.id)) FILTER
					(WHERE coalesce(t5.atomic_category,t3.id) IS NOT NULL) AS area_domain_category, 
				array_agg(DISTINCT coalesce(t9.atomic_category,t7.id) 
					ORDER BY coalesce(t9.atomic_category, t7.id)) FILTER
					(WHERE coalesce(t9.atomic_category,t7.id) IS NOT NULL) AS sub_population_category
			FROM	@extschema@.t_variable AS t1
			INNER JOIN
				unnest($2) AS t2(var_denom)
			ON t1.id = t2.var_denom
			LEFT JOIN @extschema@.c_area_domain_category AS t3
			ON t1.area_domain_category = t3.id
			LEFT JOIN @extschema@.c_area_domain AS t4
			ON t3.area_domain = t4.id
			LEFT JOIN @extschema@.cm_area_domain_category AS t5
			ON t3.id = t5.area_domain_category
			LEFT JOIN @extschema@.c_area_domain_category AS t6
			ON t5.atomic_category = t6.id
			LEFT JOIN @extschema@.c_sub_population_category AS t7
			ON t1.sub_population_category = t7.id
			LEFT JOIN @extschema@.c_sub_population AS t8
			ON t7.sub_population = t8.id
			LEFT JOIN @extschema@.cm_sub_population_category AS t9
			ON t7.id = t9.sub_population_category
			LEFT JOIN @extschema@.c_sub_population_category AS t10
			ON t9.atomic_category = t10.id
			GROUP BY t1.id
		), w_final_exact_match AS (

		-- this is the exact pair, where categories from numerator
		-- are the same as categories from denominator (both ad and sp)
			SELECT 	t1.variable_num, t2.variable_denom, 
				--t1.adc_label, t2.adc_label, 
				--t1.spc_label, t2.spc_label
				t1.area_domain_category, t2.area_domain_category,
				t1.sub_population_category, t2.sub_population_category,
				array_length(t1.area_domain_category,1) AS num_ad_dim,
				array_length(t2.area_domain_category,1) AS denom_ad_dim,
				array_length(t1.sub_population_category,1) AS num_sp_dim,
				array_length(t2.sub_population_category,1) AS denom_sp_dim
			FROM
				w_numerator AS t1
			INNER JOIN
				w_denominator AS t2
			ON
				coalesce(t1.area_domain_category,array[0]) = 
				coalesce(t2.area_domain_category,array[0]) AND
				coalesce(t1.sub_population_category,array[0]) = 
				coalesce(t2.sub_population_category,array[0])
		), w_final_rest_match AS (
		-- the rest, where the categories from denominator are the subset of the
		-- categories from numerator - valid for both ad and sp
			SELECT 	t1.variable_num, t2.variable_denom, 
				--t1.adc_label, t2.adc_label, 
				--t1.spc_label, t2.spc_label
				t1.area_domain_category, t2.area_domain_category,
				t1.sub_population_category, t2.sub_population_category,
				array_length(t1.area_domain_category,1) AS num_ad_dim,
				array_length(t2.area_domain_category,1) AS denom_ad_dim,
				array_length(t1.sub_population_category,1) AS num_sp_dim,
				array_length(t2.sub_population_category,1) AS denom_sp_dim
			FROM
				w_numerator AS t1
			INNER JOIN
				w_denominator AS t2
			ON
				(coalesce(t1.sub_population_category,array[0]) = coalesce(t2.sub_population_category,array[0]) AND 
				(t1.area_domain_category @> t2.area_domain_category OR
				(t1.area_domain_category IS NOT NULL AND t2.area_domain_category IS NULL))
				) OR
				(coalesce(t1.area_domain_category,array[0]) = coalesce(t2.area_domain_category,array[0]) AND 
				(t1.sub_population_category @> t2.sub_population_category OR
				(t1.sub_population_category IS NOT NULL AND t2.sub_population_category IS NULL))
				)
			WHERE t1.variable_num NOT IN (SELECT variable_num FROM w_final_exact_match)
		), w_final AS (
			SELECT 	*, 
				(coalesce(num_ad_dim,0) - coalesce(denom_ad_dim,0)) AS ad_diff,
				(coalesce(num_sp_dim,0) - coalesce(denom_sp_dim,0)) AS sp_diff
			FROM w_final_exact_match
			UNION ALL
			SELECT 	*,
				(coalesce(num_ad_dim,0) - coalesce(denom_ad_dim,0)) AS ad_diff,
				(coalesce(num_sp_dim,0) - coalesce(denom_sp_dim,0)) AS sp_diff
			FROM w_final_rest_match
		), w_topfinal AS (
			SELECT	*, 
				min(ad_diff) OVER (PARTITION BY variable_num) AS min_ad_diff,
				min(sp_diff) OVER (PARTITION BY variable_num) AS min_sp_diff
			FROM w_final
		)
		--SELECT * FROM w_numerator;
		--SELECT * FROM w_denominator;
		--SELECT * FROM w_final_exact_match;
		--SELECT * FROM w_final_rest_match;
		SELECT variable_num, variable_denom FROM w_topfinal 
		WHERE 
			(ad_diff = min_ad_diff OR ad_diff IS NULL) AND
			(sp_diff = min_sp_diff OR sp_diff IS NULL)
		ORDER BY variable_num
		'
		USING _num_var, _denom_var;
	END IF;
END;
$function$
LANGUAGE plpgsql
PARALLEL SAFE;

COMMENT ON FUNCTION @extschema@.fn_get_num_denom_variables(integer[],integer[]) IS 'Function returns table with assigned denominator variables to numerator variables.';

-- </function>

