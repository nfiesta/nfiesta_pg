# This is a minimalistinc script demonstrating the nfiesta package 
# that implemented the same type of estimators as the core nFIESTA postgresql 
# extention (nfiesta_pg). The purpose of the package was just a parallel, 
# independent  implementation of the same estimators to validate estimtion 
# results produced by nfiesta_pg extension. A larga number of various results 
# has been configured and calculated by both implementations. Dicrepancies were 
# clarified, bugs removed so finaly the an two set of numerically identical 
# estimates  were obtained for the same input data.
# These estimates were then hardwired into nfiesta_pg by a number of regression
# tests. 

# download nfiesta_data.RData from
  # https://gitlab.com/nfiesta/nfiesta_pg/-/tree/main/r_test
# downloadd nfiesta package in nfiesta_1.3.tar.gz format from
  #https://gitlab.com/nfiesta/nfiesta_pg/-/tree/main/r_test

# install Rtools manualy first
install.packages("RPostgreSQL")

# install the nfiesta package
install.packages("D:/Users/KMAdolt/Downloads/nfiesta_1.3.tar.gz", 
                 repos = NULL, type = "source")

# load data frame contained testing data and estimates configuration based 
# on Czech NFI example data, see: 
# https://gitlab.com/nfiesta/nfiesta_pg/-/wikis/Minimal-Working-Example#data-used-in-minimal-working-example

# first set your working  directory to where you saved the 
# nfiesta_data.RData file, e.g.: 
# setwd("D:/Users/KMAdolt/Downloads")

load("nfiesta_data.RData")

# load nfiesta package
library(nfiesta)

# run first 4 of the list of estimates configured in the data 'a' (orifginally 
# obtained from nfista_pg databse, but here uploaded from the .RData file) 
# silent option
fnEstimate(data=a, estimate = 1:4, verbose=FALSE)

# verbose option
fnEstimate(data=a, estimate = 1:4, verbose=TRUE)

# Note, that the R package was done just for sake of quality control 
# of the main implementation in Postgresql extensions and GUI modules. This 
# package contains just a small fraction of functionality of the current 
# nFIESTA modules and it is not very convenient for the user.

