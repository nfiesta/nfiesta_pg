1) install R:

	sudo apt-get update
	sudo apt-get install r-base r-base-dev

2)	install required package(s) in R:
	
	sudo R
	
	# run R as root and in R Console type:
	
	> install.packages("RPostgreSQL");
	> q();
	
3) install nfiesta package:

	sudo R CMD INSTALL nfiesta_1.2.tar.gz

4) basic usage of nfiesta package:

	R
	
	# run R as current user and in R Console type:
	
	> library(nfiesta);				# this code loads nfiesta package functions
	> data <- fnLoadData();				# this code connects to nfiesta database and loads data for estimate computation
	> fnEstimate(data, 143, verbose = TRUE);	# this code calculates estimate number 143 from data and prints results on screen
	> fnEstimate(data, 628, verbose = TRUE);	# this code calculates estimate number 628 from data and prints results on screen
