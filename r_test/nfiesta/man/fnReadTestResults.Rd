\name{fnReadTestResults}
\alias{fnReadTestResults}
\title{Read nfiesta results}
\description{Function fnReadTestResults loads file that comes from nfiesta system.
This file is used for comparision and validation nfiesta system results with control results calculated in R.}
\usage{fnReadTestResults(file)}
\arguments{
	\item{file}{Path to text file with results from nfiesta system.}
}
\details{}
\value{
Function returns data.frame with columns:
	\item{est_id}{Estimate identifier}
	\item{point}{Point estimate}
	\item{var}{Variance estimate}
}
\references{Adolt,R., Fejfar,J., Lanz,A. 2019. nFIESTA (new Forest Inventory ESTimation and Analysis) Estimation methods}
\author{\packageAuthor{nfiesta}}
\note{}
\seealso{}
\examples{
	FullStackTest <- fnReadTestResults(file = "D:/GITLAB/nfiesta_pg_uhul/expected/full_stack_test_1p_total.out");
}