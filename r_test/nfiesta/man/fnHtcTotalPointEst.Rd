\name{fnHtcTotalPointEst}
\alias{fnHtcTotalPointEst}
\title{Single-phase estimator of the total (point estimate)}
\description{Function returns single-phase estimator of the total for given estimate identifier.}
\usage{fnHtcTotalPointEst(data, estimate, aux = NA, verbose = FALSE, inList = FALSE)}
\arguments{
	\item{data}{Input data (list of data.frames obtained by function fnLoadData)}
	\item{estimate}{Estimate identifier}
	\item{aux}{Auxiliary variable identifier, default NA value determines main variable}
	\item{verbose}{Boolean - TRUE|FALSE - function prints (does not print) its results on R console and returns NULL}
	\item{inList}{Boolean - TRUE - function returns (does not return) all partial results in list object for testing nfiesta system, FALSE (default) - function returns only main result (single-phase estimation of the total) in data.frame object}
}
\details{}
\value{
When inList argument is FALSE then function returns data.frame with columns:
	\item{cell_id}{Estimation cell identifier}
	\item{cell_name}{Estimation cell name}
	\item{total}{Single-phase estimator of the total}
When inList argument is TRUE then function returns list with items:
	\item{data}{Data selected for estimate computation (list of data.frames obtained by function fnSelectData)}
	\item{ldsity}{Data.frame with cluster level local densities for estimation cell}
	\item{ldsities}{List of data.frames with cluster level local densities for each estimation cell and stratum intersection}
	\item{idsity}{Data.frame with cluster level inclusion densities}
	\item{clusters}{Merged data.frame of clusters}
	\item{strata}{Data.frame of partial single-phase estimations of total and variance for each estimation cell and stratum intersection}
	\item{initialized}{Boolean - is data.frame of cluster level local densities for estimation cell initialized}
	\item{sum_ldsity_d}{Check sum of cluster level local densities of clusters inside estimation cell}
	\item{sum_pix_d}{Check sum of cluster level inclusion densities of clusters inside estimation cell}
	\item{sum_var_a}{First part of the sum in variance estimation  [Equation number 30], value is not initialized in this function, NA value}
	\item{sum_var_b}{Second part of the sum in variance estimation [Equation number 30], value is not initialized in this function, NA value}
	\item{estimate_id}{Estimate identifier}
	\item{estimate_name}{Estimate name}
	\item{cell_id}{Estimation cell identifier}
	\item{cell_name}{Estimation cell name}
	\item{var_id}{Main (or auxiliary) variable identifier}
	\item{var_name}{Main (or auxiliary) variable name}
	\item{phase_id}{Estimate phase identifier}
	\item{phase_name}{Estimate phase name}
	\item{time_start}{Time when estimate computation starts}
	\item{time_end}{Time when estimate computation ends}
	\item{est_time}{Length of time for estimate computation}
	\item{total}{Single-phase estimation of the total}
	\item{var}{Single-phase estimation of the total variance, value is not initialized in this function, NA value}
}
\references{Adolt,R., Fejfar,J., Lanz,A. 2019. nFIESTA (new Forest Inventory ESTimation and Analysis) Estimation methods}
\author{\packageAuthor{nfiesta}}
\note{}
\seealso{}
\examples{
	fnHtcTotalPointEst(data = NFiestaData, estimate =   1, verbose = TRUE);
	fnHtcTotalPointEst(data = NFiestaData, estimate =   1, aux = 5, verbose = TRUE);
}