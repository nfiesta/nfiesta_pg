\name{fnRelativeWeightsSum}
\alias{fnRelativeWeightsSum}
\title{Sum of cluster relative weights inside estimation cell and stratum intersection (or inside stratum)}
\description{}
\usage{fnRelativeWeightsSum(data, cell, stratum)}
\arguments{
	\item{data}{Input data (list of data.frames obtained for estimate identifier by function fnSelectData)}
	\item{cell}{Estimation cell identifier, when NA, then function returns sum of cluster relative weights inside stratum}
	\item{stratum}{Stratum identifier}
}
\details{}
\value{Function returns sum of cluster relative weights inside estimation cell and stratum intersection or inside stratum}
\references{Adolt,R., Fejfar,J., Lanz,A. 2019. nFIESTA (new Forest Inventory ESTimation and Analysis) Estimation methods}
\author{\packageAuthor{nfiesta}}
\note{}
\seealso{}
\examples{
	fnRelativeWeightsSum (data = fnSelectData(data = NFiestaData, estimate =   1), cell =   2, stratum = 2);
	fnRelativeWeightsSum (data = fnSelectData(data = NFiestaData, estimate =   1), cell =  NA, stratum = 2);
}