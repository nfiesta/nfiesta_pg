\name{fnEstimate}
\alias{fnEstimate}
\title{General function for estimate computation}
\description{This function calculates all types of estimates.
Estimate type and phase is selected according to estimate identifier and its configuration stored in nfiesta database.
Function also makes possible to define and calculate set of estimates.
}
\usage{fnEstimate(data, estimate, verbose = FALSE)}
\arguments{
	\item{data}{Input data (list of data.frames obtained by function fnLoadData)}
	\item{estimate}{Estimate identifier or array of estimate identifiers}
	\item{verbose}{Boolean - TRUE|FALSE - function prints (does not print) its results on R console and returns NULL}
}
\details{}
\value{
Function returns list of two data.frames.
Data.frame Total contains results of total estimators with columns:
	\item{est_id}{Estimate identifier}
	\item{est_name}{Estimate name}
	\item{cell_id}{Estimation cell identifier}
	\item{cell_name}{Estimation cell name}
	\item{total}{Estimator of the total}
	\item{var}{Estimator of the total variance}
Data.frame Ratio contains results of ratio estimators with columns:
	\item{est_id}{Estimate identifier}
	\item{est_name}{Estimate name}
	\item{cell_id}{Estimation cell identifier}
	\item{cell_name}{Estimation cell name}
	\item{numerator}{Estimate of total in numerator}
	\item{denominator}{Estimate of total in denominator}
	\item{ratio}{Estimate of ratio}
	\item{var}{Estimate of ratio variance}
}
\references{Adolt,R., Fejfar,J., Lanz,A. 2019. nFIESTA (new Forest Inventory ESTimation and Analysis) Estimation methods}
\author{\packageAuthor{nfiesta}}
\note{}
\seealso{}
\examples{
	results <- fnEstimate(data = NFiestaData, estimate = 1:4, verbose = FALSE);
	results <- fnEstimate(data = NFiestaData, estimate = c(1,2,3,4), verbose = FALSE);
	results <- fnEstimate(data = NFiestaData, estimate = c(1:4,305:307), verbose = FALSE);
}