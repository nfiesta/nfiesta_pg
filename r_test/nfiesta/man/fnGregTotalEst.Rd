\name{fnGregTotalEst}
\alias{fnGregTotalEst}
\title{Generalized regression estimator of the total (point estimate + variance estimate)}
\description{Function returns generalized regression estimator of the total and variance of the total for given estimate identifier.}
\usage{fnGregTotalEst(data, estimate, verbose = FALSE, inList = FALSE)}
\arguments{
	\item{data}{Input data (list of data.frames obtained by function fnLoadData)}
	\item{estimate}{Estimate identifier}
	\item{verbose}{Boolean - TRUE|FALSE - function prints (does not print) its results on R console and returns NULL}
	\item{inList}{Boolean - TRUE - function returns (does not return) all partial results in list object for testing nfiesta system, FALSE (default) - function returns only main result (generalized regression estimator of the total) in data.frame object}
}
\details{}
\value{
When inList argument is FALSE then function returns data.frame with columns:
	\item{cell_id}{Estimation cell identifier}
	\item{cell_name}{Estimation cell name}
	\item{total}{Generalized regression estimator of the total}
	\item{var}{Generalized regression estimator of the total variance}
When inList argument is TRUE then function returns list with items:
	\item{data}{Data selected for estimate computation (list of data.frames obtained by function fnSelectData)}
	\item{model}{Data.frame of known totals (model values) of auxiliary variable for estimation cell}
	\item{ldsity}{Data.frame with cluster level local densities for estimation cell}
	\item{idsity}{Data.frame with cluster level inclusion densities for estimation cell}
	\item{clusters}{Merged data.frame of clusters}
	\item{aux}{List of auxiliary variables}
	\item{strata}{Data.frame of check sums for each estimation cell and stratum intersection}
	\item{ItD}{(1 x nD+) Row matrix of indicator value whether a cluster belongs to estimation cell}
	\item{Pip}{(nD+ x nD+) Diagnoal matrix of sampling weights}
	\item{Sigma}{(nD+ x nD+) Diagnoal matrix of weights}
	\item{YtD}{(1 x nD+) Row matrix of cluster level local densities of main variable inside estimation cell}
	\item{YtP}{(1 x nD+) Row matrix of cluster level local densities of main variable inside parametrization area}
	\item{XtD}{(p x nD+) Matrix of cluster level local densities of auxiliary variables inside estimation cell}
	\item{XtP}{(p x nD+) Matrix of cluster level local densities of auxiliary variables inside parametrization area}
	\item{ty}{(1 x 1) Matrix of single-phase estimator of the total of the main variable}
	\item{tx}{(1 x p) Row matrix of the known totals of the auxiliary variables}
	\item{txm}{(1 x p) Row matrix of single-phase estimators of the totals of the auxiliary variables}
	\item{Delta}{(1 x p) Row matrix of differences between known totals and single-phase estimators of the totals of the auxiliary variables}
	\item{Regular}{(p x p) Regular matrix for calculation inverse matrix}
	\item{Inv}{ (p x p) Inverse matrix}
	\item{Beta}{(1 x p) Row matrix of regression coeficients}
	\item{GBeta}{(p x nD+) GBeta matrix}
	\item{ConditionNumber}{Condition number}
	\item{IsRegular}{Boolean - is matrix for calculation inverse matrix non-singular}
	\item{string_delta}{Row matrix of differences between known totals and single-phase estimators of the totals of the auxiliary variables converted into text}
	\item{string_beta}{Row matrix of regression coeficients converted into text}
	\item{sum_ldsity_d}{Check sum of cluster level local densities of clusters inside estimation cell}
	\item{sum_ldsity_p}{Check sum of cluster level local densities of clusters inside parametrization area}
	\item{sum_sweights_d}{Check sum of weights of clusters inside estimation cell}
	\item{sum_sweights_p}{Check sum of weights of clusters inside parametrization area}
	\item{sum_pix_d}{Check sum of cluster level inclusion densities of clusters inside estimation cell}
	\item{sum_pix_p}{Check sum of cluster level inclusion densities of clusters inside parametrization area}
	\item{sum_et_d}{Check sum of cluster level residuals of clusters inside estimation cell}
	\item{sum_et_p}{Check sum of cluster level residuals of clusters inside parametrization area}
	\item{sum_gbeta}{Check sum of all elements in GBeta matrix}
	\item{sum_var_a}{First part of the sum in variance estimation  [Equation number 30]}
	\item{sum_var_b}{Second part of the sum in variance estimation [Equation number 30]}
	\item{estimate_id}{Estimate identifier}
	\item{estimate_name}{Estimate name}
	\item{cell_id}{Estimation cell identifier}
	\item{cell_name}{Estimation cell name}
	\item{param_id}{Parametrization area identifier}
	\item{param_name}{Parametrization area name}
	\item{model_id}{Model identifier}
	\item{model_name}{Model name}
	\item{model_sigma}{Boolean - when FALSE then diagnoal matrix of weights Sigma is identity matrix, when TRUE then diagonal elements of Sigma are calculated from equation number 79}
	\item{var_id}{Main variable identifier}
	\item{var_name}{Main variable name}
	\item{phase_id}{Estimate phase identifier}
	\item{phase_name}{Estimate phase name}
	\item{time_start}{Time when estimate computation starts}
	\item{time_end}{Time when estimate computation ends}
	\item{est_time}{Length of time for estimate computation}
	\item{total}{Generalized regression estimator of the total}
	\item{var}{Generalized regression estimator of the total variance}
}
\references{Adolt,R., Fejfar,J., Lanz,A. 2019. nFIESTA (new Forest Inventory ESTimation and Analysis) Estimation methods}
\author{\packageAuthor{nfiesta}}
\note{}
\seealso{}
\examples{
	fnGregTotalEst(data = NFiestaData, estimate = 533, verbose = TRUE);
}