#
# Copyright 2017, 2022 ÚHÚL
#
# Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
# You may not use this work except in compliance with the Licence.
# You may obtain a copy of the Licence at:
#
# https://joinup.ec.europa.eu/software/page/eupl
#
# Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the Licence for the specific language governing permissions and limitations under the Licence.
#

fnLoadData <-
function(host = NA, port = NA, dbname = NA, schema = NA, user = NA, password = NA)
{
#Function parameters:
#host     <- "127.0.0.1";# IP adress of database server
#port     <- "5434";# TCP port of database server
#dbname   <- "contrib_regression";# Database name
#schema   <- "nfiesta";# Database schema name
#user     <- "vagrant";# User name
#password <- "vagrant";# User password

#rm (host, port, dbname, schema, user, password);
# rm(a, drv, con, panel, conf);

########################################################################################
# Return type - initialization

a       <- list();

a$SqlPlots <- NA;
a$SqlTarget <- NA;
a$SqlAux <- NA;
a$SqlCell<- NA;
a$SqlParam <- NA;
a$SqlModel <- NA;
a$SqlPanel <- NA;
a$SqlConf <- NA;

a$Plots <- NA;
a$Target <- NA;
a$Aux <- NA;
a$Cell<- NA;
a$Param <- NA;
a$Model <- NA;
a$Panel <- NA;
a$Conf <- NA;

##############################################################################################
# SQL statements

# Inventory plots list
a$SqlPlots <- paste(
"WITH w_plots AS",
"(",
"SELECT",
"c.id                                           AS cluster_id,",
"p.gid                                          AS plot_id,",
"cl.sampling_weight                             AS chi,",
"g.plots_per_cluster                            AS k,",
"0.0001 * s.area_m2                             AS lambda_s,",
"0.0001 * s.buffered_area_m2                    AS lambda_f,",
"CASE WHEN (g.plots_per_cluster IS NOT DISTINCT FROM 1)",
"     THEN 0.0001 * s.area_m2",
"ELSE 0.0001 * s.buffered_area_m2 END      AS lambda,",
"l.id                                           AS panel_id,",
"s.id                                           AS stratum_id,",
"u.id                                           AS strata_set_id,",
"a.id                                           AS country_id,",
"coalesce(y.id,0)                               AS ref_year_id,",
"i.id                                           AS inventory_id,",
"c.cluster                                      AS cluster_name,",
"p.plot                                         AS plot_name,",
"l.panel                                        AS panel_name,",
"s.stratum                                      AS stratum_name,",
"u.strata_set                                   AS strata_set_name,",
"a.label                                        AS country_name,",
"y.label                                        AS ref_year_name,",
"i.label                                        AS inventory_name",
"FROM %schema%.f_p_plot                               AS p",
"LEFT JOIN %schema%.t_cluster                         AS c  ON (c.id = p.cluster)",
"LEFT JOIN %schema%.cm_cluster2panel_mapping          AS cl ON (cl.cluster = c.id)",
"LEFT JOIN %schema%.t_panel                           AS l  ON (l.id = cl.panel)",
"LEFT JOIN %schema%.t_stratum                         AS s  ON (s.id = l.stratum)",
"LEFT JOIN %schema%.t_strata_set                      AS u  ON (u.id = s.strata_set)",
"LEFT JOIN %schema%.c_country                         AS a  ON (a.id = u.country)",
"LEFT JOIN %schema%.cm_plot2cluster_config_mapping    AS pg ON (pg.plot = p.gid)",
"LEFT JOIN %schema%.t_cluster_configuration           AS g  ON (g.id = pg.cluster_configuration)",
"LEFT JOIN %schema%.cm_refyearset2panel_mapping       AS yg ON (yg.panel = l.id)",
"LEFT JOIN %schema%.t_reference_year_set              AS y  ON (y.id = yg.reference_year_set)",
"LEFT JOIN %schema%.t_inventory_campaign              AS i  ON (i.id = y.inventory_campaign)",
")",
"SELECT cluster_id, plot_id, chi, k, lambda_s, lambda_f, lambda,",
"       panel_id, stratum_id, strata_set_id, country_id, ref_year_id, inventory_id,",
"       cluster_name, plot_name, panel_name, stratum_name, strata_set_name,",
"       country_name, ref_year_name, inventory_name",
"FROM w_plots",
"ORDER BY plot_id, panel_id;");

# Main variable plot level local densities
a$SqlTarget <- paste(
"WITH w_target_data AS",
"(",
"SELECT",
"t.plot                                         AS plot_id,",
"t.value                                        AS ldsity,",
"v.id                                           AS var_id,",
"vt.id                                          AS var_type_id,",
"vs.id                                          AS var_state_id,",
"y.id                                           AS ref_year_id,",
"i.id                                           AS inventory_id,",
"coalesce(adc.id, 0)                            AS area_dom_cat_id,",
"coalesce(ad.id, 0)                             AS area_dom_id,",
"coalesce(spc.id, 0)                            AS subpop_cat_id,",
"coalesce(sp.id, 0)                             AS subpop_id,",
"v.label                                        AS var_name,",
"vt.label                                       AS var_type_name,",
"vs.label                                       AS var_state_name,",
"y.reference_year_set                           AS ref_year_name,",
"i.label                                        AS inventory_name,",
"coalesce(adc.label, 'all')                     AS area_dom_cat_name,",
"coalesce(ad.label, 'all')                      AS area_dom_name,",
"coalesce(spc.label, 'all')                     AS subpop_cat_name,",
"coalesce(sp.label, 'all')                      AS subpop_name",
"FROM %schema%.t_target_data                         AS t",
"LEFT JOIN %schema%.c_target_variable                 AS v   ON (v.id = t.target_variable)",
"LEFT JOIN %schema%.c_variable_type                   AS vt  ON (vt.id = v.variable_type)",
"LEFT JOIN %schema%.c_state_or_change                 AS vs  ON (vs.id = v.state_or_change)",
"LEFT JOIN %schema%.t_reference_year_set              AS y   ON (y.id = t.reference_year_set)",
"LEFT JOIN %schema%.t_inventory_campaign              AS i   ON (i.id = y.inventory_campaign)",
"LEFT JOIN %schema%.c_area_domain_category            AS adc ON (adc.id = t.area_domain_category)",
"LEFT JOIN %schema%.c_area_domain                     AS ad  ON (ad.id = adc.domain)",
"LEFT JOIN %schema%.c_sub_population_category         AS spc ON (spc.id = t.sub_population_category)",
"LEFT JOIN %schema%.c_sub_population                  AS sp  ON (sp.id = spc.sub_population)",
")",
"SELECT plot_id, ldsity, var_id, var_type_id, var_state_id, ref_year_id, inventory_id,",
"       area_dom_cat_id, area_dom_id, subpop_cat_id, subpop_id,",
"       var_name, var_type_name, var_state_name, ref_year_name, inventory_name,",
"       area_dom_cat_name, area_dom_name, subpop_cat_name, subpop_name",
"FROM w_target_data",
"ORDER BY plot_id;");

# Auxiliary variables plot level local densities
a$SqlAux <- paste(
"WITH w_auxiliary_data AS",
"(",
"SELECT",
"x.plot                                              AS plot_id,",
"x.value                                             AS ldsity,",
"w.id                                                AS var_id,",
"xc.id                                               AS aux_cat_id,",
"xv.id                                               AS aux_id,",
"xc.label                                            AS aux_cat_name,",
"xv.label                                            AS aux_name",
"FROM %schema%.t_auxiliary_data                            AS x",
"LEFT JOIN %schema%.c_auxiliary_variable_category          AS xc   ON (xc.id = x.auxiliary_variable_category)",
"LEFT JOIN %schema%.c_auxiliary_variable                   AS xv   ON (xv.id = xc.auxiliary_variable)",
"LEFT JOIN %schema%.t_variable                             AS w    ON (xc.id = w.auxiliary_variable_category)",
")",
"SELECT plot_id, ldsity, var_id, aux_cat_id, aux_id, aux_cat_name, aux_name",
"FROM w_auxiliary_data",
"ORDER BY plot_id, var_id;");

# Inventory plots to estimation cell mapping
a$SqlCell <- paste(
"WITH w_cell AS",
"(",
"SELECT DISTINCT",
"pd.plot                                    AS plot_id,",
"d.id                                       AS cell_id,",
"b.id                                       AS collection_id,",
"d.label                                    AS cell_name,",
"b.label                                    AS collection_name",
"FROM %schema%.cm_plot2cell_mapping               AS pd",
"LEFT JOIN %schema%.c_estimation_cell             AS d ON (d.id = pd.estimation_cell)",
"LEFT JOIN %schema%.c_estimation_cell_collection  AS b ON (b.id = d.estimation_cell_collection)",
")",
"SELECT plot_id, cell_id, collection_id, cell_name, collection_name",
"FROM w_cell",
"ORDER BY plot_id, cell_id;");

# Inventory plots to parametrization area mapping
a$SqlParam <- paste(
"WITH w_param_area AS",
"(",
"SELECT DISTINCT",
"pq.plot                                AS plot_id,",
"q.gid                                  AS param_id,",
"q.param_area_code                      AS param_name,",
"qt.label                               AS param_type",
"FROM %schema%.cm_plot2param_area_mapping     AS pq",
"LEFT JOIN %schema%.f_a_param_area            AS q  ON (q.gid = pq.param_area)",
"LEFT JOIN %schema%.c_param_area_type         AS qt ON (qt.id = q.param_area_type)",
")",
"SELECT plot_id, param_id, param_name, param_type",
"FROM w_param_area",
"ORDER BY plot_id, param_id;");

# Auxiliary variables list with its known totals
a$SqlModel <- paste(
"WITH w_model AS",
"(",
"SELECT",
"m.id                                           AS model_id,",
"w.id                                           AS var_id,",
"xc.id                                          AS aux_cat_id,",
"d.id                                           AS cell_id,",
"xt.aux_total                                   AS total,",
"m.description                                  AS model_name,",
"xc.label                                       AS aux_cat_name,",
"d.label                                        AS cell_name",
"FROM %schema%.t_model                                AS m",
"LEFT JOIN %schema%.t_model_variables                 AS mv ON (m.id  = mv.model)",
"LEFT JOIN %schema%.t_variable                        AS w  ON (w.id  = mv.variable)",
"LEFT JOIN %schema%.c_auxiliary_variable_category     AS xc ON (xc.id = w.auxiliary_variable_category)",
"LEFT JOIN %schema%.t_aux_total                       AS xt ON (xc.id = xt.auxiliary_variable_category)",
"LEFT JOIN %schema%.c_estimation_cell                 AS d  ON (d.id  = xt.estimation_cell)",
")",
"SELECT model_id, var_id, aux_cat_id, cell_id, total, model_name, aux_cat_name, cell_name",
"FROM w_model",
"ORDER BY model_id, var_id, aux_cat_id, cell_id;");

# Panel list from configuration
a$SqlPanel <- paste(
"WITH w_panels AS",
"(",
"SELECT",
"ec.id                                       AS est_id,",
"s.id                                        AS stratum_id,",
"l.id                                        AS panel_id,",
"pt.reference_year_set                       AS ref_year_id",
"FROM %schema%.t_estimate_conf                     AS ec",
"LEFT JOIN %schema%.t_total_estimate_conf          AS tc  ON (tc.id = ec.total_estimate_conf)",
"LEFT JOIN %schema%.t_panel2total_2ndph_estimate_conf   AS pt  ON (tc.id = pt.total_estimate_conf)",
"LEFT JOIN %schema%.t_panel                        AS l   ON (l.id = pt.panel)",
"LEFT JOIN %schema%.t_stratum                      AS s   ON (s.id = l.stratum)",
")",
"SELECT est_id, stratum_id, panel_id, ref_year_id",
"FROM w_panels",
"WHERE (panel_id IS NOT NULL) AND (ref_year_id IS NOT NULL)",
"ORDER BY est_id, panel_id, ref_year_id;");

# Estimates configuration
a$SqlConf <- paste(
"WITH w_config AS",
"(",
"SELECT",
"ec.id                                       AS est_id,",
"tc.id   AS total_id,",
"et.id                                       AS type_id,",
"pe.id                                       AS phase_id,",
"d.id                                        AS cell_id,",
"b.id   AS collection_id,",
"v.id                                        AS var_id,",
"coalesce(w.area_domain_category, 0)         AS area_dom_cat_id,",
"coalesce(w.sub_population_category, 0)      AS subpop_cat_id,",
"coalesce(ax.model, 0)                       AS model_id,",
"coalesce(ax.param_area, 0)                  AS param_id,",
"coalesce(ec.denominator, 0)                 AS denom_id,",
"CASE WHEN (ax.sigma IS NULL)                    THEN 0",
"     WHEN (ax.sigma IS NOT DISTINCT FROM TRUE)  THEN 1",
"     WHEN (ax.sigma IS NOT DISTINCT FROM FALSE) THEN 0",
"ELSE 0 END                             AS sigma,",
"CASE WHEN (tc.force_synthetic IS NULL)                    THEN 0",
"     WHEN (tc.force_synthetic IS NOT DISTINCT FROM TRUE)  THEN 1",
"     WHEN (tc.force_synthetic IS NOT DISTINCT FROM FALSE) THEN 0",
"ELSE 0 END                             AS synthetic,",
"tc.total_estimate_conf                      AS est_name,",
"et.label                                    AS type_name,",
"pe.label                                    AS phase_name,",
"d.label                                     AS cell_name,",
"b.label                                     AS collection_name,",
"v.label                                     AS var_name,",
"coalesce(adc.label, 'all')                  AS area_dom_cat_name,",
"coalesce(spc.label, 'all')                  AS subpop_cat_name,",
"m.description                               AS model_name,",
"q.param_area_code                           AS param_name",
"FROM %schema%.t_estimate_conf                     AS ec",
"LEFT JOIN %schema%.c_estimate_type                AS et  ON (et.id = ec.estimate_type)",
"LEFT JOIN %schema%.t_total_estimate_conf          AS tc  ON (tc.id = ec.total_estimate_conf)",
"LEFT JOIN %schema%.c_phase_estimate_type          AS pe  ON (pe.id = tc.phase_estimate_type)",
"LEFT JOIN %schema%.t_variable                     AS w   ON (w.id  = tc.target_variable)",
"LEFT JOIN %schema%.c_estimation_cell              AS d   ON (d.id  = tc.estimation_cell)",
"LEFT JOIN %schema%.c_estimation_cell_collection   AS b   ON (b.id  = d.estimation_cell_collection)",
"LEFT JOIN %schema%.c_target_variable              AS v   ON (v.id  = w.target_variable)",
"LEFT JOIN %schema%.c_area_domain_category         AS adc ON (adc.id = w.area_domain_category)",
"LEFT JOIN %schema%.c_sub_population_category      AS spc ON (spc.id = w.sub_population_category)",
"LEFT JOIN %schema%.t_aux_conf                     AS ax  ON (ax.id = tc.aux_conf)",
"LEFT JOIN %schema%.t_model                        AS m   ON (m.id = ax.model)",
"LEFT JOIN %schema%.f_a_param_area                 AS q   ON (q.gid = ax.param_area)",
")",
"SELECT est_id, total_id, type_id, phase_id, cell_id, collection_id, var_id, area_dom_cat_id, subpop_cat_id,",
"       model_id, param_id, denom_id, sigma, synthetic,",
"       est_name, type_name, phase_name, cell_name, collection_name, var_name, area_dom_cat_name, subpop_cat_name,",
"       model_name, param_name",
"FROM w_config",
"WHERE (phase_id = 1) OR ((phase_id = 2) AND (model_id != 0) AND (param_id != 0))",
"ORDER BY est_id;");

##############################################################################################
# Loading data from database

require(RPostgreSQL);

drv <- dbDriver("PostgreSQL");

# This code gets connection string parameters from user (if they did not obtain in function parameters)
host <- ifelse(is.na(host),     readline(prompt = "Enter database server IP: "),       host);
port <- ifelse(is.na(port),     readline(prompt = "Enter database server TPC port: "), port);
dbname <- ifelse(is.na(dbname),   readline(prompt = "Enter database name: "),            dbname);
schema<- ifelse(is.na(schema),   readline(prompt = "Enter schema name: "),              schema);
user <- ifelse(is.na(user),     readline(prompt = "Enter user name: "),                user);
password <- ifelse(is.na(password), readline(prompt = "Enter user password: "),            password);

con <- dbConnect(drv=drv, dbname=dbname, host=host, port=port, user=user, password=password);

# Schema name replacement in sql statements
a$SqlPlots<- gsub("%schema%", schema, a$SqlPlots);
a$SqlTarget <- gsub("%schema%", schema, a$SqlTarget);
a$SqlAux<- gsub("%schema%", schema, a$SqlAux);
a$SqlCell         <- gsub("%schema%", schema, a$SqlCell);
a$SqlParam<- gsub("%schema%", schema, a$SqlParam);
a$SqlModel<- gsub("%schema%", schema, a$SqlModel);
a$SqlPanel <- gsub("%schema%", schema, a$SqlPanel);
a$SqlConf<- gsub("%schema%", schema, a$SqlConf);

# This code loads data from database
a$Plots     <- dbGetQuery(con, a$SqlPlots);
a$Target    <- dbGetQuery(con, a$SqlTarget);
a$Aux       <- dbGetQuery(con, a$SqlAux);
a$Cell            <- dbGetQuery(con, a$SqlCell);
a$Param     <- dbGetQuery(con, a$SqlParam);
a$Model     <- dbGetQuery(con, a$SqlModel);
panel    <- dbGetQuery(con, a$SqlPanel);
conf          <- dbGetQuery(con, a$SqlConf);

# This code disconnects database server
dbDisconnect(con);
dbUnloadDriver(drv);

# head (a$Plots,  n=20);
# head (a$Target, n=20);
# head (a$Aux,    n=20);
# head (a$Model,  n=20);
# head (a$Param,  n=20);
# head (panel,    n=20);
# head (conf,     n=20);

##############################################################################################
# Data.frame PANELS CONFIGURATION

# This code checks data of panels configuration (not null constraints)
if ( any(is.na(panel$est_id)) )   { warning("Estimate identifier in panels configuration may not be NULL!"); }
if ( any(is.na(panel$stratum_id)) )   { warning("Stratum identifier in panels configuration may not be NULL!"); }
if ( any(is.na(panel$panel_id)) )   { warning("Panel identifier in panels configuration may not be NULL!"); }
if ( any(is.na(panel$ref_year_id)) )   { warning("Reference year set identifier in panels configuration may not be NULL!"); }

a$Panel <- data.frame(panel, row.names = NULL);

# head(a$Panel);

##############################################################################################
# Data.frame ESTIMATES CONFIGURATION

# This code checks data of estimates configuration (not null constraints)
if ( any(is.na(conf$est_id)) )   { warning("Estimate identifier in estimates configurations may not be NULL!"); }
if ( any(is.na(conf$total_id)) ) { warning("Total estimate identifier in estimates configurations may not be NULL!"); }
if ( any(is.na(conf$type_id)) )  { warning("Estimate type identifier(total|ratio) in estimates configurations may not be NULL!"); }
if ( any(is.na(conf$phase_id)) ) { warning("Estimate phase identifier (1p|2p_reg|2p) in estimates confiturations may not be NULL!"); }
if ( any(is.na(conf$cell_id)) )  { warning("Estimation cell identifier in estimates configurations may not be NULL!"); }
if ( any(is.na(conf$collection_id)) )  { warning("Estimation cell collection identifier in estimates configurations may not be NULL!"); }
if ( any(is.na(conf$var_id)) ) { warning("Main variable identifier in estimates configurations may not be NULL!"); }
if ( any(is.na(conf$area_dom_cat_id)) ) { warning("Area domain category identifier in estimates configurations may not be NULL!"); }
if ( any(is.na(conf$subpop_cat_id)) ) { warning("Subpopulation category identifier in estimates configurations may not be NULL!"); }
if ( any(is.na(conf$model_id)) ) { warning("Model identifier in estimates configurations may not be NULL!"); }
if ( any(is.na(conf$param_id)) ) { warning("Parametrization area identifier in estimates configurations may not be NULL!"); }
if ( any(is.na(conf$denom_id)) ) { warning("Denominator identifier in estimates configurations may not be NULL!"); }
if ( any(is.na(conf$sigma)) ) { warning("Sigma value in estimates configurations may not be NULL!"); }
if ( any(is.na(conf$synthetic)) ) { warning("Synthetic value in estimates configurations may not be NULL!"); }
if ( any(is.na(conf$est_name)) ) { warning("Estimate name in estimates configurations may not be NULL!"); }
if ( any(is.na(conf$type_name)) ) { warning("Estimate type name (total|ratio) in estimates configurations may not be NULL!"); }
if ( any(is.na(conf$phase_name)) ) { warning("Estimate phase name (1p|2p_reg|2p) in estimates configurations may not be NULL!"); }
if ( any(is.na(conf$cell_name)) ) { warning("Estimation cell name in estimates configurations may not be NULL!"); }
if ( any(is.na(conf$collection_name)) ){ warning("Estimation cell collection name in estimates configurations may not be NULL!"); }
if ( any(is.na(conf$var_name)) ){ warning("Main variable name in estimates configurations may not be NULL!"); }
if ( any(is.na(conf$area_dom_cat_name)) ){ warning("Area domain category name in estimates configurations may not be NULL!"); }
if ( any(is.na(conf$subpop_cat_name)) ){ warning("Subpopulation category name in estimates configurations may not be NULL!"); }

# This code checks unique constraint of primary key (est_id - estimate identifier) in estimates configuration data.frame (est_id):
if ( length(conf$est_id) != length(unique(conf$est_id)) ) {
warning("Estimate identifier have to be unique in estimates configuration data.frame!");
}

# This code checks unique constraint of total estimate identifier inside total estimates configurations subset
if ( length(conf[conf$type_id == 1,]$total_id) != length(unique(conf[conf$type_id == 1,]$total_id)) ) {
warning("Total estimate identifier in estimates configurations have to be unique inside total estimates configurations subset!");
}

# This code converts sigma value (0,1) into boolean (FALSE, TRUE)
conf$sigma <- ifelse(conf$sigma == 1, TRUE, FALSE);

# This code converts synthetic value (0,1) into boolean (FALSE, TRUE)
conf$synthetic <- ifelse(conf$synthetic== 1, TRUE, FALSE);

a$Conf <- data.frame(conf, row.names = NULL);

# head(a$Conf);

##############################################################################################

return (a);

# Function returns list of data.frames:
# Plots  - Inventory plots data.frame
# Target - Main variable plot level local densities data.frame
# Aux    - Auxiliary variables plot level local densities data.frame
# Cell   - Inventory plots to estimation cell mapping data.frame
# Param  - Inventory plots to parametrization area mapping data.frame
# Model  - Auxiliary variables list with its known totals data.frame
# Panel  - Panels configuration data.frame
# Conf   - Estimates configuration data.frame
}
