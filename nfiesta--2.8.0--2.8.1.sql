--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--"CREATE ROLE adm_nfiesta; CREATE ROLE app_nfiesta; CREATE ROLE app_nfiesta_mng; GRANT app_nfiesta TO app_nfiesta_mng;"

/*
WITH w_objects AS (
	SELECT replace(unnest(extconfig)::regclass::text, 'nfiesta_test', '@extschema@') obj_name FROM pg_extension WHERE extname = 'nfiesta'
)
, w_cmds AS (
SELECT
	obj_name,
	format('GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE	 %s TO app_nfiesta;' , obj_name) AS cmd
FROM w_objects
WHERE obj_name::text NOT LIKE '%_seq'
UNION ALL
SELECT
	obj_name,
	format('GRANT USAGE, SELECT, UPDATE ON SEQUENCE		 %s TO app_nfiesta;' , obj_name) AS cmd
FROM w_objects
WHERE obj_name::text LIKE '%_seq'
)
select cmd from w_cmds ORDER BY obj_name
;
*/
--:r !psql -d contrib_regression_fst -f nfiesta--2.7.0--2.8.1.sql --no-psqlrc -qAt

SELECT pg_catalog.pg_extension_config_dump('@extschema@.c_estimation_period_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.c_panel_refyearset_group_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_panel_refyearset_group_id_seq', '');

---------------------------------------NFIESTA_PG-----------------------------
----------------------------------------inventory data-------------------------
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE	 @extschema@.c_area_domain TO app_nfiesta;
GRANT USAGE, SELECT, UPDATE ON SEQUENCE		 @extschema@.c_area_domain_id_seq TO app_nfiesta;

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE	 @extschema@.c_area_domain_category TO app_nfiesta;
GRANT USAGE, SELECT, UPDATE ON SEQUENCE		 @extschema@.c_area_domain_category_id_seq TO app_nfiesta;

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE	 @extschema@.c_auxiliary_variable TO app_nfiesta;
GRANT USAGE, SELECT, UPDATE ON SEQUENCE		 @extschema@.c_auxiliary_variable_id_seq TO app_nfiesta;

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE	 @extschema@.c_auxiliary_variable_category TO app_nfiesta;
GRANT USAGE, SELECT, UPDATE ON SEQUENCE		 @extschema@.c_auxiliary_variable_category_id_seq TO app_nfiesta;

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE	 @extschema@.c_estimation_cell TO app_nfiesta;
GRANT USAGE, SELECT, UPDATE ON SEQUENCE		 @extschema@.c_estimation_cell_id_seq TO app_nfiesta;

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE	 @extschema@.c_estimation_cell_collection TO app_nfiesta;
GRANT USAGE, SELECT, UPDATE ON SEQUENCE		 @extschema@.c_estimation_cell_collection_id_seq TO app_nfiesta;

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE	 @extschema@.c_state_or_change TO app_nfiesta;
GRANT USAGE, SELECT, UPDATE ON SEQUENCE		 @extschema@.c_state_or_change_id_seq TO app_nfiesta;

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE	 @extschema@.c_sub_population TO app_nfiesta;
GRANT USAGE, SELECT, UPDATE ON SEQUENCE		 @extschema@.c_sub_population_id_seq TO app_nfiesta;

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE	 @extschema@.c_sub_population_category TO app_nfiesta;
GRANT USAGE, SELECT, UPDATE ON SEQUENCE		 @extschema@.c_sub_population_category_id_seq TO app_nfiesta;

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE	 @extschema@.c_target_variable TO app_nfiesta;
GRANT USAGE, SELECT, UPDATE ON SEQUENCE		 @extschema@.c_target_variable_id_seq TO app_nfiesta;

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE	 @extschema@.c_variable_type TO app_nfiesta;
GRANT USAGE, SELECT, UPDATE ON SEQUENCE		 @extschema@.c_variable_type_id_seq TO app_nfiesta;

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE	 @extschema@.cm_tvariable2variable_type TO app_nfiesta;
GRANT USAGE, SELECT, UPDATE ON SEQUENCE		 @extschema@.cm_tvariable2variable_type_id_seq TO app_nfiesta;

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE	 @extschema@.cm_plot2cell_mapping TO app_nfiesta;
GRANT USAGE, SELECT, UPDATE ON SEQUENCE		 @extschema@.cm_plot2cell_mapping_id_seq TO app_nfiesta;

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE	 @extschema@.t_auxiliary_data TO app_nfiesta;
GRANT USAGE, SELECT, UPDATE ON SEQUENCE		 @extschema@.t_auxiliary_data_id_seq TO app_nfiesta;

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE	 @extschema@.t_available_datasets TO app_nfiesta;
GRANT USAGE, SELECT, UPDATE ON SEQUENCE		 @extschema@.t_available_datasets_id_seq TO app_nfiesta;

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE	 @extschema@.t_target_data TO app_nfiesta;
GRANT USAGE, SELECT, UPDATE ON SEQUENCE		 @extschema@.t_target_data_id_seq TO app_nfiesta;

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE	 @extschema@.t_variable TO app_nfiesta;
GRANT USAGE, SELECT, UPDATE ON SEQUENCE		 @extschema@.t_variable_id_seq TO app_nfiesta;

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE	 @extschema@.t_variable_hierarchy TO app_nfiesta;
GRANT USAGE, SELECT, UPDATE ON SEQUENCE		 @extschema@.t_variable_hierarchy_id_seq TO app_nfiesta;

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE	 @extschema@.t_estimation_cell_hierarchy TO app_nfiesta;
GRANT USAGE, SELECT, UPDATE ON SEQUENCE		 @extschema@.t_estimation_cell_hierarchy_id_seq TO app_nfiesta;

----------------------------------------estimates configuration----------------

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE	 @extschema@.t_g_beta TO app_nfiesta;
GRANT USAGE, SELECT, UPDATE ON SEQUENCE		 @extschema@.t_g_beta_id_seq TO app_nfiesta;

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE	 @extschema@.t_result TO app_nfiesta;
GRANT USAGE, SELECT, UPDATE ON SEQUENCE		 @extschema@.t_result_id_seq TO app_nfiesta;

-----

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE	 @extschema@.c_param_area_type TO app_nfiesta;
GRANT USAGE, SELECT, UPDATE ON SEQUENCE		 @extschema@.c_param_area_type_id_seq TO app_nfiesta;

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE	 @extschema@.cm_cell2param_area_mapping TO app_nfiesta;
GRANT USAGE, SELECT, UPDATE ON SEQUENCE		 @extschema@.cm_cell2param_area_mapping_id_seq TO app_nfiesta;

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE	 @extschema@.cm_plot2param_area_mapping TO app_nfiesta;
GRANT USAGE, SELECT, UPDATE ON SEQUENCE		 @extschema@.cm_plot2param_area_mapping_id_seq TO app_nfiesta;

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE	 @extschema@.f_a_cell TO app_nfiesta;
GRANT USAGE, SELECT, UPDATE ON SEQUENCE		 @extschema@.f_a_cell_gid_seq TO app_nfiesta;

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE	 @extschema@.f_a_param_area TO app_nfiesta;
GRANT USAGE, SELECT, UPDATE ON SEQUENCE		 @extschema@.f_a_param_area_gid_seq TO app_nfiesta;

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE	 @extschema@.t_aux_conf TO app_nfiesta;
GRANT USAGE, SELECT, UPDATE ON SEQUENCE		 @extschema@.t_aux_conf_id_seq TO app_nfiesta;

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE	 @extschema@.t_aux_total TO app_nfiesta;
GRANT USAGE, SELECT, UPDATE ON SEQUENCE		 @extschema@.t_aux_total_id_seq TO app_nfiesta;

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE	 @extschema@.t_estimate_conf TO app_nfiesta;
GRANT USAGE, SELECT, UPDATE ON SEQUENCE		 @extschema@.t_estimate_conf_id_seq TO app_nfiesta;

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE	 @extschema@.t_model TO app_nfiesta;
GRANT USAGE, SELECT, UPDATE ON SEQUENCE		 @extschema@.t_model_id_seq TO app_nfiesta;

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE	 @extschema@.t_model_variables TO app_nfiesta;
GRANT USAGE, SELECT, UPDATE ON SEQUENCE		 @extschema@.t_model_variables_id_seq TO app_nfiesta;

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE	 @extschema@.t_panel2total_1stph_estimate_conf TO app_nfiesta;
GRANT USAGE, SELECT, UPDATE ON SEQUENCE		 @extschema@.t_panel2total_1stph_estimate_conf_id_seq TO app_nfiesta;

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE	 @extschema@.t_stratum_in_estimation_cell TO app_nfiesta;
GRANT USAGE, SELECT, UPDATE ON SEQUENCE		 @extschema@.t_stratum_in_estimation_cell_id_seq TO app_nfiesta;

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE	 @extschema@.t_total_estimate_conf TO app_nfiesta;
GRANT USAGE, SELECT, UPDATE ON SEQUENCE		 @extschema@.t_total_estimate_conf_id_seq TO app_nfiesta;

----------------------------------------------manager----------------------------------------------------------

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE	 @extschema@.c_estimation_period TO app_nfiesta_mng;
GRANT USAGE, SELECT, UPDATE ON SEQUENCE		 @extschema@.c_estimation_period_id_seq TO app_nfiesta_mng;

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE	 @extschema@.c_panel_refyearset_group TO app_nfiesta_mng;
GRANT USAGE, SELECT, UPDATE ON SEQUENCE		 @extschema@.c_panel_refyearset_group_id_seq TO app_nfiesta_mng;

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE	 @extschema@.t_panel_refyearset_group TO app_nfiesta_mng;
GRANT USAGE, SELECT, UPDATE ON SEQUENCE		 @extschema@.t_panel_refyearset_group_id_seq TO app_nfiesta_mng;
