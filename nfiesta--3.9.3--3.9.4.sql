-- Copyright 2017, 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.

-- <function name="@extschema@.fn_get_panels_in_estimation_cells(INT[], DATE, DATE, INT)" schema="extschema" src="functions/extschema/configuration/fn_get_panels_in_estimation_cells.sql">
--
-- Copyright 2017, 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: nfiesta.fn_get_panels_in_estimation_cells(INT[], DATE, DATE, INT)
-- DROP FUNCTION nfiesta.fn_get_panels_in_estimation_cells(INT[], DATE, DATE, INT);

CREATE OR REPLACE FUNCTION nfiesta.fn_get_panels_in_estimation_cells(_estimation_cells INT[], _estimate_date_begin DATE, _estimate_date_end DATE, _target_variable INT)
RETURNS TABLE (
	stratum	integer,
	stratum_label	varchar(20),
	panel	integer,
	panel_label	varchar(20),
	panel_subset	integer,
	reference_year_set	integer,
	reference_year_set_label	varchar(20),
	reference_date_begin	date,
	reference_date_end	date,
	reference_year_set_fit	boolean,
	portion_of_panel_inside	double precision,
	portion_of_period_covered	double precision,
	total	integer,
	is_max	boolean
)
AS
$function$
DECLARE
_change_variable 	boolean;
_estimation_cell	integer;
_cell_area_m2		double precision;
_stratas_all		integer[];
_stratas		integer[];
_sum_stratas_m2		double precision;
_result_test		boolean;

BEGIN
-- checking input arguments for NULL
IF _estimation_cells IS NULL THEN
	RAISE EXCEPTION 'fn_get_panels_in_estimation_cells: Function argument _estimation_cells INT[] must not be NULL!';
END IF;

IF _estimate_date_begin IS NULL THEN
	RAISE EXCEPTION 'fn_get_panels_in_estimation_cells: Function argument _estimate_date_begin must not be NULL!';
END IF;

IF _estimate_date_end IS NULL THEN
	RAISE EXCEPTION 'fn_get_panels_in_estimation_cells: Function argument _estimate_date_end must not be NULL!';
END IF;

IF _target_variable IS NULL THEN
	RAISE EXCEPTION 'fn_get_panels_in_estimation_cells: Function argument _target_variable must not be NULL!';
END IF;

-- checking _estimation_cells array if does not contain NULL
IF (SELECT array_position(_estimation_cells, NULL) IS NOT NULL) THEN
	RAISE EXCEPTION 'fn_get_panels_in_estimation_cells: Function argument _estimation_cells INT[] must not be an array containing NULL!';
END IF;

-- checking if esctimation cells are fully covered by sampling strata
-- the loop could be replaced by a smart query, f_a_cell.geom is MULTIPOLYGON so if a unique constraint is added to the FKEY estmation_cell, the aggregation of ST_Arrea(geom)
--  by estimation cell will not be needed anymore
FOREACH _estimation_cell IN ARRAY _estimation_cells
LOOP

	-- cell data
	WITH w_cell AS (
		-- cell geometry can be divided into smaller blocks, thats why sum() 
		SELECT estimation_cell, sum(st_area(geom)) AS area_m2
		FROM nfiesta.f_a_cell
		WHERE estimation_cell = _estimation_cell
		GROUP BY estimation_cell
	)
	SELECT	area_m2
	FROM w_cell
	INTO _cell_area_m2;

	-- stratas in cell
	WITH w_stratas AS (
		SELECT t1.stratum, t1.area_m2
		FROM nfiesta.t_stratum_in_estimation_cell AS t1
		WHERE estimation_cell = _estimation_cell
	)
	SELECT	array_agg(t1.stratum), sum(t1.area_m2)
	FROM w_stratas AS t1
	INTO _stratas, _sum_stratas_m2;

	IF _sum_stratas_m2 IS NULL
	THEN
		RAISE EXCEPTION 'fn_get_panels_in_estimation_cells: Estimation cell (id = %) is not intersected by any sampling stratum.', _estimation_cell;
	END IF;

	-- test on cell coverage
	-- two decimal places considered when comparing the final product of division with 1
	_result_test := (SELECT CASE WHEN round(_sum_stratas_m2::numeric/_cell_area_m2::numeric, 2) = 1 THEN true ELSE false END);
	
	IF _result_test = false
	THEN
		RAISE WARNING 'fn_get_panels_in_estimation_cells: The estimation cell (id = %) is not fully covered by sampling strata.', _estimation_cell;
	END IF;

	_stratas_all := _stratas_all || _stratas;

END LOOP;

-- return the list of panels for given target variable
-- if the reference time period is met then it is stated in column reference_year_set_fit
RETURN QUERY EXECUTE '
WITH w_all_refyearsets AS MATERIALIZED ( -- this CTE is needed to complement reference_date_begin and reference_date_end to change reference-year sets
	SELECT 
		t1.id,
		t1.inventory_campaign,
		t1.reference_year_set,
		coalesce(t1.reference_date_begin, t2.reference_date_begin) AS reference_date_begin,
		coalesce(t1.reference_date_end, t3.reference_date_end) AS reference_date_end,
		t1.label,
		t1.comment,
		t1.reference_year_set_begin,
		t1.reference_year_set_end,
		t1.status_variables
	FROM 
		sdesign.t_reference_year_set AS t1
	LEFT JOIN 
		sdesign.t_reference_year_set AS t2
		ON t1.reference_year_set_begin = t2.id
	LEFT JOIN 
		sdesign.t_reference_year_set AS t3
		ON t1.reference_year_set_end = t3.id
), w_panel_refyearest_combinations_with_tv_data AS MATERIALIZED (
	SELECT
		t2.stratum,
		t2.id AS panel,
		t2.panel AS panel_label,
		t2.plot_count AS total, -- should be cluster count for comparisons between strata using cluster or plots
		t2.panel_subset,
		t4.id AS reference_year_set,
		t4.reference_year_set AS reference_year_set_label,
		t4.reference_date_begin,
		t4.reference_date_end,
		-- test if reference_year_set fits the parameteres given
		CASE
			WHEN 	t4.reference_date_begin >= $2 AND t4.reference_date_end <= $3 THEN true
			ELSE false
		END AS reference_year_set_fit,

		-- range of estimation period
		tsrange($2,$3) AS estimate_range,
		-- range of reference period
		tsrange(t4.reference_date_begin,t4.reference_date_end) AS reference_range,

		-- intersection of dates
		tsrange(t4.reference_date_begin,t4.reference_date_end) * tsrange($2,$3) AS intersection_range

	FROM
		sdesign.t_panel AS t2
	-- inner join, assuming every panel has SOME reference_year_set mapping
	INNER JOIN
		sdesign.cm_refyearset2panel_mapping AS t3
	ON
		t2.id = t3.panel
	-- again inner join, I am curious, what other reference year sets are possible in case the required dates does not fit (but target variable available)
	INNER JOIN
		w_all_refyearsets AS t4
	ON
		t3.reference_year_set = t4.id
	-- inner join, target variable must fit
	INNER JOIN
		nfiesta.t_available_datasets AS t5
	ON
		t2.id = t5.panel AND
		t4.id = t5.reference_year_set AND
		t5.variable = $4 
), w_data AS MATERIALIZED (
	SELECT
		t1.id AS stratum,
		t1.stratum AS stratum_label, 
		t2.panel, 
		t2.panel_label,
		t2.panel_subset,
		t2.reference_year_set,
		t2.reference_year_set_label,
		t2.reference_date_begin,
		t2.reference_date_end,
		t2.reference_year_set_fit,
		t2.estimate_range,
		t2.reference_range,
		t2.intersection_range,

		-- find out what portion of panel is inside the estimate range
		CASE WHEN isempty(intersection_range) = false
		THEN
			EXTRACT(DAYS FROM (upper(intersection_range) - lower(intersection_range))) / -- interval of intersection
			EXTRACT(DAYS FROM (upper(reference_range) - lower(reference_range))) -- interval of reference period

		ELSE
			0 -- 0 percent of panel inside the period
		END AS portion_of_panel_inside,

		-- find out what portion of estimation period is covered by panel
		CASE WHEN isempty(intersection_range) = false
		THEN
			EXTRACT(DAYS FROM (upper(intersection_range) - lower(intersection_range))) / -- interval of intersection
			EXTRACT(DAYS FROM (upper(estimate_range) - lower(estimate_range))) -- interval of estimation period
		ELSE 0 -- 0 percent covered by panel
		END AS portion_of_period_covered,
		t2.total
	FROM
		(SELECT t1.id, t1.stratum FROM sdesign.t_stratum AS t1 WHERE array[id] <@ $5) AS t1
	-- could be inner join, every stratum has to have at least one panel
	-- but in case of not available target variable, stratum would disappear from the list
	-- later comment by Radim: if there is no TV there will be no panels and refyearsets either, so the LEFT JOIN preservas the stratum in the list
	-- but no panels and refyearset combination are shown for such stratum, which confuses the user 
	-- I change it to INNER JOIN, at least for the time being, the way how single-phase estimates are configured over more strata
	-- needs to be revised. In addition, input TV should be changed for variable (inclusing categorisations).
	INNER JOIN
		w_panel_refyearest_combinations_with_tv_data AS t2
	ON
		t1.id = t2.stratum
), w_max AS (
	SELECT 
		t1.stratum, t1.stratum_label, t1.panel, t1.panel_label, t1.panel_subset,
		t1.reference_year_set, t1.reference_year_set_label, t1.reference_date_begin, t1.reference_date_end, t1.reference_year_set_fit, 
		t1.portion_of_panel_inside, t1.portion_of_period_covered,
		t1.total,
		-- maximum number of plots for a group of panels with the same reference_year_set 
		-- and within belonging to the required reference period
		max(t1.total) OVER(PARTITION BY t1.stratum, t1.reference_year_set, t1.reference_year_set_fit) AS max_total
	FROM w_data AS t1
)
SELECT
	t1.stratum, t1.stratum_label, t1.panel, t1.panel_label, t1.panel_subset,
	t1.reference_year_set, t1.reference_year_set_label, t1.reference_date_begin, t1.reference_date_end, t1.reference_year_set_fit, 
	t1.portion_of_panel_inside, t1.portion_of_period_covered, t1.total,
	CASE WHEN t1.total = t1.max_total THEN true ELSE false END AS is_max
FROM
	w_max AS t1' USING _estimation_cells, _estimate_date_begin, _estimate_date_end, _target_variable, _stratas_all;
END;
$function$
LANGUAGE plpgsql
PARALLEL SAFE;

COMMENT ON FUNCTION nfiesta.fn_get_panels_in_estimation_cells(integer[], date, date, integer) IS 
'This function returns list of combinations of panels and reference yearsets by strata intersecting any '
'of the estimation cells in the input array _estimation_cells. For each combination the '
'reference_year_set_fit indicator tells if it intersects the estimation period and the max indicator '
'tells if this is a combination with the maximum possible No of plots.';
-- </function>

-- <function name="@extschema@.fn_api_get_panel_refyearset_combinations4groups(INT[])" schema="extschema" src="functions/extschema/configuration/fn_api_get_panel_refyearset_combinations4groups.sql">
-- Copyright 2017, 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_api_get_panel_refyearset_combinations4groups(integer[])
--DROP FUNCTION @extschema@.fn_api_get_panel_refyearset_combinations4groups(integer[]);

CREATE OR REPLACE FUNCTION @extschema@.fn_api_get_panel_refyearset_combinations4groups(_panel_refyearset_groups INT[])
RETURNS TABLE (panel INT, reference_year_set INT)
AS
$function$
BEGIN

IF _panel_refyearset_groups IS NULL THEN
	RAISE EXCEPTION 'fn_api_get_panel_refyearset_combinations4groups: Function argument __panel_refyearset_groups INT[] must not be NULL!';
END IF;

-- checking _panel_refyearset_groups array if does not contain NULL
IF (SELECT array_position(_panel_refyearset_groups, NULL) IS NOT NULL) THEN
	RAISE EXCEPTION 'fn_api_get_panel_refyearset_combinations4groups: Function argument _panel_refyearset_groups INT[] must not be an array containing NULL!';
END IF;

RETURN QUERY EXECUTE '
SELECT DISTINCT
	panel,
	reference_year_set
FROM 
	@extschema@.t_panel_refyearset_group
WHERE ARRAY[panel_refyearset_group] <@ $1;' USING _panel_refyearset_groups;
END;
$function$
LANGUAGE plpgsql
PARALLEL SAFE;

COMMENT ON FUNCTION @extschema@.fn_api_get_panel_refyearset_combinations4groups(integer[]) IS 
'The function returns equaly ordered arrays of panels and reference-year sets '
'belonging to any group of panels and reference year combinations passed ' 
'to the function as an argument. Only distinct pairs of panel and reference year '
'sets are returned.';

/*
-- testing false inputs

-- passing NULL for _panel_refyearset_groups
SELECT * FROM @extschema@.fn_api_get_panel_refyearset_combinations4groups(NULL);

-- passing an _panel_refyearset_groups containing NULL
SELECT * FROM @extschema@.fn_api_get_panel_refyearset_combinations4groups(ARRAY[23, 10, NULL]);

-- testing valid inputs

-- group 23
SELECT * FROM @extschema@.fn_api_get_panel_refyearset_combinations4groups(ARRAY[23]);

-- group 10 (no reference year sets)
SELECT * FROM @extschema@.fn_api_get_panel_refyearset_combinations4groups(ARRAY[10]);

-- group 20
SELECT * FROM @extschema@.fn_api_get_panel_refyearset_combinations4groups(ARRAY[20]);

-- groups 20 and 23, note combination 1 (panel) and 2 (refyearset) included only once in the function output
SELECT * FROM @extschema@.fn_api_get_panel_refyearset_combinations4groups(ARRAY[20,23]);

-- non-existing group, no records
SELECT * FROM @extschema@.fn_api_get_panel_refyearset_combinations4groups(ARRAY[-20]);

 -- combination of non-existing and existing group, records of the existing returned
SELECT * FROM @extschema@.fn_api_get_panel_refyearset_combinations4groups(ARRAY[-20, 23]);
*/
-- </function>
