-- Copyright 2017, 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.

-- <function name="fn_api_get_data_options4single_phase_config" schema="extschema" src="functions/extschema/configuration/fn_api_get_data_options4single_phase_config.sql">
--
-- Copyright 2017, 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: nfiesta.nfiesta.fn_api_get_data_options4single_phase_config(INT[], INT[], DATE, DATE, DOUBLE PRECISION)
-- DROP FUNCTION nfiesta.fn_api_get_data_options4single_phase_config(INT[], INT[], DATE, DATE, DOUBLE PRECISION);

CREATE OR REPLACE FUNCTION nfiesta.fn_api_get_data_options4single_phase_config(_estimation_cells INT[], _variables INT[], _estimate_date_begin DATE, _estimate_date_end DATE, 
_cell_coverage_tolerance DOUBLE PRECISION DEFAULT 1E-3)
RETURNS TABLE (
	estimation_cells INT[], 
	country_id INT,
	country_label VARCHAR(200),
	country_label_en VARCHAR(200),
	country_description TEXT,
	country_description_en TEXT,
	strata_set_id INT,
	strata_set_label VARCHAR(200),
	strata_set_label_en VARCHAR(200),
	strata_set_description TEXT,
	strata_set_description_en TEXT,
	stratum_id INT,
	stratum_label VARCHAR(200),
	stratum_label_en VARCHAR(200),
	stratum_description TEXT,
	stratum_description_en TEXT,
	panel_id INT,
	panel_label	VARCHAR(200),
	panel_label_en	VARCHAR(200),
	panel_description TEXT,
	panel_description_en TEXT,
	reference_year_set_id INT,
	reference_year_set_label VARCHAR(200),
	reference_year_set_label_en VARCHAR(200),
	reference_year_set_description TEXT,
	reference_year_set_description_en TEXT,
	reference_date_begin DATE,
	reference_date_end DATE,
	refyearset_fully_within_estimation_period BOOLEAN,
	share_of_refyearset_intersected_by_estimation_period FLOAT,
	share_of_estimation_period_intersected_by_refyearset FLOAT,
	ssize INT,
	max_ssize_within_estimation_period_and_refyearset BOOLEAN
)
AS
$function$
DECLARE
_unsampled_cells INT[];
_partially_sampled_cells INT[];
_cells_with_no_data INT[];

BEGIN
-- checking input arguments for NULL
IF _estimation_cells IS NULL THEN
	RAISE EXCEPTION 'fn_api_get_data_options4single_phase_config: Function argument _estimation_cells INT[] must not be NULL!';
END IF;

IF _estimate_date_begin IS NULL THEN
	RAISE EXCEPTION 'fn_api_get_data_options4single_phase_config: Function argument _estimate_date_begin DATE must not be NULL!';
END IF;

IF _estimate_date_end IS NULL THEN
	RAISE EXCEPTION 'fn_api_get_data_options4single_phase_config: Function argument _estimate_date_end DATE must not be NULL!';
END IF;

IF _estimate_date_begin >= _estimate_date_end THEN
	RAISE EXCEPTION 'fn_api_get_data_options4single_phase_config: Function argument _estimate_date_begin must be less than _estimate_date_end!';
END IF;

IF _variables IS NULL THEN
	RAISE EXCEPTION 'fn_api_get_data_options4single_phase_config: Function argument _variables INT[] must not be NULL!';
END IF;

-- checking if _estimation_cells array does not contain NULL
IF (SELECT array_position(_estimation_cells, NULL) IS NOT NULL) THEN
	RAISE EXCEPTION 'fn_api_get_data_options4single_phase_config: Function argument _estimation_cells INT[] must not be an array containing NULL!';
END IF;

-- checking if _variables array does not contain NULL
IF (SELECT array_position(_variables, NULL) IS NOT NULL) THEN
	RAISE EXCEPTION 'fn_api_get_data_options4single_phase_config: Function argument _variables INT[] must not be an array containing NULL!';
END IF;

IF _cell_coverage_tolerance >= 1 OR _cell_coverage_tolerance <= 0  THEN
	RAISE EXCEPTION 'fn_api_get_data_options4single_phase_config: Function argument _cell_coverage_tolerance (%) DOUBLE PRECISION must be within the range (0;1)!', _cell_coverage_tolerance;
END IF;

-- check if the estimation cell is intersected by any strata 
WITH w AS (
	SELECT unnest(_estimation_cells) AS unsampled_cells 
	EXCEPT 
	SELECT estimation_cell AS unsampled_cells FROM nfiesta.t_stratum_in_estimation_cell
)
SELECT array_agg(unsampled_cells ORDER BY unsampled_cells) AS unsampled_cells FROM w INTO _unsampled_cells;

IF _unsampled_cells IS NOT NULL THEN
	RAISE EXCEPTION 'fn_api_get_data_options4single_phase_config: Estimation cell(s) (id = %) is(are) not intersected by any sampling stratum.', _unsampled_cells;
END IF;

-- checking if estimation cells are fully covered by sampling strata
-- cells' total areas		
-- cell geometry can be divided into smaller blocks, thats why sum() 
WITH w_cell_area AS MATERIALIZED (
	SELECT 
		estimation_cell,
		sum(st_area(geom)) AS cell_area 
	FROM 
		nfiesta.f_a_cell 
	WHERE ARRAY[estimation_cell] <@ $1
	GROUP BY estimation_cell
), 
-- test if the whole estimation cell is covered by strata
-- the logic must folow stratasets because two stratasets may ne positioned one on top of the other within the same country, so the sum of the strata area must
-- exclude overlaps strata of different stratasets within the same country   
w_strataset_areas_by_cell AS MATERIALIZED ( 
	SELECT 
		t1.estimation_cell,
		t3.country AS country_id,
		t2.strata_set AS strata_set_id,
		sum(t1.area_m2) AS strataset_area_in_cell
	FROM 
		nfiesta.t_stratum_in_estimation_cell AS t1 
	INNER JOIN 
		sdesign.t_stratum AS t2
	ON ARRAY[t1.estimation_cell] <@ $1 AND t1.stratum = t2.id
	INNER JOIN
		sdesign.t_strata_set AS t3
	ON t2.strata_set = t3.id
	GROUP BY t1.estimation_cell, t3.country, t2.strata_set 
), w_max_strataset_area_by_cell AS MATERIALIZED ( -- here we filter the records according to the maximum area covered by one single strataset within a country
	SELECT 
		t1.estimation_cell,
		t1.country_id,
		max(t1.strataset_area_in_cell) AS max_strataset_area_in_cell
	FROM 
		 w_strataset_areas_by_cell  AS t1
	GROUP BY t1.estimation_cell, t1.country_id
), w_cell_area_sampled AS MATERIALIZED (
SELECT
	t1.estimation_cell, 
	sum(max_strataset_area_in_cell) AS cell_area_sampled
FROM	
	w_max_strataset_area_by_cell AS t1
GROUP BY t1.estimation_cell
)
SELECT 
	array_agg(t1.estimation_cell ORDER BY t1.estimation_cell) AS partially_sampled_cells
FROM 
	w_cell_area AS t1
	INNER JOIN
	w_cell_area_sampled AS t2
	ON t1.estimation_cell = t2.estimation_cell
WHERE t1.cell_area > t2.cell_area_sampled AND abs(t1.cell_area - t2.cell_area_sampled)::numeric / t1.cell_area::numeric > $5 INTO _partially_sampled_cells;

IF _partially_sampled_cells IS NOT NULL THEN
	RAISE WARNING 'fn_api_get_data_options4single_phase_config: The estimation cell(s) (id = %) is(are) not fully covered by sampling strata.', _partially_sampled_cells;
END IF;

-- check if data of all variables is available for all input cells
WITH w_strata_in_cell AS MATERIALIZED (
SELECT DISTINCT -- some cells may have more than one intersection geometry with some strata
	t1.estimation_cell,
	t4.id AS country,
	t3.id AS strata_set,
	t2.id AS stratum
	FROM
	nfiesta.t_stratum_in_estimation_cell AS t1
INNER JOIN
	sdesign.t_stratum AS t2
	ON ARRAY[t1.estimation_cell] <@ $1  AND t1.stratum = t2.id 
INNER JOIN
	sdesign.t_strata_set AS t3
ON t2.strata_set = t3.id 
INNER JOIN
	sdesign.c_country AS t4
ON t3.country = t4.id
), w_panels_containing_data AS MATERIALIZED (
SELECT DISTINCT 
	t1.estimation_cell
FROM
	w_strata_in_cell AS t1
INNER JOIN 
	sdesign.t_panel AS t2
ON t1.stratum = t2.stratum
INNER JOIN 
	nfiesta.t_available_datasets AS t3
ON ARRAY[t3.variable] <@ $2 AND t3.panel = t2.id
GROUP BY t1.country, t1.strata_set, t1.stratum, t2.id, t3.reference_year_set, t1.estimation_cell
HAVING array_agg(t3.variable ORDER BY t3.variable) <@ $2 AND array_agg(t3.variable ORDER BY t3.variable) @> $2  
-- we only accept panels and reference yearsets with data available for all _variables 
), w_cells_with_no_data AS MATERIALIZED (
SELECT unnest($1) AS estimation_cell
EXCEPT 
SELECT  estimation_cell FROM w_panels_containing_data
)
SELECT array_agg(estimation_cell ORDER BY estimation_cell) FROM w_cells_with_no_data INTO _cells_with_no_data;

IF _cells_with_no_data <@ $1 AND _cells_with_no_data @> $1 THEN 
	RAISE EXCEPTION 'fn_api_get_data_options4single_phase_config: For the combination of input variables and estimation cells there is no plot data at all!';
END IF;

IF _cells_with_no_data IS NOT NULL THEN 
	RAISE WARNING 'fn_api_get_data_options4single_phase_config: Data of input variables is not available for some estimation cells (id = %)!', _cells_with_no_data;
END IF;

RETURN QUERY EXECUTE '
WITH w_strata_in_cell AS MATERIALIZED (
SELECT DISTINCT -- some cells may have more than one intersection geometry with some strata
	t1.estimation_cell,
	t4.id AS country,
	t3.id AS strata_set,
	t2.id AS stratum
	FROM
	nfiesta.t_stratum_in_estimation_cell AS t1
INNER JOIN
	sdesign.t_stratum AS t2
	ON ARRAY[t1.estimation_cell] && $1 AND t1.stratum = t2.id 
INNER JOIN
	sdesign.t_strata_set AS t3
ON t2.strata_set = t3.id 
INNER JOIN
	sdesign.c_country AS t4
ON t3.country = t4.id
), w_panels_containing_data AS MATERIALIZED (
SELECT 
	t1.country,
	t1.strata_set,
	t1.stratum,
	t2.id AS panel,
	t3.reference_year_set,
	t1.estimation_cell,
	array_agg(t3.variable ORDER BY t3.variable) AS variables
FROM
	w_strata_in_cell AS t1
INNER JOIN 
	sdesign.t_panel AS t2
ON t1.stratum = t2.stratum
INNER JOIN 
	nfiesta.t_available_datasets AS t3
ON ARRAY[t3.variable] && $2 AND t3.panel = t2.id
GROUP BY t1.country, t1.strata_set, t1.stratum, t2.id, t3.reference_year_set, t1.estimation_cell
HAVING array_agg(t3.variable ORDER BY t3.variable) <@ $2 AND array_agg(t3.variable ORDER BY t3.variable) @> $2 
-- we only accept panels and reference yearsets with data available for all _variables   
), w_panels_containing_data_agg AS MATERIALIZED (
SELECT 
	array_agg(country ORDER BY country, strata_set, stratum, panel, reference_year_set) AS countries,
	array_agg(strata_set ORDER BY country, strata_set, stratum, panel, reference_year_set) AS strata_sets,
	array_agg(stratum ORDER BY country, strata_set, stratum, panel, reference_year_set) AS strata,
	array_agg(panel ORDER BY country, strata_set, stratum, panel, reference_year_set) AS panels,
	array_agg(reference_year_set ORDER BY country, strata_set, stratum, panel, reference_year_set) As reference_year_sets,
	estimation_cell
FROM 
	w_panels_containing_data
GROUP BY estimation_cell
), w_agg_data_options_by_cell_combination AS MATERIALIZED ( -- get distinct combinations of countries, stratasets, strata, panels and refyearsets and aggregate cells matching each combination 
SELECT 
	countries,
	strata_sets,
	strata,
	panels,
	reference_year_sets,
	array_agg(estimation_cell) AS estimation_cells
FROM 
	w_panels_containing_data_agg
GROUP BY 
	countries, strata_sets, strata, panels, reference_year_sets
 ), w_data_options_by_cell_combination AS MATERIALIZED ( -- keep combinations of estimation cells but unnest the other fields
SELECT 
	estimation_cells,
	unnest(countries) AS country,
	unnest(strata_sets) AS strata_set,
	unnest(strata) AS stratum,
	unnest(panels) AS panel,
	unnest(reference_year_sets) AS reference_year_set
FROM w_agg_data_options_by_cell_combination
), w_data_options_with_ranges AS MATERIALIZED ( -- attach time ranges to the data
SELECT 
		t1.*,
		coalesce(t2.reference_date_begin, t3.reference_date_begin) AS reference_date_begin,
		coalesce(t2.reference_date_end, t4.reference_date_end) AS reference_date_end,
		CASE WHEN coalesce(t2.reference_date_begin, t3.reference_date_begin) >= $3 AND coalesce(t2.reference_date_end, t4.reference_date_end) <= $4 THEN TRUE
		ELSE FALSE
		END AS refyearset_fully_within_estimation_period,
		tsrange(coalesce(t2.reference_date_begin, t3.reference_date_begin), coalesce(t2.reference_date_end, t4.reference_date_end)) AS data_range,
		tsrange($3, $4) estimation_period_range,
		tsrange(coalesce(t2.reference_date_begin, t3.reference_date_begin), coalesce(t2.reference_date_end, t4.reference_date_end))*tsrange($3, $4) AS intersection_range		
FROM 
	w_data_options_by_cell_combination AS t1
INNER JOIN 
	sdesign.t_reference_year_set AS t2
ON t1.reference_year_set = t2.id
LEFT JOIN 
	sdesign.t_reference_year_set AS t3
ON t2.reference_year_set_begin = t3.id
LEFT JOIN 
	sdesign.t_reference_year_set AS t4
ON t2.reference_year_set_end = t4.id
) -- prepare final result 
SELECT 
	t1.estimation_cells,
	t1.country AS country_id,
	t2.label::VARCHAR(200) AS country_label,
	coalesce(t2.label_en, t2.label)::VARCHAR(200) AS country_label_en,
	t2.description::TEXT AS country_description,
	coalesce(t2.description_en, t2.description)::TEXT AS country_description_en,
	t1.strata_set AS strata_set_id,
	t3.strata_set::VARCHAR(200) AS strata_set_label, 
	t3.strata_set::VARCHAR(200) AS strata_set_label_en,
	t3.label::TEXT AS strata_set_description,
	t3.label::TEXT AS strata_set_description_en,
	t1.stratum AS stratum_id,
	t4.stratum::VARCHAR(200) AS stratum_label, 
	t4.stratum::VARCHAR(200) AS stratum_label_en,
	t4.label::TEXT AS stratum_description,
	t4.label::TEXT AS stratum_description_en,
	t1.panel AS panel_id,
	t5.panel::VARCHAR(200) AS panel_label,
	t5.panel::VARCHAR(200) AS panel_label_en,
	t5.label::TEXT AS panel_description,
	t5.label::TEXT AS panel_description_en,
	t1.reference_year_set AS reference_year_set_id,
	t6.reference_year_set::VARCHAR(200) AS reference_year_set_label,
	t6.reference_year_set::VARCHAR(200) AS reference_year_set_label_en,
	t6.label::TEXT AS reference_year_set_description, 
	t6.label::TEXT AS reference_year_set_description_en,
	t1.reference_date_begin,
	t1.reference_date_end,
	t1.refyearset_fully_within_estimation_period,
	CASE WHEN isempty(intersection_range) = FALSE THEN
		round((EXTRACT(DAYS FROM (upper(intersection_range) - lower(intersection_range))) / EXTRACT(DAYS FROM (upper(data_range) - lower(data_range))))::NUMERIC,1)::FLOAT -- interval of reference period
	ELSE
		0::FLOAT
	END AS share_of_refyearset_intersected_by_estimation_period,
	CASE WHEN isempty(intersection_range) = FALSE THEN
		round((EXTRACT(DAYS FROM (upper(intersection_range) - lower(intersection_range))) / EXTRACT(DAYS FROM (upper(estimation_period_range) - lower(estimation_period_range))))::NUMERIC, 1)::FLOAT
		ELSE 0::FLOAT 
	END AS share_of_estimation_period_intersected_by_refyearset,
	t5.plot_count AS ssize,
	CASE 
		WHEN max(t5.plot_count) OVER (PARTITION BY t1.estimation_cells, t1.country, t1.strata_set, t1.stratum, t1.refyearset_fully_within_estimation_period, t1.reference_year_set) = t5.plot_count THEN TRUE 
		ELSE FALSE 
	END AS max_ssize_within_estimation_period_and_refyearset 
FROM 
	w_data_options_with_ranges AS t1
INNER JOIN 
	sdesign.c_country AS t2
ON t1.country = t2.id
INNER JOIN 
	sdesign.t_strata_set AS t3
ON t1.strata_set = t3.id
INNER JOIN 
	sdesign.t_stratum AS t4
ON t1.stratum = t4.id
INNER JOIN 
	sdesign.t_panel AS t5
ON t1.panel = t5.id
INNER JOIN 
	sdesign.t_reference_year_set AS t6
ON t1.reference_year_set = t6.id'  USING _estimation_cells, _variables, _estimate_date_begin, _estimate_date_end;
END;
$function$
LANGUAGE plpgsql
PARALLEL SAFE;

COMMENT ON FUNCTION nfiesta.fn_api_get_data_options4single_phase_config(INT[], INT[], DATE, DATE, DOUBLE PRECISION) IS 
'This function returns unique sets of panel and reference yearset pairs by combinations of estimation cells. '
'Based on this data the user chooses panels and reference yearsets for each group of cells intersecting specific ' 
'combination of countries, strata sets and strata. The panels and reference yearsets fully within the estimation '
'period (refyearset_fully_within_estmation_period = TRUE) and with the maximum sample size '
'(max_ssize_within_estimation_period_and_refyearset = TRUE) are to be preselected by nFIESTA GUI.';

/* tests
------------------------------------------------- testing invalid inputs ------------------------------------------------------------------------------
-- test NULL for _estimation_cells argument
SELECT * FROM nfiesta.fn_api_get_data_options4single_phase_config (NULL, ARRAY[1], '1-Jan-2016', '31-Dec-2017');

-- test for NULL within _estimation_cells
SELECT * FROM nfiesta.fn_api_get_data_options4single_phase_config (ARRAY[144, NULL], ARRAY[1], '1-Jan-2016', '31-Dec-2017');

-- test NULL for _variables argument
SELECT * FROM nfiesta.fn_api_get_data_options4single_phase_config (ARRAY[144], NULL, '1-Jan-2016', '31-Dec-2017');

-- test for NULL within _variables
SELECT * FROM nfiesta.fn_api_get_data_options4single_phase_config (ARRAY[144], ARRAY[1, NULL], '1-Jan-2016', '31-Dec-2017');

-- test NULL for _estimate_date_begin
SELECT * FROM nfiesta.fn_api_get_data_options4single_phase_config (ARRAY[144], ARRAY[1], NULL, '31-Dec-2017');

-- test NULL for _estimate_date_end
SELECT * FROM nfiesta.fn_api_get_data_options4single_phase_config (ARRAY[144], ARRAY[1], '1-Jan-2016', NULL);

-- test of _estimate_date_begin after _estimate_date_end
SELECT * FROM nfiesta.fn_api_get_data_options4single_phase_config (ARRAY[144], ARRAY[1], '31-Dec-2017', '1-Jan-2016');

-- test of _estimate_date_begin equal _estimate_date_end
SELECT * FROM nfiesta.fn_api_get_data_options4single_phase_config (ARRAY[144], ARRAY[1], '31-Dec-2017', '31-Dec-2017');

-- test for valid range of _cell_coverage_tolerance, exception because tolerance higher than 1 
SELECT * FROM nfiesta.fn_api_get_data_options4single_phase_config (ARRAY[144], ARRAY[1], '1-Jan-2016', '31-Dec-2017', 1E3);

-- test for valid range of _cell_coverage_tolerance, exception because tolerance equals 1 
SELECT * FROM nfiesta.fn_api_get_data_options4single_phase_config (ARRAY[144], ARRAY[1], '1-Jan-2016', '31-Dec-2017', 1);

-- test for valid range of _cell_coverage_tolerance, exception becase tolerance equals 0 
SELECT * FROM nfiesta.fn_api_get_data_options4single_phase_config (ARRAY[144], ARRAY[1], '1-Jan-2016', '31-Dec-2017', 0);

-- test for valid range of _cell_coverage_tolerance, exception because tolerance equals -1 
SELECT * FROM nfiesta.fn_api_get_data_options4single_phase_config (ARRAY[144], ARRAY[1], '1-Jan-2016', '31-Dec-2017', -1);

-- test of no data situation for all estimation cells
SELECT * FROM nfiesta.fn_api_get_data_options4single_phase_config(ARRAY[17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43], ARRAY[1,2,3, -1], '1-Jan-2011', '31-Dec-2014');

------------------------------------------------ testing valid inputs ---------------------------------------------------------------------------------
-- test for NUTS0 CZ in pathfinder_ds_1_5 DB - 52 records returned, warning saying cell ins not fully covered by strata (Slovakia missing)
SELECT * FROM nfiesta.fn_api_get_data_options4single_phase_config(ARRAY[144], ARRAY[1], '1-Jan-2016', '31-Dec-2017');

-- test for NUTS2 CZ in pathfinder_ds_1_5 DB - 224 records returned, warnings saying cell ins not fully covered by strata (Slovakia missing)
SELECT * FROM nfiesta.fn_api_get_data_options4single_phase_config(ARRAY[146, 147, 148, 149, 150, 151, 152, 153], ARRAY[1], '1-Jan-2016', '31-Dec-2017');

-- test data of nfiesta, test for INSPIRE 50 km cells and forest variables (altogether, coniferous, broadleaved)
SELECT * FROM nfiesta.fn_api_get_data_options4single_phase_config(ARRAY[17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43], ARRAY[1,2,3], '1-Jan-2011', '31-Dec-2014');

-- same as above + the lower _cell_area_tolerance, leading to a longer list of cells included in the warning.
SELECT * FROM nfiesta.fn_api_get_data_options4single_phase_config(ARRAY[17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43], ARRAY[1,2,3], '1-Jan-2011', '31-Dec-2014', 1E-8);

------------------------------------------------ helping code ---------------------------------------------------------------------------------
-- point 1 according to the wiki specs of single-phase configuration, https://gitlab.com/nfiesta/nfiesta_gui/-/wikis/Use-case-for-configuration-of-single-phase-totals-and-ratios
SELECT DISTINCT country_label, strata_set_label, stratum_label, panel_label, reference_year_set_label, refyearset_fully_within_estimation_period, max_ssize_within_estimation_period 
FROM nfiesta.fn_api_get_data_options4single_phase_config(ARRAY[17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43], ARRAY[1,2,3], '1-Jan-2011', '31-Dec-2014');

-- point 2 according to the wiki specs of single-phase configuration, https://gitlab.com/nfiesta/nfiesta_gui/-/wikis/Use-case-for-configuration-of-single-phase-totals-and-ratios   
SELECT DISTINCT estimation_cells, country_label, strata_set_label, stratum_label
FROM nfiesta.fn_api_get_data_options4single_phase_config(ARRAY[17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43], ARRAY[1,2,3], '1-Jan-2011', '31-Dec-2014');
*/

-- </function>