-- Copyright 2017, 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.

-- <function name="@extschema@.fn_api_get_1pgroups4regtotal(INT,INT[], INT[])" schema="extschema" src="functions/extschema/configuration/fn_api_get_1pgroups4regtotal.sql">
-- Copyright 2017, 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_api_get_1pgroups4regtotal(INT, ARRAY[INT], ARRAY[INT])
-- DROP FUNCTION @extschema@.fn_api_get_1pgroups4regtotal(INT, INT[], INT[]);

CREATE OR REPLACE FUNCTION @extschema@.fn_api_get_1pgroups4regtotal(_estimation_period INT, _estimation_cells INT[], _variables INT[])
RETURNS TABLE(
	estimation_period_id INT, 
	estimation_period_label VARCHAR(200), 
	estimation_period_label_en VARCHAR(200), 
	estimation_period_description TEXT, 
	estimation_period_description_en TEXT,
	country_id INT,
	country_label VARCHAR(200),
	country_label_en VARCHAR(200),
	country_description TEXT,
	country_description_en TEXT,
	strata_set_id INT,
	strata_set_label VARCHAR(200),
	strata_set_label_en VARCHAR(200),
	strata_set_description TEXT,
	strata_set_description_en TEXT,
	stratum_id INT,
	stratum_label VARCHAR(200),
	stratum_label_en VARCHAR(200),
	stratum_description TEXT,
	stratum_description_en TEXT,
	panel_refyearset_group_id INT, 
	panel_refyearset_group_label VARCHAR(200),
	panel_refyearset_group_label_en VARCHAR(200),
	panel_refyearset_group_description TEXT,
	panel_refyearset_group_description_en TEXT,
	complete_group BOOLEAN)
AS
$function$
BEGIN

-- testing input arguments if not NULL
IF _estimation_period IS NULL THEN
	RAISE EXCEPTION 'fn_api_get_1pgroups4regtotal: Function argument _estimation_period INT must not be NULL!';
END IF;

IF _estimation_cells IS NULL THEN
	RAISE EXCEPTION 'fn_api_get_1pgroups4regtotal: Function argument _estimation_cells INT[] must not be NULL!';
END IF;

IF _variables IS NULL THEN
	RAISE EXCEPTION 'fn_api_get_1pgroups4regtotal: Function argument _variables INT[] must not be NULL!';
END IF;

-- checking input arrays if they do not contain NULL
IF (SELECT array_position(_estimation_cells, NULL) IS NOT NULL) THEN
	RAISE EXCEPTION 'fn_api_get_1pgroups4regtotal: Function argument _estimation_cells INT[] must not be an array containing NULL!';
END IF;

IF (SELECT array_position(_variables, NULL) IS NOT NULL) THEN
	RAISE EXCEPTION 'fn_api_get_1pgroups4regtotal: Function argument _variables INT[] must not be an array containing NULL!';
END IF;

-- checking one cell collection within input argument _estimation_cells 
IF (SELECT count(DISTINCT estimation_cell_collection) FROM @extschema@.c_estimation_cell WHERE ARRAY[id] <@ _estimation_cells) IS DISTINCT FROM 1 THEN 
	RAISE EXCEPTION 'fn_api_get_1pgroups4regtotal: Function argument _estimation_cells INT[] must contain cells corresponding to one and the only estimation cell collection!';
END IF;

RETURN QUERY EXECUTE '
WITH w_reg2conf AS MATERIALIZED (
	SELECT 
		$1 AS estimation_period_reg,
		t4.strata_set,
		t3.stratum AS stratum_reg,
		t1.estimation_cell AS estimation_cell_reg,
		t2.variable AS variable_reg
	FROM
	  (SELECT unnest($2) AS estimation_cell) AS t1 
	 CROSS JOIN
	  (SELECT unnest($3) AS variable) AS t2
	 INNER JOIN
	  @extschema@.t_stratum_in_estimation_cell AS t3
	 ON t1.estimation_cell = t3.estimation_cell
	 INNER JOIN 
	 sdesign.t_stratum AS t4
	 ON t3.stratum = t4.id
), w_groups_in_strata_sets AS MATERIALIZED (
	SELECT DISTINCT t1.panel_refyearset_group, t4.strata_set 
	FROM 
		@extschema@.t_total_estimate_conf  AS t1
	INNER JOIN 
		@extschema@.t_panel_refyearset_group AS t2
		ON t1.panel_refyearset_group = t2.panel_refyearset_group AND t2.reference_year_set IS NOT NULL
	INNER JOIN 
		sdesign.t_panel AS t3
		ON t2.panel = t3.id
	INNER JOIN 
		sdesign.t_stratum AS t4
		ON t3.stratum = t4.id 
	INNER JOIN 
		w_reg2conf AS t5
		ON t5.strata_set = t4.strata_set
), w_conf1p AS MATERIALIZED (
SELECT 
	t1.*,
	t2.*,
	--t3.*,
	count(*) OVER (PARTITION BY t1.estimation_period_reg, t1.strata_set) AS no_of_desired_reg_estimates,
	count(*) FILTER (WHERE t2.panel_refyearset_group IS NOT NULL AND t3.strata_set IS NOT NULL) OVER (PARTITION BY t1.estimation_period_reg, t1.stratum_reg, t2.panel_refyearset_group) AS no_configured_1pestimates_per_group
FROM 
	w_reg2conf AS t1
LEFT JOIN @extschema@.t_total_estimate_conf  AS t2
	ON 
	t1.estimation_period_reg = t2.estimation_period AND
	t1.estimation_cell_reg = t2.estimation_cell AND
	t1.variable_reg = t2.variable AND
	t2.phase_estimate_type = 1
LEFT JOIN w_groups_in_strata_sets AS t3
	ON t1.strata_set = t3.strata_set 
WHERE t3.strata_set IS NOT NULL
), w_conf1p_agg AS MATERIALIZED ( 
SELECT DISTINCT
	estimation_period_reg,
	stratum_reg,
	panel_refyearset_group,
	no_of_desired_reg_estimates = no_configured_1pestimates_per_group AS complete_group
FROM 
	w_conf1p
WHERE panel_refyearset_group IS NOT NULL
-- the above could be the final query, but we need to add records for strata with NULL in panel_refyearset_group in case there are no 1p configiration 
-- for any cell and for any varibale either so the WHERE condition filter such records out
-- at the same time the WHERE is needed in order to avoid duplicate records (one with NULL group, the other NOT NULL but still incomplete) for groups with
-- partial configuration for some cells or for some variables only
)
SELECT 
-- here by left JOIN we get records for all strata at any circumstances, 
--potentially with NULL groups (no 1p configs at all for any cell and any desired variable within the given stratum)
	t3.id AS estimation_period_id,
	t3.label::VARCHAR(200) AS estimation_period_label,
	t3.label_en::VARCHAR(200) AS estimation_period_label_en,
    t3.description AS estimation_period_description,
	t3.description_en AS estimation_period_description_en,
	t6.id AS country_id,
	t6.label::VARCHAR(200) AS country_label,
	t6.label_en::VARCHAR(200) AS country_label_en,
	t6.description AS country_description,
	t6.description_en AS country_description_en,
	t5.id AS strata_set_id,
	t5.strata_set::VARCHAR(200) AS strata_set_label,
	t5.strata_set::VARCHAR(200) AS strata_set_label_en,
	t5.label::TEXT AS strata_set_description,
	t5.label::TEXT AS strata_set_description_en,
	t4.id AS stratum_id,
	t4.stratum::VARCHAR(200) AS stratum_label,
	t4.stratum::VARCHAR(200) AS stratum_label_en,
	t4.label::TEXT AS stratum_description,
	t4.label::TEXT AS stratum_description_en,
	t7.id AS panel_refyearset_group_id,
	t7.label AS panel_refyearset_group_label,
	t7.label_en AS panel_refyearset_group_label_en,
	t7.description AS panel_refyearset_group_description,
	t7.description_en AS panel_refyearset_group_description_en,
	t2.complete_group
FROM 
	(SELECT DISTINCT estimation_period_reg, stratum_reg FROM w_reg2conf) AS t1
LEFT JOIN
	w_conf1p_agg AS t2
	ON t1.estimation_period_reg = t2.estimation_period_reg AND t1.stratum_reg = t2.stratum_reg
INNER JOIN 
	@extschema@.c_estimation_period AS t3
	ON t1.estimation_period_reg = t3.id
INNER JOIN 
	sdesign.t_stratum AS t4
	ON t1.stratum_reg = t4.id
INNER JOIN 
	sdesign.t_strata_set AS t5
	ON t4.strata_set = t5.id
INNER JOIN 
	sdesign.c_country AS t6
	ON t5.country = t6.id
LEFT JOIN 
	@extschema@.c_panel_refyearset_group AS t7
	ON t2.panel_refyearset_group = t7.id
WHERE t2.stratum_reg IS NOT NULL;' USING _estimation_period, _estimation_cells, _variables;
END;
$function$
LANGUAGE plpgsql
PARALLEL SAFE;

COMMENT ON FUNCTION  @extschema@.fn_api_get_1pgroups4regtotal(INT, INT[], INT[]) IS 
'Function returns list of strata and panel refyearset groups with a Bollean '
'indicator saying whether 1p configurations using the group correspond to all '
'desired regression estimates, i.e. considering a subset of cells for estimation ' 
'intersecting (not just touching) the stratum and considering the desired list of '
'variables.';

/* tests
-- test NULL for _estimation_period argument
SELECT * FROM @extschema@.fn_api_get_1pgroups4regtotal(NULL, ARRAY[44,45,46,47,48,49,50,51,52,53,56,57,58,62], ARRAY[1,2,3]);

-- test NULL for _estimation_cell argument
SELECT * FROM @extschema@.fn_api_get_1pgroups4regtotal(1, NULL, ARRAY[1,2,3]);

-- test NULL for _variables argument
SELECT * FROM @extschema@.fn_api_get_1pgroups4regtotal(1, ARRAY[44,45,46,47,48,49,50,51,52,53,56,57,58,62], NULL);

-- test for NULL as an element of _estimation_cell argument
SELECT * FROM @extschema@.fn_api_get_1pgroups4regtotal(1, ARRAY[44,45,46,47,NULL,49,50,51,52,53,56,57,58,62], ARRAY[1,2,3]);
SELECT * FROM @extschema@.fn_api_get_1pgroups4regtotal(1, ARRAY[NULL], ARRAY[1,2,3]);
SELECT * FROM @extschema@.fn_api_get_1pgroups4regtotal(1, ARRAY[NULL]::int[], ARRAY[1,2,3]);

-- test for NULL as an element of _variables argument
SELECT * FROM @extschema@.fn_api_get_1pgroups4regtotal(1, ARRAY[44,45,46,47,48,49,50,51,52,53,56,57,58,62], ARRAY[NULL,2,NULL]);
SELECT * FROM @extschema@.fn_api_get_1pgroups4regtotal(1, ARRAY[44,45,46,47,48,49,50,51,52,53,56,57,58,62], ARRAY[NULL]);
SELECT * FROM @extschema@.fn_api_get_1pgroups4regtotal(1, ARRAY[44,45,46,47,48,49,50,51,52,53,56,57,58,62], ARRAY[NULL]::int[]);

-- test if estimation cells belong to one estimation cell collection (adding 1 to the valid array _estimation_cells, 1 
-- belongs to another collection but the same stratum
SELECT * FROM @extschema@.fn_api_get_1pgroups4regtotal(1, ARRAY[1,44,45,46,47,48,49,50,51,52,53,56,57,58,62], ARRAY[1,2,3]);

-- tests on valid input
-- cells of one stratum three variables, complete group
SELECT * FROM @extschema@.fn_api_get_1pgroups4regtotal(1, ARRAY[44,45,46,47,48,49,50,51,52,53,56,57,58,62], ARRAY[1,2,3]);

-- cells of one stratum, one variable without configuration added to three with configuration making the group incomplete
SELECT * FROM @extschema@.fn_api_get_1pgroups4regtotal(1, ARRAY[44,45,46,47,48,49,50,51,52,53,56,57,58,62], ARRAY[1,2,3,-1]);

-- cells of one stratum, one variable without configuration, no group given (NULL)
SELECT * FROM @extschema@.fn_api_get_1pgroups4regtotal(1, ARRAY[44,45,46,47,48,49,50,51,52,53,56,57,58,62], ARRAY[-1]);
*/
-- </function>

-- <function name="@extschema@.fn_api_get_wmodels_sigma_paramtypes(INT[], INT[])" schema="extschema" src="functions/extschema/configuration/fn_api_get_wmodels_sigma_paramtypes.sql">
-- Copyright 2017, 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.

-- Function: @extschema@.fn_api_get_wmodels_sigma_paramtypes(INT[], INT[])
-- DROP FUNCTION @extschema@.fn_api_get_wmodels_sigma_paramtypes(INT[], INT[]);

CREATE OR REPLACE FUNCTION @extschema@.fn_api_get_wmodels_sigma_paramtypes(_panels INT[], _estimation_cells INT[])
RETURNS TABLE(
	working_model_id INT,
	working_model_label VARCHAR(200),
	working_model_label_en VARCHAR(200),
	working_model_description TEXT,
	working_model_description_en TEXT,
	sigma BOOLEAN,
	param_area_type_id INT,
	param_area_type_label VARCHAR(200),
	param_area_type_label_en VARCHAR(200),
	param_area_type_description TEXT,
	param_area_type_descriptipn_en TEXT,
	no_of_cells_covered BIGINT,
	all_cells_included BOOLEAN
)
AS
$function$
BEGIN

-- testing input parameters if not NULL
IF _panels IS NULL THEN
	RAISE EXCEPTION 'fn_api_get_wmodels_sigma_paramtypes: Function argument _panels INT[] must not be NULL!';
END IF;

IF _estimation_cells IS NULL THEN
	RAISE EXCEPTION 'fn_api_get_wmodels_sigma_paramtypes: Function argument _estimation_cells INT[] must not be NULL!';
END IF;

-- checking input arrays if they do not contain NULL
IF (SELECT array_position(_panels, NULL) IS NOT NULL) THEN
	RAISE EXCEPTION 'fn_api_get_wmodels_sigma_paramtypes:  Function argument _panels INT[] must not be an array containing NULL!';
END IF;

IF (SELECT array_position(_estimation_cells, NULL) IS NOT NULL) THEN
	RAISE EXCEPTION 'fn_api_get_wmodels_sigma_paramtypes:  Function argument _estimation_cells INT[] must not be an array containing NULL!';
END IF;

-- checking one cell collection within input parameter _estimation_cells 
IF (SELECT count(DISTINCT estimation_cell_collection) FROM @extschema@.c_estimation_cell WHERE ARRAY[id] <@ _estimation_cells) IS DISTINCT FROM 1 THEN 
	RAISE EXCEPTION 'fn_api_get_wmodels_sigma_paramtypes: Function argument _estimation_cells INT[] must contain cells corresponding to one and the only estimation cell collection!';
END IF;

RETURN QUERY EXECUTE ' 
WITH w_eligible_panel_groups AS MATERIALIZED (
	SELECT 
		t1.panel_refyearset_group,
		array_agg(t2.panel) AS panels_aux
	FROM 
		(SELECT DISTINCT panel_refyearset_group FROM @extschema@.t_aux_conf) AS t1
	INNER JOIN
		@extschema@.t_panel_refyearset_group AS t2
	ON t1.panel_refyearset_group = t2.panel_refyearset_group  
	GROUP BY t1.panel_refyearset_group
	HAVING array_agg(t2.panel) <@ $1 AND $1 <@ array_agg(t2.panel)
), w_eligible_models_sigma_paramtypes AS MATERIALIZED (
	SELECT 
		t1.model,
		t1.sigma,
		t4.param_area_type,
		t4.gid AS param_area_id,
		t4.param_area_code,
		t3.estimation_cell, 
		count(*) OVER (PARTITION BY t1.model, t1.sigma, t4.param_area_type) AS no_of_cells_covered,
		count(*) OVER (PARTITION BY t1.model, t1.sigma, t4.param_area_type) = array_length($2,1) AS all_cells_covered
	FROM 
		@extschema@.t_aux_conf AS t1
	INNER JOIN
		w_eligible_panel_groups AS t2
		ON t1.panel_refyearset_group = t2.panel_refyearset_group
	INNER JOIN 
		@extschema@.cm_cell2param_area_mapping AS t3
	ON t1.param_area = t3.param_area AND ARRAY[t3.estimation_cell] <@  $2
	INNER JOIN 
		@extschema@.f_a_param_area AS t4
		ON t1.param_area = t4.gid
)
SELECT DISTINCT
		t1.model AS model_id,
		t2.label AS model_label,
		t2.label_en AS model_label_en,
		t2.description AS model_description,
		t2.description_en AS model_description_en,
		t1.sigma,
		t1.param_area_type AS param_area_type_id,
		t3.label AS param_area_type_label,
		t3.label_en AS param_area_type_label,
		t3.description AS param_area_type_description,
		t3.description_en AS param_area_type_description_en,
		t1.no_of_cells_covered,
		t1.all_cells_covered
FROM 
		w_eligible_models_sigma_paramtypes AS t1
INNER JOIN
		@extschema@.t_model AS t2
		ON t1.model = t2.id
INNER JOIN 
		@extschema@.c_param_area_type AS t3
		ON t1.param_area_type = t3.id;' USING _panels, _estimation_cells;

END;
$function$
LANGUAGE plpgsql
PARALLEL SAFE;

COMMENT ON FUNCTION  @extschema@.fn_api_get_wmodels_sigma_paramtypes(INT[], INT[]) IS 
'For the input arrays of panels and estimation cells the function returns '
'a table with alternative combinations of working model, sigma and parameterisation '
'area type for which the regression estimates of total can be configured. Each ' 
'record in the output table corresponds to a set of existing records in the table ' 
'@extschema@.t_aux_conf.' ;

/* tests
-------------------------------------------------------------------------------------------------------------------------------------------------------
-- tests for function @extschema@.fn_api_get_wmodels_sigma_paramtypes(_panels INT[], _estimation_cells INT[])
-------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------- testing invalid inputs ------------------------------------------------------------------------------
-- test NULL for _panels argument
SELECT * FROM @extschema@.fn_api_get_wmodels_sigma_paramtypes(NULL, ARRAY[51,55,56,60,61,67,68,69,70,75,76,77,83]);

-- test NULL for _estimation_cell argument
SELECT * FROM @extschema@.fn_api_get_wmodels_sigma_paramtypes(ARRAY[1,2], NULL);

-- test for NULL as an element of _panels
SELECT * FROM @extschema@.fn_api_get_wmodels_sigma_paramtypes(ARRAY[1,NULL], ARRAY[51,55,56,60,61,67,68,69,70,75,76,77,83]);
SELECT * FROM @extschema@.fn_api_get_wmodels_sigma_paramtypes(ARRAY[NULL], ARRAY[51,55,56,60,61,67,68,69,70,75,76,77,83]);
SELECT * FROM @extschema@.fn_api_get_wmodels_sigma_paramtypes(ARRAY[NULL]::int[], ARRAY[51,55,56,60,61,67,68,69,70,75,76,77,83]);

-- test for NULL as an element of _estimation_cells
SELECT * FROM @extschema@.fn_api_get_wmodels_sigma_paramtypes(ARRAY[1,2], ARRAY[NULL,55,56,60,61,67,68,69,70,75,76,77,83]);
SELECT * FROM @extschema@.fn_api_get_wmodels_sigma_paramtypes(ARRAY[1,2], ARRAY[NULL]);
SELECT * FROM @extschema@.fn_api_get_wmodels_sigma_paramtypes(ARRAY[1,2], ARRAY[NULL]::int[]);

-- test if estimation cells belong to one estimation cell collection (adding 1 to the valid array _estimation_cells
SELECT * FROM @extschema@.fn_api_get_wmodels_sigma_paramtypes(ARRAY[1,2], ARRAY[1,55,56,60,61,67,68,69,70,75,76,77,83]);

------------------------------------------------ testing valid inputs ---------------------------------------------------------------------------------
SELECT * FROM @extschema@.fn_api_get_wmodels_sigma_paramtypes(ARRAY[1,2], ARRAY[55,56,60,61,67,68,69,70,75,76,77,83]);


-------------------------------------------------------- helping code ---------------------------------------------------------------------------------
	SELECT 
		t1.param_area,
		t2.estimation_cell_collection,
		array_agg(t1.estimation_cell) AS estimation_cells 
	FROM 
		@extschema@.cm_cell2param_area_mapping AS t1
	INNER JOIN 
		@extschema@.c_estimation_cell AS t2
	ON t1.estimation_cell = t2.id
	GROUP BY param_area, t2.estimation_cell_collection

	
	SELECT * FROM @extschema@.fn_api_get_1pgroups4regtotal(1,ARRAY[77,83], ARRAY[1,2,3]) -- 16
	SELECT * FROM @extschema@.fn_api_get_1pgroups4regtotal(1,ARRAY[11,12,13,14,15,16], ARRAY[1,2,3]) -- 18
	SELECT * FROM @extschema@.fn_api_get_1pgroups4regtotal(1,ARRAY[51,55,56,60,61,67,68,69,70,75,76,77,83], ARRAY[1,2,3]) -- 14, 16, 19
	
	SELECT * FROM @extschema@.fn_api_get_panel_refyearset_combinations4groups(ARRAY[16]) -- 1
	SELECT * FROM @extschema@.fn_api_get_panel_refyearset_combinations4groups(ARRAY[18]) -- 7
	SELECT * FROM @extschema@.fn_api_get_panel_refyearset_combinations4groups(ARRAY[14,16,19]) -- 1,2
*/
-- </function>
