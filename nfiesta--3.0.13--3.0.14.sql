--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--


DROP FUNCTION IF EXISTS @extschema@.fn_etl_check_target_variable_metadatas(json, character varying, boolean) CASCADE;
DROP FUNCTION IF EXISTS @extschema@.fn_etl_update_target_variable_metadata(integer, json, boolean, character varying) CASCADE;
DROP FUNCTION IF EXISTS @extschema@.fn_etl_check_target_variable(json) CASCADE;
DROP FUNCTION IF EXISTS @extschema@.fn_etl_get_target_variable(character varying) CASCADE;



-- <function name="fn_etl_check_target_variable_metadatas" schema="extschema" src="functions/extschema/etl/fn_etl_check_target_variable_metadatas.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_check_target_variable_metadatas(json, character varying, boolean)

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_check_target_variable_metadatas(json, character varying, boolean) CASCADE;

create or replace function @extschema@.fn_etl_check_target_variable_metadatas
(
	_metadatas			json,
	_national_language	character varying(2) default 'en'::character varying,
	_metadata_diff		boolean default null::boolean
)
returns table
(
	target_variable_source				integer,
	target_variable_target				integer,
	metadata_source						json,
	metadata_target						json,
	metadata_diff						boolean,
	metadata_diff_national_language		boolean,
	metadata_diff_english_language		boolean,
	missing_metadata_national_language	boolean
)
as
$$
declare
		_cond	text;
begin
		if _metadatas is null
		then
			raise exception 'Error 01: fn_etl_check_target_variable_metadatas: Input argument _metadatas must not by NULL!';
		end if;
		-------------------------------
		if _metadata_diff is null
		then
			_cond := 'TRUE';
		else
			if _metadata_diff = true
			then 
				_cond := 'metadata_diff = true';
			else
				_cond := 'metadata_diff = false';
			end if;
		end if;
		-------------------------------
		if _national_language = 'en'
		then
			return query execute
			'
			with
			w1 as	(
					select json_array_elements($1) as s
					)
			,w2 as	(
					select
							(s->>''target_variable'')::integer		as target_variable_source,
							(s->>''etl_id'')::integer				as target_variable_target,
							(s->>''metadata'')::json				as metadata_source
					from w1
					)
			,w3 as	(
					select
							ctv.id,
							ctv.metadata as metadata_target
					from
							@extschema@.c_target_variable as ctv
					where
							ctv.id in (select w2.target_variable_target from w2)
					)
			,w4 as	(
					select
							w2.target_variable_source,
							w2.target_variable_target,
							w2.metadata_source,
							w3.metadata_target,
							null::boolean as metadata_diff_national_language,
							case
								when	(
										(w2.metadata_source->''en'')::jsonb @> (w3.metadata_target->''en'')::jsonb
										and
										(w3.metadata_target->''en'')::jsonb @> (w2.metadata_source->''en'')::jsonb
										)
								then
										false
								else
										true
							end
								as metadata_diff_english_language,
							null::boolean as missing_metadata_national_language
					from
							w2 inner join w3 on w2.target_variable_target = w3.id
					)
			,w5 as	(
					select
							w4.target_variable_source,
							w4.target_variable_target,
							w4.metadata_source,
							json_build_object(''en'',w4.metadata_target->''en'') as metadata_target,
							w4.metadata_diff_english_language as metadata_diff,
							w4.metadata_diff_national_language,
							w4.metadata_diff_english_language,
							w4.missing_metadata_national_language
					from
							w4
					)
			select
					w5.target_variable_source,
					w5.target_variable_target,
					w5.metadata_source,
					w5.metadata_target,
					w5.metadata_diff,
					w5.metadata_diff_national_language,
					w5.metadata_diff_english_language,
					w5.missing_metadata_national_language
			from
					w5
			where
					'|| _cond ||'
			order
					by w5.target_variable_source;
			'
			using _metadatas;
		else -- check national and english metadatas
			return query execute
			'
			with
			w1 as	(
					select json_array_elements($2) as s
					)
			,w2 as	(
					select
							(s->>''target_variable'')::integer		as target_variable_source,
							(s->>''etl_id'')::integer				as target_variable_target,
							(s->>''metadata'')::json				as metadata_source
					from w1
					)
			,w3 as	(
					select
							ctv.id,
							ctv.metadata as metadata_target
					from
							@extschema@.c_target_variable as ctv
					where
							ctv.id in (select w2.target_variable_target from w2)
					)
			,w4 as	(
					select
							w2.target_variable_source,
							w2.target_variable_target,
							w2.metadata_source,
							w3.metadata_target
					from
							w2 inner join w3 on w2.target_variable_target = w3.id
					)
			,w5 as	(
					select
							t.target_variable_target,
							array_agg(t.json_keys_target) as json_keys_target
					from
							(
							select
									w4.target_variable_target,
									json_object_keys(w4.metadata_target) as json_keys_target
							from w4
							) as t
					group
							by t.target_variable_target
					)
			,w6 as	(
					select
							w4.*,
							w5.json_keys_target,
							case when $1 = any(w5.json_keys_target) then false else true end as missing_metadata_national_language
					from
							w4 inner join w5 on w4.target_variable_target = w5.target_variable_target
					)
			,w7 as	(
					select
							w6.target_variable_source,
							w6.target_variable_target,
							w6.metadata_source,
							w6.metadata_target,
							w6.json_keys_target,
							w6.missing_metadata_national_language,
							-------------------------------------
							case
								when w6.missing_metadata_national_language = true
								then
									true
								else
									case
										when	(
												(w6.metadata_source->$1)::jsonb @> (w6.metadata_target->$1)::jsonb
												and
												(w6.metadata_target->$1)::jsonb @> (w6.metadata_source->$1)::jsonb
												)
										then
												false
										else
												true
									end
							end
								as metadata_diff_national_language,
							-------------------------------------
							case
								when	(
										(w6.metadata_source->''en'')::jsonb @> (w6.metadata_target->''en'')::jsonb
										and
										(w6.metadata_target->''en'')::jsonb @> (w6.metadata_source->''en'')::jsonb
										)
								then
										false
								else
										true
							end
								as metadata_diff_english_language
							-------------------------------------
					from
							w6
					)
			,w8 as	(
					select
							w7.*,
							case
								when w7.metadata_diff_national_language = true or w7.metadata_diff_english_language = true
								then true
								else false
							end as metadata_diff
					from w7
					)
			,w9 as	(
					select
							w8.target_variable_source,
							w8.target_variable_target,
							w8.metadata_source,
							w8.metadata_target as metadata_target_original,
							w8.metadata_diff,
							w8.metadata_diff_national_language,
							w8.metadata_diff_english_language,
							w8.missing_metadata_national_language,
							--------------------------------------
							case
								when w8.missing_metadata_national_language = true
								then
									json_build_object(''en'',w8.metadata_target->''en'')
								else
									json_build_object	(
														$1,w8.metadata_target->$1,
														''en'',w8.metadata_target->''en''
														)
							end
								as metadata_target
					from
							w8
					)
			select
					w9.target_variable_source,
					w9.target_variable_target,
					w9.metadata_source,
					w9.metadata_target,
					w9.metadata_diff,
					w9.metadata_diff_national_language,
					w9.metadata_diff_english_language,
					w9.missing_metadata_national_language
			from
					w9
			where
					'|| _cond ||'
			order
					by w9.target_variable_source;
			'
			using _national_language, _metadatas;
		end if;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_check_target_variable_metadatas(json, character varying, boolean) is
'Function returns records of target variables for given input arguments. If input argument _metadata_diff = true
then function returns records of target variables that their metadatas are not different. If input argument
_metadata_diff = false then function returns records of target variables that their metadatas are different.
If input argument _metadata_diff is null (_metadata is true or false) then function returns both cases.';

grant execute on function @extschema@.fn_etl_check_target_variable_metadatas(json, character varying, boolean) to public;
-- </function>



-- <function name="fn_etl_update_target_variable_metadata" schema="extschema" src="functions/extschema/etl/fn_etl_update_target_variable_metadata.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_update_target_variable_metadata(integer, json, boolean, boolean, boolean, boolean, boolean, character varying)

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_update_target_variable_metadata(integer, json, boolean, boolean, boolean, boolean, boolean, character varying) CASCADE;

create or replace function @extschema@.fn_etl_update_target_variable_metadata
(
	_target_variable					integer, 											-- ETL_ID of target variable
	_metadata							json,												-- metadatas from source DB
	_metadata_diff_national_language	boolean,											-- identification if (TRUE) national language metadatas are different between source or target variable
	_metadata_diff_english_language		boolean,											-- identification if (TRUE) english language metadatas are different between source or target variable
	_missing_national_language			boolean,											-- identification if (TRUE) national language metadatas are missing for target variable
	_english_language_boolean			boolean,											-- english element for update
	_national_language_boolean			boolean,											-- national element for update
	_national_language					character varying default 'en'::character varying	-- identification of language mode for this function
)
returns text
as
$$
declare
		_variant				text;
		_national_languages		character varying[];
		_json_keys_target_db	text[];
		_json_objs_target_db	text[];
		_json_keys_source_db	text[];
		_json_objs_source_db	text[];
		_json_keys4update		text[];
		_json_objs4update		text[];
		_query_i_metadata		text;
		_query_i_join			text;
		_query_i_columns		text;
		_query_i_elements		text;
		_query_metadata			text;
		_query_join				text;
		_query_columns			text;
		_query_elements			text;
		_query_res				text;
		_json4update			json;
		_res					text;
begin
	if _target_variable is null
	then
		raise exception 'Error 01: fn_etl_update_target_variable_metadata: Input argument _target_variable must not be NULL!';
	end if; 

	if _metadata is null
	then
		raise exception 'Error 02: fn_etl_update_target_variable_metadata: Input argument _metadata must not be NULL!';
	end if;

	if _metadata_diff_english_language is null
	then
		raise exception 'Error 03: fn_etl_update_target_variable_metadata: Input argument _metadata_diff_english_language must not be NULL!';
	end if;

	if _english_language_boolean is null
	then
		raise exception 'Error 04: fn_etl_update_target_variable_metadata: Input argument _english_language_boolean must not be NULL!';
	end if;	

	if _national_language_boolean is null
	then
		raise exception 'Error 05: fn_etl_update_target_variable_metadata: Input argument _national_language_boolean must not be NULL!';
	end if;

	if _english_language_boolean = false and _national_language_boolean = false
	then
		raise exception 'Error 06: fn_etl_update_target_variable_metadata: One of the input argument _english_language_boolean or _national_language_boolean must be TRUE!';
	end if;
	-------------------------------------------------------
	if _national_language = 'en'
	then
			-- V0
			if (_english_language_boolean = true and _national_language_boolean = false)
			then
				if _metadata_diff_national_language is not null or _missing_national_language is not null
				then
					raise exception 'Error 07: fn_etl_update_target_variable_metadata: If national language is set to "EN" then update is allowed only for english metadata. The both input arguments "_metadata_diff_national_language" and "_missing_national_language" must be NULL!';
				end if;

				raise notice 'UPDATE is allowed';
				_variant := 'V0';
			else
				raise exception 'Error 08: fn_etl_update_target_variable_metadata: If national language is set to "EN" then update is allowed only for english metadata.';
			end if;
	else
		-- branch 'national language'

		-- V1
		if _english_language_boolean = true and _national_language_boolean = false
		then
			if _metadata_diff_english_language = true
			then
				raise notice 'UPDATE is allowed';
				_variant := 'V1';
			else
				raise exception 'Error 09: fn_etl_update_target_variable_metadata: If for language application mode is set national language and user wants to update only english metadata then the input argument _english_language_boolean must be true and the inpurt argument _national_language_boolean must be false!';
			end if;
		end if;

		-- V2
		if _english_language_boolean = true and _national_language_boolean = true
		then
			if 
				_metadata_diff_english_language = true and
				(_metadata_diff_national_language = true and _missing_national_language = false)
			then
				raise notice 'UPDATE is allowed';
				_variant := 'V2';
			else
				raise exception 'Error 10: fn_etl_update_target_variable_metadata: If for language application mode is set national language and user wants to update english and national metadata then the input argument _english_language_boolean must be true and the input argument _national_language_boolean must be true, and in case english updating the input argument _metadata_diff_english_language must be true, and in case national updating the input argument _metadata_diff_national_language must be true and the input argument _missing_national_language must be false!';
			end if;
		end if;

		-- V3
		if _english_language_boolean = false and _national_language_boolean = true
		then
			if	(_metadata_diff_national_language = true and _missing_national_language = false)
			then
				raise notice 'UPDATE is allowed';
				_variant := 'V3';
			else
				raise exception 'Error 11: fn_etl_update_target_variable_metadata: If for language application mode is set national language and user wants to update national metadata then the input argument _english_language_boolean must be false and the input argument _national_language_boolean must be true, and in case national updating the input argument _metadata_diff_national_language must be true and the input argument _missing_national_language must be false!';				
			end if;
		end if;

	end if;
	-------------------------------------------------------


	-------------------------------------------------------
	-------------------------------------------------------
	/*
	case
		when _national_language is     null and _english_language = true then  _national_languages := array['en'::character varying];
		when _national_language is not null and _english_language = true then  _national_languages := array['en'::character varying] || array[_national_language];
		when _national_language is not null and _english_language = false then _national_languages := array[_national_language];
		else
			raise exception 'Error 04: fn_etl_update_target_variable_metadata: If input argument _national_language is null then input argument _english_language must be TRUE!';
	end case;
	*/

	case
		when _variant in ('V0','V1') then  _national_languages := array['en'::character varying];
		when _variant in ('V2') then  _national_languages := array['en'::character varying] || array[_national_language];
		when _variant in ('V3') then _national_languages := array[_national_language];
		else
			raise exception 'Error 12: fn_etl_update_target_variable_metadata: Not defince internal argument _variant';
	end case;	

	/*
	-- distinct of values in _national_languages, becouse national language can be the same as english language
	select array_agg(t2.res) from (select distinct t1.res from (select unnest(_national_languages) as res) as t1) as t2
	into _national_languages;
	*/

	if (array_length(_national_languages,1) > 2 or array_length(_national_languages,1) < 1)
	then
		raise exception 'Error 13: fn_etl_update_target_variable_metadata: Number of elements in internal argument _national_languages must be one or two!';
	end if;

	-- check that elements in internal argument _national_languages are contained in input argument _metadata and in target DB
	for i in 1..array_length(_national_languages,1)
	loop
		-- input argument _metadata
		if	(
			with
			w1 as	(
					select json_object_keys(_metadata) as json_key
					)
			select count(w1.json_key) is distinct from 1 from w1
			where w1.json_key = _national_languages[i]
			)
		then
			raise exception 'Error 14: fn_etl_update_target_variable_metadata: The language = "%" for update in input argument _national_languages is not present in argument _metadata!',_national_languages[i];
		end if;

		-- metadata in target DB
		if	(
			with
			w1 as	(
					select json_object_keys(ctv.metadata) as json_key
					from @extschema@.c_target_variable as ctv
					where ctv.id = _target_variable
					)
			select count(w1.json_key) is distinct from 1 from w1
			where w1.json_key = _national_languages[i]
			)
		then
			raise exception 'Error 15: fn_etl_update_target_variable_metadata: The language = % for update in input argument _national_languages is not present in metadatas for given target_variable in target DB!',_national_languages[i];
		end if;

	end loop;
	-------------------------------------------------------
	-------------------------------------------------------

	-- get ALL json keys in target DB for given target variable that will not be changed
	with
	w1 as	(
			select json_object_keys(ctv.metadata) as json_key
			from @extschema@.c_target_variable as ctv
			where ctv.id = _target_variable
			)
	,w2 as	(
			select distinct w1.json_key, 'tdb' as json_obj from w1
			)
	select
			array_agg(w2.json_key),
			array_agg(w2.json_obj)
	from
			w2 where w2.json_key not in (select unnest(_national_languages))	-- NOT IN !!!
	into
			_json_keys_target_db,	-- list of json keys for no changes
			_json_objs_target_db;
		
	-- json keys source
	with
	w1 as	(
			select
					unnest(_national_languages) as json_key,
					'sdb' as json_obj
			)
	select
			array_agg(w1.json_key),
			array_agg(w1.json_obj)
	from
			w1
	into
			_json_keys_source_db,
			_json_objs_source_db;
		
	if _json_keys_target_db is null
	then
		_json_keys4update := _json_keys_source_db;
		_json_objs4update := _json_objs_source_db;
	else
		_json_keys4update := _json_keys_target_db || _json_keys_source_db;
		_json_objs4update := _json_objs_target_db || _json_objs_source_db;
	end if;

	for i in 1..array_length(_json_keys4update,1)
	loop
		-----------------------------------------
		if _json_objs4update[i] = 'tdb'
		then
			if i = 1
			then
				_query_i_metadata := concat
				(
				'with w',i,' as (select 1 as id4join, ctv',i,'.metadata->''',_json_keys4update[i],''' as metadata from @extschema@.c_target_variable as ctv',i,' where ctv',i,'.id = $1)'
				);
			else
				_query_i_metadata := concat
				(
				',w',i,' as (select 1 as id4join, ctv',i,'.metadata->''',_json_keys4update[i],''' as metadata from @extschema@.c_target_variable as ctv',i,' where ctv',i,'.id = $1)'
				);
			end if;
		else
			if i = 1
			then
				_query_i_metadata := concat
				(
				'with w',i,' as (select 1 as id4join, $2->''',_json_keys4update[i],''' as metadata)'
				);
			else
				_query_i_metadata := concat
				(
				',w',i,' as (select 1 as id4join, $2->''',_json_keys4update[i],''' as metadata)'
				);
			end if;
		end if;
		-----------------------------------------
		if i = 1
		then
			_query_i_join := concat
			(
			' from w',i
			);
		else
			_query_i_join := concat
			(
			' inner join w',i,' on w',i-1,'.id4join = w',i,'.id4join'
			);
		end if;
		-----------------------------------------
		if i = 1
		then
			_query_i_columns := concat
			(
			'w1.id4join, w1.metadata as metadata_1'
			);
		else
			_query_i_columns := concat
			(
			', w',i,'.metadata as metadata_',i,''
			);
		end if;
		-----------------------------------------
		if i = 1
		then
			_query_i_elements := concat
			(
			'''',_json_keys4update[i],''',metadata_',i,''
			);
		else
			_query_i_elements := concat
			(
			',''',_json_keys4update[i],''',metadata_',i,''
			);
		end if;
		-----------------------------------------
		if i = 1
		then
			_query_metadata := _query_i_metadata;
			_query_join := _query_i_join;
			_query_columns := _query_i_columns;
			_query_elements := _query_i_elements;
		else
			_query_metadata := _query_metadata || _query_i_metadata;
			_query_join := _query_join || _query_i_join;
			_query_columns := _query_columns || _query_i_columns;
			_query_elements := _query_elements || _query_i_elements;
		end if;
	end loop;

	_query_res := concat
	(
		_query_metadata,
		',w_inner as (select ',_query_columns,_query_join,')
		select json_build_object(',_query_elements,') from w_inner;
		'
	);

	execute ''||_query_res||'' using _target_variable, _metadata into _json4update;

	update @extschema@.c_target_variable as ctv set metadata = _json4update
	where ctv.id = _target_variable;
		
	_res := concat('The metadata of target variable [c_target_variable.id = ',_target_variable,'] for language elements = ',_national_languages,' was changed.');

	return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_update_target_variable_metadata(integer, json, boolean, boolean, boolean, boolean, boolean, character varying) is
'Function update metadatas of target variable in table c_target_variable for given language elements.';

grant execute on function @extschema@.fn_etl_update_target_variable_metadata(integer, json, boolean, boolean, boolean, boolean, boolean, character varying) to public;
-- </function>



-- <function name="fn_etl_insert_target_variable_metadata" schema="extschema" src="functions/extschema/etl/fn_etl_insert_target_variable_metadata.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_insert_target_variable_metadata(integer, json, boolean, character varying)

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_insert_target_variable_metadata(integer, json, boolean, character varying) CASCADE;

create or replace function @extschema@.fn_etl_insert_target_variable_metadata
(
	_target_variable					integer, 				-- ETL_ID of target variable
	_metadata							json,					-- metadatas from source DB
	_missing_national_language			boolean,				-- identification if (TRUE) national language metadatas are missing for target variable
	_national_language					character varying		-- identification of language mode for this function
)
returns text
as
$$
declare
		_json_keys_target_db	text[];
		_query_i_select			text;
		_query_i_join			text;
		_query_i_metadata		text;
		_query_i_object			text;
		_query_select			text;
		_query_join				text;
		_query_metadata			text;
		_query_object			text;
		_query_res				text;
		_json4update			json;
		_res					text;
begin
	if _target_variable is null
	then
		raise exception 'Error 01: fn_etl_insert_target_variable_metadata: Input argument _target_variable must not be NULL!';
	end if; 

	if _metadata is null
	then
		raise exception 'Error 02: fn_etl_insert_target_variable_metadata: Input argument _metadata must not be NULL!';
	end if;

	if _missing_national_language is null
	then
		raise exception 'Error 03: fn_etl_insert_target_variable_metadata: Input argument _missing_national_language must not be NULL!';
	end if;
	-------------------------------------------------------
	if _national_language = 'en'
	then
		raise exception 'Error 04: fn_etl_insert_target_variable_metadata: If national language is set to "EN" then insert is not allowed. Insert is allowed only for national language metadata.';
	else
		-- branch 'national language'

		if _missing_national_language = true
		then
			raise notice 'INSERT is allowed';
		else
			raise exception 'Error 05: fn_etl_insert_target_variable_metadata: If for language application mode is set national language different from "EN" then the input argument _missing_national_language must be true!';
		end if;
	end if;
	-------------------------------------------------------
	if length(_national_language) > 2 or length(_national_language) < 2
	then
		raise exception 'Error 06: fn_etl_insert_target_variable_metadata: The length of input argument _national_language must by two.';
	end if;
	-------------------------------------------------------
	-- check that language in internal argument _national_language is contained in input argument _metadata
	if	(
		with
		w1 as	(
				select json_object_keys(_metadata) as json_key
				)
		select count(w1.json_key) is distinct from 1 from w1
		where w1.json_key = _national_language
		)
	then
		raise exception 'Error 07: fn_etl_insert_target_variable_metadata: The language = "%" for insert is not present in input argument _metadata!',_national_language;
	end if;
	-------------------------------------------------------
	-- check that language in internal argument _national_language is NOT contained in TARGET metadata for given target variable
	if	(
		with
		w1 as	(
				select json_object_keys(ctv.metadata) as json_key
				from @extschema@.c_target_variable as ctv
				where ctv.id = _target_variable
				)
		select count(w1.json_key) is distinct from 0 from w1
		where w1.json_key = _national_language 
		)
	then
		raise exception 'Error 08: fn_etl_insert_target_variable_metadata: The language = % for insert is present in target metadata for given target variable!',_national_language;
	end if;
	-------------------------------------------------------
	-------------------------------------------------------
	with
	w1 as	(
			select json_object_keys(ctv.metadata) as json_key
			from @extschema@.c_target_variable as ctv
			where ctv.id = _target_variable
			)
	,w2 as	(
			select distinct w1.json_key from w1
			)
	select array_agg(w2.json_key) from w2
	into _json_keys_target_db; -- list of language elements without changes
	-------------------------------------------------------
	for i in 1..array_length(_json_keys_target_db,1)
	loop
		_query_i_select := concat
		(
		',w',i,' as (select 1 as id4join, ctv',i,'.metadata->''',_json_keys_target_db[i],''' as metadata from @extschema@.c_target_variable as ctv',i,' where ctv',i,'.id = $2)'
		);

		_query_i_join := concat
		(
		' inner join w',i,' on w.id4join = w',i,'.id4join'
		);

		_query_i_metadata := concat
		(
		',w',i,'.metadata as metadata_',_json_keys_target_db[i]
		);

		_query_i_object := concat
		(
		',''',_json_keys_target_db[i],''',w_inner.metadata_',_json_keys_target_db[i]
		);
		
		if i = 1
		then
			_query_select := _query_i_select;
			_query_join := _query_i_join;
			_query_metadata := _query_i_metadata;
			_query_object := _query_i_object;
		else
			_query_select := _query_select || _query_i_select;
			_query_join := _query_join || _query_i_join;
			_query_metadata := _query_metadata || _query_i_metadata;
			_query_object := _query_object || _query_i_object;
		end if;
	end loop;
	-------------------------------------------------------
	_query_res := concat
	(
	'
	with
	w as	(
			select 1 as id4join, ($1->''',_national_language,''') as metadata
			)
	'
	,_query_select,
	'
	,w_inner as	(
				select
						w.id4join,w.metadata'
						,_query_metadata,
				' from w'
				,_query_join,
				')'
	'
	select
			--w_inner.id4join,
			json_build_object
				(
				''',_national_language,''',w_inner.metadata
				',_query_object,'
				) as metadata
	from
		w_inner;'
	);
	-------------------------------------------------------
	execute ''||_query_res||'' using _metadata, _target_variable into _json4update;
		
	update @extschema@.c_target_variable as ctv set metadata = _json4update
	where ctv.id = _target_variable;
		
	_res := concat('The metadata of target variable [c_target_variable.id = ',_target_variable,'] was changed. Added metadata for national language "',_national_language,'".');
	-------------------------------------------------------
	return _res;

end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_insert_target_variable_metadata(integer, json, boolean, character varying) is
'Function insert missing national language metadatas of target variable in table c_target_variable.';

grant execute on function @extschema@.fn_etl_insert_target_variable_metadata(integer, json, boolean, character varying) to public;
-- </function>



-- <function name="fn_etl_check_target_variable" schema="extschema" src="functions/extschema/etl/fn_etl_check_target_variable.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_check_target_variable(json, integer[])

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_check_target_variable(json, integer[]) CASCADE;

create or replace function @extschema@.fn_etl_check_target_variable
(
	_metadata json,
	_etl_id integer[] default null::integer[]
)
returns integer
as
$$
declare
		_id integer;
begin
		if _metadata is null
		then
			raise exception 'Error 01: fn_etl_check_target_variable: Input argument _metadata must not by NULL!';
		end if; 
	
		select ctv.id from @extschema@.c_target_variable as ctv
		where ctv.id not in (select unnest(_etl_id))
		and	(
				(ctv.metadata->'en')::jsonb @> (_metadata->'en')::jsonb
				and
				(_metadata->'en')::jsonb @> (ctv.metadata->'en')::jsonb
				)
		into _id;

		return _id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_check_target_variable(json, integer[]) is
'The function returns record ID from table c_target_variable if source metadata of target variable were founded (are the same) in c_target_variable table in target database.';

grant execute on function @extschema@.fn_etl_check_target_variable(json, integer[]) to public;
-- </function>



-- <function name="fn_etl_get_target_variable" schema="extschema" src="functions/extschema/etl/fn_etl_get_target_variable.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_get_target_variable(character varying, integer[])

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_get_target_variable(character varying, integer[]) CASCADE;

create or replace function @extschema@.fn_etl_get_target_variable
(
	_national_language	character varying(2) default 'en'::character varying,
	_etl_id				integer[] default null::integer[]
)
returns table
(
	id			integer,
	metadata	json
)
as
$$
declare
begin
		if _national_language = 'en'
		then
			return query
			with
			w1 as	(
					select
							ctv.id,
							ctv.metadata->'en' as metadata
					from
							 @extschema@.c_target_variable as ctv
					where
							ctv.id not in (select unnest(_etl_id))
					)
			select
					w1.id,
					json_build_object('en',w1.metadata) as metadata
			from
					w1 order by w1.id;
		else
			return query
			with
			w1 as	(
					select
							ctv.id,
							ctv.metadata->'en' as metadata
					from
							 @extschema@.c_target_variable as ctv
					where
							ctv.id not in (select unnest(_etl_id))
					)
			,w2 as	(
					select
							ctv.id,
							ctv.metadata->_national_language as metadata
					from
							 @extschema@.c_target_variable as ctv
					where
							ctv.id not in (select unnest(_etl_id))
					)
			,w3 as	(
					select
							w1.id,
							w1.metadata as metadata_en,
							w2.metadata as metadata_nl
					from
								w1
					inner join	w2 on w1.id = w2.id
					)
			select
					w3.id,
					json_build_object
						(
						_national_language,w3.metadata_nl,
						'en',w3.metadata_en
						) as metadata
			from
					w3 order by w3.id;
		end if;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_get_target_variable(character varying, integer[]) is
'Function returns records from table c_target_variable.';

grant execute on function @extschema@.fn_etl_get_target_variable(character varying, integer[]) to public;
-- </function>