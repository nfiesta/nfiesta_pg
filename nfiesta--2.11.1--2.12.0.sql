--
-- Copyright 2021, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--


-------------------------------------------------------------------------------
-- update column metadata => add information state or change and etl_join_id
-------------------------------------------------------------------------------
with
w1 as	(
		select
				id,
				state_or_change,
				etl_join_id,
				metadata,
				((metadata->'cs')->'indikátor') as cs_indicator,
				((metadata->'cs')->'jednotka výstupu') as cs_unit,
				((metadata->'cs')->'definiční varianta') as cs_definition_variant,
				((metadata->'cs')->'plošná doména') as cs_area_domain,
				((metadata->'cs')->'populace') as cs_population,
				((metadata->'cs')->'stav opravy dat') as cs_state,
				((metadata->'en')->'indicator') as en_indicator,
				((metadata->'en')->'unit of result') as en_unit,
				((metadata->'en')->'definition variant') as en_definition_variant,
				((metadata->'en')->'area domain') as en_area_domain,
				((metadata->'en')->'population') as en_population,
				((metadata->'en')->'state of data correction') as en_state
		from
				@extschema@.c_target_variable
		),
w2 as	(
		select
				id,
				state_or_change,
				etl_join_id,
				metadata,
				json_build_object
					(
					'cs',json_build_object
							(
							'indikátor',cs_indicator,
							'jednotka výstupu',cs_unit,
							'definiční varianta',cs_definition_variant,
							'plošná doména',cs_area_domain,
							'populace',cs_population,
							'stav opravy dat',cs_state,
							'stav nebo změna',
									case when state_or_change = 1
										then json_build_object('id',1,'label','stav','description','Stavová proměnná.')
										else json_build_object('id',2,'label','změna','description','Změna proměnné - rozdíl dvou stavů.')
									end,
							'etl_join_id',etl_join_id
							),
					'en',json_build_object
							(
							'indicator',en_indicator,
							'unit of result',en_unit,
							'definition variant',en_definition_variant,
							'area domain',en_area_domain,
							'population',en_population,
							'state of data correction',en_state,
							'state or change',
									case when state_or_change = 1
										then json_build_object('id',1,'label','state','description','State variable.')
										else json_build_object('id',2,'label','change','description','Change variable - difference of two states.')
									end,
							'etl_join_id',etl_join_id
							)
					) as metadata4update
		from w1
		)
update @extschema@.c_target_variable set metadata = w2.metadata4update
from w2 where w2.id = c_target_variable.id;
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------


DROP VIEW @extschema@.v_conf_overview;


ALTER TABLE  @extschema@.c_target_variable DROP COLUMN state_or_change;
ALTER TABLE  @extschema@.c_target_variable DROP COLUMN etl_join_id;


DROP TABLE @extschema@.c_state_or_change;


-- <view name="v_conf_overview" schema="extschema" src="views/extschema/v_conf_overview.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- drop view if exists @extschema@.v_conf_overview;
create or replace view @extschema@.v_conf_overview as (
with w_confs AS MATERIALIZED (
	select
		t_estimate_conf.id as estimate_conf,
		t_total_estimate_conf.id as total_estimate_conf,
		t_total_estimate_conf.panel_refyearset_group,
		t_aux_conf.id as aux_conf,
		t_aux_conf.panel_refyearset_group AS aux_conf_prg,
		t_total_estimate_conf_denom.id as total_estimate_conf__denom,
		t_aux_conf_denom.id as aux_conf__denom,
		case 	when 
				t_total_estimate_conf.id is not null and 
				t_aux_conf.id is null and 
				t_total_estimate_conf_denom.id is null and 
				t_aux_conf_denom.id is null 
			then '1p_total'
			when 
				t_total_estimate_conf.id is not null and 
				t_aux_conf.id is null and 
				t_total_estimate_conf_denom.id is not null and 
				t_aux_conf_denom.id is null 
			then '1p1p_ratio'
			when 
				t_total_estimate_conf.id is not null and 
				t_aux_conf.id is not null and 
				t_total_estimate_conf_denom.id is null and 
				t_aux_conf_denom.id is null 
			then '2p_total'
			when 
				t_total_estimate_conf.id is not null and 
				t_aux_conf.id is not null and 
				t_total_estimate_conf_denom.id is not null and 
				t_aux_conf_denom.id is null 
			then '2p1p_ratio'
			when 
				t_total_estimate_conf.id is not null and 
				t_aux_conf.id is null and 
				t_total_estimate_conf_denom.id is not null and 
				t_aux_conf_denom.id is not null 
			then '1p2p_ratio'
			when 
				t_total_estimate_conf.id is not null and 
				t_aux_conf.id is not null and 
				t_total_estimate_conf_denom.id is not null and 
				t_aux_conf_denom.id is not null 
			then '2p2p_ratio'
			else 'unknown'
		end as estimate_type_str,
		coalesce(t_aux_conf.sigma, t_aux_conf_denom.sigma) as sigma,
		coalesce(t_total_estimate_conf.force_synthetic, t_total_estimate_conf_denom.force_synthetic) as force_synthetic,
------------------------additional info begin-----------------------------------
		coalesce(t_aux_conf.param_area, t_aux_conf_denom.param_area) as param_area, 
		coalesce(f_a_param_area.param_area_code, f_a_param_area_denom.param_area_code) as param_area_code,
		coalesce(t_aux_conf.model, t_aux_conf_denom.model) as model, 
		coalesce(t_model.description, t_model_denom.description) as model_description,
		t_total_estimate_conf.estimation_cell, c_estimation_cell.label as estimation_cell_label, c_estimation_cell.estimation_cell_collection, --f_a_cell.geom,
		t_variable.id as variable, t_variable.target_variable,
		(((c_target_variable.metadata->'en')->'indicator')->>'label'::varchar) as target_variable_label,
		t_variable.sub_population_category, c_sub_population_category.label as sub_population_category_label,
		t_variable.area_domain_category, c_area_domain_category.label as area_domain_category_label,
		c_estimation_period.estimate_date_begin, c_estimation_period.estimate_date_end
------------------------additional info end-------------------------------------
	from @extschema@.t_estimate_conf
        inner join @extschema@.t_total_estimate_conf ON t_total_estimate_conf.id = t_estimate_conf.total_estimate_conf
	inner join @extschema@.c_estimation_period ON c_estimation_period.id = t_total_estimate_conf.estimation_period
	left join @extschema@.t_aux_conf ON t_aux_conf.id = t_total_estimate_conf.aux_conf
        left join @extschema@.t_total_estimate_conf as t_total_estimate_conf_denom ON t_total_estimate_conf_denom.id = t_estimate_conf.denominator
	left join @extschema@.t_aux_conf as t_aux_conf_denom ON t_aux_conf_denom.id = t_total_estimate_conf_denom.aux_conf
------------------------additional info begin-----------------------------------
	left join @extschema@.f_a_param_area on (t_aux_conf.param_area = f_a_param_area.gid)
	left join @extschema@.t_model on (t_aux_conf.model = t_model.id)
	left join @extschema@.f_a_param_area as f_a_param_area_denom on (t_aux_conf_denom.param_area = f_a_param_area_denom.gid)
	left join @extschema@.t_model as t_model_denom on (t_aux_conf_denom.model = t_model_denom.id)
	inner join @extschema@.c_estimation_cell on (t_total_estimate_conf.estimation_cell = c_estimation_cell.id)
	inner join @extschema@.t_variable on (t_total_estimate_conf.variable = t_variable.id)
	inner join @extschema@.c_target_variable on (t_variable.target_variable = c_target_variable.id)
	left join @extschema@.c_sub_population_category on (t_variable.sub_population_category = c_sub_population_category.id)
	left join @extschema@.c_area_domain_category on (t_variable.area_domain_category = c_area_domain_category.id)
------------------------additional info end-------------------------------------
	order by t_estimate_conf.id
)
, w_confs_2nd_panels AS MATERIALIZED (
	select w_confs.*,
		array_agg(t_panel_refyearset_group.panel order by t_panel_refyearset_group.panel) as panels,
		array_agg(t_panel_refyearset_group.reference_year_set order by t_panel_refyearset_group.panel) as ref_year_sets
	from w_confs
	inner join @extschema@.t_panel_refyearset_group on (t_panel_refyearset_group.panel_refyearset_group = w_confs.panel_refyearset_group)
	group by w_confs.estimate_conf, w_confs.total_estimate_conf, w_confs.panel_refyearset_group, 
			w_confs.aux_conf, w_confs.aux_conf_prg, w_confs.total_estimate_conf__denom, w_confs.aux_conf__denom,
			w_confs.estimate_type_str, w_confs.sigma, w_confs.force_synthetic,
			w_confs.param_area, w_confs.param_area_code,
			w_confs.model, w_confs.model_description,
			w_confs.estimation_cell, w_confs.estimation_cell_label, w_confs.estimation_cell_collection,
			w_confs.variable, w_confs.target_variable, w_confs.target_variable_label,
			w_confs.sub_population_category, w_confs.sub_population_category_label, w_confs.area_domain_category, w_confs.area_domain_category_label,
			w_confs.estimate_date_begin, w_confs.estimate_date_end
)
	select w_confs_2nd_panels.*,
		array_agg(t_panel_refyearset_group.panel order by t_panel_refyearset_group.panel) as aux_conf_panels
	from w_confs_2nd_panels
	left join @extschema@.t_panel_refyearset_group on (t_panel_refyearset_group.panel_refyearset_group = w_confs_2nd_panels.aux_conf_prg)
	group by w_confs_2nd_panels.estimate_conf, w_confs_2nd_panels.total_estimate_conf, w_confs_2nd_panels.panel_refyearset_group, 
			w_confs_2nd_panels.aux_conf, w_confs_2nd_panels.aux_conf_prg, w_confs_2nd_panels.total_estimate_conf__denom, w_confs_2nd_panels.aux_conf__denom,
			w_confs_2nd_panels.estimate_type_str, w_confs_2nd_panels.sigma, w_confs_2nd_panels.force_synthetic,
			w_confs_2nd_panels.param_area, w_confs_2nd_panels.param_area_code,
			w_confs_2nd_panels.model, w_confs_2nd_panels.model_description,
			w_confs_2nd_panels.estimation_cell, w_confs_2nd_panels.estimation_cell_label, w_confs_2nd_panels.estimation_cell_collection, w_confs_2nd_panels.variable, w_confs_2nd_panels.target_variable, w_confs_2nd_panels.target_variable_label,
			w_confs_2nd_panels.sub_population_category, w_confs_2nd_panels.sub_population_category_label, w_confs_2nd_panels.area_domain_category, w_confs_2nd_panels.area_domain_category_label,
			w_confs_2nd_panels.estimate_date_begin, w_confs_2nd_panels.estimate_date_end,
			w_confs_2nd_panels.panels, w_confs_2nd_panels.ref_year_sets

);
--select * from @extschema@.v_conf_overview;

GRANT SELECT ON TABLE @extschema@.v_conf_overview TO PUBLIC;
-- </view>



-- <function name="fn_get_panels_in_estimation_cells" schema="extschema" src="functions/extschema/configuration/fn_get_panels_in_estimation_cells.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_get_panels_in_estimation_cells(integer, regclass)

--DROP FUNCTION @extschema@.fn_get_panels_in_estimation_cells(integer,date,date,character varying,integer,integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_get_panels_in_estimation_cells(_estimation_cells integer[], _estimate_date_begin date, _estimate_date_end date, _target_variable integer)
RETURNS TABLE (
stratum 			integer,
stratum_label 			varchar(20),
panel 				integer,
panel_label			varchar(20),
panel_subset			integer,
reference_year_set 		integer,
reference_year_set_label 	varchar(20),
reference_date_begin 		date,
reference_date_end 		date,
reference_year_set_fit 		boolean,
portion_of_panel_inside		double precision,
portion_of_period_covered	double precision,
total 				integer,
is_max				boolean
)
AS
$function$
DECLARE
_change_variable 	boolean;
_estimation_cell	integer;
_cell_area_m2		double precision;
_stratas_all		integer[];
_stratas		integer[];
_sum_stratas_m2		double precision;
_result_test		boolean;

BEGIN
/*
_change_variable := (
		SELECT		CASE WHEN t3.id = 2 THEN true ELSE false END
		FROM 		@extschema@.t_variable AS t1
		LEFT JOIN 	@extschema@.c_target_variable AS t2 ON t1.target_variable = t2.id
		LEFT JOIN	@extschema@.c_state_or_change AS t3 ON t2.state_or_change = t3.id
		WHERE t1.id = $5
		);
*/
	
-- panels order
--IF _panels IS NOT NULL THEN _panels := (SELECT array_agg(panel ORDER BY panel) FROM unnest(_panels) AS t(panel));
--END IF;

FOREACH _estimation_cell IN ARRAY _estimation_cells
LOOP

	-- cell data
	WITH w_cell AS (
		-- cell geometry can be divided into smaller blocks, thats why sum() 
		SELECT estimation_cell, sum(st_area(geom)) AS area_m2
		FROM @extschema@.f_a_cell
		WHERE estimation_cell = _estimation_cell
		GROUP BY estimation_cell
	)
	SELECT	area_m2
	FROM w_cell
	INTO _cell_area_m2;

	-- stratas in cell
	WITH w_stratas AS (
		SELECT t1.stratum, t1.area_m2
		FROM @extschema@.t_stratum_in_estimation_cell AS t1
		WHERE estimation_cell = _estimation_cell
	)
	SELECT	array_agg(t1.stratum), sum(t1.area_m2)
	FROM w_stratas AS t1
	INTO _stratas, _sum_stratas_m2;

	IF _sum_stratas_m2 IS NULL
	THEN
		RAISE EXCEPTION 'Estimation cell is not covered by any stratum (estimation_cell = %).', _estimation_cell;
	END IF;

	-- test on cell coverage
	-- two decimal places considered when comparing the final product of division with 1
	_result_test := (SELECT CASE WHEN round(_sum_stratas_m2::numeric/_cell_area_m2::numeric, 2) = 1 THEN true ELSE false END);
	
	IF _result_test = false
	THEN
		RAISE WARNING 'The intersection of stratas and cell (% ha) is less than area of whole estimation cell (% ha).', 
				round(_sum_stratas_m2::numeric/10000.0,1), round(_cell_area_m2::numeric/10000.0,1);
	END IF;

	_stratas_all := _stratas_all || _stratas;

END LOOP;

-- return the list of panels for given target variable
-- if the reference time period is met then it is stated in column reference_year_set_fit
RETURN QUERY
WITH w_data AS (
	SELECT
		t1.id AS stratum,
		t1.stratum AS stratum_label, 
		t2.panel, 
		t2.panel_label,
		t2.panel_subset,
		t2.reference_year_set,
		t2.reference_year_set_label,
		t2.reference_date_begin,
		t2.reference_date_end,
		t2.reference_year_set_fit,
		t2.estimate_range,
		t2.reference_range,
		t2.intersection_range,

		-- find out what portion of panel is inside the estimate range
		CASE WHEN isempty(intersection_range) = false
		THEN
			EXTRACT(DAYS FROM (upper(intersection_range) - lower(intersection_range))) / -- interval of intersection
			EXTRACT(DAYS FROM (upper(reference_range) - lower(reference_range))) -- interval of reference period

		ELSE
			0 -- 0 percent of panel inside the period
		END AS portion_of_panel_inside,

		-- find out what portion of estimation period is covered by panel
		CASE WHEN isempty(intersection_range) = false
		THEN
			EXTRACT(DAYS FROM (upper(intersection_range) - lower(intersection_range))) / -- interval of intersection
			EXTRACT(DAYS FROM (upper(estimate_range) - lower(estimate_range))) -- interval of estimation period
		ELSE 0 -- 0 percent covered by panel
		END AS portion_of_period_covered,
		t2.total
	FROM
		(SELECT t1.id, t1.stratum FROM sdesign.t_stratum AS t1 WHERE array[id] <@ _stratas_all) AS t1
	-- could be inner join, every stratum has to have at least one panel
	-- but in case of not available target variable, stratum would disappear from the list
	LEFT JOIN
		(SELECT
			t2.stratum,
			t2.id AS panel,
			t2.panel AS panel_label,
			t2.plot_count AS total,
			t2.panel_subset,
			t4.id AS reference_year_set,
			t4.reference_year_set AS reference_year_set_label,
			t4.reference_date_begin,
			t4.reference_date_end,
			-- test if reference_year_set fits the parameteres given
			CASE
			WHEN 	t4.reference_date_begin >= $2 AND
				t4.reference_date_end <= $3 
			THEN true
			ELSE false
			END AS reference_year_set_fit,

			-- range of estimation period
			tsrange(_estimate_date_begin,_estimate_date_end) AS estimate_range,
			-- range of reference period
			tsrange(t4.reference_date_begin,t4.reference_date_end) AS reference_range,

			-- intersection of dates
			tsrange(t4.reference_date_begin,t4.reference_date_end) * 
			tsrange(_estimate_date_begin,_estimate_date_end) AS intersection_range

		FROM
			sdesign.t_panel AS t2
		-- inner join, assuming every panel has SOME reference_year_set mapping
		INNER JOIN
			sdesign.cm_refyearset2panel_mapping AS t3
		ON
			t2.id = t3.panel
		-- again inner join, I am curious, what other reference year sets are possible in case the required dates does not fit (but target variable available)
		INNER JOIN
			sdesign.t_reference_year_set AS t4
		ON
			t3.reference_year_set = t4.id
		-- inner join, target variable must fit
		INNER JOIN
			@extschema@.t_available_datasets AS t5
		ON
			t2.id = t5.panel AND
			t4.id = t5.reference_year_set AND
			t5.variable = $4 
		) AS t2
	ON
		t1.id = t2.stratum

), w_max AS (
	SELECT 
		t1.stratum, t1.stratum_label, t1.panel, t1.panel_label, t1.panel_subset,
		t1.reference_year_set, t1.reference_year_set_label, t1.reference_date_begin, t1.reference_date_end, t1.reference_year_set_fit, 
		t1.portion_of_panel_inside, t1.portion_of_period_covered,
		t1.total,
		-- maximum number of plots for a group of panels with the same reference_year_set 
		-- and within belonging to the required reference period
		max(t1.total) OVER(PARTITION BY t1.stratum, t1.reference_year_set, t1.reference_year_set_fit) AS max_total
	FROM w_data AS t1
)
SELECT
	t1.stratum, t1.stratum_label, t1.panel, t1.panel_label, t1.panel_subset,
	t1.reference_year_set, t1.reference_year_set_label, t1.reference_date_begin, t1.reference_date_end, t1.reference_year_set_fit, 
	t1.portion_of_panel_inside, t1.portion_of_period_covered, t1.total,
	CASE WHEN t1.total = t1.max_total THEN true ELSE false END AS is_max
FROM
	w_max AS t1;

END;
$function$
LANGUAGE plpgsql
PARALLEL SAFE;

COMMENT ON FUNCTION @extschema@.fn_get_panels_in_estimation_cells(integer[], date, date, integer) IS 'Function returns list of stratas and panels in the estimation cell. For each panel there is an indicator (reference_year_set_fit) if the panel lies within estimation period and indicator max which tells if the panel is the one with the maximum possible plots.';
-- </function>



-- <function name="fn_1p_est_configuration" schema="extschema" src="functions/extschema/configuration/fn_1p_est_configuration.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_1p_est_configuration(integer, integer, integer, varchar, integer)

--DROP FUNCTION @extschema@.fn_1p_est_configuration(integer, integer, integer, character varying, integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_1p_est_configuration(
		_panel_refyearset_group integer, 
		_estimation_cell integer, _estimation_period integer, 
		_note varchar, _variable integer)
RETURNS integer
AS
$function$
DECLARE
_total_estimate_conf		integer;
_stratas_wp			integer[];
_target_label			varchar;
_cell				varchar;
_change_variable		boolean;
BEGIN

	_target_label := (
			SELECT		replace(
						replace(
							concat(coalesce(((t2.metadata->'en')->'indicator')->>'label'::varchar,'x'), ',', coalesce(t3.label,'x'), ',', coalesce(t4.label,'x'), ',', coalesce(t5.label,'x')),
						'x,',''),
					',x','') AS label
			FROM		@extschema@.t_variable AS t1
			LEFT JOIN 	@extschema@.c_target_variable AS t2 ON t1.target_variable = t2.id
			LEFT JOIN	@extschema@.c_sub_population_category AS t3 ON t1.sub_population_category = t3.id
			LEFT JOIN	@extschema@.c_area_domain_category AS t4 ON t1.area_domain_category = t4.id
			LEFT JOIN	@extschema@.c_auxiliary_variable_category AS t5 ON t1.auxiliary_variable_category = t5.id
			WHERE
				t1.id = _variable
			);

	/*
	_change_variable := (
			SELECT		CASE WHEN t3.id = 2 THEN true ELSE false END
			FROM 		@extschema@.t_variable AS t1
			LEFT JOIN 	@extschema@.c_target_variable AS t2 ON t1.target_variable = t2.id
			LEFT JOIN	@extschema@.c_state_or_change AS t3 ON t2.state_or_change = t3.id
			WHERE t1.id = _variable
			);
	*/
		

	_cell := (SELECT label FROM @extschema@.c_estimation_cell WHERE id = _estimation_cell);

	-- reset sequence (usually when previous atempt to configure fails, the sequence remains shifted)
	PERFORM setval('@extschema@.t_total_estimate_conf_id_seq', (SELECT coalesce(max(id),0)+1 FROM @extschema@.t_total_estimate_conf), FALSE);

	-- insert into table t_total_estimate_conf
	INSERT INTO @extschema@.t_total_estimate_conf (estimation_cell, estimation_period, total_estimate_conf, 
							variable, phase_estimate_type, aux_conf, panel_refyearset_group)
	VALUES
		(_estimation_cell, _estimation_period, concat('1p;T=',_target_label,';Cell=',_cell,_note), _variable, 1, NULL, _panel_refyearset_group)
	RETURNING id
	INTO _total_estimate_conf;

	-- insert into table t_estimate_conf
	INSERT INTO @extschema@.t_estimate_conf (estimate_type, total_estimate_conf, denominator)
	SELECT 1, _total_estimate_conf, NULL;

	RETURN _total_estimate_conf;
END;
$function$
LANGUAGE plpgsql
PARALLEL SAFE;

COMMENT ON FUNCTION @extschema@.fn_1p_est_configuration(integer,integer,integer,varchar,integer) IS 'Function makes the estimate configuration present in the database - provides inserts into the all necessary tables.';
-- </function>



-- <function name="fn_2p_est_configuration" schema="extschema" src="functions/extschema/configuration/fn_2p_est_configuration.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_2p_est_configuration(integer, regclass)

--DROP FUNCTION @extschema@.fn_2p_est_configuration(integer,integer,character varying,integer,integer,boolean);

CREATE OR REPLACE FUNCTION @extschema@.fn_2p_est_configuration(_estimation_cell integer, _estimation_period integer, _note varchar, _target_variable integer, _aux_conf integer, _force_synthetic boolean default False)
RETURNS integer
AS
$function$
DECLARE
_total_estimate_conf		integer;
_estimate_date_begin		date;
_estimate_date_end		date;
_stratas			integer[];
_stratas_wp			integer[];
_panels				integer[];
_refyearsets			integer[];
_panels_aux			integer[];
_param_area			integer;
_param_area_code		varchar;
_target_label			varchar;
_model				integer;
_cell				varchar;
_panel_refyearset_group		integer;
BEGIN

-- test for existing g_betas
-- otherwise the configuration cannot be done (sometimes the g_betas cannot be computed)
-- so this prevents to configure non-computable estimates

IF (SELECT count(*) FROM @extschema@.t_g_beta WHERE aux_conf = $5) = 0
THEN
	RAISE EXCEPTION 'G-betas for required aux_conf (%) are not available (cell=%, period=%). The computation of it was not run or is not able to compute (mostly the problem of matrix inversion).', _aux_conf, _estimation_cell, _estimation_period;
END IF;

SELECT estimate_date_begin, estimate_date_end
FROM @extschema@.c_estimation_period
WHERE id = _estimation_period
INTO _estimate_date_begin, _estimate_date_end;

IF _estimate_date_begin IS NULL OR _estimate_date_end IS NULL
THEN
	RAISE EXCEPTION 'At leats one of the estimate period dates (%, %) is NULL!', _estimate_date_begin, _estimate_date_end;
END IF;


-- create the label of estimate
_target_label := (
		SELECT		replace(
					replace(
						concat(coalesce(((t2.metadata->'en')->'indicator')->>'label'::varchar,'x'), ',', coalesce(t3.label,'x'), ',', coalesce(t4.label,'x'), ',', coalesce(t5.label,'x')),
					'x,',''),
			       	',x','') AS label
		FROM		@extschema@.t_variable AS t1
		LEFT JOIN 	@extschema@.c_target_variable AS t2 ON t1.target_variable = t2.id
		LEFT JOIN	@extschema@.c_sub_population_category AS t3 ON t1.sub_population_category = t3.id
		LEFT JOIN	@extschema@.c_area_domain_category AS t4 ON t1.area_domain_category = t4.id
		LEFT JOIN	@extschema@.c_auxiliary_variable_category AS t5 ON t1.auxiliary_variable_category = t5.id
		WHERE
			t1.id = _target_variable
		);

_param_area := (SELECT param_area FROM @extschema@.t_aux_conf WHERE id = $5);
_model := (SELECT model FROM @extschema@.t_aux_conf WHERE id = $5);
_param_area_code := (SELECT param_area_code FROM @extschema@.f_a_param_area WHERE gid = _param_area);
_cell := (SELECT label FROM @extschema@.c_estimation_cell WHERE id = $1);

-- reset sequence (usually when previous atempt to configure fails, the sequence remains shifted)
PERFORM setval('@extschema@.t_total_estimate_conf_id_seq', (SELECT coalesce(max(id),0)+1 FROM @extschema@.t_total_estimate_conf), FALSE);

	-- test on param_area_coverage
		SELECT
			array_agg(t1.id ORDER BY t1.id)
		FROM
			sdesign.t_stratum AS t1
		INNER JOIN
			@extschema@.f_a_param_area AS t2
		ON
			-- buffered stratum?
			-- no, if only buffer of the stratum would intersect the cell, 
			-- no consequences on estimate would be introduced, the buffer is in general used only for the edge effect compensation (the compensated area is within non-buffered stratum)
			ST_Intersects(t1.geom, t2.geom) AND NOT ST_Touches(t1.geom, t2.geom)
		WHERE
			t2.gid = _param_area
		INTO _stratas;

		IF _stratas IS NULL
		THEN
			RAISE EXCEPTION 'The specified cell is not intersected by any stratum. Choose another estimation cell.';
		END IF;

	-- existing panels configured in panel2aux_conf
		SELECT
			array_agg(t1.panel ORDER BY t1.panel)
		FROM
			@extschema@.t_panel_refyearset_group AS t1
		INNER JOIN
			@extschema@.t_aux_conf AS t2
		ON	t1.panel_refyearset_group = t2.panel_refyearset_group
		WHERE
			t2.id = _aux_conf
		INTO _panels_aux;

	-- check of panel2total_2ndph
	-- and addition of panels from param_area - is the target variable available not only in cell?

		WITH w_data AS MATERIALIZED (
			SELECT
				t1.id AS stratum, t2.id AS panel, t9.id AS reference_year_set, t2.plot_count AS total
			FROM
				sdesign.t_stratum AS t1
			INNER JOIN
				sdesign.t_panel AS t2
			ON
				t1.id = t2.stratum
			INNER JOIN
				sdesign.cm_refyearset2panel_mapping AS t8
			ON
				t2.id = t8.panel --AND
				--t8.id = t9.reference_year_set
			INNER JOIN
				sdesign.t_reference_year_set AS t9
			ON
				t8.reference_year_set = t9.id
			INNER JOIN
				@extschema@.t_available_datasets AS t6
			ON
				t2.id = t6.panel AND
				t9.id = t6.reference_year_set
			INNER JOIN
				@extschema@.t_variable AS t7
			ON
				t6.variable = t7.id
			WHERE
				array[t1.id] <@ _stratas AND
				t7.id = _target_variable AND 
				(t9.reference_date_begin >= _estimate_date_begin AND
				t9.reference_date_end <= _estimate_date_end)
			GROUP BY
				t1.id, t2.id, t9.id
		)
		SELECT
			array_agg(DISTINCT stratum ORDER BY stratum) AS stratas,
			array_agg(panel ORDER BY panel) AS panels,
			array_agg(reference_year_set ORDER BY panel) AS refyearsets
		FROM
			(SELECT
				stratum, panel, reference_year_set,
				total,
				max(total) OVER(PARTITION BY stratum, panel, reference_year_set) AS max_total
			FROM
				w_data
			) AS t1
		WHERE
			-- pick up the most dense panel with target variable
			total = max_total
		INTO _stratas_wp, _panels, _refyearsets;

		IF _panels != _panels_aux OR _panels IS NULL
		THEN
			RAISE EXCEPTION 'Not all panels coming from g_beta have available target variable! aux_conf: %, panels: %, panels_aux: %',
			_aux_conf, _panels, _panels_aux;
		END IF;

		_panel_refyearset_group := (SELECT @extschema@.fn_get_panel_refyearset_group(_panels, _refyearsets));

		IF _panel_refyearset_group IS NULL
		THEN
			_panel_refyearset_group := (SELECT @extschema@.fn_save_panel_refyearset_group(_panels, _refyearsets));
		END IF;

-- insert into table t_total_estimate_conf
INSERT INTO @extschema@.t_total_estimate_conf (estimation_cell, estimation_period, total_estimate_conf, variable, phase_estimate_type, force_synthetic, aux_conf, panel_refyearset_group)
VALUES
	($1, $2, concat('2p;T=',_target_label,';C=',_cell,';PA=',_param_area_code, ';m=',_model,_note), $4, 2, $6, $5, _panel_refyearset_group)
ON CONFLICT (estimation_cell, estimation_period, variable, phase_estimate_type, coalesce(force_synthetic,false), coalesce(aux_conf,0), panel_refyearset_group)
DO NOTHING
RETURNING id
INTO _total_estimate_conf;

IF _total_estimate_conf IS NOT NULL
THEN
	-- insert into table t_estimate_conf
	INSERT INTO @extschema@.t_estimate_conf (estimate_type, total_estimate_conf, denominator)
	SELECT 1, _total_estimate_conf, NULL;

	IF	(
			SELECT
				array_agg(t_variable.id ORDER BY t_variable.id)
			FROM @extschema@.t_aux_total
			INNER JOIN @extschema@.t_variable 		ON (t_aux_total.variable = t_variable.id)
			INNER JOIN @extschema@.c_estimation_cell 	ON (t_aux_total.estimation_cell = c_estimation_cell.id)
			INNER JOIN @extschema@.t_model_variables 	ON t_model_variables.variable = t_variable.id
			INNER JOIN @extschema@.t_model 			ON t_model.id = t_model_variables.model
			INNER JOIN @extschema@.t_aux_conf 		ON t_aux_conf.model = t_model_variables.model
			WHERE 	c_estimation_cell.id = $1 AND
				t_aux_conf.id = $5 AND
				t_aux_total.is_latest

		)
		!= (
			SELECT
				array_agg(t_model_variables.variable order by variable)
			FROM @extschema@.t_aux_conf
			INNER JOIN @extschema@.t_model ON t_model.id = t_aux_conf.model
			INNER JOIN @extschema@.t_model_variables ON t_model_variables.model = t_model.id
			WHERE t_aux_conf.id = $5
		)
	THEN
		RAISE EXCEPTION 'fn_2p_est_configuration: t_aux_total not found! (total_estimate_conf: %)', _total_estimate_conf;
	END IF;
ELSE
	RAISE NOTICE 'Required configuration already exists!';
END IF;


RETURN _total_estimate_conf;


END;
$function$
LANGUAGE plpgsql
PARALLEL SAFE;

--COMMENT ON FUNCTION @extschema@.fn_2p_est_configuration() IS '.';
-- </function>

