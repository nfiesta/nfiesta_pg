-- Copyright 2017, 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.

-- update labels and descriptions of c_phase_estimate_type

UPDATE @extschema@.c_phase_estimate_type
SET label = 'single-phase'
WHERE id =  1;

UPDATE @extschema@.c_phase_estimate_type
SET description = 'A family of single-phase estimators using true (field) data only.'
WHERE id =  1;

UPDATE @extschema@.c_phase_estimate_type
SET label = 'GREG-map'
WHERE id =  2;

UPDATE @extschema@.c_phase_estimate_type
SET description = 'A family of single-phase GREGs (Generalised Regression Estimators) estimators using true (field) data and auxiliary data extracted from wall-to-wall maps.'
WHERE id =  2;

UPDATE @extschema@.c_phase_estimate_type
SET label = 'two-phase GREG'
WHERE id =  3;

UPDATE @extschema@.c_phase_estimate_type
SET description = 'A family of two-phase GREGs (generalised regression estimators) using true (field) data and larger samples of auxiliary data.'
WHERE id =  3;

-- <function name="fn_api_save_panel_refyearset_group" schema="extschema" src="functions/extschema/configuration/fn_api_save_panel_refyearset_group.sql">
-- Copyright 2017, 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.

-- Function: @extschema@.fn_api_save_panel_refyearset_group(INT[], INT[], VARCHAR(200), TEXT, VARCHAR(200), TEXT)
-- DROP FUNCTION @extschema@.fn_api_save_panel_refyearset_group(INT[], INT[], VARCHAR(200), TEXT, VARCHAR(200), TEXT);

CREATE OR REPLACE FUNCTION @extschema@.fn_api_save_panel_refyearset_group(_panels INT[], _refyearsets INT[], _label VARCHAR(200), _description TEXT, 
	_label_en VARCHAR(200), _description_en TEXT)
RETURNS INT
AS
$function$
BEGIN
-- testing input parameters if not NULL
IF _panels IS NULL THEN
	RAISE EXCEPTION 'fn_api_save_panel_refyearset_group: Function argument _panels INT[] must not be NULL!';
END IF;

IF _refyearsets IS NULL THEN
	RAISE EXCEPTION 'fn_api_save_panel_refyearset_group: Function argument _refyearsets INT[] must not be NULL!';
END IF;

IF _label IS NULL THEN
	RAISE EXCEPTION 'fn_api_save_panel_refyearset_group: Function argument _label VARCHAR(200) must not be NULL!';
END IF;

IF _description IS NULL THEN
	RAISE EXCEPTION 'fn_api_save_panel_refyearset_group: Function argument _description TEXT must not be NULL!';
END IF;


IF _label_en IS NULL THEN
	RAISE EXCEPTION 'fn_api_save_panel_refyearset_group: Function argument _label_en VARCHAR(200) must not be NULL!';
END IF;

IF _description_en IS NULL THEN
	RAISE EXCEPTION 'fn_api_save_panel_refyearset_group: Function argument _description_en TEXT must not be NULL!';
END IF;

-- checking input arrays if they do not contain NULL
IF (SELECT array_position(_panels, NULL) IS NOT NULL) THEN
	RAISE EXCEPTION 'fn_api_save_panel_refyearset_group:  Function argument _panels INT[] must not be an array containing NULL!';
END IF;

IF (SELECT array_position(_refyearsets, NULL) IS NOT NULL) THEN
	RAISE EXCEPTION 'fn_api_save_panel_refyearset_group:  Function argument _refyearsets INT[] must not be an array containing NULL!';
END IF;


RETURN @extschema@.fn_save_panel_refyearset_group(_panels, _refyearsets, _label, _description, _label_en, _description_en); 

END;
$function$
LANGUAGE plpgsql
PARALLEL SAFE;

COMMENT ON FUNCTION  @extschema@.fn_api_save_panel_refyearset_group(INT[], INT[], VARCHAR(200), TEXT, VARCHAR(200), TEXT) IS 
'This is a wrapper API function. Unlike the internally called fn_save_panel_refyearset_group '
'this API has no defaults. More specifically, labels and descriptions must always be provided.';

/* tests
-------------------------------------------------------------------------------------------------------------------------------------------------------
-- tests for function @extschema@.fn_api_save_panel_refyearset_group(_panels INT[], _refyearsets INT[], _label VARCHAR(200), _description TEXT, 
--	_label_en VARCHAR(200), _description_en TEXT
-------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------- testing invalid inputs ------------------------------------------------------------------------------
-- test NULL for _panels argument
SELECT * FROM @extschema@.fn_api_save_panel_refyearset_group(NULL, ARRAY[2,3,3], 'nejaky label', 'nejaky description', 'some label en', 'some description_en');

-- test NULL for _refyearsets argument
SELECT * FROM @extschema@.fn_api_save_panel_refyearset_group(ARRAY[1,5,7], NULL, 'nejaky label', 'nejaky description', 'some label en', 'some description_en');

-- passing NULL for _label
SELECT * FROM @extschema@.fn_api_save_panel_refyearset_group(ARRAY[1,5,7], ARRAY[2,3,3], NULL, 'nejaky description', 'some label en', 'some description_en')

-- passing NULL for _description
SELECT * FROM @extschema@.fn_api_save_panel_refyearset_group(ARRAY[1,5,7], ARRAY[2,3,3], 'nejaky label', NULL, 'some label en', 'some description_en');

-- passing NULL for _label_en
SELECT * FROM @extschema@.fn_api_save_panel_refyearset_group(ARRAY[1,5,7], ARRAY[2,3,3], 'nejaky label', 'nejaky description', NULL, 'some description_en');

-- passing NULL for _description
SELECT * FROM @extschema@.fn_api_save_panel_refyearset_group(ARRAY[1,5,7], ARRAY[2,3,3], 'nejaky label', 'nejaky description', 'some label en', NULL);

-- test for NULL as an element of _panels
SELECT * FROM @extschema@.fn_api_save_panel_refyearset_group(ARRAY[1,NULL,7], ARRAY[2,3,3], 'nejaky label', 'nejaky description', 'some label en', 'some description_en');
SELECT * FROM @extschema@.fn_api_save_panel_refyearset_group(ARRAY[NULL], ARRAY[2,3,3], 'nejaky label', 'nejaky description', 'some label en', 'some description_en');
SELECT * FROM @extschema@.fn_api_save_panel_refyearset_group(ARRAY[NULL]::int[], ARRAY[2,3,3], 'nejaky label', 'nejaky description', 'some label en', 'some description_en');

-- test for NULL as an element of _refyearsets
SELECT * FROM @extschema@.fn_api_save_panel_refyearset_group(ARRAY[1,5,7], ARRAY[1,NULL,7], 'nejaky label', 'nejaky description', 'some label en', 'some description_en');
SELECT * FROM @extschema@.fn_api_save_panel_refyearset_group(ARRAY[1,5,7], ARRAY[NULL], 'nejaky label', 'nejaky description', 'some label en', 'some description_en');
SELECT * FROM @extschema@.fn_api_save_panel_refyearset_group(ARRAY[1,5,7], ARRAY[NULL]::int[], 'nejaky label', 'nejaky description', 'some label en', 'some description_en');

-- test if saving a group with non-existing panel fails
SELECT * FROM @extschema@.fn_api_save_panel_refyearset_group(ARRAY[-1,5,7], ARRAY[2,3,3], 'nejaky label', 'nejaky description', 'some label en', 'some description_en');

-- test if saving a group with non-existing reference yearset fails
SELECT * FROM @extschema@.fn_api_save_panel_refyearset_group(ARRAY[1,5,7], ARRAY[-1,3,3], 'nejaky label', 'nejaky description', 'some label en', 'some description_en');

-- test if saving a group with a list of panels and refyearsets equal to an existing group fails
SELECT * FROM @extschema@.fn_api_save_panel_refyearset_group(ARRAY[1,5,7], ARRAY[2,3,3], 'nejaky label', 'nejaky description', 'some label en', 'some description_en');

------------------------------------------------ testing valid inputs ---------------------------------------------------------------------------------
SELECT * FROM @extschema@.fn_api_save_panel_refyearset_group(ARRAY[1,7], ARRAY[2,3], 'nejaky label', 'nejaky description', 'some label en', 'some description_en');

-- helping code
SELECT 
	t1.id, t1.label, 
	array_agg(t2.panel), 
	array_agg(t2.reference_year_set) 
FROM 
	@extschema@.c_panel_refyearset_group AS t1 
INNER JOIN 
	@extschema@.t_panel_refyearset_group AS t2 
	ON t1.id = t2.panel_refyearset_group 
GROUP BY t1.id, t1.label;
*/
-- </function>

-- update of string for phase_estimate_type / temporal solution
-- <function name="fn_2p_est_configuration" schema="extschema" src="functions/extschema/configuration/fn_2p_est_configuration.sql">
-- Copyright 2017, 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_2p_est_configuration(integer, regclass)

--DROP FUNCTION @extschema@.fn_2p_est_configuration(integer,integer,character varying,integer,integer,boolean);

CREATE OR REPLACE FUNCTION @extschema@.fn_2p_est_configuration(_estimation_cell integer, _estimation_period integer, _note varchar, _target_variable integer, _aux_conf integer, _force_synthetic boolean default False)
RETURNS integer
AS
$function$
DECLARE
_total_estimate_conf		integer;
_estimate_date_begin		date;
_estimate_date_end		date;
_stratas			integer[];
_stratas_wp			integer[];
_panels				integer[];
_refyearsets			integer[];
_panels_aux			integer[];
_param_area			integer;
_param_area_code		varchar;
_target_label			varchar;
_model				integer;
_cell				varchar;
_panel_refyearset_group		integer;
BEGIN

-- test for existing g_betas
-- otherwise the configuration cannot be done (sometimes the g_betas cannot be computed)
-- so this prevents to configure non-computable estimates

IF (SELECT count(*) FROM @extschema@.t_g_beta WHERE aux_conf = $5) = 0
THEN
	RAISE EXCEPTION 'G-betas for required aux_conf (%) are not available (cell=%, period=%). The computation of it was not run or is not able to compute (mostly the problem of matrix inversion).', _aux_conf, _estimation_cell, _estimation_period;
END IF;

SELECT estimate_date_begin, estimate_date_end
FROM @extschema@.c_estimation_period
WHERE id = _estimation_period
INTO _estimate_date_begin, _estimate_date_end;

IF _estimate_date_begin IS NULL OR _estimate_date_end IS NULL
THEN
	RAISE EXCEPTION 'At leats one of the estimate period dates (%, %) is NULL!', _estimate_date_begin, _estimate_date_end;
END IF;


-- create the label of estimate
_target_label := (
		SELECT		replace(
					replace(
						concat(coalesce(((t2.metadata->'en')->'indicator')->>'label'::varchar,'x'), ',', coalesce(t3.label,'x'), ',', coalesce(t4.label,'x'), ',', coalesce(t5.label,'x')),
					'x,',''),
			       	',x','') AS label
		FROM		@extschema@.t_variable AS t1
		LEFT JOIN 	@extschema@.c_target_variable AS t2 ON t1.target_variable = t2.id
		LEFT JOIN	@extschema@.c_sub_population_category AS t3 ON t1.sub_population_category = t3.id
		LEFT JOIN	@extschema@.c_area_domain_category AS t4 ON t1.area_domain_category = t4.id
		LEFT JOIN	@extschema@.c_auxiliary_variable_category AS t5 ON t1.auxiliary_variable_category = t5.id
		WHERE
			t1.id = _target_variable
		);

_param_area := (SELECT param_area FROM @extschema@.t_aux_conf WHERE id = $5);
_model := (SELECT model FROM @extschema@.t_aux_conf WHERE id = $5);
_param_area_code := (SELECT param_area_code FROM @extschema@.f_a_param_area WHERE gid = _param_area);
_cell := (SELECT label FROM @extschema@.c_estimation_cell WHERE id = $1);

-- reset sequence (usually when previous atempt to configure fails, the sequence remains shifted)
PERFORM setval('@extschema@.t_total_estimate_conf_id_seq', (SELECT coalesce(max(id),0)+1 FROM @extschema@.t_total_estimate_conf), FALSE);

	-- test on param_area_coverage
		SELECT
			array_agg(t1.id ORDER BY t1.id)
		FROM
			sdesign.t_stratum AS t1
		INNER JOIN
			@extschema@.f_a_param_area AS t2
		ON
			-- buffered stratum?
			-- no, if only buffer of the stratum would intersect the cell, 
			-- no consequences on estimate would be introduced, the buffer is in general used only for the edge effect compensation (the compensated area is within non-buffered stratum)
			ST_Intersects(t1.geom, t2.geom) AND NOT ST_Touches(t1.geom, t2.geom)
		WHERE
			t2.gid = _param_area
		INTO _stratas;

		IF _stratas IS NULL
		THEN
			RAISE EXCEPTION 'The specified cell is not intersected by any stratum. Choose another estimation cell.';
		END IF;

	-- existing panels configured in panel2aux_conf
		SELECT
			array_agg(t1.panel ORDER BY t1.panel)
		FROM
			@extschema@.t_panel_refyearset_group AS t1
		INNER JOIN
			@extschema@.t_aux_conf AS t2
		ON	t1.panel_refyearset_group = t2.panel_refyearset_group
		WHERE
			t2.id = _aux_conf
		INTO _panels_aux;

	-- check of panel2total_2ndph
	-- and addition of panels from param_area - is the target variable available not only in cell?

		WITH w_data AS MATERIALIZED (
			SELECT
				t1.id AS stratum, t2.id AS panel, t9.id AS reference_year_set, t2.plot_count AS total
			FROM
				sdesign.t_stratum AS t1
			INNER JOIN
				sdesign.t_panel AS t2
			ON
				t1.id = t2.stratum
			INNER JOIN
				sdesign.cm_refyearset2panel_mapping AS t8
			ON
				t2.id = t8.panel --AND
				--t8.id = t9.reference_year_set
			INNER JOIN
				sdesign.t_reference_year_set AS t9
			ON
				t8.reference_year_set = t9.id
			INNER JOIN
				@extschema@.t_available_datasets AS t6
			ON
				t2.id = t6.panel AND
				t9.id = t6.reference_year_set
			INNER JOIN
				@extschema@.t_variable AS t7
			ON
				t6.variable = t7.id
			WHERE
				array[t1.id] <@ _stratas AND
				t7.id = _target_variable AND 
				(t9.reference_date_begin >= _estimate_date_begin AND
				t9.reference_date_end <= _estimate_date_end)
			GROUP BY
				t1.id, t2.id, t9.id
		)
		SELECT
			array_agg(DISTINCT stratum ORDER BY stratum) AS stratas,
			array_agg(panel ORDER BY panel) AS panels,
			array_agg(reference_year_set ORDER BY panel) AS refyearsets
		FROM
			(SELECT
				stratum, panel, reference_year_set,
				total,
				max(total) OVER(PARTITION BY stratum, panel, reference_year_set) AS max_total
			FROM
				w_data
			) AS t1
		WHERE
			-- pick up the most dense panel with target variable
			total = max_total
		INTO _stratas_wp, _panels, _refyearsets;

		IF _panels != _panels_aux OR _panels IS NULL
		THEN
			RAISE EXCEPTION 'Not all panels coming from g_beta have available target variable! aux_conf: %, panels: %, panels_aux: %',
			_aux_conf, _panels, _panels_aux;
		END IF;

		_panel_refyearset_group := (SELECT @extschema@.fn_get_panel_refyearset_group(_panels, _refyearsets));

		IF _panel_refyearset_group IS NULL
		THEN
			_panel_refyearset_group := (SELECT @extschema@.fn_save_panel_refyearset_group(_panels, _refyearsets));
		END IF;

-- insert into table t_total_estimate_conf
INSERT INTO @extschema@.t_total_estimate_conf (estimation_cell, estimation_period, total_estimate_conf, variable, phase_estimate_type, force_synthetic, aux_conf, panel_refyearset_group)
VALUES
	($1, $2, concat('GREG-map;T=',_target_label,';C=',_cell,';PA=',_param_area_code, ';m=',_model,_note), $4, 2, $6, $5, _panel_refyearset_group)
ON CONFLICT (estimation_cell, estimation_period, variable, phase_estimate_type, coalesce(force_synthetic,false), coalesce(aux_conf,0), panel_refyearset_group)
DO NOTHING
RETURNING id
INTO _total_estimate_conf;

IF _total_estimate_conf IS NOT NULL
THEN
	-- insert into table t_estimate_conf
	INSERT INTO @extschema@.t_estimate_conf (estimate_type, total_estimate_conf, denominator)
	SELECT 1, _total_estimate_conf, NULL;

	IF	(
			SELECT
				array_agg(t_variable.id ORDER BY t_variable.id)
			FROM @extschema@.t_aux_total
			INNER JOIN @extschema@.t_variable 		ON (t_aux_total.variable = t_variable.id)
			INNER JOIN @extschema@.c_estimation_cell 	ON (t_aux_total.estimation_cell = c_estimation_cell.id)
			INNER JOIN @extschema@.t_model_variables 	ON t_model_variables.variable = t_variable.id
			INNER JOIN @extschema@.t_model 			ON t_model.id = t_model_variables.model
			INNER JOIN @extschema@.t_aux_conf 		ON t_aux_conf.model = t_model_variables.model
			WHERE 	c_estimation_cell.id = $1 AND
				t_aux_conf.id = $5 AND
				t_aux_total.is_latest

		)
		!= (
			SELECT
				array_agg(t_model_variables.variable order by variable)
			FROM @extschema@.t_aux_conf
			INNER JOIN @extschema@.t_model ON t_model.id = t_aux_conf.model
			INNER JOIN @extschema@.t_model_variables ON t_model_variables.model = t_model.id
			WHERE t_aux_conf.id = $5
		)
	THEN
		RAISE EXCEPTION 'fn_2p_est_configuration: t_aux_total not found! (total_estimate_conf: %)', _total_estimate_conf;
	END IF;
ELSE
	RAISE NOTICE 'Required configuration already exists!';
END IF;


RETURN _total_estimate_conf;


END;
$function$
LANGUAGE plpgsql
PARALLEL SAFE;

--COMMENT ON FUNCTION @extschema@.fn_2p_est_configuration() IS '.';

-- </function>

-- <function name="fn_api_save_greg_map_total_config" schema="extschema" src="functions/extschema/configuration/fn_api_save_greg_map_total_config.sql">
-- Copyright 2017, 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.

-- Function: @extschema@.fn_api_save_greg_map_total_config(INT, INT, INT, INT, INT, BOOLEAN, INT, BOOLEAN)
-- DROP FUNCTION @extschema@.fn_api_save_greg_map_total_config(INT, INT, INT, INT, INT, BOOLEAN, INT, BOOLEAN);

CREATE OR REPLACE FUNCTION @extschema@.fn_api_save_greg_map_total_config(_estimation_period INT, _estimation_cell INT, _variable INT,  
	_panel_refyearset_group INT, _working_model INT, _sigma BOOLEAN, _param_area_type INT, _force_synthetic BOOLEAN)
RETURNS INT
AS
$function$
DECLARE 
_panels INT[];
_null_refyearsets INT[]; 
_panel_group_aux INT[];
_param_area INT[];
_aux_conf INT[];

BEGIN
-- testing input parameters if not NULL
IF _estimation_period IS NULL THEN
	RAISE EXCEPTION 'fn_api_save_greg_map_total_config: Function argument _estimation_period INT must not be NULL!';
END IF;	
	
IF _estimation_cell IS NULL THEN
	RAISE EXCEPTION 'fn_api_save_greg_map_total_config: Function argument _estimation_cell INT must not be NULL!';
END IF;

IF _variable IS NULL THEN
	RAISE EXCEPTION 'fn_api_save_greg_map_total_config: Function argument _variable INT must not be NULL!';
END IF;

IF _panel_refyearset_group IS NULL THEN
	RAISE EXCEPTION 'fn_api_save_greg_map_total_config: Function argument _panel_refyearset_group INT must not be NULL!';
END IF;

IF _working_model IS NULL THEN
	RAISE EXCEPTION 'fn_api_save_greg_map_total_config: Function argument _working_model INT must not be NULL!';
END IF;

IF _sigma IS NULL THEN
	RAISE EXCEPTION 'fn_api_save_greg_map_total_config: Function argument _sigma BOOLEAN must not be NULL!';
END IF;

IF _param_area_type IS NULL THEN
	RAISE EXCEPTION 'fn_api_save_greg_map_total_config: Function argument _param_area_type INT must not be NULL!';
END IF;

IF _force_synthetic IS NULL THEN
	RAISE EXCEPTION 'fn_api_save_greg_map_total_config: Function argument _force_synthetic BOOLEAN must not be NULL!';
END IF;

-- find out panels of the input _panel_refyearset_group
SELECT array_agg(panel) FROM @extschema@.fn_api_get_panel_refyearset_combinations4groups(ARRAY[_panel_refyearset_group]) INTO _panels; -- panels 1,2

IF _panels IS NULL THEN
	RAISE EXCEPTION 'fn_api_save_greg_map_total_config: No panels found for the chosen group of panels and '
	'reference-year set combinations (function argument _panel_refyearset_group). The configuration process cannot proceed!';
END IF;


-- construct array of NULLs with the length corresponfing to _panels
SELECT 
	array_agg(NULL::int)::int[]
FROM 
	generate_series(1, array_length(_panels,1)) AS a(i)
INTO _null_refyearsets;

-- determine the panel group having the same set of panel but no reference-year sets, this should be found in the matching records of t_aux_conf 
SELECT array_agg(id) FROM @extschema@.fn_api_get_group4panel_refyearset_combinations(_panels, _null_refyearsets) INTO _panel_group_aux; -- panel group 6

IF _panel_group_aux IS NULL THEN
	RAISE EXCEPTION 'fn_api_save_greg_map_total_config: No auxiliary group of panels matching the chosen group of panels and '
	'reference-year set combinations (function argument _panel_refyearset_group) found! The configuration process cannot proceed!';
END IF;

IF array_length(_panel_group_aux,1) > 1 THEN
	RAISE EXCEPTION 'fn_api_save_greg_map_total_config: More than one auxiliary group of panels matching the chosen group of panels and '
	'reference-year set combinations (function argument _panel_refyearset_group) found! The configuration process cannot proceed!';
END IF;

-- determine parameterisation areas beonging to the input _param_area_type and containing the cell  
SELECT 
	array_agg(gid)
FROM 
	@extschema@.f_a_param_area AS t1
INNER JOIN 
	@extschema@.cm_cell2param_area_mapping AS t2
	ON t1.gid = t2.param_area
WHERE 
	t1.param_area_type = _param_area_type AND 
	t2.estimation_cell = _estimation_cell
INTO _param_area;

IF _param_area IS NULL THEN
	RAISE EXCEPTION 'fn_api_save_greg_map_total_config: No parameteristion area matching the combination of estimation cell (_estimation_cell) '
	'and paramatrisation area type (_param_area_type) found! The configuration process cannot proceed!';
END IF;

IF array_length(_param_area,1) > 1 THEN
	RAISE EXCEPTION 'fn_api_save_greg_map_total_config: More than one parameteristion area matching the combination of estimation cell '
	'(_estimation_cell) and paramatrisation area type (_param_area_type) found! The configuration process cannot proceed!';
END IF;

SELECT 
	array_agg(id) 
FROM 
	@extschema@.t_aux_conf AS t1
WHERE 
	sigma = _sigma AND	
	model = _working_model AND 
	panel_refyearset_group = _panel_group_aux[1] AND
	param_area = _param_area[1]
INTO _aux_conf;

IF _aux_conf IS NULL THEN
	RAISE EXCEPTION 'fn_api_save_greg_map_total_config: No matching auxiliary configuration found! The configuration process cannot proceed!';
END IF;

IF array_length(_aux_conf,1) > 1 THEN
	RAISE EXCEPTION 'fn_api_save_greg_map_total_config: More than one matching auxiliary configuration found! The configuration process cannot proceed!';
END IF;

RETURN @extschema@.fn_2p_est_configuration(_estimation_cell, _estimation_period, 'API-GUI configuration', _variable, _aux_conf[1], _force_synthetic);

END;
$function$
LANGUAGE plpgsql
PARALLEL SAFE;

COMMENT ON FUNCTION  @extschema@.fn_api_save_greg_map_total_config(INT, INT, INT, INT, INT, BOOLEAN, INT, BOOLEAN) IS 
'This is a wrapper API function saving the configuration of GREG estimates of total that use auxiliary data in the form of maps.';

/* tests
-------------------------------------------------------------------------------------------------------------------------------------------------------
-- tests for function @extschema@.fn_api_save_greg_map_total_config(_estimation_period INT, _estimation_cell INT, _variable INT,  
--	_panel_refyearset_group INT, _working_model INT, _sigma BOOLEAN, _param_area_type INT, _force_synthetic BOOLEAN)
-------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------- testing invalid inputs ------------------------------------------------------------------------------
-- test NULL for _estimation_period argument
SELECT * FROM @extschema@.fn_api_save_greg_map_total_config(NULL,55,1,19,1,FALSE,3,FALSE);

-- test NULL for _estimation_cell argument
SELECT * FROM @extschema@.fn_api_save_greg_map_total_config(1,NULL,1,19,1,FALSE,3,FALSE);

-- test NULL for _variable argument
SELECT * FROM @extschema@.fn_api_save_greg_map_total_config(1,55,NULL,19,1,FALSE,3,FALSE);

-- test NULL for _panel_refyearset_group argument
SELECT * FROM @extschema@.fn_api_save_greg_map_total_config(1,55,1,NULL,1,FALSE,3,FALSE);

-- test NULL for _working_model argument
SELECT * FROM @extschema@.fn_api_save_greg_map_total_config(1,55,1,19,NULL,FALSE,3,FALSE);

-- test NULL for _sigma argument
SELECT * FROM @extschema@.fn_api_save_greg_map_total_config(1,55,1,19,1,NULL,3,FALSE);

-- test NULL for _param_area_type argument
SELECT * FROM @extschema@.fn_api_save_greg_map_total_config(1,55,1,19,1,FALSE,NULL,FALSE);

-- test NULL for _force_synthetic argument
SELECT * FROM @extschema@.fn_api_save_greg_map_total_config(1,55,1,19,1,FALSE,3,NULL);

-- test for an invalid group of panel and reference-year set combinations - no panels  found
SELECT * FROM @extschema@.fn_api_save_greg_map_total_config(1,55,1,-1,1,FALSE,3,FALSE);

-- test for a group of panel and reference-year set combinations that has no equivaent in t_aux_conf (with no reference year sets attached)
SELECT * FROM @extschema@.fn_api_save_greg_map_total_config(1,55,1,33,1,FALSE,3,FALSE);

-- test for a parametrisation area not matching the estimation cell
SELECT * FROM @extschema@.fn_api_save_greg_map_total_config(1,55,1,19,1,FALSE,2,FALSE);

-- test for missing aux configuration - no one has sigma TRUE
SELECT * FROM @extschema@.fn_api_save_greg_map_total_config(1,55,1,19,1,TRUE,3,FALSE);

------------------------------------------------ testing valid inputs ---------------------------------------------------------------------------------
-- save a configuration with force_synthetic = FALSE  
SELECT * FROM @extschema@.fn_api_save_greg_map_total_config(1,55,1,19,1,FALSE,3,FALSE);

-- save a configuration with force_synthetic = TRUE  
SELECT * FROM @extschema@.fn_api_save_greg_map_total_config(1,55,1,19,1,FALSE,3,TRUE);


-- helping code
-- aux_conf corresponding to group 19
SELECT * FROM @extschema@.fn_api_get_wmodels_sigma_paramtypes(ARRAY[1,2], ARRAY[55,56,60,61,67,68,69,70,75,76,77,83]); -- model 1, sigma FALSE, param area type 3

_estimation_cell = 55,
_panel_refyearset_group = 19
_working_model = 1
_sigma = FALSE
_param_area_type = 3
_param_area = 28
_estimation_period = 1,
_variable = 1
aux_panel_refyearset_group = 6

SELECT * FROM 
 (SELECT panel_refyearset_group, array_agg(panel ORDER BY panel) AS p, array_agg(reference_year_set) FROM @extschema@.t_panel_refyearset_group WHERE reference_year_set IS NOT NULL GROUP BY panel_refyearset_group) AS t1  
LEFT JOIN 
 (SELECT panel_refyearset_group, array_agg(panel ORDER BY panel) AS p, array_agg(reference_year_set) FROM @extschema@.t_panel_refyearset_group WHERE reference_year_set IS NULL GROUP BY panel_refyearset_group) AS t2
ON t1.p = t2.p

*/
-- </function>