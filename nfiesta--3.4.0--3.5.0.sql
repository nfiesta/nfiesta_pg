--
-- Copyright 2017, 2023 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--



DROP FUNCTION IF EXISTS @extschema@.fn_etl_check_area_domain_categories4etl(integer, json) CASCADE;
-- <function name="fn_etl_check_area_domain_categories4etl" schema="extschema" src="functions/extschema/etl/fn_etl_check_area_domain_categories4etl.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_check_area_domain_categories4etl(integer, json)

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_check_area_domain_categories4etl(integer, json) CASCADE;

create or replace function @extschema@.fn_etl_check_area_domain_categories4etl
(
	_area_domain			integer, -- id from target c_area_domain
	_area_domain_category	json
)
returns table
(
	id						integer,
	area_domain_category	integer[],
	etl_id					integer,
	label					character varying,
	description				text,
	label_en				character varying,
	description_en			text,
	check_label				boolean
)
as
$$
declare
		_label_en_type_source				varchar[];
		_check_label_en_type				integer;
		_check_count_categories				integer;
		_check_count_elements				integer;
		_check_count_categories_distinct	integer;
		_check_count_label_en_distinct		integer;
		_raise_notice_text					text;
		_check_info							integer;
		_check_count_pairing_label_en		integer;
begin	
		if _area_domain is null
		then
			raise exception 'Error 01: fn_etl_check_area_domain_categories4etl: Input argument _area_domain must not be NULL!';
		end if;
	
		if _area_domain_category is null
		then
			raise exception 'Error 02: fn_etl_check_area_domain_categories4etl: Input argument _area_domain_category must not be NULL!';
		end if;
		---------------------------------------------------
		with
		w1 as	(
				select _area_domain_category as res
				)
		,w2 as	(
				select json_array_elements(w1.res) as res from w1
				)
		,w3 as	(
				select (res->>'label_en_type')::varchar as label_en_type_source	from w2
				)
		,w4 as	(
				select replace(lower(w3.label_en_type_source),' ','') as label_en_type_source from w3
				)
		select array_agg(t.label_en_type_source) from (select distinct w4.label_en_type_source from w4) as t
		into _label_en_type_source;

		if array_length(_label_en_type_source,1) is distinct from 1
		then
			raise exception 'Error 03: fn_etl_check_area_domain_categories4etl: English labels of area domain in input argument _area_domain_category are not equal!';
		end if;
		--------------------------------------------------
		-- check if input area domain is equal with target
		with
		w1 as	(
				select
						cad.id,
						string_to_array(replace(lower(cad.label_en),' ',''),';') as label_en_type_target4compare
				from
						@extschema@.c_area_domain as cad
				where
						cad.id = _area_domain
				)
		select count(w1.*) from w1 where @extschema@.fn_etl_array_compare(string_to_array(lower(_label_en_type_source[1]),';'),w1.label_en_type_target4compare) = true
		into _check_label_en_type;
		-- there will be value 0 or 1
		-- 0 => source area domain label_en is different from target area domain label_en
		-----------------------------------------------------------------------
		-----------------------------------------------------------------------
		-- source area domain label_en is equal with target area domain label_en => next step are checks of categories
		---------------------------------------------------
		-- count of categories for target area domain
		select count(cadc.*) from @extschema@.c_area_domain_category as cadc
		where cadc.area_domain = _area_domain
		into _check_count_categories;
		---------------------------------------------------
		-- count of categories for source area domain
		with
		w1 as	(
				select _area_domain_category as res
				)
		,w2 as	(
				select json_array_elements(w1.res) from w1
				)
		select count(w2.*) from w2
		into _check_count_elements;
		---------------------------------------------------
		if _check_count_categories is distinct from _check_count_elements
		then
			--raise exception 'Error 03: fn_etl_check_area_domain_categories4etl: Count elements in input argument _area_domain_category is not equal count of categories of input area domain [ID = %] in target c_area_domain_category table!',_area_domain;
			if _check_label_en_type = 1
			then
				--_raise_notice_text := 'Selected area domain to pair is equal with source but count of categories is not equal!';
				_raise_notice_text := 'Different count of categories, the selected area domain cannot be paired!';
			else
				--_raise_notice_text := 'Selected area domain to pair is not equal with source and count of categories is not equal!';
				_raise_notice_text := 'Different count of categories, the selected area domain cannot be paired!';
			end if;

			_check_info := 1;
		else
			_check_info := 0;
		end if;
		---------------------------------------------------
		select count(t.*) from
		(select distinct cadc.label_en from @extschema@.c_area_domain_category as cadc
		where cadc.area_domain = _area_domain) as t
		into _check_count_categories_distinct;
		---------------------------------------------------
		if _check_count_categories is distinct from _check_count_categories_distinct
		then
			--raise exception 'Error 04: fn_etl_check_area_domain_categories4etl: English label of categories of input area domain [ID = %] in target c_area_domain_category table is not unique!',_area_domain;
			if _check_label_en_type = 1
			then			
				--_raise_notice_text := 'Selected area domain to pair is equal with source but english lables of target are not unique!';
				_raise_notice_text := 'Duplicate categories in the target database, the selected area domain cannot be paired!';
			else
				--_raise_notice_text := 'Selected area domain to pair is not equal with source and english lables of target are not unique!';
				_raise_notice_text := 'Duplicate categories in the target database, the selected area domain cannot be paired!';
			end if;

			_check_info := _check_info + 1;
		else
			_check_info := _check_info + 0;
		end if;
		---------------------------------------------------
		with
		w1 as	(
				select _area_domain_category as res
				)
		,w2 as	(
				select json_array_elements(w1.res) as res from w1
				)
		,w3 as	(
				select (res->>'label_en')::varchar as label_en from	w2
				)
		select count(t.*) from
		(select distinct w3.label_en from w3) as t
		into _check_count_label_en_distinct;
		---------------------------------------------------
		if _check_count_elements is distinct from _check_count_label_en_distinct
		then
			--raise exception 'Error 05: fn_etl_check_area_domain_categories4etl: English label of categories of input argument _area_domain_category = % is not unique!',_area_domain_category;
			if _check_label_en_type = 1
			then
				--_raise_notice_text := 'Selected area domain to pair is equal with source but english lables of source are not unique!';
				_raise_notice_text := 'Duplicate categories in the source database, the selected area domain cannot be paired!';
			else
				--_raise_notice_text := 'Selected area domain to pair is not equal with source and english lables of source are not unique!';
				_raise_notice_text := 'Duplicate categories in the source database, the selected area domain cannot be paired!';
			end if;

			_check_info := _check_info + 1;
		else
			_check_info := _check_info + 0;
		end if;	
		--------------------------------------------------
		-----------------------------------------------------------------------
		-----------------------------------------------------------------------
		if _check_info = 0
		then
			-- OK
			-- check if pairing over international language will have already some record
			with
			w1a as	(
					select _area_domain_category as res
					)
			,w2a as	(
					select json_array_elements(w1a.res) as res from w1a
					)
			,w3a as	(
					select
							(res->>'id')::integer as id_ordering_source,
							replace(replace((res->>'area_domain_category')::varchar,'[',''),']','') as area_domain_category_source,
							(res->>'label_en')::varchar as label_en_source
					from
							w2a
					)
			,w4a as	(
					select
							w3a.id_ordering_source,
							(string_to_array(w3a.area_domain_category_source,','))::integer[] as area_domain_category_source,
							w3a.label_en_source,
							string_to_array(replace(lower(w3a.label_en_source),' ',''),';') as label_en_source4compare
					from
							w3a
					)
			---------------------
			,w1b as	(
					select
							cadc.id as etl_id,
							cadc.area_domain,
							cadc.label as label_target,
							cadc.description as description_target,
							cadc.label_en as label_en_target,
							cadc.description_en as description_en_target,
							string_to_array(replace(lower(cadc.label_en),' ',''),';') as label_en_target4compare
					from
							@extschema@.c_area_domain_category as cadc
					where
							cadc.area_domain = _area_domain
					)
			---------------------
			,w1 as 	(
					select w1b.*, w4a.* from w1b cross join w4a
					)
			,w2 as	(
					select w1.*, true as check_label_en from w1
					where @extschema@.fn_etl_array_compare(w1.label_en_source4compare,w1.label_en_target4compare)
					)
			,w3 as	(
					select
							w2.id_ordering_source,
							w2.area_domain_category_source,
							w2.etl_id,
							w2.label_target as label,
							w2.description_target as description,
							w2.label_en_target as label_en,
							w2.description_en_target as description_en,
							w2.check_label_en
					from
							w2
					)
			,w4 as	(
					select
							w4a.id_ordering_source as id,
							w4a.area_domain_category_source as area_domain_category,
							w3.etl_id,
							w3.label,
							w3.description,
							w3.label_en,
							w3.description_en,
							w3.check_label_en
					from
							w4a left join w3
					on
							w4a.id_ordering_source = w3.id_ordering_source
					and		w4a.area_domain_category_source = w3.area_domain_category_source
					)
			select count(w4.*) from w4 where w4.check_label_en = true
			into _check_count_pairing_label_en;

			if _check_count_pairing_label_en > 0
			then
				return query
				with
				w1a as	(
						select _area_domain_category as res
						)
				,w2a as	(
						select json_array_elements(w1a.res) as res from w1a
						)
				,w3a as	(
						select
								(res->>'id')::integer as id_ordering_source,
								replace(replace((res->>'area_domain_category')::varchar,'[',''),']','') as area_domain_category_source,
								(res->>'label_en')::varchar as label_en_source
						from
								w2a
						)
				,w4a as	(
						select
								w3a.id_ordering_source,
								(string_to_array(w3a.area_domain_category_source,','))::integer[] as area_domain_category_source,
								w3a.label_en_source,
								string_to_array(replace(lower(w3a.label_en_source),' ',''),';') as label_en_source4compare
						from
								w3a
						)
				---------------------
				,w1b as	(
						select
								cadc.id as etl_id,
								cadc.area_domain,
								cadc.label as label_target,
								cadc.description as description_target,
								cadc.label_en as label_en_target,
								cadc.description_en as description_en_target,
								string_to_array(replace(lower(cadc.label_en),' ',''),';') as label_en_target4compare
						from
								@extschema@.c_area_domain_category as cadc
						where
								cadc.area_domain = _area_domain
						)
				---------------------
				,w1 as 	(
						select w1b.*, w4a.* from w1b cross join w4a
						)
				,w2 as	(
						select w1.*, true as check_label from w1
						where @extschema@.fn_etl_array_compare(w1.label_en_source4compare,w1.label_en_target4compare)
						)
				,w3 as	(
						select
								w2.id_ordering_source,
								w2.area_domain_category_source,
								w2.etl_id,
								w2.label_target as label,
								w2.description_target as description,
								w2.label_en_target as label_en,
								w2.description_en_target as description_en,
								w2.check_label
						from
								w2
						)
				,w4 as	(
						select
								w4a.id_ordering_source as id,
								w4a.area_domain_category_source as area_domain_category,
								w3.etl_id,
								w3.label,
								w3.description,
								w3.label_en,
								w3.description_en,
								w3.check_label
						from
								w4a left join w3
						on
								w4a.id_ordering_source = w3.id_ordering_source
						and		w4a.area_domain_category_source = w3.area_domain_category_source
						)
				select w4.* from w4 order by w4.id;
			else
				-- do pairing over national language
				return query
				with
				w1a as	(
						select _area_domain_category as res
						)
				,w2a as	(
						select json_array_elements(w1a.res) as res from w1a
						)
				,w3a as	(
						select
								(res->>'id')::integer as id_ordering_source,
								replace(replace((res->>'area_domain_category')::varchar,'[',''),']','') as area_domain_category_source,
								(res->>'label')::varchar as label_source
						from
								w2a
						)
				,w4a as	(
						select
								w3a.id_ordering_source,
								(string_to_array(w3a.area_domain_category_source,','))::integer[] as area_domain_category_source,
								w3a.label_source,
								string_to_array(replace(lower(w3a.label_source),' ',''),';') as label_source4compare
						from
								w3a
						)
				---------------------
				,w1b as	(
						select
								cadc.id as etl_id,
								cadc.area_domain,
								cadc.label as label_target,
								cadc.description as description_target,
								cadc.label_en as label_en_target,
								cadc.description_en as description_en_target,
								string_to_array(replace(lower(cadc.label),' ',''),';') as label_target4compare
						from
								@extschema@.c_area_domain_category as cadc
						where
								cadc.area_domain = _area_domain
						)
				---------------------
				,w1 as 	(
						select w1b.*, w4a.* from w1b cross join w4a
						)
				,w2 as	(
						select w1.*, true as check_label from w1
						where @extschema@.fn_etl_array_compare(w1.label_source4compare,w1.label_target4compare)
						)
				,w3 as	(
						select
								w2.id_ordering_source,
								w2.area_domain_category_source,
								w2.etl_id,
								w2.label_target as label,
								w2.description_target as description,
								w2.label_en_target as label_en,
								w2.description_en_target as description_en,
								w2.check_label
						from
								w2
						)
				,w4 as	(
						select
								w4a.id_ordering_source as id,
								w4a.area_domain_category_source as area_domain_category,
								w3.etl_id,
								w3.label,
								w3.description,
								w3.label_en,
								w3.description_en,
								w3.check_label
						from
								w4a left join w3
						on
								w4a.id_ordering_source = w3.id_ordering_source
						and		w4a.area_domain_category_source = w3.area_domain_category_source
						)
				select w4.* from w4 order by w4.id;				
			end if;
		else
			raise notice '%',_raise_notice_text;

			return query
			select
					null::integer as id,
					null::integer[] as area_domain_category,
					null::integer as etl_id,
					cadc.label,
					cadc.description,
					cadc.label_en,
					cadc.description_en,
					null::boolean as check_label
			from
					@extschema@.c_area_domain_category as cadc
			where
					cadc.area_domain = _area_domain
			order
					by cadc.id;			
		end if;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_check_area_domain_categories4etl(integer, json) is
'The function returns records of area domain categories for given input area domain with inforamtion if input label of area domain category was paired in target DB or not.';

grant execute on function @extschema@.fn_etl_check_area_domain_categories4etl(integer, json) to public;
-- </function>



DROP FUNCTION IF EXISTS @extschema@.fn_etl_check_sub_population_categories4etl(integer, json) CASCADE;
-- <function name="fn_etl_check_sub_population_categories4etl" schema="extschema" src="functions/extschema/etl/fn_etl_check_sub_population_categories4etl.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_check_sub_population_categories4etl(integer, json)

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_check_sub_population_categories4etl(integer, json) CASCADE;

create or replace function @extschema@.fn_etl_check_sub_population_categories4etl
(
	_sub_population				integer, -- id from target c_sub_population
	_sub_population_category	json
)
returns table
(
	id						integer,
	sub_population_category	integer[],
	etl_id					integer,
	label					character varying,
	description				text,
	label_en				character varying,
	description_en			text,
	check_label				boolean
)
as
$$
declare
		_label_en_type_source				varchar[];
		_check_label_en_type				integer;
		_check_count_categories				integer;
		_check_count_elements				integer;
		_check_count_categories_distinct	integer;
		_check_count_label_en_distinct		integer;
		_raise_notice_text					text;
		_check_info							integer;
		_check_count_pairing_label_en		integer;
begin	
		if _sub_population is null
		then
			raise exception 'Error 01: fn_etl_check_sub_population_categories4etl: Input argument _sub_population must not be NULL!';
		end if;
	
		if _sub_population_category is null
		then
			raise exception 'Error 02: fn_etl_check_sub_population_categories4etl: Input argument _sub_population_category must not be NULL!';
		end if;
		---------------------------------------------------
		with
		w1 as	(
				select _sub_population_category as res
				)
		,w2 as	(
				select json_array_elements(w1.res) as res from w1
				)
		,w3 as	(
				select (res->>'label_en_type')::varchar as label_en_type_source	from w2
				)
		,w4 as	(
				select replace(lower(w3.label_en_type_source),' ','') as label_en_type_source from w3
				)
		select array_agg(t.label_en_type_source) from (select distinct w4.label_en_type_source from w4) as t
		into _label_en_type_source;

		if array_length(_label_en_type_source,1) is distinct from 1
		then
			raise exception 'Error 03: fn_etl_check_sub_population_categories4etl: English labels of sub population in input argument _sub_population_category are not equal!';
		end if;
		--------------------------------------------------
		-- check if input sub population is equal with target
		with
		w1 as	(
				select
						csp.id,
						string_to_array(replace(lower(csp.label_en),' ',''),';') as label_en_type_target4compare
				from
						@extschema@.c_sub_population as csp
				where
						csp.id = _sub_population
				)
		select count(w1.*) from w1 where @extschema@.fn_etl_array_compare(string_to_array(lower(_label_en_type_source[1]),';'),w1.label_en_type_target4compare) = true
		into _check_label_en_type;
		-- there will be value 0 or 1
		-- 0 => source sub population label_en is different from target sub population label_en
		-----------------------------------------------------------------------
		-----------------------------------------------------------------------
		-- source sub population label_en is equal with target sub population label_en => next step are checks of categories
		---------------------------------------------------
		-- count of categories for target sub population
		select count(cspc.*) from @extschema@.c_sub_population_category as cspc
		where cspc.sub_population = _sub_population
		into _check_count_categories;
		---------------------------------------------------
		-- count of categories for source sub population
		with
		w1 as	(
				select _sub_population_category as res
				)
		,w2 as	(
				select json_array_elements(w1.res) from w1
				)
		select count(w2.*) from w2
		into _check_count_elements;
		---------------------------------------------------
		if _check_count_categories is distinct from _check_count_elements
		then
			--raise exception 'Error 03: fn_etl_check_sub_population_categories4etl: Count elements in input argument _sub_population_category is not equal count of categories of input sub population [ID = %] in target c_sub_population_category table!',_sub_population;
			if _check_label_en_type = 1
			then
				--_raise_notice_text := 'Selected sub population to pair is equal with source but count of categories is not equal!';
				_raise_notice_text := 'Different count of categories, the selected subpopulation cannot be paired!';
			else
				--_raise_notice_text := 'Selected sub population to pair is not equal with source and count of categories is not equal!';
				_raise_notice_text := 'Different count of categories, the selected subpopulation cannot be paired!';
			end if;

			_check_info := 1;
		else
			_check_info := 0;
		end if;
		---------------------------------------------------
		select count(t.*) from
		(select distinct cspc.label_en from @extschema@.c_sub_population_category as cspc
		where cspc.sub_population = _sub_population) as t
		into _check_count_categories_distinct;
		---------------------------------------------------
		if _check_count_categories is distinct from _check_count_categories_distinct
		then
			--raise exception 'Error 04: fn_etl_check_sub_population_categories4etl: English label of categories of input sub population [ID = %] in target c_sub_population_category table is not unique!',_sub_population;
			if _check_label_en_type = 1
			then			
				--_raise_notice_text := 'Selected sub population to pair is equal with source but english lables of target are not unique!';
				_raise_notice_text := 'Duplicate categories in the target database, the selected subpopulation cannot be paired!';
			else
				--_raise_notice_text := 'Selected sub population to pair is not equal with source and english lables of target are not unique!';
				_raise_notice_text := 'Duplicate categories in the target database, the selected subpopulation cannot be paired!';
			end if;

			_check_info := _check_info + 1;
		else
			_check_info := _check_info + 0;
		end if;
		---------------------------------------------------
		with
		w1 as	(
				select _sub_population_category as res
				)
		,w2 as	(
				select json_array_elements(w1.res) as res from w1
				)
		,w3 as	(
				select (res->>'label_en')::varchar as label_en from	w2
				)
		select count(t.*) from
		(select distinct w3.label_en from w3) as t
		into _check_count_label_en_distinct;
		---------------------------------------------------
		if _check_count_elements is distinct from _check_count_label_en_distinct
		then
			--raise exception 'Error 05: fn_etl_check_sub_population_categories4etl: English label of categories of input argument _sub_population_category = % is not unique!',_sub_population_category;
			if _check_label_en_type = 1
			then
				--_raise_notice_text := 'Selected sub population to pair is equal with source but english lables of source are not unique!';
				_raise_notice_text := 'Duplicate categories in the source database, the selected subpopulation cannot be paired!';
			else
				--_raise_notice_text := 'Selected sub population to pair is not equal with source and english lables of source are not unique!';
				_raise_notice_text := 'Duplicate categories in the source database, the selected subpopulation cannot be paired!';
			end if;

			_check_info := _check_info + 1;
		else
			_check_info := _check_info + 0;
		end if;	
		--------------------------------------------------
		-----------------------------------------------------------------------
		-----------------------------------------------------------------------
		if _check_info = 0
		then
			-- OK
			-- check if pairing over international language will have already some record
			with
			w1a as	(
					select _sub_population_category as res
					)
			,w2a as	(
					select json_array_elements(w1a.res) as res from w1a
					)
			,w3a as	(
					select
							(res->>'id')::integer as id_ordering_source,
							replace(replace((res->>'sub_population_category')::varchar,'[',''),']','') as sub_population_category_source,
							(res->>'label_en')::varchar as label_en_source
					from
							w2a
					)
			,w4a as	(
					select
							w3a.id_ordering_source,
							(string_to_array(w3a.sub_population_category_source,','))::integer[] as sub_population_category_source,
							w3a.label_en_source,
							string_to_array(replace(lower(w3a.label_en_source),' ',''),';') as label_en_source4compare
					from
							w3a
					)
			---------------------
			,w1b as	(
					select
							cspc.id as etl_id,
							cspc.sub_population,
							cspc.label as label_target,
							cspc.description as description_target,
							cspc.label_en as label_en_target,
							cspc.description_en as description_en_target,
							string_to_array(replace(lower(cspc.label_en),' ',''),';') as label_en_target4compare
					from
							@extschema@.c_sub_population_category as cspc
					where
							cspc.sub_population = _sub_population
					)
			---------------------
			,w1 as 	(
					select w1b.*, w4a.* from w1b cross join w4a
					)
			,w2 as	(
					select w1.*, true as check_label_en from w1
					where @extschema@.fn_etl_array_compare(w1.label_en_source4compare,w1.label_en_target4compare)
					)
			,w3 as	(
					select
							w2.id_ordering_source,
							w2.sub_population_category_source,
							w2.etl_id,
							w2.label_target as label,
							w2.description_target as description,
							w2.label_en_target as label_en,
							w2.description_en_target as description_en,
							w2.check_label_en
					from
							w2
					)
			,w4 as	(
					select
							w4a.id_ordering_source as id,
							w4a.sub_population_category_source as sub_population_category,
							w3.etl_id,
							w3.label,
							w3.description,
							w3.label_en,
							w3.description_en,
							w3.check_label_en
					from
							w4a left join w3
					on
							w4a.id_ordering_source = w3.id_ordering_source
					and		w4a.sub_population_category_source = w3.sub_population_category_source
					)
			select count(w4.*) from w4 where w4.check_label_en = true
			into _check_count_pairing_label_en;

			if _check_count_pairing_label_en > 0
			then
				return query
				with
				w1a as	(
						select _sub_population_category as res
						)
				,w2a as	(
						select json_array_elements(w1a.res) as res from w1a
						)
				,w3a as	(
						select
								(res->>'id')::integer as id_ordering_source,
								replace(replace((res->>'sub_population_category')::varchar,'[',''),']','') as sub_population_category_source,
								(res->>'label_en')::varchar as label_en_source
						from
								w2a
						)
				,w4a as	(
						select
								w3a.id_ordering_source,
								(string_to_array(w3a.sub_population_category_source,','))::integer[] as sub_population_category_source,
								w3a.label_en_source,
								string_to_array(replace(lower(w3a.label_en_source),' ',''),';') as label_en_source4compare
						from
								w3a
						)
				---------------------
				,w1b as	(
						select
								cspc.id as etl_id,
								cspc.sub_population,
								cspc.label as label_target,
								cspc.description as description_target,
								cspc.label_en as label_en_target,
								cspc.description_en as description_en_target,
								string_to_array(replace(lower(cspc.label_en),' ',''),';') as label_en_target4compare
						from
								@extschema@.c_sub_population_category as cspc
						where
								cspc.sub_population = _sub_population
						)
				---------------------
				,w1 as 	(
						select w1b.*, w4a.* from w1b cross join w4a
						)
				,w2 as	(
						select w1.*, true as check_label from w1
						where @extschema@.fn_etl_array_compare(w1.label_en_source4compare,w1.label_en_target4compare)
						)
				,w3 as	(
						select
								w2.id_ordering_source,
								w2.sub_population_category_source,
								w2.etl_id,
								w2.label_target as label,
								w2.description_target as description,
								w2.label_en_target as label_en,
								w2.description_en_target as description_en,
								w2.check_label
						from
								w2
						)
				,w4 as	(
						select
								w4a.id_ordering_source as id,
								w4a.sub_population_category_source as sub_population_category,
								w3.etl_id,
								w3.label,
								w3.description,
								w3.label_en,
								w3.description_en,
								w3.check_label
						from
								w4a left join w3
						on
								w4a.id_ordering_source = w3.id_ordering_source
						and		w4a.sub_population_category_source = w3.sub_population_category_source
						)
				select w4.* from w4 order by w4.id;
			else
				-- do pairing over national language
				return query
				with
				w1a as	(
						select _sub_population_category as res
						)
				,w2a as	(
						select json_array_elements(w1a.res) as res from w1a
						)
				,w3a as	(
						select
								(res->>'id')::integer as id_ordering_source,
								replace(replace((res->>'sub_population_category')::varchar,'[',''),']','') as sub_population_category_source,
								(res->>'label')::varchar as label_source
						from
								w2a
						)
				,w4a as	(
						select
								w3a.id_ordering_source,
								(string_to_array(w3a.sub_population_category_source,','))::integer[] as sub_population_category_source,
								w3a.label_source,
								string_to_array(replace(lower(w3a.label_source),' ',''),';') as label_source4compare
						from
								w3a
						)
				---------------------
				,w1b as	(
						select
								cspc.id as etl_id,
								cspc.sub_population,
								cspc.label as label_target,
								cspc.description as description_target,
								cspc.label_en as label_en_target,
								cspc.description_en as description_en_target,
								string_to_array(replace(lower(cspc.label),' ',''),';') as label_target4compare
						from
								@extschema@.c_sub_population_category as cspc
						where
								cspc.sub_population = _sub_population
						)
				---------------------
				,w1 as 	(
						select w1b.*, w4a.* from w1b cross join w4a
						)
				,w2 as	(
						select w1.*, true as check_label from w1
						where @extschema@.fn_etl_array_compare(w1.label_source4compare,w1.label_target4compare)
						)
				,w3 as	(
						select
								w2.id_ordering_source,
								w2.sub_population_category_source,
								w2.etl_id,
								w2.label_target as label,
								w2.description_target as description,
								w2.label_en_target as label_en,
								w2.description_en_target as description_en,
								w2.check_label
						from
								w2
						)
				,w4 as	(
						select
								w4a.id_ordering_source as id,
								w4a.sub_population_category_source as sub_population_category,
								w3.etl_id,
								w3.label,
								w3.description,
								w3.label_en,
								w3.description_en,
								w3.check_label
						from
								w4a left join w3
						on
								w4a.id_ordering_source = w3.id_ordering_source
						and		w4a.sub_population_category_source = w3.sub_population_category_source
						)
				select w4.* from w4 order by w4.id;				
			end if;
		else
			raise notice '%',_raise_notice_text;

			return query
			select
					null::integer as id,
					null::integer[] as sub_population_category,
					null::integer as etl_id,
					cspc.label,
					cspc.description,
					cspc.label_en,
					cspc.description_en,
					null::boolean as check_label
			from
					@extschema@.c_sub_population_category as cspc
			where
					cspc.sub_population = _sub_population
			order
					by cspc.id;			
		end if;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_check_sub_population_categories4etl(integer, json) is
'The function returns records of sub population categories for given input sub population with inforamtion if input label of sub population category was paired in target DB or not.';

grant execute on function @extschema@.fn_etl_check_sub_population_categories4etl(integer, json) to public;
-- </function>



DROP FUNCTION IF EXISTS @extschema@.fn_etl_get_area_domain_categories4roller(integer, integer, character varying, integer[]) CASCADE;
-- <function name="fn_etl_get_area_domain_categories4roller" schema="extschema" src="functions/extschema/etl/fn_etl_get_area_domain_categories4roller.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_get_area_domain_categories4roller(integer, integer, integer[])

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_get_area_domain_categories4roller(integer, integer, integer[]) CASCADE;

create or replace function @extschema@.fn_etl_get_area_domain_categories4roller
(
	_id_ordering			integer,
	_area_domain			integer,
	_etl_id					integer[] default null::integer[]
)
returns table
(
	id						integer,
	etl_id					integer,
	label					character varying,
	description				text,
	label_en				character varying,
	description_en			text
)
as
$$
declare
begin	
		if _id_ordering is null
		then
			raise exception 'Error 01: fn_etl_get_area_domain_categories4roller: Input argument _id_ordering must not be NULL!';
		end if;
	
		if _area_domain is null
		then
			raise exception 'Error 02: fn_etl_get_area_domain_categories4roller: Input argument _area_domain must not be NULL!';
		end if;

		return query execute
		'
		with
		w1 as	(
				select cadc.* from @extschema@.c_area_domain_category cadc
				where cadc.area_domain = $1
				and cadc.id not in (select unnest($2))
				)
		select
				$3 as id,
				w1.id as etl_id,
				w1.label,
				w1.description,
				w1.label_en,
				w1.description_en
		from
				w1 order by w1.id
		'
		using _area_domain, _etl_id, _id_ordering;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_get_area_domain_categories4roller(integer, integer, integer[]) is
'The function returns records of area domain categories for given area domain into roller.';

grant execute on function @extschema@.fn_etl_get_area_domain_categories4roller(integer, integer, integer[]) to public;
-- </function>



DROP FUNCTION IF EXISTS @extschema@.fn_etl_get_sub_population_categories4roller(integer, integer, character varying, integer[]) CASCADE;
-- <function name="fn_etl_get_sub_population_categories4roller" schema="extschema" src="functions/extschema/etl/fn_etl_get_sub_population_categories4roller.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_get_sub_population_categories4roller(integer, integer, integer[])

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_get_sub_population_categories4roller(integer, integer, integer[]) CASCADE;

create or replace function @extschema@.fn_etl_get_sub_population_categories4roller
(
	_id_ordering			integer,
	_sub_population			integer,
	_etl_id					integer[] default null::integer[]
)
returns table
(
	id						integer,
	etl_id					integer,
	label					character varying,
	description				text,
	label_en				character varying,
	description_en			text
)
as
$$
declare
begin	
		if _id_ordering is null
		then
			raise exception 'Error 01: fn_etl_get_sub_population_categories4roller: Input argument _id_ordering must not be NULL!';
		end if;
	
		if _sub_population is null
		then
			raise exception 'Error 02: fn_etl_get_sub_population_categories4roller: Input argument _sub_population must not be NULL!';
		end if;
	
		return query execute
		'
		with
		w1 as	(
				select cspc.* from @extschema@.c_sub_population_category as cspc
				where cspc.sub_population = $1
				and cspc.id not in (select unnest($2))
				)
		select
				$3 as id,
				w1.id as etl_id,
				w1.label,
				w1.description,
				w1.label_en,
				w1.description_en
		from
				w1 order by w1.id
		'
		using _sub_population, _etl_id, _id_ordering;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_get_sub_population_categories4roller(integer, integer, integer[]) is
'The function returns records of sub population categories for given sub population into roller.';

grant execute on function @extschema@.fn_etl_get_sub_population_categories4roller(integer, integer, integer[]) to public;
-- </function>



-- <function name="fn_etl_check_sub_populations4etl_json" schema="extschema" src="functions/extschema/etl/fn_etl_check_sub_populations4etl_json.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_check_sub_populations4etl_json(json, integer[])

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_check_sub_populations4etl_json(json, integer[]) CASCADE;

create or replace function @extschema@.fn_etl_check_sub_populations4etl_json
(
	_sub_populations	json,
	_etl_id				integer[] default null::integer[]
)
returns json
as
$$
declare
		_array_id										integer[];
		_check_etl_id									integer;
		_res_id_transfer								integer[];
		_res_id_manually_type_equal						integer[];
		_res_id_manually_type_equal_not					integer[];
		_res_id_manually_type_equal_not_final			integer[];	
		_res_id_pair									integer[];
		_sub_population_i								integer[];
		_label_en_type_i								varchar;
		_sub_population_etl_id							integer[];
		_sub_population_check							boolean[];
		_array_category_label_en_source					text[];
		_count_categories_target						integer;
		_res_loop_iii									integer;
		_do_action_in_loop								integer;
		_id_comulation									integer[];
		_etl_id_i_target_array							integer[];
		_sub_population_ii								integer[];
		_label_en_type_ii								varchar;	
		_sub_population_etl_id_ii						integer[];
		_sub_population_check_ii						boolean[];
		_count_category_label_en_source_type_equal_not	integer;
		_check_count_categories_type_equal_not			integer;
		_res_id_manually								integer[];
		_check_manually_pair							integer;
		_check_manually_transfer						integer;
		_check_pair_transfer							integer;
		_res											json;
begin
		if _sub_populations is null
		then
			raise exception 'Error 01: fn_etl_check_sub_populations4etl_json: Input argument _sub_populations must not be NULL!';
		end if;

		with
		w1 as	(
				select _sub_populations as res
				)
		,w2 as	(
				select json_array_elements(w1.res) as res from w1
				)
		,w3 as	(
				select (res->>'id')::integer as id from	w2
				)
		select array_agg(w3.id order by w3.id) from	w3
		into _array_id;
	
		select count(t.*) from
		(
		select csp.id from @extschema@.c_sub_population as csp except -- EXCEPT input ETL ids with ids in c_sub_population_table
		select unnest(_etl_id)
		) as t
		into _check_etl_id;
	
		if _check_etl_id = 0 -- if IDs in input argument _etl_id removed all IDs from c_sub_population table => then auto transfer
		then
			with
			w1 as	(select unnest(_array_id) as id, true as auto_transfer, false as auto_pair)
			select json_agg(json_build_object('id',w1.id,'auto_transfer',w1.auto_transfer,'auto_pair',w1.auto_pair)) from w1
			into _res;
		else
			_res_id_transfer := array[0];
			_res_id_manually_type_equal := array[0];
			_res_id_manually_type_equal_not := array[0];
			_res_id_pair := array[0];

			for i in 1..array_length(_array_id,1) -- The first cycle over TYPEs
			loop
				-----------------------------------------------------
				with
				w1 as	(
						select _sub_populations as res
						)
				,w2 as	(
						select json_array_elements(w1.res) as res from w1
						)
				,w3 as	(
						select
								(res->>'id')::integer as id,
								replace(replace((res->>'sub_population')::varchar,'[',''),']','') as sub_population,
								(res->>'label_en_type')::varchar as label_en_type
						from
								w2
						)
				,w4 as	(
						select
								w3.id,
								(string_to_array(w3.sub_population,','))::integer[] as sub_population,
								w3.label_en_type
						from
								w3
						)
				select
						w4.sub_population,
						w4.label_en_type
				from
						w4 where w4.id = _array_id[i]
				into
						_sub_population_i,
						_label_en_type_i;
				-----------------------------------------------------
				-- check on level type
				with
				w1 as	(
						select * from @extschema@.fn_etl_check_sub_populations4etl
							(
							_array_id[i],
							_sub_population_i,
							_label_en_type_i,
							'en'::varchar,
							_etl_id
							)
						)
				select
						array_agg(w1.etl_id order by w1.etl_id) as sp_etl_id, -- ID from c_sub_population table
						array_agg(w1.check_label_en order by w1.etl_id) as sp_check
				from
						w1
				into
						_sub_population_etl_id,
						_sub_population_check;
				-----------------------------------------------------					
				if _sub_population_etl_id is null  -- => auto transfer
				then
					_res_id_transfer := _res_id_transfer || _array_id[i];
				else
					if array_length(_sub_population_etl_id,1) = 1 and _sub_population_check[1] = true -- maybe to pair if all categories will be paired
					then
						-- sub population source label and sub population target label is equal
						-- => check categories
						with
						w1 as	(
								select _sub_populations as res
								)
						,w2 as	(
								select json_array_elements(w1.res) as res from w1
								)
						,w3 as	(
								select
										(res->>'id')::integer as id,
										replace(replace((res->>'sub_population')::varchar,'[',''),']','') as sub_population,
										(res->>'label_en_type')::varchar as label_en_type,
										res->'category' as category_json
								from
										w2
								)
						,w4 as	(
								select
										w3.id,
										(string_to_array(w3.sub_population,','))::integer[] as sub_population,
										w3.label_en_type,
										w3.category_json
								from
										w3 where w3.id = _array_id[i]
								)
						,w5 as	(
								select
										w4.id,
										w4.sub_population,
										w4.label_en_type,
										json_array_elements(w4.category_json) as category_json
								from
										w4
								)
						,w6 as	(
								select category_json->>'label_en_category' as category_label_en from w5
								)
						select array_agg(w6.category_label_en) from w6
						into _array_category_label_en_source;
					
						select count(cspc.*) from @extschema@.c_sub_population_category as cspc
						where cspc.sub_population = _sub_population_etl_id[1]
						into _count_categories_target;
					
						if array_length(_array_category_label_en_source,1) is distinct from _count_categories_target
						then
							-- info: Source and target labels of sub population type are equal, but count of their categories are not equal!
							-- => manually pair
							_res_loop_iii := 0; -- <= loop over categories is not needed !!!
						else
							_do_action_in_loop := 1;
							_id_comulation := array[0];

							for iii in 1..array_length(_array_category_label_en_source,1)
							loop						
								if _do_action_in_loop = 1
								then
									select array_agg(cspc.id) from @extschema@.c_sub_population_category as cspc
									where cspc.sub_population = _sub_population_etl_id[1]
									and cspc.id not in (select unnest(_id_comulation))
									and @extschema@.fn_etl_array_compare
										(
										string_to_array(replace(lower(cspc.label_en),' ',''),';'),
										string_to_array(replace(lower(_array_category_label_en_source[iii]),' ',''),';')					
										) = true
									into _etl_id_i_target_array;
									-- founded 1 element in array => ok => auto pair
									-- founded 2 or more elements in array => error
									-- founded 0 => null => ko => manualy pair
								
									if _etl_id_i_target_array is null
									then
										_do_action_in_loop := 0; -- stop action in loop
										_res_loop_iii := 0;
									else
										if array_length(_etl_id_i_target_array,1) is distinct from 1
										then
											raise exception 'Error 02: fn_etl_check_sub_populations4etl_json: For sub population type exists two or more equal categories!';
										else
											_id_comulation := _id_comulation || _etl_id_i_target_array[1];
											_res_loop_iii := 1;
										end if;
									end if;
								end if;						
							end loop;
						end if;							

						if _res_loop_iii = 0
						then
							_res_id_manually_type_equal := _res_id_manually_type_equal || _array_id[i]; -- must stay here
						else
							_res_id_pair := _res_id_pair || _array_id[i];
							--_etl_id := _etl_id || _sub_population_etl_id[1];		
						end if;	
					
						_etl_id := _etl_id || _sub_population_etl_id[1]; -- IDs input + IDs label types are equal
					else
						-- sub population type source label and sub population target label is NOT equal => manually pairing
						_res_id_manually_type_equal_not := _res_id_manually_type_equal_not || _array_id[i]; -- can move to auto transfer
					end if;
				end if;
				-----------------------------------------------------
			end loop;

			if _res_id_manually_type_equal_not is distinct from array[0] -- exists one or more types for manualy pairing that can move to auto transfer
			then
				_res_id_manually_type_equal_not_final := _res_id_manually_type_equal_not; -- here _final is original array

				for ii in 1..array_length(_res_id_manually_type_equal_not,1) -- the second cycle
				loop
					if _res_id_manually_type_equal_not[ii] is distinct from 0
					then
						-----------------------------------------------------
						with
						w1 as	(
								select _sub_populations as res
								)
						,w2 as	(
								select json_array_elements(w1.res) as res from w1
								)
						,w3 as	(
								select
										(res->>'id')::integer as id,
										replace(replace((res->>'sub_population')::varchar,'[',''),']','') as sub_population,
										(res->>'label_en_type')::varchar as label_en_type
								from
										w2
								)
						,w4 as	(
								select
										w3.id,
										(string_to_array(w3.sub_population,','))::integer[] as sub_population,
										w3.label_en_type
								from
										w3
								)
						select
								w4.sub_population,
								w4.label_en_type
						from
								w4 where w4.id = _res_id_manually_type_equal_not[ii]
						into
								_sub_population_ii,
								_label_en_type_ii;
						-----------------------------------------------------
						with
						w1 as	(
								select * from @extschema@.fn_etl_check_sub_populations4etl
									(
									_res_id_manually_type_equal_not[ii],
									_sub_population_ii,
									_label_en_type_ii,
									'en'::varchar,
									_etl_id -- this array is supplemented by (IDs input + IDs label types are equal)
									)
								)
						select
								array_agg(w1.etl_id order by w1.etl_id) as sp_etl_id, -- ID from c_sub_population table
								array_agg(w1.check_label_en order by w1.etl_id) as sp_check
						from
								w1
						into
								_sub_population_etl_id_ii,
								_sub_population_check_ii;
						-----------------------------------------------------
						if _sub_population_etl_id_ii is null  -- => auto transfer
						then
							_res_id_transfer := _res_id_transfer || _res_id_manually_type_equal_not[ii]; -- this manually ID is moved to auto transfer
							_res_id_manually_type_equal_not_final := array_remove(_res_id_manually_type_equal_not_final,_res_id_manually_type_equal_not[ii]); -- this manually ID is removed from manually pairing equal type not
						else
							-- exist one or more different types => then do check count of categories:
							-- 1. => if exists type with equal count of categories => stay in manually pairing
							-- 2. => if NOT exists any type with equal count of categories => ID can move to transfer
						
							-- => check count of source categories
							with
							w1 as	(
									select _sub_populations as res
									)
							,w2 as	(
									select json_array_elements(w1.res) as res from w1
									)
							,w3 as	(
									select
											(res->>'id')::integer as id,
											replace(replace((res->>'sub_population')::varchar,'[',''),']','') as sub_population,
											(res->>'label_en_type')::varchar as label_en_type,
											res->'category' as category_json
									from
											w2
									)
							,w4 as	(
									select
											w3.id,
											(string_to_array(w3.sub_population,','))::integer[] as sub_population,
											w3.label_en_type,
											w3.category_json
									from
											w3 where w3.id = _res_id_manually_type_equal_not[ii]
									)
							,w5 as	(
									select
											w4.id,
											w4.sub_population,
											w4.label_en_type,
											json_array_elements(w4.category_json) as category_json
									from
											w4
									)
							,w6 as	(
									select category_json->>'label_en_category' as category_label_en from w5
									)
							select count(w6.category_label_en) from w6
							into _count_category_label_en_source_type_equal_not;						
						
							with
							w1 as	(
									select * from @extschema@.fn_etl_check_sub_populations4etl
										(
										_res_id_manually_type_equal_not[ii],
										_sub_population_ii,
										_label_en_type_ii,
										'en'::varchar,
										_etl_id -- this array is supplemented by (IDs input + IDs label types are equal)
										)
									)
							,w2 as	(
									select * from @extschema@.c_sub_population_category as cspc
									where cspc.sub_population in (select w1.etl_id from w1)
									)
							,w3 as	(
									select w2.sub_population, count(w2.*) as pocet from w2
									group by w2.sub_population
									)
							select count(w3.*) from w3 where w3.pocet = _count_category_label_en_source_type_equal_not
							into _check_count_categories_type_equal_not;
						
							if _check_count_categories_type_equal_not = 0
							then
								-- ID can move to auto transfer
								_res_id_transfer := _res_id_transfer || _res_id_manually_type_equal_not[ii]; -- this manually ID is moved to auto transfer
								_res_id_manually_type_equal_not_final := array_remove(_res_id_manually_type_equal_not_final,_res_id_manually_type_equal_not[ii]); -- this manually ID is removed from manually pairing equal type not							
							else
								-- ID stay in manually pairing
								_res_id_manually_type_equal_not_final := _res_id_manually_type_equal_not_final;
							end if;
				
						end if;																
					end if;
				end loop;
			end if;
		
			_res_id_manually := _res_id_manually_type_equal || _res_id_manually_type_equal_not_final;

			
			-- check MANUALLY and PAIR
			with
			w1 as	(select unnest(_res_id_manually) as id_manually)
			,w2 as	(select unnest(_res_id_pair) as id_pair)	
			,w3 as	(select w1.* from w1 where w1.id_manually is distinct from 0)
			,w4 as	(select w2.* from w2 where w2.id_pair is distinct from 0)
			,w5 as	(select count(w3.*) as res from w3 where w3.id_manually in (select w4.id_pair from w4))
			,w6 as	(select count(w4.*) as res from w4 where w4.id_pair in (select w3.id_manually from w3))
			select sum(t.res) from (select w5.res from w5 union all	select w6.res from w6) as t
			into _check_manually_pair;

			if _check_manually_pair is distinct from 0
			then
				raise exception 'Error 03: fn_etl_check_sub_populations4etl_json: There must not exist situation auto pair and manually pair for the sub population type at the same time!';
			end if;

			-- check MANUALLY and TRANFER
			with
			w1 as	(select unnest(_res_id_manually) as id_manually)
			,w2 as	(select unnest(_res_id_transfer) as id_transfer)	
			,w3 as	(select w1.* from w1 where w1.id_manually is distinct from 0)
			,w4 as	(select w2.* from w2 where w2.id_transfer is distinct from 0)
			,w5 as	(select count(w3.*) as res from w3 where w3.id_manually in (select w4.id_transfer from w4))
			,w6 as	(select count(w4.*) as res from w4 where w4.id_transfer in (select w3.id_manually from w3))
			select sum(t.res) from (select w5.res from w5 union all	select w6.res from w6) as t
			into _check_manually_transfer;

			if _check_manually_transfer is distinct from 0
			then
				raise exception 'Error 04: fn_etl_check_sub_populations4etl_json: There must not exist situation auto transfer and manually pair for the sub population type at the same time!';
			end if;	

			-- check PAIR and TRANFER
			with
			w1 as	(select unnest(_res_id_pair) as id_pair)
			,w2 as	(select unnest(_res_id_transfer) as id_transfer)	
			,w3 as	(select w1.* from w1 where w1.id_pair is distinct from 0)
			,w4 as	(select w2.* from w2 where w2.id_transfer is distinct from 0)
			,w5 as	(select count(w3.*) as res from w3 where w3.id_pair in (select w4.id_transfer from w4))
			,w6 as	(select count(w4.*) as res from w4 where w4.id_transfer in (select w3.id_pair from w3))
			select sum(t.res) from (select w5.res from w5 union all	select w6.res from w6) as t
			into _check_pair_transfer;

			if _check_pair_transfer is distinct from 0
			then
				raise exception 'Error 05: fn_etl_check_sub_populations4etl_json: There must not exist situation auto transfer and auto pair for the sub population type at the same time!';
			end if;					

			with
			w1 as	(select unnest(_array_id) as id)
			,w2 as	(select unnest(_res_id_pair) as id_pair)
			,w3 as	(select unnest(_res_id_transfer) as id_transfer)
			,w4 as	(select w2.* from w2 where w2.id_pair is distinct from 0)
			,w5 as	(select w3.* from w3 where w3.id_transfer is distinct from 0)
			,w6 as	(
					select
							w1.id,
							case when w5.id_transfer is null then false else true end as auto_transfer,
							case when w4.id_pair is null then false else true end as auto_pair
					from
							w1
							left join w5 on w1.id = w5.id_transfer
							left join w4 on w1.id = w4.id_pair
					order
							by w1.id
					)
			select json_agg(json_build_object('id',w6.id,'auto_transfer',w6.auto_transfer,'auto_pair',w6.auto_pair)) from w6
			into _res;

		end if;
	
		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_check_sub_populations4etl_json(json, integer[]) is
'The function returns one record of sub population if input label of sub population was paired in target DB or the function returns record(s) of sub populations that are possible to pair in target DB. Output is in JSON format.';

grant execute on function @extschema@.fn_etl_check_sub_populations4etl_json(json, integer[]) to public;
-- </function>



-- <function name="fn_etl_check_area_domains4etl_json" schema="extschema" src="functions/extschema/etl/fn_etl_check_area_domains4etl_json.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_check_area_domains4etl_json(json, integer[])

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_check_area_domains4etl_json(json, integer[]) CASCADE;

create or replace function @extschema@.fn_etl_check_area_domains4etl_json
(
	_area_domains		json,
	_etl_id				integer[] default null::integer[]
)
returns json
as
$$
declare
		_array_id										integer[];
		_check_etl_id									integer;
		_res_id_transfer								integer[];
		_res_id_manually_type_equal						integer[];
		_res_id_manually_type_equal_not					integer[];
		_res_id_manually_type_equal_not_final			integer[];	
		_res_id_pair									integer[];
		_area_domain_i									integer[];
		_label_en_type_i								varchar;
		_area_domain_etl_id								integer[];
		_area_domain_check								boolean[];
		_array_category_label_en_source					text[];
		_count_categories_target						integer;
		_res_loop_iii									integer;
		_do_action_in_loop								integer;
		_id_comulation									integer[];
		_etl_id_i_target_array							integer[];
		_area_domain_ii									integer[];
		_label_en_type_ii								varchar;	
		_area_domain_etl_id_ii							integer[];
		_area_domain_check_ii							boolean[];
		_count_category_label_en_source_type_equal_not	integer;
		_check_count_categories_type_equal_not			integer;
		_res_id_manually								integer[];
		_check_manually_pair							integer;
		_check_manually_transfer						integer;
		_check_pair_transfer							integer;
		_res											json;
begin
		if _area_domains is null
		then
			raise exception 'Error 01: fn_etl_check_area_domains4etl_json: Input argument _area_domains must not be NULL!';
		end if;

		with
		w1 as	(
				select _area_domains as res
				)
		,w2 as	(
				select json_array_elements(w1.res) as res from w1
				)
		,w3 as	(
				select (res->>'id')::integer as id from	w2
				)
		select array_agg(w3.id order by w3.id) from	w3
		into _array_id;
	
		select count(t.*) from
		(
		select cad.id from @extschema@.c_area_domain as cad except -- EXCEPT input ETL ids with ids in c_area_domain table
		select unnest(_etl_id)
		) as t
		into _check_etl_id;
	
		if _check_etl_id = 0 -- if IDs in input argument _etl_id removed all IDs from c_area_domain table => then auto transfer
		then
			with
			w1 as	(select unnest(_array_id) as id, true as auto_transfer, false as auto_pair)
			select json_agg(json_build_object('id',w1.id,'auto_transfer',w1.auto_transfer,'auto_pair',w1.auto_pair)) from w1
			into _res;
		else
			_res_id_transfer := array[0];
			_res_id_manually_type_equal := array[0];
			_res_id_manually_type_equal_not := array[0];
			_res_id_pair := array[0];

			for i in 1..array_length(_array_id,1) -- The first cycle over TYPEs
			loop
				-----------------------------------------------------
				with
				w1 as	(
						select _area_domains as res
						)
				,w2 as	(
						select json_array_elements(w1.res) as res from w1
						)
				,w3 as	(
						select
								(res->>'id')::integer as id,
								replace(replace((res->>'area_domain')::varchar,'[',''),']','') as area_domain,
								(res->>'label_en_type')::varchar as label_en_type
						from
								w2
						)
				,w4 as	(
						select
								w3.id,
								(string_to_array(w3.area_domain,','))::integer[] as area_domain,
								w3.label_en_type
						from
								w3
						)
				select
						w4.area_domain,
						w4.label_en_type
				from
						w4 where w4.id = _array_id[i]
				into
						_area_domain_i,
						_label_en_type_i;
				-----------------------------------------------------
				-- check on level type
				with
				w1 as	(
						select * from @extschema@.fn_etl_check_area_domains4etl
							(
							_array_id[i],
							_area_domain_i,
							_label_en_type_i,
							'en'::varchar,
							_etl_id
							)
						)
				select
						array_agg(w1.etl_id order by w1.etl_id) as ad_etl_id, -- ID from c_area_domain table
						array_agg(w1.check_label_en order by w1.etl_id) as ad_check
				from
						w1
				into
						_area_domain_etl_id,
						_area_domain_check;
				-----------------------------------------------------					
				if _area_domain_etl_id is null  -- => auto transfer
				then
					_res_id_transfer := _res_id_transfer || _array_id[i];
				else
					if array_length(_area_domain_etl_id,1) = 1 and _area_domain_check[1] = true -- maybe to pair if all categories will be paired
					then
						-- sub population source label and sub population target label is equal
						-- => check categories
						with
						w1 as	(
								select _area_domains as res
								)
						,w2 as	(
								select json_array_elements(w1.res) as res from w1
								)
						,w3 as	(
								select
										(res->>'id')::integer as id,
										replace(replace((res->>'area_domain')::varchar,'[',''),']','') as area_domain,
										(res->>'label_en_type')::varchar as label_en_type,
										res->'category' as category_json
								from
										w2
								)
						,w4 as	(
								select
										w3.id,
										(string_to_array(w3.area_domain,','))::integer[] as area_domain,
										w3.label_en_type,
										w3.category_json
								from
										w3 where w3.id = _array_id[i]
								)
						,w5 as	(
								select
										w4.id,
										w4.area_domain,
										w4.label_en_type,
										json_array_elements(w4.category_json) as category_json
								from
										w4
								)
						,w6 as	(
								select category_json->>'label_en_category' as category_label_en from w5
								)
						select array_agg(w6.category_label_en) from w6
						into _array_category_label_en_source;
					
						select count(cadc.*) from @extschema@.c_area_domain_category as cadc
						where cadc.area_domain = _area_domain_etl_id[1]
						into _count_categories_target;
					
						if array_length(_array_category_label_en_source,1) is distinct from _count_categories_target
						then
							-- info: Source and target labels of area domain type are equal, but count of their categories are not equal!
							-- => manually pair
							_res_loop_iii := 0; -- <= loop over categories is not needed !!!
						else
							_do_action_in_loop := 1;
							_id_comulation := array[0];

							for iii in 1..array_length(_array_category_label_en_source,1)
							loop						
								if _do_action_in_loop = 1
								then
									select array_agg(cadc.id) from @extschema@.c_area_domain_category as cadc
									where cadc.area_domain = _area_domain_etl_id[1]
									and cadc.id not in (select unnest(_id_comulation))
									and @extschema@.fn_etl_array_compare
										(
										string_to_array(replace(lower(cadc.label_en),' ',''),';'),
										string_to_array(replace(lower(_array_category_label_en_source[iii]),' ',''),';')					
										) = true
									into _etl_id_i_target_array;
									-- founded 1 element in array => ok => auto pair
									-- founded 2 or more elements in array => error
									-- founded 0 => null => ko => manualy pair
								
									if _etl_id_i_target_array is null
									then
										_do_action_in_loop := 0; -- stop action in loop
										_res_loop_iii := 0;
									else
										if array_length(_etl_id_i_target_array,1) is distinct from 1
										then
											raise exception 'Error 02: fn_etl_check_area_domains4etl_json: For sub population type exists two or more equal categories!';
										else
											_id_comulation := _id_comulation || _etl_id_i_target_array[1];
											_res_loop_iii := 1;
										end if;
									end if;
								end if;						
							end loop;
						end if;							

						if _res_loop_iii = 0
						then
							_res_id_manually_type_equal := _res_id_manually_type_equal || _array_id[i]; -- must stay here
						else
							_res_id_pair := _res_id_pair || _array_id[i];
							--_etl_id := _etl_id || _area_domain_etl_id[1];		
						end if;	
					
						_etl_id := _etl_id || _area_domain_etl_id[1]; -- IDs input + IDs label types are equal
					else
						-- sub population type source label and sub population target label is NOT equal => manually pairing
						_res_id_manually_type_equal_not := _res_id_manually_type_equal_not || _array_id[i]; -- can move to auto transfer
					end if;
				end if;
				-----------------------------------------------------
			end loop;

			if _res_id_manually_type_equal_not is distinct from array[0] -- exists one or more types for manualy pairing that can move to auto transfer
			then
				_res_id_manually_type_equal_not_final := _res_id_manually_type_equal_not; -- here _final is original array

				for ii in 1..array_length(_res_id_manually_type_equal_not,1) -- the second cycle
				loop
					if _res_id_manually_type_equal_not[ii] is distinct from 0
					then
						-----------------------------------------------------
						with
						w1 as	(
								select _area_domains as res
								)
						,w2 as	(
								select json_array_elements(w1.res) as res from w1
								)
						,w3 as	(
								select
										(res->>'id')::integer as id,
										replace(replace((res->>'area_domain')::varchar,'[',''),']','') as area_domain,
										(res->>'label_en_type')::varchar as label_en_type
								from
										w2
								)
						,w4 as	(
								select
										w3.id,
										(string_to_array(w3.area_domain,','))::integer[] as area_domain,
										w3.label_en_type
								from
										w3
								)
						select
								w4.area_domain,
								w4.label_en_type
						from
								w4 where w4.id = _res_id_manually_type_equal_not[ii]
						into
								_area_domain_ii,
								_label_en_type_ii;
						-----------------------------------------------------
						with
						w1 as	(
								select * from @extschema@.fn_etl_check_area_domains4etl
									(
									_res_id_manually_type_equal_not[ii],
									_area_domain_ii,
									_label_en_type_ii,
									'en'::varchar,
									_etl_id -- this array is supplemented by (IDs input + IDs label types are equal)
									)
								)
						select
								array_agg(w1.etl_id order by w1.etl_id) as ad_etl_id, -- ID from c_area_domain table
								array_agg(w1.check_label_en order by w1.etl_id) as ad_check
						from
								w1
						into
								_area_domain_etl_id_ii,
								_area_domain_check_ii;
						-----------------------------------------------------
						if _area_domain_etl_id_ii is null  -- => auto transfer
						then
							_res_id_transfer := _res_id_transfer || _res_id_manually_type_equal_not[ii]; -- this manually ID is moved to auto transfer
							_res_id_manually_type_equal_not_final := array_remove(_res_id_manually_type_equal_not_final,_res_id_manually_type_equal_not[ii]); -- this manually ID is removed from manually pairing equal type not
						else
							-- exist one or more different types => then do check count of categories:
							-- 1. => if exists type with equal count of categories => stay in manually pairing
							-- 2. => if NOT exists any type with equal count of categories => ID can move to transfer
						
							-- => check count of source categories
							with
							w1 as	(
									select _area_domains as res
									)
							,w2 as	(
									select json_array_elements(w1.res) as res from w1
									)
							,w3 as	(
									select
											(res->>'id')::integer as id,
											replace(replace((res->>'area_domain')::varchar,'[',''),']','') as area_domain,
											(res->>'label_en_type')::varchar as label_en_type,
											res->'category' as category_json
									from
											w2
									)
							,w4 as	(
									select
											w3.id,
											(string_to_array(w3.area_domain,','))::integer[] as area_domain,
											w3.label_en_type,
											w3.category_json
									from
											w3 where w3.id = _res_id_manually_type_equal_not[ii]
									)
							,w5 as	(
									select
											w4.id,
											w4.area_domain,
											w4.label_en_type,
											json_array_elements(w4.category_json) as category_json
									from
											w4
									)
							,w6 as	(
									select category_json->>'label_en_category' as category_label_en from w5
									)
							select count(w6.category_label_en) from w6
							into _count_category_label_en_source_type_equal_not;						
						
							with
							w1 as	(
									select * from @extschema@.fn_etl_check_area_domains4etl
										(
										_res_id_manually_type_equal_not[ii],
										_area_domain_ii,
										_label_en_type_ii,
										'en'::varchar,
										_etl_id -- this array is supplemented by (IDs input + IDs label types are equal)
										)
									)
							,w2 as	(
									select * from @extschema@.c_area_domain_category as cadc
									where cadc.area_domain in (select w1.etl_id from w1)
									)
							,w3 as	(
									select w2.area_domain, count(w2.*) as pocet from w2
									group by w2.area_domain
									)
							select count(w3.*) from w3 where w3.pocet = _count_category_label_en_source_type_equal_not
							into _check_count_categories_type_equal_not;
						
							if _check_count_categories_type_equal_not = 0
							then
								-- ID can move to auto transfer
								_res_id_transfer := _res_id_transfer || _res_id_manually_type_equal_not[ii]; -- this manually ID is moved to auto transfer
								_res_id_manually_type_equal_not_final := array_remove(_res_id_manually_type_equal_not_final,_res_id_manually_type_equal_not[ii]); -- this manually ID is removed from manually pairing equal type not							
							else
								-- ID stay in manually pairing
								_res_id_manually_type_equal_not_final := _res_id_manually_type_equal_not_final;
							end if;
				
						end if;																
					end if;
				end loop;
			end if;
		
			_res_id_manually := _res_id_manually_type_equal || _res_id_manually_type_equal_not_final;

			
			-- check MANUALLY and PAIR
			with
			w1 as	(select unnest(_res_id_manually) as id_manually)
			,w2 as	(select unnest(_res_id_pair) as id_pair)	
			,w3 as	(select w1.* from w1 where w1.id_manually is distinct from 0)
			,w4 as	(select w2.* from w2 where w2.id_pair is distinct from 0)
			,w5 as	(select count(w3.*) as res from w3 where w3.id_manually in (select w4.id_pair from w4))
			,w6 as	(select count(w4.*) as res from w4 where w4.id_pair in (select w3.id_manually from w3))
			select sum(t.res) from (select w5.res from w5 union all	select w6.res from w6) as t
			into _check_manually_pair;

			if _check_manually_pair is distinct from 0
			then
				raise exception 'Error 03: fn_etl_check_area_domains4etl_json: There must not exist situation auto pair and manually pair for the sub population type at the same time!';
			end if;

			-- check MANUALLY and TRANFER
			with
			w1 as	(select unnest(_res_id_manually) as id_manually)
			,w2 as	(select unnest(_res_id_transfer) as id_transfer)	
			,w3 as	(select w1.* from w1 where w1.id_manually is distinct from 0)
			,w4 as	(select w2.* from w2 where w2.id_transfer is distinct from 0)
			,w5 as	(select count(w3.*) as res from w3 where w3.id_manually in (select w4.id_transfer from w4))
			,w6 as	(select count(w4.*) as res from w4 where w4.id_transfer in (select w3.id_manually from w3))
			select sum(t.res) from (select w5.res from w5 union all	select w6.res from w6) as t
			into _check_manually_transfer;

			if _check_manually_transfer is distinct from 0
			then
				raise exception 'Error 04: fn_etl_check_area_domains4etl_json: There must not exist situation auto transfer and manually pair for the sub population type at the same time!';
			end if;	

			-- check PAIR and TRANFER
			with
			w1 as	(select unnest(_res_id_pair) as id_pair)
			,w2 as	(select unnest(_res_id_transfer) as id_transfer)	
			,w3 as	(select w1.* from w1 where w1.id_pair is distinct from 0)
			,w4 as	(select w2.* from w2 where w2.id_transfer is distinct from 0)
			,w5 as	(select count(w3.*) as res from w3 where w3.id_pair in (select w4.id_transfer from w4))
			,w6 as	(select count(w4.*) as res from w4 where w4.id_transfer in (select w3.id_pair from w3))
			select sum(t.res) from (select w5.res from w5 union all	select w6.res from w6) as t
			into _check_pair_transfer;

			if _check_pair_transfer is distinct from 0
			then
				raise exception 'Error 05: fn_etl_check_area_domains4etl_json: There must not exist situation auto transfer and auto pair for the sub population type at the same time!';
			end if;					

			with
			w1 as	(select unnest(_array_id) as id)
			,w2 as	(select unnest(_res_id_pair) as id_pair)
			,w3 as	(select unnest(_res_id_transfer) as id_transfer)
			,w4 as	(select w2.* from w2 where w2.id_pair is distinct from 0)
			,w5 as	(select w3.* from w3 where w3.id_transfer is distinct from 0)
			,w6 as	(
					select
							w1.id,
							case when w5.id_transfer is null then false else true end as auto_transfer,
							case when w4.id_pair is null then false else true end as auto_pair
					from
							w1
							left join w5 on w1.id = w5.id_transfer
							left join w4 on w1.id = w4.id_pair
					order
							by w1.id
					)
			select json_agg(json_build_object('id',w6.id,'auto_transfer',w6.auto_transfer,'auto_pair',w6.auto_pair)) from w6
			into _res;

		end if;
	
		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_check_area_domains4etl_json(json, integer[]) is
'The function returns one record of area domain if input label of area domain was paired in target DB or the function returns record(s) of area domains that are possible to pair in target DB. Output is in JSON format.';

grant execute on function @extschema@.fn_etl_check_area_domains4etl_json(json, integer[]) to public;
-- </function>