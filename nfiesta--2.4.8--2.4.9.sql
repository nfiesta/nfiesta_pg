--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

REVOKE ALL ON ALL FUNCTIONS IN SCHEMA @extschema@ FROM PUBLIC;
REVOKE ALL ON ALL SEQUENCES IN SCHEMA @extschema@ FROM PUBLIC;
REVOKE ALL ON ALL TABLES IN SCHEMA @extschema@ FROM PUBLIC;

GRANT USAGE ON SCHEMA @extschema@ TO PUBLIC;
GRANT SELECT ON ALL TABLES IN SCHEMA @extschema@ TO PUBLIC;
GRANT SELECT, USAGE ON ALL SEQUENCES IN SCHEMA @extschema@ TO PUBLIC;
GRANT EXECUTE ON ALL FUNCTIONS IN SCHEMA @extschema@ TO PUBLIC; -- function can not update or insert data on tables where user has no permissions to do that

--GRANT INSERT UPDATE DELETE on all TABLES and SEQUENCES with DYNAMIC DATA to preferably group role is managed by DB administrator (possibly through script (see privileges.sql).

