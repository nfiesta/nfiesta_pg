-- Copyright 2017, 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.

-- renaming function fn_api_get_1pgroups4regtotal to fn_api_get_1pgroups4gregmap and change of its logic and output (the new version works by strata-combinations).
DROP FUNCTION nfiesta.fn_api_get_1pgroups4regtotal(INT, INT[], INT[]);
-- <function name="nfiesta.fn_api_get_1pgroups4gregmap" schema="extschema" src="functions/extschema/configuration/fn_api_get_1pgroups4gregmap.sql">
-- Copyright 2017, 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: nfiesta.fn_api_get_1pgroups4gregmap(INT, INT[], INT[])
-- DROP FUNCTION nfiesta.fn_api_get_1pgroups4gregmap(INT, INT[], INT[]);

CREATE OR REPLACE FUNCTION nfiesta.fn_api_get_1pgroups4gregmap(_estimation_period INT, _estimation_cells INT[], _variables INT[])
RETURNS TABLE(
	estimation_period_id INT, 
	estimation_period_label VARCHAR(200), 
	estimation_period_label_en VARCHAR(200), 
	estimation_period_description TEXT, 
	estimation_period_description_en TEXT,
	country_id INT[],
	country_label VARCHAR(200)[],
	country_label_en VARCHAR(200)[],
	country_description TEXT[],
	country_description_en TEXT[],
	strata_set_id INT[],
	strata_set_label VARCHAR(200)[],
	strata_set_label_en VARCHAR(200)[],
	strata_set_description TEXT[],
	strata_set_description_en TEXT[],
	stratum_id INT[],
	stratum_label VARCHAR(200)[],
	stratum_label_en VARCHAR(200)[],
	stratum_description TEXT[],
	stratum_description_en TEXT[],
	panel_refyearset_group_id INT, 
	panel_refyearset_group_label VARCHAR(200),
	panel_refyearset_group_label_en VARCHAR(200),
	panel_refyearset_group_description TEXT,
	panel_refyearset_group_description_en TEXT,
	complete_group BOOLEAN)
AS
$function$
BEGIN

-- testing input arguments if not NULL
IF _estimation_period IS NULL THEN
	RAISE EXCEPTION 'fn_api_get_1pgroups4gregmap: Function argument _estimation_period INT must not be NULL!';
END IF;

IF _estimation_cells IS NULL THEN
	RAISE EXCEPTION 'fn_api_get_1pgroups4gregmap: Function argument _estimation_cells INT[] must not be NULL!';
END IF;

IF _variables IS NULL THEN
	RAISE EXCEPTION 'fn_api_get_1pgroups4gregmap: Function argument _variables INT[] must not be NULL!';
END IF;

-- checking input arrays if they do not contain NULL
IF (SELECT array_position(_estimation_cells, NULL) IS NOT NULL) THEN
	RAISE EXCEPTION 'fn_api_get_1pgroups4gregmap: Function argument _estimation_cells INT[] must not be an array containing NULL!';
END IF;

IF (SELECT array_position(_variables, NULL) IS NOT NULL) THEN
	RAISE EXCEPTION 'fn_api_get_1pgroups4gregmap: Function argument _variables INT[] must not be an array containing NULL!';
END IF;

-- checking one cell collection within input argument _estimation_cells 
IF (SELECT count(DISTINCT estimation_cell_collection) FROM nfiesta.c_estimation_cell WHERE ARRAY[id] <@ _estimation_cells) IS DISTINCT FROM 1 THEN 
	RAISE EXCEPTION 'fn_api_get_1pgroups4gregmap: Function argument _estimation_cells INT[] must contain cells corresponding to one and the only estimation cell collection!';
END IF;

RETURN QUERY EXECUTE '
WITH w_reg2conf AS MATERIALIZED (
	SELECT DISTINCT -- distinct filters duplicate records for cells intersectiong more strata (we need to follow strata combinations only) 
		$1 AS estimation_period,
		array_agg(t3.stratum) OVER (PARTITION BY t1.estimation_cell, t2.variable) AS strata_combination, -- partition does not include estimation_period because there is only one, and it include variable to avoid duplicates
		t1.estimation_cell,
		t2.variable
	FROM
	  (SELECT unnest($2) AS estimation_cell) AS t1 
	 CROSS JOIN
	  (SELECT unnest($3) AS variable) AS t2
	 INNER JOIN
	  nfiesta.t_stratum_in_estimation_cell AS t3
	 ON t1.estimation_cell = t3.estimation_cell
), w_no_desired_reg_estimates AS MATERIALIZED (
SELECT
	estimation_period, 
	strata_combination,
	count(*)  AS no_of_desired_reg_estimates
FROM w_reg2conf
GROUP BY estimation_period, strata_combination
), w_conf1p AS MATERIALIZED (
SELECT 
	t1.*,
	t3.panel_refyearset_group,
	t2.no_of_desired_reg_estimates,
	count(*) FILTER (WHERE t3.panel_refyearset_group IS NOT NULL) OVER (PARTITION BY t1.strata_combination, t3.panel_refyearset_group) AS no_configured_1pestimates_per_group
FROM 
	w_reg2conf AS t1
INNER JOIN 
	w_no_desired_reg_estimates AS t2
ON t1.strata_combination = t2.strata_combination
LEFT JOIN nfiesta.t_total_estimate_conf  AS t3
	ON 
	t1.estimation_period = t3.estimation_period AND
	t1.estimation_cell = t3.estimation_cell AND
	t1.variable = t3.variable AND
	t3.phase_estimate_type = 1
), w_conf1p_agg AS MATERIALIZED ( 
SELECT DISTINCT
	estimation_period,
	strata_combination,
	panel_refyearset_group,
	no_of_desired_reg_estimates = no_configured_1pestimates_per_group AS complete_group
FROM 
	w_conf1p
WHERE panel_refyearset_group IS NOT NULL
-- the above could be the final query, but we need to add records for strata with NULL in panel_refyearset_group in case there are no 1p configurations 
-- for any cell and for any variable either, so the WHERE condition filter such records out
-- at the same time the WHERE is needed in order to avoid duplicate records (one with NULL group, the other NOT NULL but still incomplete) for groups with
-- partial configuration for some cells or for some variables only
)
SELECT DISTINCT
-- here by left JOIN we get records for all strata_cominations no matter if 1p configurations exists for the corresponding cells and variables, 
--potentially with NULL groups (no 1p configs at all for any cell and any desired variable within the given stratum)
	t3.id AS estimation_period_id,
	t3.label::VARCHAR(200) AS estimation_period_label,
	coalesce(t3.label_en, t3.label)::VARCHAR(200) AS estimation_period_label_en,
    t3.description AS estimation_period_description,
	coalesce(t3.description_en, t3.description) AS estimation_period_description_en,
	array_agg(t6.id) OVER (PARTITION BY t1.strata_combination) AS country_id,
	array_agg(t6.label::VARCHAR(200)) OVER (PARTITION BY t1.strata_combination) AS country_label,
	array_agg(t6.label_en::VARCHAR(200)) OVER (PARTITION BY t1.strata_combination) AS country_label_en,
	array_agg(t6.description) OVER (PARTITION BY t1.strata_combination) AS country_description,
	array_agg(t6.description_en) OVER (PARTITION BY t1.strata_combination) AS country_description_en,
	array_agg(t5.id) OVER (PARTITION BY t1.strata_combination) AS strata_set_id,
	array_agg(t5.strata_set::VARCHAR(200)) OVER (PARTITION BY t1.strata_combination) AS strata_set_label,
	array_agg(t5.strata_set::VARCHAR(200)) OVER (PARTITION BY t1.strata_combination) AS strata_set_label_en,
	array_agg(t5.label::TEXT) OVER (PARTITION BY t1.strata_combination) AS strata_set_description,
	array_agg(t5.label::TEXT) OVER (PARTITION BY t1.strata_combination) AS strata_set_description_en,
	array_agg(t1.stratum) OVER (PARTITION BY t1.strata_combination) AS stratum,
	array_agg(t4.stratum::VARCHAR(200)) OVER (PARTITION BY t1.strata_combination) AS stratum_label,
	array_agg(t4.stratum::VARCHAR(200)) OVER (PARTITION BY t1.strata_combination) AS stratum_label_en,
	array_agg(t4.label::TEXT) OVER (PARTITION BY t1.strata_combination) AS stratum_description,
	array_agg(t4.label::TEXT) OVER (PARTITION BY t1.strata_combination) AS stratum_description_en,
	t7.id AS panel_refyearset_group_id,
	t7.label AS panel_refyearset_group_label,
	coalesce(t7.label_en, t7.label)  AS panel_refyearset_group_label_en,
	t7.description AS panel_refyearset_group_description,
	coalesce(t7.description_en, t7.description) AS panel_refyearset_group_description_en,
	t2.complete_group
FROM 
	(SELECT estimation_period, strata_combination, unnest(strata_combination) AS stratum FROM w_no_desired_reg_estimates) AS t1 -- the unnest is necessary to get all arrays in the same order
LEFT JOIN
	w_conf1p_agg AS t2
	ON t1.strata_combination = t2.strata_combination
INNER JOIN 
	nfiesta.c_estimation_period AS t3
	ON t1.estimation_period = t3.id
INNER JOIN 
	sdesign.t_stratum AS t4
	ON t1.strata_combination <@ ARRAY[t4.id]
INNER JOIN 
	sdesign.t_strata_set AS t5
	ON t4.strata_set = t5.id
INNER JOIN 
	sdesign.c_country AS t6
	ON t5.country = t6.id
LEFT JOIN 
	nfiesta.c_panel_refyearset_group AS t7
	ON t2.panel_refyearset_group = t7.id;' USING _estimation_period, _estimation_cells, _variables;
END;
$function$
LANGUAGE plpgsql
PARALLEL SAFE;

COMMENT ON FUNCTION  nfiesta.fn_api_get_1pgroups4gregmap(INT, INT[], INT[]) IS 
'Function returns list of strata combinations found among the set of estimation cells '
'and panel refyearset groups for which single-phase estimates of total have already '
'been configured, together with a Bollean indicator saying whether the existing '
'single-phase configurations using the given group cover all desired regression estimates '
'considering a subset of cells for estimation intersecting (not just touching) any ' 
'stratum in the combination and considering the list of variables.';

/* tests
-- test NULL for _estimation_period argument
SELECT * FROM nfiesta.fn_api_get_1pgroups4gregmap(NULL, ARRAY[44,45,46,47,48,49,50,51,52,53,56,57,58,62], ARRAY[1,2,3]);

-- test NULL for _estimation_cell argument
SELECT * FROM nfiesta.fn_api_get_1pgroups4gregmap(1, NULL, ARRAY[1,2,3]);

-- test NULL for _variables argument
SELECT * FROM nfiesta.fn_api_get_1pgroups4gregmap(1, ARRAY[44,45,46,47,48,49,50,51,52,53,56,57,58,62], NULL);

-- test for NULL as an element of _estimation_cell argument
SELECT * FROM nfiesta.fn_api_get_1pgroups4gregmap(1, ARRAY[44,45,46,47,NULL,49,50,51,52,53,56,57,58,62], ARRAY[1,2,3]);
SELECT * FROM nfiesta.fn_api_get_1pgroups4gregmap(1, ARRAY[NULL], ARRAY[1,2,3]);
SELECT * FROM nfiesta.fn_api_get_1pgroups4gregmap(1, ARRAY[NULL]::int[], ARRAY[1,2,3]);

-- test for NULL as an element of _variables argument
SELECT * FROM nfiesta.fn_api_get_1pgroups4gregmap(1, ARRAY[44,45,46,47,48,49,50,51,52,53,56,57,58,62], ARRAY[NULL,2,NULL]);
SELECT * FROM nfiesta.fn_api_get_1pgroups4gregmap(1, ARRAY[44,45,46,47,48,49,50,51,52,53,56,57,58,62], ARRAY[NULL]);
SELECT * FROM nfiesta.fn_api_get_1pgroups4gregmap(1, ARRAY[44,45,46,47,48,49,50,51,52,53,56,57,58,62], ARRAY[NULL]::int[]);

-- test if estimation cells belong to one estimation cell collection (adding 1 to the valid array _estimation_cells, 1 
-- belongs to another collection but the same stratum
SELECT * FROM nfiesta.fn_api_get_1pgroups4gregmap(1, ARRAY[1,44,45,46,47,48,49,50,51,52,53,56,57,58,62], ARRAY[1,2,3]);

-- tests on valid input
-- cells of one stratum three variables, complete group
SELECT * FROM nfiesta.fn_api_get_1pgroups4gregmap(1, ARRAY[44,45,46,47,48,49,50,51,52,53,56,57,58,62], ARRAY[1,2,3]);

-- cells of one stratum, one variable without configuration added to the former three making the group incomplete
SELECT * FROM nfiesta.fn_api_get_1pgroups4gregmap(1, ARRAY[44,45,46,47,48,49,50,51,52,53,56,57,58,62], ARRAY[1,2,3,-1]);

-- cells of one stratum, one variable without configuration, no group given (NULL)
SELECT * FROM nfiesta.fn_api_get_1pgroups4gregmap(1, ARRAY[44,45,46,47,48,49,50,51,52,53,56,57,58,62], ARRAY[-1]);
*/
-- </function>

-- renaming function fn_api_save_greg_map_total_config to fn_api_save_greg_maptotal_config
DROP FUNCTION nfiesta.fn_api_save_greg_map_total_config(INT, INT, INT, INT, INT, BOOLEAN, INT, BOOLEAN);
-- <function name="nfiesta.fn_api_save_gregmap_total_config(INT, INT, INT, INT, INT, BOOLEAN, INT, BOOLEAN)" schema="extschema" src="functions/extschema/configuration/fn_api_save_gregmap_total_config.sql">
--
-- Copyright 2017, 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.

-- Function: nfiesta.fn_api_save_gregmap_total_config(INT, INT, INT, INT, INT, BOOLEAN, INT, BOOLEAN)
-- DROP FUNCTION nfiesta.fn_api_save_gregmap_total_config(INT, INT, INT, INT, INT, BOOLEAN, INT, BOOLEAN);

CREATE OR REPLACE FUNCTION nfiesta.fn_api_save_gregmap_total_config(_estimation_period INT, _estimation_cell INT, _variable INT,  
	_panel_refyearset_group INT, _working_model INT, _sigma BOOLEAN, _param_area_type INT, _force_synthetic BOOLEAN)
RETURNS INT
AS
$function$
DECLARE 
_panels INT[];
_null_refyearsets INT[]; 
_panel_group_aux INT[];
_param_area INT[];
_aux_conf INT[];

BEGIN
-- testing input parameters if not NULL
IF _estimation_period IS NULL THEN
	RAISE EXCEPTION 'fn_api_save_gregmap_total_config: Function argument _estimation_period INT must not be NULL!';
END IF;	
	
IF _estimation_cell IS NULL THEN
	RAISE EXCEPTION 'fn_api_save_gregmap_total_config: Function argument _estimation_cell INT must not be NULL!';
END IF;

IF _variable IS NULL THEN
	RAISE EXCEPTION 'fn_api_save_gregmap_total_config: Function argument _variable INT must not be NULL!';
END IF;

IF _panel_refyearset_group IS NULL THEN
	RAISE EXCEPTION 'fn_api_save_gregmap_total_config: Function argument _panel_refyearset_group INT must not be NULL!';
END IF;

IF _working_model IS NULL THEN
	RAISE EXCEPTION 'fn_api_save_gregmap_total_config: Function argument _working_model INT must not be NULL!';
END IF;

IF _sigma IS NULL THEN
	RAISE EXCEPTION 'fn_api_save_gregmap_total_config: Function argument _sigma BOOLEAN must not be NULL!';
END IF;

IF _param_area_type IS NULL THEN
	RAISE EXCEPTION 'fn_api_save_gregmap_total_config: Function argument _param_area_type INT must not be NULL!';
END IF;

IF _force_synthetic IS NULL THEN
	RAISE EXCEPTION 'fn_api_save_gregmap_total_config: Function argument _force_synthetic BOOLEAN must not be NULL!';
END IF;

-- find out panels of the input _panel_refyearset_group
SELECT array_agg(panel) FROM nfiesta.fn_api_get_panel_refyearset_combinations4groups(ARRAY[_panel_refyearset_group]) INTO _panels; -- panels 1,2

IF _panels IS NULL THEN
	RAISE EXCEPTION 'fn_api_save_gregmap_total_config: No panels found for the chosen group of panels and '
	'reference-year set combinations (function argument _panel_refyearset_group). The configuration process cannot proceed!';
END IF;


-- construct array of NULLs with the length corresponfing to _panels
SELECT 
	array_agg(NULL::int)::int[]
FROM 
	generate_series(1, array_length(_panels,1)) AS a(i)
INTO _null_refyearsets;

-- determine the panel group having the same set of panel but no reference-year sets, this should be found in the matching records of t_aux_conf 
SELECT array_agg(id) FROM nfiesta.fn_api_get_group4panel_refyearset_combinations(_panels, _null_refyearsets) INTO _panel_group_aux; -- panel group 6

IF _panel_group_aux IS NULL THEN
	RAISE EXCEPTION 'fn_api_save_gregmap_total_config: No auxiliary group of panels matching the chosen group of panels and '
	'reference-year set combinations (function argument _panel_refyearset_group) found! The configuration process cannot proceed!';
END IF;

IF array_length(_panel_group_aux,1) > 1 THEN
	RAISE EXCEPTION 'fn_api_save_gregmap_total_config: More than one auxiliary group of panels matching the chosen group of panels and '
	'reference-year set combinations (function argument _panel_refyearset_group) found! The configuration process cannot proceed!';
END IF;

-- determine parameterisation areas beonging to the input _param_area_type and containing the cell  
SELECT 
	array_agg(gid)
FROM 
	nfiesta.f_a_param_area AS t1
INNER JOIN 
	nfiesta.cm_cell2param_area_mapping AS t2
	ON t1.gid = t2.param_area
WHERE 
	t1.param_area_type = _param_area_type AND 
	t2.estimation_cell = _estimation_cell
INTO _param_area;

IF _param_area IS NULL THEN
	RAISE EXCEPTION 'fn_api_save_gregmap_total_config: No parameteristion area matching the combination of estimation cell (_estimation_cell) '
	'and paramatrisation area type (_param_area_type) found! The configuration process cannot proceed!';
END IF;

IF array_length(_param_area,1) > 1 THEN
	RAISE EXCEPTION 'fn_api_save_gregmap_total_config: More than one parameteristion area matching the combination of estimation cell '
	'(_estimation_cell) and paramatrisation area type (_param_area_type) found! The configuration process cannot proceed!';
END IF;

SELECT 
	array_agg(id) 
FROM 
	nfiesta.t_aux_conf AS t1
WHERE 
	sigma = _sigma AND	
	model = _working_model AND 
	panel_refyearset_group = _panel_group_aux[1] AND
	param_area = _param_area[1]
INTO _aux_conf;

IF _aux_conf IS NULL THEN
	RAISE EXCEPTION 'fn_api_save_gregmap_total_config: No matching auxiliary configuration found! The configuration process cannot proceed!';
END IF;

IF array_length(_aux_conf,1) > 1 THEN
	RAISE EXCEPTION 'fn_api_save_gregmap_total_config: More than one matching auxiliary configuration found! The configuration process cannot proceed!';
END IF;

RETURN nfiesta.fn_2p_est_configuration(_estimation_cell, _estimation_period, 'API-GUI configuration', _variable, _aux_conf[1], _force_synthetic);

END;
$function$
LANGUAGE plpgsql
PARALLEL SAFE;

COMMENT ON FUNCTION  nfiesta.fn_api_save_gregmap_total_config(INT, INT, INT, INT, INT, BOOLEAN, INT, BOOLEAN) IS 
'This is a wrapper API function saving the configuration of GREG estimates of total that use auxiliary data in the form of maps.';

/* tests
-------------------------------------------------------------------------------------------------------------------------------------------------------
-- tests for function nfiesta.fn_api_save_gregmap_total_config(_estimation_period INT, _estimation_cell INT, _variable INT,  
--	_panel_refyearset_group INT, _working_model INT, _sigma BOOLEAN, _param_area_type INT, _force_synthetic BOOLEAN)
-------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------- testing invalid inputs ------------------------------------------------------------------------------
-- test NULL for _estimation_period argument
SELECT * FROM nfiesta.fn_api_save_gregmap_total_config(NULL,55,1,19,1,FALSE,3,FALSE);

-- test NULL for _estimation_cell argument
SELECT * FROM nfiesta.fn_api_save_gregmap_total_config(1,NULL,1,19,1,FALSE,3,FALSE);

-- test NULL for _variable argument
SELECT * FROM nfiesta.fn_api_save_gregmap_total_config(1,55,NULL,19,1,FALSE,3,FALSE);

-- test NULL for _panel_refyearset_group argument
SELECT * FROM nfiesta.fn_api_save_gregmap_total_config(1,55,1,NULL,1,FALSE,3,FALSE);

-- test NULL for _working_model argument
SELECT * FROM nfiesta.fn_api_save_gregmap_total_config(1,55,1,19,NULL,FALSE,3,FALSE);

-- test NULL for _sigma argument
SELECT * FROM nfiesta.fn_api_save_gregmap_total_config(1,55,1,19,1,NULL,3,FALSE);

-- test NULL for _param_area_type argument
SELECT * FROM nfiesta.fn_api_save_gregmap_total_config(1,55,1,19,1,FALSE,NULL,FALSE);

-- test NULL for _force_synthetic argument
SELECT * FROM nfiesta.fn_api_save_gregmap_total_config(1,55,1,19,1,FALSE,3,NULL);

-- test for an invalid group of panel and reference-year set combinations - no panels  found
SELECT * FROM nfiesta.fn_api_save_gregmap_total_config(1,55,1,-1,1,FALSE,3,FALSE);

-- test for a group of panel and reference-year set combinations that has no equivaent in t_aux_conf (with no reference year sets attached)
SELECT * FROM nfiesta.fn_api_save_gregmap_total_config(1,55,1,33,1,FALSE,3,FALSE);

-- test for a parametrisation area not matching the estimation cell
SELECT * FROM nfiesta.fn_api_save_gregmap_total_config(1,55,1,19,1,FALSE,2,FALSE);

-- test for missing aux configuration - no one has sigma TRUE
SELECT * FROM nfiesta.fn_api_save_gregmap_total_config(1,55,1,19,1,TRUE,3,FALSE);

------------------------------------------------ testing valid inputs ---------------------------------------------------------------------------------
-- save a configuration with force_synthetic = FALSE  
SELECT * FROM nfiesta.fn_api_save_gregmap_total_config(1,55,1,19,1,FALSE,3,FALSE);

-- save a configuration with force_synthetic = TRUE  
SELECT * FROM nfiesta.fn_api_save_gregmap_total_config(1,55,1,19,1,FALSE,3,TRUE);


-- helping code
-- aux_conf corresponding to group 19
SELECT * FROM nfiesta.fn_api_get_wmodels_sigma_paramtypes(ARRAY[1,2], ARRAY[55,56,60,61,67,68,69,70,75,76,77,83]); -- model 1, sigma FALSE, param area type 3

_estimation_cell = 55,
_panel_refyearset_group = 19
_working_model = 1
_sigma = FALSE
_param_area_type = 3
_param_area = 28
_estimation_period = 1,
_variable = 1
aux_panel_refyearset_group = 6

SELECT * FROM 
 (SELECT panel_refyearset_group, array_agg(panel ORDER BY panel) AS p, array_agg(reference_year_set) FROM nfiesta.t_panel_refyearset_group WHERE reference_year_set IS NOT NULL GROUP BY panel_refyearset_group) AS t1  
LEFT JOIN 
 (SELECT panel_refyearset_group, array_agg(panel ORDER BY panel) AS p, array_agg(reference_year_set) FROM nfiesta.t_panel_refyearset_group WHERE reference_year_set IS NULL GROUP BY panel_refyearset_group) AS t2
ON t1.p = t2.p

*/

-- </function>

-- new function to choose from possible configuration of GREG-map to single-phase ratio
-- <function name="nfiesta.fn_api_save_fn_api_get_gregmap2single_configs4ratio(INT, INT[], INT[], INT[])" schema="extschema" src="functions/extschema/configuration/fn_api_get_gregmap2single_configs4ratio.sql">
-- Copyright 2017, 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: nfiesta.fn_api_get_gregmap2single_configs4ratio(INT, INT[], INT[], INT[])
-- DROP FUNCTION nfiesta.fn_api_get_gregmap2single_configs4ratio(INT, INT[], INT[], INT[]);

CREATE OR REPLACE FUNCTION nfiesta.fn_api_get_gregmap2single_configs4ratio(_estimation_period INT, _estimation_cells INT[], _variables_nom INT[], _variables_denom INT[])
RETURNS TABLE(
 			panel_refyearset_group_id INT,
			panel_refyearset_group_label VARCHAR(200),  
			panel_refyearset_group_label_en VARCHAR(200),
			panel_refyearset_group_description TEXT,
			panel_refyearset_group_description_en TEXT,
			nominator_param_area_type_id INT,
			nominator_param_area_type_label VARCHAR(200),
			nominator_param_area_type_label_en VARCHAR(200),
			nominator_param_area_type_description TEXT,
			nominator_param_area_type_description_en TEXT,
			nominator_force_synthetic BOOLEAN, -- always FALSE, because single-phase nominator is always design-based and we want to avoid mixed inference
			nominator_model_id INT, 
			nominator_model_label VARCHAR(200),
			nominator_model_label_en VARCHAR(200),
			nominator_model_description TEXT,
			nominator_model_description_en TEXT,
			nominator_model_sigma BOOLEAN, 
			all_estimates_configurable BOOLEAN,
			total_estimate_conf_nominator INT[],
			total_estimate_conf_denominator INT[]
	)
AS
$function$
BEGIN

-- testing input arguments if not NULL
IF _estimation_period IS NULL THEN
	RAISE EXCEPTION 'fn_api_get_gregmap2single_configs4ratio: Function argument _estimation_period INT must not be NULL!';
END IF;

IF _estimation_cells IS NULL THEN
	RAISE EXCEPTION 'fn_api_get_gregmap2single_configs4ratio: Function argument _estimation_cells INT[] must not be NULL!';
END IF;

IF _variables_nom IS NULL THEN
	RAISE EXCEPTION 'fn_api_get_gregmap2single_configs4ratio: Function argument _variables_nom INT[] must not be NULL!';
END IF;

IF _variables_denom IS NULL THEN
	RAISE EXCEPTION 'fn_api_get_gregmap2single_configs4ratio: Function argument _variables_denom INT[] must not be NULL!';
END IF;

-- checking input arrays if they do not contain NULL
IF (SELECT array_position(_estimation_cells, NULL) IS NOT NULL) THEN
	RAISE EXCEPTION 'fn_api_get_gregmap2single_configs4ratio: Function argument _estimation_cells INT[] must not be an array containing NULL!';
END IF;

IF (SELECT array_position(_variables_nom, NULL) IS NOT NULL) THEN
	RAISE EXCEPTION 'fn_api_get_gregmap2single_configs4ratio: Function argument _variables_nom INT[] must not be an array containing NULL!';
END IF;

IF (SELECT array_position(_variables_denom, NULL) IS NOT NULL) THEN
	RAISE EXCEPTION 'fn_api_get_gregmap2single_configs4ratio: Function argument _variables_denom INT[] must not be an array containing NULL!';
END IF;

-- checking input arrays if they do not contain NULL
IF (SELECT array_position(_estimation_cells, NULL) IS NOT NULL) THEN
	RAISE EXCEPTION 'fn_api_get_gregmap2single_configs4ratio: Function argument _estimation_cells INT[] must not be an array containing NULL!';
END IF; 

IF (SELECT array_position(_variables_nom, NULL) IS NOT NULL) THEN
	RAISE EXCEPTION 'fn_api_get_gregmap2single_configs4ratio: Function argument _variables_nom INT[] must not be an array containing NULL!';
END IF; 

IF (SELECT array_position(_variables_denom, NULL) IS NOT NULL) THEN
	RAISE EXCEPTION 'fn_api_get_gregmap2single_configs4ratio: Function argument _variables_denom INT[] must not be an array containing NULL!';
END IF; 

-- check equality of lengths of _variables_nom and _variables_denom
IF (SELECT array_length(_variables_nom, 1) != array_length(_variables_denom, 1)) THEN
	RAISE EXCEPTION 'fn_api_get_gregmap2single_configs4ratio: Function arguments _variables_nom INT[] and _variables_denom INT[] must be arrays of the same lengths!';
END IF; 

RETURN QUERY EXECUTE '
	WITH w_vars AS MATERIALIZED ( -- combinations of variables in nominator and denominator
		SELECT 
			vnom.vid AS variable_nominator, 
			vdenom.vid AS variable_denominator
		FROM 
			unnest($3) WITH ORDINALITY vnom (vid,ord) 
		FULL JOIN 
			unnest($4) WITH ORDINALITY vdenom (vid, ord) 
		ON vnom.ord = vdenom.ord
	), w_greg_map_totals_conf AS MATERIALIZED (
		SELECT 
			array_agg(t2.id) OVER (PARTITION BY t1.panel_refyearset_group, t1.force_synthetic, t5.param_area_type, t4.model, t4.sigma) AS total_estimate_conf_nominator,
			array_agg(t1.id) OVER (PARTITION BY t1.panel_refyearset_group, t1.force_synthetic, t5.param_area_type, t4.model, t4.sigma) AS total_estimate_conf_denominator,
			t1.estimation_period,
			t1.estimation_cell,
			t1.panel_refyearset_group,
			t8.label AS panel_refyearset_group_label,
			t8.label_en AS panel_refyearset_group_label_en,
			t8.description AS panel_refyearset_group_description,
			t8.description_en AS panel_refyearset_group_description_en,
			t1.variable AS nominator_variable,
			t2.variable AS denominator_variable,
			t5.gid AS param_area,
			t5.label AS param_area_label,
			t6.id AS param_area_type,
			t6.label AS param_area_type_label,
			t6.label_en AS param_area_type_label_en,
			t6.description AS param_area_type_description,
			t6.description_en AS param_area_type_description_en,
			t7.id AS model,
			t7.label AS model_label,
			t7.label_en AS model_label_en,
			t7.description AS model_description,
			t7.description_en AS model_description_en,
			t4.sigma AS model_sigma,
			count(*) OVER (PARTITION BY t1.panel_refyearset_group, t1.force_synthetic, t5.param_area_type, t4.model, t4.sigma) AS n_configuarations,
			count(*) OVER (PARTITION BY t1.panel_refyearset_group, t1.force_synthetic, t5.param_area_type, t4.model, t4.sigma) = array_length($2,1) * array_length($3,1) AS all_estimates_configurable
		FROM
			nfiesta.t_total_estimate_conf AS t1 -- look up  single-phase total configs for the ratio
		INNER JOIN
			nfiesta.t_total_estimate_conf AS t2 -- look up GREG-map total configs for the ratio
		ON 
			t1.estimation_period = $1 AND t2.estimation_period = $1  AND
			ARRAY[t1.estimation_cell] <@ $2 AND
			ARRAY[t2.estimation_cell] <@ $2 AND
			ARRAY[t1.variable] <@ $4 AND -- single-phase total must have configs for variables listed for denominator
			ARRAY[t2.variable] <@ $3 AND -- GREG-map total must have configs for variables listed for nominator
			t1.phase_estimate_type = 1 AND t2.phase_estimate_type = 2 AND -- the required ratio is a combination of single-phase and GREG-map totals
			t1.estimation_cell = t2.estimation_cell AND 
			t1.panel_refyearset_group = t2.panel_refyearset_group AND -- group of panels and reference year sets must match between nom and denom
			NOT t2.force_synthetic -- combination with single-phase (always design-based) is only possible with design-based GREG-map estimators 
		INNER JOIN 
			w_vars AS t3
		ON 
			t1.variable = t3.variable_denominator AND
			t2.variable = t3.variable_nominator
		INNER JOIN 
			nfiesta.t_aux_conf AS t4 
			ON t2.aux_conf = t4.id
		INNER JOIN 
			nfiesta.f_a_param_area AS t5
			ON t4.param_area = t5.gid
		INNER JOIN 
			nfiesta.c_param_area_type AS t6
			ON t5.param_area_type = t6.id
		INNER JOIN 
			nfiesta.t_model AS t7 
			ON t4.model = t7.id
		 INNER JOIN 
		 nfiesta.c_panel_refyearset_group AS t8
		 ON t1.panel_refyearset_group = t8.id
	)
	SELECT DISTINCT 
			panel_refyearset_group AS panel_refyearset_group_id,
			panel_refyearset_group_label,  
			panel_refyearset_group_label_en,
			panel_refyearset_group_description,
			panel_refyearset_group_description_en,
			param_area_type AS param_area_type_id,
			param_area_type_label,
			param_area_type_label_en,
			param_area_type_description,
			param_area_type_description_en,
			FALSE AS force_synthetic,
			model AS denominator_model_id, 
			model_label AS denominator_model_label,
			model_label_en AS denominator_model_label_en,
			model_description AS denominator_model_description,
			model_description_en AS denominator_model_description_en,
			model_sigma AS denominator_model_sigma,
			all_estimates_configurable,
			total_estimate_conf_nominator,
			total_estimate_conf_denominator
	FROM 
		 w_greg_map_totals_conf' USING _estimation_period, _estimation_cells, _variables_nom, _variables_denom;
END;
$function$
LANGUAGE plpgsql
PARALLEL SAFE;

COMMENT ON FUNCTION  nfiesta.fn_api_get_gregmap2single_configs4ratio(INT, INT[], INT[], INT[]) IS 
'For input estimation period, estimation cells, nominator and denominator variables '
'the function returns combinations of panel-refyearset group, param. area type, force synthetic '
'(allways FALSE due to the design-based single-phase total in the denominator), model and sigma '
'of already configured single-phase (denominator) and matching GREG-map (nominator) totals. The '
'parameter all_estimates_configurable is TRUE if the corresponding ratio can be configured for '
'all input combinations of estimation cell, nominator variable and denominator variable.';

/* tests
------------------------------------------------- testing invalid inputs ------------------------------------------------------------------------------
-- test NULL for _estimation_period argument
SELECT * FROM nfiesta.fn_api_get_gregmap2single_configs4ratio(NULL, ARRAY[100], ARRAY[1,2,3], ARRAY[3,3,3]);

-- test NULL for _estimation_cell argument
SELECT * FROM nfiesta.fn_api_get_gregmap2single_configs4ratio(1, NULL, ARRAY[1,2,3], ARRAY[3,3,3]);

-- test NULL for _variables_nom argument
SELECT * FROM nfiesta.fn_api_get_gregmap2single_configs4ratio(1, ARRAY[100], NULL, ARRAY[3,3,3]);

-- test NULL for _variables_denom argument
SELECT * FROM nfiesta.fn_api_get_gregmap2single_configs4ratio(1, ARRAY[100], ARRAY[1,2,3], NULL);

-- test for NULL as an element of _estimation_cell argument
SELECT * FROM nfiesta.fn_api_get_gregmap2single_configs4ratio(1, ARRAY[100, NULL], ARRAY[1,2,3], ARRAY[3,3,3]);
SELECT * FROM nfiesta.fn_api_get_gregmap2single_configs4ratio(1, ARRAY[NULL]::int[], ARRAY[1,2,3], ARRAY[3,3,3]);

-- test for NULL as an element of _variables_nom argument
SELECT * FROM nfiesta.fn_api_get_gregmap2single_configs4ratio(1, ARRAY[100], ARRAY[1,NULL,3], ARRAY[3,3,3]);
SELECT * FROM nfiesta.fn_api_get_gregmap2single_configs4ratio(1, ARRAY[100], ARRAY[NULL]::int[], ARRAY[3,3,3]);

-- test for NULL as an element of _variables_denom argument
SELECT * FROM nfiesta.fn_api_get_gregmap2single_configs4ratio(1, ARRAY[100], ARRAY[1,2,3], ARRAY[3,3,NULL]);
SELECT * FROM nfiesta.fn_api_get_gregmap2single_configs4ratio(1, ARRAY[100], ARRAY[1,2,3]::int[], ARRAY[NULL]::int[]);

-- test if _variables_nom and _variables_denom have equal lengths
SELECT FROM nfiesta.fn_api_get_gregmap2single_configs4ratio(1, ARRAY[100], ARRAY[1,2,3], ARRAY[3,3]);

------------------------------------------------ testing valid inputs ---------------------------------------------------------------------------------
-- one uncomplete config, INSPIRE 50x50 km param area type, common panel refyearset group (19) exists for cells 55 and 69, but not including cell 57
-- the other uncomplete config for using strata set as param area
SELECT * FROM nfiesta.fn_api_get_gregmap2single_configs4ratio(1, ARRAY[55,57,69], ARRAY[1,2,3], ARRAY[3,3,3]);

-- one complete and one uncomplete config is obtained if cell 57 is dropped from the above input
SELECT * FROM nfiesta.fn_api_get_gregmap2single_configs4ratio(1, ARRAY[55,69], ARRAY[1,2,3], ARRAY[3,3,3]);

-- two complete configs, two param area types: INSPIRE 50x50 km and the whole strata set 
SELECT * FROM nfiesta.fn_api_get_gregmap2single_configs4ratio(1, ARRAY[100], ARRAY[1,2,3], ARRAY[3,3,3]);

------------------------------------------------ helping code ---------------------------------------------------------------------------------
-- cells 55 and 69 use the same panel refyearset group, but a different one from cell 57, therefore there is no config covering all estimation cells
SELECT estimation_cell, panel_refyearset_group FROM nfiesta.t_total_estimate_conf 
WHERE phase_estimate_type = 1 AND ARRAY[estimation_cell] <@  ARRAY[55,57,69] AND ARRAY[variable] <@ ARRAY[3];

-- all three cells have a configuration using the same common panel refyearset group for 50km param area type,
-- cell 55, but not the other two, has a configuration for the same panel refyearset group and the param area type 
-- correspondong to the whole strata set, herefore not all cells can be configured with this param area type  (note the the second test call using valid inputs)
SELECT * FROM nfiesta.t_total_estimate_conf 
WHERE phase_estimate_type = 2 AND ARRAY[estimation_cell] <@  ARRAY[55,57,69] AND ARRAY[variable] <@ ARRAY[1,2,3] AND NOT force_synthetic;

-- for estimatin cell 100 and variable 3 there is one single phase configuration using panel refyearset group no 25
SELECT estimation_cell, panel_refyearset_group FROM nfiesta.t_total_estimate_conf 
WHERE phase_estimate_type = 1 AND ARRAY[estimation_cell] <@  ARRAY[100] AND ARRAY[variable] <@ ARRAY[3];

-- for the cell 100 and each of the variables (1,2,3) there are two GREG-map total configs, one with param area type 50km INSPIRE, and the other using
-- the whole strataset as param area type
SELECT * FROM nfiesta.t_total_estimate_conf 
WHERE phase_estimate_type = 2 AND ARRAY[estimation_cell] <@  ARRAY[100] AND ARRAY[variable] <@ ARRAY[1,2,3] AND NOT force_synthetic;

*/

-- </function>

-- new function to choose from possible configurations of single-pase to GREG-map ratio
-- <function name="nfiesta.fn_api_save_fn_api_get_single2gregmap_configs4ratio(INT, INT[], INT[], INT[])" schema="extschema" src="functions/extschema/configuration/fn_api_get_single2gregmap_configs4ratio.sql">
-- Copyright 2017, 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: nfiesta.fn_api_get_single2gregmap_configs4ratio(INT, INT[], INT[], INT[])
-- DROP FUNCTION nfiesta.fn_api_get_single2gregmap_configs4ratio(INT, INT[], INT[], INT[]);

CREATE OR REPLACE FUNCTION nfiesta.fn_api_get_single2gregmap_configs4ratio(_estimation_period INT, _estimation_cells INT[], _variables_nom INT[], _variables_denom INT[])
RETURNS TABLE(
 			panel_refyearset_group_id INT,
			panel_refyearset_group_label VARCHAR(200),  
			panel_refyearset_group_label_en VARCHAR(200),
			panel_refyearset_group_description TEXT,
			panel_refyearset_group_description_en TEXT,
			denominator_param_area_type_id INT,
			denominator_param_area_type_label VARCHAR(200),
			denominator_param_area_type_label_en VARCHAR(200),
			denominator_param_area_type_description TEXT,
			denominator_param_area_type_description_en TEXT,
			denominator_force_synthetic BOOLEAN, -- always FALSE, because single-phase nominator is always design-based and we want to avoid mixed inference
			denominator_model_id INT, 
			denominator_model_label VARCHAR(200),
			denominator_model_label_en VARCHAR(200),
			denominator_model_description TEXT,
			denominator_model_description_en TEXT,
			denominator_model_sigma BOOLEAN, 
			all_estimates_configurable BOOLEAN,
			total_estimate_conf_nominator INT[],
			total_estimate_conf_denominator INT[]
	)
AS
$function$
BEGIN

-- testing input arguments if not NULL
IF _estimation_period IS NULL THEN
	RAISE EXCEPTION 'fn_api_get_single2gregmap_configs4ratio: Function argument _estimation_period INT must not be NULL!';
END IF;

IF _estimation_cells IS NULL THEN
	RAISE EXCEPTION 'fn_api_get_single2gregmap_configs4ratio: Function argument _estimation_cells INT[] must not be NULL!';
END IF;

IF _variables_nom IS NULL THEN
	RAISE EXCEPTION 'fn_api_get_single2gregmap_configs4ratio: Function argument _variables_nom INT[] must not be NULL!';
END IF;

IF _variables_denom IS NULL THEN
	RAISE EXCEPTION 'fn_api_get_single2gregmap_configs4ratio: Function argument _variables_denom INT[] must not be NULL!';
END IF;

-- checking input arrays if they do not contain NULL
IF (SELECT array_position(_estimation_cells, NULL) IS NOT NULL) THEN
	RAISE EXCEPTION 'fn_api_get_single2gregmap_configs4ratio: Function argument _estimation_cells INT[] must not be an array containing NULL!';
END IF;

IF (SELECT array_position(_variables_nom, NULL) IS NOT NULL) THEN
	RAISE EXCEPTION 'fn_api_get_single2gregmap_configs4ratio: Function argument _variables_nom INT[] must not be an array containing NULL!';
END IF;

IF (SELECT array_position(_variables_denom, NULL) IS NOT NULL) THEN
	RAISE EXCEPTION 'fn_api_get_single2gregmap_configs4ratio: Function argument _variables_denom INT[] must not be an array containing NULL!';
END IF;

-- checking one cell collection within input argument _estimation_cells 
IF (SELECT count(DISTINCT estimation_cell_collection) FROM nfiesta.c_estimation_cell WHERE ARRAY[id] <@ _estimation_cells) IS DISTINCT FROM 1 THEN 
	RAISE EXCEPTION 'fn_api_get_single2gregmap_configs4ratio: Function argument _estimation_cells INT[] must contain cells corresponding to one and the only estimation cell collection!';
END IF;

-- checking input arrays if they do not contain NULL
IF (SELECT array_position(_estimation_cells, NULL) IS NOT NULL) THEN
	RAISE EXCEPTION 'fn_api_get_single2gregmap_configs4ratio: Function argument _estimation_cells INT[] must not be an array containing NULL!';
END IF; 

IF (SELECT array_position(_variables_nom, NULL) IS NOT NULL) THEN
	RAISE EXCEPTION 'fn_api_get_single2gregmap_configs4ratio: Function argument _variables_nom INT[] must not be an array containing NULL!';
END IF; 

IF (SELECT array_position(_variables_denom, NULL) IS NOT NULL) THEN
	RAISE EXCEPTION 'fn_api_get_single2gregmap_configs4ratio: Function argument _variables_denom INT[] must not be an array containing NULL!';
END IF; 

-- check equality of lengths of _variables_nom and _variables_denom
IF (SELECT array_length(_variables_nom, 1) != array_length(_variables_denom, 1)) THEN
	RAISE EXCEPTION 'fn_api_get_single2gregmap_configs4ratio: Function arguments _variables_nom INT[] and _variables_denom INT[] must be arrays of the same lengths!';
END IF; 

RETURN QUERY EXECUTE '
	WITH w_vars AS MATERIALIZED ( -- combinations of variables in nominator and denominator
		SELECT 
			vnom.vid AS variable_nominator, 
			vdenom.vid AS variable_denominator
		FROM 
			unnest($3) WITH ORDINALITY vnom (vid,ord) 
		FULL JOIN 
			unnest($4) WITH ORDINALITY vdenom (vid, ord) 
		ON vnom.ord = vdenom.ord
	), w_greg_map_totals_conf AS MATERIALIZED (
		SELECT 
			array_agg(t1.id) OVER (PARTITION BY t1.panel_refyearset_group, t1.force_synthetic, t5.param_area_type, t4.model, t4.sigma) AS total_estimate_conf_nominator,
			array_agg(t2.id) OVER (PARTITION BY t1.panel_refyearset_group, t1.force_synthetic, t5.param_area_type, t4.model, t4.sigma) AS total_estimate_conf_denominator,
			t1.id AS total_estimate_conf_single,
			t2.id AS total_estimate_conf_greg_map,
			t1.estimation_period,
			t1.estimation_cell,
			t1.panel_refyearset_group,
			t8.label AS panel_refyearset_group_label,
			t8.label_en AS panel_refyearset_group_label_en,
			t8.description AS panel_refyearset_group_description,
			t8.description_en AS panel_refyearset_group_description_en,
			t1.variable AS nominator_variable,
			t2.variable AS denominator_variable,
			t5.gid AS param_area,
			t5.label AS param_area_label,
			t6.id AS param_area_type,
			t6.label AS param_area_type_label,
			t6.label_en AS param_area_type_label_en,
			t6.description AS param_area_type_description,
			t6.description_en AS param_area_type_description_en,
			t7.id AS model,
			t7.label AS model_label,
			t7.label_en AS model_label_en,
			t7.description AS model_description,
			t7.description_en AS model_description_en,
			t4.sigma AS model_sigma,
			count(*) OVER (PARTITION BY t1.panel_refyearset_group, t1.force_synthetic, t5.param_area_type, t4.model, t4.sigma) AS n_configuarations,
			count(*) OVER (PARTITION BY t1.panel_refyearset_group, t1.force_synthetic, t5.param_area_type, t4.model, t4.sigma) = array_length($2,1) * array_length($3,1) AS all_estimates_configurable
		FROM
			nfiesta.t_total_estimate_conf AS t1 -- look up  single-phase total configs for the ratio
		INNER JOIN
			nfiesta.t_total_estimate_conf AS t2 -- look up GREG-map total configs for the ratio
		ON 
			t1.estimation_period = $1 AND t2.estimation_period = $1  AND
			ARRAY[t1.estimation_cell] <@ $2 AND
			ARRAY[t2.estimation_cell] <@ $2 AND
			ARRAY[t1.variable] <@ $3 AND
			ARRAY[t2.variable] <@ $4 AND
			t1.phase_estimate_type = 1 AND t2.phase_estimate_type = 2 AND -- the required ratio is a combination of single-phase and GREG-map totals
			t1.estimation_cell = t2.estimation_cell AND 
			t1.panel_refyearset_group = t2.panel_refyearset_group AND -- group of panels and reference year sets must match between nom and denom
			NOT t2.force_synthetic -- combination with single-phase (always designe based) is only possible with design-based GREG-map estimators 
		INNER JOIN 
			w_vars AS t3
		ON 
			t1.variable = t3.variable_nominator AND
			t2.variable = t3.variable_denominator
		INNER JOIN 
			nfiesta.t_aux_conf AS t4 
			ON t2.aux_conf = t4.id
		INNER JOIN 
			nfiesta.f_a_param_area AS t5
			ON t4.param_area = t5.gid
		INNER JOIN 
			nfiesta.c_param_area_type AS t6
			ON t5.param_area_type = t6.id
		INNER JOIN 
			nfiesta.t_model AS t7 
			ON t4.model = t7.id
		 INNER JOIN 
		 nfiesta.c_panel_refyearset_group AS t8
		 ON t1.panel_refyearset_group = t8.id
	)
	SELECT DISTINCT 
			panel_refyearset_group AS panel_refyearset_group_id,
			panel_refyearset_group_label,  
			panel_refyearset_group_label_en,
			panel_refyearset_group_description,
			panel_refyearset_group_description_en,
			param_area_type AS param_area_type_id,
			param_area_type_label,
			param_area_type_label_en,
			param_area_type_description,
			param_area_type_description_en,
			FALSE AS force_synthetic,
			model AS denominator_model_id, 
			model_label AS denominator_model_label,
			model_label_en AS denominator_model_label_en,
			model_description AS denominator_model_description,
			model_description_en AS denominator_model_description_en,
			model_sigma AS denominator_model_sigma,
			all_estimates_configurable,
			total_estimate_conf_nominator,
			total_estimate_conf_denominator
	FROM 
		 w_greg_map_totals_conf' USING _estimation_period, _estimation_cells, _variables_nom, _variables_denom;
END;
$function$
LANGUAGE plpgsql
PARALLEL SAFE;

COMMENT ON FUNCTION  nfiesta.fn_api_get_single2gregmap_configs4ratio(INT, INT[], INT[], INT[]) IS 
'For input estimation period, estimation cells, nominator and denominator variables '
'the function returns combinations of panel-refyearset group, param. area type, force synthetic '
'(allways FALSE due to design-based single-phase total in the nominator), model and sigma of already '
'configured single-phase (nominator) and matching GREG-map (denominator) totals. The parameter '
'all_estimates_configurable is TRUE if the corresponding ratio can be configured for all input '
'combinations of estimation cell, nominator variable and denominator variable.';

/* tests
------------------------------------------------- testing invalid inputs ------------------------------------------------------------------------------
-- test NULL for _estimation_period argument
SELECT * FROM nfiesta.fn_api_get_single2gregmap_configs4ratio(NULL, ARRAY[100], ARRAY[1,2,3], ARRAY[3,3,3]);

-- test NULL for _estimation_cell argument
SELECT * FROM nfiesta.fn_api_get_single2gregmap_configs4ratio(1, NULL, ARRAY[1,2,3], ARRAY[3,3,3]);

-- test NULL for _variables_nom argument
SELECT * FROM nfiesta.fn_api_get_single2gregmap_configs4ratio(1, ARRAY[100], NULL, ARRAY[3,3,3]);

-- test NULL for _variables_denom argument
SELECT * FROM nfiesta.fn_api_get_single2gregmap_configs4ratio(1, ARRAY[100], ARRAY[1,2,3], NULL);

-- test for NULL as an element of _estimation_cell argument
SELECT * FROM nfiesta.fn_api_get_single2gregmap_configs4ratio(1, ARRAY[100, NULL], ARRAY[1,2,3], ARRAY[3,3,3]);
SELECT * FROM nfiesta.fn_api_get_single2gregmap_configs4ratio(1, ARRAY[NULL]::int[], ARRAY[1,2,3], ARRAY[3,3,3]);

-- test for NULL as an element of _variables_nom argument
SELECT * FROM nfiesta.fn_api_get_single2gregmap_configs4ratio(1, ARRAY[100], ARRAY[1,NULL,3], ARRAY[3,3,3]);
SELECT * FROM nfiesta.fn_api_get_single2gregmap_configs4ratio(1, ARRAY[100], ARRAY[NULL]::int[], ARRAY[3,3,3]);

-- test for NULL as an element of _variables_denom argument
SELECT * FROM nfiesta.fn_api_get_single2gregmap_configs4ratio(1, ARRAY[100], ARRAY[1,2,3], ARRAY[3,3,NULL]);
SELECT * FROM nfiesta.fn_api_get_single2gregmap_configs4ratio(1, ARRAY[100], ARRAY[1,2,3]::int[], ARRAY[NULL]::int[]);

-- test if estimation cells belong to one estimation cell collection (adding 1 to the valid array _estimation_cells, 1 
-- belongs to another collection but the same stratum
SELECT * FROM nfiesta.fn_api_get_single2gregmap_configs4ratio(1, ARRAY[1,44,45,46,47,48,49,50,51,52,53,56,57,58,62], ARRAY[1,2,3], ARRAY[3,3,3]);

-- test if _variables_nom and _variables_denom have equal lengths
SELECT FROM nfiesta.fn_api_get_single2gregmap_configs4ratio(1, ARRAY[100], ARRAY[1,2,3], ARRAY[3,3]);

------------------------------------------------ testing valid inputs ---------------------------------------------------------------------------------
-- one uncomplete config, INSPIRE 50x50 km param area type, common panel refyearset group (19) exists for cells 55 and 69, but not including cell 57
SELECT * FROM nfiesta.fn_api_get_single2gregmap_configs4ratio(1, ARRAY[55,57,69], ARRAY[1,2,3], ARRAY[3,3,3]);

-- one complete config is obtained if cell 57 is dropped from the above input
SELECT * FROM nfiesta.fn_api_get_single2gregmap_configs4ratio(1, ARRAY[55,69], ARRAY[1,2,3], ARRAY[3,3,3]);

-- two complete configs, two param area types: INSPIRE 50x50 km and the whole strata set 
SELECT * FROM nfiesta.fn_api_get_single2gregmap_configs4ratio(1, ARRAY[100], ARRAY[1,2,3], ARRAY[3,3,3]);

------------------------------------------------ helping code ---------------------------------------------------------------------------------
-- cells 55 and 69 use the same panel refyearset group, but a different one from cell 57
SELECT estimation_cell, panel_refyearset_group FROM nfiesta.t_total_estimate_conf 
WHERE phase_estimate_type = 1 AND ARRAY[estimation_cell] <@  ARRAY[55,57,69] AND ARRAY[variable] <@ ARRAY[1,2,3];

-- all three cells have a configuration using the same common panel refyearset group
SELECT estimation_cell, panel_refyearset_group FROM nfiesta.t_total_estimate_conf 
WHERE phase_estimate_type = 2 AND ARRAY[estimation_cell] <@  ARRAY[55,57,69] AND ARRAY[variable] <@ ARRAY[3] AND NOT force_synthetic;

	WITH w_vars AS MATERIALIZED ( -- combinations of variables in nominator and denominator
		SELECT 
			vnom.vid AS variable_nominator, 
			vdenom.vid AS variable_denominator
		FROM 
			unnest(ARRAY[1,2,3]) WITH ORDINALITY vnom (vid,ord) 
		FULL JOIN 
			unnest(ARRAY[3,3,3]) WITH ORDINALITY vdenom (vid, ord) 
		ON vnom.ord = vdenom.ord
	) -- w_greg_map_totals_conf AS MATERIALIZED (
		SELECT 
			t1.id AS total_estimate_conf_single,
			t2.id AS total_estimate_conf_greg_map,
			t1.estimation_period,
			t1.estimation_cell,
			t1.panel_refyearset_group,
			t8.label AS panel_refyearset_group_label,
			t8.label_en AS panel_refyearset_group_label_en,
			t8.description AS panel_refyearset_group_description,
			t8.description_en AS panel_refyearset_group_description_en,
			t1.variable AS nominator_variable,
			t2.variable AS denominator_variable,
			t5.gid AS param_area,
			t5.label AS param_area_label,
			t6.id AS param_area_type,
			t6.label AS param_area_type_label,
			t6.label_en AS param_area_type_label_en,
			t6.description AS param_area_type_description,
			t6.description_en AS param_area_type_description_en,
			t7.id AS model,
			t7.label AS model_label,
			t7.label_en AS model_label_en,
			t7.description AS model_description,
			t7.description_en AS model_description_en,
			t4.sigma AS model_sigma,
			count(*) OVER (PARTITION BY t1.panel_refyearset_group, t1.force_synthetic, t5.param_area_type, t4.model, t4.sigma) AS n_configuarations,
			count(*) OVER (PARTITION BY t1.panel_refyearset_group, t1.force_synthetic, t5.param_area_type, t4.model, t4.sigma) = array_length(ARRAY[55,57,69],1) * array_length(ARRAY[1,2,3],1) AS all_estimates_configurable
		FROM
			nfiesta.t_total_estimate_conf AS t1 -- look up  single-phase total configs for the ratio
		INNER JOIN
			nfiesta.t_total_estimate_conf AS t2 -- look up GREG-map total configs for the ratio
		ON 
			t1.estimation_period = 1 AND t2.estimation_period = 1  AND
			ARRAY[t1.estimation_cell] <@ ARRAY[55,57,69] AND
			ARRAY[t2.estimation_cell] <@ ARRAY[55,57,69] AND
			ARRAY[t1.variable] <@ ARRAY[1,2,3] AND
			ARRAY[t2.variable] <@ ARRAY[3,3,3] AND
			t1.phase_estimate_type = 1 AND t2.phase_estimate_type = 2 AND -- the required ratio is a combination of single-phase and GREG-map totals
			t1.estimation_cell = t2.estimation_cell AND 
			t1.panel_refyearset_group = t2.panel_refyearset_group AND -- group of panels and reference year sets must match between nom and denom
			NOT t2.force_synthetic -- combination with single-phase (always designe based) is only possible with design-based GREG-map estimators 
		INNER JOIN 
			w_vars AS t3
		ON 
			t1.variable = t3.variable_nominator AND
			t2.variable = t3.variable_denominator
		INNER JOIN 
			nfiesta.t_aux_conf AS t4 
			ON t2.aux_conf = t4.id
		INNER JOIN 
			nfiesta.f_a_param_area AS t5
			ON t4.param_area = t5.gid
		INNER JOIN 
			nfiesta.c_param_area_type AS t6
			ON t5.param_area_type = t6.id
		INNER JOIN 
			nfiesta.t_model AS t7 
			ON t4.model = t7.id
		 INNER JOIN 
		 nfiesta.c_panel_refyearset_group AS t8
		 ON t1.panel_refyearset_group = t8.id
	)
	SELECT DISTINCT 
			panel_refyearset_group AS panel_refyearset_group_id,
			panel_refyearset_group_label,  
			panel_refyearset_group_label_en,
			panel_refyearset_group_description,
			panel_refyearset_group_description_en,
			param_area_type AS param_area_type_id,
			param_area_type_label,
			param_area_type_label_en,
			param_area_type_description,
			param_area_type_description_en,
			FALSE AS force_synthetic,
			model AS denominator_model_id, 
			model_label AS denominator_model_label,
			model_label_en AS denominator_model_label_en,
			model_description AS denominator_model_description,
			model_description_en AS denominator_model_description_en,
			model_sigma AS denominator_model_sigma,
			all_estimates_configurable
	FROM 
		 w_greg_map_totals_conf

*/
-- </function>

-- new function to choose from possible configuration of a ratio of two GREG-map totals 
-- <function name="nfiesta.fn_api_save_fn_api_get_gregmap_configs4ratio(INT, INT[], INT[], INT[])" schema="extschema" src="functions/extschema/configuration/fn_api_get_gregmap_configs4ratio.sql">
-- Copyright 2017, 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: nfiesta.fn_api_get_gregmap_configs4ratio(INT, INT[], INT[], INT[])
-- DROP FUNCTION nfiesta.fn_api_get_gregmap_configs4ratio(INT, INT[], INT[], INT[]);

CREATE OR REPLACE FUNCTION nfiesta.fn_api_get_gregmap_configs4ratio(_estimation_period INT, _estimation_cells INT[], _variables_nom INT[], _variables_denom INT[])
RETURNS TABLE(
 			panel_refyearset_group_id INT,
			panel_refyearset_group_label VARCHAR(200),  
			panel_refyearset_group_label_en VARCHAR(200),
			panel_refyearset_group_description TEXT,
			panel_refyearset_group_description_en TEXT,
			param_area_type_id INT,
			param_area_type_label VARCHAR(200),
			param_area_type_label_en VARCHAR(200),
			param_area_type_description TEXT,
			param_area_type_description_en TEXT,
			force_synthetic BOOLEAN,
			nominator_model_id INT, 
			nominator_model_label VARCHAR(200),
			nominator_model_label_en VARCHAR(200),
			nominator_model_description TEXT,
			nominator_model_description_en TEXT,
			nominator_model_sigma BOOLEAN,
			denominator_model_id INT, 
			denominator_model_label VARCHAR(200),
			denominator_model_label_en VARCHAR(200),
			denominator_model_description TEXT,
			denominator_model_description_en TEXT,
			denominator_model_sigma BOOLEAN,
			all_estimates_configurable BOOLEAN,
			total_estimate_conf_nominator INT[],
			total_estimate_conf_denominator INT[]
	)
AS
$function$
BEGIN

-- testing input arguments if not NULL
IF _estimation_period IS NULL THEN
	RAISE EXCEPTION 'fn_api_get_gregmap_configs4ratio: Function argument _estimation_period INT must not be NULL!';
END IF;

IF _estimation_cells IS NULL THEN
	RAISE EXCEPTION 'fn_api_get_gregmap_configs4ratio: Function argument _estimation_cells INT[] must not be NULL!';
END IF;

IF _variables_nom IS NULL THEN
	RAISE EXCEPTION 'fn_api_get_gregmap_configs4ratio: Function argument _variables_nom INT[] must not be NULL!';
END IF;

IF _variables_denom IS NULL THEN
	RAISE EXCEPTION 'fn_api_get_gregmap_configs4ratio: Function argument _variables_denom INT[] must not be NULL!';
END IF;

-- checking input arrays if they do not contain NULL
IF (SELECT array_position(_estimation_cells, NULL) IS NOT NULL) THEN
	RAISE EXCEPTION 'fn_api_get_gregmap_configs4ratio: Function argument _estimation_cells INT[] must not be an array containing NULL!';
END IF;

IF (SELECT array_position(_variables_nom, NULL) IS NOT NULL) THEN
	RAISE EXCEPTION 'fn_api_get_gregmap_configs4ratio: Function argument _variables_nom INT[] must not be an array containing NULL!';
END IF;

IF (SELECT array_position(_variables_denom, NULL) IS NOT NULL) THEN
	RAISE EXCEPTION 'fn_api_get_gregmap_configs4ratio: Function argument _variables_denom INT[] must not be an array containing NULL!';
END IF;

-- checking one cell collection within input argument _estimation_cells 
IF (SELECT count(DISTINCT estimation_cell_collection) FROM nfiesta.c_estimation_cell WHERE ARRAY[id] <@ _estimation_cells) IS DISTINCT FROM 1 THEN 
	RAISE EXCEPTION 'fn_api_get_gregmap_configs4ratio: Function argument _estimation_cells INT[] must contain cells corresponding to one and the only estimation cell collection!';
END IF;

-- checking input arrays if they do not contain NULL
IF (SELECT array_position(_estimation_cells, NULL) IS NOT NULL) THEN
	RAISE EXCEPTION 'fn_api_get_gregmap_configs4ratio: Function argument _estimation_cells INT[] must not be an array containing NULL!';
END IF; 

IF (SELECT array_position(_variables_nom, NULL) IS NOT NULL) THEN
	RAISE EXCEPTION 'fn_api_get_gregmap_configs4ratio: Function argument _variables_nom INT[] must not be an array containing NULL!';
END IF; 

IF (SELECT array_position(_variables_denom, NULL) IS NOT NULL) THEN
	RAISE EXCEPTION 'fn_api_get_gregmap_configs4ratio: Function argument _variables_denom INT[] must not be an array containing NULL!';
END IF; 

-- check equality of lengths of _variables_nom and _variables_denom
IF (SELECT array_length(_variables_nom, 1) != array_length(_variables_denom, 1)) THEN
	RAISE EXCEPTION 'fn_api_get_gregmap_configs4ratio: Function arguments _variables_nom INT[] and _variables_denom INT[] must be arrays of the same lengths!';
END IF; 

RETURN QUERY EXECUTE '
	WITH w_vars AS MATERIALIZED ( -- combinations of variables in nominator and denominator
		SELECT 
			vnom.vid AS variable_nominator, 
			vdenom.vid AS variable_denominator
		FROM 
			unnest($3) WITH ORDINALITY vnom (vid,ord) 
		INNER JOIN 
			unnest($4) WITH ORDINALITY vdenom (vid, ord) 
		ON vnom.ord = vdenom.ord
	), w_greg_map_totals_conf AS MATERIALIZED (
		SELECT 
			array_agg(t1.id) OVER (PARTITION BY t1.panel_refyearset_group, t1.force_synthetic, t6.param_area_type, t4.model, t4.sigma, t5.model, t5.sigma) AS total_estimate_conf_nominator,
			array_agg(t2.id) OVER (PARTITION BY t1.panel_refyearset_group, t1.force_synthetic, t6.param_area_type, t4.model, t4.sigma, t5.model, t5.sigma) AS total_estimate_conf_denominator,
			t1.estimation_period,
			t1.estimation_cell,
			t1.panel_refyearset_group,
			t10.label AS panel_refyearset_group_label,
			t10.label_en AS panel_refyearset_group_label_en,
			t10.description AS panel_refyearset_group_description,
			t10.description_en AS panel_refyearset_group_description_en,
			t1.force_synthetic,
			t1.variable AS nominator_variable,
			t2.variable AS denominator_variable,
			t6.gid AS param_area,
			t6.label AS param_area_label,
			t7.id AS param_area_type,
			t7.label AS param_area_type_label,
			t7.label_en AS param_area_type_label_en,
			t7.description AS param_area_type_description,
			t7.description_en AS param_area_type_description_en,
			t8.id AS nominator_model,
			t8.label AS nominator_model_label,
			t8.label_en AS nominator_model_label_en,
			t8.description AS nominator_model_description,
			t8.description_en AS nominator_model_description_en,
			t4.sigma AS nominator_model_sigma,
			t9.id AS denominator_model,
			t9.label AS denominator_model_label,
			t9.label_en AS denominator_model_label_en,
			t9.description AS denominator_model_description,
			t9.description_en AS denominator_model_description_en,
			t5.sigma AS denominator_model_sigma,
			count(*) OVER (PARTITION BY t1.panel_refyearset_group, t1.force_synthetic, t6.param_area_type, t4.model, t4.sigma, t5.model, t5.sigma) AS n_configuarations,
			count(*) OVER (PARTITION BY t1.panel_refyearset_group, t1.force_synthetic, t6.param_area_type, t4.model, t4.sigma, t5.model, t5.sigma) = array_length($2,1) * array_length($3,1) AS all_estimates_configurable
		FROM
			nfiesta.t_total_estimate_conf AS t1 -- search GREG-map total configs for the nominator of the ratio
		INNER JOIN
			nfiesta.t_total_estimate_conf AS t2 -- search GREG-map total configs for the denominator of the ratio
		ON 
			t1.estimation_period = $1 AND t2.estimation_period = $1  AND
			ARRAY[t1.estimation_cell] <@ $2 AND
			ARRAY[t2.estimation_cell] <@ $2 AND
			ARRAY[t1.variable] <@ $3 AND
			ARRAY[t2.variable] <@ $4 AND
			t1.phase_estimate_type = 2 AND t2.phase_estimate_type = 2 AND -- nom and denom total must be of type GREG-map
			t1.estimation_cell = t2.estimation_cell AND 
			t1.panel_refyearset_group = t2.panel_refyearset_group AND -- group of panels and reference year sets must match between nom and denom
			t1.force_synthetic = t2.force_synthetic -- force synthetic must also match between nom and denom totals
		INNER JOIN 
			w_vars AS t3
		ON 
			t1.variable = t3.variable_nominator AND
			t2.variable = t3.variable_denominator
		INNER JOIN 
			nfiesta.t_aux_conf AS t4 
			ON t1.aux_conf = t4.id
		INNER JOIN 
			nfiesta.t_aux_conf AS t5
			ON t2.aux_conf = t5.id AND t4.param_area = t5.param_area -- parametrisation areas between nom and denom must match
		INNER JOIN 
			nfiesta.f_a_param_area AS t6
			ON t4.param_area = t6.gid
		INNER JOIN 
			nfiesta.c_param_area_type AS t7
			ON t6.param_area_type = t7.id
		INNER JOIN 
			nfiesta.t_model AS t8 
			ON t4.model = t8.id
		INNER JOIN 
			nfiesta.t_model AS t9
			ON t5.model = t9.id
		 INNER JOIN 
		 nfiesta.c_panel_refyearset_group AS t10
		 ON t1.panel_refyearset_group = t10.id
	)
	SELECT DISTINCT 
			panel_refyearset_group AS panel_refyearset_group_id,
			panel_refyearset_group_label,  
			panel_refyearset_group_label_en,
			panel_refyearset_group_description,
			panel_refyearset_group_description_en,
			param_area_type AS param_area_type_id,
			param_area_type_label,
			param_area_type_label_en,
			param_area_type_description,
			param_area_type_description_en,
			force_synthetic,
			nominator_model AS nominator_model_id, 
			nominator_model_label,
			nominator_model_label_en,
			nominator_model_description,
			nominator_model_description_en,
			nominator_model_sigma,
			denominator_model AS denominator_model_id, 
			denominator_model_label,
			denominator_model_label_en,
			denominator_model_description,
			denominator_model_description_en,
			denominator_model_sigma,
			all_estimates_configurable,
			total_estimate_conf_nominator,
			total_estimate_conf_denominator
	FROM 
		 w_greg_map_totals_conf' USING _estimation_period, _estimation_cells, _variables_nom, _variables_denom;
END;
$function$
LANGUAGE plpgsql
PARALLEL SAFE;

COMMENT ON FUNCTION  nfiesta.fn_api_get_gregmap_configs4ratio(INT, INT[], INT[], INT[]) IS 
'For input estimation period, estimation cells, nominator and denominator variables '
'the function returns combinations of panel-refyearset group, param. area type, force synthetic, '
'model and sigma of already configured GREG-map totals, so a GREG-map ratio of these two totals '
'can be configured. The parameter all_estimates_configurable is TRUE if such a ratio can be '
'configured for all input combinations of estimation cell, nominator variable and denominator '
'variable.';

/* tests
------------------------------------------------- testing invalid inputs ------------------------------------------------------------------------------
-- test NULL for _estimation_period argument
SELECT * FROM nfiesta.fn_api_get_gregmap_configs4ratio(NULL, ARRAY[100], ARRAY[1,2,3], ARRAY[3,3,3]);

-- test NULL for _estimation_cell argument
SELECT * FROM nfiesta.fn_api_get_gregmap_configs4ratio(1, NULL, ARRAY[1,2,3], ARRAY[3,3,3]);

-- test NULL for _variables_nom argument
SELECT * FROM nfiesta.fn_api_get_gregmap_configs4ratio(1, ARRAY[100], NULL, ARRAY[3,3,3]);

-- test NULL for _variables_denom argument
SELECT * FROM nfiesta.fn_api_get_gregmap_configs4ratio(1, ARRAY[100], ARRAY[1,2,3], NULL);

-- test for NULL as an element of _estimation_cell argument
SELECT * FROM nfiesta.fn_api_get_gregmap_configs4ratio(1, ARRAY[100, NULL], ARRAY[1,2,3], ARRAY[3,3,3]);
SELECT * FROM nfiesta.fn_api_get_gregmap_configs4ratio(1, ARRAY[NULL]::int[], ARRAY[1,2,3], ARRAY[3,3,3]);

-- test for NULL as an element of _variables_nom argument
SELECT * FROM nfiesta.fn_api_get_gregmap_configs4ratio(1, ARRAY[100], ARRAY[1,NULL,3], ARRAY[3,3,3]);
SELECT * FROM nfiesta.fn_api_get_gregmap_configs4ratio(1, ARRAY[100], ARRAY[NULL]::int[], ARRAY[3,3,3]);

-- test for NULL as an element of _variables_denom argument
SELECT * FROM nfiesta.fn_api_get_gregmap_configs4ratio(1, ARRAY[100], ARRAY[1,2,3], ARRAY[3,3,NULL]);
SELECT * FROM nfiesta.fn_api_get_gregmap_configs4ratio(1, ARRAY[100], ARRAY[1,2,3]::int[], ARRAY[NULL]::int[]);

-- test if estimation cells belong to one estimation cell collection (adding 1 to the valid array _estimation_cells, 1 
-- belongs to another collection but the same stratum
SELECT * FROM nfiesta.fn_api_get_gregmap_configs4ratio(1, ARRAY[1,44,45,46,47,48,49,50,51,52,53,56,57,58,62], ARRAY[1,2,3], ARRAY[3,3,3]);

-- test if _variables_nom and _variables_denom have equal lengths
SELECT FROM nfiesta.fn_api_get_gregmap_configs4ratio(1, ARRAY[100], ARRAY[1,2,3], ARRAY[3,3]);

------------------------------------------------ testing valid inputs ---------------------------------------------------------------------------------
-- two complete configs, one force true, the other false, INSPIRE 50x50 km param area type
SELECT * FROM nfiesta.fn_api_get_gregmap_configs4ratio(1, ARRAY[55,57,69], ARRAY[1,2,3], ARRAY[3,3,3]);

-- two complete configs, one force true, the other false, two param area types: INSPIRE 50x50 km and strata set 
SELECT * FROM nfiesta.fn_api_get_gregmap_configs4ratio(1, ARRAY[100], ARRAY[1,2,3], ARRAY[3,3,3]);
*/

-- </function>

-- new function to safe the desired ratio configuration involving the GREG-map
-- <function name="nfiesta.fn_api_save_ratio_config(INT, INT)" schema="extschema" src="functions/extschema/configuration/fn_api_save_ratio_config.sql">
--
-- Copyright 2017, 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: nfiesta.fn_api_save_ratio_config(INT, INT)

--DROP FUNCTION nfiesta.fn_api_save_ratio_config(INT, INT);

CREATE OR REPLACE FUNCTION nfiesta.fn_api_save_ratio_config(_total_estimate_conf_nominator INT, _total_estimate_conf_denominator INT)
RETURNS INT
AS
$function$
DECLARE
_estimate_conf INT;
_nom_panels INT[];
_denom_panels INT[];

BEGIN
	
	-- testing input parameters if not NULL
	IF _total_estimate_conf_nominator IS NULL THEN
		RAISE EXCEPTION 'fn_api_save_ratio_config: Function argument _total_estimate_conf_nominator INT must not be NULL!';
	END IF;	
	
	IF _total_estimate_conf_denominator IS NULL THEN
		RAISE EXCEPTION 'fn_api_save_ratio_config: Function argument _total_estimate_conf_denominator INT must not be NULL!';
	END IF;	
	
	-- testing for exsistence of total configurations
	IF NOT EXISTS (SELECT id FROM nfiesta.t_total_estimate_conf WHERE id = $1) THEN
			RAISE EXCEPTION 'fn_api_save_ratio_config: There is no total configuration corresponding to the function argument _total_estimate_conf_nominator: % !', _total_estimate_conf_nominator;
	END IF;	

	IF NOT EXISTS (SELECT id FROM nfiesta.t_total_estimate_conf WHERE id = $2) THEN
			RAISE EXCEPTION 'fn_api_save_ratio_config: There is no total configuration corresponding to the function argument _total_estimate_conf_denominator: %!', _total_estimate_conf_denominator;
	END IF;	


	-- testing the equality of panel groups between nominator and denominator (can be relased provided the ratio estimator functions are updated)
    -- to permit for unequal sets of panels in the nominator and denominator.
	SELECT 
		array_agg(panel ORDER BY panel ASC) 
	FROM
		nfiesta.t_total_estimate_conf AS t1
	INNER JOIN
		nfiesta.t_panel_refyearset_group AS t2
	ON t1.id  = $1 AND t1.panel_refyearset_group = t2.id
	INTO _nom_panels;

		SELECT 
		array_agg(panel ORDER BY panel ASC) 
	FROM
		nfiesta.t_total_estimate_conf AS t1
	INNER JOIN
		nfiesta.t_panel_refyearset_group AS t2
	ON t1.id  = $2 AND t1.panel_refyearset_group = t2.id
	INTO _denom_panels;

	IF _nom_panels <> _denom_panels THEN
	RAISE EXCEPTION 'fn_api_save_ratio_config: The set of panels between the nominator and the denominator do not match! '
					'Such ratio estimators have not yet benn implemented!';
	END IF;
	
	-- reset sequence (usually when previous atempt to configure fails, the sequence remains shifted)
	PERFORM setval('nfiesta.t_estimate_conf_id_seq', (SELECT coalesce(max(id),0)+1 FROM nfiesta.t_estimate_conf), FALSE);

	-- insert into table t_estimate_conf
	INSERT INTO nfiesta.t_estimate_conf (estimate_type, total_estimate_conf, denominator)
	VALUES(2, _total_estimate_conf_nominator, _total_estimate_conf_denominator)
	ON CONFLICT (total_estimate_conf, coalesce(denominator,0)) DO NOTHING
	RETURNING id INTO _estimate_conf;
	
	IF _estimate_conf IS NULL THEN
		RAISE NOTICE 'fn_api_save_ratio_config: An identical ratio configuration already exists! Saving the same configuration has been skipped.';
	END IF;

RETURN _estimate_conf;

END;
$function$
LANGUAGE plpgsql
PARALLEL SAFE;

COMMENT ON FUNCTION nfiesta.fn_api_save_ratio_config(INT, INT) IS 
'The function saves configuration of a ratio estimator into the t_estimate_conf table. '
'It presumes existence of total configurations for the nominator as well as the '
'the enominator of the ratio. Any type of total estimators can be combined in the '
'nominator and the denominator as long as they use the same group of panels.';

/* tests
-------------------------------------------------------------------------------------------------------------------------------------------------------
-- tests for function nfiesta.fn_api_save_ratio_config(_total_estimate_conf_nominator INT, _total_estimate_conf_denominator INT)
-------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------- testing invalid inputs ------------------------------------------------------------------------------
-- test NULL for _total_estimate_conf_nominator
SELECT * FROM nfiesta.fn_api_save_ratio_config(NULL,1);

-- test NULL for _total_estimate_conf_denominator
SELECT * FROM nfiesta.fn_api_save_ratio_config(1,NULL);

-- test for non existing _total_estimate_conf_nominator (among records of t_total_estimate_conf)
SELECT * FROM nfiesta.fn_api_save_ratio_config(-1,1);

-- test for non existing _total_estimate_conf_denominator (among records of t_total_estimate_conf)
SELECT * FROM nfiesta.fn_api_save_ratio_config(1,-1);

-- trying to configure a ratio of totals, with different sets of panels (panel_refyearset_groups 14 and 15)
SELECT * FROM nfiesta.fn_api_save_ratio_config(1,5);

------------------------------------------------ testing valid inputs ---------------------------------------------------------------------------------
-- save a configuration of a ratio of forest to non-forest within the same cell and using the same panel_refyearset_group and return new config id
SELECT * FROM nfiesta.fn_api_save_ratio_config(1,2);

-- skip already existing configuration and return NULL
SELECT * FROM nfiesta.fn_api_save_ratio_config(1,2);

------------------------------------------------ helping code ---------------------------------------------------------------------------------
-- helping code
SELECT * FROM nfiesta.t_total_estimate_conf;

SELECT * FROM nfiesta.t_estimate_conf WHERE estimate_type = 2 ORDER BY total_estimate_conf, denominator;

*/

-- </function>