--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

ALTER TABLE @extschema@.c_target_variable ADD UNIQUE (label);
ALTER TABLE @extschema@.c_variable_type ADD UNIQUE (label);
ALTER TABLE @extschema@.c_sub_population_category ADD UNIQUE (label);
ALTER TABLE @extschema@.c_sub_population ADD UNIQUE (label);
ALTER TABLE @extschema@.c_area_domain_category ADD UNIQUE (label);
ALTER TABLE @extschema@.c_area_domain ADD UNIQUE (label);
--ALTER TABLE @extschema@.c_auxiliary_variable_category ADD UNIQUE (label);

alter table @extschema@.t_estimation_cell_hierarchy rename column cell_sup to cell_superior;
alter table @extschema@.t_estimation_cell_hierarchy alter column cell_superior set not null;

alter table @extschema@.t_variable_hierarchy rename column var to variable;
alter table @extschema@.t_variable_hierarchy rename column var_sup to variable_superior;
alter table @extschema@.t_variable_hierarchy alter column variable_superior set not null;

COMMENT ON COLUMN @extschema@.t_variable_hierarchy.variable IS 'Foreign key to id of t_variable.';
COMMENT ON COLUMN @extschema@.t_variable_hierarchy.variable_superior IS 'Foreign key to id of t_variable which is superior in hierarchy.';

--------------------------------------FUNCTIONS & VIEWS--------------------------------------
drop view @extschema@.v_add_aux_total_geo;
drop view @extschema@.v_add_res_total_geo;
drop view @extschema@.v_estimation_cell_hierarchy;

-- <view name="v_estimation_cell_hierarchy" schema="extschema" src="views/extschema/v_estimation_cell_hierarchy.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

CREATE VIEW @extschema@.v_estimation_cell_hierarchy AS
SELECT cell_superior as node, array_agg(cell order by cell) AS edges
FROM @extschema@.t_estimation_cell_hierarchy
GROUP BY cell_superior
ORDER BY cell_superior;

-- </view>

-- <view name="v_add_aux_total_geo" schema="extschema" src="views/extschema/v_add_aux_total_geo.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

create view @extschema@.v_add_aux_total_geo as
with w_aux_total_est_cell as not materialized (
	select t_aux_total.*, c_estimation_cell.id as c_estimation_cell__id
	from @extschema@.t_aux_total
	inner join @extschema@.c_estimation_cell on (t_aux_total.estimation_cell = c_estimation_cell.id)
	where t_aux_total.is_latest
)
, w_node_sum as (
	select
		w_aux_total_est_cell.estimation_cell, w_aux_total_est_cell.variable,
		sum(aux_total) as node_sum
	from w_aux_total_est_cell
	inner join @extschema@.v_estimation_cell_hierarchy on (v_estimation_cell_hierarchy.node = w_aux_total_est_cell.c_estimation_cell__id)
	group by w_aux_total_est_cell.estimation_cell, w_aux_total_est_cell.variable
	order by w_aux_total_est_cell.estimation_cell, w_aux_total_est_cell.variable
)
, w_edge_sum as (
	select
		w_node_sum.estimation_cell, w_node_sum.variable,
		v_estimation_cell_hierarchy.node,
		w_node_sum.node_sum,
		v_estimation_cell_hierarchy.edges as edges_def,
		array_agg(w_aux_total_est_cell.c_estimation_cell__id order by w_aux_total_est_cell.c_estimation_cell__id) as edges_found,
		sum(w_aux_total_est_cell.aux_total) as edges_sum
	from w_node_sum
	inner join @extschema@.v_estimation_cell_hierarchy on (v_estimation_cell_hierarchy.node = w_node_sum.estimation_cell)
	left join w_aux_total_est_cell on (
		w_aux_total_est_cell.variable = w_node_sum.variable
		and w_aux_total_est_cell.c_estimation_cell__id = any(v_estimation_cell_hierarchy.edges))
	group by w_node_sum.estimation_cell, w_node_sum.variable, v_estimation_cell_hierarchy.node, w_node_sum.node_sum, v_estimation_cell_hierarchy.edges
)
, w_diff as (
	select
		*,
		case
			when node_sum != 0.0 and edges_sum = 0.0 then 100.0
			when node_sum = 0.0 and edges_sum = 0.0 then 0
			else abs(1 - (node_sum / edges_sum )) * 100.0
		end as diff
	from w_edge_sum
)
select * from w_diff order by diff desc
;

-- </view>

-- <view name="v_add_res_total_geo" schema="extschema" src="views/extschema/v_add_res_total_geo.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

create view @extschema@.v_add_res_total_geo as
with w_result_est_cell as not materialized (
	select
		t_result.point,
		t_total_estimate_conf.estimation_cell,
		t_total_estimate_conf.target_variable as t_variable__id,
		t_total_estimate_conf.aux_conf, force_synthetic
	from @extschema@.t_result
	inner join @extschema@.t_estimate_conf ON t_estimate_conf.id = t_result.estimate_conf
	inner join @extschema@.t_total_estimate_conf ON t_total_estimate_conf.id = t_estimate_conf.total_estimate_conf
	where t_estimate_conf.estimate_type = 1
	and t_result.is_latest
)
, w_node_sum as (
	select
		estimation_cell,
		w_result_est_cell.t_variable__id, aux_conf, force_synthetic,
		sum(point) as node_sum
	from w_result_est_cell
	inner join @extschema@.v_estimation_cell_hierarchy on (v_estimation_cell_hierarchy.node = w_result_est_cell.estimation_cell)
	group by estimation_cell, w_result_est_cell.t_variable__id, aux_conf, force_synthetic
	order by estimation_cell, w_result_est_cell.t_variable__id, aux_conf, force_synthetic
)
, w_edge_sum as (
	select
		w_node_sum.estimation_cell, w_node_sum.t_variable__id as variable, w_node_sum.aux_conf, w_node_sum.force_synthetic,
		v_estimation_cell_hierarchy.node,
		w_node_sum.node_sum,
		v_estimation_cell_hierarchy.edges as edges_def,
		array_agg(w_result_est_cell.estimation_cell order by w_result_est_cell.estimation_cell) as edges_found,
		sum(w_result_est_cell.point) as edges_sum
	from w_node_sum
	inner join @extschema@.v_estimation_cell_hierarchy on (v_estimation_cell_hierarchy.node = w_node_sum.estimation_cell)
	left join w_result_est_cell on (
		w_result_est_cell.t_variable__id = w_node_sum.t_variable__id
		and case when
			w_result_est_cell.aux_conf is null and w_node_sum.aux_conf is null then true
			else w_result_est_cell.aux_conf = w_node_sum.aux_conf end
		and case when
			w_result_est_cell.force_synthetic is null and w_node_sum.force_synthetic is null then true
			else w_result_est_cell.force_synthetic = w_node_sum.force_synthetic end
		and w_result_est_cell.estimation_cell = any(v_estimation_cell_hierarchy.edges))
	group by w_node_sum.estimation_cell, w_node_sum.t_variable__id, w_node_sum.aux_conf, w_node_sum.force_synthetic, v_estimation_cell_hierarchy.node, w_node_sum.node_sum, v_estimation_cell_hierarchy.edges
)
, w_diff as (
	select
		*,
		case
			when node_sum != 0.0 and edges_sum = 0.0 then 100.0
			when node_sum = 0.0 and edges_sum = 0.0 then 0
			else abs(1 - (node_sum / edges_sum )) * 100.0
		end as diff
	from w_edge_sum
)
select * from w_diff order by diff desc
;

-- </view>

--------------------------------------
drop view @extschema@.v_add_plot_target_attr;
drop view @extschema@.v_add_plot_aux_attr;
drop view @extschema@.v_add_aux_total_attr;
drop view @extschema@.v_add_res_total_attr;
drop view @extschema@.v_add_res_ratio_attr;
drop view @extschema@.v_variable_hierarchy;

-- <view name="v_variable_hierarchy" schema="extschema" src="views/extschema/v_variable_hierarchy.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

CREATE VIEW @extschema@.v_variable_hierarchy AS
SELECT variable_superior as node, array_agg(variable order by variable) AS edges
FROM @extschema@.t_variable_hierarchy
GROUP BY variable_superior
ORDER BY variable_superior;

-- </view>

-- <view name="v_add_plot_target_attr" schema="extschema" src="views/extschema/v_add_plot_target_attr.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

create view @extschema@.v_add_plot_target_attr as
with w_target_data_variable as not materialized (
	select
		f_p_plot.gid as plot, t_available_datasets.variable, t_available_datasets.reference_year_set,
		coalesce(t_target_data.value, 0) as value
	from @extschema@.f_p_plot
	inner join @extschema@.t_cluster on (f_p_plot.cluster = t_cluster.id)
	inner join @extschema@.cm_cluster2panel_mapping on (t_cluster.id = cm_cluster2panel_mapping.cluster)
	inner join @extschema@.t_panel on (cm_cluster2panel_mapping.panel = t_panel.id)
	inner join @extschema@.t_available_datasets on (t_panel.id = t_available_datasets.panel)
	inner join @extschema@.t_variable on (t_available_datasets.variable = t_variable.id and t_variable.auxiliary_variable_category is null)
	left join @extschema@.t_target_data on (
		f_p_plot.gid = t_target_data.plot
		and t_available_datasets.variable = t_target_data.variable
		and t_available_datasets.reference_year_set = t_target_data.reference_year_set
		and t_target_data.is_latest)
)
, w_node_sum as (
	select
		plot,
		w_target_data_variable.reference_year_set,
		w_target_data_variable.variable,
		value as node_sum
	from w_target_data_variable
	inner join @extschema@.v_variable_hierarchy on (v_variable_hierarchy.node = w_target_data_variable.variable)
)
, w_edge_sum as (
	select
		w_node_sum.plot,
		w_node_sum.reference_year_set,
		v_variable_hierarchy.node,
		w_node_sum.node_sum,
		v_variable_hierarchy.edges as edges_def,
		array_agg(w_target_data_variable.variable order by w_target_data_variable.variable) as edges_found,
		sum(w_target_data_variable.value) as edges_sum
	from w_node_sum
	inner join @extschema@.v_variable_hierarchy on (v_variable_hierarchy.node = w_node_sum.variable)
	left join w_target_data_variable on (
		w_target_data_variable.plot = w_node_sum.plot
		and w_target_data_variable.reference_year_set = w_node_sum.reference_year_set
		and w_target_data_variable.variable = any(v_variable_hierarchy.edges))
	group by w_node_sum.plot, w_node_sum.reference_year_set, v_variable_hierarchy.node, w_node_sum.node_sum, v_variable_hierarchy.edges
)
, w_diff as (
	select
		*,
		case
			when node_sum != 0.0 and edges_sum = 0.0 then 100.0
			when node_sum = 0.0 and edges_sum = 0.0 then 0
			else abs(1 - (node_sum / edges_sum )) * 100.0
		end as diff
	from w_edge_sum
)
select * from w_diff order by diff desc
;

-- </view>

-- <view name="v_add_plot_aux_attr" schema="extschema" src="views/extschema/v_add_plot_aux_attr.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

create view @extschema@.v_add_plot_aux_attr as
with w_auxiliary_data_variable as not materialized (
	select
		f_p_plot.gid as plot, t_available_datasets.variable, t_available_datasets.reference_year_set,
		coalesce(t_auxiliary_data.value, 0) as value
	from @extschema@.f_p_plot
	inner join @extschema@.t_cluster on (f_p_plot.cluster = t_cluster.id)
	inner join @extschema@.cm_cluster2panel_mapping on (t_cluster.id = cm_cluster2panel_mapping.cluster)
	inner join @extschema@.t_panel on (cm_cluster2panel_mapping.panel = t_panel.id)
	inner join @extschema@.t_available_datasets on (t_panel.id = t_available_datasets.panel)
	inner join @extschema@.t_variable on (t_available_datasets.variable = t_variable.id and t_variable.auxiliary_variable_category is not null)
	left join @extschema@.t_auxiliary_data on (
		f_p_plot.gid = t_auxiliary_data.plot
		and t_available_datasets.variable = t_auxiliary_data.variable
		and t_auxiliary_data.is_latest)
)
, w_node_sum as (
	select
		plot,
		w_auxiliary_data_variable.variable,
		value as node_sum
	from w_auxiliary_data_variable
	inner join @extschema@.v_variable_hierarchy on (v_variable_hierarchy.node = w_auxiliary_data_variable.variable)
)
, w_edge_sum as (
	select
		w_node_sum.plot,
		v_variable_hierarchy.node,
		w_node_sum.node_sum,
		v_variable_hierarchy.edges as edges_def,
		array_agg(w_auxiliary_data_variable.variable order by w_auxiliary_data_variable.variable) as edges_found,
		sum(w_auxiliary_data_variable.value) as edges_sum
	from w_node_sum
	inner join @extschema@.v_variable_hierarchy on (v_variable_hierarchy.node = w_node_sum.variable)
	left join w_auxiliary_data_variable on (
		w_auxiliary_data_variable.plot = w_node_sum.plot
		and w_auxiliary_data_variable.variable = any(v_variable_hierarchy.edges))
	group by w_node_sum.plot, v_variable_hierarchy.node, w_node_sum.node_sum, v_variable_hierarchy.edges
)
, w_diff as (
	select
		*,
		case
			when node_sum != 0.0 and edges_sum = 0.0 then 100.0
			when node_sum = 0.0 and edges_sum = 0.0 then 0
			else abs(1 - (node_sum / edges_sum )) * 100.0
		end as diff
	from w_edge_sum
)
select * from w_diff order by diff desc
;

-- </view>

-- <view name="v_add_aux_total_attr" schema="extschema" src="views/extschema/v_add_aux_total_attr.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

create view @extschema@.v_add_aux_total_attr as
with w_aux_total_variable as not materialized (
	select t_aux_total.*, t_variable.id as t_variable__id
	from @extschema@.t_aux_total
	inner join @extschema@.t_variable on (t_aux_total.variable = t_variable.id)
	where t_aux_total.is_latest
)
, w_node_sum as (
	select
		estimation_cell,
		w_aux_total_variable.t_variable__id,
		sum(aux_total) as node_sum
	from w_aux_total_variable
	inner join @extschema@.v_variable_hierarchy on (v_variable_hierarchy.node = w_aux_total_variable.t_variable__id)
	group by estimation_cell, w_aux_total_variable.t_variable__id
)
, w_edge_sum as (
	select
		w_node_sum.estimation_cell,
		v_variable_hierarchy.node,
		w_node_sum.node_sum,
		v_variable_hierarchy.edges as edges_def,
		array_agg(w_aux_total_variable.t_variable__id order by w_aux_total_variable.t_variable__id) as edges_found,
		sum(w_aux_total_variable.aux_total) as edges_sum

	from w_node_sum
	inner join @extschema@.v_variable_hierarchy on (v_variable_hierarchy.node = w_node_sum.t_variable__id)
	left join w_aux_total_variable on (
		w_aux_total_variable.estimation_cell = w_node_sum.estimation_cell
		and w_aux_total_variable.t_variable__id = any(v_variable_hierarchy.edges))
	group by w_node_sum.estimation_cell, v_variable_hierarchy.node, w_node_sum.node_sum, v_variable_hierarchy.edges
)
, w_diff as (
	select
		*,
		case
			when node_sum != 0.0 and edges_sum = 0.0 then 100.0
			when node_sum = 0.0 and edges_sum = 0.0 then 0
			else abs(1 - (node_sum / edges_sum )) * 100.0
		end as diff
	from w_edge_sum
)
select * from w_diff order by diff desc
;

-- </view>

-- <view name="v_add_res_total_attr" schema="extschema" src="views/extschema/v_add_res_total_attr.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

create view @extschema@.v_add_res_total_attr as
with w_result_variable as not materialized (
	select
		t_result.point,
		t_total_estimate_conf.estimation_cell,
		t_total_estimate_conf.target_variable as t_variable__id,
		t_total_estimate_conf.aux_conf, force_synthetic
	from @extschema@.t_result
	inner join @extschema@.t_estimate_conf ON t_estimate_conf.id = t_result.estimate_conf
	inner join @extschema@.t_total_estimate_conf ON t_total_estimate_conf.id = t_estimate_conf.total_estimate_conf
	where t_estimate_conf.estimate_type = 1
	and t_result.is_latest
)
, w_node_sum as (
	select
		estimation_cell,
		w_result_variable.t_variable__id, aux_conf, force_synthetic,
		sum(point) as node_sum
	from w_result_variable
	inner join @extschema@.v_variable_hierarchy on (v_variable_hierarchy.node = w_result_variable.t_variable__id)
	group by estimation_cell, w_result_variable.t_variable__id, aux_conf, force_synthetic
	order by estimation_cell, w_result_variable.t_variable__id, aux_conf, force_synthetic
)
, w_edge_sum as (
	select
		w_node_sum.estimation_cell, w_node_sum.aux_conf, w_node_sum.force_synthetic,
		v_variable_hierarchy.node,
		w_node_sum.node_sum,
		v_variable_hierarchy.edges as edges_def,
		array_agg(w_result_variable.t_variable__id order by w_result_variable.t_variable__id) as edges_found,
		sum(w_result_variable.point) as edges_sum
	from w_node_sum
	inner join @extschema@.v_variable_hierarchy on (v_variable_hierarchy.node = w_node_sum.t_variable__id)
	left join w_result_variable on (
		w_result_variable.estimation_cell = w_node_sum.estimation_cell
		and case when
			w_result_variable.aux_conf is null and w_node_sum.aux_conf is null then true
			else w_result_variable.aux_conf = w_node_sum.aux_conf end
		and case when
			w_result_variable.force_synthetic is null and w_node_sum.force_synthetic is null then true
			else w_result_variable.force_synthetic = w_node_sum.force_synthetic end
		and w_result_variable.t_variable__id = any(v_variable_hierarchy.edges))
	group by w_node_sum.estimation_cell, w_node_sum.aux_conf, w_node_sum.force_synthetic, v_variable_hierarchy.node, w_node_sum.node_sum, v_variable_hierarchy.edges
)
, w_diff as (
	select
		*,
		case
			when node_sum != 0.0 and edges_sum = 0.0 then 100.0
			when node_sum = 0.0 and edges_sum = 0.0 then 0
			else abs(1 - (node_sum / edges_sum )) * 100.0
		end as diff
	from w_edge_sum
)
select * from w_diff order by diff desc
;

-- </view>

-- <view name="v_add_res_ratio_attr" schema="extschema" src="views/extschema/v_add_res_ratio_attr.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

create view @extschema@.v_add_res_ratio_attr as
with w_result_variable as not materialized (
	select
		t_result.point,
		t_total_estimate_conf.estimation_cell,
		t_total_estimate_conf.target_variable as t_variable__id,
		t_total_estimate_conf.aux_conf, force_synthetic, t_estimate_conf.denominator
	from @extschema@.t_result
	inner join @extschema@.t_estimate_conf ON t_estimate_conf.id = t_result.estimate_conf
	inner join @extschema@.t_total_estimate_conf ON t_total_estimate_conf.id = t_estimate_conf.total_estimate_conf
	where t_estimate_conf.estimate_type = 2
	and t_result.is_latest
)
, w_node_sum as (
	select
		estimation_cell,
		w_result_variable.t_variable__id, aux_conf, force_synthetic, denominator,
		sum(point) as node_sum
	from w_result_variable
	inner join @extschema@.v_variable_hierarchy on (v_variable_hierarchy.node = w_result_variable.t_variable__id)
	group by estimation_cell, w_result_variable.t_variable__id, aux_conf, force_synthetic, denominator
	order by estimation_cell, w_result_variable.t_variable__id, aux_conf, force_synthetic, denominator
)
, w_edge_sum as (
	select
		w_node_sum.estimation_cell, w_node_sum.aux_conf, w_node_sum.force_synthetic, w_node_sum.denominator,
		v_variable_hierarchy.node,
		w_node_sum.node_sum,
		v_variable_hierarchy.edges as edges_def,
		array_agg(w_result_variable.t_variable__id order by w_result_variable.t_variable__id) as edges_found,
		sum(w_result_variable.point) as edges_sum
	from w_node_sum
	inner join @extschema@.v_variable_hierarchy on (v_variable_hierarchy.node = w_node_sum.t_variable__id)
	left join w_result_variable on (
		w_result_variable.estimation_cell = w_node_sum.estimation_cell
		and case when
			w_result_variable.aux_conf is null and w_node_sum.aux_conf is null then true
			else w_result_variable.aux_conf = w_node_sum.aux_conf end
		and case when
			w_result_variable.force_synthetic is null and w_node_sum.force_synthetic is null then true
			else w_result_variable.force_synthetic = w_node_sum.force_synthetic end
		and w_result_variable.denominator = w_node_sum.denominator
		and w_result_variable.t_variable__id = any(v_variable_hierarchy.edges))
	group by w_node_sum.estimation_cell, w_node_sum.aux_conf, w_node_sum.force_synthetic, w_node_sum.denominator, v_variable_hierarchy.node, w_node_sum.node_sum, v_variable_hierarchy.edges
)
, w_diff as (
	select
		*,
		case
			when node_sum != 0.0 and edges_sum = 0.0 then 100.0
			when node_sum = 0.0 and edges_sum = 0.0 then 0
			else abs(1 - (node_sum / edges_sum )) * 100.0
		end as diff
	from w_edge_sum
)
select * from w_diff order by diff desc
;

-- </view>
