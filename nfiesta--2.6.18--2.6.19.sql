--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

alter table @extschema@.t_panel add column sweight_panel_sum double precision;
comment on column @extschema@.t_panel.sweight_panel_sum is 'Sum of sampling weights for all sampling units in the panel.';

with w_clusters_data as (
	select cm_cluster2panel_mapping.panel, sum(sampling_weight) as sweight_panel_sum
	from @extschema@.t_panel
	inner join @extschema@.cm_cluster2panel_mapping on (t_panel.id = cm_cluster2panel_mapping.panel)
	group by cm_cluster2panel_mapping.panel order by cm_cluster2panel_mapping.panel
)
update @extschema@.t_panel set sweight_panel_sum = w_clusters_data.sweight_panel_sum
from w_clusters_data
where t_panel.id = w_clusters_data.panel
;

alter table @extschema@.t_panel alter column sweight_panel_sum set not null;

-- <function_name="fn_1p_data" function_schema="extschema" src="functions/extschema/fn_1p_data.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_1p_data(integer)

-- DROP FUNCTION @extschema@.fn_1p_data(integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_1p_data(
    IN conf_id integer
)
  RETURNS TABLE(
	gid bigint,
	cluster integer,
	attribute integer,
	stratum integer,
	plots_per_cluster integer,
	plcount bigint,
	cluster_is_in_cell boolean,
	ldsity_d double precision,
	ldsity_d_plus double precision,
	is_aux boolean,
	is_target boolean,
	geom geometry(MultiPoint),
	ldsity_res_d double precision,
	ldsity_res_d_plus double precision,
	pix double precision,
	sweight double precision,
	DELTA_T__G_beta double precision,
	nb_sampling_units integer, 
	sweight_strata_sum double precision, 
	lambda_d_plus double precision
) AS
$BODY$
DECLARE
	_complete_query text;
BEGIN
	--------------------------------QUERY--------------------------------
	_complete_query := '
WITH
---------------------------------------------------------
-- Data block
---------------------------------------------------------
w_ldsity_plot AS MATERIALIZED (
	select
		f_p_plot.gid,
		t_total_estimate_conf.id as conf_id,
		t_panel.stratum,
		t_panel2total_2ndph_estimate_conf.panel,
		t_cluster.id as cluster,
		cm_cluster2panel_mapping.sampling_weight,
		t_total_estimate_conf.target_variable as attribute,
		plots_per_cluster,
		true AS plot_is_in_cell,
		coalesce(t_target_data.value, 0) as ldsity,
		f_p_plot.geom
	from @extschema@.t_total_estimate_conf
	inner join @extschema@.t_panel2total_2ndph_estimate_conf on t_total_estimate_conf.id = t_panel2total_2ndph_estimate_conf.total_estimate_conf
	inner join @extschema@.t_panel ON t_panel.id = t_panel2total_2ndph_estimate_conf.panel
	inner join @extschema@.cm_cluster2panel_mapping ON cm_cluster2panel_mapping.panel = t_panel.id
	inner join @extschema@.t_cluster ON t_cluster.id = cm_cluster2panel_mapping."cluster"
	inner join @extschema@.f_p_plot ON f_p_plot."cluster" = t_cluster.id
	inner join @extschema@.cm_plot2cluster_config_mapping on f_p_plot.gid = cm_plot2cluster_config_mapping.plot
	inner join @extschema@.t_cluster_configuration ON (t_panel.cluster_configuration = t_cluster_configuration.id
											and cm_plot2cluster_config_mapping.cluster_configuration = t_cluster_configuration.id)
	inner join @extschema@.t_stratum ON t_panel.stratum = t_stratum.id
	inner join @extschema@.cm_plot2cell_mapping ON (cm_plot2cell_mapping.estimation_cell = t_total_estimate_conf.estimation_cell 
											and cm_plot2cell_mapping.plot = f_p_plot.gid)
	left join @extschema@.t_target_data on (
		f_p_plot.gid = t_target_data.plot and
		t_panel2total_2ndph_estimate_conf.reference_year_set = t_target_data.reference_year_set and
		t_total_estimate_conf.target_variable = t_target_data.variable and
		t_target_data.is_latest)
	where t_total_estimate_conf.id = ' || conf_id || '
)
, w_ldsity_cluster AS MATERIALIZED (
	SELECT
		w_ldsity_plot.conf_id,
		concat(w_ldsity_plot.conf_id, ROW_NUMBER() OVER ())::bigint as gid,
		w_ldsity_plot.cluster,
		w_ldsity_plot.attribute,
		w_ldsity_plot.stratum,
                w_ldsity_plot.panel,
		w_ldsity_plot.plots_per_cluster,
		w_ldsity_plot.sampling_weight,
		count(*) as plcount,
		sum(w_ldsity_plot.plot_is_in_cell::int) > 0 as cluster_is_in_cell,
		sum(w_ldsity_plot.ldsity * plot_is_in_cell::int)/w_ldsity_plot.plots_per_cluster AS ldsity_D,
		sum(w_ldsity_plot.ldsity)/w_ldsity_plot.plots_per_cluster AS ldsity_D_plus,
		st_collect(w_ldsity_plot.geom)::geometry(MultiPoint) as geom
	FROM       w_ldsity_plot
	GROUP BY conf_id, stratum, panel, cluster, sampling_weight, plots_per_cluster, attribute
	ORDER BY stratum, cluster, attribute
)
, w_strata_sum AS MATERIALIZED (
	select
		t.conf_id,
		t_panel.stratum,
		case when t_cluster_configuration.cluster_design then
			t_stratum.buffered_area_m2/10000
		else
			t_stratum.area_m2/10000
		end as lambda_d_plus,
		sum(t_panel.cluster_count) as nb_sampling_units,
		sum(t_panel.sweight_panel_sum) as sweight_strata_sum
	from @extschema@.t_stratum
	inner join @extschema@.t_panel on (t_panel.stratum = t_stratum.id)
	inner join @extschema@.t_cluster_configuration on (t_panel.cluster_configuration = t_cluster_configuration.id)
	inner join (select distinct conf_id, panel from w_ldsity_plot) as t on (t_panel.id = t.panel)
	group by conf_id, t_panel.stratum, lambda_d_plus
)
, w_1p_data AS MATERIALIZED (
	select
		w_ldsity_cluster.gid, w_ldsity_cluster.cluster,
		w_ldsity_cluster.attribute, w_ldsity_cluster.stratum, w_ldsity_cluster.plots_per_cluster,
		w_ldsity_cluster.plcount, w_ldsity_cluster.cluster_is_in_cell, w_ldsity_cluster.ldsity_d,
		w_ldsity_cluster.ldsity_d_plus, false::boolean as is_aux, true::boolean as is_target,
		w_ldsity_cluster.geom,
		NULL::double precision as ldsity_res_D, NULL::double precision as ldsity_res_D_plus,
		NULL::double precision as pix,  w_ldsity_cluster.sampling_weight as sweight, NULL::double precision as DELTA_T__G_beta,
		w_strata_sum.nb_sampling_units::integer, w_strata_sum.sweight_strata_sum, w_strata_sum.lambda_d_plus
	from w_ldsity_cluster
	INNER JOIN w_strata_sum ON w_ldsity_cluster.stratum = w_strata_sum.stratum
)
select * from w_1p_data;
';

	--RAISE NOTICE '%', _complete_query;
	RETURN QUERY EXECUTE _complete_query;
END;
$BODY$
  LANGUAGE plpgsql STABLE
  COST 100
  ROWS 100000;

COMMENT ON FUNCTION @extschema@.fn_1p_data(integer) IS 'Function preparing data for regression estimate.';

-- </function>
