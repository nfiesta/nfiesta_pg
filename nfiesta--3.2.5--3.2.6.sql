--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

CREATE UNIQUE INDEX cm_plot2cell_mapping_plot_est_cell_idx ON @extschema@.cm_plot2cell_mapping (plot, estimation_cell);
ALTER TABLE @extschema@.cm_plot2cell_mapping ADD CONSTRAINT cm_plot2cell_mapping__unique_estimation_cell UNIQUE USING INDEX cm_plot2cell_mapping_plot_est_cell_idx;

-- <function name="fn_delete_param_area" schema="extschema" src="functions/extschema/configuration/fn_delete_param_area.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_delete_param_area(integer)
--DROP FUNCTION @extschema@.fn_delete_param_area(integer);
CREATE OR REPLACE FUNCTION @extschema@.fn_delete_param_area(_aux_conf integer)
RETURNS void
AS
$function$
DECLARE
BEGIN

-- 2nd order tables
-- t_aux_conf
PERFORM	@extschema@.fn_delete_aux_conf(id)
FROM	@extschema@.t_aux_conf
WHERE	param_area = $1;

-- cm_cell2param_area_mapping
DELETE FROM @extschema@.cm_cell2param_area_mapping WHERE param_area = $1;
RAISE NOTICE 'Deleting cell to parametrization area mapping...';
-- cm_plot2param_area_mapping
DELETE FROM @extschema@.cm_plot2param_area_mapping WHERE param_area = $1;
RAISE NOTICE 'Deleting plot to parametrization area mapping...';

-- 1st order table
-- t_aux_conf
DELETE FROM @extschema@.f_a_param_area WHERE gid = $1;
RAISE NOTICE 'Deleting parametrization area = %.', $1;

RETURN;
END;
$function$
LANGUAGE plpgsql
PARALLEL UNSAFE;

COMMENT ON FUNCTION @extschema@.fn_delete_param_area(integer) IS 'Function for deleting the parametrization area and all records in referenced tables.';


-- </function>

-- <function name="fn_1p_data" schema="extschema" src="functions/extschema/fn_1p_data.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_1p_data(integer)

-- DROP FUNCTION @extschema@.fn_1p_data(integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_1p_data(
    IN conf_id integer
)
  RETURNS TABLE(
	gid bigint,
	cluster integer,
	attribute integer,
	stratum integer,
	plots_per_cluster integer,
	plcount bigint,
	cluster_is_in_cell boolean,
	ldsity_d double precision,
	ldsity_d_plus double precision,
	is_aux boolean,
	is_target boolean,
	geom geometry(MultiPoint),
	ldsity_res_d double precision,
	ldsity_res_d_plus double precision,
	pix double precision,
	sweight double precision,
	DELTA_T__G_beta double precision,
	nb_sampling_units integer,
	sweight_strata_sum double precision,
	lambda_d_plus double precision
) AS
$BODY$
DECLARE
	_complete_query text;
BEGIN
	--------------------------------QUERY--------------------------------
	_complete_query := '
WITH
---------------------------------------------------------
-- Data block
---------------------------------------------------------
w_ldsity_plot AS MATERIALIZED (
	select
		f_p_plot.gid,
		t_total_estimate_conf.id as conf_id,
		t_panel.stratum,
		t_cluster.id as cluster,
		t_panel_refyearset_group.reference_year_set,
		t_panel_refyearset_group.panel,
		cm_cluster2panel_mapping.sampling_weight_ha as sampling_weight,
		t_total_estimate_conf.variable as attribute,
		plots_per_cluster,
		true AS plot_is_in_cell,
		coalesce(tdads.value, 0) as ldsity,
		f_p_plot.geom
	from @extschema@.t_total_estimate_conf
	inner join @extschema@.c_panel_refyearset_group on t_total_estimate_conf.panel_refyearset_group = c_panel_refyearset_group.id
	inner join @extschema@.t_panel_refyearset_group on t_panel_refyearset_group.panel_refyearset_group = c_panel_refyearset_group.id
	inner join sdesign.t_panel ON t_panel.id = t_panel_refyearset_group.panel
	inner join sdesign.cm_cluster2panel_mapping ON cm_cluster2panel_mapping.panel = t_panel.id
	inner join sdesign.t_cluster ON t_cluster.id = cm_cluster2panel_mapping."cluster"
	inner join sdesign.f_p_plot ON f_p_plot."cluster" = t_cluster.id
	inner join sdesign.cm_plot2cluster_config_mapping on f_p_plot.gid = cm_plot2cluster_config_mapping.plot
	inner join sdesign.t_cluster_configuration ON (t_panel.cluster_configuration = t_cluster_configuration.id
											and cm_plot2cluster_config_mapping.cluster_configuration = t_cluster_configuration.id)
	inner join sdesign.t_stratum ON t_panel.stratum = t_stratum.id
	inner join @extschema@.cm_plot2cell_mapping ON (cm_plot2cell_mapping.estimation_cell = t_total_estimate_conf.estimation_cell
											and cm_plot2cell_mapping.plot = f_p_plot.gid)
	left join (	select 
				plot, reference_year_set, variable, value, is_latest
			from @extschema@.t_target_data
			inner join @extschema@.t_available_datasets on (t_target_data.available_datasets = t_available_datasets.id)
		) as tdads on (
			f_p_plot.gid = tdads.plot and
			t_panel_refyearset_group.reference_year_set = tdads.reference_year_set and
			t_total_estimate_conf.variable = tdads.variable and
			tdads.is_latest
		)
	where t_total_estimate_conf.id = ' || conf_id || '
)
, w_ldsity_cluster AS MATERIALIZED (
	SELECT
		w_ldsity_plot.conf_id,
		concat(w_ldsity_plot.conf_id, ROW_NUMBER() OVER ())::bigint as gid,
		w_ldsity_plot.cluster,
		w_ldsity_plot.attribute,
		w_ldsity_plot.stratum,
                w_ldsity_plot.panel,
		w_ldsity_plot.plots_per_cluster,
		w_ldsity_plot.sampling_weight,
		count(*) as plcount,
		sum(w_ldsity_plot.plot_is_in_cell::int) > 0 as cluster_is_in_cell,
		sum(w_ldsity_plot.ldsity * plot_is_in_cell::int)/w_ldsity_plot.plots_per_cluster AS ldsity_D,
		sum(w_ldsity_plot.ldsity)/w_ldsity_plot.plots_per_cluster AS ldsity_D_plus,
		st_collect(w_ldsity_plot.geom)::geometry(MultiPoint) as geom
	FROM       w_ldsity_plot
	GROUP BY conf_id, stratum, panel, cluster, sampling_weight, plots_per_cluster, attribute
	ORDER BY stratum, cluster, attribute
)
, w_strata_sum AS MATERIALIZED (
	select
		t.conf_id,
		t_panel.stratum,
		case when t_cluster_configuration.cluster_design then
			t_stratum.frame_area_ha
		else
			t_stratum.area_ha
		end as lambda_d_plus,
		sum(t_panel.cluster_count) as nb_sampling_units,
		sum(t_panel.sweight_panel_sum) as sweight_strata_sum
	from sdesign.t_stratum
	inner join sdesign.t_panel on (t_panel.stratum = t_stratum.id)
	inner join sdesign.t_cluster_configuration on (t_panel.cluster_configuration = t_cluster_configuration.id)
	inner join (select distinct conf_id, panel from w_ldsity_plot) as t on (t_panel.id = t.panel)
	group by conf_id, t_panel.stratum, lambda_d_plus
)
, w_1p_data AS MATERIALIZED (
	select
		w_ldsity_cluster.gid, w_ldsity_cluster.cluster,
		w_ldsity_cluster.attribute, w_ldsity_cluster.stratum, w_ldsity_cluster.plots_per_cluster,
		w_ldsity_cluster.plcount, w_ldsity_cluster.cluster_is_in_cell, w_ldsity_cluster.ldsity_d,
		w_ldsity_cluster.ldsity_d_plus, false::boolean as is_aux, true::boolean as is_target,
		w_ldsity_cluster.geom,
		NULL::double precision as ldsity_res_D, NULL::double precision as ldsity_res_D_plus,
		NULL::double precision as pix,  w_ldsity_cluster.sampling_weight as sweight, NULL::double precision as DELTA_T__G_beta,
		w_strata_sum.nb_sampling_units::integer, w_strata_sum.sweight_strata_sum, w_strata_sum.lambda_d_plus
	from w_ldsity_cluster
	INNER JOIN w_strata_sum ON w_ldsity_cluster.stratum = w_strata_sum.stratum
)
select * from w_1p_data;
';

	--RAISE NOTICE '%', _complete_query;
	RETURN QUERY EXECUTE _complete_query;
END;
$BODY$
  LANGUAGE plpgsql STABLE
  COST 100
  ROWS 100000;

COMMENT ON FUNCTION @extschema@.fn_1p_data(integer) IS 'Function preparing data for regression estimate.';

-- </function>

-- <function name="fn_1p_total_var" schema="extschema" src="functions/extschema/fn_1p_total_var.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_1p_total_var(integer)

-- DROP FUNCTION @extschema@.fn_1p_total_var(integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_1p_total_var(
    IN conf_id integer
)
  RETURNS TABLE(
	attribute integer,
	point1p double precision,
	var1p double precision,
	point2p double precision,
	var2p double precision,
	min_ssize double precision,
	act_ssize bigint,
	est_info json
) AS
$BODY$
DECLARE
	_complete_query text;
BEGIN
	--------------------------------QUERY--------------------------------
	_complete_query := '
-------------------------------------------------------------------------------
------------------------------------2p TOTAL-----------------------------------
-------------------------------------------------------------------------------

	with w_data AS MATERIALIZED (
		select * from @extschema@.fn_1p_data(' || conf_id || ') where is_target
	)
-------------------------------------------------------------------------------
------------------------------------RESULTS------------------------------------
-------------------------------------------------------------------------------

------------------------------------2p
------------------------------------USING point SQL


------------------------------------2p var using HTC implemented in C function
--DROP EXTENSION htc; CREATE EXTENSION htc;
, w_data_agg AS MATERIALIZED (
	SELECT
		stratum, attribute, nb_sampling_units, sweight_strata_sum, lambda_d_plus as buffered_area_ha,
		array_agg(cluster order by cluster) as cids,
		array_agg(ldsity_d order by cluster) as ldsitys,
		array_agg(sweight * (lambda_d_plus / sweight_strata_sum) order by cluster) as sweights
	FROM
		w_data
		where ldsity_d != 0.0
		group by stratum, attribute, nb_sampling_units, sweight_strata_sum, buffered_area_ha
		order by attribute
)
, w_est1p_stratum AS MATERIALIZED (
	SELECT
		stratum, attribute,
		htc_compute_sweight_ha(cids, ldsitys, sweights, nb_sampling_units) as res
	from w_data_agg
)
, w_est1p AS MATERIALIZED (
	SELECT
		conf.attribute,
		coalesce(sum((res).total), 0.0) as point1p, coalesce(sum((res).var), 0.0) AS var1p
	from w_est1p_stratum
	right join (select distinct attribute from w_data) as conf on (w_est1p_stratum.attribute = conf.attribute)
	group by conf.attribute order by conf.attribute
)
, w_units_json AS MATERIALIZED (
	select attribute, array_to_json(array_agg(row_to_json(t))) as s_units from
	(
		select
			stratum, attribute,
			count(*) as s_units_param_area,
			sum(cluster_is_in_cell::int) as s_units_cell,
			sum((ldsity_d != 0)::int) as s_units_cell_nonzero
		from w_data
		group by stratum, attribute
		order by stratum, attribute
	) as t group by attribute
)
, w_minssize AS MATERIALIZED (
	with
	w_input as materialized (
		select attribute, stratum, sweight, ldsity_d as in_vec from w_data
	), w_check as (
		select count(*) != 1 as notpossible from (select stratum, sweight from w_input group by stratum, sweight) as t
	), w_sample_mean as (
		select attribute, sum(in_vec) / count(in_vec) as sample_mean_scalar, count(in_vec) as n from w_input group by attribute
	), w_res_pow_2 as (
		select attribute, sum(power(in_vec - sample_mean_scalar, 2)) as res_2 from w_input
			inner join w_sample_mean using (attribute) group by attribute
	), w_res_pow_3 as (
		select attribute, sum(power(in_vec - sample_mean_scalar, 3)) as res_3 from w_input
			inner join w_sample_mean using (attribute) group by attribute
	), w_g_one as (
		select attribute, power(n, 0.5) * (res_3 / power(res_2, 1.5)) as g_one from w_sample_mean
			inner join w_res_pow_2 using (attribute) inner join w_res_pow_3 using (attribute) group by attribute, n, res_2, res_3
	), w_g_one_big as (
		select attribute, n, power(n * (n-1), 0.5) / (n - 2) * g_one as g_one_big from w_sample_mean
			inner join w_g_one using (attribute) group by attribute, n, g_one
	), w_min_ssize as (
		select attribute, n, 25 * power(g_one_big, 2) as min_ssize from w_g_one_big group by attribute, n, g_one_big
	)
	select
		attribute,
		case when notpossible then -3 else
			case when (res_2 = 0.0) then -1 else
				(select min_ssize from w_min_ssize)
			end
		end as min_ssize,
		(select n from w_sample_mean) as act_ssize
	from w_res_pow_2, w_check
)
select
	w_est1p.attribute,
	point1p, var1p,
	NULL::double precision as point2p, NULL::double precision as var2p,
	min_ssize, act_ssize,
	s_units
from w_est1p
inner join w_units_json using (attribute)
inner join w_minssize using (attribute)
        ';

	--RAISE NOTICE '%', _complete_query;
	RETURN QUERY EXECUTE _complete_query;
END;
$BODY$
  LANGUAGE plpgsql STABLE
;

COMMENT ON FUNCTION @extschema@.fn_1p_total_var(integer) IS 'Function computing total and variance using regression estimate. 
Negative min_ssize has error code meaning, see https://gitlab.com/nfiesta/nfiesta_pg/-/wikis/Estimates-Calculation#minimal-sample-size-error-codes for comment.';

-- </function>

-- <function name="fn_1p1p_ratio_var" schema="extschema" src="functions/extschema/fn_1p1p_ratio_var.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_1p1p_ratio_var(integer, integer)

-- DROP FUNCTION @extschema@.fn_1p1p_ratio_var(integer, integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_1p1p_ratio_var(
    IN conf_id integer,
    IN conf_id__denom integer
)
  RETURNS TABLE(
	attribute integer,
	point1p double precision,
	var1p double precision,
	point2p double precision,
	var2p double precision,
	min_ssize double precision,
	act_ssize bigint,
	est_info json
) AS
$BODY$
DECLARE
	_complete_query text;
BEGIN
	--------------------------------QUERY--------------------------------
	_complete_query := '
-------------------------------------------------------------------------------
------------------------------------2p RATIO-----------------------------------
-------------------------------------------------------------------------------
	with w_conf AS MATERIALIZED (
            select nom.id as nom_conf_id, denom.id as denom_conf_id
            from (select * from @extschema@.t_total_estimate_conf where id = ' || conf_id || ') as nom
            , (select * from @extschema@.t_total_estimate_conf where id = ' || conf_id__denom || ') as denom
        )
------------------------------------nominator-----------------------------------
        , w_data__nom AS MATERIALIZED (
		select * from (select (@extschema@.fn_1p_data(nom_conf_id)).* from w_conf) as alldata where is_target
	)
	, w_data_agg__nom  AS MATERIALIZED (
		SELECT
			stratum, attribute, nb_sampling_units, sweight_strata_sum, lambda_d_plus as buffered_area_ha,
			array_agg(cluster order by cluster) as cids,
			array_agg(ldsity_d order by cluster) as ldsitys,
			array_agg(sweight * (lambda_d_plus / sweight_strata_sum) order by cluster) as sweights
		FROM
			w_data__nom
			where ldsity_d != 0.0
			group by stratum, attribute, nb_sampling_units, sweight_strata_sum, buffered_area_ha
			order by attribute
	)
	, w_est1p_stratum__nom  AS MATERIALIZED (
		SELECT
			stratum, attribute,
			htc_compute(cids, ldsitys, sweights, buffered_area_ha, sweight_strata_sum, nb_sampling_units) as res
		from w_data_agg__nom
	)
	, w_est1p__nom  AS MATERIALIZED (
		SELECT
			conf.attribute,
			coalesce(sum((res).total), 0.0) as point1p, coalesce(sum((res).var), 0.0) AS var1p
		from w_est1p_stratum__nom
		right join (select distinct attribute from w_data__nom) as conf on (w_est1p_stratum__nom.attribute = conf.attribute)
		group by conf.attribute order by conf.attribute
	)
------------------------------------denominator-----------------------------------
        , w_data__denom AS MATERIALIZED (
		select * from (select (@extschema@.fn_1p_data(denom_conf_id)).* from w_conf) as alldata where is_target
	)
	, w_data_agg__denom  AS MATERIALIZED (
		SELECT
			stratum, attribute, nb_sampling_units, sweight_strata_sum, lambda_d_plus as buffered_area_ha,
			array_agg(cluster order by cluster) as cids,
			array_agg(ldsity_d order by cluster) as ldsitys,
			array_agg(sweight * (lambda_d_plus / sweight_strata_sum) order by cluster) as sweights
		FROM
			w_data__denom
			where ldsity_d != 0.0
			group by stratum, attribute, nb_sampling_units, sweight_strata_sum, buffered_area_ha
			order by attribute
	)
	, w_est1p_stratum__denom  AS MATERIALIZED (
		SELECT
			stratum, attribute,
			htc_compute(cids, ldsitys, sweights, buffered_area_ha, sweight_strata_sum, nb_sampling_units) as res
		from w_data_agg__denom
	)
	, w_est1p__denom  AS MATERIALIZED (
		SELECT
			conf.attribute,
			coalesce(sum((res).total), 0.0) as point1p, coalesce(sum((res).var), 0.0) AS var1p
		from w_est1p_stratum__denom
		right join (select distinct attribute from w_data__denom) as conf on (w_est1p_stratum__denom.attribute = conf.attribute)
		group by conf.attribute order by conf.attribute
	)
-------------------------------------------------------------------------------
------------------------------------RESULTS------------------------------------
-------------------------------------------------------------------------------
------------------------------------2p var using HTC implemented in C function
, w_ratio_1p AS MATERIALIZED (
        SELECT nom.attribute as c, 
            case when denom.total = 0 then NULL::double precision else nom.total / denom.total end as val,
            denom.total as denom_total from 
        (select attribute, point1p::double precision as total from w_est1p__nom) as nom,
        (select attribute, point1p::double precision as total from w_est1p__denom) as denom
)
, w_data_1pr_agg AS MATERIALIZED (
        SELECT
	    stratum, w_ratio_1p.c as attribute, nb_sampling_units, sweight_strata_sum, lambda_d_plus as buffered_area_ha,
            array_agg(cluster order by cluster) as cids,
            array_agg(nom.ldsity_d - denom.ldsity_d * w_ratio_1p.val order by cluster) as z,
            array_agg(sweight order by cluster) as sweights
        FROM
            (select * from w_data__nom order by cluster) as nom
            inner join (select cluster, ldsity_d from w_data__denom order by cluster) as denom using (cluster)
            inner join w_ratio_1p on (nom.attribute = w_ratio_1p.c)
	group by stratum, w_ratio_1p.c, nb_sampling_units, sweight_strata_sum, buffered_area_ha order by attribute
)
, w_est1pr_stratum AS MATERIALIZED (
	SELECT 
            stratum, attribute, (htc_compute(cids, z, sweights, buffered_area_ha, sweight_strata_sum, nb_sampling_units)).* 
	    from w_data_1pr_agg
)
, w_est1pr AS MATERIALIZED (
	SELECT
	    attribute, sum(total) as point_residual, sum(var) AS var1p 
	    from w_est1pr_stratum group by attribute order by attribute
)
, w_1p_ratio_var AS MATERIALIZED (
	select 
            w_est1pr.attribute, 
	    w_ratio_1p.val as point1p, 
            case when denom_total = 0 then NULL::double precision else (1.0 / denom_total^2) * w_est1pr.var1p end as var1p
	    from     	w_est1pr 
	    inner join 	w_ratio_1p on (w_est1pr.attribute = w_ratio_1p.c)
)
, w_units_json AS MATERIALIZED (
	select attribute_nom as attribute, array_to_json(array_agg(row_to_json(t))) as s_units from
	(
		with w_nom as (select
			stratum, attribute,
			count(*) as s_units_param_area,
			sum(cluster_is_in_cell::int) as s_units_cell,
			sum((ldsity_d != 0)::int) as s_units_cell_nonzero
		from w_data__nom
		group by stratum, attribute
		order by stratum, attribute)
		, w_denom as (select
			stratum, attribute,
			count(*) as s_units_param_area,
			sum(cluster_is_in_cell::int) as s_units_cell,
			sum((ldsity_d != 0)::int) as s_units_cell_nonzero
		from w_data__denom
		group by stratum, attribute
		order by stratum, attribute)
		select
			w_nom.attribute			as attribute_nom,
			w_nom.stratum			as stratum_nom,
			w_nom.s_units_param_area	as s_units_param_area_nom,
			w_nom.s_units_cell		as s_units_cell_nom,
			w_nom.s_units_cell_nonzero	as s_units_cell_nonzero_nom,
			w_denom.attribute		as attribute_denom,
			w_denom.stratum			as stratum_denom,
			w_denom.s_units_param_area	as s_units_param_area_denom,
			w_denom.s_units_cell		as s_units_cell_denom,
			w_denom.s_units_cell_nonzero	as s_units_cell_nonzero_denom
		from w_nom inner join w_denom using (stratum) --!!!depends on condition, that it will be not used with multiple attributes in nom / denom setting
	) as t group by attribute
)
, w_minssize AS MATERIALIZED (
	with
	w_input as materialized (
		select attribute, stratum, unnest(sweights) as sweight, unnest(z) as in_vec from w_data_1pr_agg
	), w_check as (
		select count(*) != 1 as notpossible from (select stratum, sweight from w_input group by stratum, sweight) as t
	), w_sample_mean as (
		select attribute, sum(in_vec) / count(in_vec) as sample_mean_scalar, count(in_vec) as n from w_input group by attribute
	), w_res_pow_2 as (
		select attribute, sum(power(in_vec - sample_mean_scalar, 2)) as res_2 from w_input
			inner join w_sample_mean using (attribute) group by attribute
	), w_res_pow_3 as (
		select attribute, sum(power(in_vec - sample_mean_scalar, 3)) as res_3 from w_input
			inner join w_sample_mean using (attribute) group by attribute
	), w_g_one as (
		select attribute, power(n, 0.5) * (res_3 / power(res_2, 1.5)) as g_one from w_sample_mean
			inner join w_res_pow_2 using (attribute) inner join w_res_pow_3 using (attribute) group by attribute, n, res_2, res_3
	), w_g_one_big as (
		select attribute, n, power(n * (n-1), 0.5) / (n - 2) * g_one as g_one_big from w_sample_mean
			inner join w_g_one using (attribute) group by attribute, n, g_one
	), w_min_ssize as (
		select attribute, n, 25 * power(g_one_big, 2) as min_ssize from w_g_one_big group by attribute, n, g_one_big
	)
	select
		attribute,
		case when notpossible then -3 else
			case when (res_2 = 0.0) then -1 else
				case when (select point1p = 0.0 from w_est1p__denom) then -2 else
					(select min_ssize from w_min_ssize)
				end
			end
		end as min_ssize,
		(select n from w_sample_mean) as act_ssize
	from w_res_pow_2, w_check
)
select
	attribute,
	point1p, var1p,
	NULL::double precision as point2p, NULL::double precision as var2p,
	min_ssize, act_ssize,
	s_units
from w_1p_ratio_var
inner join w_units_json using (attribute)
inner join w_minssize using (attribute)
;
        ';

	--RAISE NOTICE '%', _complete_query;
	RETURN QUERY EXECUTE _complete_query;
END;
$BODY$
  LANGUAGE plpgsql STABLE
;

COMMENT ON FUNCTION @extschema@.fn_1p1p_ratio_var(integer, integer) IS 'Function computing ratio and variance using regression estimate. 
Negative min_ssize has error code meaning, see https://gitlab.com/nfiesta/nfiesta_pg/-/wikis/Estimates-Calculation#minimal-sample-size-error-codes for comment.';

-- </function>

-- <function name="fn_g_beta" schema="extschema" src="functions/extschema/fn_g_beta.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_G_beta(integer)

-- DROP FUNCTION @extschema@.fn_G_beta(integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_G_beta(
    IN conf_id integer
)
  RETURNS TABLE(
	variable integer,
	cluster integer,
	val double precision
) AS
$BODY$
DECLARE
	_complete_query text;
BEGIN
	--------------------------------QUERY--------------------------------
	_complete_query := '
WITH
w_configuration AS MATERIALIZED (
	select
		t_aux_conf.id, t_aux_conf.param_area, t_aux_conf.model, t_aux_conf.sigma, t_model.description,
		array_agg(t_model_variables.variable order by t_model_variables.variable) as aux_attributes,
		t_aux_conf.panel_refyearset_group
	from @extschema@.t_aux_conf
	inner join @extschema@.t_model ON t_model.id = t_aux_conf.model
	inner join @extschema@.t_model_variables ON t_model_variables.model = t_model.id
        where t_aux_conf.id = ' || conf_id || '
	group by t_aux_conf.id, t_aux_conf.param_area, t_aux_conf.model, t_model.description
)
, w_param_area_selection AS MATERIALIZED (
	SELECT
		w_configuration.id as conf_id,
		gid as param_area_gid,
		geom,
		panel_refyearset_group
	FROM @extschema@.f_a_param_area
	INNER JOIN w_configuration ON (w_configuration.param_area = f_a_param_area.gid)
)
, w_plot AS MATERIALIZED (-------------------------LIST OF PLOTS IN PARAMETRIZATION AREA
	select distinct
		w_param_area_selection.conf_id, f_p_plot.gid, t_cluster.id as cluster, t_panel.stratum,
		t_panel_refyearset_group.panel,
		f_p_plot.geom
	from w_param_area_selection
	inner join @extschema@.t_panel_refyearset_group on w_param_area_selection.panel_refyearset_group = t_panel_refyearset_group.panel_refyearset_group
	inner join sdesign.t_panel ON t_panel.id = t_panel_refyearset_group.panel
	inner join sdesign.cm_cluster2panel_mapping ON cm_cluster2panel_mapping.panel = t_panel.id
	inner join sdesign.t_cluster ON t_cluster.id = cm_cluster2panel_mapping."cluster"
	inner join sdesign.f_p_plot ON f_p_plot."cluster" = t_cluster.id
	inner join @extschema@.cm_plot2param_area_mapping ON cm_plot2param_area_mapping.plot = f_p_plot.gid
	inner join @extschema@.f_a_param_area ON (f_a_param_area.gid = cm_plot2param_area_mapping.param_area and w_param_area_selection.param_area_gid = f_a_param_area.gid)
)
, w_ldsity_plot AS MATERIALIZED (
	SELECT
		w_plot.conf_id,
		w_plot.gid,
		w_plot.stratum,
                w_plot.panel,
		w_plot.cluster,
		t_variable.id as attribute,
		t_cluster_configuration.plots_per_cluster,
		coalesce(adads.value, 0) as ldsity,
		w_plot.geom,
		true as is_aux
	FROM w_plot
	inner join sdesign.cm_plot2cluster_config_mapping on w_plot.gid = cm_plot2cluster_config_mapping.plot
	inner join sdesign.t_cluster_configuration on cm_plot2cluster_config_mapping.cluster_configuration = t_cluster_configuration.id
	inner join w_configuration on w_plot.conf_id = w_configuration.id
	inner join @extschema@.t_variable on t_variable.id = ANY (w_configuration.aux_attributes)
	left join (	select
			plot, variable, value, is_latest
		from @extschema@.t_auxiliary_data
		inner join @extschema@.t_available_datasets on (t_auxiliary_data.available_datasets = t_available_datasets.id)
	) as adads
	on (
		w_plot.gid = adads.plot and
		t_variable.id = adads.variable and
		adads.is_latest)
)
, w_ldsity_cluster AS MATERIALIZED (
 	SELECT
		w_ldsity_plot.conf_id,
		concat(w_ldsity_plot.conf_id, ROW_NUMBER() OVER ())::bigint as gid,
		w_ldsity_plot.cluster,
		w_ldsity_plot.attribute,
		w_ldsity_plot.stratum,
                w_ldsity_plot.panel,
		w_ldsity_plot.plots_per_cluster,
		count(*) as plcount,
		sum(w_ldsity_plot.ldsity)/w_ldsity_plot.plots_per_cluster AS ldsity_D_plus, -- eq 15,
		w_ldsity_plot.is_aux, --
		st_collect(w_ldsity_plot.geom)::geometry(MultiPoint) as geom
	FROM       w_ldsity_plot
	GROUP BY conf_id, stratum, panel, cluster, plots_per_cluster, attribute, is_aux
	ORDER BY stratum, cluster, attribute
)
, w_X AS MATERIALIZED ( -------------------------AUX LOCAL DENSITY ON TRACT LEVEL
	SELECT
		conf_id,
		attribute AS r,
		cluster AS c,
		ldsity_D_plus as val
	FROM w_ldsity_cluster
	WHERE is_aux
)
, w_clusters AS MATERIALIZED (-------------------------LIST OF TRACTS
	select distinct conf_id, stratum, panel, cluster from w_ldsity_cluster
)
, w_strata_sum AS MATERIALIZED (
	select
		w_configuration.id as conf_id,
                t_stratum.id as f_a_sampling_stratum_gid,
		case when t_cluster_configuration.cluster_design then
			t_stratum.frame_area_ha
		else
			t_stratum.area_ha
		end as lambda_d_plus,
		plots_per_cluster,
		sum(t_panel.cluster_count) as nb_sampling_units,
		sum(t_panel.sweight_panel_sum) as sweight_strata_sum
	from w_configuration
	inner join @extschema@.t_aux_conf on t_aux_conf.id = w_configuration.id
	inner join @extschema@.t_panel_refyearset_group ON t_panel_refyearset_group.panel_refyearset_group = t_aux_conf.panel_refyearset_group
	inner join sdesign.t_panel ON t_panel.id = t_panel_refyearset_group.panel
	inner join sdesign.t_cluster_configuration ON t_panel.cluster_configuration = t_cluster_configuration.id
	inner join sdesign.t_stratum ON t_stratum.id = t_panel.stratum
	group by w_configuration.id, f_a_sampling_stratum_gid, lambda_d_plus, plots_per_cluster
)
, w_pix AS MATERIALIZED (-------------------------INCLUSION DENSITY PIX
	SELECT
		w_clusters.conf_id,
		w_clusters.stratum,
		w_clusters.cluster,
		1.0 / (cm_cluster2panel_mapping.sampling_weight_ha * (v_strata_sum.lambda_d_plus / v_strata_sum.sweight_strata_sum)) as pix
	FROM w_clusters
        INNER JOIN sdesign.cm_cluster2panel_mapping ON (w_clusters.cluster = cm_cluster2panel_mapping.cluster
								and w_clusters.panel = cm_cluster2panel_mapping.panel)
	INNER JOIN w_strata_sum as v_strata_sum ON w_clusters.stratum = v_strata_sum.f_a_sampling_stratum_gid
)
, w_SIGMA AS MATERIALIZED (
	SELECT
		conf_id,
		1 as r,
		cluster as c,
		case when w_configuration.sigma then (plots_per_cluster^2)::float / plcount::float 
		else 1::float end
		as val
	from (select conf_id, cluster, plots_per_cluster, plcount 
		from w_ldsity_cluster 
		group by conf_id, cluster, plots_per_cluster, plcount) as m
	inner join w_configuration on (m.conf_id = w_configuration.id)
	order by r, c
)
, w_PI AS MATERIALIZED (
	SELECT
		conf_id,
		1 as r,
		cluster as c,
		1.0 / pix as val
	from w_pix
	order by r, c
)
, w_SIGMA_PI AS MATERIALIZED (
	SELECT
		A.conf_id,
		1 as r,
		A.c as c,
		A.val * B.val as val
	from w_SIGMA as A inner join w_PI as B on (A.c = B.c and A.conf_id = B.conf_id) 
	order by r, c
)
, w_XT AS MATERIALIZED (
	SELECT
		conf_id,
		c as r,
		r as c,
		val as val
	from w_X
	order by r, c
)
, w_X_SIGMA_PI AS MATERIALIZED ( -- element-wise multiplication
	select 
		A.conf_id,
		A.r, 
		A.c,
		A.val * B.val as val
	from w_X as A
	inner join w_SIGMA_PI as B on (A.c = B.c and A.conf_id = B.conf_id)
	ORDER BY r, c
)
, w_X_SIGMA_PI_XT AS MATERIALIZED ( -- matrix multiplication
	SELECT 
		A.conf_id,
		ROW_NUMBER() OVER (partition by A.conf_id order by A.r, B.c) AS mid,
		A.r, 
		B.c, 
		sum(A.val * B.val) as val
	FROM w_X_SIGMA_PI as A, w_XT as B
	WHERE A.c = B.r and A.conf_id = B.conf_id
	GROUP BY A.r, B.c, A.conf_id
	ORDER BY r, c
)
, w_agg AS MATERIALIZED (
	select conf_id, r, array_agg(val order by c) as val from w_X_SIGMA_PI_XT group by conf_id, r
)
, w_aggagg AS MATERIALIZED (
	select conf_id, array_agg(val order by r) as val from w_agg group by conf_id
)
, w_inv AS MATERIALIZED (
	select conf_id, @extschema@.fn_inverse(val) AS val from w_aggagg
)
, w_inv_id AS MATERIALIZED (
	select conf_id, mid, invval from w_inv, unnest(w_inv.val) WITH ORDINALITY AS t(invval, mid)
)
, w_X_SIGMA_PI_XT_inv AS MATERIALIZED (
	SELECT 
		conf_id, r, c, invval as val 
	FROM w_X_SIGMA_PI_XT 
	inner join w_inv_id using (conf_id, mid)
	ORDER BY r, c
)
, w_X_SIGMA_PI_XT_inv_X AS MATERIALIZED (
	select
		A.conf_id,
		A.r,
		B.c,
		sum(A.val * B.val) as val
	FROM
		w_X_SIGMA_PI_XT_inv as A, w_X as B
	WHERE A.c = B.r and A.conf_id = B.conf_id
	GROUP BY A.r, B.c, A.conf_id
	ORDER BY r, c
)
, w_G_beta AS MATERIALIZED (
	select
		A.conf_id,
		A.r,
		A.c,
		A.val * B.val as val
	FROM
		w_X_SIGMA_PI_XT_inv_X as A, w_SIGMA as B
	WHERE A.c = B.c and A.conf_id = B.conf_id
	ORDER BY r, c
)
select r as variable, c as cluster, val from w_G_beta;
        ';

	--RAISE NOTICE '%', _complete_query;
	RETURN QUERY EXECUTE _complete_query;
END;
$BODY$
  LANGUAGE plpgsql STABLE
  COST 100
  ROWS 100000;
;

COMMENT ON FUNCTION @extschema@.fn_G_beta(integer) IS 'Function computing matrix G_beta used for regression estimators. G_beta is dependent on model and parametrization domain (not cell). Matrix is represented by variable (row) and cluster (column) indices.';

-- </function>

-- <function name="fn_2p_data" schema="extschema" src="functions/extschema/fn_2p_data.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_2p_data(integer)

-- DROP FUNCTION @extschema@.fn_2p_data(integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_2p_data(
    IN conf_id integer
)
  RETURNS TABLE(
	gid bigint,
	cluster integer,
	attribute integer,
	stratum integer,
	plots_per_cluster integer,
	plcount bigint,
	cluster_is_in_cell boolean,
	ldsity_d double precision,
	ldsity_d_plus double precision,
	is_aux boolean,
	is_target boolean,
	geom geometry(MultiPoint),
	ldsity_res_d double precision,
	ldsity_res_d_plus double precision,
	pix double precision,
	sweight double precision,
	DELTA_T__G_beta double precision,
	nb_sampling_units integer,
	sweight_strata_sum double precision,
	lambda_d_plus double precision,
	sigma boolean
) AS
$$
begin
--------------------------------QUERY--------------------------------
---------------------------------------------------------
-- Configuration block
---------------------------------------------------------
execute '
create temporary table w_configuration ON COMMIT DROP AS (
	with w_a AS NOT MATERIALIZED (
		select
			t_total_estimate_conf.estimation_cell, t_aux_conf.model, 
			t_aux_conf.sigma, t_total_estimate_conf.force_synthetic, t_aux_conf.param_area,
			array_agg(t_total_estimate_conf.id order by t_total_estimate_conf.variable) as t_total_estimate_conf__id,
			array_agg(t_total_estimate_conf.panel_refyearset_group order by t_total_estimate_conf.variable) as t_total_estimate_conf__panel_refyearset_group,
			array_agg(t_aux_conf.id order by t_total_estimate_conf.variable) as t_aux_conf__id,
			array_agg(t_total_estimate_conf.variable order by t_total_estimate_conf.variable) as target_attributes,
			t_total_estimate_conf.aux_conf
		from @extschema@.t_total_estimate_conf
		inner join @extschema@.t_aux_conf ON t_aux_conf.id = t_total_estimate_conf.aux_conf
		where t_total_estimate_conf.id = $1
		group by t_total_estimate_conf.estimation_cell, t_aux_conf.model, t_aux_conf.sigma, t_total_estimate_conf.force_synthetic,
			t_aux_conf.param_area, t_total_estimate_conf.aux_conf
	)
	select
		t_total_estimate_conf__id[1] as id,
		t_total_estimate_conf__panel_refyearset_group[1] AS panel_refyearset_group,
		t_total_estimate_conf__panel_refyearset_group,
		w_a.t_total_estimate_conf__id, w_a.t_aux_conf__id,
		w_a.param_area, w_a.estimation_cell as cell, w_a.model, w_a.sigma, w_a.force_synthetic,
		array_agg(t_model_variables.variable order by t_model_variables.variable) as aux_attributes, w_a.target_attributes
	from w_a
	inner join @extschema@.t_aux_conf ON t_aux_conf.id = w_a.aux_conf
	inner join @extschema@.t_model ON t_model.id = t_aux_conf.model
	inner join @extschema@.t_model_variables ON t_model_variables.model = t_model.id
	group by w_a.estimation_cell, w_a.model, w_a.sigma, w_a.force_synthetic, w_a.param_area, w_a.target_attributes, 
		w_a.t_total_estimate_conf__id, w_a.t_total_estimate_conf__panel_refyearset_group, w_a.t_aux_conf__id
	order by id limit 1
);' using fn_2p_data.conf_id;
analyze w_configuration;

execute '
create temporary table w_cell_selection ON COMMIT DROP AS (
	SELECT
		w_configuration.id as conf_id,
		estimation_cell as estimation_cell,
		geom
	FROM @extschema@.f_a_cell
	INNER JOIN w_configuration ON (w_configuration.cell = f_a_cell.estimation_cell)
);';
analyze w_cell_selection;

execute '
create temporary table w_plot ON COMMIT DROP as (
WITH
w_param_area_selection AS NOT MATERIALIZED (
	SELECT
		w_configuration.id as conf_id,
		gid as param_area_gid,
		geom
	FROM @extschema@.f_a_param_area
	INNER JOIN w_configuration ON (w_configuration.param_area = f_a_param_area.gid)
)
---------------------------------------------------------
-- Data block
---------------------------------------------------------
, w_plot AS NOT MATERIALIZED (
	select distinct
		f_p_plot.gid, w_configuration.id as conf_id, t_cluster.id as cluster, t_panel.stratum,
		case when w_configuration.force_synthetic = True then False else cm_plot2cell_mapping.id IS NOT NULL end AS plot_is_in_cell, 
		-- t_panel2aux_conf.reference_year_set as reference_year_set, t_panel2aux_conf.panel as panel,
		t_panel_refyearset_group.reference_year_set as reference_year_set, t_panel_refyearset_group.panel as panel,
		f_p_plot.geom
	from w_configuration
        inner join @extschema@.t_panel_refyearset_group on w_configuration.panel_refyearset_group = t_panel_refyearset_group.panel_refyearset_group
	--inner join @extschema@.t_panel2aux_conf on w_configuration.t_aux_conf__id[1] = t_panel2aux_conf.aux_conf
	inner join sdesign.t_panel ON (t_panel.id = t_panel_refyearset_group.panel)
	inner join sdesign.cm_cluster2panel_mapping ON cm_cluster2panel_mapping.panel = t_panel.id
	inner join sdesign.t_cluster ON t_cluster.id = cm_cluster2panel_mapping."cluster"
	inner join sdesign.f_p_plot ON f_p_plot."cluster" = t_cluster.id
	left join @extschema@.cm_plot2cell_mapping ON (cm_plot2cell_mapping.estimation_cell = w_configuration.cell and cm_plot2cell_mapping.plot = f_p_plot.gid)
	inner join @extschema@.cm_plot2param_area_mapping ON (cm_plot2param_area_mapping.param_area = w_configuration.param_area and cm_plot2param_area_mapping.plot = f_p_plot.gid)
)
select * from w_plot
);';
analyze w_plot;

execute '
create temporary table w_ldsity_plot ON COMMIT DROP as (
	with w_plot AS NOT MATERIALIZED (select * from w_plot)
		select
			w_plot.gid,
			w_plot.conf_id,
			w_plot.stratum,
			w_plot.panel,
			w_plot.cluster,
			t_variable.id as attribute,
			t_cluster_configuration.plots_per_cluster,
			w_plot.plot_is_in_cell,
			coalesce(adads.value, 0) as ldsity,
			true as is_aux, false as is_target,
			w_plot.geom
		from w_plot as w_plot
		inner join sdesign.cm_plot2cluster_config_mapping on w_plot.gid = cm_plot2cluster_config_mapping.plot
		inner join sdesign.t_cluster_configuration on cm_plot2cluster_config_mapping.cluster_configuration = t_cluster_configuration.id
		inner join w_configuration on w_plot.conf_id = w_configuration.id
		inner join @extschema@.t_variable on t_variable.id = ANY (w_configuration.aux_attributes)
		left join (	select
				plot, variable, value, is_latest
			from @extschema@.t_auxiliary_data 
			inner join @extschema@.t_available_datasets on (t_auxiliary_data.available_datasets = t_available_datasets.id)
		) as adads
		on (
			w_plot.gid = adads.plot and
			t_variable.id = adads.variable and
			adads.is_latest)
	union all
		select
			w_plot.gid,
			w_plot.conf_id,
			w_plot.stratum,
			w_plot.panel,
			w_plot.cluster,
			t_variable.id as attribute,
			t_cluster_configuration.plots_per_cluster,
			w_plot.plot_is_in_cell,
			coalesce(tdads.value, 0) as ldsity,
			false as is_aux, true as is_target,
			w_plot.geom
		from w_plot as w_plot
		inner join sdesign.cm_plot2cluster_config_mapping on w_plot.gid = cm_plot2cluster_config_mapping.plot
		inner join sdesign.t_cluster_configuration on cm_plot2cluster_config_mapping.cluster_configuration = t_cluster_configuration.id
		inner join w_configuration on w_plot.conf_id = w_configuration.id
                inner join @extschema@.t_variable on (t_variable.id = ANY (w_configuration.target_attributes))
		left join (	select 
				plot, reference_year_set, variable, value, is_latest 
			from @extschema@.t_target_data 
			inner join @extschema@.t_available_datasets on (t_target_data.available_datasets = t_available_datasets.id)
		) as tdads
		on (
			w_plot.gid = tdads.plot and
			w_plot.reference_year_set = tdads.reference_year_set and
			t_variable.id = tdads.variable and
			tdads.is_latest)
);';
analyze w_ldsity_plot;

execute '
create temporary table w_ldsity_cluster ON COMMIT DROP as (
	SELECT
		w_ldsity_plot.conf_id,
		concat(w_ldsity_plot.conf_id, ROW_NUMBER() OVER ())::bigint as gid,
		w_ldsity_plot.cluster,
		w_ldsity_plot.attribute,
		w_ldsity_plot.stratum,
		w_ldsity_plot.panel,
		w_ldsity_plot.plots_per_cluster,
		count(*) as plcount,
		sum(w_ldsity_plot.plot_is_in_cell::int) > 0 as cluster_is_in_cell,
		sum(w_ldsity_plot.ldsity * plot_is_in_cell::int)/w_ldsity_plot.plots_per_cluster AS ldsity_D,
		sum(w_ldsity_plot.ldsity)/w_ldsity_plot.plots_per_cluster AS ldsity_D_plus,
		w_ldsity_plot.is_aux, w_ldsity_plot.is_target,
		st_collect(w_ldsity_plot.geom)::geometry(MultiPoint) as geom
	FROM       w_ldsity_plot
	GROUP BY conf_id, stratum, panel, cluster, plots_per_cluster, attribute, is_aux, is_target
	ORDER BY stratum, cluster, attribute
);';
analyze w_ldsity_cluster;

execute '
create temporary table w_clusters ON COMMIT DROP as (
	select distinct conf_id, stratum, panel, cluster, cluster_is_in_cell from w_ldsity_cluster
);';
analyze w_clusters;

execute '
create temporary table w_strata_sum ON COMMIT DROP as (
	select
		w_configuration.id as conf_id,
                t_stratum.id as f_a_sampling_stratum_gid,
		case when t_cluster_configuration.cluster_design then
			t_stratum.frame_area_ha
		else
			t_stratum.area_ha
		end as lambda_d_plus,
		plots_per_cluster,
		sum(t_panel.cluster_count) as nb_sampling_units,
		sum(t_panel.sweight_panel_sum) as sweight_strata_sum
	from w_configuration
	inner join @extschema@.t_aux_conf on t_aux_conf.id = ANY (w_configuration.t_aux_conf__id)
	inner join @extschema@.t_panel_refyearset_group ON t_panel_refyearset_group.panel_refyearset_group = t_aux_conf.panel_refyearset_group
	inner join sdesign.t_panel ON t_panel.id = t_panel_refyearset_group.panel
	inner join sdesign.t_cluster_configuration ON t_panel.cluster_configuration = t_cluster_configuration.id
	inner join sdesign.t_stratum ON t_stratum.id = t_panel.stratum
	group by w_configuration.id, f_a_sampling_stratum_gid, lambda_d_plus, plots_per_cluster
);';
analyze w_strata_sum;
---------------------------------------------------------
-- Matrix block
---------------------------------------------------------
execute '
create temporary table w_X ON COMMIT DROP AS (
	SELECT
		conf_id,
		attribute AS r,
		cluster AS c,
		ldsity_D as val_D,
		ldsity_D_plus as val
	FROM w_ldsity_cluster
	WHERE is_aux
);';
analyze w_X;

execute '
create temporary table w_Y ON COMMIT DROP AS (
	SELECT
		conf_id,
		attribute AS r,
		cluster AS c,
		ldsity_D as val_D,
		ldsity_D_plus as val
	FROM w_ldsity_cluster
	WHERE is_target
);';
analyze w_Y;

execute '
create temporary table w_pix ON COMMIT DROP as (
with
w_pix AS NOT MATERIALIZED (-------------------------INCLUSION DENSITY PIX
	SELECT
		w_clusters.conf_id,
		w_clusters.stratum,
		w_clusters.cluster,
		1.0 / (cm_cluster2panel_mapping.sampling_weight_ha * (w_strata_sum.lambda_d_plus / w_strata_sum.sweight_strata_sum)) as pix,
		cm_cluster2panel_mapping.sampling_weight_ha as sweight_ha
	FROM w_clusters
	INNER JOIN sdesign.cm_cluster2panel_mapping ON (w_clusters.cluster = cm_cluster2panel_mapping.cluster
								and w_clusters.panel = cm_cluster2panel_mapping.panel)
	INNER JOIN w_strata_sum as w_strata_sum ON w_clusters.stratum = w_strata_sum.f_a_sampling_stratum_gid
)
select * from w_pix
);';
analyze w_pix;

execute '
create temporary table w_t_G_beta ON COMMIT DROP as (
        select
                w_configuration.id as conf_id,
                t_g_beta.variable as r, t_g_beta.cluster as c, t_g_beta.val
        from @extschema@.t_g_beta
        inner join w_configuration on (t_g_beta.aux_conf = w_configuration.t_aux_conf__id[1])
	where t_g_beta.is_latest
);';
analyze w_t_G_beta;

execute '
create temporary table w_PI ON COMMIT DROP as (
	SELECT
		conf_id,
		1 as r,
		cluster as c,
		1.0 / pix as val
	from w_pix
	order by r, c
);';
analyze w_PI;

execute '
create temporary table w_DELTA_G_beta ON COMMIT DROP as (
with w_I AS NOT MATERIALIZED (
	SELECT 
		conf_id,
		1 AS r,
		cluster AS c,
		cluster_is_in_cell::int AS val
	FROM	w_clusters
	order by r, c
)
, w_SIGMA AS NOT MATERIALIZED (
	SELECT
		conf_id,
		1 as r,
		cluster as c,
		case when w_configuration.sigma then (plots_per_cluster^2)::float / plcount::float 
		else 1::float end
		as val
	from (select conf_id, cluster, plots_per_cluster, plcount 
		from w_ldsity_cluster 
		group by conf_id, cluster, plots_per_cluster, plcount) as m
	inner join w_configuration on (m.conf_id = w_configuration.id)
	order by r, c
)
, w_SIGMA_PI AS NOT MATERIALIZED (
	SELECT
		A.conf_id,
		1 as r,
		A.c as c,
		A.val * B.val as val
	from w_SIGMA as A 
	inner join w_PI as B on (A.c = B.c and A.conf_id = B.conf_id) 
	order by r, c
)
, w_t_hat AS NOT MATERIALIZED (  -- eq. 18
	select 
		A.conf_id,
		A.r,
		1 AS c,
		sum(A.val_D * B.val * C.val) as val
	from w_X 		AS A 
	inner join w_PI     	AS B 	on (A.c = B.c and A.conf_id = B.conf_id)
	inner join w_I		AS C 	on (A.c = C.c and A.conf_id = C.conf_id)
	group by A.r, A.conf_id
	order by A.r
)
, w_t AS NOT MATERIALIZED (
	with w_aux_total as (
		select estimation_cell as cell, t_variable.id as attribute, aux_total
		from @extschema@.t_aux_total
		inner join @extschema@.t_variable on (t_aux_total.variable = t_variable.id)
		where t_aux_total.is_latest
	)
	, w_aux_ldsity as (
		select distinct conf_id, r as attribute from w_X
	)
	select
		w_cell_selection.conf_id,
		w_aux_total.attribute as r,
		1 as c,
		coalesce(w_aux_total.aux_total,
			@extschema@.fn_raise_notice(
				format(''fn_2p_data.w_t: t_aux_total not found! (conf_id:%s attribute:%s)'',
					w_cell_selection.conf_id,
					w_aux_ldsity.attribute),
				''exception''
			)::int::float
		) as val
	from
	w_aux_ldsity
	inner join w_cell_selection on (w_aux_ldsity.conf_id = w_cell_selection.conf_id)
	left join w_aux_total on (w_aux_ldsity.attribute = w_aux_total.attribute and w_cell_selection.estimation_cell = w_aux_total.cell)
)
, w_DELTA_T AS NOT MATERIALIZED (
	select
		conf_id,
		c as r, 
		r as c, 
		w_t.val - w_t_hat.val as val 
	from w_t 
	inner join w_t_hat using (r, c, conf_id)
)
, w_DELTA_G_beta AS NOT MATERIALIZED (
	select
		A.conf_id,
		A.r,
		B.c, 
		sum(A.val * B.val) as val
	FROM 	w_DELTA_T as A
	inner join w_t_G_beta as B ON (A.c = B.r and A.conf_id = B.conf_id)
	GROUP BY A.r, B.c, A.conf_id
	order by r, c
)
select * from w_DELTA_G_beta
);';
analyze w_DELTA_G_beta;

execute '
create temporary table w_X_beta ON COMMIT DROP as (
with w_Y_T AS NOT MATERIALIZED (
	SELECT
		conf_id,
		c as r,
		r as c,
		val,
		val_D -- used for 1p estimete
	from w_Y
	order by r, c
)
, w_G_PI AS NOT MATERIALIZED (
	select 
		A.conf_id,
		A.r, 
		A.c AS c,
		A.val * B.val as val
	from w_t_G_beta AS A, w_PI AS B 
	where A.c = B.c and A.conf_id = B.conf_id
	order by r, c
)
, w_beta_hat AS NOT MATERIALIZED (
	select
		A.conf_id,
		A.r,
		B.c, 
		sum(A.val * B.val) as val
	FROM w_G_PI as A, w_Y_T as B
	WHERE A.c = B.r and A.conf_id = B.conf_id
	GROUP BY A.r, B.c, A.conf_id
	order by r, c
)
, w_X_plots_T AS NOT MATERIALIZED (
	SELECT conf_id, gid AS r, cluster, attribute AS c, plots_per_cluster, ldsity AS val 
	FROM w_ldsity_plot WHERE is_aux
)
, w_X_beta AS NOT MATERIALIZED (
	select
		A.conf_id,
		A.r,
		B.c, 
		sum(A.val * B.val) as val
	FROM w_X_plots_T as A, w_beta_hat as B
	WHERE A.c = B.r and A.conf_id = B.conf_id
	GROUP BY A.r, B.c, A.conf_id
	order by r, c 
)
select * from w_X_beta
);';
analyze w_X_beta;

execute '
create temporary table w_ldsity_residuals_cluster ON COMMIT DROP as (
with w_Y_plots_T AS NOT MATERIALIZED (
	SELECT conf_id, gid AS r, cluster, attribute AS c, plots_per_cluster, ldsity AS val 
	FROM w_ldsity_plot WHERE is_target
)
, w_residuals_plot AS NOT MATERIALIZED ( -------------------------RESIDUAL LOCAL DENSITY ON TRACT LEVEL
	select
		w_Y_plots_T.conf_id,
		w_Y_plots_T.r AS plot, 
		w_Y_plots_T.c AS attribute, 
		(w_Y_plots_T.val - w_X_beta.val) AS ldsity 
	from w_Y_plots_T 
	INNER JOIN w_X_beta USING (r, c, conf_id)
)
, w_residuals_cluster AS NOT MATERIALIZED (
	with
	w_residuals_plot AS NOT MATERIALIZED (select * from w_residuals_plot),
	w_ldsity_plot AS NOT MATERIALIZED (select * from w_ldsity_plot)
	SELECT
		w_ldsity_plot.conf_id,
		w_ldsity_plot.cluster,
		w_residuals_plot.attribute,
		w_ldsity_plot.stratum,
		w_ldsity_plot.plots_per_cluster,
		count(*) as plcount,
		sum(plot_is_in_cell::int) > 0 as cluster_is_in_cell,
		sum(w_residuals_plot.ldsity * plot_is_in_cell::int)/w_ldsity_plot.plots_per_cluster AS ldsity_res_D, --eq 38
		sum(w_residuals_plot.ldsity)/w_ldsity_plot.plots_per_cluster AS ldsity_res_D_plus -- eq 39
	FROM w_ldsity_plot AS w_ldsity_plot
	INNER JOIN w_residuals_plot AS w_residuals_plot ON w_ldsity_plot.gid = w_residuals_plot.plot AND w_ldsity_plot.attribute = w_residuals_plot.attribute and w_ldsity_plot.conf_id = w_residuals_plot.conf_id
	GROUP BY stratum, w_ldsity_plot.conf_id, w_ldsity_plot.cluster, w_ldsity_plot.plots_per_cluster, w_residuals_plot.attribute
	ORDER BY stratum, w_ldsity_plot.conf_id, w_ldsity_plot.cluster, w_residuals_plot.attribute
)
, w_ldsity_residuals_cluster AS NOT MATERIALIZED (
	select 
		w_ldsity_cluster.conf_id, w_ldsity_cluster.gid, w_ldsity_cluster.cluster, 
		w_ldsity_cluster.attribute, w_ldsity_cluster.stratum, w_ldsity_cluster.plots_per_cluster, 
		w_ldsity_cluster.plcount, w_ldsity_cluster.cluster_is_in_cell, w_ldsity_cluster.ldsity_d, w_ldsity_cluster.ldsity_d_plus, 
		w_ldsity_cluster.is_aux, w_ldsity_cluster.is_target, w_ldsity_cluster.geom,
		w_residuals_cluster.ldsity_res_D, w_residuals_cluster.ldsity_res_D_plus
	from w_ldsity_cluster
	LEFT JOIN w_residuals_cluster USING (conf_id, cluster, attribute, stratum)
)
select * from w_ldsity_residuals_cluster
);';
analyze w_ldsity_residuals_cluster;

--EXPLAIN (ANALYZE, COSTS, VERBOSE, BUFFERS/*, FORMAT JSON*/)
return query execute '
with w_2p_data AS NOT MATERIALIZED (
	with w_ldsity_residuals_cluster AS NOT MATERIALIZED (select * from w_ldsity_residuals_cluster)
	select 
		w_ldsity_residuals_cluster.conf_id, w_ldsity_residuals_cluster.gid, w_ldsity_residuals_cluster.cluster, 
		w_ldsity_residuals_cluster.attribute, w_ldsity_residuals_cluster.stratum, w_ldsity_residuals_cluster.plots_per_cluster, 
		w_ldsity_residuals_cluster.plcount, w_ldsity_residuals_cluster.cluster_is_in_cell, w_ldsity_residuals_cluster.ldsity_d, 
		w_ldsity_residuals_cluster.ldsity_d_plus, 
		w_ldsity_residuals_cluster.is_aux, w_ldsity_residuals_cluster.is_target, w_ldsity_residuals_cluster.geom,
		w_ldsity_residuals_cluster.ldsity_res_D, w_ldsity_residuals_cluster.ldsity_res_D_plus,
		w_pix.pix, w_pix.sweight_ha, w_DELTA_G_beta.val as DELTA_T__G_beta, 
		w_strata_sum.nb_sampling_units::integer, w_strata_sum.sweight_strata_sum, w_strata_sum.lambda_d_plus
	from w_ldsity_residuals_cluster
	INNER JOIN w_pix USING (conf_id, cluster, stratum)
	INNER JOIN w_DELTA_G_beta ON (w_pix.conf_id = w_DELTA_G_beta.conf_id and cluster = w_DELTA_G_beta.c)
	inner join w_strata_sum on (stratum = f_a_sampling_stratum_gid)
)
, w_2p_data_sigma AS NOT MATERIALIZED (
	select 
		w_2p_data.gid, w_2p_data.cluster, w_2p_data.attribute, w_2p_data.stratum, w_2p_data.plots_per_cluster, w_2p_data.plcount,
		w_2p_data.cluster_is_in_cell as cluster_is_in_cell, 
		w_2p_data.ldsity_d, w_2p_data.ldsity_d_plus, w_2p_data.is_aux, w_2p_data.is_target,
		w_2p_data.geom, w_2p_data.ldsity_res_d, w_2p_data.ldsity_res_d_plus, w_2p_data.pix, w_2p_data.sweight_ha as sweight, w_2p_data.DELTA_T__G_beta,
		w_2p_data.nb_sampling_units, w_2p_data.sweight_strata_sum, w_2p_data.lambda_d_plus, w_configuration.sigma
	from w_2p_data 
	inner join w_configuration on (w_2p_data.conf_id = w_configuration.id)
)
select * from w_2p_data_sigma;';

drop table w_configuration;
drop table w_cell_selection;
drop table w_plot;
drop table w_ldsity_plot;
drop table w_ldsity_cluster;
drop table w_clusters;
drop table w_strata_sum;
drop table w_X;
drop table w_Y;
drop table w_pix;
drop table w_t_G_beta;
drop table w_PI;
drop table w_DELTA_G_beta;
drop table w_X_beta;
drop table w_ldsity_residuals_cluster;
end;
$$
  LANGUAGE plpgsql
  COST 100
  ROWS 100000;

COMMENT ON FUNCTION @extschema@.fn_2p_data(integer) IS 'Function preparing data for regression estimate.';

-- </function>

-- <function name="fn_etl_import_ldsity_values" schema="extschema" src="functions/extschema/etl/fn_etl_import_ldsity_values.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_etl_import_ldsity_values(json)

-- DROP FUNCTION IF EXISTS @extschema@.fn_etl_import_ldsity_values(json) CASCADE;

create or replace function @extschema@.fn_etl_import_ldsity_values
(
	_available_datasets_and_ldsity_values json
)
returns text
as
$$
declare
		_max_id_ttd								integer;
		_max_id_tad								integer;
		_target_variable						integer;
		_target_variable_ad						integer;
		_country								varchar;
		_country_ad								varchar;
		_strata_sets							varchar[];
		_strata_sets_ad							varchar[];
		_stratums								varchar[];
		_stratums_ad							varchar[];
		_panels									varchar[];
		_panels_ad								varchar[];
		_reference_year_sets					varchar[];
		_reference_year_sets_ad					varchar[];
		_inventory_campaigns					varchar[];
		_inventory_campaigns_ad					varchar[];
		_clusters								varchar[];
		_cluster_configurations					varchar[];
		_cluster_configurations_ad				varchar[];
		_array_target_variable					integer[];
		_array_target_variable_ad				integer[];
		_array_sub_population_category			integer[];
		_array_sub_population_category_ad		integer[];
		_array_area_domain_category				integer[];
		_array_area_domain_category_ad			integer[];
		_array_panel							varchar[];
		_array_panel_ad							varchar[];
		_array_reference_year_set				varchar[];
		_array_reference_year_set_ad			varchar[];
		_check_cmrys2pm							integer;
		_check_cmrys2pm_ad						integer;
		_array_cmrys2pm							integer[];
		_array_cmrys2pm_ad						integer[];
		_check_count_records_input_json			integer;
		_check_count_records_after_inner_join	integer;
		_res_ttd								text;
		_res_tad								text;
		_res									text;
		_check_ldsity_threshold					integer;
		_check_ldsity_threshold_unique			integer;
begin
		if _available_datasets_and_ldsity_values is null
		then
			raise exception 'Error 01: fn_etl_import_ldsity_values: Input argument _available_datasets_and_ldsity_values must not by NULL!';
		end if;
	
		_max_id_ttd := (select coalesce(max(id),0) from @extschema@.t_target_data);
		_max_id_tad := (select coalesce(max(id),0) from @extschema@.t_available_datasets);

		---------------------------------------------------------------------------------
		-- check target variable [element "available_datasets"] --
		with
		w1 as	(select json_array_elements((_available_datasets_and_ldsity_values->>'available_datasets')::json) as s)
		,w2 as	(select	(s->>'target_variable')::integer as target_variable	from w1)
		select distinct w2.target_variable from w2
		into _target_variable_ad;

		if (select count(ctv.*) is distinct from 1 from @extschema@.c_target_variable as ctv where ctv.id = _target_variable_ad)
		then
			raise exception 'Error 02a: fn_etl_import_ldsity_values: Input JSON element "target_variable = %" in element "available_datasets" is not present in table c_target_variable!',_target_variable_ad;
		end if;

		-- check target variable [element "ldsity_values"] --
		with
		w1 as	(select json_array_elements((_available_datasets_and_ldsity_values->>'ldsity_values')::json) as s)
		,w2 as	(select	(s->>'target_variable')::integer as target_variable	from w1)
		select distinct w2.target_variable from w2
		into _target_variable;

		if (select count(ctv.*) is distinct from 1 from @extschema@.c_target_variable as ctv where ctv.id = _target_variable)
		then
			raise exception 'Error 02b: fn_etl_import_ldsity_values: Input JSON element "target_variable = %" in element "ldsity_values" is not present in table c_target_variable!',_target_variable;
		end if;
		---------------------------------------------------------------------------------
		-- check country [element "available_datasets"] --
		with
		w1 as	(select json_array_elements((_available_datasets_and_ldsity_values->>'available_datasets')::json) as s)
		,w2 as	(select	(s->>'country')::varchar as country	from w1)
		select distinct w2.country from w2
		into _country_ad;

		if (select count(cc.*) is distinct from 1 from sdesign.c_country as cc where cc.label = _country_ad)
		then
			raise exception 'Error 03a: fn_etl_import_ldsity_values: Input JSON element "country = %" in element "available_datasets" is not present in table sdesign.c_country!',_country_ad;
		end if;

		-- check country [element "ldsity_values"] --
		with
		w1 as	(select json_array_elements((_available_datasets_and_ldsity_values->>'ldsity_values')::json) as s)
		,w2 as	(select	(s->>'country')::varchar as country	from w1)
		select distinct w2.country from w2
		into _country;

		if (select count(cc.*) is distinct from 1 from sdesign.c_country as cc where cc.label = _country)
		then
			raise exception 'Error 03b: fn_etl_import_ldsity_values: Input JSON element "country = %" in element "ldsity_values" is not present in table sdesign.c_country!',_country;
		end if;
		---------------------------------------------------------------------------------
		-- check strata sets [element "available_datasets"] --
		with
		w1 as	(select json_array_elements((_available_datasets_and_ldsity_values->>'available_datasets')::json) as s)
		,w2 as	(select	(s->>'strata_set')::varchar as strata_set from w1)
		,w3 as	(select distinct w2.strata_set from w2)
		select array_agg(w3.strata_set order by w3.strata_set) from w3
		into _strata_sets_ad;

		for i in 1..array_length(_strata_sets_ad,1)
		loop
			if	(
				select count(tss.*) is distinct from 1 from sdesign.t_strata_set as tss where tss.country =
				(select cc.id from sdesign.c_country as cc where cc.label = _country_ad)
				and tss.strata_set = _strata_sets_ad[i]
				)
			then
				raise exception 'Error 04a: fn_etl_import_ldsity_values: Some of input JSON element "strata_set = %" in element "available_datasets" is not present in table sdesign.t_strata for input JSON element "country = %"!',_strata_sets_ad[i], _country_ad;
			end if;		
		end loop;

		-- check strata sets [element "ldsity_values"] --
		with
		w1 as	(select json_array_elements((_available_datasets_and_ldsity_values->>'ldsity_values')::json) as s)
		,w2 as	(select	(s->>'strata_set')::varchar as strata_set from w1)
		,w3 as	(select distinct w2.strata_set from w2)
		select array_agg(w3.strata_set order by w3.strata_set) from w3
		into _strata_sets;

		for i in 1..array_length(_strata_sets,1)
		loop
			if	(
				select count(tss.*) is distinct from 1 from sdesign.t_strata_set as tss where tss.country =
				(select cc.id from sdesign.c_country as cc where cc.label = _country)
				and tss.strata_set = _strata_sets[i]
				)
			then
				raise exception 'Error 04b: fn_etl_import_ldsity_values: Some of input JSON element "strata_set = %" in element "ldsity_values" is not present in table sdesign.t_strata for input JSON element "country = %"!',_strata_sets[i], _country;
			end if;		
		end loop;
		---------------------------------------------------------------------------------
		-- check stratums [element "available_datasets"] --
		with
		w1 as	(select json_array_elements((_available_datasets_and_ldsity_values->>'available_datasets')::json) as s)
		,w2 as	(select	(s->>'stratum')::varchar as stratum from w1)
		,w3 as	(select distinct w2.stratum from w2)
		select array_agg(w3.stratum order by w3.stratum) from w3
		into _stratums_ad;

		for i in 1..array_length(_stratums_ad,1)
		loop
			if	(
				select count(ts.*) is distinct from 1 from sdesign.t_stratum as ts where ts.strata_set in
				(select tss.id from sdesign.t_strata_set as tss where tss.country =
				(select cc.id from sdesign.c_country as cc where cc.label = _country_ad))
				and ts.stratum = _stratums_ad[i]
				)
			then
				raise exception 'Error 05a: fn_etl_import_ldsity_values: Some of input JSON element "stratum = %" in element "available_datasets" is not present in table sdesign.t_stratum for input JSON element "country = %"!',_stratums_ad[i], _country_ad;
			end if;	
		end loop;

		-- check stratums [element "ldsity_values"] --
		with
		w1 as	(select json_array_elements((_available_datasets_and_ldsity_values->>'ldsity_values')::json) as s)
		,w2 as	(select	(s->>'stratum')::varchar as stratum from w1)
		,w3 as	(select distinct w2.stratum from w2)
		select array_agg(w3.stratum order by w3.stratum) from w3
		into _stratums;

		for i in 1..array_length(_stratums,1)
		loop
			if	(
				select count(ts.*) is distinct from 1 from sdesign.t_stratum as ts where ts.strata_set in
				(select tss.id from sdesign.t_strata_set as tss where tss.country =
				(select cc.id from sdesign.c_country as cc where cc.label = _country))
				and ts.stratum = _stratums[i]
				)
			then
				raise exception 'Error 05b: fn_etl_import_ldsity_values: Some of input JSON element "stratum = %" in element "ldsity_values" is not present in table sdesign.t_stratum for input JSON element "country = %"!',_stratums[i], _country;
			end if;	
		end loop;
		---------------------------------------------------------------------------------
		-- check panels [element "available_datasets"] --
		with
		w1 as	(select json_array_elements((_available_datasets_and_ldsity_values->>'available_datasets')::json) as s)
		,w2 as	(select	(s->>'panel')::varchar as panel	from w1)
		,w3 as	(select distinct w2.panel from w2)
		select array_agg(w3.panel order by w3.panel) from w3
		into _panels_ad;

		for i in 1..array_length(_panels_ad,1)
		loop
			if	(
				select count(tp.*) is distinct from 1 from sdesign.t_panel as tp where tp.stratum in
				(select ts.id from sdesign.t_stratum as ts where ts.strata_set in
				(select tss.id from sdesign.t_strata_set as tss where tss.country = 
				(select cc.id from sdesign.c_country as cc where cc.label = _country_ad)))
				and tp.panel = _panels_ad[i]
				)
			then
				raise exception 'Error 06a: fn_etl_import_ldsity_values: Some of input JSON element "panel = %" in element "available_datasets" is not present in table sdesign.t_panel for input JSON element "country = %"!',_panels_ad[i], _country_ad;
			end if;		
		end loop;

		-- check panels [element "ldsity_values"] --
		with
		w1 as	(select json_array_elements((_available_datasets_and_ldsity_values->>'ldsity_values')::json) as s)
		,w2 as	(select	(s->>'panel')::varchar as panel	from w1)
		,w3 as	(select distinct w2.panel from w2)
		select array_agg(w3.panel order by w3.panel) from w3
		into _panels;

		for i in 1..array_length(_panels,1)
		loop
			if	(
				select count(tp.*) is distinct from 1 from sdesign.t_panel as tp where tp.stratum in
				(select ts.id from sdesign.t_stratum as ts where ts.strata_set in
				(select tss.id from sdesign.t_strata_set as tss where tss.country = 
				(select cc.id from sdesign.c_country as cc where cc.label = _country)))
				and tp.panel = _panels[i]
				)
			then
				raise exception 'Error 06b: fn_etl_import_ldsity_values: Some of input JSON element "panel = %" in element "ldsity_values" is not present in table sdesign.t_panel for input JSON element "country = %"!',_panels[i], _country;
			end if;		
		end loop;
		---------------------------------------------------------------------------------
		-- check reference year sets [element "available_datasets"] --
		with
		w1 as	(select json_array_elements((_available_datasets_and_ldsity_values->>'available_datasets')::json) as s)
		,w2 as	(select	(s->>'reference_year_set')::varchar as reference_year_set from w1)
		,w3 as	(select distinct w2.reference_year_set from w2)
		select array_agg(w3.reference_year_set order by w3.reference_year_set) from w3
		into _reference_year_sets_ad;

		for i in 1..array_length(_reference_year_sets_ad,1)
		loop
			if	(
				select count(trys.*) is distinct from 1 from sdesign.t_reference_year_set as trys where trys.id in
				(select cmr.reference_year_set from sdesign.cm_refyearset2panel_mapping as cmr where cmr.panel in
				(select tp.id from sdesign.t_panel as tp where tp.stratum in
				(select ts.id from sdesign.t_stratum as ts where ts.strata_set in
				(select tss.id from sdesign.t_strata_set as tss where tss.country = 
				(select cc.id from sdesign.c_country as cc where cc.label = _country_ad)))))
				and trys.reference_year_set = _reference_year_sets_ad[i]
				)
			then
				raise exception 'Error 07a: fn_etl_import_ldsity_values: Some of input JSON element "reference_year_set = %" in element "available_datasets" is not present in table sdesign.t_reference_year_set for input JSON element "country = %"!',_reference_year_sets_ad[i], _country_ad;
			end if;		
		end loop;

		-- check reference year sets [element "ldsity_values"] --
		with
		w1 as	(select json_array_elements((_available_datasets_and_ldsity_values->>'ldsity_values')::json) as s)
		,w2 as	(select	(s->>'reference_year_set')::varchar as reference_year_set from w1)
		,w3 as	(select distinct w2.reference_year_set from w2)
		select array_agg(w3.reference_year_set order by w3.reference_year_set) from w3
		into _reference_year_sets;

		for i in 1..array_length(_reference_year_sets,1)
		loop
			if	(
				select count(trys.*) is distinct from 1 from sdesign.t_reference_year_set as trys where trys.id in
				(select cmr.reference_year_set from sdesign.cm_refyearset2panel_mapping as cmr where cmr.panel in
				(select tp.id from sdesign.t_panel as tp where tp.stratum in
				(select ts.id from sdesign.t_stratum as ts where ts.strata_set in
				(select tss.id from sdesign.t_strata_set as tss where tss.country = 
				(select cc.id from sdesign.c_country as cc where cc.label = _country)))))
				and trys.reference_year_set = _reference_year_sets[i]
				)
			then
				raise exception 'Error 07b: fn_etl_import_ldsity_values: Some of input JSON element "reference_year_set = %" in element "ldsity_values" is not present in table sdesign.t_reference_year_set for input JSON element "country = %"!',_reference_year_sets[i], _country;
			end if;		
		end loop;
		---------------------------------------------------------------------------------
		-- check inventory campaigns [element "available_datasets"] --
		with
		w1 as	(select json_array_elements((_available_datasets_and_ldsity_values->>'available_datasets')::json) as s)
		,w2 as	(select	(s->>'inventory_campaign')::varchar as inventory_campaign from w1)
		,w3 as	(select distinct w2.inventory_campaign from w2)
		select array_agg(w3.inventory_campaign order by w3.inventory_campaign) from w3
		into _inventory_campaigns_ad;

		for i in 1..array_length(_inventory_campaigns_ad,1)
		loop
			if	(
				select count(tic.*) is distinct from 1 from sdesign.t_inventory_campaign as tic where tic.id in
				(select trys.inventory_campaign from sdesign.t_reference_year_set as trys where trys.id in
				(select cmr.reference_year_set from sdesign.cm_refyearset2panel_mapping as cmr where cmr.panel in
				(select tp.id from sdesign.t_panel as tp where tp.stratum in
				(select ts.id from sdesign.t_stratum as ts where ts.strata_set in
				(select tss.id from sdesign.t_strata_set as tss where tss.country = 
				(select cc.id from sdesign.c_country as cc where cc.label = _country_ad))))))
				and tic.inventory_campaign = _inventory_campaigns_ad[i]
				)
			then
				raise exception 'Error 08a: fn_etl_import_ldsity_values: Some of input JSON element "inventory_campaign = %" in element "available_datasets" is not present in table sdesign.t_inventory_campaign for input JSON element "country = %"!',_inventory_campaigns_ad[i], _country_ad;
			end if;		
		end loop;

		-- check inventory campaigns [element "ldsity_values"] --
		with
		w1 as	(select json_array_elements((_available_datasets_and_ldsity_values->>'ldsity_values')::json) as s)
		,w2 as	(select	(s->>'inventory_campaign')::varchar as inventory_campaign from w1)
		,w3 as	(select distinct w2.inventory_campaign from w2)
		select array_agg(w3.inventory_campaign order by w3.inventory_campaign) from w3
		into _inventory_campaigns;

		for i in 1..array_length(_inventory_campaigns,1)
		loop
			if	(
				select count(tic.*) is distinct from 1 from sdesign.t_inventory_campaign as tic where tic.id in
				(select trys.inventory_campaign from sdesign.t_reference_year_set as trys where trys.id in
				(select cmr.reference_year_set from sdesign.cm_refyearset2panel_mapping as cmr where cmr.panel in
				(select tp.id from sdesign.t_panel as tp where tp.stratum in
				(select ts.id from sdesign.t_stratum as ts where ts.strata_set in
				(select tss.id from sdesign.t_strata_set as tss where tss.country = 
				(select cc.id from sdesign.c_country as cc where cc.label = _country))))))
				and tic.inventory_campaign = _inventory_campaigns[i]
				)
			then
				raise exception 'Error 08b: fn_etl_import_ldsity_values: Some of input JSON element "inventory_campaign = %" in element "ldsity_values" is not present in table sdesign.t_inventory_campaign for input JSON element "country = %"!',_inventory_campaigns[i], _country;
			end if;		
		end loop;
		---------------------------------------------------------------------------------
		-- check clusters [element "ldsity_values"] --
		with
		w1 as	(select json_array_elements((_available_datasets_and_ldsity_values->>'ldsity_values')::json) as s)
		,w2 as	(select	(s->>'cluster')::varchar as cluster from w1)
		,w3 as	(select distinct w2.cluster from w2)
		select array_agg(w3.cluster order by w3.cluster) from w3
		into _clusters;

		for i in 1..array_length(_clusters,1)
		loop
			if	(
				select count(tc.*) is distinct from 1 from sdesign.t_cluster as tc where tc.id in
				(select ccpm.cluster from sdesign.cm_cluster2panel_mapping as ccpm where ccpm.panel in
				(select tp.id from sdesign.t_panel as tp where tp.stratum in
				(select ts.id from sdesign.t_stratum as ts where ts.strata_set in
				(select tss.id from sdesign.t_strata_set as tss where tss.country = 
				(select cc.id from sdesign.c_country as cc where cc.label = _country)))))
				and tc.cluster = _clusters[i]
				)
			then
				raise exception 'Error 09: fn_etl_import_ldsity_values: Some of input JSON element "cluster = %" is not present in table sdesign.t_cluster for input JSON element "country = %"!',_clusters[i], _country;
			end if;		
		end loop;
		---------------------------------------------------------------------------------
		-- check cluster configurations [element "available_datasets"] --
		with
		w1 as	(select json_array_elements((_available_datasets_and_ldsity_values->>'available_datasets')::json) as s)
		,w2 as	(select	(s->>'cluster_configuration')::varchar as cluster_configuration from w1)
		,w3 as	(select distinct w2.cluster_configuration from w2)
		select array_agg(w3.cluster_configuration order by w3.cluster_configuration) from w3
		into _cluster_configurations_ad;

		for i in 1..array_length(_cluster_configurations_ad,1)
		loop
			if	(
				select count(tcc.*) is distinct from 1 from sdesign.t_cluster_configuration as tcc where tcc.id in
				(select tp.cluster_configuration from sdesign.t_panel as tp where tp.stratum in
				(select ts.id from sdesign.t_stratum as ts where ts.strata_set in
				(select tss.id from sdesign.t_strata_set as tss where tss.country = 
				(select cc.id from sdesign.c_country as cc where cc.label = _country_ad))))
				and tcc.cluster_configuration = _cluster_configurations_ad[i]
				)
			then
				raise exception 'Error 10b: fn_etl_import_ldsity_values: Some of input JSON element "cluster_configuration = %" in element "available_datasets" is not present in table sdesign.t_cluster_configuration for input JSON element "country = %"!',_cluster_configurations_ad[i], _country_ad;
			end if;		
		end loop;

		-- check cluster configurations [element "ldsity_values"] --
		with
		w1 as	(select json_array_elements((_available_datasets_and_ldsity_values->>'ldsity_values')::json) as s)
		,w2 as	(select	(s->>'cluster_configuration')::varchar as cluster_configuration from w1)
		,w3 as	(select distinct w2.cluster_configuration from w2)
		select array_agg(w3.cluster_configuration order by w3.cluster_configuration) from w3
		into _cluster_configurations;

		for i in 1..array_length(_cluster_configurations,1)
		loop
			if	(
				select count(tcc.*) is distinct from 1 from sdesign.t_cluster_configuration as tcc where tcc.id in
				(select tp.cluster_configuration from sdesign.t_panel as tp where tp.stratum in
				(select ts.id from sdesign.t_stratum as ts where ts.strata_set in
				(select tss.id from sdesign.t_strata_set as tss where tss.country = 
				(select cc.id from sdesign.c_country as cc where cc.label = _country))))
				and tcc.cluster_configuration = _cluster_configurations[i]
				)
			then
				raise exception 'Error 10b: fn_etl_import_ldsity_values: Some of input JSON element "cluster_configuration = %" in element "ldsity_values" is not present in table sdesign.t_cluster_configuration for input JSON element "country = %"!',_cluster_configurations[i], _country;
			end if;		
		end loop;
		---------------------------------------------------------------------------------
		-- check combinations of target_variable, sub_population_category and area_domain_category [element "available_datasets"] --
		with
		w1 as	(
				select json_array_elements((_available_datasets_and_ldsity_values->>'available_datasets')::json) as s
				)
		,w2 as	(
				select
						(s->>'target_variable')::integer			as target_variable,
						(s->>'sub_population_category')::integer	as sub_population_category,
						(s->>'area_domain_category')::integer		as area_domain_category	
				from w1
				)
		,w3 as	(select distinct target_variable, sub_population_category, area_domain_category from w2)
		select
				array_agg(w3.target_variable order by w3.sub_population_category, w3.area_domain_category) as target_variable,
				array_agg(w3.sub_population_category order by w3.sub_population_category, w3.area_domain_category) as sub_population_category,
				array_agg(w3.area_domain_category order by w3.sub_population_category, w3.area_domain_category) as area_domain_category
		from
				w3
		into
				_array_target_variable_ad,
				_array_sub_population_category_ad,
				_array_area_domain_category_ad;

		for i in 1..array_length(_array_sub_population_category_ad,1)
		loop
			if	(
				with
				w1 as	(
						select
								id,
								target_variable,
								case when sub_population_category is null then 0 else sub_population_category end as sub_population_category,
								case when area_domain_category is null then 0 else area_domain_category end as area_domain_category
						from
								@extschema@.t_variable where target_variable = _array_target_variable_ad[i]
						)
				select count(w1.*) is distinct from 1 from w1
				where w1.sub_population_category = _array_sub_population_category_ad[i]
				and w1.area_domain_category = _array_area_domain_category_ad[i]
				)
			then
				raise exception 'Error 11a: fn_etl_import_ldsity_values: Some of input combination JSON elements "target_variable = %", "sub_population_category = %" and "area_domain_category = %" in element "available_datasets" is not present in table t_variable!',_array_target_variable_ad[i], _array_sub_population_category_ad[i], _array_area_domain_category_ad[i];
			end if;
		end loop;

		-- check combinations of target_variable, sub_population_category and area_domain_category [element "ldsity_values"] --
		with
		w1 as	(
				select json_array_elements((_available_datasets_and_ldsity_values->>'ldsity_values')::json) as s
				)
		,w2 as	(
				select
						(s->>'target_variable')::integer			as target_variable,
						(s->>'sub_population_category')::integer	as sub_population_category,
						(s->>'area_domain_category')::integer		as area_domain_category	
				from w1
				)
		,w3 as	(select distinct target_variable, sub_population_category, area_domain_category from w2)
		select
				array_agg(w3.target_variable order by w3.sub_population_category, w3.area_domain_category) as target_variable,
				array_agg(w3.sub_population_category order by w3.sub_population_category, w3.area_domain_category) as sub_population_category,
				array_agg(w3.area_domain_category order by w3.sub_population_category, w3.area_domain_category) as area_domain_category
		from
				w3
		into
				_array_target_variable,
				_array_sub_population_category,
				_array_area_domain_category;

		for i in 1..array_length(_array_sub_population_category,1)
		loop
			if	(
				with
				w1 as	(
						select
								id,
								target_variable,
								case when sub_population_category is null then 0 else sub_population_category end as sub_population_category,
								case when area_domain_category is null then 0 else area_domain_category end as area_domain_category
						from
								@extschema@.t_variable where target_variable = _array_target_variable[i]
						)
				select count(w1.*) is distinct from 1 from w1
				where w1.sub_population_category = _array_sub_population_category[i]
				and w1.area_domain_category = _array_area_domain_category[i]
				)
			then
				raise exception 'Error 11b: fn_etl_import_ldsity_values: Some of input combination JSON elements "target_variable = %", "sub_population_category = %" and "area_domain_category = %" in element "ldsity_values" is not present in table t_variable!',_array_target_variable[i], _array_sub_population_category[i], _array_area_domain_category[i];
			end if;
		end loop;
		---------------------------------------------------------------------------------
		-- check combinations of panel and reference year set [element "available_datasets"] --
		with
		w1 as	(
				select json_array_elements((_available_datasets_and_ldsity_values->>'available_datasets')::json) as s
				)
		,w2 as	(
				select
						(s->>'panel')::varchar				as panel,
						(s->>'reference_year_set')::varchar	as reference_year_set
				from w1
				)
		,w3 as	(select distinct panel, reference_year_set from w2)
		select
				array_agg(w3.panel order by w3.panel, w3.reference_year_set) as panel,
				array_agg(w3.reference_year_set order by w3.panel, w3.reference_year_set) as reference_year_set
		from
				w3
		into
				_array_panel_ad,
				_array_reference_year_set_ad;

		for i in 1..array_length(_array_panel_ad,1)
		loop
			select cmrys.id from sdesign.cm_refyearset2panel_mapping as cmrys
			where cmrys.panel =	(
							select tp.id from sdesign.t_panel as tp where tp.stratum in
							(select ts.id from sdesign.t_stratum as ts where ts.strata_set in
							(select tss.id from sdesign.t_strata_set as tss where tss.country = 
							(select cc.id from sdesign.c_country as cc where cc.label = _country_ad)))
							and tp.panel = _array_panel_ad[i]
							)
			and cmrys.reference_year_set =	(
											select trys.id from sdesign.t_reference_year_set as trys where trys.id in
											(select cmr.reference_year_set from sdesign.cm_refyearset2panel_mapping as cmr where cmr.panel in
											(select tp.id from sdesign.t_panel as tp where tp.stratum in
											(select ts.id from sdesign.t_stratum as ts where ts.strata_set in
											(select tss.id from sdesign.t_strata_set as tss where tss.country = 
											(select cc.id from sdesign.c_country as cc where cc.label = _country_ad)))))
											and trys.reference_year_set = _array_reference_year_set_ad[i]
											)
			into _check_cmrys2pm_ad;

			if _check_cmrys2pm_ad is null
			then
				raise exception 'Error 12a: fn_etl_import_ldsity_values: Some of input combination JSON elements "panel = %" and "reference_year_set = %" in element "available_datasets" is not present in table sdesign.cm_refyearset2panel_mapping for input JSON element "country = %"!',_array_panel_ad[i], _array_reference_year_set_ad[i], _country_ad;
			end if;

			if i = 1
			then
				_array_cmrys2pm_ad := array[_check_cmrys2pm_ad];
			else
				_array_cmrys2pm_ad := _array_cmrys2pm_ad || array[_check_cmrys2pm_ad];
			end if;
		end loop;

		-- check combinations of panel and reference year set [element "ldsity_values"] --
		with
		w1 as	(
				select json_array_elements((_available_datasets_and_ldsity_values->>'ldsity_values')::json) as s
				)
		,w2 as	(
				select
						(s->>'panel')::varchar				as panel,
						(s->>'reference_year_set')::varchar	as reference_year_set
				from w1
				)
		,w3 as	(select distinct panel, reference_year_set from w2)
		select
				array_agg(w3.panel order by w3.panel, w3.reference_year_set) as panel,
				array_agg(w3.reference_year_set order by w3.panel, w3.reference_year_set) as reference_year_set
		from
				w3
		into
				_array_panel,
				_array_reference_year_set;

		for i in 1..array_length(_array_panel,1)
		loop
			select cmrys.id from sdesign.cm_refyearset2panel_mapping as cmrys
			where cmrys.panel =	(
							select tp.id from sdesign.t_panel as tp where tp.stratum in
							(select ts.id from sdesign.t_stratum as ts where ts.strata_set in
							(select tss.id from sdesign.t_strata_set as tss where tss.country = 
							(select cc.id from sdesign.c_country as cc where cc.label = _country)))
							and tp.panel = _array_panel[i]
							)
			and cmrys.reference_year_set =	(
											select trys.id from sdesign.t_reference_year_set as trys where trys.id in
											(select cmr.reference_year_set from sdesign.cm_refyearset2panel_mapping as cmr where cmr.panel in
											(select tp.id from sdesign.t_panel as tp where tp.stratum in
											(select ts.id from sdesign.t_stratum as ts where ts.strata_set in
											(select tss.id from sdesign.t_strata_set as tss where tss.country = 
											(select cc.id from sdesign.c_country as cc where cc.label = _country)))))
											and trys.reference_year_set = _array_reference_year_set[i]
											)
			into _check_cmrys2pm;

			if _check_cmrys2pm is null
			then
				raise exception 'Error 12b: fn_etl_import_ldsity_values: Some of input combination JSON elements "panel = %" and "reference_year_set = %" in element "ldsity_values" is not present in table sdesign.cm_refyearset2panel_mapping for input JSON element "country = %"!',_array_panel[i], _array_reference_year_set[i], _country;
			end if;

			if i = 1
			then
				_array_cmrys2pm := array[_check_cmrys2pm];
			else
				_array_cmrys2pm := _array_cmrys2pm || array[_check_cmrys2pm];
			end if;
		end loop;
		---------------------------------------------------------------------------------
		-- check NOT NULL of ldsity_threshold [element "available_datasets"] --
		with
		w1 as	(
				select json_array_elements((_available_datasets_and_ldsity_values->>'available_datasets')::json) as s
				)
		,w2 as	(
				select
						(s->>'ldsity_threshold')::double precision as ldsity_threshold
				from w1
				)
		select count(w2.*) from w2 where w2.ldsity_threshold is null
		into _check_ldsity_threshold;

		if _check_ldsity_threshold > 0
		then
			raise exception 'Error 13: fn_etl_import_ldsity_values: In element "available_datasets->>ldsity_threshold" is NULL"!';
		end if;
		---------------------------------------------------------------------------------
		---------------------------------------------------------------------------------
		-- get count of records in input JSON in part ldsity_values
		with
		w1 as	(
				select json_array_elements((_available_datasets_and_ldsity_values->>'ldsity_values')::json) as s
				)
		,w2 as	(
				select
						(s->>'target_variable')::integer			as target_variable,
						(s->>'country')::varchar					as country,
						(s->>'strata_set')::varchar					as strata_set,
						(s->>'stratum')::varchar					as stratum,
						(s->>'reference_year_set')::varchar			as reference_year_set,
						(s->>'panel')::varchar						as panel,
						(s->>'cluster')::varchar					as cluster,
						(s->>'cluster_configuration')::varchar		as cluster_configuration,
						(s->>'inventory_campaign')::varchar			as inventory_campaign,
						(s->>'plot')::varchar						as plot
				from w1
				)
		select count(w2.*) from w2
		into _check_count_records_input_json;
		---------------------------------------------------------------------------------
		-- get count of records after inner join with sdesign for given combinations of panels and reference year sets
		with
		w1 as	(
				select json_array_elements((_available_datasets_and_ldsity_values->>'ldsity_values')::json) as s
				)
		,w2 as	(
				select
						(s->>'target_variable')::integer			as target_variable,
						(s->>'country')::varchar					as country,
						(s->>'strata_set')::varchar					as strata_set,
						(s->>'stratum')::varchar					as stratum,
						(s->>'reference_year_set')::varchar			as reference_year_set,
						(s->>'panel')::varchar						as panel,
						(s->>'cluster')::varchar					as cluster,
						(s->>'cluster_configuration')::varchar		as cluster_configuration,
						(s->>'inventory_campaign')::varchar			as inventory_campaign,
						(s->>'plot')::varchar						as plot
				from w1
				)
		,w3 as	(
				select		
						t_panel.id as t_panel__id,
						t_panel.panel as t_panel__panel,
						t_cluster.id as t_cluster__id,
						t_cluster.cluster as t_cluster__cluster,
						f_p_plot.gid as f_p_plot__gid,
						f_p_plot.plot as f_p_plot__plot,
						t_cluster_configuration.id as t_cluster_configuration__id,
						t_cluster_configuration.cluster_configuration as t_cluster_configuration__cluster_configuration,
						t_stratum.id as t_stratum__id,
						t_stratum.stratum as t_stratum__stratum,
						t_strata_set.id as t_strata_set__id,
						t_strata_set.strata_set as t_strata_set__strata_set,
						c_country.id as c_country__id,
						c_country.label as c_country__label,
						t_reference_year_set.id as t_reference_year_set__id,
						t_reference_year_set.reference_year_set as t_reference_year_set__reference_year_set,
						t_inventory_campaign.id as t_inventory_campaign__id,
						t_inventory_campaign.inventory_campaign as t_inventory_campaign__inventory
				from
							sdesign.cm_refyearset2panel_mapping
				inner join	sdesign.t_panel on t_panel.id = cm_refyearset2panel_mapping.panel
				inner join	sdesign.cm_cluster2panel_mapping on cm_cluster2panel_mapping.panel = t_panel.id
				inner join	sdesign.t_cluster on t_cluster.id = cm_cluster2panel_mapping."cluster"
				inner join	sdesign.f_p_plot on f_p_plot."cluster" = t_cluster.id
				inner join	sdesign.cm_plot2cluster_config_mapping on f_p_plot.gid = cm_plot2cluster_config_mapping.plot
				inner join	sdesign.t_cluster_configuration on (t_panel.cluster_configuration = t_cluster_configuration.id and cm_plot2cluster_config_mapping.cluster_configuration = t_cluster_configuration.id)
				inner join	sdesign.t_stratum on t_panel.stratum = t_stratum.id
				inner join	sdesign.t_strata_set on t_stratum.strata_set = t_strata_set.id
				inner join	sdesign.c_country on t_strata_set.country = c_country.id
				inner join	sdesign.t_reference_year_set on cm_refyearset2panel_mapping.reference_year_set = t_reference_year_set.id
				inner join	sdesign.t_inventory_campaign on t_reference_year_set.inventory_campaign = t_inventory_campaign.id
				where
						cm_refyearset2panel_mapping.id in (select unnest(_array_cmrys2pm))
				)
		,w4 as	(
				select
						w2.*, w3.f_p_plot__gid
				from
						w2 inner join w3
						on w2.plot = w3.f_p_plot__plot
						and w2.cluster = w3.t_cluster__cluster
						and w2.panel = w3.t_panel__panel
						and w2.stratum = w3.t_stratum__stratum
						and w2.reference_year_set = w3.t_reference_year_set__reference_year_set
						and w2.inventory_campaign = w3.t_inventory_campaign__inventory
						and w2.cluster_configuration = w3.t_cluster_configuration__cluster_configuration
						and w2.strata_set = w3.t_strata_set__strata_set
						and w2.country = w3.c_country__label
				)
		select count(w4.*) from w4
		into _check_count_records_after_inner_join;
		---------------------------------------------------------------------------------
		-- check records
		if _check_count_records_input_json is distinct from _check_count_records_after_inner_join
		then
				raise exception 'Error 14: fn_etl_import_ldsity_values: Composite key in input JSON elements is not compatible with SDESIGN on target DB!';
		end if;
		---------------------------------------------------------------------------------
		---------------------------------------------------------------------------------
		-- INSERT available datasets INTO t_available_datasets table
		with
		w1 as	(
				select json_array_elements((_available_datasets_and_ldsity_values->>'available_datasets')::json) as s
				)
		,w2 as	(
				select
						(s->>'target_variable')::integer			as target_variable,
						(s->>'country')::varchar					as country,
						(s->>'strata_set')::varchar					as strata_set,
						(s->>'stratum')::varchar					as stratum,
						(s->>'reference_year_set')::varchar			as reference_year_set,
						(s->>'panel')::varchar						as panel,
						(s->>'cluster_configuration')::varchar		as cluster_configuration,
						(s->>'inventory_campaign')::varchar			as inventory_campaign,
						(s->>'sub_population_category')::integer	as sub_population_category,
						(s->>'area_domain_category')::integer		as area_domain_category,
						(s->>'ldsity_threshold')::double precision	as ldsity_threshold
				from w1
				)
		,w3 as	(
				select distinct t.t_panel__id, t.t_panel__panel, t.t_cluster_configuration__id, t.t_cluster_configuration__cluster_configuration,
				t.t_stratum__id, t.t_stratum__stratum, t.t_strata_set__id, t.t_strata_set__strata_set, t.c_country__id, t.c_country__label,
				t.t_reference_year_set__id, t.t_reference_year_set__reference_year_set, t.t_inventory_campaign__id, t.t_inventory_campaign__inventory
				from
						(
						select		
								t_panel.id as t_panel__id,
								t_panel.panel as t_panel__panel,
								t_cluster.id as t_cluster__id,
								t_cluster.cluster as t_cluster__cluster,
								f_p_plot.gid as f_p_plot__gid,
								f_p_plot.plot as f_p_plot__plot,
								t_cluster_configuration.id as t_cluster_configuration__id,
								t_cluster_configuration.cluster_configuration as t_cluster_configuration__cluster_configuration,
								t_stratum.id as t_stratum__id,
								t_stratum.stratum as t_stratum__stratum,
								t_strata_set.id as t_strata_set__id,
								t_strata_set.strata_set as t_strata_set__strata_set,
								c_country.id as c_country__id,
								c_country.label as c_country__label,
								t_reference_year_set.id as t_reference_year_set__id,
								t_reference_year_set.reference_year_set as t_reference_year_set__reference_year_set,
								t_inventory_campaign.id as t_inventory_campaign__id,
								t_inventory_campaign.inventory_campaign as t_inventory_campaign__inventory
						from
									sdesign.cm_refyearset2panel_mapping
						inner join	sdesign.t_panel on t_panel.id = cm_refyearset2panel_mapping.panel
						inner join	sdesign.cm_cluster2panel_mapping on cm_cluster2panel_mapping.panel = t_panel.id
						inner join	sdesign.t_cluster on t_cluster.id = cm_cluster2panel_mapping."cluster"
						inner join	sdesign.f_p_plot on f_p_plot."cluster" = t_cluster.id
						inner join	sdesign.cm_plot2cluster_config_mapping on f_p_plot.gid = cm_plot2cluster_config_mapping.plot
						inner join	sdesign.t_cluster_configuration on (t_panel.cluster_configuration = t_cluster_configuration.id and cm_plot2cluster_config_mapping.cluster_configuration = t_cluster_configuration.id)
						inner join	sdesign.t_stratum on t_panel.stratum = t_stratum.id
						inner join	sdesign.t_strata_set on t_stratum.strata_set = t_strata_set.id
						inner join	sdesign.c_country on t_strata_set.country = c_country.id
						inner join	sdesign.t_reference_year_set on cm_refyearset2panel_mapping.reference_year_set = t_reference_year_set.id
						inner join	sdesign.t_inventory_campaign on t_reference_year_set.inventory_campaign = t_inventory_campaign.id
						where
								cm_refyearset2panel_mapping.id in (select unnest(_array_cmrys2pm_ad))
						) as t
				)
		,w4 as	(
				select
						w2.*,
						w3.t_panel__id,
						w3.t_reference_year_set__id
				from
						w2 inner join w3
						on w2.panel = w3.t_panel__panel
						and w2.stratum = w3.t_stratum__stratum
						and w2.reference_year_set = w3.t_reference_year_set__reference_year_set
						and w2.inventory_campaign = w3.t_inventory_campaign__inventory
						and w2.cluster_configuration = w3.t_cluster_configuration__cluster_configuration
						and w2.strata_set = w3.t_strata_set__strata_set
						and w2.country = w3.c_country__label
				)
		,w5 as	(
				select
						id,
						target_variable,
						case when sub_population_category is null then 0 else sub_population_category end as sub_population_category,
						case when area_domain_category is null then 0 else area_domain_category end as area_domain_category
				from
						@extschema@.t_variable 
				where target_variable = (select distinct w2.target_variable from w2)
				)
		,w6 as	(
				select
						w4.target_variable,
						w4.t_panel__id as panel,
						w4.t_reference_year_set__id as reference_year_set,
						w5.id as variable,
						w4.ldsity_threshold
				from
						w4 inner join w5 on (w4.sub_population_category = w5.sub_population_category and w4.area_domain_category = w5.area_domain_category)
				)
		,w7 as	(-- new records for insert without ldsity_threshold
				select w6.panel, w6.reference_year_set, w6.variable from w6
				except
				select panel, reference_year_set, variable from @extschema@.t_available_datasets
				--where variable in (select w6.variable from w6))
				)
		,w8 as	(-- recods for update without ldsity_threshold
				select w6.panel, w6.reference_year_set, w6.variable from w6
				except
				select w7.panel, w7.reference_year_set, w7.variable from w7
				)
		,w9 as	(
				-- new records for insert with ldsity_threshold
				select w6.* from w6 inner join w7
				on w6.panel = w7.panel
				and w6.reference_year_set = w7.reference_year_set
				and w6.variable = w7.variable
				)
		,w10 as	(
				-- records for update with ldsity_threshold
				select w6.* from w6 inner join w8
				on w6.panel = w8.panel
				and w6.reference_year_set = w8.reference_year_set
				and w6.variable = w8.variable
				)
		,w11 as	(
				update @extschema@.t_available_datasets
				set
					ldsity_threshold = w10.ldsity_threshold
				from
					w10
				where
					w10.panel = t_available_datasets.panel
				and
					w10.reference_year_set = t_available_datasets.reference_year_set
				and
					w10.variable = t_available_datasets.variable
				)
		insert into @extschema@.t_available_datasets(panel,reference_year_set,variable,ldsity_threshold,last_change)
		select w9.panel, w9.reference_year_set, w9.variable, w9.ldsity_threshold, now() from w9
		order by w9.panel, w9.reference_year_set, w9.variable;
		---------------------------------------------------------------------------------
		---------------------------------------------------------------------------------
		-- INSERT ldsity values INTO t_target_data table
		with
		w1 as	(
				select json_array_elements((_available_datasets_and_ldsity_values->>'ldsity_values')::json) as s
				)
		,w2 as	(
				select
						(s->>'target_variable')::integer			as target_variable,
						(s->>'country')::varchar					as country,
						(s->>'strata_set')::varchar					as strata_set,
						(s->>'stratum')::varchar					as stratum,
						(s->>'reference_year_set')::varchar			as reference_year_set,
						(s->>'panel')::varchar						as panel,
						(s->>'cluster')::varchar					as cluster,
						(s->>'cluster_configuration')::varchar		as cluster_configuration,
						(s->>'inventory_campaign')::varchar			as inventory_campaign,
						(s->>'plot')::varchar						as plot,
						(s->>'sub_population_category')::integer	as sub_population_category,
						(s->>'area_domain_category')::integer		as area_domain_category,
						(s->>'value')::float						as value
				from w1
				)
		,w3 as	(
				select		
						t_panel.id as t_panel__id,
						t_panel.panel as t_panel__panel,
						t_cluster.id as t_cluster__id,
						t_cluster.cluster as t_cluster__cluster,
						f_p_plot.gid as f_p_plot__gid,
						f_p_plot.plot as f_p_plot__plot,
						t_cluster_configuration.id as t_cluster_configuration__id,
						t_cluster_configuration.cluster_configuration as t_cluster_configuration__cluster_configuration,
						t_stratum.id as t_stratum__id,
						t_stratum.stratum as t_stratum__stratum,
						t_strata_set.id as t_strata_set__id,
						t_strata_set.strata_set as t_strata_set__strata_set,
						c_country.id as c_country__id,
						c_country.label as c_country__label,
						t_reference_year_set.id as t_reference_year_set__id,
						t_reference_year_set.reference_year_set as t_reference_year_set__reference_year_set,
						t_inventory_campaign.id as t_inventory_campaign__id,
						t_inventory_campaign.inventory_campaign as t_inventory_campaign__inventory
				from
							sdesign.cm_refyearset2panel_mapping
				inner join	sdesign.t_panel on t_panel.id = cm_refyearset2panel_mapping.panel
				inner join	sdesign.cm_cluster2panel_mapping on cm_cluster2panel_mapping.panel = t_panel.id
				inner join	sdesign.t_cluster on t_cluster.id = cm_cluster2panel_mapping."cluster"
				inner join	sdesign.f_p_plot on f_p_plot."cluster" = t_cluster.id
				inner join	sdesign.cm_plot2cluster_config_mapping on f_p_plot.gid = cm_plot2cluster_config_mapping.plot
				inner join	sdesign.t_cluster_configuration on (t_panel.cluster_configuration = t_cluster_configuration.id and cm_plot2cluster_config_mapping.cluster_configuration = t_cluster_configuration.id)
				inner join	sdesign.t_stratum on t_panel.stratum = t_stratum.id
				inner join	sdesign.t_strata_set on t_stratum.strata_set = t_strata_set.id
				inner join	sdesign.c_country on t_strata_set.country = c_country.id
				inner join	sdesign.t_reference_year_set on cm_refyearset2panel_mapping.reference_year_set = t_reference_year_set.id
				inner join	sdesign.t_inventory_campaign on t_reference_year_set.inventory_campaign = t_inventory_campaign.id
				where
						cm_refyearset2panel_mapping.id in (select unnest(_array_cmrys2pm))
				)		
		,w4 as	(
				select
						w2.*,
						w3.f_p_plot__gid,
						w3.t_panel__id,
						w3.t_reference_year_set__id
				from
						w2 inner join w3
						on w2.plot = w3.f_p_plot__plot
						and w2.cluster = w3.t_cluster__cluster
						and w2.panel = w3.t_panel__panel
						and w2.stratum = w3.t_stratum__stratum
						and w2.reference_year_set = w3.t_reference_year_set__reference_year_set
						and w2.inventory_campaign = w3.t_inventory_campaign__inventory
						and w2.cluster_configuration = w3.t_cluster_configuration__cluster_configuration
						and w2.strata_set = w3.t_strata_set__strata_set
						and w2.country = w3.c_country__label
				)
		,w5 as	(
				select
						id,
						target_variable,
						case when sub_population_category is null then 0 else sub_population_category end as sub_population_category,
						case when area_domain_category is null then 0 else area_domain_category end as area_domain_category
				from
						@extschema@.t_variable 
				where target_variable = (select distinct w2.target_variable from w2)
				)
		,w6 as	(
				select
						w4.target_variable,
						w4.f_p_plot__gid as plot,
						w4.value,
						w4.t_panel__id as panel,
						w4.t_reference_year_set__id as reference_year_set,
						w5.id as variable
				from
						w4 inner join w5 on (w4.sub_population_category = w5.sub_population_category and w4.area_domain_category = w5.area_domain_category)
				)
		,w7 as	(
				select distinct w6.target_variable, w6.plot, w6.panel, w6.value, w6.reference_year_set, w6.variable from w6
				)
		,w10 as	(
				select
						w7.*,
						tad.id as available_datasets,
						tad.ldsity_threshold
				from
						w7
						inner join @extschema@.t_available_datasets as tad
				on	(
					w7.panel = tad.panel and
					w7.reference_year_set = tad.reference_year_set and
					w7.variable = tad.variable
					)
				)
		,w_ttd as	(
					select
							ttd.id,
							ttd.plot,
							(	select t_panel.id as panel_upr
								from 		sdesign.f_p_plot
								inner join 	sdesign.t_cluster on (f_p_plot.cluster = t_cluster.id)
								inner join	sdesign.cm_cluster2panel_mapping on (t_cluster.id = cm_cluster2panel_mapping.cluster)
								inner join	sdesign.t_panel on (cm_cluster2panel_mapping.panel = t_panel.id)
								inner join	sdesign.t_cluster_configuration on (t_panel.cluster_configuration = t_cluster_configuration.id)
								inner join	sdesign.cm_plot2cluster_config_mapping on (t_cluster_configuration.id = cm_plot2cluster_config_mapping.cluster_configuration and
																f_p_plot.gid = cm_plot2cluster_config_mapping.plot)
								where f_p_plot.gid = coalesce(w10.plot,ttd.plot)
							),
							ttd.value,
							coalesce(w10.plot,ttd.plot) as plot_upr,
							coalesce(w10.available_datasets, ttd.available_datasets) as available_datasets_upr,
							case
								when ttd.value is     null and w10.value is not null then w10.value
								when ttd.value is not null and ttd.value is distinct from 0.0 and w10.value is null then 0.0
								when ttd.value is not null and ttd.value = 0.0 and w10.value is null then null::double precision
								else w10.value
							end
								as value_upr,
							case
								when ttd.value is     null and w10.value is not null then false
								when ttd.value is not null and ttd.value is distinct from 0.0 and w10.value is null then false
								when ttd.value is not null and ttd.value = 0.0 and w10.value is null then null::boolean									
								else
									case
										when ttd.value = 0.0 and w10.value = 0.0 then true
										when ttd.value = 0.0 and w10.value is distinct from 0.0
												then
													case
														when (abs(1 - (ttd.value / w10.value)) * 100.0) <= w10.ldsity_threshold then true
														else false
													end
										when ttd.value is distinct from 0.0 and w10.value = 0.0
												then
													case
														when (abs(1 - (w10.value / ttd.value)) * 100.0) <= w10.ldsity_threshold then true
														else false
													end
										else
											case
												when (abs(1 - (ttd.value / w10.value)) * 100.0) <= w10.ldsity_threshold then true
												else false
											end
									end									
							end
								as value_identic				
					from
						(
						select * 
						from @extschema@.t_target_data
						where available_datasets in (
							select id 
							from @extschema@.t_available_datasets 
							where panel in (select distinct panel from w10)
							and variable in (select id from @extschema@.t_variable where target_variable = (select distinct w2.target_variable from w2))
							and reference_year_set in (select distinct w4.t_reference_year_set__id from w4)
						)
						and plot in (select distinct w10.plot from w10)
						and is_latest = true
						) as ttd
					full outer join w10
							on (ttd.plot = w10.plot and ttd.available_datasets = w10.available_datasets)
					)
		,w_update as	(
						update @extschema@.t_target_data set is_latest = false where id in
						(select id from w_ttd where value_identic = false and id is not null)
						returning t_target_data.plot, t_target_data.available_datasets
						)
		insert into @extschema@.t_target_data(plot,value,value_inserted,is_latest,available_datasets)
		select
				w_ttd.plot_upr as plot,
				w_ttd.value_upr as value,
				now() as value_inserted,
				true as is_latest,
				available_datasets_upr
		from
				w_ttd
				left join w_update on w_ttd.plot_upr = w_update.plot and w_ttd.available_datasets_upr = w_update.available_datasets
		where
				w_ttd.value_identic = false
		order
				by w_ttd.plot_upr, w_ttd.available_datasets_upr;

	_res_ttd := concat('The ',(select count(*) from @extschema@.t_target_data where id > _max_id_ttd),' new local densities were inserted for ',(select count(t.plot) from (select distinct plot from @extschema@.t_target_data where id > _max_id_ttd) as t),' plots.');
	_res_tad := concat('The ',(select count(*) from @extschema@.t_available_datasets where id > _max_id_tad),' new rows were inserted into t_available_datasets.');
			
	_res := _res_ttd || ' ' || _res_tad;

	return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function @extschema@.fn_etl_import_ldsity_values(json) IS
'The function for the specified list of input arguments inserts data into the t_available_datasets table and inserts data into the t_target_data table (aggregated local density at the plot level).';

grant execute on function @extschema@.fn_etl_import_ldsity_values(json) to public;

-- </function>

alter table  @extschema@.c_estimation_cell add constraint plot_cell_associations_label_key unique (label);
