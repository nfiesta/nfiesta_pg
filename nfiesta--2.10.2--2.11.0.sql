--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

alter table     @extschema@.c_variable_type rename TO c_topic;
alter sequence  @extschema@.c_variable_type_id_seq rename to c_topic_id_seq;
alter index     @extschema@.pkey__c_variable_type rename to pkey__c_topic;
alter index     @extschema@.c_variable_type_label_key rename to c_topic_key;

comment on table @extschema@.c_topic is                'Table of topics';
comment ON column @extschema@.c_topic.id is             'Identifier of topic, primary key.';
comment ON column @extschema@.c_topic.label is          'Label of topic.';
comment ON column @extschema@.c_topic.description is    'Description of topic.';
comment ON column @extschema@.c_topic.label_en is       'Label of topic.';
comment ON column @extschema@.c_topic.description_en is 'Description of topic.';

alter table     @extschema@.cm_tvariable2variable_type rename TO cm_tvariable2topic;
alter table     @extschema@.cm_tvariable2topic rename COLUMN variable_type TO topic;
alter sequence  @extschema@.cm_tvariable2variable_type_id_seq rename to cm_tvariable2topic_id_seq;
alter index     @extschema@.pkey__cm_tvariable2variable_type__id rename to pkey__cm_tvariable2topic;
alter table     @extschema@.cm_tvariable2topic rename CONSTRAINT    fkey__tvariable2variable_type__c_target_variable
                                                                TO  fkey__tvariable2topic__c_target_variable;
alter table     @extschema@.cm_tvariable2topic rename CONSTRAINT    fkey__tvariable2variable_type__c_variable_type 
                                                                TO  fkey__tvariable2topic__c_topic;

comment on Table    @extschema@.cm_tvariable2topic is 'Mapping between target variable and topic.';
comment ON column   @extschema@.cm_tvariable2topic.topic is 'Topic, foreign key to c_topic.';