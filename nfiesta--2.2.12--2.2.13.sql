--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

DROP FUNCTION IF EXISTS @extschema@.fn_raise_notice(text);
-- <function name="fn_raise_notice.sql" schema="extschema" src="functions/extschema/fn_raise_notice.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

DROP FUNCTION IF EXISTS @extschema@.fn_raise_notice(text, text);
create or replace function @extschema@.fn_raise_notice(message text, type_of_msg text DEFAULT 'notice') returns boolean as $$
begin
	if type_of_msg = 'notice' then
		raise notice '%	%', clock_timestamp(), message;
		return true;
	elsif type_of_msg = 'exception' then
		raise exception '%>%', clock_timestamp(), message;
		return true;
	else
		raise exception 'type_of_msg not known';
		return false;
	end if;
end;
$$ language plpgsql;
--select @extschema@.fn_raise_notice('start of ETL')

-- </function>

-- <function name="fn_2p_data.sql" schema="extschema" src="functions/extschema/fn_2p_data.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_2p_data(integer)

-- DROP FUNCTION @extschema@.fn_2p_data(integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_2p_data(
    IN conf_id integer
)
  RETURNS TABLE(
	gid bigint,
	cluster integer,
	attribute integer,
	stratum integer,
	plots_per_cluster integer,
	plcount bigint,
	cluster_is_in_cell boolean,
	ldsity_d double precision,
	ldsity_d_plus double precision,
	is_aux boolean,
	is_target boolean,
	geom geometry(MultiPoint),
	ldsity_res_d double precision,
	ldsity_res_d_plus double precision,
        pix double precision,
        sweight double precision,
        DELTA_T__G_beta double precision,
        nb_sampling_units integer, 
        sweight_strata_sum double precision, 
        lambda_d_plus double precision,
        sigma boolean
) AS
$BODY$
DECLARE
	_complete_query text;
BEGIN
	--------------------------------QUERY--------------------------------
	_complete_query := '
WITH
---------------------------------------------------------
-- Configuration block
---------------------------------------------------------
w_configuration AS MATERIALIZED (
	with w_a AS MATERIALIZED (
		select
			t_total_estimate_conf.estimation_cell, t_aux_conf.model, 
			t_aux_conf.sigma, t_total_estimate_conf.force_synthetic, t_aux_conf.param_area,
			array_agg(t_total_estimate_conf.id order by t_total_estimate_conf.target_variable) as t_total_estimate_conf__id,
			array_agg(t_aux_conf.id order by t_total_estimate_conf.target_variable) as t_aux_conf__id,
			array_agg(t_total_estimate_conf.target_variable order by t_total_estimate_conf.target_variable) as target_attributes,
			t_total_estimate_conf.aux_conf
		from @extschema@.t_total_estimate_conf
		inner join @extschema@.t_aux_conf ON t_aux_conf.id = t_total_estimate_conf.aux_conf
		where t_total_estimate_conf.id = ' || conf_id || '
		group by t_total_estimate_conf.estimation_cell, t_aux_conf.model, t_aux_conf.sigma, t_total_estimate_conf.force_synthetic,
			t_aux_conf.param_area, t_total_estimate_conf.aux_conf
	)
	select
		t_total_estimate_conf__id[1] as id,
		w_a.t_total_estimate_conf__id, w_a.t_aux_conf__id,
		w_a.param_area, w_a.estimation_cell as cell, w_a.model, w_a.sigma, w_a.force_synthetic,
		array_agg(t_model_variables.variable order by t_model_variables.variable) as aux_attributes, w_a.target_attributes
	from w_a
	inner join @extschema@.t_aux_conf ON t_aux_conf.id = w_a.aux_conf
	inner join @extschema@.t_model ON t_model.id = t_aux_conf.model
	inner join @extschema@.t_model_variables ON t_model_variables.model = t_model.id
	group by w_a.estimation_cell, w_a.model, w_a.sigma, w_a.force_synthetic, w_a.param_area, w_a.target_attributes, w_a.t_total_estimate_conf__id, w_a.t_aux_conf__id
	order by id limit 1
)
, w_param_area_selection AS MATERIALIZED (
	SELECT
		w_configuration.id as conf_id,
		gid as param_area_gid,
		geom
	FROM @extschema@.f_a_param_area
	INNER JOIN w_configuration ON (w_configuration.param_area = f_a_param_area.gid)
)
, w_cell_selection AS MATERIALIZED (
	SELECT
		w_configuration.id as conf_id,
		estimation_cell as estimation_cell,
		geom
	FROM @extschema@.f_a_cell
	INNER JOIN w_configuration ON (w_configuration.cell = f_a_cell.estimation_cell)
)
---------------------------------------------------------
-- Data block
---------------------------------------------------------
, w_plot AS MATERIALIZED (
	select distinct
		f_p_plot.gid, w_configuration.id as conf_id, t_cluster.id as cluster, t_panel.stratum,
		case when w_configuration.force_synthetic = True then False else cm_plot2cell_mapping.id IS NOT NULL end AS plot_is_in_cell, 
		-- t_panel2aux_conf.reference_year_set as reference_year_set, t_panel2aux_conf.panel as panel,
		t_panel2total_2ndph_estimate_conf.reference_year_set as reference_year_set, t_panel2total_2ndph_estimate_conf.panel as panel,
		f_p_plot.geom
	from w_configuration
        inner join @extschema@.t_panel2total_2ndph_estimate_conf on w_configuration.t_total_estimate_conf__id[1] = t_panel2total_2ndph_estimate_conf.total_estimate_conf
	--inner join @extschema@.t_panel2aux_conf on w_configuration.t_aux_conf__id[1] = t_panel2aux_conf.aux_conf
	inner join @extschema@.t_panel ON (t_panel.id = t_panel2total_2ndph_estimate_conf.panel)
	inner join @extschema@.cm_cluster2panel_mapping ON cm_cluster2panel_mapping.panel = t_panel.id
	inner join @extschema@.t_cluster ON t_cluster.id = cm_cluster2panel_mapping."cluster"
	inner join @extschema@.f_p_plot ON f_p_plot."cluster" = t_cluster.id
	left join @extschema@.cm_plot2cell_mapping ON (cm_plot2cell_mapping.estimation_cell = w_configuration.cell and cm_plot2cell_mapping.plot = f_p_plot.gid)
	inner join @extschema@.cm_plot2param_area_mapping ON (cm_plot2param_area_mapping.param_area = w_configuration.param_area and cm_plot2param_area_mapping.plot = f_p_plot.gid)
)
, w_ldsity_plot AS MATERIALIZED (
	with w_plot AS MATERIALIZED (select * from w_plot)
		select
			w_plot.gid,
			w_plot.conf_id,
			w_plot.stratum,
			w_plot.panel,
			w_plot.cluster,
			t_variable.id as attribute,
			t_cluster_configuration.plots_per_cluster,
			w_plot.plot_is_in_cell,
			t_auxiliary_data.value as ldsity,
			true as is_aux, false as is_target,
			w_plot.geom
		from w_plot as w_plot
		inner join @extschema@.t_auxiliary_data on w_plot.gid = t_auxiliary_data.plot 
		inner join @extschema@.t_variable on t_variable.auxiliary_variable_category = t_auxiliary_data.auxiliary_variable_category
		inner join w_configuration on t_variable.id = ANY (w_configuration.aux_attributes)
		inner join @extschema@.cm_plot2cluster_config_mapping on w_plot.gid = cm_plot2cluster_config_mapping.plot
		inner join @extschema@.t_cluster_configuration on cm_plot2cluster_config_mapping.cluster_configuration = t_cluster_configuration.id
	union all
		select
			w_plot.gid,
			w_plot.conf_id,
			w_plot.stratum,
			w_plot.panel,
			w_plot.cluster,
			t_variable.id as attribute,
			t_cluster_configuration.plots_per_cluster,
			w_plot.plot_is_in_cell,
			t_target_data.value as ldsity,
			false as is_aux, true as is_target,
			w_plot.geom
		from w_plot as w_plot
		inner join @extschema@.t_target_data on (w_plot.gid = t_target_data.plot and w_plot.reference_year_set = t_target_data.reference_year_set)
                inner join @extschema@.t_variable on (
			t_target_data.target_variable = t_variable.target_variable and
                        case when (t_variable.sub_population_category is not null) then
                                t_target_data.sub_population_category = t_variable.sub_population_category
                        else t_target_data.sub_population_category is null end and
                        case when (t_variable.area_domain_category is not null) then
                                t_target_data.area_domain_category = t_variable.area_domain_category
                        else t_target_data.area_domain_category is null end
	                )
		inner join w_configuration on t_variable.id = ANY (w_configuration.target_attributes)
		inner join @extschema@.cm_plot2cluster_config_mapping on w_plot.gid = cm_plot2cluster_config_mapping.plot
		inner join @extschema@.t_cluster_configuration on cm_plot2cluster_config_mapping.cluster_configuration = t_cluster_configuration.id
)
, w_ldsity_cluster AS MATERIALIZED (
	SELECT
		w_ldsity_plot.conf_id,
		concat(w_ldsity_plot.conf_id, ROW_NUMBER() OVER ())::bigint as gid,
		w_ldsity_plot.cluster,
		w_ldsity_plot.attribute,
		w_ldsity_plot.stratum,
		w_ldsity_plot.panel,
		w_ldsity_plot.plots_per_cluster,
		count(*) as plcount,
		sum(w_ldsity_plot.plot_is_in_cell::int) > 0 as cluster_is_in_cell,
		sum(w_ldsity_plot.ldsity * plot_is_in_cell::int)/w_ldsity_plot.plots_per_cluster AS ldsity_D,
		sum(w_ldsity_plot.ldsity)/w_ldsity_plot.plots_per_cluster AS ldsity_D_plus,
		w_ldsity_plot.is_aux, w_ldsity_plot.is_target,
		st_collect(w_ldsity_plot.geom)::geometry(MultiPoint) as geom
	FROM       w_ldsity_plot
	GROUP BY conf_id, stratum, panel, cluster, plots_per_cluster, attribute, is_aux, is_target
	ORDER BY stratum, cluster, attribute
)
---------------------------------------------------------
-- Matrix block
---------------------------------------------------------
, w_X AS MATERIALIZED (
	SELECT
		conf_id,
		attribute AS r,
		cluster AS c,
		ldsity_D as val_D,
		ldsity_D_plus as val
	FROM w_ldsity_cluster
	WHERE is_aux
)
, w_Y AS MATERIALIZED (
	SELECT
		conf_id,
		attribute AS r,
		cluster AS c,
		ldsity_D as val_D,
		ldsity_D_plus as val
	FROM w_ldsity_cluster
	WHERE is_target
)
, w_clusters AS MATERIALIZED (-------------------------LIST OF CLUSTERS
	select distinct conf_id, stratum, panel, cluster, cluster_is_in_cell from w_ldsity_cluster
)
, w_strata_sum AS MATERIALIZED (
	select
		w_configuration.id as conf_id,
                t_stratum.id as f_a_sampling_stratum_gid, coalesce(buffered_area_m2, area_m2)/10000 as lambda_d_plus, plots_per_cluster,
		count(*) as nb_sampling_units, sum (sampling_weight) as sweight_strata_sum
	from w_configuration
	inner join @extschema@.t_aux_conf on t_aux_conf.id = ANY (w_configuration.t_aux_conf__id)
	inner join @extschema@.t_panel2aux_conf ON t_panel2aux_conf.aux_conf = t_aux_conf.id
	inner join @extschema@.t_panel ON t_panel.id = t_panel2aux_conf.panel
	inner join @extschema@.t_cluster_configuration ON t_panel.cluster_configuration = t_cluster_configuration.id
	inner join @extschema@.t_stratum ON t_stratum.id = t_panel.stratum
	inner join @extschema@.cm_cluster2panel_mapping ON cm_cluster2panel_mapping.panel = t_panel.id
	group by w_configuration.id, f_a_sampling_stratum_gid, lambda_d_plus, plots_per_cluster
)
, w_pix AS MATERIALIZED (-------------------------INCLUSION DENSITY PIX
	SELECT
		w_clusters.conf_id,
		w_clusters.stratum,
		w_clusters.cluster,
		w_strata_sum.sweight_strata_sum / (w_strata_sum.lambda_d_plus * cm_cluster2panel_mapping.sampling_weight) as pix,
		cm_cluster2panel_mapping.sampling_weight as sweight
	FROM w_clusters
	INNER JOIN @extschema@.cm_cluster2panel_mapping ON (w_clusters.cluster = cm_cluster2panel_mapping.cluster 
								and w_clusters.panel = cm_cluster2panel_mapping.panel)
	INNER JOIN w_strata_sum as w_strata_sum ON w_clusters.stratum = w_strata_sum.f_a_sampling_stratum_gid
)
, w_I AS MATERIALIZED (
	SELECT 
		conf_id,
		1 AS r,
		cluster AS c,
		cluster_is_in_cell::int AS val
	FROM	w_clusters
	order by r, c
)
, w_SIGMA AS MATERIALIZED (
	SELECT
		conf_id,
		1 as r,
		cluster as c,
		case when w_configuration.sigma then (plots_per_cluster^2)::float / plcount::float 
		else 1::float end
		as val
	from (select conf_id, cluster, plots_per_cluster, plcount 
		from w_ldsity_cluster 
		group by conf_id, cluster, plots_per_cluster, plcount) as m
	inner join w_configuration on (m.conf_id = w_configuration.id)
	order by r, c
)
, w_PI AS MATERIALIZED (
	SELECT
		conf_id,
		1 as r,
		cluster as c,
		1.0 / pix as val
	from w_pix
	order by r, c
)
, w_SIGMA_PI AS MATERIALIZED (
	SELECT
		A.conf_id,
		1 as r,
		A.c as c,
		A.val * B.val as val
	from w_SIGMA as A 
	inner join w_PI as B on (A.c = B.c and A.conf_id = B.conf_id) 
	order by r, c
)
, w_t_hat AS MATERIALIZED (  -- eq. 18
	select 
		A.conf_id,
		A.r,
		1 AS c,
		sum(A.val_D * B.val * C.val) as val
	from w_X 		AS A 
	inner join w_PI     	AS B 	on (A.c = B.c and A.conf_id = B.conf_id)
	inner join w_I		AS C 	on (A.c = C.c and A.conf_id = C.conf_id)
	group by A.r, A.conf_id
	order by A.r
)
, w_t AS MATERIALIZED (
	with w_aux_total as (
		select estimation_cell as cell, t_variable.id as attribute, aux_total
		from @extschema@.t_aux_total
		inner join @extschema@.t_variable on (t_aux_total.auxiliary_variable_category = t_variable.auxiliary_variable_category)
	)
	, w_aux_ldsity as (
		select distinct conf_id, r as attribute from w_X
	)
	select
		w_cell_selection.conf_id,
		w_aux_total.attribute as r,
		1 as c,
		coalesce(w_aux_total.aux_total,
			@extschema@.fn_raise_notice(
				format(''fn_2p_data.w_t: t_aux_total not found! (conf_id:%s attribute:%s)'',
					w_cell_selection.conf_id,
					w_aux_ldsity.attribute),
				''exception''
			)::int::float
		) as val
	from
	w_aux_ldsity
	inner join w_cell_selection on (w_aux_ldsity.conf_id = w_cell_selection.conf_id)
	left join w_aux_total on (w_aux_ldsity.attribute = w_aux_total.attribute and w_cell_selection.estimation_cell = w_aux_total.cell)
)
, w_DELTA_T AS MATERIALIZED (
	select
		conf_id,
		c as r, 
		r as c, 
		w_t.val - w_t_hat.val as val 
	from w_t 
	inner join w_t_hat using (r, c, conf_id)
)
, w_t_G_beta AS MATERIALIZED (
/*        select
                w_configuration.id as conf_id,
                (@extschema@.fn_g_beta(w_configuration.t_aux_conf__id[1])).*
        from w_configuration*/
        select
                w_configuration.id as conf_id,
                t_g_beta.r, t_g_beta.c, t_g_beta.val
        from @extschema@.t_g_beta
        inner join w_configuration on (t_g_beta.aux_conf = w_configuration.t_aux_conf__id[1])
)
, w_DELTA_G_beta AS MATERIALIZED (
	select
		A.conf_id,
		A.r,
		B.c, 
		sum(A.val * B.val) as val
	FROM 	w_DELTA_T as A
	inner join w_t_G_beta as B ON (A.c = B.r and A.conf_id = B.conf_id)
	GROUP BY A.r, B.c, A.conf_id
	order by r, c
)
, w_Y_T AS MATERIALIZED (
	SELECT
		conf_id,
		c as r,
		r as c,
		val,
		val_D -- used for 1p estimete
	from w_Y
	order by r, c
)
, w_G_PI AS MATERIALIZED (
	select 
		A.conf_id,
		A.r, 
		A.c AS c,
		A.val * B.val as val
	from w_t_G_beta AS A, w_PI AS B 
	where A.c = B.c and A.conf_id = B.conf_id
	order by r, c
)
, w_beta_hat AS MATERIALIZED (
	select
		A.conf_id,
		A.r,
		B.c, 
		sum(A.val * B.val) as val
	FROM w_G_PI as A, w_Y_T as B
	WHERE A.c = B.r and A.conf_id = B.conf_id
	GROUP BY A.r, B.c, A.conf_id
	order by r, c
)
, w_X_plots_T AS MATERIALIZED (
	SELECT conf_id, gid AS r, cluster, attribute AS c, plots_per_cluster, ldsity AS val 
	FROM w_ldsity_plot WHERE is_aux
)
, w_Y_plots_T AS MATERIALIZED (
	SELECT conf_id, gid AS r, cluster, attribute AS c, plots_per_cluster, ldsity AS val 
	FROM w_ldsity_plot WHERE is_target
)
, w_X_beta AS MATERIALIZED (
	select
		A.conf_id,
		A.r,
		B.c, 
		sum(A.val * B.val) as val
	FROM w_X_plots_T as A, w_beta_hat as B
	WHERE A.c = B.r and A.conf_id = B.conf_id
	GROUP BY A.r, B.c, A.conf_id
	order by r, c 
)
, w_residuals_plot AS MATERIALIZED ( -------------------------RESIDUAL LOCAL DENSITY ON TRACT LEVEL
	select
		w_Y_plots_T.conf_id,
		w_Y_plots_T.r AS plot, 
		w_Y_plots_T.c AS attribute, 
		(w_Y_plots_T.val - w_X_beta.val) AS ldsity 
	from w_Y_plots_T 
	INNER JOIN w_X_beta USING (r, c, conf_id)
)
, w_residuals_cluster AS MATERIALIZED (
	with
	w_residuals_plot AS MATERIALIZED (select * from w_residuals_plot),
	w_ldsity_plot AS MATERIALIZED (select * from w_ldsity_plot)
	SELECT
		w_ldsity_plot.conf_id,
		w_ldsity_plot.cluster,
		w_residuals_plot.attribute,
		w_ldsity_plot.stratum,
		w_ldsity_plot.plots_per_cluster,
		count(*) as plcount,
		sum(plot_is_in_cell::int) > 0 as cluster_is_in_cell,
		sum(w_residuals_plot.ldsity * plot_is_in_cell::int)/w_ldsity_plot.plots_per_cluster AS ldsity_res_D, --eq 38
		sum(w_residuals_plot.ldsity)/w_ldsity_plot.plots_per_cluster AS ldsity_res_D_plus -- eq 39
	FROM w_ldsity_plot AS w_ldsity_plot
	INNER JOIN w_residuals_plot AS w_residuals_plot ON w_ldsity_plot.gid = w_residuals_plot.plot AND w_ldsity_plot.attribute = w_residuals_plot.attribute and w_ldsity_plot.conf_id = w_residuals_plot.conf_id
	GROUP BY stratum, w_ldsity_plot.conf_id, w_ldsity_plot.cluster, w_ldsity_plot.plots_per_cluster, w_residuals_plot.attribute
	ORDER BY stratum, w_ldsity_plot.conf_id, w_ldsity_plot.cluster, w_residuals_plot.attribute
)
, w_ldsity_residuals_cluster AS MATERIALIZED (
	select 
		w_ldsity_cluster.conf_id, w_ldsity_cluster.gid, w_ldsity_cluster.cluster, 
		w_ldsity_cluster.attribute, w_ldsity_cluster.stratum, w_ldsity_cluster.plots_per_cluster, 
		w_ldsity_cluster.plcount, w_ldsity_cluster.cluster_is_in_cell, w_ldsity_cluster.ldsity_d, w_ldsity_cluster.ldsity_d_plus, 
		w_ldsity_cluster.is_aux, w_ldsity_cluster.is_target, w_ldsity_cluster.geom,
		w_residuals_cluster.ldsity_res_D, w_residuals_cluster.ldsity_res_D_plus
	from w_ldsity_cluster
	LEFT JOIN w_residuals_cluster USING (conf_id, cluster, attribute, stratum)
)
, w_2p_data AS MATERIALIZED (
	with w_ldsity_residuals_cluster AS MATERIALIZED (select * from w_ldsity_residuals_cluster)
	select 
		w_ldsity_residuals_cluster.conf_id, w_ldsity_residuals_cluster.gid, w_ldsity_residuals_cluster.cluster, 
		w_ldsity_residuals_cluster.attribute, w_ldsity_residuals_cluster.stratum, w_ldsity_residuals_cluster.plots_per_cluster, 
		w_ldsity_residuals_cluster.plcount, w_ldsity_residuals_cluster.cluster_is_in_cell, w_ldsity_residuals_cluster.ldsity_d, 
		w_ldsity_residuals_cluster.ldsity_d_plus, 
		w_ldsity_residuals_cluster.is_aux, w_ldsity_residuals_cluster.is_target, w_ldsity_residuals_cluster.geom,
		w_ldsity_residuals_cluster.ldsity_res_D, w_ldsity_residuals_cluster.ldsity_res_D_plus,
		w_pix.pix, w_pix.sweight, w_DELTA_G_beta.val as DELTA_T__G_beta, 
		w_strata_sum.nb_sampling_units::integer, w_strata_sum.sweight_strata_sum, w_strata_sum.lambda_d_plus
	from w_ldsity_residuals_cluster
	INNER JOIN w_pix USING (conf_id, cluster, stratum)
	INNER JOIN w_DELTA_G_beta ON (w_pix.conf_id = w_DELTA_G_beta.conf_id and cluster = w_DELTA_G_beta.c)
	inner join w_strata_sum on (stratum = f_a_sampling_stratum_gid)
)
, w_2p_data_sigma AS MATERIALIZED (
	select 
		w_2p_data.gid, w_2p_data.cluster, w_2p_data.attribute, w_2p_data.stratum, w_2p_data.plots_per_cluster, w_2p_data.plcount,
		w_2p_data.cluster_is_in_cell as cluster_is_in_cell, 
		w_2p_data.ldsity_d, w_2p_data.ldsity_d_plus, w_2p_data.is_aux, w_2p_data.is_target,
		w_2p_data.geom, w_2p_data.ldsity_res_d, w_2p_data.ldsity_res_d_plus, w_2p_data.pix, w_2p_data.sweight, w_2p_data.DELTA_T__G_beta,
		w_2p_data.nb_sampling_units, w_2p_data.sweight_strata_sum, w_2p_data.lambda_d_plus, w_configuration.sigma
	from w_2p_data 
	inner join w_configuration on (w_2p_data.conf_id = w_configuration.id)
)
select * from w_2p_data_sigma;
';

	--RAISE NOTICE '%', _complete_query;
	RETURN QUERY EXECUTE _complete_query;
END;
$BODY$
  LANGUAGE plpgsql STABLE
  COST 100
  ROWS 100000;

COMMENT ON FUNCTION @extschema@.fn_2p_data(integer) IS 'Function preparing data for regression estimate.';

-- </function>

-- <function name="fn_2p_est_configuration.sql" schema="extschema" src="functions/extschema/configuration/fn_2p_est_configuration.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_2p_est_configuration(integer, regclass)
--DROP FUNCTION @extschema@.fn_2p_est_configuration(integer,date,date,character varying,integer,integer);
CREATE OR REPLACE FUNCTION @extschema@.fn_2p_est_configuration(_estimation_cell integer, _estimate_date_begin date, _estimate_date_end date, _note varchar, _target_variable integer, _aux_conf integer, _force_synthetic boolean default False)
RETURNS integer
AS
$function$
DECLARE
_total_estimate_conf		integer;
_stratas			integer[];
_stratas_wp			integer[];
_panels				integer[];
_refyearsets			integer[];
_panels_aux			integer[];
_param_area			integer;
_param_area_code		varchar;
_target_label			varchar;
_model				integer;
_cell				varchar;
BEGIN

-- test for existing g_betas
-- otherwise the configuration cannot be done (sometimes the g_betas cannot be computed)
-- so this prevents to configure non-computable estimates

IF (SELECT count(*) FROM @extschema@.t_g_beta WHERE aux_conf = $6) = 0
THEN
	RAISE EXCEPTION 'G-betas for required aux_conf are not available. The computation of it was not run or is not able to compute (mostly the problem of matrix inversion).';
END IF;

-- create the label of estimate
_target_label := (
		SELECT		replace(
					replace(
						concat(coalesce(t2.label,'x'), ',', coalesce(t3.label,'x'), ',', coalesce(t4.label,'x'), ',', coalesce(t5.label,'x')),
					'x,',''),
			       	',x','') AS label
		FROM		@extschema@.t_variable AS t1
		LEFT JOIN 	@extschema@.c_target_variable AS t2 ON t1.target_variable = t2.id
		LEFT JOIN	@extschema@.c_sub_population_category AS t3 ON t1.sub_population_category = t3.id
		LEFT JOIN	@extschema@.c_area_domain_category AS t4 ON t1.area_domain_category = t4.id
		LEFT JOIN	@extschema@.c_auxiliary_variable_category AS t5 ON t1.auxiliary_variable_category = t5.id
		WHERE
			t1.id = $5
		);

_param_area := (SELECT param_area FROM @extschema@.t_aux_conf WHERE id = $6);
_model := (SELECT model FROM @extschema@.t_aux_conf WHERE id = $6);
_param_area_code := (SELECT param_area_code FROM @extschema@.f_a_param_area WHERE gid = _param_area);
_cell := (SELECT label FROM @extschema@.c_estimation_cell WHERE id = $1);

-- reset sequence (usually when previous atempt to configure fails, the sequence remains shifted)
PERFORM setval('@extschema@.t_total_estimate_conf_id_seq', (SELECT coalesce(max(id),0)+1 FROM @extschema@.t_total_estimate_conf), FALSE);

-- insert into table t_total_estimate_conf
INSERT INTO @extschema@.t_total_estimate_conf (estimation_cell, estimate_date_begin, estimate_date_end, total_estimate_conf, target_variable, phase_estimate_type, force_synthetic, aux_conf)
VALUES
	($1, $2, $3, concat('2p;T=',_target_label,';Cell=',_cell,';PA_code=',_param_area_code, ';model=',_model,_note), $5, 2, $7, $6)
RETURNING id
INTO _total_estimate_conf;

-- test on param_area_coverage
	SELECT
		array_agg(t1.id ORDER BY t1.id)
	FROM
		@extschema@.t_stratum AS t1
	INNER JOIN
		@extschema@.f_a_param_area AS t2
	ON
		-- buffered stratum?
		-- no, if only buffer of the stratum would intersect the cell, 
		-- no consequences on estimate would be introduced, the buffer is in general used only for the edge effect compensation (the compensated area is within non-buffered stratum)
		ST_Intersects(t1.geom, t2.geom)
	WHERE
		t2.gid = _param_area
	INTO _stratas;

	IF _stratas IS NULL
	THEN
		RAISE EXCEPTION 'The specified cell is not intersected by any stratum. Choose another estimation cell.';
	END IF;

-- existing panels configured in panel2aux_conf
	SELECT
		array_agg(panel ORDER BY panel)
	FROM
		@extschema@.t_panel2aux_conf AS t1
	WHERE
		t1.aux_conf = $6
	INTO _panels_aux;


-- check of panel2total_2ndph
-- and addition of panels from param_area - is the target variable available not only in cell?

	WITH w_data AS MATERIALIZED (
		SELECT
			t1.id AS stratum, t2.id AS panel, t6.reference_year_set, count(*) AS total
		FROM
			@extschema@.t_stratum AS t1
		INNER JOIN
			@extschema@.t_panel AS t2
		ON
			t1.id = t2.stratum
		INNER JOIN
			@extschema@.cm_cluster2panel_mapping AS t3
		ON
			t2.id = t3.panel
		INNER JOIN
			@extschema@.t_cluster AS t4
		ON
			t3.cluster = t4.id
		INNER JOIN
			@extschema@.f_p_plot AS t5
		ON
			t4.id = t5.cluster
		INNER JOIN
			@extschema@.t_target_data AS t6
		ON
			t5.gid = t6.plot
		INNER JOIN
			@extschema@.t_variable AS t7
		ON
			t6.target_variable = t7.target_variable AND
			CASE WHEN t6.area_domain_category IS NULL THEN t7.area_domain_category IS NULL
			ELSE t6.area_domain_category = t7.area_domain_category END AND
			CASE WHEN t6.sub_population_category IS NULL THEN t7.sub_population_category IS NULL
			ELSE t6.sub_population_category = t7.sub_population_category END
		INNER JOIN
			@extschema@.t_reference_year_set AS t8
		ON
			t6.reference_year_set = t8.id
		INNER JOIN
			@extschema@.cm_refyearset2panel_mapping AS t9
		ON
			t2.id = t9.panel AND
			t8.id = t9.reference_year_set
		WHERE
			array[t1.id] <@ _stratas AND
			t7.id = $5 AND 
			(t8.reference_date_begin >= $2 AND
			t8.reference_date_end <= $3)
		GROUP BY
			t1.id, t2.id, t6.reference_year_set
	)
	SELECT
		array_agg(DISTINCT stratum ORDER BY stratum) AS stratas,
		array_agg(panel ORDER BY panel) AS panels,
		array_agg(reference_year_set ORDER BY panel) AS refyearsets
	FROM
		(SELECT
			stratum, panel, reference_year_set,
			total,
			max(total) OVER(PARTITION BY stratum, panel, reference_year_set) AS max_total
		FROM
			w_data
		) AS t1
	WHERE
		-- pick up the most dense panel with target variable
		total = max_total
	INTO _stratas_wp, _panels, _refyearsets;

	IF _panels != _panels_aux OR _panels IS NULL
	THEN
		RAISE EXCEPTION 'Not all panels coming from g_beta have available target variable! total_estimate_conf: %, panels: %, panels_aux: %', _total_estimate_conf, _panels, _panels_aux;
	END IF;

-- insert into table t_panel2total_2ndph_estimate_conf
INSERT INTO @extschema@.t_panel2total_2ndph_estimate_conf (total_estimate_conf, panel, reference_year_set)
SELECT
	_total_estimate_conf, panel, reference_year_set
FROM
	unnest(_panels) WITH ORDINALITY AS t1(panel, id)
INNER JOIN
	unnest(_refyearsets) WITH ORDINALITY AS t2(reference_year_set,id)
ON
	t1.id = t2.id;

-- insert into table t_estimate_conf
INSERT INTO @extschema@.t_estimate_conf (estimate_type, total_estimate_conf, denominator)
SELECT 1, _total_estimate_conf, NULL;

IF	(
		select
			array_agg(t_variable.id order by t_variable.id)
		from @extschema@.t_aux_total
		inner join @extschema@.t_variable on (t_aux_total.auxiliary_variable_category = t_variable.auxiliary_variable_category)
		inner join @extschema@.c_estimation_cell on (t_aux_total.estimation_cell = c_estimation_cell.id)
		where c_estimation_cell.id = $1
	)
	!= (
		select
			array_agg(variable order by variable)
		from @extschema@.t_aux_conf
		inner join @extschema@.t_model ON t_model.id = t_aux_conf.model
		inner join @extschema@.t_model_variables ON t_model_variables.model = t_model.id
		where t_aux_conf.id = $6
	)
THEN
	RAISE WARNING 'fn_2p_est_configuration: t_aux_total not found! (total_estimate_conf: %)', _total_estimate_conf;
END IF;

RETURN _total_estimate_conf;


END;
$function$
LANGUAGE plpgsql
PARALLEL SAFE;

--COMMENT ON FUNCTION @extschema@.fn_2p_est_configuration() IS '.';

-- </function>
